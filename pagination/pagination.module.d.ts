import { ModuleWithProviders } from '@angular/core';
export { NgtPagination, NgtPaginationEllipsis, NgtPaginationFirst, NgtPaginationLast, NgtPaginationNext, NgtPaginationNumber, NgtPaginationPrevious } from './pagination';
export { NgtPaginationConfig } from './pagination-config';
export declare class NgtPaginationModule {
    static forRoot(): ModuleWithProviders;
}
