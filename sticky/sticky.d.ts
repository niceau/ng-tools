import { ElementRef, EventEmitter, OnInit, AfterViewInit, Renderer2 } from '@angular/core';
import { NgtStickyConfig } from './sticky-config';
export declare class NgtSticky implements OnInit, AfterViewInit {
    private element;
    private _renderer;
    /**
     * Makes an element sticky
     *
     *  <sticky>Sticky element</sticky>
     *  <div sticky>Sticky div</div>
     */
    sticky: string;
    /**
     * Controls z-index CSS parameter of the sticky element
     */
    zIndex: number;
    /**
     * Width of the sticky element
     */
    width: string;
    /**
     * Pixels between the top of the page or container and the element
     */
    offsetTop: number;
    /**
     *  Pixels between the bottom of the page or container and the element
     */
    offsetBottom: number;
    /**
     * Position where the element should start to stick
     */
    start: number;
    /**
     * CSS class that will be added after the element starts sticking
     */
    stickClass: string;
    /**
     * CSS class that will be added to the sticky element after it ends sticking
     */
    endStickClass: string;
    /**
     * Media query that allows to use sticky
     */
    mediaQuery: string;
    /**
     * If true, then the sticky element will be stuck relatively to the parent containers. Otherwise, window will be used as the parent container.
     * NOTE: the "position: relative" styling is added to the parent element automatically in order to use the absolute positioning
     */
    parentMode: boolean;
    /**
     * Orientation for sticky element ("left", "right", "none")
     */
    orientation: string;
    activated: EventEmitter<{}>;
    deactivated: EventEmitter<{}>;
    reset: EventEmitter<{}>;
    isStuck: boolean;
    private elem;
    private container;
    private originalCss;
    private windowHeight;
    private containerHeight;
    private containerWidth;
    private containerTop;
    private elemHeight;
    private containerStart;
    private scrollFinish;
    onChange($event: any): void;
    constructor(element: ElementRef, _renderer: Renderer2, config: NgtStickyConfig);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    defineOriginalDimensions(): void;
    defineYDimensions(): void;
    defineXDimensions(): void;
    resetElement(): void;
    stuckElement(): void;
    unstuckElement(): void;
    matchMediaQuery(): any;
    sticker(): void;
    private scrollbarYPos;
    private getBoundingClientRectValue;
    private getCssValue;
    private getCssNumber;
    private setStyles;
}
