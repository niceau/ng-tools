/**
 * Configuration service for the NgtSticky component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the stickies used in the application.
 */
export declare class NgtStickyConfig {
    sticky: string;
    zIndex: number;
    width: string;
    offsetTop: number;
    offsetBottom: number;
    start: number;
    stickClass: string;
    endStickClass: string;
    mediaQuery: string;
    parentMode: boolean;
    orientation: 'left' | 'right' | 'none';
}
