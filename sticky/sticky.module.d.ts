import { ModuleWithProviders } from '@angular/core';
export { NgtSticky } from './sticky';
export { NgtStickyConfig } from './sticky-config';
export declare class NgtStickyModule {
    static forRoot(): ModuleWithProviders;
}
