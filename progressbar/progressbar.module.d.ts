import { ModuleWithProviders } from '@angular/core';
export { NgtProgressbar } from './progressbar';
export { NgtProgressbarConfig } from './progressbar-config';
export declare class NgtProgressbarModule {
    static forRoot(): ModuleWithProviders;
}
