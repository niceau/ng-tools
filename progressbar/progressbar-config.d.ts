/**
 * Configuration service for the NgtProgressbar component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the progress bars used in the application.
 */
export declare class NgtProgressbarConfig {
    max: number;
    animated: boolean;
    striped: boolean;
    type: string;
    showValue: boolean;
    height: string;
}
