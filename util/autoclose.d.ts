import { NgZone } from '@angular/core';
import { Observable } from 'rxjs';
export declare function ngtAutoClose(zone: NgZone, document: any, type: boolean | 'inside' | 'outside', close: () => void, closed$: Observable<any>, insideElements: HTMLElement[], ignoreElements?: HTMLElement[]): void;
