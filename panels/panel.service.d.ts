import { Subject } from 'rxjs';
import { NgtPanel } from './panel.model';
export declare class NgtPanelService {
    private panels;
    activePanels: any[];
    panelWillOpened: Subject<string>;
    panelWillClosed: Subject<string>;
    panelClosingDidStart: Subject<{}>;
    panelClosingDidDone: Subject<{}>;
    panelOpeningDidStart: Subject<{}>;
    panelOpeningDidDone: Subject<{}>;
    isPanelsChanged: Subject<{}>;
    isPanelHide: boolean;
    isPanelExpand: boolean;
    panelState: string;
    stateEvents: {
        left: {
            expand: Subject<boolean>;
            hide: Subject<boolean>;
        };
        right: {
            expand: Subject<boolean>;
            hide: Subject<boolean>;
        };
    };
    constructor();
    add(panel: NgtPanel): void;
    remove(id: string): void;
    addToActive(id: string): void;
    removeFromActive(id: string): void;
    openPanel(id: string): void;
    closePanel(id?: string): void;
    updateState(): void;
}
