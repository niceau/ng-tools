import { ElementRef } from '@angular/core';
export declare class NgtPanel {
    id: string;
    el: ElementRef;
    constructor(id: string, el: ElementRef);
}
