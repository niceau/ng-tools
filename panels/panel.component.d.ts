import { ElementRef, Injector, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgtPanelService } from './panel.service';
import { NgtPanelConfigInterface } from './panel.config';
export declare class NgtPanelComponent implements OnInit, OnDestroy {
    private _panelService;
    private _elRef;
    id: string;
    dir: string;
    private _isOpen;
    statuses: {
        left: {
            expanded: {
                open: string;
                close: string;
            };
            collapsed: {
                open: string;
                close: string;
            };
            hidden: {
                open: string;
                close: string;
            };
        };
        right: {
            expanded: {
                open: string;
                close: string;
            };
            collapsed: {
                open: string;
                close: string;
            };
            hidden: {
                open: string;
                close: string;
            };
        };
    };
    openStatuses: string[];
    closeStatuses: string[];
    subscriptions: Array<Subscription>;
    openState: string;
    closeState: string;
    styles: {};
    element: any;
    el: ElementRef;
    config: NgtPanelConfigInterface;
    isOpen: boolean;
    constructor(_panelService: NgtPanelService, _elRef: ElementRef, injector: Injector);
    ngOnInit(): void;
    ngOnDestroy(): void;
    open(): void;
    close(): void;
    updateStates(dir: string): void;
    setStyle(i: number): void;
    animationAction(event: any): void;
}
