import { NgtPanelService } from './panel.service';
export declare class NgtPanelOpenDirective {
    private _panelService;
    id: string;
    constructor(_panelService: NgtPanelService);
    onClick(e: any): void;
}
