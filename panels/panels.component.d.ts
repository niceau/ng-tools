import { ElementRef, OnDestroy, OnInit } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { NgtPanelService } from './panel.service';
export declare class NgtPanelsComponent implements OnInit, OnDestroy {
    private _panelService;
    private elRef;
    overlay: boolean;
    leftPanelExpand: Subject<boolean>;
    leftPanelHide: Subject<boolean>;
    rightPanelExpand: Subject<boolean>;
    rightPanelHide: Subject<boolean>;
    subscriptions: Array<Subscription>;
    private _isOpen;
    onClick(event: any): void;
    isOpen: boolean;
    constructor(_panelService: NgtPanelService, elRef: ElementRef);
    ngOnInit(): void;
    ngOnDestroy(): void;
    onClickOutside(e: any): void;
}
