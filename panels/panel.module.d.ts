import { ModuleWithProviders } from '@angular/core';
export { NgtPanelService } from './panel.service';
export { NgtPanelComponent } from './panel.component';
export { NgtPanelsComponent } from './panels.component';
export { NgtPanelOpenDirective } from './panel-open.directive';
export { NgtPanelCloseDirective } from './panel-close.directive';
export { NgtPanel } from './panel.model';
export { NGC_PANEL_CONFIG, DEFAULT_NGC_PANEL_CONFIG, NgtPanelConfigInterface } from './panel.config';
export declare class NgtPanelModule {
    static forRoot(): ModuleWithProviders;
}
