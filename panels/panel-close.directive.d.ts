import { NgtPanelService } from './panel.service';
export declare class NgtPanelCloseDirective {
    private _panelService;
    id: string;
    constructor(_panelService: NgtPanelService);
    onClick(e: any): void;
}
