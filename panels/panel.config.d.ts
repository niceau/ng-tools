import { InjectionToken } from '@angular/core';
export declare const NGC_PANEL_CONFIG: InjectionToken<{}>;
export declare const DEFAULT_NGC_PANEL_CONFIG: NgtPanelConfigInterface;
export interface NgtPanelConfigInterface {
    leftPanelExpandedShift?: number;
    leftPanelCollapsedShift?: number;
    rightPanelExpandedShift?: number;
    rightPanelCollapsedShift?: number;
}
export declare class NgtPanelConfig implements NgtPanelConfigInterface {
    leftPanelExpandedShift?: number;
    leftPanelCollapsedShift?: number;
    rightPanelExpandedShift?: number;
    rightPanelCollapsedShift?: number;
    constructor(config?: NgtPanelConfigInterface);
    assign(config?: NgtPanelConfigInterface): void;
}
