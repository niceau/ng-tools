import { ComponentRef } from '@angular/core';
export declare class NgtNotification {
    type: NgtNotificationType;
    message: string;
    timeout: number;
    typeClass: string;
    aside: boolean;
    title: string;
    _ref: ComponentRef<NgtNotification>;
}
export declare enum NgtNotificationType {
    Success = 0,
    Error = 1,
    Info = 2,
    Warning = 3
}
