import { AfterViewInit, ElementRef, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
export declare class NgtNotificationComponent implements OnInit, AfterViewInit {
    private elRef;
    animation: boolean;
    display: string;
    type: any;
    typeClass: string;
    title: string;
    message: string;
    aside: boolean;
    timeout: any;
    _ref: any;
    timer: Subject<number>;
    progressBar: ElementRef;
    interval: any;
    progress: number;
    lastProgress: number;
    private _resolve;
    private _reject;
    /**
     * A promise that is resolved when the notification is closed
     */
    result: Promise<any>;
    constructor(elRef: ElementRef);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    initProgressBar(): void;
    closeNotification(): void;
    onMouseenter(): void;
    onMouseleave(): void;
}
