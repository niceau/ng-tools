import { ApplicationRef, ComponentFactoryResolver, Injector, RendererFactory2 } from '@angular/core';
import { NgtNotification } from './notification.model';
export declare class NgtNotificationStack {
    private _injector;
    private _appRef;
    private _rendererFactory;
    private _document;
    containerEl: any;
    private _notificationRefs;
    private _notificationAttributes;
    constructor(_injector: Injector, _appRef: ApplicationRef, _rendererFactory: RendererFactory2, _document: any);
    initContainer(): void;
    show(moduleCFR: ComponentFactoryResolver, options: NgtNotification): void;
    clearAll(): void;
    private _attachNotificationComponent;
    private _applyNotificationOptions;
    private _registerNotificationRef;
}
