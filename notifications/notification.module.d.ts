import { ModuleWithProviders } from '@angular/core';
export { NgtNotificationService } from './notification.service';
export { NgtNotificationStack } from './notification-stack';
export { NgtNotificationComponent } from './notification.component';
export { NgtNotification } from './notification.model';
export declare class NgtNotificationModule {
    static forRoot(): ModuleWithProviders;
}
