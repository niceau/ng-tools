import { ComponentFactoryResolver } from '@angular/core';
import { NgtNotificationStack } from './notification-stack';
export declare class NgtNotificationService {
    private stack;
    private _moduleCFR;
    timeout: number;
    notificationsData: {
        success: {
            typeClass: string;
            aside: string;
        };
        error: {
            typeClass: string;
            aside: string;
        };
        info: {
            typeClass: string;
            aside: string;
        };
        warning: {
            typeClass: string;
            aside: string;
        };
    };
    constructor(stack: NgtNotificationStack, _moduleCFR: ComponentFactoryResolver);
    getInfo(type: any, kind: any): any;
    success(data: any): void;
    error(data: any): void;
    info(data: any): void;
    warn(data: any): void;
    notification(data: any): void;
    clear(): void;
}
