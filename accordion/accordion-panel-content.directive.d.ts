import { TemplateRef } from '@angular/core';
/**
 * This directive must be used to wrap accordion panel content.
 */
export declare class NgtAccordionPanelContentDirective {
    templateRef: TemplateRef<any>;
    constructor(templateRef: TemplateRef<any>);
}
