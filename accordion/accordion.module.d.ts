import { ModuleWithProviders } from '@angular/core';
export { NgtAccordionComponent } from './accordion.component';
export { NgtAccordionPanelDirective } from './accordion-panel.directive';
export { NgtAccordionPanelTitleDirective } from './accordion-panel-title.directive';
export { NgtAccordionPanelContentDirective } from './accordion-panel-content.directive';
export { NgtAccordionPanelHeaderDirective } from './accordion-panel-header.directive';
export { NgtAccordionPanelToggleDirective } from './accordion-panel-toggle.directive';
export { NgtAccordionPanelChangeEvent } from './accordion-panel-change-event';
export { NgtAccordionConfig } from './accordion-config';
export declare class NgtAccordionModule {
    static forRoot(): ModuleWithProviders;
}
