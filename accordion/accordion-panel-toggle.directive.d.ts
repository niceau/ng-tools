import { NgtAccordionPanelDirective } from './accordion-panel.directive';
import { NgtAccordionComponent } from './accordion.component';
/**
 * A directive to put on a button that toggles panel opening and closing.
 * To be used inside the `NgtAccordionPanelHeaderDirective`
 */
export declare class NgtAccordionPanelToggleDirective {
    accordion: NgtAccordionComponent;
    panel: NgtAccordionPanelDirective;
    ngtAccordionPanelToggle: NgtAccordionPanelDirective;
    onClick(): void;
    constructor(accordion: NgtAccordionComponent, panel: NgtAccordionPanelDirective);
}
