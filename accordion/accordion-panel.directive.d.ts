import { AfterContentChecked, OnInit, QueryList } from '@angular/core';
import { NgtAccordionPanelTitleDirective } from './accordion-panel-title.directive';
import { NgtAccordionPanelHeaderDirective } from './accordion-panel-header.directive';
import { NgtAccordionPanelContentDirective } from './accordion-panel-content.directive';
import { NgtAccordionService } from './accordion.service';
export declare class NgtAccordionPanelDirective implements OnInit, AfterContentChecked {
    private $accordionService;
    private _isOpen;
    id: string;
    title: string;
    bg: string;
    disabled: boolean;
    isOpen: boolean;
    titleTpl: NgtAccordionPanelTitleDirective | null;
    headerTpl: NgtAccordionPanelHeaderDirective | null;
    contentTpl: NgtAccordionPanelContentDirective | null;
    titleTpls: QueryList<NgtAccordionPanelTitleDirective>;
    headerTpls: QueryList<NgtAccordionPanelHeaderDirective>;
    contentTpls: QueryList<NgtAccordionPanelContentDirective>;
    constructor($accordionService: NgtAccordionService);
    ngOnInit(): void;
    ngAfterContentChecked(): void;
}
