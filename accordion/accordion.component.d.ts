import { AfterContentChecked, EventEmitter, OnInit, QueryList } from '@angular/core';
import { NgtAccordionPanelDirective } from './accordion-panel.directive';
import { NgtAccordionPanelChangeEvent } from './accordion-panel-change-event';
import { NgtAccordionConfig } from './accordion-config';
/**
 * The NgtAccordion directive is a collection of panels.
 * It can assure that only one panel can be opened at a time.
 */
export declare class NgtAccordionComponent implements OnInit, AfterContentChecked {
    panels: QueryList<NgtAccordionPanelDirective>;
    /**
     *  Whether the other panels should be closed when a panel is opened
     */
    closeOtherPanels: boolean;
    /**
     * An array or comma separated strings of panel identifiers that should be opened
     */
    activeIds: string | string[];
    /**
     *  Accordion's types of panels.
     *  System recognizes the following types: "light" and "outline"
     */
    type: string;
    /**
     *  Accordion's bg's of panels to be applied globally.
     *  System recognizes the following bg's: "primary", "secondary", "success", "danger", "warning", "info", "light" , "dark"
     *  and other utilities bg's
     */
    bg: string;
    /**
     * A panel change event fired right before the panel toggle happens. See NgtPanelChangeEvent for payload details
     */
    panelChange: EventEmitter<NgtAccordionPanelChangeEvent>;
    class: string;
    constructor(config: NgtAccordionConfig);
    ngOnInit(): void;
    /**
     * Checks if a panel with a given id is expanded or not.
     */
    isExpanded(panelId: string): boolean;
    /**
     * Expands a panel with a given id. Has no effect if the panel is already expanded or disabled.
     */
    expand(panelId: string): void;
    /**
     * Expands all panels if [closeOthers]="false". For the [closeOthers]="true" case will have no effect if there is an
     * open panel, otherwise the first panel will be expanded.
     */
    expandAll(): void;
    /**
     * Collapses a panel with a given id. Has no effect if the panel is already collapsed or disabled.
     */
    collapse(panelId: string): void;
    /**
     * Collapses all open panels.
     */
    collapseAll(): void;
    /**
     * Programmatically toggle a panel with a given id. Has no effect if the panel is disabled.
     */
    toggle(panelId: string): void;
    /**
     * Toggle all panels.
     */
    toggleAll(nextState?: boolean): void;
    ngAfterContentChecked(): void;
    private _changeOpenState;
    private _closeOthers;
    private _findPanelById;
    private _updateActiveIds;
}
