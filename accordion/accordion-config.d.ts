/**
 * Configuration service for the NgtAccordion component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the accordions used in the application.
 */
export declare class NgtAccordionConfig {
    closeOthers: boolean;
    type: string;
    bg: string;
}
