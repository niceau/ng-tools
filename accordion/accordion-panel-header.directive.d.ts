import { TemplateRef } from '@angular/core';
/**
 * A directive to wrap an accordion panel header to contain any HTML markup and a toggling button with `NgtAccordionPanelToggleDirective`
 */
export declare class NgtAccordionPanelHeaderDirective {
    templateRef: TemplateRef<any>;
    constructor(templateRef: TemplateRef<any>);
}
