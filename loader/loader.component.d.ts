import { OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { NgtLoaderService } from './loader.service';
export declare class NgtLoaderComponent implements OnInit, OnDestroy {
    private _loaderService;
    private _cdr;
    id: string;
    content: string;
    /**
     *  Spinner type: 'border' | 'grow';
     */
    type: string;
    /**
     *  Spinner extra class;
     */
    spinnerClass: string;
    private _isPresent;
    isPresent: boolean;
    constructor(_loaderService: NgtLoaderService, _cdr: ChangeDetectorRef);
    ngOnInit(): void;
    ngOnDestroy(): void;
    present(): Observable<boolean> | Promise<boolean>;
    dismiss(): Observable<boolean> | Promise<boolean>;
    animationDone(event: any): void;
}
