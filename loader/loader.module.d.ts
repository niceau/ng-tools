import { ModuleWithProviders } from '@angular/core';
export { NgtLoaderService } from './loader.service';
export { NgtLoaderComponent } from './loader.component';
export { NgtLoaderDirective } from './loader.directive';
export { NgtLoader } from './loader.model';
export declare class NgtLoaderModule {
    static forRoot(): ModuleWithProviders;
}
