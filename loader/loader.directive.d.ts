import { ComponentFactoryResolver, ComponentRef, ElementRef, OnInit, ViewContainerRef } from '@angular/core';
import { NgtLoader } from './loader.model';
/**
 * Loader instance
 */
export declare class NgtLoaderDirective implements OnInit {
    private resolver;
    private viewContainerRef;
    private elRef;
    id: string;
    container: ViewContainerRef;
    componentRef: ComponentRef<NgtLoader>;
    constructor(resolver: ComponentFactoryResolver, viewContainerRef: ViewContainerRef, elRef: ElementRef);
    ngOnInit(): void;
}
