import { Subject } from 'rxjs';
import { NgtLoader } from './loader.model';
export declare class NgtLoaderService {
    loaders: Array<NgtLoader>;
    isloaderClosed: Subject<boolean>;
    isloaderOpened: Subject<boolean>;
    add(loader: NgtLoader): void;
    remove(id: string): void;
    create(data: {
        id: string;
        content?: string;
    }): any;
}
