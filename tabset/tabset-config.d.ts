/**
 * Configuration service for the NgtTabset component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the tabsets used in the application.
 */
export declare class NgtTabsetConfig {
    justify: 'start' | 'center' | 'end' | 'fill' | 'justified';
    orientation: 'horizontal' | 'vertical';
    type: 'tabs' | 'pills';
}
