import { ModuleWithProviders } from '@angular/core';
export { NgtTabset, NgtTab, NgtTabContent, NgtTabTitle, NgtTabChangeEvent } from './tabset';
export { NgtTabsetConfig } from './tabset-config';
export declare class NgtTabsetModule {
    static forRoot(): ModuleWithProviders;
}
