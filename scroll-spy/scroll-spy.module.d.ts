import { ModuleWithProviders } from '@angular/core';
export { NgtScrollSpy } from './scroll-spy';
export declare class NgtScrollSpyModule {
    static forRoot(): ModuleWithProviders;
}
