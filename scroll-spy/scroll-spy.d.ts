import { ElementRef, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
export declare class NgtScrollSpy {
    private _document;
    private _router;
    private _elem;
    private _renderer;
    ngtScrollSpy: any;
    spyAnchor?: string;
    spyOffset?: string;
    constructor(_document: any, _router: Router, _elem: ElementRef, _renderer: Renderer2);
    private _getTarget;
    onWindowScroll(event: Event): void;
}
