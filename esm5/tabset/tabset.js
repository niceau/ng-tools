/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ContentChildren, QueryList, Directive, TemplateRef, Output, EventEmitter } from '@angular/core';
import { NgtTabsetConfig } from './tabset-config';
/** @type {?} */
var nextId = 0;
/**
 * This directive should be used to wrap tab titles that need to contain HTML markup or other directives.
 */
var NgtTabTitle = /** @class */ (function () {
    function NgtTabTitle(templateRef) {
        this.templateRef = templateRef;
    }
    NgtTabTitle.decorators = [
        { type: Directive, args: [{ selector: 'ng-template[ngtTabTitle]' },] }
    ];
    /** @nocollapse */
    NgtTabTitle.ctorParameters = function () { return [
        { type: TemplateRef }
    ]; };
    return NgtTabTitle;
}());
export { NgtTabTitle };
if (false) {
    /** @type {?} */
    NgtTabTitle.prototype.templateRef;
}
/**
 * This directive must be used to wrap content to be displayed in a tab.
 */
var NgtTabContent = /** @class */ (function () {
    function NgtTabContent(templateRef) {
        this.templateRef = templateRef;
    }
    NgtTabContent.decorators = [
        { type: Directive, args: [{ selector: 'ng-template[ngtTabContent]' },] }
    ];
    /** @nocollapse */
    NgtTabContent.ctorParameters = function () { return [
        { type: TemplateRef }
    ]; };
    return NgtTabContent;
}());
export { NgtTabContent };
if (false) {
    /** @type {?} */
    NgtTabContent.prototype.templateRef;
}
/**
 * A directive representing an individual tab.
 */
var NgtTab = /** @class */ (function () {
    function NgtTab() {
        /**
         * Unique tab identifier. Must be unique for the entire document for proper accessibility support.
         */
        this.id = "ngt-tab-" + nextId++;
        /**
         * Allows toggling disabled state of a given state. Disabled tabs can't be selected.
         */
        this.disabled = false;
    }
    /**
     * @return {?}
     */
    NgtTab.prototype.ngAfterContentChecked = /**
     * @return {?}
     */
    function () {
        // We are using @ContentChildren instead of @ContentChild as in the Angular version being used
        // only @ContentChildren allows us to specify the {descendants: false} option.
        // Without {descendants: false} we are hitting bugs described in:
        // https://github.com/ng-bootstrap/ng-bootstrap/issues/2240
        this.titleTpl = this.titleTpls.first;
        this.contentTpl = this.contentTpls.first;
    };
    NgtTab.decorators = [
        { type: Directive, args: [{ selector: 'ngt-tab' },] }
    ];
    NgtTab.propDecorators = {
        id: [{ type: Input }],
        title: [{ type: Input }],
        disabled: [{ type: Input }],
        titleTpls: [{ type: ContentChildren, args: [NgtTabTitle, { descendants: false },] }],
        contentTpls: [{ type: ContentChildren, args: [NgtTabContent, { descendants: false },] }]
    };
    return NgtTab;
}());
export { NgtTab };
if (false) {
    /**
     * Unique tab identifier. Must be unique for the entire document for proper accessibility support.
     * @type {?}
     */
    NgtTab.prototype.id;
    /**
     * Simple (string only) title. Use the "NgtTabTitle" directive for more complex use-cases.
     * @type {?}
     */
    NgtTab.prototype.title;
    /**
     * Allows toggling disabled state of a given state. Disabled tabs can't be selected.
     * @type {?}
     */
    NgtTab.prototype.disabled;
    /** @type {?} */
    NgtTab.prototype.titleTpl;
    /** @type {?} */
    NgtTab.prototype.contentTpl;
    /** @type {?} */
    NgtTab.prototype.titleTpls;
    /** @type {?} */
    NgtTab.prototype.contentTpls;
}
/**
 * The payload of the change event fired right before the tab change
 * @record
 */
export function NgtTabChangeEvent() { }
if (false) {
    /**
     * Id of the currently active tab
     * @type {?}
     */
    NgtTabChangeEvent.prototype.activeId;
    /**
     * Id of the newly selected tab
     * @type {?}
     */
    NgtTabChangeEvent.prototype.nextId;
    /**
     * Function that will prevent tab switch if called
     * @type {?}
     */
    NgtTabChangeEvent.prototype.preventDefault;
}
/**
 * A component that makes it easy to create tabbed interface.
 */
var NgtTabset = /** @class */ (function () {
    function NgtTabset(config) {
        /**
         * Whether the closed tabs should be hidden without destroying them
         */
        this.destroyOnHide = true;
        /**
         * A tab change event fired right before the tab selection happens. See NgtTabChangeEvent for payload details
         */
        this.tabChange = new EventEmitter();
        this.type = config.type;
        this.justify = config.justify;
        this.orientation = config.orientation;
    }
    Object.defineProperty(NgtTabset.prototype, "justify", {
        /**
         * The horizontal alignment of the nav with flexbox utilities. Can be one of 'start', 'center', 'end', 'fill' or
         * 'justified'
         * The default value is 'start'.
         */
        set: /**
         * The horizontal alignment of the nav with flexbox utilities. Can be one of 'start', 'center', 'end', 'fill' or
         * 'justified'
         * The default value is 'start'.
         * @param {?} className
         * @return {?}
         */
        function (className) {
            if (className === 'fill' || className === 'justified') {
                this.justifyClass = "nav-" + className;
            }
            else {
                this.justifyClass = "justify-content-" + className;
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Selects the tab with the given id and shows its associated pane.
     * Any other tab that was previously selected becomes unselected and its associated pane is hidden.
     */
    /**
     * Selects the tab with the given id and shows its associated pane.
     * Any other tab that was previously selected becomes unselected and its associated pane is hidden.
     * @param {?} tabId
     * @return {?}
     */
    NgtTabset.prototype.select = /**
     * Selects the tab with the given id and shows its associated pane.
     * Any other tab that was previously selected becomes unselected and its associated pane is hidden.
     * @param {?} tabId
     * @return {?}
     */
    function (tabId) {
        /** @type {?} */
        var selectedTab = this._getTabById(tabId);
        if (selectedTab && !selectedTab.disabled && this.activeId !== selectedTab.id) {
            /** @type {?} */
            var defaultPrevented_1 = false;
            this.tabChange.emit({
                activeId: this.activeId, nextId: selectedTab.id, preventDefault: (/**
                 * @return {?}
                 */
                function () {
                    defaultPrevented_1 = true;
                })
            });
            if (!defaultPrevented_1) {
                this.activeId = selectedTab.id;
            }
        }
    };
    /**
     * @return {?}
     */
    NgtTabset.prototype.ngAfterContentChecked = /**
     * @return {?}
     */
    function () {
        // auto-correct activeId that might have been set incorrectly as input
        /** @type {?} */
        var activeTab = this._getTabById(this.activeId);
        this.activeId = activeTab ? activeTab.id : (this.tabs.length ? this.tabs.first.id : null);
    };
    /**
     * @private
     * @param {?} id
     * @return {?}
     */
    NgtTabset.prototype._getTabById = /**
     * @private
     * @param {?} id
     * @return {?}
     */
    function (id) {
        /** @type {?} */
        var tabsWithId = this.tabs.filter((/**
         * @param {?} tab
         * @return {?}
         */
        function (tab) { return tab.id === id; }));
        return tabsWithId.length ? tabsWithId[0] : null;
    };
    NgtTabset.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-tabset',
                    exportAs: 'ngtTabset',
                    template: "\n        <ul [class]=\"'nav nav-' + type + (orientation == 'horizontal'?  ' ' + justifyClass : ' flex-column')\" role=\"tablist\">\n            <li class=\"nav-item\" *ngFor=\"let tab of tabs\">\n                <a [id]=\"tab.id\" class=\"nav-link\" [class.active]=\"tab.id === activeId\" [class.disabled]=\"tab.disabled\"\n                   href (click)=\"select(tab.id); $event.preventDefault()\" role=\"tab\" [attr.tabindex]=\"(tab.disabled ? '-1': undefined)\"\n                   [attr.aria-controls]=\"(!destroyOnHide || tab.id === activeId ? tab.id + '-panel' : null)\"\n                   [attr.aria-expanded]=\"tab.id === activeId\" [attr.aria-disabled]=\"tab.disabled\">\n                    {{tab.title}}\n                    <ng-template [ngTemplateOutlet]=\"tab.titleTpl?.templateRef\"></ng-template>\n                </a>\n            </li>\n        </ul>\n        <div class=\"tab-content\">\n            <ng-template ngFor let-tab [ngForOf]=\"tabs\">\n                <div\n                    class=\"tab-pane {{tab.id === activeId ? 'active' : null}}\"\n                    *ngIf=\"!destroyOnHide || tab.id === activeId\"\n                    role=\"tabpanel\"\n                    [attr.aria-labelledby]=\"tab.id\" id=\"{{tab.id}}-panel\"\n                    [attr.aria-expanded]=\"tab.id === activeId\">\n                    <ng-template [ngTemplateOutlet]=\"tab.contentTpl?.templateRef\"></ng-template>\n                </div>\n            </ng-template>\n        </div>\n    "
                }] }
    ];
    /** @nocollapse */
    NgtTabset.ctorParameters = function () { return [
        { type: NgtTabsetConfig }
    ]; };
    NgtTabset.propDecorators = {
        tabs: [{ type: ContentChildren, args: [NgtTab,] }],
        activeId: [{ type: Input }],
        destroyOnHide: [{ type: Input }],
        justify: [{ type: Input }],
        orientation: [{ type: Input }],
        type: [{ type: Input }],
        tabChange: [{ type: Output }]
    };
    return NgtTabset;
}());
export { NgtTabset };
if (false) {
    /** @type {?} */
    NgtTabset.prototype.justifyClass;
    /** @type {?} */
    NgtTabset.prototype.tabs;
    /**
     * An identifier of an initially selected (active) tab. Use the "select" method to switch a tab programmatically.
     * @type {?}
     */
    NgtTabset.prototype.activeId;
    /**
     * Whether the closed tabs should be hidden without destroying them
     * @type {?}
     */
    NgtTabset.prototype.destroyOnHide;
    /**
     * The orientation of the nav (horizontal or vertical).
     * The default value is 'horizontal'.
     * @type {?}
     */
    NgtTabset.prototype.orientation;
    /**
     * Type of navigation to be used for tabs. Can be one of Bootstrap defined types ('tabs' or 'pills').
     * Since 3.0.0 can also be an arbitrary string (for custom themes).
     * @type {?}
     */
    NgtTabset.prototype.type;
    /**
     * A tab change event fired right before the tab selection happens. See NgtTabChangeEvent for payload details
     * @type {?}
     */
    NgtTabset.prototype.tabChange;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFic2V0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJ0YWJzZXQvdGFic2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0gsU0FBUyxFQUNULEtBQUssRUFDTCxlQUFlLEVBQ2YsU0FBUyxFQUNULFNBQVMsRUFDVCxXQUFXLEVBR1gsTUFBTSxFQUNOLFlBQVksRUFDZixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUJBQWlCLENBQUM7O0lBRTlDLE1BQU0sR0FBRyxDQUFDOzs7O0FBS2Q7SUFFSSxxQkFBbUIsV0FBNkI7UUFBN0IsZ0JBQVcsR0FBWCxXQUFXLENBQWtCO0lBQ2hELENBQUM7O2dCQUhKLFNBQVMsU0FBQyxFQUFDLFFBQVEsRUFBRSwwQkFBMEIsRUFBQzs7OztnQkFiN0MsV0FBVzs7SUFpQmYsa0JBQUM7Q0FBQSxBQUpELElBSUM7U0FIWSxXQUFXOzs7SUFDUixrQ0FBb0M7Ozs7O0FBT3BEO0lBRUksdUJBQW1CLFdBQTZCO1FBQTdCLGdCQUFXLEdBQVgsV0FBVyxDQUFrQjtJQUNoRCxDQUFDOztnQkFISixTQUFTLFNBQUMsRUFBQyxRQUFRLEVBQUUsNEJBQTRCLEVBQUM7Ozs7Z0JBdEIvQyxXQUFXOztJQTBCZixvQkFBQztDQUFBLEFBSkQsSUFJQztTQUhZLGFBQWE7OztJQUNWLG9DQUFvQzs7Ozs7QUFPcEQ7SUFBQTs7OztRQUthLE9BQUUsR0FBRyxhQUFXLE1BQU0sRUFBSSxDQUFDOzs7O1FBUTNCLGFBQVEsR0FBRyxLQUFLLENBQUM7SUFnQjlCLENBQUM7Ozs7SUFSRyxzQ0FBcUI7OztJQUFyQjtRQUNJLDhGQUE4RjtRQUM5Riw4RUFBOEU7UUFDOUUsaUVBQWlFO1FBQ2pFLDJEQUEyRDtRQUMzRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7SUFDN0MsQ0FBQzs7Z0JBNUJKLFNBQVMsU0FBQyxFQUFDLFFBQVEsRUFBRSxTQUFTLEVBQUM7OztxQkFLM0IsS0FBSzt3QkFJTCxLQUFLOzJCQUlMLEtBQUs7NEJBS0wsZUFBZSxTQUFDLFdBQVcsRUFBRSxFQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUM7OEJBQ2pELGVBQWUsU0FBQyxhQUFhLEVBQUUsRUFBQyxXQUFXLEVBQUUsS0FBSyxFQUFDOztJQVV4RCxhQUFDO0NBQUEsQUE3QkQsSUE2QkM7U0E1QlksTUFBTTs7Ozs7O0lBSWYsb0JBQW9DOzs7OztJQUlwQyx1QkFBdUI7Ozs7O0lBSXZCLDBCQUEwQjs7SUFFMUIsMEJBQTZCOztJQUM3Qiw0QkFBaUM7O0lBRWpDLDJCQUFzRjs7SUFDdEYsNkJBQTRGOzs7Ozs7QUFlaEcsdUNBZUM7Ozs7OztJQVhHLHFDQUFpQjs7Ozs7SUFLakIsbUNBQWU7Ozs7O0lBS2YsMkNBQTJCOzs7OztBQU0vQjtJQTJFSSxtQkFBWSxNQUF1Qjs7OztRQWpDMUIsa0JBQWEsR0FBRyxJQUFJLENBQUM7Ozs7UUErQnBCLGNBQVMsR0FBRyxJQUFJLFlBQVksRUFBcUIsQ0FBQztRQUd4RCxJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDO1FBQzlCLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztJQUMxQyxDQUFDO0lBOUJELHNCQUNJLDhCQUFPO1FBTlg7Ozs7V0FJRzs7Ozs7Ozs7UUFDSCxVQUNZLFNBQTREO1lBQ3BFLElBQUksU0FBUyxLQUFLLE1BQU0sSUFBSSxTQUFTLEtBQUssV0FBVyxFQUFFO2dCQUNuRCxJQUFJLENBQUMsWUFBWSxHQUFHLFNBQU8sU0FBVyxDQUFDO2FBQzFDO2lCQUFNO2dCQUNILElBQUksQ0FBQyxZQUFZLEdBQUcscUJBQW1CLFNBQVcsQ0FBQzthQUN0RDtRQUNMLENBQUM7OztPQUFBO0lBeUJEOzs7T0FHRzs7Ozs7OztJQUNILDBCQUFNOzs7Ozs7SUFBTixVQUFPLEtBQWE7O1lBQ1osV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1FBQ3pDLElBQUksV0FBVyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLFdBQVcsQ0FBQyxFQUFFLEVBQUU7O2dCQUN0RSxrQkFBZ0IsR0FBRyxLQUFLO1lBRTVCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUNmO2dCQUNJLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxXQUFXLENBQUMsRUFBRSxFQUFFLGNBQWM7OztnQkFBRTtvQkFDN0Qsa0JBQWdCLEdBQUcsSUFBSSxDQUFDO2dCQUM1QixDQUFDLENBQUE7YUFDSixDQUFDLENBQUM7WUFFUCxJQUFJLENBQUMsa0JBQWdCLEVBQUU7Z0JBQ25CLElBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQzthQUNsQztTQUNKO0lBQ0wsQ0FBQzs7OztJQUVELHlDQUFxQjs7O0lBQXJCOzs7WUFFUSxTQUFTLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQy9DLElBQUksQ0FBQyxRQUFRLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzlGLENBQUM7Ozs7OztJQUVPLCtCQUFXOzs7OztJQUFuQixVQUFvQixFQUFVOztZQUN0QixVQUFVLEdBQWEsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxHQUFHLElBQUksT0FBQSxHQUFHLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBYixDQUFhLEVBQUM7UUFDakUsT0FBTyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUNwRCxDQUFDOztnQkFoSEosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxZQUFZO29CQUN0QixRQUFRLEVBQUUsV0FBVztvQkFDckIsUUFBUSxFQUFFLDg5Q0F3QlQ7aUJBQ0o7Ozs7Z0JBM0dRLGVBQWU7Ozt1QkErR25CLGVBQWUsU0FBQyxNQUFNOzJCQUt0QixLQUFLO2dDQUtMLEtBQUs7MEJBT0wsS0FBSzs4QkFhTCxLQUFLO3VCQU1MLEtBQUs7NEJBS0wsTUFBTTs7SUF3Q1gsZ0JBQUM7Q0FBQSxBQWpIRCxJQWlIQztTQXBGWSxTQUFTOzs7SUFDbEIsaUNBQXFCOztJQUVyQix5QkFBaUQ7Ozs7O0lBS2pELDZCQUEwQjs7Ozs7SUFLMUIsa0NBQThCOzs7Ozs7SUFvQjlCLGdDQUFnRDs7Ozs7O0lBTWhELHlCQUF5Qzs7Ozs7SUFLekMsOEJBQTREIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICAgIENvbXBvbmVudCxcclxuICAgIElucHV0LFxyXG4gICAgQ29udGVudENoaWxkcmVuLFxyXG4gICAgUXVlcnlMaXN0LFxyXG4gICAgRGlyZWN0aXZlLFxyXG4gICAgVGVtcGxhdGVSZWYsXHJcbiAgICBDb250ZW50Q2hpbGQsXHJcbiAgICBBZnRlckNvbnRlbnRDaGVja2VkLFxyXG4gICAgT3V0cHV0LFxyXG4gICAgRXZlbnRFbWl0dGVyXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5ndFRhYnNldENvbmZpZyB9IGZyb20gJy4vdGFic2V0LWNvbmZpZyc7XHJcblxyXG5sZXQgbmV4dElkID0gMDtcclxuXHJcbi8qKlxyXG4gKiBUaGlzIGRpcmVjdGl2ZSBzaG91bGQgYmUgdXNlZCB0byB3cmFwIHRhYiB0aXRsZXMgdGhhdCBuZWVkIHRvIGNvbnRhaW4gSFRNTCBtYXJrdXAgb3Igb3RoZXIgZGlyZWN0aXZlcy5cclxuICovXHJcbkBEaXJlY3RpdmUoe3NlbGVjdG9yOiAnbmctdGVtcGxhdGVbbmd0VGFiVGl0bGVdJ30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RUYWJUaXRsZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdGVtcGxhdGVSZWY6IFRlbXBsYXRlUmVmPGFueT4pIHtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIFRoaXMgZGlyZWN0aXZlIG11c3QgYmUgdXNlZCB0byB3cmFwIGNvbnRlbnQgdG8gYmUgZGlzcGxheWVkIGluIGEgdGFiLlxyXG4gKi9cclxuQERpcmVjdGl2ZSh7c2VsZWN0b3I6ICduZy10ZW1wbGF0ZVtuZ3RUYWJDb250ZW50XSd9KVxyXG5leHBvcnQgY2xhc3MgTmd0VGFiQ29udGVudCB7XHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdGVtcGxhdGVSZWY6IFRlbXBsYXRlUmVmPGFueT4pIHtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIEEgZGlyZWN0aXZlIHJlcHJlc2VudGluZyBhbiBpbmRpdmlkdWFsIHRhYi5cclxuICovXHJcbkBEaXJlY3RpdmUoe3NlbGVjdG9yOiAnbmd0LXRhYid9KVxyXG5leHBvcnQgY2xhc3MgTmd0VGFiIHtcclxuICAgIC8qKlxyXG4gICAgICogVW5pcXVlIHRhYiBpZGVudGlmaWVyLiBNdXN0IGJlIHVuaXF1ZSBmb3IgdGhlIGVudGlyZSBkb2N1bWVudCBmb3IgcHJvcGVyIGFjY2Vzc2liaWxpdHkgc3VwcG9ydC5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgaWQgPSBgbmd0LXRhYi0ke25leHRJZCsrfWA7XHJcbiAgICAvKipcclxuICAgICAqIFNpbXBsZSAoc3RyaW5nIG9ubHkpIHRpdGxlLiBVc2UgdGhlIFwiTmd0VGFiVGl0bGVcIiBkaXJlY3RpdmUgZm9yIG1vcmUgY29tcGxleCB1c2UtY2FzZXMuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHRpdGxlOiBzdHJpbmc7XHJcbiAgICAvKipcclxuICAgICAqIEFsbG93cyB0b2dnbGluZyBkaXNhYmxlZCBzdGF0ZSBvZiBhIGdpdmVuIHN0YXRlLiBEaXNhYmxlZCB0YWJzIGNhbid0IGJlIHNlbGVjdGVkLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBkaXNhYmxlZCA9IGZhbHNlO1xyXG5cclxuICAgIHRpdGxlVHBsOiBOZ3RUYWJUaXRsZSB8IG51bGw7XHJcbiAgICBjb250ZW50VHBsOiBOZ3RUYWJDb250ZW50IHwgbnVsbDtcclxuXHJcbiAgICBAQ29udGVudENoaWxkcmVuKE5ndFRhYlRpdGxlLCB7ZGVzY2VuZGFudHM6IGZhbHNlfSkgdGl0bGVUcGxzOiBRdWVyeUxpc3Q8Tmd0VGFiVGl0bGU+O1xyXG4gICAgQENvbnRlbnRDaGlsZHJlbihOZ3RUYWJDb250ZW50LCB7ZGVzY2VuZGFudHM6IGZhbHNlfSkgY29udGVudFRwbHM6IFF1ZXJ5TGlzdDxOZ3RUYWJDb250ZW50PjtcclxuXHJcbiAgICBuZ0FmdGVyQ29udGVudENoZWNrZWQoKSB7XHJcbiAgICAgICAgLy8gV2UgYXJlIHVzaW5nIEBDb250ZW50Q2hpbGRyZW4gaW5zdGVhZCBvZiBAQ29udGVudENoaWxkIGFzIGluIHRoZSBBbmd1bGFyIHZlcnNpb24gYmVpbmcgdXNlZFxyXG4gICAgICAgIC8vIG9ubHkgQENvbnRlbnRDaGlsZHJlbiBhbGxvd3MgdXMgdG8gc3BlY2lmeSB0aGUge2Rlc2NlbmRhbnRzOiBmYWxzZX0gb3B0aW9uLlxyXG4gICAgICAgIC8vIFdpdGhvdXQge2Rlc2NlbmRhbnRzOiBmYWxzZX0gd2UgYXJlIGhpdHRpbmcgYnVncyBkZXNjcmliZWQgaW46XHJcbiAgICAgICAgLy8gaHR0cHM6Ly9naXRodWIuY29tL25nLWJvb3RzdHJhcC9uZy1ib290c3RyYXAvaXNzdWVzLzIyNDBcclxuICAgICAgICB0aGlzLnRpdGxlVHBsID0gdGhpcy50aXRsZVRwbHMuZmlyc3Q7XHJcbiAgICAgICAgdGhpcy5jb250ZW50VHBsID0gdGhpcy5jb250ZW50VHBscy5maXJzdDtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIFRoZSBwYXlsb2FkIG9mIHRoZSBjaGFuZ2UgZXZlbnQgZmlyZWQgcmlnaHQgYmVmb3JlIHRoZSB0YWIgY2hhbmdlXHJcbiAqL1xyXG5leHBvcnQgaW50ZXJmYWNlIE5ndFRhYkNoYW5nZUV2ZW50IHtcclxuICAgIC8qKlxyXG4gICAgICogSWQgb2YgdGhlIGN1cnJlbnRseSBhY3RpdmUgdGFiXHJcbiAgICAgKi9cclxuICAgIGFjdGl2ZUlkOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJZCBvZiB0aGUgbmV3bHkgc2VsZWN0ZWQgdGFiXHJcbiAgICAgKi9cclxuICAgIG5leHRJZDogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogRnVuY3Rpb24gdGhhdCB3aWxsIHByZXZlbnQgdGFiIHN3aXRjaCBpZiBjYWxsZWRcclxuICAgICAqL1xyXG4gICAgcHJldmVudERlZmF1bHQ6ICgpID0+IHZvaWQ7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBBIGNvbXBvbmVudCB0aGF0IG1ha2VzIGl0IGVhc3kgdG8gY3JlYXRlIHRhYmJlZCBpbnRlcmZhY2UuXHJcbiAqL1xyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbmd0LXRhYnNldCcsXHJcbiAgICBleHBvcnRBczogJ25ndFRhYnNldCcsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDx1bCBbY2xhc3NdPVwiJ25hdiBuYXYtJyArIHR5cGUgKyAob3JpZW50YXRpb24gPT0gJ2hvcml6b250YWwnPyAgJyAnICsganVzdGlmeUNsYXNzIDogJyBmbGV4LWNvbHVtbicpXCIgcm9sZT1cInRhYmxpc3RcIj5cclxuICAgICAgICAgICAgPGxpIGNsYXNzPVwibmF2LWl0ZW1cIiAqbmdGb3I9XCJsZXQgdGFiIG9mIHRhYnNcIj5cclxuICAgICAgICAgICAgICAgIDxhIFtpZF09XCJ0YWIuaWRcIiBjbGFzcz1cIm5hdi1saW5rXCIgW2NsYXNzLmFjdGl2ZV09XCJ0YWIuaWQgPT09IGFjdGl2ZUlkXCIgW2NsYXNzLmRpc2FibGVkXT1cInRhYi5kaXNhYmxlZFwiXHJcbiAgICAgICAgICAgICAgICAgICBocmVmIChjbGljayk9XCJzZWxlY3QodGFiLmlkKTsgJGV2ZW50LnByZXZlbnREZWZhdWx0KClcIiByb2xlPVwidGFiXCIgW2F0dHIudGFiaW5kZXhdPVwiKHRhYi5kaXNhYmxlZCA/ICctMSc6IHVuZGVmaW5lZClcIlxyXG4gICAgICAgICAgICAgICAgICAgW2F0dHIuYXJpYS1jb250cm9sc109XCIoIWRlc3Ryb3lPbkhpZGUgfHwgdGFiLmlkID09PSBhY3RpdmVJZCA/IHRhYi5pZCArICctcGFuZWwnIDogbnVsbClcIlxyXG4gICAgICAgICAgICAgICAgICAgW2F0dHIuYXJpYS1leHBhbmRlZF09XCJ0YWIuaWQgPT09IGFjdGl2ZUlkXCIgW2F0dHIuYXJpYS1kaXNhYmxlZF09XCJ0YWIuZGlzYWJsZWRcIj5cclxuICAgICAgICAgICAgICAgICAgICB7e3RhYi50aXRsZX19XHJcbiAgICAgICAgICAgICAgICAgICAgPG5nLXRlbXBsYXRlIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInRhYi50aXRsZVRwbD8udGVtcGxhdGVSZWZcIj48L25nLXRlbXBsYXRlPlxyXG4gICAgICAgICAgICAgICAgPC9hPlxyXG4gICAgICAgICAgICA8L2xpPlxyXG4gICAgICAgIDwvdWw+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInRhYi1jb250ZW50XCI+XHJcbiAgICAgICAgICAgIDxuZy10ZW1wbGF0ZSBuZ0ZvciBsZXQtdGFiIFtuZ0Zvck9mXT1cInRhYnNcIj5cclxuICAgICAgICAgICAgICAgIDxkaXZcclxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cInRhYi1wYW5lIHt7dGFiLmlkID09PSBhY3RpdmVJZCA/ICdhY3RpdmUnIDogbnVsbH19XCJcclxuICAgICAgICAgICAgICAgICAgICAqbmdJZj1cIiFkZXN0cm95T25IaWRlIHx8IHRhYi5pZCA9PT0gYWN0aXZlSWRcIlxyXG4gICAgICAgICAgICAgICAgICAgIHJvbGU9XCJ0YWJwYW5lbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgW2F0dHIuYXJpYS1sYWJlbGxlZGJ5XT1cInRhYi5pZFwiIGlkPVwie3t0YWIuaWR9fS1wYW5lbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgW2F0dHIuYXJpYS1leHBhbmRlZF09XCJ0YWIuaWQgPT09IGFjdGl2ZUlkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPG5nLXRlbXBsYXRlIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInRhYi5jb250ZW50VHBsPy50ZW1wbGF0ZVJlZlwiPjwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIGBcclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndFRhYnNldCBpbXBsZW1lbnRzIEFmdGVyQ29udGVudENoZWNrZWQge1xyXG4gICAganVzdGlmeUNsYXNzOiBzdHJpbmc7XHJcblxyXG4gICAgQENvbnRlbnRDaGlsZHJlbihOZ3RUYWIpIHRhYnM6IFF1ZXJ5TGlzdDxOZ3RUYWI+O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQW4gaWRlbnRpZmllciBvZiBhbiBpbml0aWFsbHkgc2VsZWN0ZWQgKGFjdGl2ZSkgdGFiLiBVc2UgdGhlIFwic2VsZWN0XCIgbWV0aG9kIHRvIHN3aXRjaCBhIHRhYiBwcm9ncmFtbWF0aWNhbGx5LlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBhY3RpdmVJZDogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogV2hldGhlciB0aGUgY2xvc2VkIHRhYnMgc2hvdWxkIGJlIGhpZGRlbiB3aXRob3V0IGRlc3Ryb3lpbmcgdGhlbVxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBkZXN0cm95T25IaWRlID0gdHJ1ZTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFRoZSBob3Jpem9udGFsIGFsaWdubWVudCBvZiB0aGUgbmF2IHdpdGggZmxleGJveCB1dGlsaXRpZXMuIENhbiBiZSBvbmUgb2YgJ3N0YXJ0JywgJ2NlbnRlcicsICdlbmQnLCAnZmlsbCcgb3JcclxuICAgICAqICdqdXN0aWZpZWQnXHJcbiAgICAgKiBUaGUgZGVmYXVsdCB2YWx1ZSBpcyAnc3RhcnQnLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgc2V0IGp1c3RpZnkoY2xhc3NOYW1lOiAnc3RhcnQnIHwgJ2NlbnRlcicgfCAnZW5kJyB8ICdmaWxsJyB8ICdqdXN0aWZpZWQnKSB7XHJcbiAgICAgICAgaWYgKGNsYXNzTmFtZSA9PT0gJ2ZpbGwnIHx8IGNsYXNzTmFtZSA9PT0gJ2p1c3RpZmllZCcpIHtcclxuICAgICAgICAgICAgdGhpcy5qdXN0aWZ5Q2xhc3MgPSBgbmF2LSR7Y2xhc3NOYW1lfWA7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5qdXN0aWZ5Q2xhc3MgPSBganVzdGlmeS1jb250ZW50LSR7Y2xhc3NOYW1lfWA7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhlIG9yaWVudGF0aW9uIG9mIHRoZSBuYXYgKGhvcml6b250YWwgb3IgdmVydGljYWwpLlxyXG4gICAgICogVGhlIGRlZmF1bHQgdmFsdWUgaXMgJ2hvcml6b250YWwnLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBvcmllbnRhdGlvbjogJ2hvcml6b250YWwnIHwgJ3ZlcnRpY2FsJztcclxuXHJcbiAgICAvKipcclxuICAgICAqIFR5cGUgb2YgbmF2aWdhdGlvbiB0byBiZSB1c2VkIGZvciB0YWJzLiBDYW4gYmUgb25lIG9mIEJvb3RzdHJhcCBkZWZpbmVkIHR5cGVzICgndGFicycgb3IgJ3BpbGxzJykuXHJcbiAgICAgKiBTaW5jZSAzLjAuMCBjYW4gYWxzbyBiZSBhbiBhcmJpdHJhcnkgc3RyaW5nIChmb3IgY3VzdG9tIHRoZW1lcykuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHR5cGU6ICd0YWJzJyB8ICdwaWxscycgfCBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBIHRhYiBjaGFuZ2UgZXZlbnQgZmlyZWQgcmlnaHQgYmVmb3JlIHRoZSB0YWIgc2VsZWN0aW9uIGhhcHBlbnMuIFNlZSBOZ3RUYWJDaGFuZ2VFdmVudCBmb3IgcGF5bG9hZCBkZXRhaWxzXHJcbiAgICAgKi9cclxuICAgIEBPdXRwdXQoKSB0YWJDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPE5ndFRhYkNoYW5nZUV2ZW50PigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbmZpZzogTmd0VGFic2V0Q29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy50eXBlID0gY29uZmlnLnR5cGU7XHJcbiAgICAgICAgdGhpcy5qdXN0aWZ5ID0gY29uZmlnLmp1c3RpZnk7XHJcbiAgICAgICAgdGhpcy5vcmllbnRhdGlvbiA9IGNvbmZpZy5vcmllbnRhdGlvbjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbGVjdHMgdGhlIHRhYiB3aXRoIHRoZSBnaXZlbiBpZCBhbmQgc2hvd3MgaXRzIGFzc29jaWF0ZWQgcGFuZS5cclxuICAgICAqIEFueSBvdGhlciB0YWIgdGhhdCB3YXMgcHJldmlvdXNseSBzZWxlY3RlZCBiZWNvbWVzIHVuc2VsZWN0ZWQgYW5kIGl0cyBhc3NvY2lhdGVkIHBhbmUgaXMgaGlkZGVuLlxyXG4gICAgICovXHJcbiAgICBzZWxlY3QodGFiSWQ6IHN0cmluZykge1xyXG4gICAgICAgIGxldCBzZWxlY3RlZFRhYiA9IHRoaXMuX2dldFRhYkJ5SWQodGFiSWQpO1xyXG4gICAgICAgIGlmIChzZWxlY3RlZFRhYiAmJiAhc2VsZWN0ZWRUYWIuZGlzYWJsZWQgJiYgdGhpcy5hY3RpdmVJZCAhPT0gc2VsZWN0ZWRUYWIuaWQpIHtcclxuICAgICAgICAgICAgbGV0IGRlZmF1bHRQcmV2ZW50ZWQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMudGFiQ2hhbmdlLmVtaXQoXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWN0aXZlSWQ6IHRoaXMuYWN0aXZlSWQsIG5leHRJZDogc2VsZWN0ZWRUYWIuaWQsIHByZXZlbnREZWZhdWx0OiAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlZmF1bHRQcmV2ZW50ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaWYgKCFkZWZhdWx0UHJldmVudGVkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZUlkID0gc2VsZWN0ZWRUYWIuaWQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdBZnRlckNvbnRlbnRDaGVja2VkKCkge1xyXG4gICAgICAgIC8vIGF1dG8tY29ycmVjdCBhY3RpdmVJZCB0aGF0IG1pZ2h0IGhhdmUgYmVlbiBzZXQgaW5jb3JyZWN0bHkgYXMgaW5wdXRcclxuICAgICAgICBsZXQgYWN0aXZlVGFiID0gdGhpcy5fZ2V0VGFiQnlJZCh0aGlzLmFjdGl2ZUlkKTtcclxuICAgICAgICB0aGlzLmFjdGl2ZUlkID0gYWN0aXZlVGFiID8gYWN0aXZlVGFiLmlkIDogKHRoaXMudGFicy5sZW5ndGggPyB0aGlzLnRhYnMuZmlyc3QuaWQgOiBudWxsKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9nZXRUYWJCeUlkKGlkOiBzdHJpbmcpOiBOZ3RUYWIge1xyXG4gICAgICAgIGxldCB0YWJzV2l0aElkOiBOZ3RUYWJbXSA9IHRoaXMudGFicy5maWx0ZXIodGFiID0+IHRhYi5pZCA9PT0gaWQpO1xyXG4gICAgICAgIHJldHVybiB0YWJzV2l0aElkLmxlbmd0aCA/IHRhYnNXaXRoSWRbMF0gOiBudWxsO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==