/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtTabset, NgtTab, NgtTabContent, NgtTabTitle } from './tabset';
export { NgtTabset, NgtTab, NgtTabContent, NgtTabTitle } from './tabset';
export { NgtTabsetConfig } from './tabset-config';
/** @type {?} */
var NGT_TABSET_DIRECTIVES = [NgtTabset, NgtTab, NgtTabContent, NgtTabTitle];
var NgtTabsetModule = /** @class */ (function () {
    function NgtTabsetModule() {
    }
    /**
     * @return {?}
     */
    NgtTabsetModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtTabsetModule };
    };
    NgtTabsetModule.decorators = [
        { type: NgModule, args: [{
                    declarations: NGT_TABSET_DIRECTIVES,
                    exports: NGT_TABSET_DIRECTIVES,
                    imports: [CommonModule]
                },] }
    ];
    return NgtTabsetModule;
}());
export { NgtTabsetModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFic2V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsidGFic2V0L3RhYnNldC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLE1BQU0sVUFBVSxDQUFDO0FBRXpFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQXFCLE1BQU0sVUFBVSxDQUFDO0FBQzVGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQzs7SUFFNUMscUJBQXFCLEdBQUcsQ0FBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxXQUFXLENBQUM7QUFFN0U7SUFBQTtJQVNBLENBQUM7Ozs7SUFIVSx1QkFBTzs7O0lBQWQ7UUFDSSxPQUFPLEVBQUMsUUFBUSxFQUFFLGVBQWUsRUFBQyxDQUFDO0lBQ3ZDLENBQUM7O2dCQVJKLFFBQVEsU0FBQztvQkFDTixZQUFZLEVBQUUscUJBQXFCO29CQUNuQyxPQUFPLEVBQUUscUJBQXFCO29CQUM5QixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7aUJBQzFCOztJQUtELHNCQUFDO0NBQUEsQUFURCxJQVNDO1NBSlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5pbXBvcnQgeyBOZ3RUYWJzZXQsIE5ndFRhYiwgTmd0VGFiQ29udGVudCwgTmd0VGFiVGl0bGUgfSBmcm9tICcuL3RhYnNldCc7XHJcblxyXG5leHBvcnQgeyBOZ3RUYWJzZXQsIE5ndFRhYiwgTmd0VGFiQ29udGVudCwgTmd0VGFiVGl0bGUsIE5ndFRhYkNoYW5nZUV2ZW50IH0gZnJvbSAnLi90YWJzZXQnO1xyXG5leHBvcnQgeyBOZ3RUYWJzZXRDb25maWcgfSBmcm9tICcuL3RhYnNldC1jb25maWcnO1xyXG5cclxuY29uc3QgTkdUX1RBQlNFVF9ESVJFQ1RJVkVTID0gW05ndFRhYnNldCwgTmd0VGFiLCBOZ3RUYWJDb250ZW50LCBOZ3RUYWJUaXRsZV07XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBOR1RfVEFCU0VUX0RJUkVDVElWRVMsXHJcbiAgICBleHBvcnRzOiBOR1RfVEFCU0VUX0RJUkVDVElWRVMsXHJcbiAgICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0VGFic2V0TW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgICAgIHJldHVybiB7bmdNb2R1bGU6IE5ndFRhYnNldE1vZHVsZX07XHJcbiAgICB9XHJcbn1cclxuIl19