/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { ChangeDetectorRef, ContentChild, ContentChildren, Directive, ElementRef, EventEmitter, forwardRef, HostBinding, HostListener, Inject, Input, NgZone, Output, QueryList, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Subject } from 'rxjs';
import { positionElements } from '../util/positioning';
import { ngtAutoClose } from '../util/autoclose';
import { Key } from '../util/key';
import { NgtDropdownConfig } from './dropdown-config';
/**
 * A directive you should put put on a dropdown item to enable keyboard navigation.
 * Keyboard navigation using arrow keys will move focus between items marked with this directive.
 */
var NgtDropdownItemDirective = /** @class */ (function () {
    function NgtDropdownItemDirective(elementRef) {
        this.elementRef = elementRef;
        this._disabled = false;
        this.class = true;
        this.classDisabled = this.disabled;
    }
    Object.defineProperty(NgtDropdownItemDirective.prototype, "disabled", {
        get: /**
         * @return {?}
         */
        function () {
            return this._disabled;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._disabled = (/** @type {?} */ (value)) === '' || value === true; // accept an empty attribute as true
            this.classDisabled = this._disabled;
        },
        enumerable: true,
        configurable: true
    });
    NgtDropdownItemDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtDropdownItem]'
                },] }
    ];
    /** @nocollapse */
    NgtDropdownItemDirective.ctorParameters = function () { return [
        { type: ElementRef }
    ]; };
    NgtDropdownItemDirective.propDecorators = {
        class: [{ type: HostBinding, args: ['class.dropdown-item',] }],
        classDisabled: [{ type: HostBinding, args: ['class.disabled',] }],
        disabled: [{ type: Input }]
    };
    return NgtDropdownItemDirective;
}());
export { NgtDropdownItemDirective };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtDropdownItemDirective.prototype._disabled;
    /** @type {?} */
    NgtDropdownItemDirective.prototype.class;
    /** @type {?} */
    NgtDropdownItemDirective.prototype.classDisabled;
    /** @type {?} */
    NgtDropdownItemDirective.prototype.elementRef;
}
/**
 *
 */
var NgtDropdownMenuDirective = /** @class */ (function () {
    function NgtDropdownMenuDirective(dropdown, _elementRef, _renderer) {
        var _this = this;
        this.dropdown = dropdown;
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this.placement = 'bottom';
        this.isOpen = false;
        this.dropdownMenu = true;
        this.show = this.dropdown.isOpen();
        this.xPlacement = this.placement;
        this.dropdown.openChange.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            _this.show = status;
        }));
    }
    /**
     * @return {?}
     */
    NgtDropdownMenuDirective.prototype.getNativeElement = /**
     * @return {?}
     */
    function () {
        return this._elementRef.nativeElement;
    };
    /**
     * @param {?} triggerEl
     * @param {?} placement
     * @return {?}
     */
    NgtDropdownMenuDirective.prototype.position = /**
     * @param {?} triggerEl
     * @param {?} placement
     * @return {?}
     */
    function (triggerEl, placement) {
        this.applyPlacement(positionElements(triggerEl, this._elementRef.nativeElement, placement));
    };
    /**
     * @param {?} _placement
     * @return {?}
     */
    NgtDropdownMenuDirective.prototype.applyPlacement = /**
     * @param {?} _placement
     * @return {?}
     */
    function (_placement) {
        // remove the current placement classes
        this._renderer.removeClass(this._elementRef.nativeElement.parentNode, 'dropup');
        this._renderer.removeClass(this._elementRef.nativeElement.parentNode, 'dropdown');
        this.placement = _placement;
        /**
         * apply the new placement
         * in case of top use up-arrow or down-arrow otherwise
         */
        if (_placement.search('^top') !== -1) {
            this._renderer.addClass(this._elementRef.nativeElement.parentNode, 'dropup');
        }
        else {
            this._renderer.addClass(this._elementRef.nativeElement.parentNode, 'dropdown');
        }
    };
    NgtDropdownMenuDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtDropdownMenu]'
                },] }
    ];
    /** @nocollapse */
    NgtDropdownMenuDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [forwardRef((/**
                         * @return {?}
                         */
                        function () { return NgtDropdownDirective; })),] }] },
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    NgtDropdownMenuDirective.propDecorators = {
        menuItems: [{ type: ContentChildren, args: [NgtDropdownItemDirective,] }],
        dropdownMenu: [{ type: HostBinding, args: ['class.dropdown-menu',] }],
        show: [{ type: HostBinding, args: ['class.show',] }],
        xPlacement: [{ type: HostBinding, args: ['attr.x-placement',] }]
    };
    return NgtDropdownMenuDirective;
}());
export { NgtDropdownMenuDirective };
if (false) {
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.placement;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.isOpen;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.menuItems;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.dropdownMenu;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.show;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.xPlacement;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.dropdown;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownMenuDirective.prototype._elementRef;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownMenuDirective.prototype._renderer;
}
/**
 * Marks an element to which dropdown menu will be anchored. This is a simple version
 * of the NgtDropdownToggleDirective. It plays the same role as NgtDropdownToggleDirective but
 * doesn't listen to click events to toggle dropdown menu thus enabling support for
 * events other than click.
 */
var NgtDropdownAnchorDirective = /** @class */ (function () {
    function NgtDropdownAnchorDirective(dropdown, _elementRef) {
        this.dropdown = dropdown;
        this._elementRef = _elementRef;
        this.class = true;
        this.ariaHaspopup = true;
        this.ariaExpanded = this.dropdown.isOpen();
        this.anchorEl = _elementRef.nativeElement;
    }
    /**
     * @return {?}
     */
    NgtDropdownAnchorDirective.prototype.getNativeElement = /**
     * @return {?}
     */
    function () {
        return this._elementRef.nativeElement;
    };
    NgtDropdownAnchorDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtDropdownAnchor]'
                },] }
    ];
    /** @nocollapse */
    NgtDropdownAnchorDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [forwardRef((/**
                         * @return {?}
                         */
                        function () { return NgtDropdownDirective; })),] }] },
        { type: ElementRef }
    ]; };
    NgtDropdownAnchorDirective.propDecorators = {
        class: [{ type: HostBinding, args: ['class.dropdown-toggle',] }],
        ariaHaspopup: [{ type: HostBinding, args: ['attr.aria-haspopup.true',] }],
        ariaExpanded: [{ type: HostBinding, args: ['attr.aria-expanded',] }]
    };
    return NgtDropdownAnchorDirective;
}());
export { NgtDropdownAnchorDirective };
if (false) {
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.anchorEl;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.class;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.ariaHaspopup;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.ariaExpanded;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.dropdown;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownAnchorDirective.prototype._elementRef;
}
/**
 * Allows the dropdown to be toggled via click. This directive is optional: you can use NgtDropdownAnchorDirective as an
 * alternative.
 */
var NgtDropdownToggleDirective = /** @class */ (function (_super) {
    tslib_1.__extends(NgtDropdownToggleDirective, _super);
    function NgtDropdownToggleDirective(dropdown, elementRef) {
        var _this = _super.call(this, dropdown, elementRef) || this;
        _this.class = true;
        _this.ariaHaspopup = true;
        _this.ariaExpanded = _this.dropdown.isOpen();
        return _this;
    }
    /**
     * @return {?}
     */
    NgtDropdownToggleDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this.toggleOpen();
    };
    /**
     * @return {?}
     */
    NgtDropdownToggleDirective.prototype.toggleOpen = /**
     * @return {?}
     */
    function () {
        this.dropdown.toggle();
    };
    NgtDropdownToggleDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtDropdownToggle]',
                    providers: [{ provide: NgtDropdownAnchorDirective, useExisting: forwardRef((/**
                             * @return {?}
                             */
                            function () { return NgtDropdownToggleDirective; })) }]
                },] }
    ];
    /** @nocollapse */
    NgtDropdownToggleDirective.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [forwardRef((/**
                         * @return {?}
                         */
                        function () { return NgtDropdownDirective; })),] }] },
        { type: ElementRef }
    ]; };
    NgtDropdownToggleDirective.propDecorators = {
        class: [{ type: HostBinding, args: ['class.dropdown-toggle',] }],
        ariaHaspopup: [{ type: HostBinding, args: ['attr.aria-haspopup.true',] }],
        ariaExpanded: [{ type: HostBinding, args: ['attr.aria-expanded',] }],
        onClick: [{ type: HostListener, args: ['click',] }]
    };
    return NgtDropdownToggleDirective;
}(NgtDropdownAnchorDirective));
export { NgtDropdownToggleDirective };
if (false) {
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.class;
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.ariaHaspopup;
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.ariaExpanded;
}
/**
 * Transforms a node into a dropdown.
 */
var NgtDropdownDirective = /** @class */ (function () {
    function NgtDropdownDirective(_changeDetector, config, _document, _ngZone, _elementRef, _renderer) {
        var _this = this;
        this._changeDetector = _changeDetector;
        this._document = _document;
        this._ngZone = _ngZone;
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this._closed$ = new Subject();
        /**
         *  Defines whether or not the dropdown-menu is open initially.
         */
        this._open = false;
        /**
         *  An event fired when the dropdown is opened or closed.
         *  Event's payload equals whether dropdown is open.
         */
        this.openChange = new EventEmitter();
        this.show = this.isOpen();
        this.placement = config.placement;
        this.container = config.container;
        this.autoClose = config.autoClose;
        this._zoneSubscription = _ngZone.onStable.subscribe((/**
         * @return {?}
         */
        function () {
            _this._positionMenu();
        }));
        this.openChange.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            _this.show = status;
        }));
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtDropdownDirective.prototype.onArrowUp = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.onKeyDown($event);
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtDropdownDirective.prototype.onArrowArrowDown = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.onKeyDown($event);
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtDropdownDirective.prototype.onArrowHome = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.onKeyDown($event);
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtDropdownDirective.prototype.onArrowEnd = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.onKeyDown($event);
    };
    /**
     * @return {?}
     */
    NgtDropdownDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._applyPlacementClasses();
        if (this._open) {
            this._setCloseHandlers();
        }
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    NgtDropdownDirective.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.container && this._open) {
            this._applyContainer(this.container);
        }
        if (changes.placement && !changes.placement.isFirstChange) {
            this._applyPlacementClasses();
        }
    };
    /**
     * Checks if the dropdown menu is open or not.
     */
    /**
     * Checks if the dropdown menu is open or not.
     * @return {?}
     */
    NgtDropdownDirective.prototype.isOpen = /**
     * Checks if the dropdown menu is open or not.
     * @return {?}
     */
    function () {
        return this._open;
    };
    /**
     * Opens the dropdown menu of a given navbar or tabbed navigation.
     */
    /**
     * Opens the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    NgtDropdownDirective.prototype.open = /**
     * Opens the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    function () {
        if (!this._open) {
            this._open = true;
            this._applyContainer(this.container);
            this._positionMenu();
            this.openChange.emit(true);
            this._setCloseHandlers();
        }
    };
    /**
     * @private
     * @return {?}
     */
    NgtDropdownDirective.prototype._setCloseHandlers = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        ngtAutoClose(this._ngZone, this._document, this.autoClose, (/**
         * @return {?}
         */
        function () { return _this.close(); }), this._closed$, this._menu ? [this._menu.getNativeElement()] : [], this._anchor ? [this._anchor.getNativeElement()] : []);
    };
    /**
     * Closes the dropdown menu of a given navbar or tabbed navigation.
     */
    /**
     * Closes the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    NgtDropdownDirective.prototype.close = /**
     * Closes the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    function () {
        if (this._open) {
            this._open = false;
            this._resetContainer();
            this._closed$.next();
            this.openChange.emit(false);
            this._changeDetector.markForCheck();
        }
    };
    /**
     * Toggles the dropdown menu of a given navbar or tabbed navigation.
     */
    /**
     * Toggles the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    NgtDropdownDirective.prototype.toggle = /**
     * Toggles the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    function () {
        if (this.isOpen()) {
            this.close();
        }
        else {
            this.open();
        }
    };
    /**
     * @return {?}
     */
    NgtDropdownDirective.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this._resetContainer();
        this._closed$.next();
        this._zoneSubscription.unsubscribe();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    NgtDropdownDirective.prototype.onKeyDown = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        var _this = this;
        /** @type {?} */
        var itemElements = this._getMenuElements();
        if (!itemElements.length) {
            return false;
        }
        /** @type {?} */
        var position = -1;
        /** @type {?} */
        var isEventFromItems = false;
        /** @type {?} */
        var isEventFromToggle = this._isEventFromToggle(event);
        if (!isEventFromToggle) {
            itemElements.forEach((/**
             * @param {?} itemElement
             * @param {?} index
             * @return {?}
             */
            function (itemElement, index) {
                if (itemElement.contains((/** @type {?} */ (event.target)))) {
                    isEventFromItems = true;
                }
                if (itemElement === _this._document.activeElement) {
                    position = index;
                }
            }));
        }
        if (isEventFromToggle || isEventFromItems) {
            if (!this.isOpen()) {
                this.open();
            }
            // tslint:disable-next-line:deprecation
            switch (event.which) {
                case Key.ArrowDown:
                    position = Math.min(position + 1, itemElements.length - 1);
                    break;
                case Key.ArrowUp:
                    if (this._isDropup() && position === -1) {
                        position = itemElements.length - 1;
                        break;
                    }
                    position = Math.max(position - 1, 0);
                    break;
                case Key.Home:
                    position = 0;
                    break;
                case Key.End:
                    position = itemElements.length - 1;
                    break;
            }
            itemElements[position].focus();
            event.preventDefault();
        }
    };
    /**
     * @private
     * @return {?}
     */
    NgtDropdownDirective.prototype._isDropup = /**
     * @private
     * @return {?}
     */
    function () {
        return this._elementRef.nativeElement.classList.contains('dropup');
    };
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    NgtDropdownDirective.prototype._isEventFromToggle = /**
     * @private
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return this._anchor.getNativeElement().contains((/** @type {?} */ (event.target)));
    };
    /**
     * @private
     * @return {?}
     */
    NgtDropdownDirective.prototype._getMenuElements = /**
     * @private
     * @return {?}
     */
    function () {
        if (this._menu == null) {
            return [];
        }
        return this._menu.menuItems.filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return !item.disabled; })).map((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.elementRef.nativeElement; }));
    };
    /**
     * @private
     * @return {?}
     */
    NgtDropdownDirective.prototype._positionMenu = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.isOpen() && this._menu) {
            this._applyPlacementClasses(positionElements(this._anchor.anchorEl, this._bodyContainer || this._menuElement.nativeElement, this.placement, this.container === 'body'));
        }
    };
    /**
     * @private
     * @return {?}
     */
    NgtDropdownDirective.prototype._resetContainer = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var renderer = this._renderer;
        if (this._menuElement) {
            /** @type {?} */
            var dropdownElement = this._elementRef.nativeElement;
            /** @type {?} */
            var dropdownMenuElement = this._menuElement.nativeElement;
            renderer.appendChild(dropdownElement, dropdownMenuElement);
            renderer.removeStyle(dropdownMenuElement, 'position');
            renderer.removeStyle(dropdownMenuElement, 'transform');
        }
        if (this._bodyContainer) {
            renderer.removeChild(this._document.body, this._bodyContainer);
            this._bodyContainer = null;
        }
    };
    /**
     * @private
     * @param {?=} container
     * @return {?}
     */
    NgtDropdownDirective.prototype._applyContainer = /**
     * @private
     * @param {?=} container
     * @return {?}
     */
    function (container) {
        if (container === void 0) { container = null; }
        this._resetContainer();
        if (container === 'body') {
            /** @type {?} */
            var renderer = this._renderer;
            /** @type {?} */
            var dropdownMenuElement = this._menuElement.nativeElement;
            /** @type {?} */
            var bodyContainer = this._bodyContainer = this._bodyContainer || renderer.createElement('div');
            // Override some styles to have the positionning working
            renderer.setStyle(bodyContainer, 'position', 'absolute');
            renderer.setStyle(dropdownMenuElement, 'position', 'static');
            renderer.appendChild(bodyContainer, dropdownMenuElement);
            renderer.appendChild(this._document.body, bodyContainer);
        }
    };
    /**
     * @private
     * @param {?=} placement
     * @return {?}
     */
    NgtDropdownDirective.prototype._applyPlacementClasses = /**
     * @private
     * @param {?=} placement
     * @return {?}
     */
    function (placement) {
        if (this._menu) {
            if (!placement) {
                placement = Array.isArray(this.placement) ? this.placement[0] : (/** @type {?} */ (this.placement));
            }
            /** @type {?} */
            var renderer = this._renderer;
            /** @type {?} */
            var dropdownElement = this._elementRef.nativeElement;
            // remove the current placement classes
            renderer.removeClass(dropdownElement, 'dropup');
            renderer.removeClass(dropdownElement, 'dropdown');
            this.placement = placement;
            this._menu.placement = placement;
            /*
                        * apply the new placement
                        * in case of top use up-arrow or down-arrow otherwise
                        */
            /** @type {?} */
            var dropdownClass = placement.search('^top') !== -1 ? 'dropup' : 'dropdown';
            renderer.addClass(dropdownElement, dropdownClass);
            /** @type {?} */
            var bodyContainer = this._bodyContainer;
            if (bodyContainer) {
                renderer.removeClass(bodyContainer, 'dropup');
                renderer.removeClass(bodyContainer, 'dropdown');
                renderer.addClass(bodyContainer, dropdownClass);
            }
        }
    };
    NgtDropdownDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtDropdown]',
                    exportAs: 'ngtDropdown'
                },] }
    ];
    /** @nocollapse */
    NgtDropdownDirective.ctorParameters = function () { return [
        { type: ChangeDetectorRef },
        { type: NgtDropdownConfig },
        { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
        { type: NgZone },
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    NgtDropdownDirective.propDecorators = {
        _menu: [{ type: ContentChild, args: [NgtDropdownMenuDirective,] }],
        _menuElement: [{ type: ContentChild, args: [NgtDropdownMenuDirective, { read: ElementRef },] }],
        _anchor: [{ type: ContentChild, args: [NgtDropdownAnchorDirective,] }],
        autoClose: [{ type: Input }],
        _open: [{ type: Input, args: ['open',] }],
        placement: [{ type: Input }],
        container: [{ type: Input }],
        openChange: [{ type: Output }],
        show: [{ type: HostBinding, args: ['class.show',] }],
        onArrowUp: [{ type: HostListener, args: ['document:keydown.ArrowUp', ['$event'],] }],
        onArrowArrowDown: [{ type: HostListener, args: ['document:keydown.ArrowDown', ['$event'],] }],
        onArrowHome: [{ type: HostListener, args: ['document:keydown.Home', ['$event'],] }],
        onArrowEnd: [{ type: HostListener, args: ['document:keydown.End', ['$event'],] }]
    };
    return NgtDropdownDirective;
}());
export { NgtDropdownDirective };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._closed$;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._zoneSubscription;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._bodyContainer;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._menu;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._menuElement;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._anchor;
    /**
     * Indicates that dropdown should be closed when selecting one of dropdown items (click) or pressing ESC.
     * When it is true (default) dropdowns are automatically closed on both outside and inside (menu) clicks.
     * When it is false dropdowns are never automatically closed.
     * When it is 'outside' dropdowns are automatically closed on outside clicks but not on menu clicks.
     * When it is 'inside' dropdowns are automatically on menu clicks but not on outside clicks.
     * @type {?}
     */
    NgtDropdownDirective.prototype.autoClose;
    /**
     *  Defines whether or not the dropdown-menu is open initially.
     * @type {?}
     */
    NgtDropdownDirective.prototype._open;
    /**
     * Placement of a popover accepts:
     *    "top", "top-left", "top-right", "bottom", "bottom-left", "bottom-right",
     *    "left", "left-top", "left-bottom", "right", "right-top", "right-bottom"
     *  array or a space separated string of above values
     * @type {?}
     */
    NgtDropdownDirective.prototype.placement;
    /**
     * A selector specifying the element the dropdown should be appended to.
     * Currently only supports "body".
     * @type {?}
     */
    NgtDropdownDirective.prototype.container;
    /**
     *  An event fired when the dropdown is opened or closed.
     *  Event's payload equals whether dropdown is open.
     * @type {?}
     */
    NgtDropdownDirective.prototype.openChange;
    /** @type {?} */
    NgtDropdownDirective.prototype.show;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._changeDetector;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._document;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._ngZone;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._elementRef;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImRyb3Bkb3duL2Ryb3Bkb3duLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUNILGlCQUFpQixFQUNqQixZQUFZLEVBQUUsZUFBZSxFQUM3QixTQUFTLEVBQ1QsVUFBVSxFQUNWLFlBQVksRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFDbkQsTUFBTSxFQUNOLEtBQUssRUFDTCxNQUFNLEVBR04sTUFBTSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQy9CLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLEVBQUUsT0FBTyxFQUFnQixNQUFNLE1BQU0sQ0FBQztBQUU3QyxPQUFPLEVBQTZCLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDbEYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFFbEMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7Ozs7O0FBTXREO0lBbUJJLGtDQUFtQixVQUFtQztRQUFuQyxlQUFVLEdBQVYsVUFBVSxDQUF5QjtRQWY5QyxjQUFTLEdBQUcsS0FBSyxDQUFDO1FBRVUsVUFBSyxHQUFHLElBQUksQ0FBQztRQUNsQixrQkFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7SUFhN0QsQ0FBQztJQVhELHNCQUNJLDhDQUFROzs7O1FBS1o7WUFDSSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDMUIsQ0FBQzs7Ozs7UUFSRCxVQUNhLEtBQWM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxtQkFBSyxLQUFLLEVBQUEsS0FBSyxFQUFFLElBQUksS0FBSyxLQUFLLElBQUksQ0FBQyxDQUFFLG9DQUFvQztZQUMzRixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDeEMsQ0FBQzs7O09BQUE7O2dCQWJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsbUJBQW1CO2lCQUNoQzs7OztnQkF4QkcsVUFBVTs7O3dCQTRCVCxXQUFXLFNBQUMscUJBQXFCO2dDQUNqQyxXQUFXLFNBQUMsZ0JBQWdCOzJCQUU1QixLQUFLOztJQVlWLCtCQUFDO0NBQUEsQUFyQkQsSUFxQkM7U0FsQlksd0JBQXdCOzs7Ozs7SUFDakMsNkNBQTBCOztJQUUxQix5Q0FBaUQ7O0lBQ2pELGlEQUE2RDs7SUFZakQsOENBQTBDOzs7OztBQU0xRDtJQWFJLGtDQUMyRCxRQUFRLEVBQ3ZELFdBQW9DLEVBQ3BDLFNBQW9CO1FBSGhDLGlCQU9DO1FBTjBELGFBQVEsR0FBUixRQUFRLENBQUE7UUFDdkQsZ0JBQVcsR0FBWCxXQUFXLENBQXlCO1FBQ3BDLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFaaEMsY0FBUyxHQUFjLFFBQVEsQ0FBQztRQUNoQyxXQUFNLEdBQUcsS0FBSyxDQUFDO1FBSXFCLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQzdCLFNBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3hCLGVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBTXpELElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLE1BQU07WUFDdEMsS0FBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7UUFDdEIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsbURBQWdCOzs7SUFBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDO0lBQzFDLENBQUM7Ozs7OztJQUVELDJDQUFROzs7OztJQUFSLFVBQVMsU0FBUyxFQUFFLFNBQVM7UUFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUNoRyxDQUFDOzs7OztJQUVELGlEQUFjOzs7O0lBQWQsVUFBZSxVQUFxQjtRQUNoQyx1Q0FBdUM7UUFDdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ2hGLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQztRQUM1Qjs7O1dBR0c7UUFDSCxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ2hGO2FBQU07WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7U0FDbEY7SUFDTCxDQUFDOztnQkE1Q0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxtQkFBbUI7aUJBQ2hDOzs7O2dEQVlRLE1BQU0sU0FBQyxVQUFVOzs7d0JBQUMsY0FBTSxPQUFBLG9CQUFvQixFQUFwQixDQUFvQixFQUFDO2dCQTdEbEQsVUFBVTtnQkFPUyxTQUFTOzs7NEJBK0MzQixlQUFlLFNBQUMsd0JBQXdCOytCQUV4QyxXQUFXLFNBQUMscUJBQXFCO3VCQUNqQyxXQUFXLFNBQUMsWUFBWTs2QkFDeEIsV0FBVyxTQUFDLGtCQUFrQjs7SUFrQ25DLCtCQUFDO0NBQUEsQUE3Q0QsSUE2Q0M7U0ExQ1ksd0JBQXdCOzs7SUFDakMsNkNBQWdDOztJQUNoQywwQ0FBZTs7SUFFZiw2Q0FBMEY7O0lBRTFGLGdEQUF3RDs7SUFDeEQsd0NBQXlEOztJQUN6RCw4Q0FBNkQ7O0lBR3pELDRDQUErRDs7Ozs7SUFDL0QsK0NBQTRDOzs7OztJQUM1Qyw2Q0FBNEI7Ozs7Ozs7O0FBc0NwQztJQVVJLG9DQUFtRSxRQUFRLEVBQ3ZELFdBQW9DO1FBRFcsYUFBUSxHQUFSLFFBQVEsQ0FBQTtRQUN2RCxnQkFBVyxHQUFYLFdBQVcsQ0FBeUI7UUFMbEIsVUFBSyxHQUFHLElBQUksQ0FBQztRQUNYLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLGlCQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUlyRSxJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxhQUFhLENBQUM7SUFDOUMsQ0FBQzs7OztJQUVELHFEQUFnQjs7O0lBQWhCO1FBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQztJQUMxQyxDQUFDOztnQkFqQkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxxQkFBcUI7aUJBQ2xDOzs7O2dEQVFnQixNQUFNLFNBQUMsVUFBVTs7O3dCQUFDLGNBQU0sT0FBQSxvQkFBb0IsRUFBcEIsQ0FBb0IsRUFBQztnQkEvRzFELFVBQVU7Ozt3QkEyR1QsV0FBVyxTQUFDLHVCQUF1QjsrQkFDbkMsV0FBVyxTQUFDLHlCQUF5QjsrQkFDckMsV0FBVyxTQUFDLG9CQUFvQjs7SUFVckMsaUNBQUM7Q0FBQSxBQWxCRCxJQWtCQztTQWZZLDBCQUEwQjs7O0lBQ25DLDhDQUFTOztJQUVULDJDQUFtRDs7SUFDbkQsa0RBQTREOztJQUM1RCxrREFBeUU7O0lBRTdELDhDQUErRDs7Ozs7SUFDL0QsaURBQTRDOzs7Ozs7QUFjNUQ7SUFJZ0Qsc0RBQTBCO0lBVXRFLG9DQUE0RCxRQUFRLEVBQ3hELFVBQW1DO1FBRC9DLFlBRUksa0JBQU0sUUFBUSxFQUFFLFVBQVUsQ0FBQyxTQUM5QjtRQVhxQyxXQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ1gsa0JBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsa0JBQVksR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDOztJQVN6RSxDQUFDOzs7O0lBUHNCLDRDQUFPOzs7SUFBOUI7UUFDSSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7OztJQU9ELCtDQUFVOzs7SUFBVjtRQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7Z0JBckJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQixTQUFTLEVBQUUsQ0FBQyxFQUFDLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxXQUFXLEVBQUUsVUFBVTs7OzRCQUFDLGNBQU0sT0FBQSwwQkFBMEIsRUFBMUIsQ0FBMEIsRUFBQyxFQUFDLENBQUM7aUJBQ2hIOzs7O2dEQVdnQixNQUFNLFNBQUMsVUFBVTs7O3dCQUFDLGNBQU0sT0FBQSxvQkFBb0IsRUFBcEIsQ0FBb0IsRUFBQztnQkE1STFELFVBQVU7Ozt3QkFvSVQsV0FBVyxTQUFDLHVCQUF1QjsrQkFDbkMsV0FBVyxTQUFDLHlCQUF5QjsrQkFDckMsV0FBVyxTQUFDLG9CQUFvQjswQkFFaEMsWUFBWSxTQUFDLE9BQU87O0lBWXpCLGlDQUFDO0NBQUEsQUF0QkQsQ0FJZ0QsMEJBQTBCLEdBa0J6RTtTQWxCWSwwQkFBMEI7OztJQUVuQywyQ0FBbUQ7O0lBQ25ELGtEQUE0RDs7SUFDNUQsa0RBQXlFOzs7OztBQW1CN0U7SUFpRUksOEJBQ1ksZUFBa0MsRUFBRSxNQUF5QixFQUE0QixTQUFjLEVBQ3ZHLE9BQWUsRUFBVSxXQUFvQyxFQUFVLFNBQW9CO1FBRnZHLGlCQVlDO1FBWFcsb0JBQWUsR0FBZixlQUFlLENBQW1CO1FBQXVELGNBQVMsR0FBVCxTQUFTLENBQUs7UUFDdkcsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUFVLGdCQUFXLEdBQVgsV0FBVyxDQUF5QjtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQVc7UUE5RC9GLGFBQVEsR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDOzs7O1FBb0J4QixVQUFLLEdBQUcsS0FBSyxDQUFDOzs7OztRQW9CbkIsZUFBVSxHQUFHLElBQUksWUFBWSxFQUFXLENBQUM7UUFFeEIsU0FBSSxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQXFCNUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUM7UUFDbEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE9BQU8sQ0FBQyxRQUFRLENBQUMsU0FBUzs7O1FBQUM7WUFDaEQsS0FBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3pCLENBQUMsRUFBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxNQUFNO1lBQzVCLEtBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUE1QnFELHdDQUFTOzs7O0lBQS9ELFVBQWdFLE1BQU07UUFDbEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMzQixDQUFDOzs7OztJQUV1RCwrQ0FBZ0I7Ozs7SUFBeEUsVUFBeUUsTUFBTTtRQUMzRSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBRWtELDBDQUFXOzs7O0lBQTlELFVBQStELE1BQU07UUFDakUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMzQixDQUFDOzs7OztJQUVpRCx5Q0FBVTs7OztJQUE1RCxVQUE2RCxNQUFNO1FBQy9ELElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7OztJQWdCRCx1Q0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztRQUM5QixJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDWixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztTQUM1QjtJQUNMLENBQUM7Ozs7O0lBRUQsMENBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQzlCLElBQUksT0FBTyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ2pDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ3hDO1FBRUQsSUFBSSxPQUFPLENBQUMsU0FBUyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUU7WUFDdkQsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7U0FDakM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gscUNBQU07Ozs7SUFBTjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gsbUNBQUk7Ozs7SUFBSjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ2IsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7WUFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQzVCO0lBQ0wsQ0FBQzs7Ozs7SUFFTyxnREFBaUI7Ozs7SUFBekI7UUFBQSxpQkFJQztRQUhHLFlBQVksQ0FDUixJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7OztRQUFFLGNBQU0sT0FBQSxLQUFJLENBQUMsS0FBSyxFQUFFLEVBQVosQ0FBWSxHQUFFLElBQUksQ0FBQyxRQUFRLEVBQy9FLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNsSCxDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gsb0NBQUs7Ozs7SUFBTDtRQUNJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNaLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ25CLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFLENBQUM7U0FDdkM7SUFDTCxDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gscUNBQU07Ozs7SUFBTjtRQUNJLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFO1lBQ2YsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2hCO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDZjtJQUNMLENBQUM7Ozs7SUFFRCwwQ0FBVzs7O0lBQVg7UUFDSSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDekMsQ0FBQzs7Ozs7SUFFRCx3Q0FBUzs7OztJQUFULFVBQVUsS0FBb0I7UUFBOUIsaUJBZ0RDOztZQS9DUyxZQUFZLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixFQUFFO1FBRTVDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFO1lBQ3RCLE9BQU8sS0FBSyxDQUFDO1NBQ2hCOztZQUVHLFFBQVEsR0FBRyxDQUFDLENBQUM7O1lBQ2IsZ0JBQWdCLEdBQUcsS0FBSzs7WUFDdEIsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQztRQUV4RCxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDcEIsWUFBWSxDQUFDLE9BQU87Ozs7O1lBQUMsVUFBQyxXQUFXLEVBQUUsS0FBSztnQkFDcEMsSUFBSSxXQUFXLENBQUMsUUFBUSxDQUFDLG1CQUFBLEtBQUssQ0FBQyxNQUFNLEVBQWUsQ0FBQyxFQUFFO29CQUNuRCxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7aUJBQzNCO2dCQUNELElBQUksV0FBVyxLQUFLLEtBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFO29CQUM5QyxRQUFRLEdBQUcsS0FBSyxDQUFDO2lCQUNwQjtZQUNMLENBQUMsRUFBQyxDQUFDO1NBQ047UUFFRCxJQUFJLGlCQUFpQixJQUFJLGdCQUFnQixFQUFFO1lBQ3ZDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUU7Z0JBQ2hCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUNmO1lBQ0QsdUNBQXVDO1lBQ3ZDLFFBQVEsS0FBSyxDQUFDLEtBQUssRUFBRTtnQkFDakIsS0FBSyxHQUFHLENBQUMsU0FBUztvQkFDZCxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxFQUFFLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQzNELE1BQU07Z0JBQ1YsS0FBSyxHQUFHLENBQUMsT0FBTztvQkFDWixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxRQUFRLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQ3JDLFFBQVEsR0FBRyxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzt3QkFDbkMsTUFBTTtxQkFDVDtvQkFDRCxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNyQyxNQUFNO2dCQUNWLEtBQUssR0FBRyxDQUFDLElBQUk7b0JBQ1QsUUFBUSxHQUFHLENBQUMsQ0FBQztvQkFDYixNQUFNO2dCQUNWLEtBQUssR0FBRyxDQUFDLEdBQUc7b0JBQ1IsUUFBUSxHQUFHLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO29CQUNuQyxNQUFNO2FBQ2I7WUFDRCxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDL0IsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1NBQzFCO0lBQ0wsQ0FBQzs7Ozs7SUFFTyx3Q0FBUzs7OztJQUFqQjtRQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN2RSxDQUFDOzs7Ozs7SUFFTyxpREFBa0I7Ozs7O0lBQTFCLFVBQTJCLEtBQW9CO1FBQzNDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFFBQVEsQ0FBQyxtQkFBQSxLQUFLLENBQUMsTUFBTSxFQUFlLENBQUMsQ0FBQztJQUNqRixDQUFDOzs7OztJQUVPLCtDQUFnQjs7OztJQUF4QjtRQUNJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDcEIsT0FBTyxFQUFFLENBQUM7U0FDYjtRQUNELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFkLENBQWMsRUFBQyxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFBLElBQUksSUFBSSxPQUFBLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUE3QixDQUE2QixFQUFDLENBQUM7SUFDMUcsQ0FBQzs7Ozs7SUFFTyw0Q0FBYTs7OztJQUFyQjtRQUNJLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDN0IsSUFBSSxDQUFDLHNCQUFzQixDQUN2QixnQkFBZ0IsQ0FDWixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQzdGLElBQUksQ0FBQyxTQUFTLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7Ozs7O0lBRU8sOENBQWU7Ozs7SUFBdkI7O1lBQ1UsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTO1FBQy9CLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTs7Z0JBQ2IsZUFBZSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYTs7Z0JBQ2hELG1CQUFtQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYTtZQUUzRCxRQUFRLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO1lBQzNELFFBQVEsQ0FBQyxXQUFXLENBQUMsbUJBQW1CLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDdEQsUUFBUSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsRUFBRSxXQUFXLENBQUMsQ0FBQztTQUMxRDtRQUNELElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNyQixRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztTQUM5QjtJQUNMLENBQUM7Ozs7OztJQUVPLDhDQUFlOzs7OztJQUF2QixVQUF3QixTQUErQjtRQUEvQiwwQkFBQSxFQUFBLGdCQUErQjtRQUNuRCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdkIsSUFBSSxTQUFTLEtBQUssTUFBTSxFQUFFOztnQkFDaEIsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTOztnQkFDekIsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhOztnQkFDckQsYUFBYSxHQUFHLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsSUFBSSxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQztZQUVoRyx3REFBd0Q7WUFDeEQsUUFBUSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQ3pELFFBQVEsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLEVBQUUsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBRTdELFFBQVEsQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLG1CQUFtQixDQUFDLENBQUM7WUFDekQsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsQ0FBQztTQUM1RDtJQUNMLENBQUM7Ozs7OztJQUVPLHFEQUFzQjs7Ozs7SUFBOUIsVUFBK0IsU0FBcUI7UUFDaEQsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1osSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDWixTQUFTLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLG1CQUFBLElBQUksQ0FBQyxTQUFTLEVBQWEsQ0FBQzthQUMvRjs7Z0JBRUssUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTOztnQkFDekIsZUFBZSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYTtZQUV0RCx1Q0FBdUM7WUFDdkMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxlQUFlLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDaEQsUUFBUSxDQUFDLFdBQVcsQ0FBQyxlQUFlLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDbEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7WUFDM0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDOzs7Ozs7Z0JBTTNCLGFBQWEsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFVBQVU7WUFDN0UsUUFBUSxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsYUFBYSxDQUFDLENBQUM7O2dCQUU1QyxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWM7WUFDekMsSUFBSSxhQUFhLEVBQUU7Z0JBQ2YsUUFBUSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQzlDLFFBQVEsQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFVBQVUsQ0FBQyxDQUFDO2dCQUNoRCxRQUFRLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxhQUFhLENBQUMsQ0FBQzthQUNuRDtTQUNKO0lBQ0wsQ0FBQzs7Z0JBaFNKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsZUFBZTtvQkFDekIsUUFBUSxFQUFFLGFBQWE7aUJBQzFCOzs7O2dCQS9KRyxpQkFBaUI7Z0JBbUJaLGlCQUFpQjtnREEyTXNELE1BQU0sU0FBQyxRQUFRO2dCQXZOM0YsTUFBTTtnQkFKTixVQUFVO2dCQU9TLFNBQVM7Ozt3QkEySjNCLFlBQVksU0FBQyx3QkFBd0I7K0JBQ3JDLFlBQVksU0FBQyx3QkFBd0IsRUFBRSxFQUFDLElBQUksRUFBRSxVQUFVLEVBQUM7MEJBQ3pELFlBQVksU0FBQywwQkFBMEI7NEJBU3ZDLEtBQUs7d0JBS0wsS0FBSyxTQUFDLE1BQU07NEJBUVosS0FBSzs0QkFNTCxLQUFLOzZCQU1MLE1BQU07dUJBRU4sV0FBVyxTQUFDLFlBQVk7NEJBRXhCLFlBQVksU0FBQywwQkFBMEIsRUFBRSxDQUFDLFFBQVEsQ0FBQzttQ0FJbkQsWUFBWSxTQUFDLDRCQUE0QixFQUFFLENBQUMsUUFBUSxDQUFDOzhCQUlyRCxZQUFZLFNBQUMsdUJBQXVCLEVBQUUsQ0FBQyxRQUFRLENBQUM7NkJBSWhELFlBQVksU0FBQyxzQkFBc0IsRUFBRSxDQUFDLFFBQVEsQ0FBQzs7SUFvT3BELDJCQUFDO0NBQUEsQUFqU0QsSUFpU0M7U0E3Ulksb0JBQW9COzs7Ozs7SUFDN0Isd0NBQXVDOzs7OztJQUN2QyxpREFBd0M7Ozs7O0lBQ3hDLDhDQUFvQzs7Ozs7SUFFcEMscUNBQWdGOzs7OztJQUNoRiw0Q0FBNkY7Ozs7O0lBQzdGLHVDQUFzRjs7Ozs7Ozs7O0lBU3RGLHlDQUFtRDs7Ozs7SUFLbkQscUNBQTZCOzs7Ozs7OztJQVE3Qix5Q0FBbUM7Ozs7OztJQU1uQyx5Q0FBa0M7Ozs7OztJQU1sQywwQ0FBbUQ7O0lBRW5ELG9DQUFnRDs7Ozs7SUFtQjVDLCtDQUEwQzs7Ozs7SUFBNkIseUNBQXdDOzs7OztJQUMvRyx1Q0FBdUI7Ozs7O0lBQUUsMkNBQTRDOzs7OztJQUFFLHlDQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgIENvbnRlbnRDaGlsZCwgQ29udGVudENoaWxkcmVuLFxyXG4gICAgRGlyZWN0aXZlLFxyXG4gICAgRWxlbWVudFJlZixcclxuICAgIEV2ZW50RW1pdHRlciwgZm9yd2FyZFJlZiwgSG9zdEJpbmRpbmcsIEhvc3RMaXN0ZW5lcixcclxuICAgIEluamVjdCxcclxuICAgIElucHV0LFxyXG4gICAgTmdab25lLCBPbkNoYW5nZXMsXHJcbiAgICBPbkRlc3Ryb3ksXHJcbiAgICBPbkluaXQsXHJcbiAgICBPdXRwdXQsIFF1ZXJ5TGlzdCwgUmVuZGVyZXIyLCBTaW1wbGVDaGFuZ2VzXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERPQ1VNRU5UIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgU3ViamVjdCwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcblxyXG5pbXBvcnQgeyBQbGFjZW1lbnQsIFBsYWNlbWVudEFycmF5LCBwb3NpdGlvbkVsZW1lbnRzIH0gZnJvbSAnLi4vdXRpbC9wb3NpdGlvbmluZyc7XHJcbmltcG9ydCB7IG5ndEF1dG9DbG9zZSB9IGZyb20gJy4uL3V0aWwvYXV0b2Nsb3NlJztcclxuaW1wb3J0IHsgS2V5IH0gZnJvbSAnLi4vdXRpbC9rZXknO1xyXG5cclxuaW1wb3J0IHsgTmd0RHJvcGRvd25Db25maWcgfSBmcm9tICcuL2Ryb3Bkb3duLWNvbmZpZyc7XHJcblxyXG4vKipcclxuICogQSBkaXJlY3RpdmUgeW91IHNob3VsZCBwdXQgcHV0IG9uIGEgZHJvcGRvd24gaXRlbSB0byBlbmFibGUga2V5Ym9hcmQgbmF2aWdhdGlvbi5cclxuICogS2V5Ym9hcmQgbmF2aWdhdGlvbiB1c2luZyBhcnJvdyBrZXlzIHdpbGwgbW92ZSBmb2N1cyBiZXR3ZWVuIGl0ZW1zIG1hcmtlZCB3aXRoIHRoaXMgZGlyZWN0aXZlLlxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3REcm9wZG93bkl0ZW1dJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0RHJvcGRvd25JdGVtRGlyZWN0aXZlIHtcclxuICAgIHByaXZhdGUgX2Rpc2FibGVkID0gZmFsc2U7XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5kcm9wZG93bi1pdGVtJykgY2xhc3MgPSB0cnVlO1xyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5kaXNhYmxlZCcpIGNsYXNzRGlzYWJsZWQgPSB0aGlzLmRpc2FibGVkO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzZXQgZGlzYWJsZWQodmFsdWU6IGJvb2xlYW4pIHtcclxuICAgICAgICB0aGlzLl9kaXNhYmxlZCA9IDxhbnk+dmFsdWUgPT09ICcnIHx8IHZhbHVlID09PSB0cnVlOyAgLy8gYWNjZXB0IGFuIGVtcHR5IGF0dHJpYnV0ZSBhcyB0cnVlXHJcbiAgICAgICAgdGhpcy5jbGFzc0Rpc2FibGVkID0gdGhpcy5fZGlzYWJsZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRpc2FibGVkKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9kaXNhYmxlZDtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZWxlbWVudFJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4pIHtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndERyb3Bkb3duTWVudV0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3REcm9wZG93bk1lbnVEaXJlY3RpdmUge1xyXG4gICAgcGxhY2VtZW50OiBQbGFjZW1lbnQgPSAnYm90dG9tJztcclxuICAgIGlzT3BlbiA9IGZhbHNlO1xyXG5cclxuICAgIEBDb250ZW50Q2hpbGRyZW4oTmd0RHJvcGRvd25JdGVtRGlyZWN0aXZlKSBtZW51SXRlbXM6IFF1ZXJ5TGlzdDxOZ3REcm9wZG93bkl0ZW1EaXJlY3RpdmU+O1xyXG5cclxuICAgIEBIb3N0QmluZGluZygnY2xhc3MuZHJvcGRvd24tbWVudScpIGRyb3Bkb3duTWVudSA9IHRydWU7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLnNob3cnKSBzaG93ID0gdGhpcy5kcm9wZG93bi5pc09wZW4oKTtcclxuICAgIEBIb3N0QmluZGluZygnYXR0ci54LXBsYWNlbWVudCcpIHhQbGFjZW1lbnQgPSB0aGlzLnBsYWNlbWVudDtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBASW5qZWN0KGZvcndhcmRSZWYoKCkgPT4gTmd0RHJvcGRvd25EaXJlY3RpdmUpKSBwdWJsaWMgZHJvcGRvd24sXHJcbiAgICAgICAgcHJpdmF0ZSBfZWxlbWVudFJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4sXHJcbiAgICAgICAgcHJpdmF0ZSBfcmVuZGVyZXI6IFJlbmRlcmVyMikge1xyXG4gICAgICAgIHRoaXMuZHJvcGRvd24ub3BlbkNoYW5nZS5zdWJzY3JpYmUoc3RhdHVzID0+IHtcclxuICAgICAgICAgICB0aGlzLnNob3cgPSBzdGF0dXM7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TmF0aXZlRWxlbWVudCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIHBvc2l0aW9uKHRyaWdnZXJFbCwgcGxhY2VtZW50KSB7XHJcbiAgICAgICAgdGhpcy5hcHBseVBsYWNlbWVudChwb3NpdGlvbkVsZW1lbnRzKHRyaWdnZXJFbCwgdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCBwbGFjZW1lbnQpKTtcclxuICAgIH1cclxuXHJcbiAgICBhcHBseVBsYWNlbWVudChfcGxhY2VtZW50OiBQbGFjZW1lbnQpIHtcclxuICAgICAgICAvLyByZW1vdmUgdGhlIGN1cnJlbnQgcGxhY2VtZW50IGNsYXNzZXNcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQucGFyZW50Tm9kZSwgJ2Ryb3B1cCcpO1xyXG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZUNsYXNzKHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5wYXJlbnROb2RlLCAnZHJvcGRvd24nKTtcclxuICAgICAgICB0aGlzLnBsYWNlbWVudCA9IF9wbGFjZW1lbnQ7XHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogYXBwbHkgdGhlIG5ldyBwbGFjZW1lbnRcclxuICAgICAgICAgKiBpbiBjYXNlIG9mIHRvcCB1c2UgdXAtYXJyb3cgb3IgZG93bi1hcnJvdyBvdGhlcndpc2VcclxuICAgICAgICAgKi9cclxuICAgICAgICBpZiAoX3BsYWNlbWVudC5zZWFyY2goJ150b3AnKSAhPT0gLTEpIHtcclxuICAgICAgICAgICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnBhcmVudE5vZGUsICdkcm9wdXAnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQucGFyZW50Tm9kZSwgJ2Ryb3Bkb3duJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5cclxuLyoqXHJcbiAqIE1hcmtzIGFuIGVsZW1lbnQgdG8gd2hpY2ggZHJvcGRvd24gbWVudSB3aWxsIGJlIGFuY2hvcmVkLiBUaGlzIGlzIGEgc2ltcGxlIHZlcnNpb25cclxuICogb2YgdGhlIE5ndERyb3Bkb3duVG9nZ2xlRGlyZWN0aXZlLiBJdCBwbGF5cyB0aGUgc2FtZSByb2xlIGFzIE5ndERyb3Bkb3duVG9nZ2xlRGlyZWN0aXZlIGJ1dFxyXG4gKiBkb2Vzbid0IGxpc3RlbiB0byBjbGljayBldmVudHMgdG8gdG9nZ2xlIGRyb3Bkb3duIG1lbnUgdGh1cyBlbmFibGluZyBzdXBwb3J0IGZvclxyXG4gKiBldmVudHMgb3RoZXIgdGhhbiBjbGljay5cclxuICovXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbbmd0RHJvcGRvd25BbmNob3JdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0RHJvcGRvd25BbmNob3JEaXJlY3RpdmUge1xyXG4gICAgYW5jaG9yRWw7XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5kcm9wZG93bi10b2dnbGUnKSBjbGFzcyA9IHRydWU7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2F0dHIuYXJpYS1oYXNwb3B1cC50cnVlJykgYXJpYUhhc3BvcHVwID0gdHJ1ZTtcclxuICAgIEBIb3N0QmluZGluZygnYXR0ci5hcmlhLWV4cGFuZGVkJykgYXJpYUV4cGFuZGVkID0gdGhpcy5kcm9wZG93bi5pc09wZW4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihASW5qZWN0KGZvcndhcmRSZWYoKCkgPT4gTmd0RHJvcGRvd25EaXJlY3RpdmUpKSBwdWJsaWMgZHJvcGRvd24sXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9lbGVtZW50UmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50Pikge1xyXG4gICAgICAgIHRoaXMuYW5jaG9yRWwgPSBfZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIGdldE5hdGl2ZUVsZW1lbnQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudDtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi8qKlxyXG4gKiBBbGxvd3MgdGhlIGRyb3Bkb3duIHRvIGJlIHRvZ2dsZWQgdmlhIGNsaWNrLiBUaGlzIGRpcmVjdGl2ZSBpcyBvcHRpb25hbDogeW91IGNhbiB1c2UgTmd0RHJvcGRvd25BbmNob3JEaXJlY3RpdmUgYXMgYW5cclxuICogYWx0ZXJuYXRpdmUuXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndERyb3Bkb3duVG9nZ2xlXScsXHJcbiAgICBwcm92aWRlcnM6IFt7cHJvdmlkZTogTmd0RHJvcGRvd25BbmNob3JEaXJlY3RpdmUsIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IE5ndERyb3Bkb3duVG9nZ2xlRGlyZWN0aXZlKX1dXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3REcm9wZG93blRvZ2dsZURpcmVjdGl2ZSBleHRlbmRzIE5ndERyb3Bkb3duQW5jaG9yRGlyZWN0aXZlIHtcclxuXHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLmRyb3Bkb3duLXRvZ2dsZScpIGNsYXNzID0gdHJ1ZTtcclxuICAgIEBIb3N0QmluZGluZygnYXR0ci5hcmlhLWhhc3BvcHVwLnRydWUnKSBhcmlhSGFzcG9wdXAgPSB0cnVlO1xyXG4gICAgQEhvc3RCaW5kaW5nKCdhdHRyLmFyaWEtZXhwYW5kZWQnKSBhcmlhRXhwYW5kZWQgPSB0aGlzLmRyb3Bkb3duLmlzT3BlbigpO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJykgb25DbGljaygpIHtcclxuICAgICAgICB0aGlzLnRvZ2dsZU9wZW4oKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihASW5qZWN0KGZvcndhcmRSZWYoKCkgPT4gTmd0RHJvcGRvd25EaXJlY3RpdmUpKSBkcm9wZG93bixcclxuICAgICAgICAgICAgICAgIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWY8SFRNTEVsZW1lbnQ+KSB7XHJcbiAgICAgICAgc3VwZXIoZHJvcGRvd24sIGVsZW1lbnRSZWYpO1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZU9wZW4oKSB7XHJcbiAgICAgICAgdGhpcy5kcm9wZG93bi50b2dnbGUoKTtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIFRyYW5zZm9ybXMgYSBub2RlIGludG8gYSBkcm9wZG93bi5cclxuICovXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbbmd0RHJvcGRvd25dJyxcclxuICAgIGV4cG9ydEFzOiAnbmd0RHJvcGRvd24nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3REcm9wZG93bkRpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95LCBPbkNoYW5nZXMge1xyXG4gICAgcHJpdmF0ZSBfY2xvc2VkJCA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XHJcbiAgICBwcml2YXRlIF96b25lU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcbiAgICBwcml2YXRlIF9ib2R5Q29udGFpbmVyOiBIVE1MRWxlbWVudDtcclxuXHJcbiAgICBAQ29udGVudENoaWxkKE5ndERyb3Bkb3duTWVudURpcmVjdGl2ZSkgcHJpdmF0ZSBfbWVudTogTmd0RHJvcGRvd25NZW51RGlyZWN0aXZlO1xyXG4gICAgQENvbnRlbnRDaGlsZChOZ3REcm9wZG93bk1lbnVEaXJlY3RpdmUsIHtyZWFkOiBFbGVtZW50UmVmfSkgcHJpdmF0ZSBfbWVudUVsZW1lbnQ6IEVsZW1lbnRSZWY7XHJcbiAgICBAQ29udGVudENoaWxkKE5ndERyb3Bkb3duQW5jaG9yRGlyZWN0aXZlKSBwcml2YXRlIF9hbmNob3I6IE5ndERyb3Bkb3duQW5jaG9yRGlyZWN0aXZlO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW5kaWNhdGVzIHRoYXQgZHJvcGRvd24gc2hvdWxkIGJlIGNsb3NlZCB3aGVuIHNlbGVjdGluZyBvbmUgb2YgZHJvcGRvd24gaXRlbXMgKGNsaWNrKSBvciBwcmVzc2luZyBFU0MuXHJcbiAgICAgKiBXaGVuIGl0IGlzIHRydWUgKGRlZmF1bHQpIGRyb3Bkb3ducyBhcmUgYXV0b21hdGljYWxseSBjbG9zZWQgb24gYm90aCBvdXRzaWRlIGFuZCBpbnNpZGUgKG1lbnUpIGNsaWNrcy5cclxuICAgICAqIFdoZW4gaXQgaXMgZmFsc2UgZHJvcGRvd25zIGFyZSBuZXZlciBhdXRvbWF0aWNhbGx5IGNsb3NlZC5cclxuICAgICAqIFdoZW4gaXQgaXMgJ291dHNpZGUnIGRyb3Bkb3ducyBhcmUgYXV0b21hdGljYWxseSBjbG9zZWQgb24gb3V0c2lkZSBjbGlja3MgYnV0IG5vdCBvbiBtZW51IGNsaWNrcy5cclxuICAgICAqIFdoZW4gaXQgaXMgJ2luc2lkZScgZHJvcGRvd25zIGFyZSBhdXRvbWF0aWNhbGx5IG9uIG1lbnUgY2xpY2tzIGJ1dCBub3Qgb24gb3V0c2lkZSBjbGlja3MuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGF1dG9DbG9zZTogYm9vbGVhbiB8ICdvdXRzaWRlJyB8ICdpbnNpZGUnO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogIERlZmluZXMgd2hldGhlciBvciBub3QgdGhlIGRyb3Bkb3duLW1lbnUgaXMgb3BlbiBpbml0aWFsbHkuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgnb3BlbicpIF9vcGVuID0gZmFsc2U7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQbGFjZW1lbnQgb2YgYSBwb3BvdmVyIGFjY2VwdHM6XHJcbiAgICAgKiAgICBcInRvcFwiLCBcInRvcC1sZWZ0XCIsIFwidG9wLXJpZ2h0XCIsIFwiYm90dG9tXCIsIFwiYm90dG9tLWxlZnRcIiwgXCJib3R0b20tcmlnaHRcIixcclxuICAgICAqICAgIFwibGVmdFwiLCBcImxlZnQtdG9wXCIsIFwibGVmdC1ib3R0b21cIiwgXCJyaWdodFwiLCBcInJpZ2h0LXRvcFwiLCBcInJpZ2h0LWJvdHRvbVwiXHJcbiAgICAgKiAgYXJyYXkgb3IgYSBzcGFjZSBzZXBhcmF0ZWQgc3RyaW5nIG9mIGFib3ZlIHZhbHVlc1xyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBwbGFjZW1lbnQ6IFBsYWNlbWVudEFycmF5O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQSBzZWxlY3RvciBzcGVjaWZ5aW5nIHRoZSBlbGVtZW50IHRoZSBkcm9wZG93biBzaG91bGQgYmUgYXBwZW5kZWQgdG8uXHJcbiAgICAgKiBDdXJyZW50bHkgb25seSBzdXBwb3J0cyBcImJvZHlcIi5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgY29udGFpbmVyOiBudWxsIHwgJ2JvZHknO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogIEFuIGV2ZW50IGZpcmVkIHdoZW4gdGhlIGRyb3Bkb3duIGlzIG9wZW5lZCBvciBjbG9zZWQuXHJcbiAgICAgKiAgRXZlbnQncyBwYXlsb2FkIGVxdWFscyB3aGV0aGVyIGRyb3Bkb3duIGlzIG9wZW4uXHJcbiAgICAgKi9cclxuICAgIEBPdXRwdXQoKSBvcGVuQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxib29sZWFuPigpO1xyXG5cclxuICAgIEBIb3N0QmluZGluZygnY2xhc3Muc2hvdycpIHNob3cgPSB0aGlzLmlzT3BlbigpO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmtleWRvd24uQXJyb3dVcCcsIFsnJGV2ZW50J10pIG9uQXJyb3dVcCgkZXZlbnQpIHtcclxuICAgICAgICB0aGlzLm9uS2V5RG93bigkZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmtleWRvd24uQXJyb3dEb3duJywgWyckZXZlbnQnXSkgb25BcnJvd0Fycm93RG93bigkZXZlbnQpIHtcclxuICAgICAgICB0aGlzLm9uS2V5RG93bigkZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmtleWRvd24uSG9tZScsIFsnJGV2ZW50J10pIG9uQXJyb3dIb21lKCRldmVudCkge1xyXG4gICAgICAgIHRoaXMub25LZXlEb3duKCRldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6a2V5ZG93bi5FbmQnLCBbJyRldmVudCddKSBvbkFycm93RW5kKCRldmVudCkge1xyXG4gICAgICAgIHRoaXMub25LZXlEb3duKCRldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBfY2hhbmdlRGV0ZWN0b3I6IENoYW5nZURldGVjdG9yUmVmLCBjb25maWc6IE5ndERyb3Bkb3duQ29uZmlnLCBASW5qZWN0KERPQ1VNRU5UKSBwcml2YXRlIF9kb2N1bWVudDogYW55LFxyXG4gICAgICAgIHByaXZhdGUgX25nWm9uZTogTmdab25lLCBwcml2YXRlIF9lbGVtZW50UmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PiwgcHJpdmF0ZSBfcmVuZGVyZXI6IFJlbmRlcmVyMikge1xyXG4gICAgICAgIHRoaXMucGxhY2VtZW50ID0gY29uZmlnLnBsYWNlbWVudDtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lciA9IGNvbmZpZy5jb250YWluZXI7XHJcbiAgICAgICAgdGhpcy5hdXRvQ2xvc2UgPSBjb25maWcuYXV0b0Nsb3NlO1xyXG4gICAgICAgIHRoaXMuX3pvbmVTdWJzY3JpcHRpb24gPSBfbmdab25lLm9uU3RhYmxlLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Bvc2l0aW9uTWVudSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMub3BlbkNoYW5nZS5zdWJzY3JpYmUoc3RhdHVzID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zaG93ID0gc3RhdHVzO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuX2FwcGx5UGxhY2VtZW50Q2xhc3NlcygpO1xyXG4gICAgICAgIGlmICh0aGlzLl9vcGVuKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3NldENsb3NlSGFuZGxlcnMoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgICAgIGlmIChjaGFuZ2VzLmNvbnRhaW5lciAmJiB0aGlzLl9vcGVuKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2FwcGx5Q29udGFpbmVyKHRoaXMuY29udGFpbmVyKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChjaGFuZ2VzLnBsYWNlbWVudCAmJiAhY2hhbmdlcy5wbGFjZW1lbnQuaXNGaXJzdENoYW5nZSkge1xyXG4gICAgICAgICAgICB0aGlzLl9hcHBseVBsYWNlbWVudENsYXNzZXMoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3MgaWYgdGhlIGRyb3Bkb3duIG1lbnUgaXMgb3BlbiBvciBub3QuXHJcbiAgICAgKi9cclxuICAgIGlzT3BlbigpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fb3BlbjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE9wZW5zIHRoZSBkcm9wZG93biBtZW51IG9mIGEgZ2l2ZW4gbmF2YmFyIG9yIHRhYmJlZCBuYXZpZ2F0aW9uLlxyXG4gICAgICovXHJcbiAgICBvcGVuKCk6IHZvaWQge1xyXG4gICAgICAgIGlmICghdGhpcy5fb3Blbikge1xyXG4gICAgICAgICAgICB0aGlzLl9vcGVuID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5fYXBwbHlDb250YWluZXIodGhpcy5jb250YWluZXIpO1xyXG4gICAgICAgICAgICB0aGlzLl9wb3NpdGlvbk1lbnUoKTtcclxuICAgICAgICAgICAgdGhpcy5vcGVuQ2hhbmdlLmVtaXQodHJ1ZSk7XHJcbiAgICAgICAgICAgIHRoaXMuX3NldENsb3NlSGFuZGxlcnMoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfc2V0Q2xvc2VIYW5kbGVycygpIHtcclxuICAgICAgICBuZ3RBdXRvQ2xvc2UoXHJcbiAgICAgICAgICAgIHRoaXMuX25nWm9uZSwgdGhpcy5fZG9jdW1lbnQsIHRoaXMuYXV0b0Nsb3NlLCAoKSA9PiB0aGlzLmNsb3NlKCksIHRoaXMuX2Nsb3NlZCQsXHJcbiAgICAgICAgICAgIHRoaXMuX21lbnUgPyBbdGhpcy5fbWVudS5nZXROYXRpdmVFbGVtZW50KCldIDogW10sIHRoaXMuX2FuY2hvciA/IFt0aGlzLl9hbmNob3IuZ2V0TmF0aXZlRWxlbWVudCgpXSA6IFtdKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENsb3NlcyB0aGUgZHJvcGRvd24gbWVudSBvZiBhIGdpdmVuIG5hdmJhciBvciB0YWJiZWQgbmF2aWdhdGlvbi5cclxuICAgICAqL1xyXG4gICAgY2xvc2UoKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHRoaXMuX29wZW4pIHtcclxuICAgICAgICAgICAgdGhpcy5fb3BlbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLl9yZXNldENvbnRhaW5lcigpO1xyXG4gICAgICAgICAgICB0aGlzLl9jbG9zZWQkLm5leHQoKTtcclxuICAgICAgICAgICAgdGhpcy5vcGVuQ2hhbmdlLmVtaXQoZmFsc2UpO1xyXG4gICAgICAgICAgICB0aGlzLl9jaGFuZ2VEZXRlY3Rvci5tYXJrRm9yQ2hlY2soKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUb2dnbGVzIHRoZSBkcm9wZG93biBtZW51IG9mIGEgZ2l2ZW4gbmF2YmFyIG9yIHRhYmJlZCBuYXZpZ2F0aW9uLlxyXG4gICAgICovXHJcbiAgICB0b2dnbGUoKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNPcGVuKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5jbG9zZSgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMub3BlbigpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLl9yZXNldENvbnRhaW5lcigpO1xyXG5cclxuICAgICAgICB0aGlzLl9jbG9zZWQkLm5leHQoKTtcclxuICAgICAgICB0aGlzLl96b25lU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25LZXlEb3duKGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XHJcbiAgICAgICAgY29uc3QgaXRlbUVsZW1lbnRzID0gdGhpcy5fZ2V0TWVudUVsZW1lbnRzKCk7XHJcblxyXG4gICAgICAgIGlmICghaXRlbUVsZW1lbnRzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBsZXQgcG9zaXRpb24gPSAtMTtcclxuICAgICAgICBsZXQgaXNFdmVudEZyb21JdGVtcyA9IGZhbHNlO1xyXG4gICAgICAgIGNvbnN0IGlzRXZlbnRGcm9tVG9nZ2xlID0gdGhpcy5faXNFdmVudEZyb21Ub2dnbGUoZXZlbnQpO1xyXG5cclxuICAgICAgICBpZiAoIWlzRXZlbnRGcm9tVG9nZ2xlKSB7XHJcbiAgICAgICAgICAgIGl0ZW1FbGVtZW50cy5mb3JFYWNoKChpdGVtRWxlbWVudCwgaW5kZXgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChpdGVtRWxlbWVudC5jb250YWlucyhldmVudC50YXJnZXQgYXMgSFRNTEVsZW1lbnQpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaXNFdmVudEZyb21JdGVtcyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoaXRlbUVsZW1lbnQgPT09IHRoaXMuX2RvY3VtZW50LmFjdGl2ZUVsZW1lbnQpIHtcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbiA9IGluZGV4O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChpc0V2ZW50RnJvbVRvZ2dsZSB8fCBpc0V2ZW50RnJvbUl0ZW1zKSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5pc09wZW4oKSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vcGVuKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOmRlcHJlY2F0aW9uXHJcbiAgICAgICAgICAgIHN3aXRjaCAoZXZlbnQud2hpY2gpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgS2V5LkFycm93RG93bjpcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbiA9IE1hdGgubWluKHBvc2l0aW9uICsgMSwgaXRlbUVsZW1lbnRzLmxlbmd0aCAtIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBLZXkuQXJyb3dVcDpcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5faXNEcm9wdXAoKSAmJiBwb3NpdGlvbiA9PT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb24gPSBpdGVtRWxlbWVudHMubGVuZ3RoIC0gMTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uID0gTWF0aC5tYXgocG9zaXRpb24gLSAxLCAwKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgS2V5LkhvbWU6XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb24gPSAwO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBLZXkuRW5kOlxyXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uID0gaXRlbUVsZW1lbnRzLmxlbmd0aCAtIDE7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaXRlbUVsZW1lbnRzW3Bvc2l0aW9uXS5mb2N1cygpO1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9pc0Ryb3B1cCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LmNsYXNzTGlzdC5jb250YWlucygnZHJvcHVwJyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfaXNFdmVudEZyb21Ub2dnbGUoZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fYW5jaG9yLmdldE5hdGl2ZUVsZW1lbnQoKS5jb250YWlucyhldmVudC50YXJnZXQgYXMgSFRNTEVsZW1lbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2dldE1lbnVFbGVtZW50cygpOiBIVE1MRWxlbWVudFtdIHtcclxuICAgICAgICBpZiAodGhpcy5fbWVudSA9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBbXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX21lbnUubWVudUl0ZW1zLmZpbHRlcihpdGVtID0+ICFpdGVtLmRpc2FibGVkKS5tYXAoaXRlbSA9PiBpdGVtLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfcG9zaXRpb25NZW51KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzT3BlbigpICYmIHRoaXMuX21lbnUpIHtcclxuICAgICAgICAgICAgdGhpcy5fYXBwbHlQbGFjZW1lbnRDbGFzc2VzKFxyXG4gICAgICAgICAgICAgICAgcG9zaXRpb25FbGVtZW50cyhcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9hbmNob3IuYW5jaG9yRWwsIHRoaXMuX2JvZHlDb250YWluZXIgfHwgdGhpcy5fbWVudUVsZW1lbnQubmF0aXZlRWxlbWVudCwgdGhpcy5wbGFjZW1lbnQsXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb250YWluZXIgPT09ICdib2R5JykpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9yZXNldENvbnRhaW5lcigpIHtcclxuICAgICAgICBjb25zdCByZW5kZXJlciA9IHRoaXMuX3JlbmRlcmVyO1xyXG4gICAgICAgIGlmICh0aGlzLl9tZW51RWxlbWVudCkge1xyXG4gICAgICAgICAgICBjb25zdCBkcm9wZG93bkVsZW1lbnQgPSB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICAgICAgICAgIGNvbnN0IGRyb3Bkb3duTWVudUVsZW1lbnQgPSB0aGlzLl9tZW51RWxlbWVudC5uYXRpdmVFbGVtZW50O1xyXG5cclxuICAgICAgICAgICAgcmVuZGVyZXIuYXBwZW5kQ2hpbGQoZHJvcGRvd25FbGVtZW50LCBkcm9wZG93bk1lbnVFbGVtZW50KTtcclxuICAgICAgICAgICAgcmVuZGVyZXIucmVtb3ZlU3R5bGUoZHJvcGRvd25NZW51RWxlbWVudCwgJ3Bvc2l0aW9uJyk7XHJcbiAgICAgICAgICAgIHJlbmRlcmVyLnJlbW92ZVN0eWxlKGRyb3Bkb3duTWVudUVsZW1lbnQsICd0cmFuc2Zvcm0nKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuX2JvZHlDb250YWluZXIpIHtcclxuICAgICAgICAgICAgcmVuZGVyZXIucmVtb3ZlQ2hpbGQodGhpcy5fZG9jdW1lbnQuYm9keSwgdGhpcy5fYm9keUNvbnRhaW5lcik7XHJcbiAgICAgICAgICAgIHRoaXMuX2JvZHlDb250YWluZXIgPSBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9hcHBseUNvbnRhaW5lcihjb250YWluZXI6IG51bGwgfCAnYm9keScgPSBudWxsKSB7XHJcbiAgICAgICAgdGhpcy5fcmVzZXRDb250YWluZXIoKTtcclxuICAgICAgICBpZiAoY29udGFpbmVyID09PSAnYm9keScpIHtcclxuICAgICAgICAgICAgY29uc3QgcmVuZGVyZXIgPSB0aGlzLl9yZW5kZXJlcjtcclxuICAgICAgICAgICAgY29uc3QgZHJvcGRvd25NZW51RWxlbWVudCA9IHRoaXMuX21lbnVFbGVtZW50Lm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICAgICAgICAgIGNvbnN0IGJvZHlDb250YWluZXIgPSB0aGlzLl9ib2R5Q29udGFpbmVyID0gdGhpcy5fYm9keUNvbnRhaW5lciB8fCByZW5kZXJlci5jcmVhdGVFbGVtZW50KCdkaXYnKTtcclxuXHJcbiAgICAgICAgICAgIC8vIE92ZXJyaWRlIHNvbWUgc3R5bGVzIHRvIGhhdmUgdGhlIHBvc2l0aW9ubmluZyB3b3JraW5nXHJcbiAgICAgICAgICAgIHJlbmRlcmVyLnNldFN0eWxlKGJvZHlDb250YWluZXIsICdwb3NpdGlvbicsICdhYnNvbHV0ZScpO1xyXG4gICAgICAgICAgICByZW5kZXJlci5zZXRTdHlsZShkcm9wZG93bk1lbnVFbGVtZW50LCAncG9zaXRpb24nLCAnc3RhdGljJyk7XHJcblxyXG4gICAgICAgICAgICByZW5kZXJlci5hcHBlbmRDaGlsZChib2R5Q29udGFpbmVyLCBkcm9wZG93bk1lbnVFbGVtZW50KTtcclxuICAgICAgICAgICAgcmVuZGVyZXIuYXBwZW5kQ2hpbGQodGhpcy5fZG9jdW1lbnQuYm9keSwgYm9keUNvbnRhaW5lcik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2FwcGx5UGxhY2VtZW50Q2xhc3NlcyhwbGFjZW1lbnQ/OiBQbGFjZW1lbnQpIHtcclxuICAgICAgICBpZiAodGhpcy5fbWVudSkge1xyXG4gICAgICAgICAgICBpZiAoIXBsYWNlbWVudCkge1xyXG4gICAgICAgICAgICAgICAgcGxhY2VtZW50ID0gQXJyYXkuaXNBcnJheSh0aGlzLnBsYWNlbWVudCkgPyB0aGlzLnBsYWNlbWVudFswXSA6IHRoaXMucGxhY2VtZW50IGFzIFBsYWNlbWVudDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgcmVuZGVyZXIgPSB0aGlzLl9yZW5kZXJlcjtcclxuICAgICAgICAgICAgY29uc3QgZHJvcGRvd25FbGVtZW50ID0gdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50O1xyXG5cclxuICAgICAgICAgICAgLy8gcmVtb3ZlIHRoZSBjdXJyZW50IHBsYWNlbWVudCBjbGFzc2VzXHJcbiAgICAgICAgICAgIHJlbmRlcmVyLnJlbW92ZUNsYXNzKGRyb3Bkb3duRWxlbWVudCwgJ2Ryb3B1cCcpO1xyXG4gICAgICAgICAgICByZW5kZXJlci5yZW1vdmVDbGFzcyhkcm9wZG93bkVsZW1lbnQsICdkcm9wZG93bicpO1xyXG4gICAgICAgICAgICB0aGlzLnBsYWNlbWVudCA9IHBsYWNlbWVudDtcclxuICAgICAgICAgICAgdGhpcy5fbWVudS5wbGFjZW1lbnQgPSBwbGFjZW1lbnQ7XHJcblxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAqIGFwcGx5IHRoZSBuZXcgcGxhY2VtZW50XHJcbiAgICAgICAgICAgICogaW4gY2FzZSBvZiB0b3AgdXNlIHVwLWFycm93IG9yIGRvd24tYXJyb3cgb3RoZXJ3aXNlXHJcbiAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgIGNvbnN0IGRyb3Bkb3duQ2xhc3MgPSBwbGFjZW1lbnQuc2VhcmNoKCdedG9wJykgIT09IC0xID8gJ2Ryb3B1cCcgOiAnZHJvcGRvd24nO1xyXG4gICAgICAgICAgICByZW5kZXJlci5hZGRDbGFzcyhkcm9wZG93bkVsZW1lbnQsIGRyb3Bkb3duQ2xhc3MpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgYm9keUNvbnRhaW5lciA9IHRoaXMuX2JvZHlDb250YWluZXI7XHJcbiAgICAgICAgICAgIGlmIChib2R5Q29udGFpbmVyKSB7XHJcbiAgICAgICAgICAgICAgICByZW5kZXJlci5yZW1vdmVDbGFzcyhib2R5Q29udGFpbmVyLCAnZHJvcHVwJyk7XHJcbiAgICAgICAgICAgICAgICByZW5kZXJlci5yZW1vdmVDbGFzcyhib2R5Q29udGFpbmVyLCAnZHJvcGRvd24nKTtcclxuICAgICAgICAgICAgICAgIHJlbmRlcmVyLmFkZENsYXNzKGJvZHlDb250YWluZXIsIGRyb3Bkb3duQ2xhc3MpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==