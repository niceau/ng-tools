/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
var NgtDropdownService = /** @class */ (function () {
    function NgtDropdownService() {
        this._isOpen = new BehaviorSubject(false);
        this.onToggleChange = new Subject();
    }
    /**
     * @return {?}
     */
    NgtDropdownService.prototype.isOpen = /**
     * @return {?}
     */
    function () {
        return this._isOpen.value;
    };
    NgtDropdownService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    NgtDropdownService.ctorParameters = function () { return []; };
    return NgtDropdownService;
}());
export { NgtDropdownService };
if (false) {
    /** @type {?} */
    NgtDropdownService.prototype._isOpen;
    /** @type {?} */
    NgtDropdownService.prototype.onToggleChange;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiZHJvcGRvd24vZHJvcGRvd24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUVoRDtJQUtJO1FBSEEsWUFBTyxHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQzlDLG1CQUFjLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztJQUcvQixDQUFDOzs7O0lBRUQsbUNBQU07OztJQUFOO1FBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQztJQUM5QixDQUFDOztnQkFWSixVQUFVOzs7O0lBV1gseUJBQUM7Q0FBQSxBQVhELElBV0M7U0FWWSxrQkFBa0I7OztJQUMzQixxQ0FBOEM7O0lBQzlDLDRDQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBOZ3REcm9wZG93blNlcnZpY2Uge1xyXG4gICAgX2lzT3BlbiA9IG5ldyBCZWhhdmlvclN1YmplY3Q8Ym9vbGVhbj4oZmFsc2UpO1xyXG4gICAgb25Ub2dnbGVDaGFuZ2UgPSBuZXcgU3ViamVjdCgpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgfVxyXG5cclxuICAgIGlzT3BlbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faXNPcGVuLnZhbHVlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==