/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, HostBinding, Input } from '@angular/core';
/**
 * A directive you should put put on a dropdown item to enable keyboard navigation.
 * Keyboard navigation using arrow keys will move focus between items marked with this directive.
 */
var NgtDropdownItemDirective = /** @class */ (function () {
    function NgtDropdownItemDirective(elementRef) {
        this.elementRef = elementRef;
        this._disabled = false;
        this.class = true;
        this.classDisabled = this.disabled;
    }
    Object.defineProperty(NgtDropdownItemDirective.prototype, "disabled", {
        get: /**
         * @return {?}
         */
        function () {
            return this._disabled;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._disabled = (/** @type {?} */ (value)) === '' || value === true; // accept an empty attribute as true
            this.classDisabled = this._disabled;
        },
        enumerable: true,
        configurable: true
    });
    NgtDropdownItemDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtDropdownItem]'
                },] }
    ];
    /** @nocollapse */
    NgtDropdownItemDirective.ctorParameters = function () { return [
        { type: ElementRef }
    ]; };
    NgtDropdownItemDirective.propDecorators = {
        class: [{ type: HostBinding, args: ['class.dropdown-item',] }],
        classDisabled: [{ type: HostBinding, args: ['class.disabled',] }],
        disabled: [{ type: Input }]
    };
    return NgtDropdownItemDirective;
}());
export { NgtDropdownItemDirective };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtDropdownItemDirective.prototype._disabled;
    /** @type {?} */
    NgtDropdownItemDirective.prototype.class;
    /** @type {?} */
    NgtDropdownItemDirective.prototype.classDisabled;
    /** @type {?} */
    NgtDropdownItemDirective.prototype.elementRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24taXRlbS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImRyb3Bkb3duL2Ryb3Bkb3duLWl0ZW0uZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7OztBQU0xRTtJQW1CSSxrQ0FBbUIsVUFBbUM7UUFBbkMsZUFBVSxHQUFWLFVBQVUsQ0FBeUI7UUFmOUMsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUVVLFVBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsa0JBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBYTdELENBQUM7SUFYRCxzQkFDSSw4Q0FBUTs7OztRQUtaO1lBQ0ksT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzFCLENBQUM7Ozs7O1FBUkQsVUFDYSxLQUFjO1lBQ3ZCLElBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQUssS0FBSyxFQUFBLEtBQUssRUFBRSxJQUFJLEtBQUssS0FBSyxJQUFJLENBQUMsQ0FBRSxvQ0FBb0M7WUFDM0YsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQ3hDLENBQUM7OztPQUFBOztnQkFiSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLG1CQUFtQjtpQkFDaEM7Ozs7Z0JBUm1CLFVBQVU7Ozt3QkFZekIsV0FBVyxTQUFDLHFCQUFxQjtnQ0FDakMsV0FBVyxTQUFDLGdCQUFnQjsyQkFFNUIsS0FBSzs7SUFZViwrQkFBQztDQUFBLEFBckJELElBcUJDO1NBbEJZLHdCQUF3Qjs7Ozs7O0lBQ2pDLDZDQUEwQjs7SUFFMUIseUNBQWlEOztJQUNqRCxpREFBNkQ7O0lBWWpELDhDQUEwQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSG9zdEJpbmRpbmcsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vKipcclxuICogQSBkaXJlY3RpdmUgeW91IHNob3VsZCBwdXQgcHV0IG9uIGEgZHJvcGRvd24gaXRlbSB0byBlbmFibGUga2V5Ym9hcmQgbmF2aWdhdGlvbi5cclxuICogS2V5Ym9hcmQgbmF2aWdhdGlvbiB1c2luZyBhcnJvdyBrZXlzIHdpbGwgbW92ZSBmb2N1cyBiZXR3ZWVuIGl0ZW1zIG1hcmtlZCB3aXRoIHRoaXMgZGlyZWN0aXZlLlxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3REcm9wZG93bkl0ZW1dJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0RHJvcGRvd25JdGVtRGlyZWN0aXZlIHtcclxuICAgIHByaXZhdGUgX2Rpc2FibGVkID0gZmFsc2U7XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5kcm9wZG93bi1pdGVtJykgY2xhc3MgPSB0cnVlO1xyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5kaXNhYmxlZCcpIGNsYXNzRGlzYWJsZWQgPSB0aGlzLmRpc2FibGVkO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzZXQgZGlzYWJsZWQodmFsdWU6IGJvb2xlYW4pIHtcclxuICAgICAgICB0aGlzLl9kaXNhYmxlZCA9IDxhbnk+dmFsdWUgPT09ICcnIHx8IHZhbHVlID09PSB0cnVlOyAgLy8gYWNjZXB0IGFuIGVtcHR5IGF0dHJpYnV0ZSBhcyB0cnVlXHJcbiAgICAgICAgdGhpcy5jbGFzc0Rpc2FibGVkID0gdGhpcy5fZGlzYWJsZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRpc2FibGVkKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9kaXNhYmxlZDtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZWxlbWVudFJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4pIHtcclxuICAgIH1cclxufVxyXG4iXX0=