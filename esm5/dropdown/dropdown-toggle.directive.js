/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Directive, ElementRef, forwardRef, HostBinding, HostListener } from '@angular/core';
import { NgtDropdownAnchorDirective } from './dropdown-anchor.directive';
import { NgtDropdownService } from './dropdown.service';
/**
 * Allows the dropdown to be toggled via click. This directive is optional: you can use NgtDropdownAnchorDirective as an
 * alternative.
 */
var NgtDropdownToggleDirective = /** @class */ (function (_super) {
    tslib_1.__extends(NgtDropdownToggleDirective, _super);
    function NgtDropdownToggleDirective(elementRef, $dropdownService) {
        var _this = _super.call(this, elementRef, $dropdownService) || this;
        _this.class = true;
        _this.ariaHaspopup = true;
        _this.ariaExpanded = _this.$dropdownService.isOpen();
        return _this;
    }
    /**
     * @return {?}
     */
    NgtDropdownToggleDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this.toggleOpen();
    };
    /**
     * @return {?}
     */
    NgtDropdownToggleDirective.prototype.toggleOpen = /**
     * @return {?}
     */
    function () {
        this.$dropdownService.onToggleChange.next();
    };
    NgtDropdownToggleDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtDropdownToggle]',
                    providers: [{ provide: NgtDropdownAnchorDirective, useExisting: forwardRef((/**
                             * @return {?}
                             */
                            function () { return NgtDropdownToggleDirective; })) }]
                },] }
    ];
    /** @nocollapse */
    NgtDropdownToggleDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: NgtDropdownService }
    ]; };
    NgtDropdownToggleDirective.propDecorators = {
        class: [{ type: HostBinding, args: ['class.dropdown-toggle',] }],
        ariaHaspopup: [{ type: HostBinding, args: ['attr.aria-haspopup.true',] }],
        ariaExpanded: [{ type: HostBinding, args: ['attr.aria-expanded',] }],
        onClick: [{ type: HostListener, args: ['click',] }]
    };
    return NgtDropdownToggleDirective;
}(NgtDropdownAnchorDirective));
export { NgtDropdownToggleDirective };
if (false) {
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.class;
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.ariaHaspopup;
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.ariaExpanded;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tdG9nZ2xlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiZHJvcGRvd24vZHJvcGRvd24tdG9nZ2xlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzdGLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9CQUFvQixDQUFDOzs7OztBQU14RDtJQUlnRCxzREFBMEI7SUFVdEUsb0NBQVksVUFBbUMsRUFBRSxnQkFBb0M7UUFBckYsWUFDSSxrQkFBTSxVQUFVLEVBQUUsZ0JBQWdCLENBQUMsU0FDdEM7UUFWcUMsV0FBSyxHQUFHLElBQUksQ0FBQztRQUNYLGtCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLGtCQUFZLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxDQUFDOztJQVFqRixDQUFDOzs7O0lBTnNCLDRDQUFPOzs7SUFBOUI7UUFDSSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7OztJQU1ELCtDQUFVOzs7SUFBVjtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDaEQsQ0FBQzs7Z0JBcEJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQixTQUFTLEVBQUUsQ0FBQyxFQUFDLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxXQUFXLEVBQUUsVUFBVTs7OzRCQUFDLGNBQU0sT0FBQSwwQkFBMEIsRUFBMUIsQ0FBMEIsRUFBQyxFQUFDLENBQUM7aUJBQ2hIOzs7O2dCQVhtQixVQUFVO2dCQUVyQixrQkFBa0I7Ozt3QkFZdEIsV0FBVyxTQUFDLHVCQUF1QjsrQkFDbkMsV0FBVyxTQUFDLHlCQUF5QjsrQkFDckMsV0FBVyxTQUFDLG9CQUFvQjswQkFFaEMsWUFBWSxTQUFDLE9BQU87O0lBV3pCLGlDQUFDO0NBQUEsQUFyQkQsQ0FJZ0QsMEJBQTBCLEdBaUJ6RTtTQWpCWSwwQkFBMEI7OztJQUVuQywyQ0FBbUQ7O0lBQ25ELGtEQUE0RDs7SUFDNUQsa0RBQWlGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBFbGVtZW50UmVmLCBmb3J3YXJkUmVmLCBIb3N0QmluZGluZywgSG9zdExpc3RlbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5ndERyb3Bkb3duQW5jaG9yRGlyZWN0aXZlIH0gZnJvbSAnLi9kcm9wZG93bi1hbmNob3IuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgTmd0RHJvcGRvd25TZXJ2aWNlIH0gZnJvbSAnLi9kcm9wZG93bi5zZXJ2aWNlJztcclxuXHJcbi8qKlxyXG4gKiBBbGxvd3MgdGhlIGRyb3Bkb3duIHRvIGJlIHRvZ2dsZWQgdmlhIGNsaWNrLiBUaGlzIGRpcmVjdGl2ZSBpcyBvcHRpb25hbDogeW91IGNhbiB1c2UgTmd0RHJvcGRvd25BbmNob3JEaXJlY3RpdmUgYXMgYW5cclxuICogYWx0ZXJuYXRpdmUuXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndERyb3Bkb3duVG9nZ2xlXScsXHJcbiAgICBwcm92aWRlcnM6IFt7cHJvdmlkZTogTmd0RHJvcGRvd25BbmNob3JEaXJlY3RpdmUsIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IE5ndERyb3Bkb3duVG9nZ2xlRGlyZWN0aXZlKX1dXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3REcm9wZG93blRvZ2dsZURpcmVjdGl2ZSBleHRlbmRzIE5ndERyb3Bkb3duQW5jaG9yRGlyZWN0aXZlIHtcclxuXHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLmRyb3Bkb3duLXRvZ2dsZScpIGNsYXNzID0gdHJ1ZTtcclxuICAgIEBIb3N0QmluZGluZygnYXR0ci5hcmlhLWhhc3BvcHVwLnRydWUnKSBhcmlhSGFzcG9wdXAgPSB0cnVlO1xyXG4gICAgQEhvc3RCaW5kaW5nKCdhdHRyLmFyaWEtZXhwYW5kZWQnKSBhcmlhRXhwYW5kZWQgPSB0aGlzLiRkcm9wZG93blNlcnZpY2UuaXNPcGVuKCk7XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snKSBvbkNsaWNrKCkge1xyXG4gICAgICAgIHRoaXMudG9nZ2xlT3BlbigpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKGVsZW1lbnRSZWY6IEVsZW1lbnRSZWY8SFRNTEVsZW1lbnQ+LCAkZHJvcGRvd25TZXJ2aWNlOiBOZ3REcm9wZG93blNlcnZpY2UpIHtcclxuICAgICAgICBzdXBlcihlbGVtZW50UmVmLCAkZHJvcGRvd25TZXJ2aWNlKTtcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVPcGVuKCkge1xyXG4gICAgICAgIHRoaXMuJGRyb3Bkb3duU2VydmljZS5vblRvZ2dsZUNoYW5nZS5uZXh0KCk7XHJcbiAgICB9XHJcbn1cclxuIl19