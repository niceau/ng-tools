/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, HostBinding } from '@angular/core';
import { NgtDropdownService } from './dropdown.service';
/**
 * Marks an element to which dropdown menu will be anchored. This is a simple version
 * of the NgtDropdownToggleDirective directive. It plays the same role as NgtDropdownToggleDirective but
 * doesn't listen to click events to toggle dropdown menu thus enabling support for
 * events other than click.
 *
 * \@since 1.1.0
 */
var NgtDropdownAnchorDirective = /** @class */ (function () {
    function NgtDropdownAnchorDirective(_elementRef, $dropdownService) {
        this._elementRef = _elementRef;
        this.$dropdownService = $dropdownService;
        this.class = true;
        this.ariaHaspopup = true;
        this.ariaExpanded = this.$dropdownService.isOpen();
        this.anchorEl = _elementRef.nativeElement;
    }
    /**
     * @return {?}
     */
    NgtDropdownAnchorDirective.prototype.getNativeElement = /**
     * @return {?}
     */
    function () {
        return this._elementRef.nativeElement;
    };
    NgtDropdownAnchorDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtDropdownAnchor]'
                },] }
    ];
    /** @nocollapse */
    NgtDropdownAnchorDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: NgtDropdownService }
    ]; };
    NgtDropdownAnchorDirective.propDecorators = {
        class: [{ type: HostBinding, args: ['class.dropdown-toggle',] }],
        ariaHaspopup: [{ type: HostBinding, args: ['attr.aria-haspopup.true',] }],
        ariaExpanded: [{ type: HostBinding, args: ['attr.aria-expanded',] }]
    };
    return NgtDropdownAnchorDirective;
}());
export { NgtDropdownAnchorDirective };
if (false) {
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.anchorEl;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.class;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.ariaHaspopup;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.ariaExpanded;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownAnchorDirective.prototype._elementRef;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.$dropdownService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tYW5jaG9yLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiZHJvcGRvd24vZHJvcGRvd24tYW5jaG9yLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9CQUFvQixDQUFDOzs7Ozs7Ozs7QUFVeEQ7SUFVSSxvQ0FBb0IsV0FBb0MsRUFDckMsZ0JBQW9DO1FBRG5DLGdCQUFXLEdBQVgsV0FBVyxDQUF5QjtRQUNyQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQW9CO1FBTGpCLFVBQUssR0FBRyxJQUFJLENBQUM7UUFDWCxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixpQkFBWSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUk3RSxJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxhQUFhLENBQUM7SUFDOUMsQ0FBQzs7OztJQUVELHFEQUFnQjs7O0lBQWhCO1FBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQztJQUMxQyxDQUFDOztnQkFqQkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxxQkFBcUI7aUJBQ2xDOzs7O2dCQWJtQixVQUFVO2dCQUNyQixrQkFBa0I7Ozt3QkFnQnRCLFdBQVcsU0FBQyx1QkFBdUI7K0JBQ25DLFdBQVcsU0FBQyx5QkFBeUI7K0JBQ3JDLFdBQVcsU0FBQyxvQkFBb0I7O0lBVXJDLGlDQUFDO0NBQUEsQUFsQkQsSUFrQkM7U0FmWSwwQkFBMEI7OztJQUNuQyw4Q0FBUzs7SUFFVCwyQ0FBbUQ7O0lBQ25ELGtEQUE0RDs7SUFDNUQsa0RBQWlGOzs7OztJQUVyRSxpREFBNEM7O0lBQzVDLHNEQUEyQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSG9zdEJpbmRpbmcgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmd0RHJvcGRvd25TZXJ2aWNlIH0gZnJvbSAnLi9kcm9wZG93bi5zZXJ2aWNlJztcclxuXHJcbi8qKlxyXG4gKiBNYXJrcyBhbiBlbGVtZW50IHRvIHdoaWNoIGRyb3Bkb3duIG1lbnUgd2lsbCBiZSBhbmNob3JlZC4gVGhpcyBpcyBhIHNpbXBsZSB2ZXJzaW9uXHJcbiAqIG9mIHRoZSBOZ3REcm9wZG93blRvZ2dsZURpcmVjdGl2ZSBkaXJlY3RpdmUuIEl0IHBsYXlzIHRoZSBzYW1lIHJvbGUgYXMgTmd0RHJvcGRvd25Ub2dnbGVEaXJlY3RpdmUgYnV0XHJcbiAqIGRvZXNuJ3QgbGlzdGVuIHRvIGNsaWNrIGV2ZW50cyB0byB0b2dnbGUgZHJvcGRvd24gbWVudSB0aHVzIGVuYWJsaW5nIHN1cHBvcnQgZm9yXHJcbiAqIGV2ZW50cyBvdGhlciB0aGFuIGNsaWNrLlxyXG4gKlxyXG4gKiBAc2luY2UgMS4xLjBcclxuICovXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbbmd0RHJvcGRvd25BbmNob3JdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0RHJvcGRvd25BbmNob3JEaXJlY3RpdmUge1xyXG4gICAgYW5jaG9yRWw7XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5kcm9wZG93bi10b2dnbGUnKSBjbGFzcyA9IHRydWU7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2F0dHIuYXJpYS1oYXNwb3B1cC50cnVlJykgYXJpYUhhc3BvcHVwID0gdHJ1ZTtcclxuICAgIEBIb3N0QmluZGluZygnYXR0ci5hcmlhLWV4cGFuZGVkJykgYXJpYUV4cGFuZGVkID0gdGhpcy4kZHJvcGRvd25TZXJ2aWNlLmlzT3BlbigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2VsZW1lbnRSZWY6IEVsZW1lbnRSZWY8SFRNTEVsZW1lbnQ+LFxyXG4gICAgICAgICAgICAgICAgcHVibGljICRkcm9wZG93blNlcnZpY2U6IE5ndERyb3Bkb3duU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMuYW5jaG9yRWwgPSBfZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIGdldE5hdGl2ZUVsZW1lbnQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudDtcclxuICAgIH1cclxufVxyXG4iXX0=