/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ContentChildren, Directive, ElementRef, HostBinding, QueryList, Renderer2 } from '@angular/core';
import { NgtDropdownItemDirective } from './dropdown-item.directive';
import { positionElements } from '../util/positioning';
import { NgtDropdownService } from './dropdown.service';
/**
 *
 */
var NgtDropdownMenuDirective = /** @class */ (function () {
    function NgtDropdownMenuDirective(_elementRef, $dropdownService, _renderer) {
        var _this = this;
        this._elementRef = _elementRef;
        this.$dropdownService = $dropdownService;
        this._renderer = _renderer;
        this.placement = 'bottom';
        this.isOpen = false;
        this.dropdownMenu = true;
        this.show = false;
        this.xPlacement = this.placement;
        this.isOpenSubscription = this.$dropdownService._isOpen.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            _this.show = status;
        }));
    }
    /**
     * @return {?}
     */
    NgtDropdownMenuDirective.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.isOpenSubscription.unsubscribe();
    };
    /**
     * @return {?}
     */
    NgtDropdownMenuDirective.prototype.getNativeElement = /**
     * @return {?}
     */
    function () {
        return this._elementRef.nativeElement;
    };
    /**
     * @param {?} triggerEl
     * @param {?} placement
     * @return {?}
     */
    NgtDropdownMenuDirective.prototype.position = /**
     * @param {?} triggerEl
     * @param {?} placement
     * @return {?}
     */
    function (triggerEl, placement) {
        this.applyPlacement(positionElements(triggerEl, this._elementRef.nativeElement, placement));
    };
    /**
     * @param {?} _placement
     * @return {?}
     */
    NgtDropdownMenuDirective.prototype.applyPlacement = /**
     * @param {?} _placement
     * @return {?}
     */
    function (_placement) {
        // remove the current placement classes
        this._renderer.removeClass(this._elementRef.nativeElement.parentNode, 'dropup');
        this._renderer.removeClass(this._elementRef.nativeElement.parentNode, 'dropdown');
        this.placement = _placement;
        /**
         * apply the new placement
         * in case of top use up-arrow or down-arrow otherwise
         */
        if (_placement.search('^top') !== -1) {
            this._renderer.addClass(this._elementRef.nativeElement.parentNode, 'dropup');
        }
        else {
            this._renderer.addClass(this._elementRef.nativeElement.parentNode, 'dropdown');
        }
    };
    NgtDropdownMenuDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtDropdownMenu]'
                },] }
    ];
    /** @nocollapse */
    NgtDropdownMenuDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: NgtDropdownService },
        { type: Renderer2 }
    ]; };
    NgtDropdownMenuDirective.propDecorators = {
        menuItems: [{ type: ContentChildren, args: [NgtDropdownItemDirective,] }],
        dropdownMenu: [{ type: HostBinding, args: ['class.dropdown-menu',] }],
        show: [{ type: HostBinding, args: ['class.show',] }],
        xPlacement: [{ type: HostBinding, args: ['attr.x-placement',] }]
    };
    return NgtDropdownMenuDirective;
}());
export { NgtDropdownMenuDirective };
if (false) {
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.placement;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.isOpen;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.isOpenSubscription;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.menuItems;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.dropdownMenu;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.show;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.xPlacement;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownMenuDirective.prototype._elementRef;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.$dropdownService;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownMenuDirective.prototype._renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tbWVudS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImRyb3Bkb3duL2Ryb3Bkb3duLW1lbnUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsZUFBZSxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFhLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckgsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDckUsT0FBTyxFQUFhLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFbEUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7Ozs7QUFNeEQ7SUFjSSxrQ0FBb0IsV0FBb0MsRUFDckMsZ0JBQW9DLEVBQ25DLFNBQW9CO1FBRnhDLGlCQU1DO1FBTm1CLGdCQUFXLEdBQVgsV0FBVyxDQUF5QjtRQUNyQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQW9CO1FBQ25DLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFaeEMsY0FBUyxHQUFjLFFBQVEsQ0FBQztRQUNoQyxXQUFNLEdBQUcsS0FBSyxDQUFDO1FBS3FCLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQzdCLFNBQUksR0FBRyxLQUFLLENBQUM7UUFDUCxlQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUt6RCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxNQUFNO1lBQ3BFLEtBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELDhDQUFXOzs7SUFBWDtRQUNJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUMxQyxDQUFDOzs7O0lBRUQsbURBQWdCOzs7SUFBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDO0lBQzFDLENBQUM7Ozs7OztJQUVELDJDQUFROzs7OztJQUFSLFVBQVMsU0FBUyxFQUFFLFNBQVM7UUFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUNoRyxDQUFDOzs7OztJQUVELGlEQUFjOzs7O0lBQWQsVUFBZSxVQUFxQjtRQUNoQyx1Q0FBdUM7UUFDdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ2hGLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUNsRixJQUFJLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQztRQUM1Qjs7O1dBR0c7UUFDSCxJQUFJLFVBQVUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ2hGO2FBQU07WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7U0FDbEY7SUFDTCxDQUFDOztnQkFoREosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxtQkFBbUI7aUJBQ2hDOzs7O2dCQVpvQyxVQUFVO2dCQUl0QyxrQkFBa0I7Z0JBSnlELFNBQVM7Ozs0QkFrQnhGLGVBQWUsU0FBQyx3QkFBd0I7K0JBRXhDLFdBQVcsU0FBQyxxQkFBcUI7dUJBQ2pDLFdBQVcsU0FBQyxZQUFZOzZCQUN4QixXQUFXLFNBQUMsa0JBQWtCOztJQXFDbkMsK0JBQUM7Q0FBQSxBQWpERCxJQWlEQztTQTlDWSx3QkFBd0I7OztJQUNqQyw2Q0FBZ0M7O0lBQ2hDLDBDQUFlOztJQUNmLHNEQUFpQzs7SUFFakMsNkNBQTBGOztJQUUxRixnREFBd0Q7O0lBQ3hELHdDQUF3Qzs7SUFDeEMsOENBQTZEOzs7OztJQUVqRCwrQ0FBNEM7O0lBQzVDLG9EQUEyQzs7Ozs7SUFDM0MsNkNBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29udGVudENoaWxkcmVuLCBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIEhvc3RCaW5kaW5nLCBPbkRlc3Ryb3ksIFF1ZXJ5TGlzdCwgUmVuZGVyZXIyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5ndERyb3Bkb3duSXRlbURpcmVjdGl2ZSB9IGZyb20gJy4vZHJvcGRvd24taXRlbS5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBQbGFjZW1lbnQsIHBvc2l0aW9uRWxlbWVudHMgfSBmcm9tICcuLi91dGlsL3Bvc2l0aW9uaW5nJztcclxuXHJcbmltcG9ydCB7IE5ndERyb3Bkb3duU2VydmljZSB9IGZyb20gJy4vZHJvcGRvd24uc2VydmljZSc7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5cclxuXHJcbi8qKlxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3REcm9wZG93bk1lbnVdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0RHJvcGRvd25NZW51RGlyZWN0aXZlIGltcGxlbWVudHMgT25EZXN0cm95IHtcclxuICAgIHBsYWNlbWVudDogUGxhY2VtZW50ID0gJ2JvdHRvbSc7XHJcbiAgICBpc09wZW4gPSBmYWxzZTtcclxuICAgIGlzT3BlblN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICAgIEBDb250ZW50Q2hpbGRyZW4oTmd0RHJvcGRvd25JdGVtRGlyZWN0aXZlKSBtZW51SXRlbXM6IFF1ZXJ5TGlzdDxOZ3REcm9wZG93bkl0ZW1EaXJlY3RpdmU+O1xyXG5cclxuICAgIEBIb3N0QmluZGluZygnY2xhc3MuZHJvcGRvd24tbWVudScpIGRyb3Bkb3duTWVudSA9IHRydWU7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLnNob3cnKSBzaG93ID0gZmFsc2U7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2F0dHIueC1wbGFjZW1lbnQnKSB4UGxhY2VtZW50ID0gdGhpcy5wbGFjZW1lbnQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfZWxlbWVudFJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4sXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgJGRyb3Bkb3duU2VydmljZTogTmd0RHJvcGRvd25TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfcmVuZGVyZXI6IFJlbmRlcmVyMikge1xyXG4gICAgICAgIHRoaXMuaXNPcGVuU3Vic2NyaXB0aW9uID0gdGhpcy4kZHJvcGRvd25TZXJ2aWNlLl9pc09wZW4uc3Vic2NyaWJlKHN0YXR1cyA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvdyA9IHN0YXR1cztcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmlzT3BlblN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE5hdGl2ZUVsZW1lbnQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudDtcclxuICAgIH1cclxuXHJcbiAgICBwb3NpdGlvbih0cmlnZ2VyRWwsIHBsYWNlbWVudCkge1xyXG4gICAgICAgIHRoaXMuYXBwbHlQbGFjZW1lbnQocG9zaXRpb25FbGVtZW50cyh0cmlnZ2VyRWwsIHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgcGxhY2VtZW50KSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXBwbHlQbGFjZW1lbnQoX3BsYWNlbWVudDogUGxhY2VtZW50KSB7XHJcbiAgICAgICAgLy8gcmVtb3ZlIHRoZSBjdXJyZW50IHBsYWNlbWVudCBjbGFzc2VzXHJcbiAgICAgICAgdGhpcy5fcmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnBhcmVudE5vZGUsICdkcm9wdXAnKTtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQucGFyZW50Tm9kZSwgJ2Ryb3Bkb3duJyk7XHJcbiAgICAgICAgdGhpcy5wbGFjZW1lbnQgPSBfcGxhY2VtZW50O1xyXG4gICAgICAgIC8qKlxyXG4gICAgICAgICAqIGFwcGx5IHRoZSBuZXcgcGxhY2VtZW50XHJcbiAgICAgICAgICogaW4gY2FzZSBvZiB0b3AgdXNlIHVwLWFycm93IG9yIGRvd24tYXJyb3cgb3RoZXJ3aXNlXHJcbiAgICAgICAgICovXHJcbiAgICAgICAgaWYgKF9wbGFjZW1lbnQuc2VhcmNoKCdedG9wJykgIT09IC0xKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5wYXJlbnROb2RlLCAnZHJvcHVwJyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnBhcmVudE5vZGUsICdkcm9wZG93bicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=