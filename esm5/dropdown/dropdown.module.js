/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { NgtDropdownDirective } from './dropdown';
import { NgtDropdownAnchorDirective } from './dropdown';
import { NgtDropdownToggleDirective } from './dropdown';
import { NgtDropdownMenuDirective } from './dropdown';
import { NgtDropdownItemDirective } from './dropdown';
export { NgtDropdownDirective } from './dropdown';
export { NgtDropdownAnchorDirective } from './dropdown';
export { NgtDropdownToggleDirective } from './dropdown';
export { NgtDropdownMenuDirective } from './dropdown';
export { NgtDropdownItemDirective } from './dropdown';
export { NgtDropdownConfig } from './dropdown-config';
/** @type {?} */
var NGC_DROPDOWN_DIRECTIVES = [
    NgtDropdownDirective, NgtDropdownAnchorDirective, NgtDropdownToggleDirective, NgtDropdownMenuDirective, NgtDropdownItemDirective
];
var NgtDropdownModule = /** @class */ (function () {
    function NgtDropdownModule() {
    }
    /**
     * @return {?}
     */
    NgtDropdownModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtDropdownModule };
    };
    NgtDropdownModule.decorators = [
        { type: NgModule, args: [{ declarations: NGC_DROPDOWN_DIRECTIVES, exports: NGC_DROPDOWN_DIRECTIVES },] }
    ];
    return NgtDropdownModule;
}());
export { NgtDropdownModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJkcm9wZG93bi9kcm9wZG93bi5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTlELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUNsRCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFDeEQsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sWUFBWSxDQUFDO0FBQ3hELE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUN0RCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFFdEQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sWUFBWSxDQUFDO0FBQ2xELE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUN4RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFDeEQsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sWUFBWSxDQUFDO0FBQ3RELE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLFlBQVksQ0FBQztBQUN0RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7SUFFaEQsdUJBQXVCLEdBQUc7SUFDNUIsb0JBQW9CLEVBQUUsMEJBQTBCLEVBQUUsMEJBQTBCLEVBQUUsd0JBQXdCLEVBQUUsd0JBQXdCO0NBQ25JO0FBRUQ7SUFBQTtJQUtBLENBQUM7Ozs7SUFIVSx5QkFBTzs7O0lBQWQ7UUFDSSxPQUFPLEVBQUMsUUFBUSxFQUFFLGlCQUFpQixFQUFDLENBQUM7SUFDekMsQ0FBQzs7Z0JBSkosUUFBUSxTQUFDLEVBQUMsWUFBWSxFQUFFLHVCQUF1QixFQUFFLE9BQU8sRUFBRSx1QkFBdUIsRUFBQzs7SUFLbkYsd0JBQUM7Q0FBQSxBQUxELElBS0M7U0FKWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTmd0RHJvcGRvd25EaXJlY3RpdmUgfSBmcm9tICcuL2Ryb3Bkb3duJztcclxuaW1wb3J0IHsgTmd0RHJvcGRvd25BbmNob3JEaXJlY3RpdmUgfSBmcm9tICcuL2Ryb3Bkb3duJztcclxuaW1wb3J0IHsgTmd0RHJvcGRvd25Ub2dnbGVEaXJlY3RpdmUgfSBmcm9tICcuL2Ryb3Bkb3duJztcclxuaW1wb3J0IHsgTmd0RHJvcGRvd25NZW51RGlyZWN0aXZlIH0gZnJvbSAnLi9kcm9wZG93bic7XHJcbmltcG9ydCB7IE5ndERyb3Bkb3duSXRlbURpcmVjdGl2ZSB9IGZyb20gJy4vZHJvcGRvd24nO1xyXG5cclxuZXhwb3J0IHsgTmd0RHJvcGRvd25EaXJlY3RpdmUgfSBmcm9tICcuL2Ryb3Bkb3duJztcclxuZXhwb3J0IHsgTmd0RHJvcGRvd25BbmNob3JEaXJlY3RpdmUgfSBmcm9tICcuL2Ryb3Bkb3duJztcclxuZXhwb3J0IHsgTmd0RHJvcGRvd25Ub2dnbGVEaXJlY3RpdmUgfSBmcm9tICcuL2Ryb3Bkb3duJztcclxuZXhwb3J0IHsgTmd0RHJvcGRvd25NZW51RGlyZWN0aXZlIH0gZnJvbSAnLi9kcm9wZG93bic7XHJcbmV4cG9ydCB7IE5ndERyb3Bkb3duSXRlbURpcmVjdGl2ZSB9IGZyb20gJy4vZHJvcGRvd24nO1xyXG5leHBvcnQgeyBOZ3REcm9wZG93bkNvbmZpZyB9IGZyb20gJy4vZHJvcGRvd24tY29uZmlnJztcclxuXHJcbmNvbnN0IE5HQ19EUk9QRE9XTl9ESVJFQ1RJVkVTID0gW1xyXG4gICAgTmd0RHJvcGRvd25EaXJlY3RpdmUsIE5ndERyb3Bkb3duQW5jaG9yRGlyZWN0aXZlLCBOZ3REcm9wZG93blRvZ2dsZURpcmVjdGl2ZSwgTmd0RHJvcGRvd25NZW51RGlyZWN0aXZlLCBOZ3REcm9wZG93bkl0ZW1EaXJlY3RpdmVcclxuXTtcclxuXHJcbkBOZ01vZHVsZSh7ZGVjbGFyYXRpb25zOiBOR0NfRFJPUERPV05fRElSRUNUSVZFUywgZXhwb3J0czogTkdDX0RST1BET1dOX0RJUkVDVElWRVN9KVxyXG5leHBvcnQgY2xhc3MgTmd0RHJvcGRvd25Nb2R1bGUge1xyXG4gICAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICAgICAgcmV0dXJuIHtuZ01vZHVsZTogTmd0RHJvcGRvd25Nb2R1bGV9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==