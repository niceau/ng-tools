/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtProgressbar } from './progressbar';
export { NgtProgressbar } from './progressbar';
export { NgtProgressbarConfig } from './progressbar-config';
var NgtProgressbarModule = /** @class */ (function () {
    function NgtProgressbarModule() {
    }
    /**
     * @return {?}
     */
    NgtProgressbarModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtProgressbarModule };
    };
    NgtProgressbarModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [NgtProgressbar],
                    exports: [NgtProgressbar],
                    imports: [CommonModule]
                },] }
    ];
    return NgtProgressbarModule;
}());
export { NgtProgressbarModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZ3Jlc3NiYXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJwcm9ncmVzc2Jhci9wcm9ncmVzc2Jhci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9DLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0MsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFNUQ7SUFBQTtJQVNBLENBQUM7Ozs7SUFIVSw0QkFBTzs7O0lBQWQ7UUFDSSxPQUFPLEVBQUMsUUFBUSxFQUFFLG9CQUFvQixFQUFDLENBQUM7SUFDNUMsQ0FBQzs7Z0JBUkosUUFBUSxTQUFDO29CQUNOLFlBQVksRUFBRSxDQUFDLGNBQWMsQ0FBQztvQkFDOUIsT0FBTyxFQUFFLENBQUMsY0FBYyxDQUFDO29CQUN6QixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7aUJBQzFCOztJQUtELDJCQUFDO0NBQUEsQUFURCxJQVNDO1NBSlksb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbmltcG9ydCB7IE5ndFByb2dyZXNzYmFyIH0gZnJvbSAnLi9wcm9ncmVzc2Jhcic7XHJcblxyXG5leHBvcnQgeyBOZ3RQcm9ncmVzc2JhciB9IGZyb20gJy4vcHJvZ3Jlc3NiYXInO1xyXG5leHBvcnQgeyBOZ3RQcm9ncmVzc2JhckNvbmZpZyB9IGZyb20gJy4vcHJvZ3Jlc3NiYXItY29uZmlnJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtOZ3RQcm9ncmVzc2Jhcl0sXHJcbiAgICBleHBvcnRzOiBbTmd0UHJvZ3Jlc3NiYXJdLFxyXG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndFByb2dyZXNzYmFyTW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgICAgIHJldHVybiB7bmdNb2R1bGU6IE5ndFByb2dyZXNzYmFyTW9kdWxlfTtcclxuICAgIH1cclxufVxyXG4iXX0=