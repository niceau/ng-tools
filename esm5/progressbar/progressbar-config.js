/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtProgressbar component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the progress bars used in the application.
 */
var NgtProgressbarConfig = /** @class */ (function () {
    function NgtProgressbarConfig() {
        this.max = 100;
        this.animated = false;
        this.striped = false;
        this.showValue = false;
    }
    NgtProgressbarConfig.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */ NgtProgressbarConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtProgressbarConfig_Factory() { return new NgtProgressbarConfig(); }, token: NgtProgressbarConfig, providedIn: "root" });
    return NgtProgressbarConfig;
}());
export { NgtProgressbarConfig };
if (false) {
    /** @type {?} */
    NgtProgressbarConfig.prototype.max;
    /** @type {?} */
    NgtProgressbarConfig.prototype.animated;
    /** @type {?} */
    NgtProgressbarConfig.prototype.striped;
    /** @type {?} */
    NgtProgressbarConfig.prototype.type;
    /** @type {?} */
    NgtProgressbarConfig.prototype.showValue;
    /** @type {?} */
    NgtProgressbarConfig.prototype.height;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZ3Jlc3NiYXItY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJwcm9ncmVzc2Jhci9wcm9ncmVzc2Jhci1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFPM0M7SUFBQTtRQUVJLFFBQUcsR0FBRyxHQUFHLENBQUM7UUFDVixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFFaEIsY0FBUyxHQUFHLEtBQUssQ0FBQztLQUVyQjs7Z0JBUkEsVUFBVSxTQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQzs7OytCQVBoQztDQWVDLEFBUkQsSUFRQztTQVBZLG9CQUFvQjs7O0lBQzdCLG1DQUFVOztJQUNWLHdDQUFpQjs7SUFDakIsdUNBQWdCOztJQUNoQixvQ0FBYTs7SUFDYix5Q0FBa0I7O0lBQ2xCLHNDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuLyoqXHJcbiAqIENvbmZpZ3VyYXRpb24gc2VydmljZSBmb3IgdGhlIE5ndFByb2dyZXNzYmFyIGNvbXBvbmVudC5cclxuICogWW91IGNhbiBpbmplY3QgdGhpcyBzZXJ2aWNlLCB0eXBpY2FsbHkgaW4geW91ciByb290IGNvbXBvbmVudCwgYW5kIGN1c3RvbWl6ZSB0aGUgdmFsdWVzIG9mIGl0cyBwcm9wZXJ0aWVzIGluXHJcbiAqIG9yZGVyIHRvIHByb3ZpZGUgZGVmYXVsdCB2YWx1ZXMgZm9yIGFsbCB0aGUgcHJvZ3Jlc3MgYmFycyB1c2VkIGluIHRoZSBhcHBsaWNhdGlvbi5cclxuICovXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTmd0UHJvZ3Jlc3NiYXJDb25maWcge1xyXG4gICAgbWF4ID0gMTAwO1xyXG4gICAgYW5pbWF0ZWQgPSBmYWxzZTtcclxuICAgIHN0cmlwZWQgPSBmYWxzZTtcclxuICAgIHR5cGU6IHN0cmluZztcclxuICAgIHNob3dWYWx1ZSA9IGZhbHNlO1xyXG4gICAgaGVpZ2h0OiBzdHJpbmc7XHJcbn1cclxuIl19