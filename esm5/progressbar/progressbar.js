/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { getValueInRange } from '../util/util';
import { NgtProgressbarConfig } from './progressbar-config';
/**
 * Directive that can be used to provide feedback on the progress of a workflow or an action.
 */
var NgtProgressbar = /** @class */ (function () {
    function NgtProgressbar(config) {
        /**
         * Current value to be displayed in the progressbar. Should be smaller or equal to "max" value.
         */
        this.value = 0;
        this.max = config.max;
        this.animated = config.animated;
        this.striped = config.striped;
        this.type = config.type;
        this.showValue = config.showValue;
        this.height = config.height;
    }
    /**
     * @return {?}
     */
    NgtProgressbar.prototype.getValue = /**
     * @return {?}
     */
    function () {
        return getValueInRange(this.value, this.max);
    };
    /**
     * @return {?}
     */
    NgtProgressbar.prototype.getPercentValue = /**
     * @return {?}
     */
    function () {
        return 100 * this.getValue() / this.max;
    };
    NgtProgressbar.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-progressbar',
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    template: "\n        <div class=\"progress\" [style.height]=\"height\">\n            <div class=\"progress-bar{{type ? ' bg-' + type : ''}}{{animated ? ' progress-bar-animated' : ''}}{{striped ?\n    ' progress-bar-striped' : ''}}\" role=\"progressbar\" [style.width.%]=\"getPercentValue()\"\n                 [attr.aria-valuenow]=\"getValue()\" aria-valuemin=\"0\" [attr.aria-valuemax]=\"max\">\n                <span *ngIf=\"showValue\" i18n=\"@@ngt.progressbar.value\">{{getPercentValue()}}%</span>\n                <ng-content></ng-content>\n            </div>\n        </div>\n    "
                }] }
    ];
    /** @nocollapse */
    NgtProgressbar.ctorParameters = function () { return [
        { type: NgtProgressbarConfig }
    ]; };
    NgtProgressbar.propDecorators = {
        max: [{ type: Input }],
        animated: [{ type: Input }],
        striped: [{ type: Input }],
        showValue: [{ type: Input }],
        type: [{ type: Input }],
        value: [{ type: Input }],
        height: [{ type: Input }]
    };
    return NgtProgressbar;
}());
export { NgtProgressbar };
if (false) {
    /**
     * Maximal value to be displayed in the progressbar.
     * @type {?}
     */
    NgtProgressbar.prototype.max;
    /**
     * A flag indicating if the stripes of the progress bar should be animated. Takes effect only for browsers
     * supporting CSS3 animations, and if striped is true.
     * @type {?}
     */
    NgtProgressbar.prototype.animated;
    /**
     * A flag indicating if a progress bar should be displayed as striped.
     * @type {?}
     */
    NgtProgressbar.prototype.striped;
    /**
     * A flag indicating if the current percentage value should be shown.
     * @type {?}
     */
    NgtProgressbar.prototype.showValue;
    /**
     * Type of progress bar, can be one of "success", "info", "warning" or "danger".
     * @type {?}
     */
    NgtProgressbar.prototype.type;
    /**
     * Current value to be displayed in the progressbar. Should be smaller or equal to "max" value.
     * @type {?}
     */
    NgtProgressbar.prototype.value;
    /**
     * Height of the progress bar. Accepts any valid CSS height values, ex. '2rem'
     * @type {?}
     */
    NgtProgressbar.prototype.height;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZ3Jlc3NiYXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInByb2dyZXNzYmFyL3Byb2dyZXNzYmFyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMxRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQy9DLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDOzs7O0FBSzVEO0lBbURJLHdCQUFZLE1BQTRCOzs7O1FBUC9CLFVBQUssR0FBRyxDQUFDLENBQUM7UUFRZixJQUFJLENBQUMsR0FBRyxHQUFHLE1BQU0sQ0FBQyxHQUFHLENBQUM7UUFDdEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUM5QixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztJQUNoQyxDQUFDOzs7O0lBRUQsaUNBQVE7OztJQUFSO1FBQ0ksT0FBTyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDakQsQ0FBQzs7OztJQUVELHdDQUFlOzs7SUFBZjtRQUNJLE9BQU8sR0FBRyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO0lBQzVDLENBQUM7O2dCQWxFSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGlCQUFpQjtvQkFDM0IsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLFFBQVEsRUFBRSxpa0JBU1Q7aUJBQ0o7Ozs7Z0JBbEJRLG9CQUFvQjs7O3NCQXVCeEIsS0FBSzsyQkFNTCxLQUFLOzBCQUtMLEtBQUs7NEJBS0wsS0FBSzt1QkFLTCxLQUFLO3dCQUtMLEtBQUs7eUJBS0wsS0FBSzs7SUFrQlYscUJBQUM7Q0FBQSxBQW5FRCxJQW1FQztTQXJEWSxjQUFjOzs7Ozs7SUFJdkIsNkJBQXFCOzs7Ozs7SUFNckIsa0NBQTJCOzs7OztJQUszQixpQ0FBMEI7Ozs7O0lBSzFCLG1DQUE0Qjs7Ozs7SUFLNUIsOEJBQXNCOzs7OztJQUt0QiwrQkFBbUI7Ozs7O0lBS25CLGdDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIENoYW5nZURldGVjdGlvblN0cmF0ZWd5IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IGdldFZhbHVlSW5SYW5nZSB9IGZyb20gJy4uL3V0aWwvdXRpbCc7XHJcbmltcG9ydCB7IE5ndFByb2dyZXNzYmFyQ29uZmlnIH0gZnJvbSAnLi9wcm9ncmVzc2Jhci1jb25maWcnO1xyXG5cclxuLyoqXHJcbiAqIERpcmVjdGl2ZSB0aGF0IGNhbiBiZSB1c2VkIHRvIHByb3ZpZGUgZmVlZGJhY2sgb24gdGhlIHByb2dyZXNzIG9mIGEgd29ya2Zsb3cgb3IgYW4gYWN0aW9uLlxyXG4gKi9cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC1wcm9ncmVzc2JhcicsXHJcbiAgICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInByb2dyZXNzXCIgW3N0eWxlLmhlaWdodF09XCJoZWlnaHRcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInByb2dyZXNzLWJhcnt7dHlwZSA/ICcgYmctJyArIHR5cGUgOiAnJ319e3thbmltYXRlZCA/ICcgcHJvZ3Jlc3MtYmFyLWFuaW1hdGVkJyA6ICcnfX17e3N0cmlwZWQgP1xyXG4gICAgJyBwcm9ncmVzcy1iYXItc3RyaXBlZCcgOiAnJ319XCIgcm9sZT1cInByb2dyZXNzYmFyXCIgW3N0eWxlLndpZHRoLiVdPVwiZ2V0UGVyY2VudFZhbHVlKClcIlxyXG4gICAgICAgICAgICAgICAgIFthdHRyLmFyaWEtdmFsdWVub3ddPVwiZ2V0VmFsdWUoKVwiIGFyaWEtdmFsdWVtaW49XCIwXCIgW2F0dHIuYXJpYS12YWx1ZW1heF09XCJtYXhcIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwic2hvd1ZhbHVlXCIgaTE4bj1cIkBAbmd0LnByb2dyZXNzYmFyLnZhbHVlXCI+e3tnZXRQZXJjZW50VmFsdWUoKX19JTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQcm9ncmVzc2JhciB7XHJcbiAgICAvKipcclxuICAgICAqIE1heGltYWwgdmFsdWUgdG8gYmUgZGlzcGxheWVkIGluIHRoZSBwcm9ncmVzc2Jhci5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgbWF4OiBudW1iZXI7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBIGZsYWcgaW5kaWNhdGluZyBpZiB0aGUgc3RyaXBlcyBvZiB0aGUgcHJvZ3Jlc3MgYmFyIHNob3VsZCBiZSBhbmltYXRlZC4gVGFrZXMgZWZmZWN0IG9ubHkgZm9yIGJyb3dzZXJzXHJcbiAgICAgKiBzdXBwb3J0aW5nIENTUzMgYW5pbWF0aW9ucywgYW5kIGlmIHN0cmlwZWQgaXMgdHJ1ZS5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgYW5pbWF0ZWQ6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBIGZsYWcgaW5kaWNhdGluZyBpZiBhIHByb2dyZXNzIGJhciBzaG91bGQgYmUgZGlzcGxheWVkIGFzIHN0cmlwZWQuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHN0cmlwZWQ6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBIGZsYWcgaW5kaWNhdGluZyBpZiB0aGUgY3VycmVudCBwZXJjZW50YWdlIHZhbHVlIHNob3VsZCBiZSBzaG93bi5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgc2hvd1ZhbHVlOiBib29sZWFuO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogVHlwZSBvZiBwcm9ncmVzcyBiYXIsIGNhbiBiZSBvbmUgb2YgXCJzdWNjZXNzXCIsIFwiaW5mb1wiLCBcIndhcm5pbmdcIiBvciBcImRhbmdlclwiLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSB0eXBlOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDdXJyZW50IHZhbHVlIHRvIGJlIGRpc3BsYXllZCBpbiB0aGUgcHJvZ3Jlc3NiYXIuIFNob3VsZCBiZSBzbWFsbGVyIG9yIGVxdWFsIHRvIFwibWF4XCIgdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHZhbHVlID0gMDtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEhlaWdodCBvZiB0aGUgcHJvZ3Jlc3MgYmFyLiBBY2NlcHRzIGFueSB2YWxpZCBDU1MgaGVpZ2h0IHZhbHVlcywgZXguICcycmVtJ1xyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBoZWlnaHQ6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb25maWc6IE5ndFByb2dyZXNzYmFyQ29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy5tYXggPSBjb25maWcubWF4O1xyXG4gICAgICAgIHRoaXMuYW5pbWF0ZWQgPSBjb25maWcuYW5pbWF0ZWQ7XHJcbiAgICAgICAgdGhpcy5zdHJpcGVkID0gY29uZmlnLnN0cmlwZWQ7XHJcbiAgICAgICAgdGhpcy50eXBlID0gY29uZmlnLnR5cGU7XHJcbiAgICAgICAgdGhpcy5zaG93VmFsdWUgPSBjb25maWcuc2hvd1ZhbHVlO1xyXG4gICAgICAgIHRoaXMuaGVpZ2h0ID0gY29uZmlnLmhlaWdodDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRWYWx1ZSgpIHtcclxuICAgICAgICByZXR1cm4gZ2V0VmFsdWVJblJhbmdlKHRoaXMudmFsdWUsIHRoaXMubWF4KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRQZXJjZW50VmFsdWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIDEwMCAqIHRoaXMuZ2V0VmFsdWUoKSAvIHRoaXMubWF4O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==