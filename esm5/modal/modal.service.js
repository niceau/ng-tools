/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Injector, ComponentFactoryResolver } from '@angular/core';
import * as _ from 'lodash';
import { NgtModalConfig } from './modal-config';
import { NgtModalStack } from './modal-stack';
import * as i0 from "@angular/core";
import * as i1 from "./modal-stack";
import * as i2 from "./modal-config";
/**
 * A service to open modal windows. Creating a modal is straightforward: create a template and pass it as an argument to
 * the "open" method!
 */
var NgtModalService = /** @class */ (function () {
    function NgtModalService(_moduleCFR, _injector, _modalStack, _config) {
        this._moduleCFR = _moduleCFR;
        this._injector = _injector;
        this._modalStack = _modalStack;
        this._config = _config;
        this._components = [];
    }
    /**
     * Opens a new modal window with the specified content and using supplied options. Content can be provided
     * as a TemplateRef or a component type. If you pass a component type as content, then instances of those
     * components can be injected with an instance of the NgtActiveModal class. You can use methods on the
     * NgtActiveModal class to close / dismiss modals from "inside" of a component.
     */
    /**
     * Opens a new modal window with the specified content and using supplied options. Content can be provided
     * as a TemplateRef or a component type. If you pass a component type as content, then instances of those
     * components can be injected with an instance of the NgtActiveModal class. You can use methods on the
     * NgtActiveModal class to close / dismiss modals from "inside" of a component.
     * @param {?} content
     * @param {?=} options
     * @return {?}
     */
    NgtModalService.prototype.open = /**
     * Opens a new modal window with the specified content and using supplied options. Content can be provided
     * as a TemplateRef or a component type. If you pass a component type as content, then instances of those
     * components can be injected with an instance of the NgtActiveModal class. You can use methods on the
     * NgtActiveModal class to close / dismiss modals from "inside" of a component.
     * @param {?} content
     * @param {?=} options
     * @return {?}
     */
    function (content, options) {
        if (options === void 0) { options = {}; }
        /** @type {?} */
        var combinedOptions = Object.assign({}, this._config, options);
        return this._modalStack.open(this._moduleCFR, this._injector, content, combinedOptions);
    };
    /**
     * Dismiss all currently displayed modal windows with the supplied reason.
     */
    /**
     * Dismiss all currently displayed modal windows with the supplied reason.
     * @param {?=} reason
     * @return {?}
     */
    NgtModalService.prototype.dismissAll = /**
     * Dismiss all currently displayed modal windows with the supplied reason.
     * @param {?=} reason
     * @return {?}
     */
    function (reason) {
        this._modalStack.dismissAll(reason);
    };
    /**
     * Indicates if there are currently any open modal windows in the application.
     */
    /**
     * Indicates if there are currently any open modal windows in the application.
     * @return {?}
     */
    NgtModalService.prototype.hasOpenModals = /**
     * Indicates if there are currently any open modal windows in the application.
     * @return {?}
     */
    function () {
        return this._modalStack.hasOpenModals();
    };
    /**
     * Add modal component instance to _components list.
     * !NOTE: modal must have id;
     */
    /**
     * Add modal component instance to _components list.
     * !NOTE: modal must have id;
     * @param {?} componentRef
     * @return {?}
     */
    NgtModalService.prototype.add = /**
     * Add modal component instance to _components list.
     * !NOTE: modal must have id;
     * @param {?} componentRef
     * @return {?}
     */
    function (componentRef) {
        this._components.push(componentRef);
    };
    /**
     * Remove modal component instance from _components list.
     */
    /**
     * Remove modal component instance from _components list.
     * @param {?} id
     * @return {?}
     */
    NgtModalService.prototype.remove = /**
     * Remove modal component instance from _components list.
     * @param {?} id
     * @return {?}
     */
    function (id) {
        /** @type {?} */
        var modalToRemove = _.find(this._components, { id: id });
        this._components = _.without(this._components, modalToRemove);
    };
    /**
     * Opens a new modal window with the specified content and using supplied options founded in
     * _components list by specified id.
     */
    /**
     * Opens a new modal window with the specified content and using supplied options founded in
     * _components list by specified id.
     * @param {?} id
     * @return {?}
     */
    NgtModalService.prototype.openById = /**
     * Opens a new modal window with the specified content and using supplied options founded in
     * _components list by specified id.
     * @param {?} id
     * @return {?}
     */
    function (id) {
        /** @type {?} */
        var modalToOpen = _.find(this._components, { id: id });
        this.open(modalToOpen, modalToOpen.options);
    };
    /**
     * Call close method in modal component instance founded by specified id.
     */
    /**
     * Call close method in modal component instance founded by specified id.
     * @param {?} id
     * @param {?=} result
     * @return {?}
     */
    NgtModalService.prototype.closeById = /**
     * Call close method in modal component instance founded by specified id.
     * @param {?} id
     * @param {?=} result
     * @return {?}
     */
    function (id, result) {
        _.find(this._components, { id: id }).close(result);
    };
    /**
     * Call dismiss method in modal component instance founded by specified id.
     */
    /**
     * Call dismiss method in modal component instance founded by specified id.
     * @param {?} id
     * @param {?=} reason
     * @return {?}
     */
    NgtModalService.prototype.dismissById = /**
     * Call dismiss method in modal component instance founded by specified id.
     * @param {?} id
     * @param {?=} reason
     * @return {?}
     */
    function (id, reason) {
        _.find(this._components, { id: id }).dismiss(reason);
    };
    NgtModalService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    NgtModalService.ctorParameters = function () { return [
        { type: ComponentFactoryResolver },
        { type: Injector },
        { type: NgtModalStack },
        { type: NgtModalConfig }
    ]; };
    /** @nocollapse */ NgtModalService.ngInjectableDef = i0.defineInjectable({ factory: function NgtModalService_Factory() { return new NgtModalService(i0.inject(i0.ComponentFactoryResolver), i0.inject(i0.INJECTOR), i0.inject(i1.NgtModalStack), i0.inject(i2.NgtModalConfig)); }, token: NgtModalService, providedIn: "root" });
    return NgtModalService;
}());
export { NgtModalService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtModalService.prototype._components;
    /**
     * @type {?}
     * @private
     */
    NgtModalService.prototype._moduleCFR;
    /**
     * @type {?}
     * @private
     */
    NgtModalService.prototype._injector;
    /**
     * @type {?}
     * @private
     */
    NgtModalService.prototype._modalStack;
    /**
     * @type {?}
     * @private
     */
    NgtModalService.prototype._config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibW9kYWwvbW9kYWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFFNUIsT0FBTyxFQUFFLGNBQWMsRUFBbUIsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7OztBQVM5QztJQUlJLHlCQUNZLFVBQW9DLEVBQ3BDLFNBQW1CLEVBQ25CLFdBQTBCLEVBQzFCLE9BQXVCO1FBSHZCLGVBQVUsR0FBVixVQUFVLENBQTBCO1FBQ3BDLGNBQVMsR0FBVCxTQUFTLENBQVU7UUFDbkIsZ0JBQVcsR0FBWCxXQUFXLENBQWU7UUFDMUIsWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFOM0IsZ0JBQVcsR0FBd0IsRUFBRSxDQUFDO0lBTzlDLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7OztJQUNILDhCQUFJOzs7Ozs7Ozs7SUFBSixVQUFLLE9BQVksRUFBRSxPQUE2QjtRQUE3Qix3QkFBQSxFQUFBLFlBQTZCOztZQUN0QyxlQUFlLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUM7UUFDaEUsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsT0FBTyxFQUFFLGVBQWUsQ0FBQyxDQUFDO0lBQzVGLENBQUM7SUFFRDs7T0FFRzs7Ozs7O0lBQ0gsb0NBQVU7Ozs7O0lBQVYsVUFBVyxNQUFZO1FBQ25CLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3hDLENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCx1Q0FBYTs7OztJQUFiO1FBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQzVDLENBQUM7SUFFRDs7O09BR0c7Ozs7Ozs7SUFDSCw2QkFBRzs7Ozs7O0lBQUgsVUFBSSxZQUErQjtRQUMvQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQ7O09BRUc7Ozs7OztJQUNILGdDQUFNOzs7OztJQUFOLFVBQU8sRUFBVTs7WUFDUCxhQUFhLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUMsRUFBRSxFQUFFLEVBQUUsRUFBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLGFBQWEsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRDs7O09BR0c7Ozs7Ozs7SUFDSCxrQ0FBUTs7Ozs7O0lBQVIsVUFBUyxFQUFVOztZQUNULFdBQVcsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBQyxFQUFFLEVBQUUsRUFBRSxFQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRDs7T0FFRzs7Ozs7OztJQUNILG1DQUFTOzs7Ozs7SUFBVCxVQUFVLEVBQVUsRUFBRSxNQUFPO1FBQ3pCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQ7O09BRUc7Ozs7Ozs7SUFDSCxxQ0FBVzs7Ozs7O0lBQVgsVUFBWSxFQUFVLEVBQUUsTUFBTztRQUMzQixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBQyxFQUFFLEVBQUUsRUFBRSxFQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkQsQ0FBQzs7Z0JBekVKLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7Ozs7Z0JBYkQsd0JBQXdCO2dCQUFsQyxRQUFRO2dCQUlwQixhQUFhO2dCQURiLGNBQWM7OzswQkFIdkI7Q0F1RkMsQUExRUQsSUEwRUM7U0F6RVksZUFBZTs7Ozs7O0lBQ3hCLHNDQUE4Qzs7Ozs7SUFHMUMscUNBQTRDOzs7OztJQUM1QyxvQ0FBMkI7Ozs7O0lBQzNCLHNDQUFrQzs7Ozs7SUFDbEMsa0NBQStCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0b3IsIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcblxyXG5pbXBvcnQgeyBOZ3RNb2RhbENvbmZpZywgTmd0TW9kYWxPcHRpb25zIH0gZnJvbSAnLi9tb2RhbC1jb25maWcnO1xyXG5pbXBvcnQgeyBOZ3RNb2RhbFN0YWNrIH0gZnJvbSAnLi9tb2RhbC1zdGFjayc7XHJcbmltcG9ydCB7IE5ndE1vZGFsUmVmIH0gZnJvbSAnLi9tb2RhbC1yZWYnO1xyXG5pbXBvcnQgeyBOZ3RNb2RhbENvbXBvbmVudCB9IGZyb20gJy4vbW9kYWwuY29tcG9uZW50JztcclxuXHJcblxyXG4vKipcclxuICogQSBzZXJ2aWNlIHRvIG9wZW4gbW9kYWwgd2luZG93cy4gQ3JlYXRpbmcgYSBtb2RhbCBpcyBzdHJhaWdodGZvcndhcmQ6IGNyZWF0ZSBhIHRlbXBsYXRlIGFuZCBwYXNzIGl0IGFzIGFuIGFyZ3VtZW50IHRvXHJcbiAqIHRoZSBcIm9wZW5cIiBtZXRob2QhXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcclxuZXhwb3J0IGNsYXNzIE5ndE1vZGFsU2VydmljZSB7XHJcbiAgICBwcml2YXRlIF9jb21wb25lbnRzOiBOZ3RNb2RhbENvbXBvbmVudFtdID0gW107XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBfbW9kdWxlQ0ZSOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXHJcbiAgICAgICAgcHJpdmF0ZSBfaW5qZWN0b3I6IEluamVjdG9yLFxyXG4gICAgICAgIHByaXZhdGUgX21vZGFsU3RhY2s6IE5ndE1vZGFsU3RhY2ssXHJcbiAgICAgICAgcHJpdmF0ZSBfY29uZmlnOiBOZ3RNb2RhbENvbmZpZykge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT3BlbnMgYSBuZXcgbW9kYWwgd2luZG93IHdpdGggdGhlIHNwZWNpZmllZCBjb250ZW50IGFuZCB1c2luZyBzdXBwbGllZCBvcHRpb25zLiBDb250ZW50IGNhbiBiZSBwcm92aWRlZFxyXG4gICAgICogYXMgYSBUZW1wbGF0ZVJlZiBvciBhIGNvbXBvbmVudCB0eXBlLiBJZiB5b3UgcGFzcyBhIGNvbXBvbmVudCB0eXBlIGFzIGNvbnRlbnQsIHRoZW4gaW5zdGFuY2VzIG9mIHRob3NlXHJcbiAgICAgKiBjb21wb25lbnRzIGNhbiBiZSBpbmplY3RlZCB3aXRoIGFuIGluc3RhbmNlIG9mIHRoZSBOZ3RBY3RpdmVNb2RhbCBjbGFzcy4gWW91IGNhbiB1c2UgbWV0aG9kcyBvbiB0aGVcclxuICAgICAqIE5ndEFjdGl2ZU1vZGFsIGNsYXNzIHRvIGNsb3NlIC8gZGlzbWlzcyBtb2RhbHMgZnJvbSBcImluc2lkZVwiIG9mIGEgY29tcG9uZW50LlxyXG4gICAgICovXHJcbiAgICBvcGVuKGNvbnRlbnQ6IGFueSwgb3B0aW9uczogTmd0TW9kYWxPcHRpb25zID0ge30pOiBOZ3RNb2RhbFJlZiB7XHJcbiAgICAgICAgY29uc3QgY29tYmluZWRPcHRpb25zID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5fY29uZmlnLCBvcHRpb25zKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbW9kYWxTdGFjay5vcGVuKHRoaXMuX21vZHVsZUNGUiwgdGhpcy5faW5qZWN0b3IsIGNvbnRlbnQsIGNvbWJpbmVkT3B0aW9ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEaXNtaXNzIGFsbCBjdXJyZW50bHkgZGlzcGxheWVkIG1vZGFsIHdpbmRvd3Mgd2l0aCB0aGUgc3VwcGxpZWQgcmVhc29uLlxyXG4gICAgICovXHJcbiAgICBkaXNtaXNzQWxsKHJlYXNvbj86IGFueSkge1xyXG4gICAgICAgIHRoaXMuX21vZGFsU3RhY2suZGlzbWlzc0FsbChyZWFzb24pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW5kaWNhdGVzIGlmIHRoZXJlIGFyZSBjdXJyZW50bHkgYW55IG9wZW4gbW9kYWwgd2luZG93cyBpbiB0aGUgYXBwbGljYXRpb24uXHJcbiAgICAgKi9cclxuICAgIGhhc09wZW5Nb2RhbHMoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX21vZGFsU3RhY2suaGFzT3Blbk1vZGFscygpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkIG1vZGFsIGNvbXBvbmVudCBpbnN0YW5jZSB0byBfY29tcG9uZW50cyBsaXN0LlxyXG4gICAgICogIU5PVEU6IG1vZGFsIG11c3QgaGF2ZSBpZDtcclxuICAgICAqL1xyXG4gICAgYWRkKGNvbXBvbmVudFJlZjogTmd0TW9kYWxDb21wb25lbnQpIHtcclxuICAgICAgICB0aGlzLl9jb21wb25lbnRzLnB1c2goY29tcG9uZW50UmVmKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZSBtb2RhbCBjb21wb25lbnQgaW5zdGFuY2UgZnJvbSBfY29tcG9uZW50cyBsaXN0LlxyXG4gICAgICovXHJcbiAgICByZW1vdmUoaWQ6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IG1vZGFsVG9SZW1vdmUgPSBfLmZpbmQodGhpcy5fY29tcG9uZW50cywge2lkOiBpZH0pO1xyXG4gICAgICAgIHRoaXMuX2NvbXBvbmVudHMgPSBfLndpdGhvdXQodGhpcy5fY29tcG9uZW50cywgbW9kYWxUb1JlbW92ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBPcGVucyBhIG5ldyBtb2RhbCB3aW5kb3cgd2l0aCB0aGUgc3BlY2lmaWVkIGNvbnRlbnQgYW5kIHVzaW5nIHN1cHBsaWVkIG9wdGlvbnMgZm91bmRlZCBpblxyXG4gICAgICogX2NvbXBvbmVudHMgbGlzdCBieSBzcGVjaWZpZWQgaWQuXHJcbiAgICAgKi9cclxuICAgIG9wZW5CeUlkKGlkOiBzdHJpbmcpIHtcclxuICAgICAgICBjb25zdCBtb2RhbFRvT3BlbiA9IF8uZmluZCh0aGlzLl9jb21wb25lbnRzLCB7aWQ6IGlkfSk7XHJcbiAgICAgICAgdGhpcy5vcGVuKG1vZGFsVG9PcGVuLCBtb2RhbFRvT3Blbi5vcHRpb25zKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGwgY2xvc2UgbWV0aG9kIGluIG1vZGFsIGNvbXBvbmVudCBpbnN0YW5jZSBmb3VuZGVkIGJ5IHNwZWNpZmllZCBpZC5cclxuICAgICAqL1xyXG4gICAgY2xvc2VCeUlkKGlkOiBzdHJpbmcsIHJlc3VsdD8pIHtcclxuICAgICAgICBfLmZpbmQodGhpcy5fY29tcG9uZW50cywge2lkOiBpZH0pLmNsb3NlKHJlc3VsdCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDYWxsIGRpc21pc3MgbWV0aG9kIGluIG1vZGFsIGNvbXBvbmVudCBpbnN0YW5jZSBmb3VuZGVkIGJ5IHNwZWNpZmllZCBpZC5cclxuICAgICAqL1xyXG4gICAgZGlzbWlzc0J5SWQoaWQ6IHN0cmluZywgcmVhc29uPykge1xyXG4gICAgICAgIF8uZmluZCh0aGlzLl9jb21wb25lbnRzLCB7aWQ6IGlkfSkuZGlzbWlzcyhyZWFzb24pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==