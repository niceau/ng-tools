/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtModalService } from './modal.service';
var NgtModalCloseDirective = /** @class */ (function () {
    function NgtModalCloseDirective(_modalService) {
        this._modalService = _modalService;
    }
    /**
     * @return {?}
     */
    NgtModalCloseDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this._modalService.closeById(this.id, this.result);
    };
    NgtModalCloseDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtModalClose]'
                },] }
    ];
    /** @nocollapse */
    NgtModalCloseDirective.ctorParameters = function () { return [
        { type: NgtModalService }
    ]; };
    NgtModalCloseDirective.propDecorators = {
        id: [{ type: Input, args: ['ngtModalClose',] }],
        result: [{ type: Input }],
        onClick: [{ type: HostListener, args: ['click',] }]
    };
    return NgtModalCloseDirective;
}());
export { NgtModalCloseDirective };
if (false) {
    /** @type {?} */
    NgtModalCloseDirective.prototype.id;
    /** @type {?} */
    NgtModalCloseDirective.prototype.result;
    /**
     * @type {?}
     * @private
     */
    NgtModalCloseDirective.prototype._modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtY2xvc2UuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJtb2RhbC9tb2RhbC1jbG9zZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUvRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFbEQ7SUFPSSxnQ0FBb0IsYUFBOEI7UUFBOUIsa0JBQWEsR0FBYixhQUFhLENBQWlCO0lBQ2xELENBQUM7Ozs7SUFHRCx3Q0FBTzs7O0lBRFA7UUFFSSxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN2RCxDQUFDOztnQkFiSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGlCQUFpQjtpQkFDOUI7Ozs7Z0JBSlEsZUFBZTs7O3FCQU1uQixLQUFLLFNBQUMsZUFBZTt5QkFDckIsS0FBSzswQkFLTCxZQUFZLFNBQUMsT0FBTzs7SUFJekIsNkJBQUM7Q0FBQSxBQWRELElBY0M7U0FYWSxzQkFBc0I7OztJQUMvQixvQ0FBbUM7O0lBQ25DLHdDQUF3Qjs7Ozs7SUFFWiwrQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IE5ndE1vZGFsU2VydmljZSB9IGZyb20gJy4vbW9kYWwuc2VydmljZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndE1vZGFsQ2xvc2VdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0TW9kYWxDbG9zZURpcmVjdGl2ZSB7XHJcbiAgICBASW5wdXQoJ25ndE1vZGFsQ2xvc2UnKSBpZDogc3RyaW5nO1xyXG4gICAgQElucHV0KCkgcmVzdWx0OiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfbW9kYWxTZXJ2aWNlOiBOZ3RNb2RhbFNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdjbGljaycpXHJcbiAgICBvbkNsaWNrKCkge1xyXG4gICAgICAgIHRoaXMuX21vZGFsU2VydmljZS5jbG9zZUJ5SWQodGhpcy5pZCwgdGhpcy5yZXN1bHQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==