/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Subject } from 'rxjs';
/**
 * A reference to an active (currently opened) modal. Instances of this class
 * can be injected into components passed as modal content.
 */
var /**
 * A reference to an active (currently opened) modal. Instances of this class
 * can be injected into components passed as modal content.
 */
NgtActiveModal = /** @class */ (function () {
    function NgtActiveModal() {
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
    }
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     */
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    NgtActiveModal.prototype.close = /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    function (result) { };
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     */
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    NgtActiveModal.prototype.dismiss = /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    function (reason) { };
    return NgtActiveModal;
}());
/**
 * A reference to an active (currently opened) modal. Instances of this class
 * can be injected into components passed as modal content.
 */
export { NgtActiveModal };
if (false) {
    /** @type {?} */
    NgtActiveModal.prototype.modalOpeningDidStart;
    /** @type {?} */
    NgtActiveModal.prototype.modalOpeningDidDone;
    /** @type {?} */
    NgtActiveModal.prototype.modalClosingDidStart;
    /** @type {?} */
    NgtActiveModal.prototype.modalClosingDidDone;
    /** @type {?} */
    NgtActiveModal.prototype.backdropOpeningDidStart;
    /** @type {?} */
    NgtActiveModal.prototype.backdropOpeningDidDone;
    /** @type {?} */
    NgtActiveModal.prototype.backdropClosingDidStart;
    /** @type {?} */
    NgtActiveModal.prototype.backdropClosingDidDone;
}
/**
 * A reference to a newly opened modal returned by the 'NgtModalComponent.open()' method.
 */
var /**
 * A reference to a newly opened modal returned by the 'NgtModalComponent.open()' method.
 */
NgtModalRef = /** @class */ (function () {
    function NgtModalRef(_windowCmptRef, _contentRef, _backdropCmptRef, _beforeDismiss) {
        var _this = this;
        this._windowCmptRef = _windowCmptRef;
        this._contentRef = _contentRef;
        this._backdropCmptRef = _backdropCmptRef;
        this._beforeDismiss = _beforeDismiss;
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
        _windowCmptRef.instance.dismissEvent.subscribe((/**
         * @param {?} reason
         * @return {?}
         */
        function (reason) { return _this.dismiss(reason); }));
        _windowCmptRef.instance.modalOpeningDidStart.subscribe((/**
         * @return {?}
         */
        function () { return _this.modalOpeningDidStart.next(); }));
        _windowCmptRef.instance.modalOpeningDidDone.subscribe((/**
         * @return {?}
         */
        function () { return _this.modalOpeningDidDone.next(); }));
        _windowCmptRef.instance.modalClosingDidStart.subscribe((/**
         * @return {?}
         */
        function () { return _this.modalClosingDidStart.next(); }));
        _windowCmptRef.instance.modalClosingDidDone.subscribe((/**
         * @return {?}
         */
        function () { return _this.modalClosingDidDone.next(); }));
        _backdropCmptRef.instance.backdropOpeningDidStart.subscribe((/**
         * @return {?}
         */
        function () { return _this.backdropOpeningDidStart.next(); }));
        _backdropCmptRef.instance.backdropOpeningDidDone.subscribe((/**
         * @return {?}
         */
        function () { return _this.backdropOpeningDidDone.next(); }));
        _backdropCmptRef.instance.backdropClosingDidStart.subscribe((/**
         * @return {?}
         */
        function () { return _this.backdropClosingDidStart.next(); }));
        _backdropCmptRef.instance.backdropClosingDidDone.subscribe((/**
         * @return {?}
         */
        function () { return _this.backdropClosingDidDone.next(); }));
        this.result = new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            _this._resolve = resolve;
            _this._reject = reject;
        }));
        this.result.then(null, (/**
         * @return {?}
         */
        function () { }));
    }
    Object.defineProperty(NgtModalRef.prototype, "componentInstance", {
        /**
         * The instance of component used as modal's content.
         * Undefined when a TemplateRef is used as modal's content.
         */
        get: /**
         * The instance of component used as modal's content.
         * Undefined when a TemplateRef is used as modal's content.
         * @return {?}
         */
        function () {
            if (this._contentRef.componentRef) {
                return this._contentRef.componentRef.instance;
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     */
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    NgtModalRef.prototype.close = /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    function (result) {
        if (this._windowCmptRef) {
            this._resolve(result);
            this._removeModalElements();
        }
    };
    /**
     * @private
     * @param {?=} reason
     * @return {?}
     */
    NgtModalRef.prototype._dismiss = /**
     * @private
     * @param {?=} reason
     * @return {?}
     */
    function (reason) {
        this._reject(reason);
        this._removeModalElements();
    };
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     */
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    NgtModalRef.prototype.dismiss = /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    function (reason) {
        var _this = this;
        if (this._windowCmptRef) {
            if (!this._beforeDismiss) {
                this._dismiss(reason);
            }
            else {
                /** @type {?} */
                var dismiss = this._beforeDismiss();
                if (dismiss && dismiss.then) {
                    dismiss.then((/**
                     * @param {?} result
                     * @return {?}
                     */
                    function (result) {
                        if (result !== false) {
                            _this._dismiss(reason);
                        }
                    }), (/**
                     * @return {?}
                     */
                    function () {
                    }));
                }
                else if (dismiss !== false) {
                    this._dismiss(reason);
                }
            }
        }
    };
    /**
     * @private
     * @return {?}
     */
    NgtModalRef.prototype._removeModalElements = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.modalClosingDidDone.subscribe((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var windowNativeEl = _this._windowCmptRef.location.nativeElement;
            windowNativeEl.parentNode.removeChild(windowNativeEl);
            _this._windowCmptRef.destroy();
            _this._windowCmptRef = null;
            if (_this._contentRef && _this._contentRef.viewRef) {
                _this._contentRef.viewRef.destroy();
            }
            _this._contentRef = null;
        }));
        this._windowCmptRef.instance.animation = 'close';
        if (this._backdropCmptRef) {
            this.backdropClosingDidDone.subscribe((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var backdropNativeEl = _this._backdropCmptRef.location.nativeElement;
                backdropNativeEl.parentNode.removeChild(backdropNativeEl);
                _this._backdropCmptRef.destroy();
                _this._backdropCmptRef = null;
            }));
            this._backdropCmptRef.instance.animation = 'close';
        }
    };
    return NgtModalRef;
}());
/**
 * A reference to a newly opened modal returned by the 'NgtModalComponent.open()' method.
 */
export { NgtModalRef };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._resolve;
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._reject;
    /** @type {?} */
    NgtModalRef.prototype.modalOpeningDidStart;
    /** @type {?} */
    NgtModalRef.prototype.modalOpeningDidDone;
    /** @type {?} */
    NgtModalRef.prototype.modalClosingDidStart;
    /** @type {?} */
    NgtModalRef.prototype.modalClosingDidDone;
    /** @type {?} */
    NgtModalRef.prototype.backdropOpeningDidStart;
    /** @type {?} */
    NgtModalRef.prototype.backdropOpeningDidDone;
    /** @type {?} */
    NgtModalRef.prototype.backdropClosingDidStart;
    /** @type {?} */
    NgtModalRef.prototype.backdropClosingDidDone;
    /**
     * A promise that is resolved when the modal is closed and rejected when the modal is dismissed.
     * @type {?}
     */
    NgtModalRef.prototype.result;
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._windowCmptRef;
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._contentRef;
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._backdropCmptRef;
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._beforeDismiss;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtcmVmLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJtb2RhbC9tb2RhbC1yZWYudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUdBLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7Ozs7O0FBTy9COzs7OztJQUFBO1FBQ0kseUJBQW9CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNyQyx3QkFBbUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3BDLHlCQUFvQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDckMsd0JBQW1CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUVwQyw0QkFBdUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3hDLDJCQUFzQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDdkMsNEJBQXVCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUN4QywyQkFBc0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO0lBYTNDLENBQUM7SUFaRzs7O09BR0c7Ozs7Ozs7SUFDSCw4QkFBSzs7Ozs7O0lBQUwsVUFBTSxNQUFZLElBQVMsQ0FBQztJQUU1Qjs7O09BR0c7Ozs7Ozs7SUFDSCxnQ0FBTzs7Ozs7O0lBQVAsVUFBUSxNQUFZLElBQVMsQ0FBQztJQUVsQyxxQkFBQztBQUFELENBQUMsQUF0QkQsSUFzQkM7Ozs7Ozs7O0lBckJHLDhDQUFxQzs7SUFDckMsNkNBQW9DOztJQUNwQyw4Q0FBcUM7O0lBQ3JDLDZDQUFvQzs7SUFFcEMsaURBQXdDOztJQUN4QyxnREFBdUM7O0lBQ3ZDLGlEQUF3Qzs7SUFDeEMsZ0RBQXVDOzs7OztBQWtCM0M7Ozs7SUE2QkkscUJBQ1ksY0FBcUQsRUFDckQsV0FBdUIsRUFDdkIsZ0JBQTBELEVBQzFELGNBQXlCO1FBSnJDLGlCQW9CQztRQW5CVyxtQkFBYyxHQUFkLGNBQWMsQ0FBdUM7UUFDckQsZ0JBQVcsR0FBWCxXQUFXLENBQVk7UUFDdkIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUEwQztRQUMxRCxtQkFBYyxHQUFkLGNBQWMsQ0FBVztRQTdCckMseUJBQW9CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNyQyx3QkFBbUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3BDLHlCQUFvQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDckMsd0JBQW1CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUVwQyw0QkFBdUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3hDLDJCQUFzQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDdkMsNEJBQXVCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUN4QywyQkFBc0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBc0JuQyxjQUFjLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxNQUFXLElBQUssT0FBQSxLQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFwQixDQUFvQixFQUFDLENBQUM7UUFDdEYsY0FBYyxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTOzs7UUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxFQUFoQyxDQUFnQyxFQUFDLENBQUM7UUFDL0YsY0FBYyxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTOzs7UUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxFQUEvQixDQUErQixFQUFDLENBQUM7UUFDN0YsY0FBYyxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTOzs7UUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxFQUFoQyxDQUFnQyxFQUFDLENBQUM7UUFDL0YsY0FBYyxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTOzs7UUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxFQUEvQixDQUErQixFQUFDLENBQUM7UUFDN0YsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLFNBQVM7OztRQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLEVBQW5DLENBQW1DLEVBQUMsQ0FBQztRQUN2RyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsc0JBQXNCLENBQUMsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsRUFBbEMsQ0FBa0MsRUFBQyxDQUFDO1FBQ3JHLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTOzs7UUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxFQUFuQyxDQUFtQyxFQUFDLENBQUM7UUFDdkcsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLFNBQVM7OztRQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxFQUFFLEVBQWxDLENBQWtDLEVBQUMsQ0FBQztRQUVyRyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQ3RDLEtBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQzFCLENBQUMsRUFBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSTs7O1FBQUUsY0FBTyxDQUFDLEVBQUMsQ0FBQztJQUNyQyxDQUFDO0lBL0JELHNCQUFJLDBDQUFpQjtRQUpyQjs7O1dBR0c7Ozs7OztRQUNIO1lBQ0ksSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRTtnQkFDL0IsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUM7YUFDakQ7UUFDTCxDQUFDOzs7T0FBQTtJQTZCRDs7O09BR0c7Ozs7Ozs7SUFDSCwyQkFBSzs7Ozs7O0lBQUwsVUFBTSxNQUFZO1FBQ2QsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7U0FDL0I7SUFDTCxDQUFDOzs7Ozs7SUFFTyw4QkFBUTs7Ozs7SUFBaEIsVUFBaUIsTUFBWTtRQUN6QixJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRDs7O09BR0c7Ozs7Ozs7SUFDSCw2QkFBTzs7Ozs7O0lBQVAsVUFBUSxNQUFZO1FBQXBCLGlCQW9CQztRQW5CRyxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ3RCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDekI7aUJBQU07O29CQUNHLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFO2dCQUNyQyxJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFO29CQUN6QixPQUFPLENBQUMsSUFBSTs7OztvQkFDUixVQUFBLE1BQU07d0JBQ0YsSUFBSSxNQUFNLEtBQUssS0FBSyxFQUFFOzRCQUNsQixLQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3lCQUN6QjtvQkFDTCxDQUFDOzs7b0JBQ0Q7b0JBQ0EsQ0FBQyxFQUFDLENBQUM7aUJBQ1Y7cUJBQU0sSUFBSSxPQUFPLEtBQUssS0FBSyxFQUFFO29CQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUN6QjthQUNKO1NBQ0o7SUFDTCxDQUFDOzs7OztJQUVPLDBDQUFvQjs7OztJQUE1QjtRQUFBLGlCQXlCQztRQXhCRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUzs7O1FBQUM7O2dCQUN6QixjQUFjLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYTtZQUNqRSxjQUFjLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN0RCxLQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzlCLEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBRTNCLElBQUksS0FBSSxDQUFDLFdBQVcsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRTtnQkFDOUMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDdEM7WUFFRCxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUM1QixDQUFDLEVBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7UUFFakQsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDdkIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVM7OztZQUFDOztvQkFDNUIsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhO2dCQUNyRSxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQzFELEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDaEMsS0FBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUNqQyxDQUFDLEVBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQztTQUN0RDtJQUVMLENBQUM7SUFDTCxrQkFBQztBQUFELENBQUMsQUF2SEQsSUF1SEM7Ozs7Ozs7Ozs7SUF0SEcsK0JBQXlDOzs7OztJQUN6Qyw4QkFBd0M7O0lBRXhDLDJDQUFxQzs7SUFDckMsMENBQW9DOztJQUNwQywyQ0FBcUM7O0lBQ3JDLDBDQUFvQzs7SUFFcEMsOENBQXdDOztJQUN4Qyw2Q0FBdUM7O0lBQ3ZDLDhDQUF3Qzs7SUFDeEMsNkNBQXVDOzs7OztJQWV2Qyw2QkFBcUI7Ozs7O0lBR2pCLHFDQUE2RDs7Ozs7SUFDN0Qsa0NBQStCOzs7OztJQUMvQix1Q0FBa0U7Ozs7O0lBQ2xFLHFDQUFpQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOZ3RNb2RhbFdpbmRvd0NvbXBvbmVudCB9IGZyb20gJy4vbW9kYWwtd2luZG93LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE5ndE1vZGFsQmFja2Ryb3BDb21wb25lbnQgfSBmcm9tICcuL21vZGFsLWJhY2tkcm9wLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQ29udGVudFJlZiB9IGZyb20gJy4uL3V0aWwvcG9wdXAnO1xyXG5cclxuLyoqXHJcbiAqIEEgcmVmZXJlbmNlIHRvIGFuIGFjdGl2ZSAoY3VycmVudGx5IG9wZW5lZCkgbW9kYWwuIEluc3RhbmNlcyBvZiB0aGlzIGNsYXNzXHJcbiAqIGNhbiBiZSBpbmplY3RlZCBpbnRvIGNvbXBvbmVudHMgcGFzc2VkIGFzIG1vZGFsIGNvbnRlbnQuXHJcbiAqL1xyXG5leHBvcnQgY2xhc3MgTmd0QWN0aXZlTW9kYWwge1xyXG4gICAgbW9kYWxPcGVuaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgbW9kYWxPcGVuaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBtb2RhbENsb3NpbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBtb2RhbENsb3NpbmdEaWREb25lID0gbmV3IFN1YmplY3QoKTtcclxuXHJcbiAgICBiYWNrZHJvcE9wZW5pbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBiYWNrZHJvcE9wZW5pbmdEaWREb25lID0gbmV3IFN1YmplY3QoKTtcclxuICAgIGJhY2tkcm9wQ2xvc2luZ0RpZFN0YXJ0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIGJhY2tkcm9wQ2xvc2luZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgLyoqXHJcbiAgICAgKiBDbG9zZXMgdGhlIG1vZGFsIHdpdGggYW4gb3B0aW9uYWwgJ3Jlc3VsdCcgdmFsdWUuXHJcbiAgICAgKiBUaGUgJ05ndE1vYmFsUmVmLnJlc3VsdCcgcHJvbWlzZSB3aWxsIGJlIHJlc29sdmVkIHdpdGggcHJvdmlkZWQgdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIGNsb3NlKHJlc3VsdD86IGFueSk6IHZvaWQge31cclxuXHJcbiAgICAvKipcclxuICAgICAqIERpc21pc3NlcyB0aGUgbW9kYWwgd2l0aCBhbiBvcHRpb25hbCAncmVhc29uJyB2YWx1ZS5cclxuICAgICAqIFRoZSAnTmd0TW9kYWxSZWYucmVzdWx0JyBwcm9taXNlIHdpbGwgYmUgcmVqZWN0ZWQgd2l0aCBwcm92aWRlZCB2YWx1ZS5cclxuICAgICAqL1xyXG4gICAgZGlzbWlzcyhyZWFzb24/OiBhbnkpOiB2b2lkIHt9XHJcblxyXG59XHJcblxyXG4vKipcclxuICogQSByZWZlcmVuY2UgdG8gYSBuZXdseSBvcGVuZWQgbW9kYWwgcmV0dXJuZWQgYnkgdGhlICdOZ3RNb2RhbENvbXBvbmVudC5vcGVuKCknIG1ldGhvZC5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBOZ3RNb2RhbFJlZiB7XHJcbiAgICBwcml2YXRlIF9yZXNvbHZlOiAocmVzdWx0PzogYW55KSA9PiB2b2lkO1xyXG4gICAgcHJpdmF0ZSBfcmVqZWN0OiAocmVhc29uPzogYW55KSA9PiB2b2lkO1xyXG5cclxuICAgIG1vZGFsT3BlbmluZ0RpZFN0YXJ0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIG1vZGFsT3BlbmluZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgbW9kYWxDbG9zaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgbW9kYWxDbG9zaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcblxyXG4gICAgYmFja2Ryb3BPcGVuaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgYmFja2Ryb3BPcGVuaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBiYWNrZHJvcENsb3NpbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBiYWNrZHJvcENsb3NpbmdEaWREb25lID0gbmV3IFN1YmplY3QoKTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFRoZSBpbnN0YW5jZSBvZiBjb21wb25lbnQgdXNlZCBhcyBtb2RhbCdzIGNvbnRlbnQuXHJcbiAgICAgKiBVbmRlZmluZWQgd2hlbiBhIFRlbXBsYXRlUmVmIGlzIHVzZWQgYXMgbW9kYWwncyBjb250ZW50LlxyXG4gICAgICovXHJcbiAgICBnZXQgY29tcG9uZW50SW5zdGFuY2UoKTogYW55IHtcclxuICAgICAgICBpZiAodGhpcy5fY29udGVudFJlZi5jb21wb25lbnRSZWYpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2NvbnRlbnRSZWYuY29tcG9uZW50UmVmLmluc3RhbmNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEEgcHJvbWlzZSB0aGF0IGlzIHJlc29sdmVkIHdoZW4gdGhlIG1vZGFsIGlzIGNsb3NlZCBhbmQgcmVqZWN0ZWQgd2hlbiB0aGUgbW9kYWwgaXMgZGlzbWlzc2VkLlxyXG4gICAgICovXHJcbiAgICByZXN1bHQ6IFByb21pc2U8YW55PjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF93aW5kb3dDbXB0UmVmOiBDb21wb25lbnRSZWY8Tmd0TW9kYWxXaW5kb3dDb21wb25lbnQ+LFxyXG4gICAgICAgIHByaXZhdGUgX2NvbnRlbnRSZWY6IENvbnRlbnRSZWYsXHJcbiAgICAgICAgcHJpdmF0ZSBfYmFja2Ryb3BDbXB0UmVmPzogQ29tcG9uZW50UmVmPE5ndE1vZGFsQmFja2Ryb3BDb21wb25lbnQ+LFxyXG4gICAgICAgIHByaXZhdGUgX2JlZm9yZURpc21pc3M/OiBGdW5jdGlvbikge1xyXG4gICAgICAgIF93aW5kb3dDbXB0UmVmLmluc3RhbmNlLmRpc21pc3NFdmVudC5zdWJzY3JpYmUoKHJlYXNvbjogYW55KSA9PiB0aGlzLmRpc21pc3MocmVhc29uKSk7XHJcbiAgICAgICAgX3dpbmRvd0NtcHRSZWYuaW5zdGFuY2UubW9kYWxPcGVuaW5nRGlkU3RhcnQuc3Vic2NyaWJlKCgpID0+IHRoaXMubW9kYWxPcGVuaW5nRGlkU3RhcnQubmV4dCgpKTtcclxuICAgICAgICBfd2luZG93Q21wdFJlZi5pbnN0YW5jZS5tb2RhbE9wZW5pbmdEaWREb25lLnN1YnNjcmliZSgoKSA9PiB0aGlzLm1vZGFsT3BlbmluZ0RpZERvbmUubmV4dCgpKTtcclxuICAgICAgICBfd2luZG93Q21wdFJlZi5pbnN0YW5jZS5tb2RhbENsb3NpbmdEaWRTdGFydC5zdWJzY3JpYmUoKCkgPT4gdGhpcy5tb2RhbENsb3NpbmdEaWRTdGFydC5uZXh0KCkpO1xyXG4gICAgICAgIF93aW5kb3dDbXB0UmVmLmluc3RhbmNlLm1vZGFsQ2xvc2luZ0RpZERvbmUuc3Vic2NyaWJlKCgpID0+IHRoaXMubW9kYWxDbG9zaW5nRGlkRG9uZS5uZXh0KCkpO1xyXG4gICAgICAgIF9iYWNrZHJvcENtcHRSZWYuaW5zdGFuY2UuYmFja2Ryb3BPcGVuaW5nRGlkU3RhcnQuc3Vic2NyaWJlKCgpID0+IHRoaXMuYmFja2Ryb3BPcGVuaW5nRGlkU3RhcnQubmV4dCgpKTtcclxuICAgICAgICBfYmFja2Ryb3BDbXB0UmVmLmluc3RhbmNlLmJhY2tkcm9wT3BlbmluZ0RpZERvbmUuc3Vic2NyaWJlKCgpID0+IHRoaXMuYmFja2Ryb3BPcGVuaW5nRGlkRG9uZS5uZXh0KCkpO1xyXG4gICAgICAgIF9iYWNrZHJvcENtcHRSZWYuaW5zdGFuY2UuYmFja2Ryb3BDbG9zaW5nRGlkU3RhcnQuc3Vic2NyaWJlKCgpID0+IHRoaXMuYmFja2Ryb3BDbG9zaW5nRGlkU3RhcnQubmV4dCgpKTtcclxuICAgICAgICBfYmFja2Ryb3BDbXB0UmVmLmluc3RhbmNlLmJhY2tkcm9wQ2xvc2luZ0RpZERvbmUuc3Vic2NyaWJlKCgpID0+IHRoaXMuYmFja2Ryb3BDbG9zaW5nRGlkRG9uZS5uZXh0KCkpO1xyXG5cclxuICAgICAgICB0aGlzLnJlc3VsdCA9IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fcmVzb2x2ZSA9IHJlc29sdmU7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlamVjdCA9IHJlamVjdDtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnJlc3VsdC50aGVuKG51bGwsICgpID0+IHt9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENsb3NlcyB0aGUgbW9kYWwgd2l0aCBhbiBvcHRpb25hbCAncmVzdWx0JyB2YWx1ZS5cclxuICAgICAqIFRoZSAnTmd0TW9iYWxSZWYucmVzdWx0JyBwcm9taXNlIHdpbGwgYmUgcmVzb2x2ZWQgd2l0aCBwcm92aWRlZCB2YWx1ZS5cclxuICAgICAqL1xyXG4gICAgY2xvc2UocmVzdWx0PzogYW55KTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3dpbmRvd0NtcHRSZWYpIHtcclxuICAgICAgICAgICAgdGhpcy5fcmVzb2x2ZShyZXN1bHQpO1xyXG4gICAgICAgICAgICB0aGlzLl9yZW1vdmVNb2RhbEVsZW1lbnRzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2Rpc21pc3MocmVhc29uPzogYW55KSB7XHJcbiAgICAgICAgdGhpcy5fcmVqZWN0KHJlYXNvbik7XHJcbiAgICAgICAgdGhpcy5fcmVtb3ZlTW9kYWxFbGVtZW50cygpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGlzbWlzc2VzIHRoZSBtb2RhbCB3aXRoIGFuIG9wdGlvbmFsICdyZWFzb24nIHZhbHVlLlxyXG4gICAgICogVGhlICdOZ3RNb2RhbFJlZi5yZXN1bHQnIHByb21pc2Ugd2lsbCBiZSByZWplY3RlZCB3aXRoIHByb3ZpZGVkIHZhbHVlLlxyXG4gICAgICovXHJcbiAgICBkaXNtaXNzKHJlYXNvbj86IGFueSk6IHZvaWQge1xyXG4gICAgICAgIGlmICh0aGlzLl93aW5kb3dDbXB0UmVmKSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5fYmVmb3JlRGlzbWlzcykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fZGlzbWlzcyhyZWFzb24pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZGlzbWlzcyA9IHRoaXMuX2JlZm9yZURpc21pc3MoKTtcclxuICAgICAgICAgICAgICAgIGlmIChkaXNtaXNzICYmIGRpc21pc3MudGhlbikge1xyXG4gICAgICAgICAgICAgICAgICAgIGRpc21pc3MudGhlbihcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXN1bHQgIT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5fZGlzbWlzcyhyZWFzb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChkaXNtaXNzICE9PSBmYWxzZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2Rpc21pc3MocmVhc29uKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9yZW1vdmVNb2RhbEVsZW1lbnRzKCkge1xyXG4gICAgICAgIHRoaXMubW9kYWxDbG9zaW5nRGlkRG9uZS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCB3aW5kb3dOYXRpdmVFbCA9IHRoaXMuX3dpbmRvd0NtcHRSZWYubG9jYXRpb24ubmF0aXZlRWxlbWVudDtcclxuICAgICAgICAgICAgd2luZG93TmF0aXZlRWwucGFyZW50Tm9kZS5yZW1vdmVDaGlsZCh3aW5kb3dOYXRpdmVFbCk7XHJcbiAgICAgICAgICAgIHRoaXMuX3dpbmRvd0NtcHRSZWYuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICB0aGlzLl93aW5kb3dDbXB0UmVmID0gbnVsbDtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLl9jb250ZW50UmVmICYmIHRoaXMuX2NvbnRlbnRSZWYudmlld1JlZikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fY29udGVudFJlZi52aWV3UmVmLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5fY29udGVudFJlZiA9IG51bGw7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5fd2luZG93Q21wdFJlZi5pbnN0YW5jZS5hbmltYXRpb24gPSAnY2xvc2UnO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fYmFja2Ryb3BDbXB0UmVmKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYmFja2Ryb3BDbG9zaW5nRGlkRG9uZS5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgYmFja2Ryb3BOYXRpdmVFbCA9IHRoaXMuX2JhY2tkcm9wQ21wdFJlZi5sb2NhdGlvbi5uYXRpdmVFbGVtZW50O1xyXG4gICAgICAgICAgICAgICAgYmFja2Ryb3BOYXRpdmVFbC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGJhY2tkcm9wTmF0aXZlRWwpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fYmFja2Ryb3BDbXB0UmVmLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2JhY2tkcm9wQ21wdFJlZiA9IG51bGw7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLl9iYWNrZHJvcENtcHRSZWYuaW5zdGFuY2UuYW5pbWF0aW9uID0gJ2Nsb3NlJztcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG59XHJcbiJdfQ==