/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtModalService } from './modal.service';
var NgtModalDismissDirective = /** @class */ (function () {
    function NgtModalDismissDirective(_modalService) {
        this._modalService = _modalService;
    }
    /**
     * @return {?}
     */
    NgtModalDismissDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this._modalService.dismissById(this.id, this.reason);
    };
    NgtModalDismissDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtModalDismiss]'
                },] }
    ];
    /** @nocollapse */
    NgtModalDismissDirective.ctorParameters = function () { return [
        { type: NgtModalService }
    ]; };
    NgtModalDismissDirective.propDecorators = {
        id: [{ type: Input, args: ['ngtModalDismiss',] }],
        reason: [{ type: Input }],
        onClick: [{ type: HostListener, args: ['click',] }]
    };
    return NgtModalDismissDirective;
}());
export { NgtModalDismissDirective };
if (false) {
    /** @type {?} */
    NgtModalDismissDirective.prototype.id;
    /** @type {?} */
    NgtModalDismissDirective.prototype.reason;
    /**
     * @type {?}
     * @private
     */
    NgtModalDismissDirective.prototype._modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtZGlzbWlzcy5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbIm1vZGFsL21vZGFsLWRpc21pc3MuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRWxEO0lBT0ksa0NBQW9CLGFBQThCO1FBQTlCLGtCQUFhLEdBQWIsYUFBYSxDQUFpQjtJQUNsRCxDQUFDOzs7O0lBR0QsMENBQU87OztJQURQO1FBRUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDekQsQ0FBQzs7Z0JBYkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxtQkFBbUI7aUJBQ2hDOzs7O2dCQUpRLGVBQWU7OztxQkFNbkIsS0FBSyxTQUFDLGlCQUFpQjt5QkFDdkIsS0FBSzswQkFLTCxZQUFZLFNBQUMsT0FBTzs7SUFJekIsK0JBQUM7Q0FBQSxBQWRELElBY0M7U0FYWSx3QkFBd0I7OztJQUNqQyxzQ0FBcUM7O0lBQ3JDLDBDQUF3Qjs7Ozs7SUFFWixpREFBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IE5ndE1vZGFsU2VydmljZSB9IGZyb20gJy4vbW9kYWwuc2VydmljZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndE1vZGFsRGlzbWlzc10nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RNb2RhbERpc21pc3NEaXJlY3RpdmUge1xyXG4gICAgQElucHV0KCduZ3RNb2RhbERpc21pc3MnKSBpZDogc3RyaW5nO1xyXG4gICAgQElucHV0KCkgcmVhc29uOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfbW9kYWxTZXJ2aWNlOiBOZ3RNb2RhbFNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdjbGljaycpXHJcbiAgICBvbkNsaWNrKCkge1xyXG4gICAgICAgIHRoaXMuX21vZGFsU2VydmljZS5kaXNtaXNzQnlJZCh0aGlzLmlkLCB0aGlzLnJlYXNvbik7XHJcbiAgICB9XHJcbn1cclxuIl19