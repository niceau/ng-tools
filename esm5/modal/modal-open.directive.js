/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtModalService } from './modal.service';
var NgtModalOpenDirective = /** @class */ (function () {
    function NgtModalOpenDirective(_modalService) {
        this._modalService = _modalService;
    }
    /**
     * @return {?}
     */
    NgtModalOpenDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this._modalService.openById(this.id);
    };
    NgtModalOpenDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtModalOpen]'
                },] }
    ];
    /** @nocollapse */
    NgtModalOpenDirective.ctorParameters = function () { return [
        { type: NgtModalService }
    ]; };
    NgtModalOpenDirective.propDecorators = {
        id: [{ type: Input, args: ['ngtModalOpen',] }],
        onClick: [{ type: HostListener, args: ['click',] }]
    };
    return NgtModalOpenDirective;
}());
export { NgtModalOpenDirective };
if (false) {
    /** @type {?} */
    NgtModalOpenDirective.prototype.id;
    /**
     * @type {?}
     * @private
     */
    NgtModalOpenDirective.prototype._modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtb3Blbi5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbIm1vZGFsL21vZGFsLW9wZW4uZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRWxEO0lBTUksK0JBQW9CLGFBQThCO1FBQTlCLGtCQUFhLEdBQWIsYUFBYSxDQUFpQjtJQUNsRCxDQUFDOzs7O0lBR0QsdUNBQU87OztJQURQO1FBRUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7O2dCQVpKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsZ0JBQWdCO2lCQUM3Qjs7OztnQkFKUSxlQUFlOzs7cUJBTW5CLEtBQUssU0FBQyxjQUFjOzBCQUtwQixZQUFZLFNBQUMsT0FBTzs7SUFJekIsNEJBQUM7Q0FBQSxBQWJELElBYUM7U0FWWSxxQkFBcUI7OztJQUM5QixtQ0FBa0M7Ozs7O0lBRXRCLDhDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTmd0TW9kYWxTZXJ2aWNlIH0gZnJvbSAnLi9tb2RhbC5zZXJ2aWNlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbbmd0TW9kYWxPcGVuXSdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndE1vZGFsT3BlbkRpcmVjdGl2ZSB7XHJcbiAgICBASW5wdXQoJ25ndE1vZGFsT3BlbicpIGlkOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfbW9kYWxTZXJ2aWNlOiBOZ3RNb2RhbFNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdjbGljaycpXHJcbiAgICBvbkNsaWNrKCkge1xyXG4gICAgICAgIHRoaXMuX21vZGFsU2VydmljZS5vcGVuQnlJZCh0aGlzLmlkKTtcclxuICAgIH1cclxufVxyXG4iXX0=