/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ElementRef, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { NgtModalService } from './modal.service';
import { NgtModalConfig } from './modal-config';
import { NgtActiveModal } from './modal-ref';
import { isDefined } from '../util/util';
var NgtModalComponent = /** @class */ (function () {
    function NgtModalComponent(activeModal, _modalService, _config, _elRef) {
        this.activeModal = activeModal;
        this._modalService = _modalService;
        this._config = _config;
        this._elRef = _elRef;
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.options = {};
    }
    /**
     * @return {?}
     */
    NgtModalComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        // set options
        if (isDefined(this.backdrop)) {
            this.options.backdrop = this.backdrop;
        }
        if (isDefined(this.beforeDismiss)) {
            this.options.beforeDismiss = this.beforeDismiss;
        }
        if (isDefined(this.centered)) {
            this.options.centered = this.centered;
        }
        if (isDefined(this.container)) {
            this.options.container = this.container;
        }
        if (isDefined(this.keyboard)) {
            this.options.keyboard = this.keyboard;
        }
        if (isDefined(this.size)) {
            this.options.size = this.size;
        }
        if (isDefined(this.scrollableContent)) {
            this.options.scrollableContent = this.scrollableContent;
        }
        if (isDefined(this.windowClass)) {
            this.options.windowClass = this.windowClass;
        }
        if (isDefined(this.backdropClass)) {
            this.options.backdropClass = this.backdropClass;
        }
        // apply events
        this.activeModal.modalOpeningDidStart.subscribe((/**
         * @return {?}
         */
        function () { return _this.modalOpeningDidStart.next(); }));
        this.activeModal.modalOpeningDidDone.subscribe((/**
         * @return {?}
         */
        function () { return _this.modalOpeningDidDone.next(); }));
        this.activeModal.modalClosingDidStart.subscribe((/**
         * @return {?}
         */
        function () { return _this.modalClosingDidStart.next(); }));
        this.activeModal.modalClosingDidDone.subscribe((/**
         * @return {?}
         */
        function () { return _this.modalClosingDidDone.next(); }));
        this.activeModal.backdropOpeningDidStart.subscribe((/**
         * @return {?}
         */
        function () { return _this.backdropOpeningDidStart.next(); }));
        this.activeModal.backdropOpeningDidDone.subscribe((/**
         * @return {?}
         */
        function () { return _this.backdropOpeningDidDone.next(); }));
        this.activeModal.backdropClosingDidStart.subscribe((/**
         * @return {?}
         */
        function () { return _this.backdropClosingDidStart.next(); }));
        this.activeModal.backdropClosingDidDone.subscribe((/**
         * @return {?}
         */
        function () { return _this.backdropClosingDidDone.next(); }));
        if (!this.id) {
            // console.error('modal must have an id');
            return;
        }
        this._modalService.add(this);
    };
    /**
     * @return {?}
     */
    NgtModalComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this._modalService.remove(this.id);
    };
    /**
     * Open the modal with an modal component reference without id.
     */
    /**
     * Open the modal with an modal component reference without id.
     * @return {?}
     */
    NgtModalComponent.prototype.open = /**
     * Open the modal with an modal component reference without id.
     * @return {?}
     */
    function () {
        this._modalService.open(this, this.options);
    };
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     */
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    NgtModalComponent.prototype.close = /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    function (result) {
        this.activeModal.close(result);
    };
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     */
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    NgtModalComponent.prototype.dismiss = /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    function (reason) {
        this.activeModal.dismiss(reason);
    };
    NgtModalComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-modal',
                    template: "\n        <ng-content></ng-content>\n    ",
                    providers: [NgtActiveModal]
                }] }
    ];
    /** @nocollapse */
    NgtModalComponent.ctorParameters = function () { return [
        { type: NgtActiveModal },
        { type: NgtModalService },
        { type: NgtModalConfig },
        { type: ElementRef }
    ]; };
    NgtModalComponent.propDecorators = {
        id: [{ type: Input }],
        backdrop: [{ type: Input }],
        beforeDismiss: [{ type: Input }],
        centered: [{ type: Input }],
        container: [{ type: Input }],
        keyboard: [{ type: Input }],
        size: [{ type: Input }],
        scrollableContent: [{ type: Input }],
        windowClass: [{ type: Input }],
        backdropClass: [{ type: Input }],
        modalClosingDidStart: [{ type: Output }],
        modalClosingDidDone: [{ type: Output }],
        modalOpeningDidStart: [{ type: Output }],
        modalOpeningDidDone: [{ type: Output }],
        backdropClosingDidStart: [{ type: Output }],
        backdropClosingDidDone: [{ type: Output }],
        backdropOpeningDidStart: [{ type: Output }],
        backdropOpeningDidDone: [{ type: Output }]
    };
    return NgtModalComponent;
}());
export { NgtModalComponent };
if (false) {
    /** @type {?} */
    NgtModalComponent.prototype.id;
    /** @type {?} */
    NgtModalComponent.prototype.backdrop;
    /** @type {?} */
    NgtModalComponent.prototype.beforeDismiss;
    /** @type {?} */
    NgtModalComponent.prototype.centered;
    /** @type {?} */
    NgtModalComponent.prototype.container;
    /** @type {?} */
    NgtModalComponent.prototype.keyboard;
    /** @type {?} */
    NgtModalComponent.prototype.size;
    /** @type {?} */
    NgtModalComponent.prototype.scrollableContent;
    /** @type {?} */
    NgtModalComponent.prototype.windowClass;
    /** @type {?} */
    NgtModalComponent.prototype.backdropClass;
    /** @type {?} */
    NgtModalComponent.prototype.modalClosingDidStart;
    /** @type {?} */
    NgtModalComponent.prototype.modalClosingDidDone;
    /** @type {?} */
    NgtModalComponent.prototype.modalOpeningDidStart;
    /** @type {?} */
    NgtModalComponent.prototype.modalOpeningDidDone;
    /** @type {?} */
    NgtModalComponent.prototype.backdropClosingDidStart;
    /** @type {?} */
    NgtModalComponent.prototype.backdropClosingDidDone;
    /** @type {?} */
    NgtModalComponent.prototype.backdropOpeningDidStart;
    /** @type {?} */
    NgtModalComponent.prototype.backdropOpeningDidDone;
    /** @type {?} */
    NgtModalComponent.prototype.options;
    /** @type {?} */
    NgtModalComponent.prototype.activeModal;
    /**
     * @type {?}
     * @private
     */
    NgtModalComponent.prototype._modalService;
    /**
     * @type {?}
     * @private
     */
    NgtModalComponent.prototype._config;
    /**
     * @type {?}
     * @private
     */
    NgtModalComponent.prototype._elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJtb2RhbC9tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDSCxTQUFTLEVBQ1QsS0FBSyxFQUdMLFVBQVUsRUFDVixNQUFNLEVBQ1QsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUUvQixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDbEQsT0FBTyxFQUFFLGNBQWMsRUFBbUIsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzdDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFFekM7SUE2QkksMkJBQW1CLFdBQTJCLEVBQzFCLGFBQThCLEVBQzlCLE9BQXVCLEVBQ3ZCLE1BQWtCO1FBSG5CLGdCQUFXLEdBQVgsV0FBVyxDQUFnQjtRQUMxQixrQkFBYSxHQUFiLGFBQWEsQ0FBaUI7UUFDOUIsWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFDdkIsV0FBTSxHQUFOLE1BQU0sQ0FBWTtRQWI1Qix5QkFBb0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3JDLHdCQUFtQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDcEMseUJBQW9CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNyQyx3QkFBbUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3BDLDRCQUF1QixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDeEMsMkJBQXNCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUN2Qyw0QkFBdUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3hDLDJCQUFzQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDakQsWUFBTyxHQUFvQixFQUFFLENBQUM7SUFNOUIsQ0FBQzs7OztJQUVELG9DQUFROzs7SUFBUjtRQUFBLGlCQTZDQztRQTVDRyxjQUFjO1FBQ2QsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7U0FDekM7UUFDRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDL0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztTQUNuRDtRQUNELElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1NBQ3pDO1FBQ0QsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQzNCLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7U0FDM0M7UUFDRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztTQUN6QztRQUNELElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN0QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2pDO1FBQ0QsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUU7WUFDbkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7U0FDM0Q7UUFDRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztTQUMvQztRQUNELElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUMvQixJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1NBQ25EO1FBRUQsZUFBZTtRQUNmLElBQUksQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsRUFBaEMsQ0FBZ0MsRUFBQyxDQUFDO1FBQ3hGLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsRUFBL0IsQ0FBK0IsRUFBQyxDQUFDO1FBQ3RGLElBQUksQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsRUFBaEMsQ0FBZ0MsRUFBQyxDQUFDO1FBQ3hGLElBQUksQ0FBQyxXQUFXLENBQUMsbUJBQW1CLENBQUMsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsRUFBL0IsQ0FBK0IsRUFBQyxDQUFDO1FBQ3RGLElBQUksQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsRUFBbkMsQ0FBbUMsRUFBQyxDQUFDO1FBQzlGLElBQUksQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsRUFBbEMsQ0FBa0MsRUFBQyxDQUFDO1FBQzVGLElBQUksQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsRUFBbkMsQ0FBbUMsRUFBQyxDQUFDO1FBQzlGLElBQUksQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsRUFBbEMsQ0FBa0MsRUFBQyxDQUFDO1FBRTVGLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ1YsMENBQTBDO1lBQzFDLE9BQU87U0FDVjtRQUNELElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7SUFFRCx1Q0FBVzs7O0lBQVg7UUFDSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILGdDQUFJOzs7O0lBQUo7UUFDSSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRDs7O09BR0c7Ozs7Ozs7SUFDSCxpQ0FBSzs7Ozs7O0lBQUwsVUFBTSxNQUFZO1FBQ2QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7OztJQUNILG1DQUFPOzs7Ozs7SUFBUCxVQUFRLE1BQVk7UUFDaEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDckMsQ0FBQzs7Z0JBM0dKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsV0FBVztvQkFDckIsUUFBUSxFQUFFLDJDQUVUO29CQUNELFNBQVMsRUFBRSxDQUFDLGNBQWMsQ0FBQztpQkFDOUI7Ozs7Z0JBVFEsY0FBYztnQkFGZCxlQUFlO2dCQUNmLGNBQWM7Z0JBTm5CLFVBQVU7OztxQkFtQlQsS0FBSzsyQkFDTCxLQUFLO2dDQUNMLEtBQUs7MkJBQ0wsS0FBSzs0QkFDTCxLQUFLOzJCQUNMLEtBQUs7dUJBQ0wsS0FBSztvQ0FDTCxLQUFLOzhCQUNMLEtBQUs7Z0NBQ0wsS0FBSzt1Q0FDTCxNQUFNO3NDQUNOLE1BQU07dUNBQ04sTUFBTTtzQ0FDTixNQUFNOzBDQUNOLE1BQU07eUNBQ04sTUFBTTswQ0FDTixNQUFNO3lDQUNOLE1BQU07O0lBa0ZYLHdCQUFDO0NBQUEsQUE1R0QsSUE0R0M7U0FwR1ksaUJBQWlCOzs7SUFDMUIsK0JBQW9COztJQUNwQixxQ0FBc0M7O0lBQ3RDLDBDQUF5RDs7SUFDekQscUNBQTJCOztJQUMzQixzQ0FBMkI7O0lBQzNCLHFDQUEyQjs7SUFDM0IsaUNBQTJCOztJQUMzQiw4Q0FBb0M7O0lBQ3BDLHdDQUE2Qjs7SUFDN0IsMENBQStCOztJQUMvQixpREFBK0M7O0lBQy9DLGdEQUE4Qzs7SUFDOUMsaURBQStDOztJQUMvQyxnREFBOEM7O0lBQzlDLG9EQUFrRDs7SUFDbEQsbURBQWlEOztJQUNqRCxvREFBa0Q7O0lBQ2xELG1EQUFpRDs7SUFDakQsb0NBQThCOztJQUVsQix3Q0FBa0M7Ozs7O0lBQ2xDLDBDQUFzQzs7Ozs7SUFDdEMsb0NBQStCOzs7OztJQUMvQixtQ0FBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgSW5wdXQsXHJcbiAgICBPbkluaXQsXHJcbiAgICBPbkRlc3Ryb3ksXHJcbiAgICBFbGVtZW50UmVmLFxyXG4gICAgT3V0cHV0XHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IE5ndE1vZGFsU2VydmljZSB9IGZyb20gJy4vbW9kYWwuc2VydmljZSc7XHJcbmltcG9ydCB7IE5ndE1vZGFsQ29uZmlnLCBOZ3RNb2RhbE9wdGlvbnMgfSBmcm9tICcuL21vZGFsLWNvbmZpZyc7XHJcbmltcG9ydCB7IE5ndEFjdGl2ZU1vZGFsIH0gZnJvbSAnLi9tb2RhbC1yZWYnO1xyXG5pbXBvcnQgeyBpc0RlZmluZWQgfSBmcm9tICcuLi91dGlsL3V0aWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC1tb2RhbCcsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuICAgIGAsXHJcbiAgICBwcm92aWRlcnM6IFtOZ3RBY3RpdmVNb2RhbF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBOZ3RNb2RhbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIEBJbnB1dCgpIGlkOiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKSBiYWNrZHJvcDogYm9vbGVhbiB8ICdzdGF0aWMnO1xyXG4gICAgQElucHV0KCkgYmVmb3JlRGlzbWlzczogKCkgPT4gYm9vbGVhbiB8IFByb21pc2U8Ym9vbGVhbj47XHJcbiAgICBASW5wdXQoKSBjZW50ZXJlZDogYm9vbGVhbjtcclxuICAgIEBJbnB1dCgpIGNvbnRhaW5lcjogc3RyaW5nO1xyXG4gICAgQElucHV0KCkga2V5Ym9hcmQ6IGJvb2xlYW47XHJcbiAgICBASW5wdXQoKSBzaXplOiAnc20nIHwgJ2xnJztcclxuICAgIEBJbnB1dCgpIHNjcm9sbGFibGVDb250ZW50OiBib29sZWFuO1xyXG4gICAgQElucHV0KCkgd2luZG93Q2xhc3M6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIGJhY2tkcm9wQ2xhc3M6IHN0cmluZztcclxuICAgIEBPdXRwdXQoKSBtb2RhbENsb3NpbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBAT3V0cHV0KCkgbW9kYWxDbG9zaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBAT3V0cHV0KCkgbW9kYWxPcGVuaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgQE91dHB1dCgpIG1vZGFsT3BlbmluZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgQE91dHB1dCgpIGJhY2tkcm9wQ2xvc2luZ0RpZFN0YXJ0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIEBPdXRwdXQoKSBiYWNrZHJvcENsb3NpbmdEaWREb25lID0gbmV3IFN1YmplY3QoKTtcclxuICAgIEBPdXRwdXQoKSBiYWNrZHJvcE9wZW5pbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBAT3V0cHV0KCkgYmFja2Ryb3BPcGVuaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBvcHRpb25zOiBOZ3RNb2RhbE9wdGlvbnMgPSB7fTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgYWN0aXZlTW9kYWw6IE5ndEFjdGl2ZU1vZGFsLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfbW9kYWxTZXJ2aWNlOiBOZ3RNb2RhbFNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9jb25maWc6IE5ndE1vZGFsQ29uZmlnLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfZWxSZWY6IEVsZW1lbnRSZWYpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICAvLyBzZXQgb3B0aW9uc1xyXG4gICAgICAgIGlmIChpc0RlZmluZWQodGhpcy5iYWNrZHJvcCkpIHtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zLmJhY2tkcm9wID0gdGhpcy5iYWNrZHJvcDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGlzRGVmaW5lZCh0aGlzLmJlZm9yZURpc21pc3MpKSB7XHJcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5iZWZvcmVEaXNtaXNzID0gdGhpcy5iZWZvcmVEaXNtaXNzO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoaXNEZWZpbmVkKHRoaXMuY2VudGVyZWQpKSB7XHJcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5jZW50ZXJlZCA9IHRoaXMuY2VudGVyZWQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChpc0RlZmluZWQodGhpcy5jb250YWluZXIpKSB7XHJcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5jb250YWluZXIgPSB0aGlzLmNvbnRhaW5lcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGlzRGVmaW5lZCh0aGlzLmtleWJvYXJkKSkge1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMua2V5Ym9hcmQgPSB0aGlzLmtleWJvYXJkO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoaXNEZWZpbmVkKHRoaXMuc2l6ZSkpIHtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zLnNpemUgPSB0aGlzLnNpemU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChpc0RlZmluZWQodGhpcy5zY3JvbGxhYmxlQ29udGVudCkpIHtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zLnNjcm9sbGFibGVDb250ZW50ID0gdGhpcy5zY3JvbGxhYmxlQ29udGVudDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGlzRGVmaW5lZCh0aGlzLndpbmRvd0NsYXNzKSkge1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMud2luZG93Q2xhc3MgPSB0aGlzLndpbmRvd0NsYXNzO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoaXNEZWZpbmVkKHRoaXMuYmFja2Ryb3BDbGFzcykpIHtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zLmJhY2tkcm9wQ2xhc3MgPSB0aGlzLmJhY2tkcm9wQ2xhc3M7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBhcHBseSBldmVudHNcclxuICAgICAgICB0aGlzLmFjdGl2ZU1vZGFsLm1vZGFsT3BlbmluZ0RpZFN0YXJ0LnN1YnNjcmliZSgoKSA9PiB0aGlzLm1vZGFsT3BlbmluZ0RpZFN0YXJ0Lm5leHQoKSk7XHJcbiAgICAgICAgdGhpcy5hY3RpdmVNb2RhbC5tb2RhbE9wZW5pbmdEaWREb25lLnN1YnNjcmliZSgoKSA9PiB0aGlzLm1vZGFsT3BlbmluZ0RpZERvbmUubmV4dCgpKTtcclxuICAgICAgICB0aGlzLmFjdGl2ZU1vZGFsLm1vZGFsQ2xvc2luZ0RpZFN0YXJ0LnN1YnNjcmliZSgoKSA9PiB0aGlzLm1vZGFsQ2xvc2luZ0RpZFN0YXJ0Lm5leHQoKSk7XHJcbiAgICAgICAgdGhpcy5hY3RpdmVNb2RhbC5tb2RhbENsb3NpbmdEaWREb25lLnN1YnNjcmliZSgoKSA9PiB0aGlzLm1vZGFsQ2xvc2luZ0RpZERvbmUubmV4dCgpKTtcclxuICAgICAgICB0aGlzLmFjdGl2ZU1vZGFsLmJhY2tkcm9wT3BlbmluZ0RpZFN0YXJ0LnN1YnNjcmliZSgoKSA9PiB0aGlzLmJhY2tkcm9wT3BlbmluZ0RpZFN0YXJ0Lm5leHQoKSk7XHJcbiAgICAgICAgdGhpcy5hY3RpdmVNb2RhbC5iYWNrZHJvcE9wZW5pbmdEaWREb25lLnN1YnNjcmliZSgoKSA9PiB0aGlzLmJhY2tkcm9wT3BlbmluZ0RpZERvbmUubmV4dCgpKTtcclxuICAgICAgICB0aGlzLmFjdGl2ZU1vZGFsLmJhY2tkcm9wQ2xvc2luZ0RpZFN0YXJ0LnN1YnNjcmliZSgoKSA9PiB0aGlzLmJhY2tkcm9wQ2xvc2luZ0RpZFN0YXJ0Lm5leHQoKSk7XHJcbiAgICAgICAgdGhpcy5hY3RpdmVNb2RhbC5iYWNrZHJvcENsb3NpbmdEaWREb25lLnN1YnNjcmliZSgoKSA9PiB0aGlzLmJhY2tkcm9wQ2xvc2luZ0RpZERvbmUubmV4dCgpKTtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLmlkKSB7XHJcbiAgICAgICAgICAgIC8vIGNvbnNvbGUuZXJyb3IoJ21vZGFsIG11c3QgaGF2ZSBhbiBpZCcpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuX21vZGFsU2VydmljZS5hZGQodGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fbW9kYWxTZXJ2aWNlLnJlbW92ZSh0aGlzLmlkKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE9wZW4gdGhlIG1vZGFsIHdpdGggYW4gbW9kYWwgY29tcG9uZW50IHJlZmVyZW5jZSB3aXRob3V0IGlkLlxyXG4gICAgICovXHJcbiAgICBvcGVuKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX21vZGFsU2VydmljZS5vcGVuKHRoaXMsIHRoaXMub3B0aW9ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDbG9zZXMgdGhlIG1vZGFsIHdpdGggYW4gb3B0aW9uYWwgJ3Jlc3VsdCcgdmFsdWUuXHJcbiAgICAgKiBUaGUgJ05ndE1vYmFsUmVmLnJlc3VsdCcgcHJvbWlzZSB3aWxsIGJlIHJlc29sdmVkIHdpdGggcHJvdmlkZWQgdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIGNsb3NlKHJlc3VsdD86IGFueSk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuYWN0aXZlTW9kYWwuY2xvc2UocmVzdWx0KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERpc21pc3NlcyB0aGUgbW9kYWwgd2l0aCBhbiBvcHRpb25hbCAncmVhc29uJyB2YWx1ZS5cclxuICAgICAqIFRoZSAnTmd0TW9kYWxSZWYucmVzdWx0JyBwcm9taXNlIHdpbGwgYmUgcmVqZWN0ZWQgd2l0aCBwcm92aWRlZCB2YWx1ZS5cclxuICAgICAqL1xyXG4gICAgZGlzbWlzcyhyZWFzb24/OiBhbnkpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmFjdGl2ZU1vZGFsLmRpc21pc3MocmVhc29uKTtcclxuICAgIH1cclxufVxyXG4iXX0=