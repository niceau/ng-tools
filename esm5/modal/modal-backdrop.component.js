/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, HostBinding, HostListener, Input, Output } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Subject } from 'rxjs';
var NgtModalBackdropComponent = /** @class */ (function () {
    function NgtModalBackdropComponent() {
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.zIndex = '1050';
        this.animation = 'start';
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtModalBackdropComponent.prototype.onAnimationStart = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.animationAction($event);
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtModalBackdropComponent.prototype.onAnimationDone = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.animationAction($event);
    };
    /**
     * @return {?}
     */
    NgtModalBackdropComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.class = 'modal-backdrop fade show' + (this.backdropClass ? ' ' + this.backdropClass : '');
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtModalBackdropComponent.prototype.animationAction = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        switch ($event.phaseName) {
            case 'start':
                switch ($event.toState) {
                    case 'start':
                        this.backdropOpeningDidStart.next();
                        break;
                    case 'close':
                        this.backdropClosingDidStart.next();
                        break;
                }
                break;
            case 'done':
                switch ($event.toState) {
                    case 'start':
                        this.backdropOpeningDidDone.next();
                        break;
                    case 'close':
                        this.backdropClosingDidDone.next();
                        break;
                }
                break;
        }
    };
    NgtModalBackdropComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-modal-backdrop',
                    template: '',
                    animations: [
                        trigger('animation', [
                            state('close', style({ opacity: 0 })),
                            transition('void => *', [
                                style({ opacity: 0 }),
                                animate(200)
                            ]),
                            transition('* => void', [
                                animate(200, style({ opacity: 0 }))
                            ]),
                            transition('* => close', animate('0.3s'))
                        ])
                    ]
                }] }
    ];
    NgtModalBackdropComponent.propDecorators = {
        backdropClass: [{ type: Input }],
        backdropClosingDidStart: [{ type: Output, args: ['backdropClosingDidStart',] }],
        backdropClosingDidDone: [{ type: Output, args: ['backdropClosingDidDone',] }],
        backdropOpeningDidStart: [{ type: Output, args: ['backdropOpeningDidStart',] }],
        backdropOpeningDidDone: [{ type: Output, args: ['backdropOpeningDidDone',] }],
        class: [{ type: HostBinding, args: ['class',] }],
        zIndex: [{ type: HostBinding, args: ['style.z-index',] }],
        animation: [{ type: HostBinding, args: ['@animation',] }],
        onAnimationStart: [{ type: HostListener, args: ['@animation.start', ['$event'],] }],
        onAnimationDone: [{ type: HostListener, args: ['@animation.done', ['$event'],] }]
    };
    return NgtModalBackdropComponent;
}());
export { NgtModalBackdropComponent };
if (false) {
    /** @type {?} */
    NgtModalBackdropComponent.prototype.backdropClass;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.backdropClosingDidStart;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.backdropClosingDidDone;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.backdropOpeningDidStart;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.backdropOpeningDidDone;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.class;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.zIndex;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.animation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtYmFja2Ryb3AuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJtb2RhbC9tb2RhbC1iYWNrZHJvcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzVGLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDakYsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUUvQjtJQUFBO1FBbUJ1Qyw0QkFBdUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3pDLDJCQUFzQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDdEMsNEJBQXVCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUN6QywyQkFBc0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBRTNDLFdBQU0sR0FBRyxNQUFNLENBQUM7UUFDbkIsY0FBUyxHQUFHLE9BQU8sQ0FBQztJQXNDbkQsQ0FBQzs7Ozs7SUFwQ2lELG9EQUFnQjs7OztJQUE5RCxVQUErRCxNQUFNO1FBQ2pFLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFNEMsbURBQWU7Ozs7SUFBNUQsVUFBNkQsTUFBTTtRQUMvRCxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7SUFFRCw0Q0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLDBCQUEwQixHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ25HLENBQUM7Ozs7O0lBRUQsbURBQWU7Ozs7SUFBZixVQUFnQixNQUFNO1FBQ2xCLFFBQVEsTUFBTSxDQUFDLFNBQVMsRUFBRTtZQUN0QixLQUFLLE9BQU87Z0JBQ1IsUUFBUSxNQUFNLENBQUMsT0FBTyxFQUFFO29CQUNwQixLQUFLLE9BQU87d0JBQ1IsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxDQUFDO3dCQUNwQyxNQUFNO29CQUNWLEtBQUssT0FBTzt3QkFDUixJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQ3BDLE1BQU07aUJBQ2I7Z0JBQ0QsTUFBTTtZQUNWLEtBQUssTUFBTTtnQkFDUCxRQUFRLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0JBQ3BCLEtBQUssT0FBTzt3QkFDUixJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQ25DLE1BQU07b0JBQ1YsS0FBSyxPQUFPO3dCQUNSLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDbkMsTUFBTTtpQkFDYjtnQkFDRCxNQUFNO1NBQ2I7SUFDTCxDQUFDOztnQkE5REosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLFFBQVEsRUFBRSxFQUFFO29CQUNaLFVBQVUsRUFBRTt3QkFDUixPQUFPLENBQUMsV0FBVyxFQUFFOzRCQUNqQixLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxFQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDOzRCQUNuQyxVQUFVLENBQUMsV0FBVyxFQUFFO2dDQUNwQixLQUFLLENBQUMsRUFBQyxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUM7Z0NBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUM7NkJBQ2YsQ0FBQzs0QkFDRixVQUFVLENBQUMsV0FBVyxFQUFFO2dDQUNwQixPQUFPLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxFQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDOzZCQUNwQyxDQUFDOzRCQUNGLFVBQVUsQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO3lCQUM1QyxDQUFDO3FCQUNMO2lCQUNKOzs7Z0NBRUksS0FBSzswQ0FDTCxNQUFNLFNBQUMseUJBQXlCO3lDQUNoQyxNQUFNLFNBQUMsd0JBQXdCOzBDQUMvQixNQUFNLFNBQUMseUJBQXlCO3lDQUNoQyxNQUFNLFNBQUMsd0JBQXdCO3dCQUMvQixXQUFXLFNBQUMsT0FBTzt5QkFDbkIsV0FBVyxTQUFDLGVBQWU7NEJBQzNCLFdBQVcsU0FBQyxZQUFZO21DQUV4QixZQUFZLFNBQUMsa0JBQWtCLEVBQUUsQ0FBQyxRQUFRLENBQUM7a0NBSTNDLFlBQVksU0FBQyxpQkFBaUIsRUFBRSxDQUFDLFFBQVEsQ0FBQzs7SUFnQy9DLGdDQUFDO0NBQUEsQUEvREQsSUErREM7U0E5Q1kseUJBQXlCOzs7SUFDbEMsa0RBQStCOztJQUMvQiw0REFBMkU7O0lBQzNFLDJEQUF5RTs7SUFDekUsNERBQTJFOztJQUMzRSwyREFBeUU7O0lBQ3pFLDBDQUE0Qjs7SUFDNUIsMkNBQThDOztJQUM5Qyw4Q0FBK0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEhvc3RCaW5kaW5nLCBIb3N0TGlzdGVuZXIsIElucHV0LCBPbkluaXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBhbmltYXRlLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIHRyaWdnZXIgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC1tb2RhbC1iYWNrZHJvcCcsXHJcbiAgICB0ZW1wbGF0ZTogJycsXHJcbiAgICBhbmltYXRpb25zOiBbXHJcbiAgICAgICAgdHJpZ2dlcignYW5pbWF0aW9uJywgW1xyXG4gICAgICAgICAgICBzdGF0ZSgnY2xvc2UnLCBzdHlsZSh7b3BhY2l0eTogMH0pKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbigndm9pZCA9PiAqJywgW1xyXG4gICAgICAgICAgICAgICAgc3R5bGUoe29wYWNpdHk6IDB9KSxcclxuICAgICAgICAgICAgICAgIGFuaW1hdGUoMjAwKVxyXG4gICAgICAgICAgICBdKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbignKiA9PiB2b2lkJywgW1xyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZSgyMDAsIHN0eWxlKHtvcGFjaXR5OiAwfSkpXHJcbiAgICAgICAgICAgIF0pLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCcqID0+IGNsb3NlJywgYW5pbWF0ZSgnMC4zcycpKVxyXG4gICAgICAgIF0pXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RNb2RhbEJhY2tkcm9wQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgpIGJhY2tkcm9wQ2xhc3M6IHN0cmluZztcclxuICAgIEBPdXRwdXQoJ2JhY2tkcm9wQ2xvc2luZ0RpZFN0YXJ0JykgYmFja2Ryb3BDbG9zaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgQE91dHB1dCgnYmFja2Ryb3BDbG9zaW5nRGlkRG9uZScpIGJhY2tkcm9wQ2xvc2luZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgQE91dHB1dCgnYmFja2Ryb3BPcGVuaW5nRGlkU3RhcnQnKSBiYWNrZHJvcE9wZW5pbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBAT3V0cHV0KCdiYWNrZHJvcE9wZW5pbmdEaWREb25lJykgYmFja2Ryb3BPcGVuaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzJykgY2xhc3M7XHJcbiAgICBASG9zdEJpbmRpbmcoJ3N0eWxlLnotaW5kZXgnKSB6SW5kZXggPSAnMTA1MCc7XHJcbiAgICBASG9zdEJpbmRpbmcoJ0BhbmltYXRpb24nKSBhbmltYXRpb24gPSAnc3RhcnQnO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ0BhbmltYXRpb24uc3RhcnQnLCBbJyRldmVudCddKSBvbkFuaW1hdGlvblN0YXJ0KCRldmVudCkge1xyXG4gICAgICAgIHRoaXMuYW5pbWF0aW9uQWN0aW9uKCRldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignQGFuaW1hdGlvbi5kb25lJywgWyckZXZlbnQnXSkgb25BbmltYXRpb25Eb25lKCRldmVudCkge1xyXG4gICAgICAgIHRoaXMuYW5pbWF0aW9uQWN0aW9uKCRldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jbGFzcyA9ICdtb2RhbC1iYWNrZHJvcCBmYWRlIHNob3cnICsgKHRoaXMuYmFja2Ryb3BDbGFzcyA/ICcgJyArIHRoaXMuYmFja2Ryb3BDbGFzcyA6ICcnKTtcclxuICAgIH1cclxuXHJcbiAgICBhbmltYXRpb25BY3Rpb24oJGV2ZW50KSB7XHJcbiAgICAgICAgc3dpdGNoICgkZXZlbnQucGhhc2VOYW1lKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ3N0YXJ0JzpcclxuICAgICAgICAgICAgICAgIHN3aXRjaCAoJGV2ZW50LnRvU3RhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdzdGFydCc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYmFja2Ryb3BPcGVuaW5nRGlkU3RhcnQubmV4dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdjbG9zZSc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYmFja2Ryb3BDbG9zaW5nRGlkU3RhcnQubmV4dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdkb25lJzpcclxuICAgICAgICAgICAgICAgIHN3aXRjaCAoJGV2ZW50LnRvU3RhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdzdGFydCc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYmFja2Ryb3BPcGVuaW5nRGlkRG9uZS5uZXh0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2Nsb3NlJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5iYWNrZHJvcENsb3NpbmdEaWREb25lLm5leHQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19