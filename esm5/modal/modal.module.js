/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtModalService } from './modal.service';
import { NgtModalComponent } from './modal.component';
import { NgtModalBackdropComponent } from './modal-backdrop.component';
import { NgtModalOpenDirective } from './modal-open.directive';
import { NgtModalCloseDirective } from './modal-close.directive';
import { NgtModalDismissDirective } from './modal-dismiss.directive';
import { NgtModalWindowComponent } from './modal-window.component';
export { NgtModalService } from './modal.service';
export { NgtModalRef, NgtActiveModal } from './modal-ref';
export { NgtModalComponent } from './modal.component';
export { NgtModalOpenDirective } from './modal-open.directive';
export { NgtModalCloseDirective } from './modal-close.directive';
export { NgtModalDismissDirective } from './modal-dismiss.directive';
export { NgtModalConfig } from './modal-config';
export { ModalDismissReasons } from './modal-dismiss-reasons';
/** @type {?} */
var NGC_MODAL_DIRECTIVES = [
    NgtModalOpenDirective,
    NgtModalCloseDirective,
    NgtModalDismissDirective,
    NgtModalWindowComponent,
    NgtModalBackdropComponent,
    NgtModalComponent
];
var NgtModalModule = /** @class */ (function () {
    function NgtModalModule() {
    }
    /**
     * @return {?}
     */
    NgtModalModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtModalModule };
    };
    NgtModalModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [NGC_MODAL_DIRECTIVES],
                    exports: [NGC_MODAL_DIRECTIVES],
                    imports: [CommonModule],
                    providers: [NgtModalService],
                    entryComponents: [NgtModalComponent, NgtModalWindowComponent, NgtModalBackdropComponent]
                },] }
    ];
    return NgtModalModule;
}());
export { NgtModalModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJtb2RhbC9tb2RhbC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDbEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDdEQsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDdkUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDL0QsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDakUsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDckUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFHbkUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxXQUFXLEVBQUUsY0FBYyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzFELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ2pFLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3JFLE9BQU8sRUFBRSxjQUFjLEVBQW1CLE1BQU0sZ0JBQWdCLENBQUM7QUFDakUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUJBQXlCLENBQUM7O0lBRXhELG9CQUFvQixHQUFHO0lBQ3pCLHFCQUFxQjtJQUNyQixzQkFBc0I7SUFDdEIsd0JBQXdCO0lBQ3hCLHVCQUF1QjtJQUN2Qix5QkFBeUI7SUFDekIsaUJBQWlCO0NBQ3BCO0FBRUQ7SUFBQTtJQVdBLENBQUM7Ozs7SUFIVSxzQkFBTzs7O0lBQWQ7UUFDSSxPQUFPLEVBQUMsUUFBUSxFQUFFLGNBQWMsRUFBQyxDQUFDO0lBQ3RDLENBQUM7O2dCQVZKLFFBQVEsU0FBQztvQkFDTixZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztvQkFDcEMsT0FBTyxFQUFFLENBQUMsb0JBQW9CLENBQUM7b0JBQy9CLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztvQkFDdkIsU0FBUyxFQUFFLENBQUMsZUFBZSxDQUFDO29CQUM1QixlQUFlLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSx1QkFBdUIsRUFBRSx5QkFBeUIsQ0FBQztpQkFDM0Y7O0lBS0QscUJBQUM7Q0FBQSxBQVhELElBV0M7U0FKWSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbmltcG9ydCB7IE5ndE1vZGFsU2VydmljZSB9IGZyb20gJy4vbW9kYWwuc2VydmljZSc7XHJcbmltcG9ydCB7IE5ndE1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9tb2RhbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBOZ3RNb2RhbEJhY2tkcm9wQ29tcG9uZW50IH0gZnJvbSAnLi9tb2RhbC1iYWNrZHJvcC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBOZ3RNb2RhbE9wZW5EaXJlY3RpdmUgfSBmcm9tICcuL21vZGFsLW9wZW4uZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgTmd0TW9kYWxDbG9zZURpcmVjdGl2ZSB9IGZyb20gJy4vbW9kYWwtY2xvc2UuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgTmd0TW9kYWxEaXNtaXNzRGlyZWN0aXZlIH0gZnJvbSAnLi9tb2RhbC1kaXNtaXNzLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IE5ndE1vZGFsV2luZG93Q29tcG9uZW50IH0gZnJvbSAnLi9tb2RhbC13aW5kb3cuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTmd0QWN0aXZlTW9kYWwgfSBmcm9tICcuL21vZGFsLXJlZic7XHJcblxyXG5leHBvcnQgeyBOZ3RNb2RhbFNlcnZpY2UgfSBmcm9tICcuL21vZGFsLnNlcnZpY2UnO1xyXG5leHBvcnQgeyBOZ3RNb2RhbFJlZiwgTmd0QWN0aXZlTW9kYWwgfSBmcm9tICcuL21vZGFsLXJlZic7XHJcbmV4cG9ydCB7IE5ndE1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9tb2RhbC5jb21wb25lbnQnO1xyXG5leHBvcnQgeyBOZ3RNb2RhbE9wZW5EaXJlY3RpdmUgfSBmcm9tICcuL21vZGFsLW9wZW4uZGlyZWN0aXZlJztcclxuZXhwb3J0IHsgTmd0TW9kYWxDbG9zZURpcmVjdGl2ZSB9IGZyb20gJy4vbW9kYWwtY2xvc2UuZGlyZWN0aXZlJztcclxuZXhwb3J0IHsgTmd0TW9kYWxEaXNtaXNzRGlyZWN0aXZlIH0gZnJvbSAnLi9tb2RhbC1kaXNtaXNzLmRpcmVjdGl2ZSc7XHJcbmV4cG9ydCB7IE5ndE1vZGFsQ29uZmlnLCBOZ3RNb2RhbE9wdGlvbnMgfSBmcm9tICcuL21vZGFsLWNvbmZpZyc7XHJcbmV4cG9ydCB7IE1vZGFsRGlzbWlzc1JlYXNvbnMgfSBmcm9tICcuL21vZGFsLWRpc21pc3MtcmVhc29ucyc7XHJcblxyXG5jb25zdCBOR0NfTU9EQUxfRElSRUNUSVZFUyA9IFtcclxuICAgIE5ndE1vZGFsT3BlbkRpcmVjdGl2ZSxcclxuICAgIE5ndE1vZGFsQ2xvc2VEaXJlY3RpdmUsXHJcbiAgICBOZ3RNb2RhbERpc21pc3NEaXJlY3RpdmUsXHJcbiAgICBOZ3RNb2RhbFdpbmRvd0NvbXBvbmVudCxcclxuICAgIE5ndE1vZGFsQmFja2Ryb3BDb21wb25lbnQsXHJcbiAgICBOZ3RNb2RhbENvbXBvbmVudFxyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW05HQ19NT0RBTF9ESVJFQ1RJVkVTXSxcclxuICAgIGV4cG9ydHM6IFtOR0NfTU9EQUxfRElSRUNUSVZFU10sXHJcbiAgICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcclxuICAgIHByb3ZpZGVyczogW05ndE1vZGFsU2VydmljZV0sXHJcbiAgICBlbnRyeUNvbXBvbmVudHM6IFtOZ3RNb2RhbENvbXBvbmVudCwgTmd0TW9kYWxXaW5kb3dDb21wb25lbnQsIE5ndE1vZGFsQmFja2Ryb3BDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RNb2RhbE1vZHVsZSB7XHJcbiAgICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge25nTW9kdWxlOiBOZ3RNb2RhbE1vZHVsZX07XHJcbiAgICB9XHJcbn1cclxuIl19