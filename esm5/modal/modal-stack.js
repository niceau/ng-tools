/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ApplicationRef, Inject, Injectable, Injector, RendererFactory2, TemplateRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Subject } from 'rxjs';
import { NgtActiveModal, NgtModalRef } from './modal-ref';
import { ScrollBar } from '../util/scrollbar';
import { isDefined, isString } from '../util/util';
import { ngtFocusTrap } from '../util/focus-trap';
import { NgtModalWindowComponent } from './modal-window.component';
import { NgtModalBackdropComponent } from './modal-backdrop.component';
import { ContentRef } from '../util/popup';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "../util/scrollbar";
var NgtModalStack = /** @class */ (function () {
    function NgtModalStack(_applicationRef, _injector, _document, _scrollBar, _rendererFactory) {
        var _this = this;
        this._applicationRef = _applicationRef;
        this._injector = _injector;
        this._document = _document;
        this._scrollBar = _scrollBar;
        this._rendererFactory = _rendererFactory;
        this._activeWindowCmptHasChanged = new Subject();
        this._ariaHiddenValues = new Map();
        this._modalRefs = [];
        this._backdropAttributes = ['backdropClass'];
        this._windowAttributes = ['backdrop', 'centered', 'keyboard', 'size', 'scrollableContent', 'windowClass'];
        this._backdropEvents = ['backdropOpeningDidStart', 'backdropOpeningDidDone', 'backdropClosingDidStart', 'backdropClosingDidDone'];
        this._windowEvents = ['modalOpeningDidStart', 'modalOpeningDidDone', 'modalClosingDidStart', 'modalClosingDidDone'];
        this._windowCmpts = [];
        // Trap focus on active WindowCmpt
        this._activeWindowCmptHasChanged.subscribe((/**
         * @return {?}
         */
        function () {
            if (_this._windowCmpts.length) {
                /** @type {?} */
                var activeWindowCmpt = _this._windowCmpts[_this._windowCmpts.length - 1];
                ngtFocusTrap(activeWindowCmpt.location.nativeElement, _this._activeWindowCmptHasChanged);
                _this._revertAriaHidden();
                _this._setAriaHidden(activeWindowCmpt.location.nativeElement);
            }
        }));
    }
    /**
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @param {?} options
     * @return {?}
     */
    NgtModalStack.prototype.open = /**
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @param {?} options
     * @return {?}
     */
    function (moduleCFR, contentInjector, content, options) {
        var _this = this;
        /** @type {?} */
        var containerEl = isDefined(options.container) ? this._document.querySelector(options.container) : this._document.body;
        /** @type {?} */
        var renderer = this._rendererFactory.createRenderer(null, null);
        /** @type {?} */
        var revertPaddingForScrollBar = this._scrollBar.compensate();
        /** @type {?} */
        var removeBodyClass = (/**
         * @return {?}
         */
        function () {
            if (!_this._modalRefs.length) {
                renderer.removeClass(_this._document.body, 'modal-open');
            }
        });
        if (!containerEl) {
            throw new Error("The specified modal container \"" + (options.container || 'body') + "\" was not found in the DOM.");
        }
        var _a = this._getContentRef(moduleCFR, options.injector || contentInjector, content), contentRef = _a.contentRef, activeModal = _a.activeModal;
        /** @type {?} */
        var backdropCmptRef = options.backdrop !== false ? this._attachBackdrop(moduleCFR, containerEl) : null;
        /** @type {?} */
        var windowCmptRef = this._attachWindowComponent(moduleCFR, containerEl, contentRef);
        /** @type {?} */
        var ngtModalRef = new NgtModalRef(windowCmptRef, contentRef, backdropCmptRef, options.beforeDismiss);
        this._registerModalRef(ngtModalRef);
        this._registerWindowCmpt(windowCmptRef);
        ngtModalRef.result.then(revertPaddingForScrollBar, revertPaddingForScrollBar);
        ngtModalRef.result.then(removeBodyClass, removeBodyClass);
        activeModal.close = (/**
         * @param {?} result
         * @return {?}
         */
        function (result) {
            ngtModalRef.close(result);
        });
        activeModal.dismiss = (/**
         * @param {?} reason
         * @return {?}
         */
        function (reason) {
            ngtModalRef.dismiss(reason);
        });
        this._applyWindowOptions(windowCmptRef.instance, options);
        this._applyEvents(ngtModalRef, activeModal, this._windowEvents);
        if (this._modalRefs.length === 1) {
            renderer.addClass(this._document.body, 'modal-open');
        }
        if (backdropCmptRef && backdropCmptRef.instance) {
            this._applyBackdropOptions(backdropCmptRef.instance, options);
            this._applyEvents(ngtModalRef, activeModal, this._backdropEvents);
        }
        return ngtModalRef;
    };
    /**
     * @param {?=} reason
     * @return {?}
     */
    NgtModalStack.prototype.dismissAll = /**
     * @param {?=} reason
     * @return {?}
     */
    function (reason) {
        this._modalRefs.forEach((/**
         * @param {?} ngtModalRef
         * @return {?}
         */
        function (ngtModalRef) { return ngtModalRef.dismiss(reason); }));
    };
    /**
     * @return {?}
     */
    NgtModalStack.prototype.hasOpenModals = /**
     * @return {?}
     */
    function () {
        return this._modalRefs.length > 0;
    };
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @return {?}
     */
    NgtModalStack.prototype._attachBackdrop = /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @return {?}
     */
    function (moduleCFR, containerEl) {
        /** @type {?} */
        var backdropFactory = moduleCFR.resolveComponentFactory(NgtModalBackdropComponent);
        /** @type {?} */
        var backdropCmptRef = backdropFactory.create(this._injector);
        this._applicationRef.attachView(backdropCmptRef.hostView);
        containerEl.appendChild(backdropCmptRef.location.nativeElement);
        return backdropCmptRef;
    };
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @param {?} contentRef
     * @return {?}
     */
    NgtModalStack.prototype._attachWindowComponent = /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @param {?} contentRef
     * @return {?}
     */
    function (moduleCFR, containerEl, contentRef) {
        /** @type {?} */
        var windowFactory = moduleCFR.resolveComponentFactory(NgtModalWindowComponent);
        /** @type {?} */
        var windowCmptRef = windowFactory.create(this._injector, contentRef.nodes);
        this._applicationRef.attachView(windowCmptRef.hostView);
        containerEl.appendChild(windowCmptRef.location.nativeElement);
        return windowCmptRef;
    };
    /**
     * @private
     * @param {?} windowInstance
     * @param {?} options
     * @return {?}
     */
    NgtModalStack.prototype._applyWindowOptions = /**
     * @private
     * @param {?} windowInstance
     * @param {?} options
     * @return {?}
     */
    function (windowInstance, options) {
        this._windowAttributes.forEach((/**
         * @param {?} optionName
         * @return {?}
         */
        function (optionName) {
            if (isDefined(options[optionName])) {
                windowInstance[optionName] = options[optionName];
            }
        }));
    };
    /**
     * @private
     * @param {?} backdropInstance
     * @param {?} options
     * @return {?}
     */
    NgtModalStack.prototype._applyBackdropOptions = /**
     * @private
     * @param {?} backdropInstance
     * @param {?} options
     * @return {?}
     */
    function (backdropInstance, options) {
        this._backdropAttributes.forEach((/**
         * @param {?} optionName
         * @return {?}
         */
        function (optionName) {
            if (isDefined(options[optionName])) {
                backdropInstance[optionName] = options[optionName];
            }
        }));
    };
    /**
     * @private
     * @param {?} instanceToSubscribe
     * @param {?} instanceToTrigger
     * @param {?} events
     * @return {?}
     */
    NgtModalStack.prototype._applyEvents = /**
     * @private
     * @param {?} instanceToSubscribe
     * @param {?} instanceToTrigger
     * @param {?} events
     * @return {?}
     */
    function (instanceToSubscribe, instanceToTrigger, events) {
        events.forEach((/**
         * @param {?} eventName
         * @return {?}
         */
        function (eventName) {
            if (isDefined(instanceToSubscribe[eventName]) && isDefined(instanceToTrigger[eventName])) {
                instanceToSubscribe[eventName].subscribe((/**
                 * @return {?}
                 */
                function () { return instanceToTrigger[eventName].next(); }));
            }
        }));
    };
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @return {?}
     */
    NgtModalStack.prototype._getContentRef = /**
     * @private
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @return {?}
     */
    function (moduleCFR, contentInjector, content) {
        /** @type {?} */
        var activeModal = new NgtActiveModal();
        /** @type {?} */
        var contentRef;
        if (!content) {
            contentRef = new ContentRef([]);
        }
        else if (content instanceof TemplateRef) {
            contentRef = this._createFromTemplateRef(content, activeModal);
        }
        else if (isString(content)) {
            contentRef = this._createFromString(content);
        }
        else if (typeof content === 'function') {
            contentRef = this._createFromComponentConstructor(moduleCFR, contentInjector, content, activeModal);
        }
        else {
            contentRef = this._createFromComponentRef(content);
            activeModal = contentRef.componentRef.activeModal || activeModal;
        }
        return { contentRef: contentRef, activeModal: activeModal };
    };
    /**
     * @private
     * @param {?} content
     * @param {?} activeModal
     * @return {?}
     */
    NgtModalStack.prototype._createFromTemplateRef = /**
     * @private
     * @param {?} content
     * @param {?} activeModal
     * @return {?}
     */
    function (content, activeModal) {
        /** @type {?} */
        var context = {
            $implicit: activeModal,
            close: /**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                activeModal.close(result);
            },
            dismiss: /**
             * @param {?} reason
             * @return {?}
             */
            function (reason) {
                activeModal.dismiss(reason);
            }
        };
        /** @type {?} */
        var viewRef = content.createEmbeddedView(context);
        this._applicationRef.attachView(viewRef);
        return new ContentRef([viewRef.rootNodes], viewRef);
    };
    /**
     * @private
     * @param {?} content
     * @return {?}
     */
    NgtModalStack.prototype._createFromString = /**
     * @private
     * @param {?} content
     * @return {?}
     */
    function (content) {
        /** @type {?} */
        var component = this._document.createTextNode("" + content);
        return new ContentRef([[component]]);
    };
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @param {?} context
     * @return {?}
     */
    NgtModalStack.prototype._createFromComponentConstructor = /**
     * @private
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @param {?} context
     * @return {?}
     */
    function (moduleCFR, contentInjector, content, context) {
        /** @type {?} */
        var contentCmptFactory = moduleCFR.resolveComponentFactory(content);
        /** @type {?} */
        var modalContentInjector = Injector.create({ providers: [{ provide: NgtActiveModal, useValue: context }], parent: contentInjector });
        /** @type {?} */
        var componentRef = contentCmptFactory.create(modalContentInjector);
        this._applicationRef.attachView(componentRef.hostView);
        return new ContentRef([[componentRef.location.nativeElement]], componentRef.hostView, componentRef);
    };
    /**
     * @private
     * @param {?} componentRef
     * @return {?}
     */
    NgtModalStack.prototype._createFromComponentRef = /**
     * @private
     * @param {?} componentRef
     * @return {?}
     */
    function (componentRef) {
        return new ContentRef([[componentRef._elRef.nativeElement]], null, componentRef);
    };
    /**
     * @private
     * @param {?} element
     * @return {?}
     */
    NgtModalStack.prototype._setAriaHidden = /**
     * @private
     * @param {?} element
     * @return {?}
     */
    function (element) {
        var _this = this;
        /** @type {?} */
        var parent = element.parentElement;
        if (parent && element !== this._document.body) {
            Array.from(parent.children).forEach((/**
             * @param {?} sibling
             * @return {?}
             */
            function (sibling) {
                if (sibling !== element && sibling.nodeName !== 'SCRIPT') {
                    _this._ariaHiddenValues.set(sibling, sibling.getAttribute('aria-hidden'));
                    sibling.setAttribute('aria-hidden', 'true');
                }
            }));
            this._setAriaHidden(parent);
        }
    };
    /**
     * @private
     * @return {?}
     */
    NgtModalStack.prototype._revertAriaHidden = /**
     * @private
     * @return {?}
     */
    function () {
        this._ariaHiddenValues.forEach((/**
         * @param {?} value
         * @param {?} element
         * @return {?}
         */
        function (value, element) {
            if (value) {
                element.setAttribute('aria-hidden', value);
            }
            else {
                element.removeAttribute('aria-hidden');
            }
        }));
        this._ariaHiddenValues.clear();
    };
    /**
     * @private
     * @param {?} ngtModalRef
     * @return {?}
     */
    NgtModalStack.prototype._registerModalRef = /**
     * @private
     * @param {?} ngtModalRef
     * @return {?}
     */
    function (ngtModalRef) {
        var _this = this;
        /** @type {?} */
        var unregisterModalRef = (/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var index = _this._modalRefs.indexOf(ngtModalRef);
            if (index > -1) {
                _this._modalRefs.splice(index, 1);
            }
        });
        this._modalRefs.push(ngtModalRef);
        ngtModalRef.result.then(unregisterModalRef, unregisterModalRef);
    };
    /**
     * @private
     * @param {?} ngtWindowCmpt
     * @return {?}
     */
    NgtModalStack.prototype._registerWindowCmpt = /**
     * @private
     * @param {?} ngtWindowCmpt
     * @return {?}
     */
    function (ngtWindowCmpt) {
        var _this = this;
        this._windowCmpts.push(ngtWindowCmpt);
        this._activeWindowCmptHasChanged.next();
        ngtWindowCmpt.onDestroy((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var index = _this._windowCmpts.indexOf(ngtWindowCmpt);
            if (index > -1) {
                _this._windowCmpts.splice(index, 1);
                _this._activeWindowCmptHasChanged.next();
            }
        }));
    };
    NgtModalStack.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    NgtModalStack.ctorParameters = function () { return [
        { type: ApplicationRef },
        { type: Injector },
        { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
        { type: ScrollBar },
        { type: RendererFactory2 }
    ]; };
    /** @nocollapse */ NgtModalStack.ngInjectableDef = i0.defineInjectable({ factory: function NgtModalStack_Factory() { return new NgtModalStack(i0.inject(i0.ApplicationRef), i0.inject(i0.INJECTOR), i0.inject(i1.DOCUMENT), i0.inject(i2.ScrollBar), i0.inject(i0.RendererFactory2)); }, token: NgtModalStack, providedIn: "root" });
    return NgtModalStack;
}());
export { NgtModalStack };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._activeWindowCmptHasChanged;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._ariaHiddenValues;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._modalRefs;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._backdropAttributes;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._windowAttributes;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._backdropEvents;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._windowEvents;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._windowCmpts;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._applicationRef;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._injector;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._document;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._scrollBar;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._rendererFactory;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtc3RhY2suanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbIm1vZGFsL21vZGFsLXN0YWNrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0gsY0FBYyxFQUdkLE1BQU0sRUFDTixVQUFVLEVBQ1YsUUFBUSxFQUNSLGdCQUFnQixFQUNoQixXQUFXLEVBQ2QsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLGNBQWMsRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDMUQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ25ELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNuRSxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN2RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7O0FBRTNDO0lBV0ksdUJBQ1ksZUFBK0IsRUFDL0IsU0FBbUIsRUFDRCxTQUFjLEVBQ2hDLFVBQXFCLEVBQ3JCLGdCQUFrQztRQUw5QyxpQkFlQztRQWRXLG9CQUFlLEdBQWYsZUFBZSxDQUFnQjtRQUMvQixjQUFTLEdBQVQsU0FBUyxDQUFVO1FBQ0QsY0FBUyxHQUFULFNBQVMsQ0FBSztRQUNoQyxlQUFVLEdBQVYsVUFBVSxDQUFXO1FBQ3JCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFkdEMsZ0NBQTJCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUM1QyxzQkFBaUIsR0FBeUIsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNwRCxlQUFVLEdBQWtCLEVBQUUsQ0FBQztRQUMvQix3QkFBbUIsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3hDLHNCQUFpQixHQUFHLENBQUMsVUFBVSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLG1CQUFtQixFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ3JHLG9CQUFlLEdBQUcsQ0FBQyx5QkFBeUIsRUFBRSx3QkFBd0IsRUFBRSx5QkFBeUIsRUFBRSx3QkFBd0IsQ0FBQyxDQUFDO1FBQzdILGtCQUFhLEdBQUcsQ0FBQyxzQkFBc0IsRUFBRSxxQkFBcUIsRUFBRSxzQkFBc0IsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1FBQy9HLGlCQUFZLEdBQTRDLEVBQUUsQ0FBQztRQVEvRCxrQ0FBa0M7UUFDbEMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLFNBQVM7OztRQUFDO1lBQ3ZDLElBQUksS0FBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7O29CQUNwQixnQkFBZ0IsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztnQkFDeEUsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsS0FBSSxDQUFDLDJCQUEyQixDQUFDLENBQUM7Z0JBQ3hGLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2dCQUN6QixLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUNoRTtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7Ozs7SUFFRCw0QkFBSTs7Ozs7OztJQUFKLFVBQUssU0FBbUMsRUFBRSxlQUF5QixFQUFFLE9BQVksRUFBRSxPQUFPO1FBQTFGLGlCQTZDQzs7WUE1Q1MsV0FBVyxHQUFHLFNBQVMsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJOztZQUNsSCxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDOztZQUUzRCx5QkFBeUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRTs7WUFDeEQsZUFBZTs7O1FBQUc7WUFDcEIsSUFBSSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFO2dCQUN6QixRQUFRLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFDO2FBQzNEO1FBQ0wsQ0FBQyxDQUFBO1FBRUQsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNkLE1BQU0sSUFBSSxLQUFLLENBQUMsc0NBQWtDLE9BQU8sQ0FBQyxTQUFTLElBQUksTUFBTSxrQ0FBNkIsQ0FBQyxDQUFDO1NBQy9HO1FBRUssSUFBQSxpRkFBd0csRUFBdkcsMEJBQVUsRUFBRSw0QkFBMkY7O1lBRXhHLGVBQWUsR0FDakIsT0FBTyxDQUFDLFFBQVEsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxFQUFFLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJOztZQUM5RSxhQUFhLEdBQTBDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsQ0FBQzs7WUFDdEgsV0FBVyxHQUFnQixJQUFJLFdBQVcsQ0FBQyxhQUFhLEVBQUUsVUFBVSxFQUFFLGVBQWUsRUFBRSxPQUFPLENBQUMsYUFBYSxDQUFDO1FBRW5ILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDeEMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMseUJBQXlCLEVBQUUseUJBQXlCLENBQUMsQ0FBQztRQUM5RSxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFFMUQsV0FBVyxDQUFDLEtBQUs7Ozs7UUFBRyxVQUFDLE1BQVc7WUFDNUIsV0FBVyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QixDQUFDLENBQUEsQ0FBQztRQUNGLFdBQVcsQ0FBQyxPQUFPOzs7O1FBQUcsVUFBQyxNQUFXO1lBQzlCLFdBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFBLENBQUM7UUFFRixJQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2hFLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQzlCLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsWUFBWSxDQUFDLENBQUM7U0FDeEQ7UUFFRCxJQUFJLGVBQWUsSUFBSSxlQUFlLENBQUMsUUFBUSxFQUFFO1lBQzdDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLFdBQVcsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDckU7UUFDRCxPQUFPLFdBQVcsQ0FBQztJQUN2QixDQUFDOzs7OztJQUVELGtDQUFVOzs7O0lBQVYsVUFBVyxNQUFZO1FBQ25CLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTzs7OztRQUFDLFVBQUEsV0FBVyxJQUFJLE9BQUEsV0FBVyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBM0IsQ0FBMkIsRUFBQyxDQUFDO0lBQ3hFLENBQUM7Ozs7SUFFRCxxQ0FBYTs7O0lBQWI7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7Ozs7O0lBRU8sdUNBQWU7Ozs7OztJQUF2QixVQUF3QixTQUFtQyxFQUFFLFdBQWdCOztZQUNuRSxlQUFlLEdBQUcsU0FBUyxDQUFDLHVCQUF1QixDQUFDLHlCQUF5QixDQUFDOztZQUM5RSxlQUFlLEdBQUcsZUFBZSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzlELElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxRCxXQUFXLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDaEUsT0FBTyxlQUFlLENBQUM7SUFDM0IsQ0FBQzs7Ozs7Ozs7SUFFTyw4Q0FBc0I7Ozs7Ozs7SUFBOUIsVUFBK0IsU0FBbUMsRUFBRSxXQUFnQixFQUFFLFVBQWU7O1lBRTNGLGFBQWEsR0FBRyxTQUFTLENBQUMsdUJBQXVCLENBQUMsdUJBQXVCLENBQUM7O1lBQzFFLGFBQWEsR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsVUFBVSxDQUFDLEtBQUssQ0FBQztRQUM1RSxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDeEQsV0FBVyxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzlELE9BQU8sYUFBYSxDQUFDO0lBQ3pCLENBQUM7Ozs7Ozs7SUFFTywyQ0FBbUI7Ozs7OztJQUEzQixVQUE0QixjQUF1QyxFQUFFLE9BQWU7UUFDaEYsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU87Ozs7UUFBQyxVQUFDLFVBQWtCO1lBQzlDLElBQUksU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFO2dCQUNoQyxjQUFjLENBQUMsVUFBVSxDQUFDLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ3BEO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7O0lBRU8sNkNBQXFCOzs7Ozs7SUFBN0IsVUFBOEIsZ0JBQTJDLEVBQUUsT0FBZTtRQUN0RixJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTzs7OztRQUFDLFVBQUMsVUFBa0I7WUFDaEQsSUFBSSxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEVBQUU7Z0JBQ2hDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUN0RDtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7Ozs7SUFFTyxvQ0FBWTs7Ozs7OztJQUFwQixVQUFxQixtQkFBd0IsRUFBRSxpQkFBc0IsRUFBRSxNQUFNO1FBQ3pFLE1BQU0sQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQyxTQUFpQjtZQUM3QixJQUFJLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFO2dCQUN0RixtQkFBbUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTOzs7Z0JBQUMsY0FBTSxPQUFBLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFuQyxDQUFtQyxFQUFDLENBQUM7YUFDdkY7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7O0lBRU8sc0NBQWM7Ozs7Ozs7SUFBdEIsVUFBdUIsU0FBbUMsRUFBRSxlQUF5QixFQUFFLE9BQVk7O1lBQzNGLFdBQVcsR0FBRyxJQUFJLGNBQWMsRUFBRTs7WUFDbEMsVUFBVTtRQUNkLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDVixVQUFVLEdBQUcsSUFBSSxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDbkM7YUFBTSxJQUFJLE9BQU8sWUFBWSxXQUFXLEVBQUU7WUFDdkMsVUFBVSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLENBQUM7U0FDbEU7YUFBTSxJQUFJLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUMxQixVQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ2hEO2FBQU0sSUFBSSxPQUFPLE9BQU8sS0FBSyxVQUFVLEVBQUU7WUFDdEMsVUFBVSxHQUFHLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxTQUFTLEVBQUUsZUFBZSxFQUFFLE9BQU8sRUFBRSxXQUFXLENBQUMsQ0FBQztTQUN2RzthQUFNO1lBQ0gsVUFBVSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNuRCxXQUFXLEdBQUcsVUFBVSxDQUFDLFlBQVksQ0FBQyxXQUFXLElBQUksV0FBVyxDQUFDO1NBQ3BFO1FBQ0QsT0FBTyxFQUFDLFVBQVUsWUFBQSxFQUFFLFdBQVcsYUFBQSxFQUFDLENBQUM7SUFDckMsQ0FBQzs7Ozs7OztJQUVPLDhDQUFzQjs7Ozs7O0lBQTlCLFVBQStCLE9BQXlCLEVBQUUsV0FBMkI7O1lBQzNFLE9BQU8sR0FBRztZQUNaLFNBQVMsRUFBRSxXQUFXO1lBQ3RCLEtBQUs7Ozs7c0JBQUMsTUFBTTtnQkFDUixXQUFXLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzlCLENBQUM7WUFDRCxPQUFPOzs7O3NCQUFDLE1BQU07Z0JBQ1YsV0FBVyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoQyxDQUFDO1NBQ0o7O1lBQ0ssT0FBTyxHQUFHLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUM7UUFDbkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUN4RCxDQUFDOzs7Ozs7SUFFTyx5Q0FBaUI7Ozs7O0lBQXpCLFVBQTBCLE9BQWU7O1lBQy9CLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxLQUFHLE9BQVMsQ0FBQztRQUM3RCxPQUFPLElBQUksVUFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDekMsQ0FBQzs7Ozs7Ozs7O0lBRU8sdURBQStCOzs7Ozs7OztJQUF2QyxVQUNJLFNBQW1DLEVBQUUsZUFBeUIsRUFBRSxPQUFZLEVBQzVFLE9BQXVCOztZQUNqQixrQkFBa0IsR0FBRyxTQUFTLENBQUMsdUJBQXVCLENBQUMsT0FBTyxDQUFDOztZQUMvRCxvQkFBb0IsR0FDdEIsUUFBUSxDQUFDLE1BQU0sQ0FBQyxFQUFDLFNBQVMsRUFBRSxDQUFDLEVBQUMsT0FBTyxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFDLENBQUMsRUFBRSxNQUFNLEVBQUUsZUFBZSxFQUFDLENBQUM7O1lBQ25HLFlBQVksR0FBRyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsb0JBQW9CLENBQUM7UUFDcEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZELE9BQU8sSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsUUFBUSxFQUFFLFlBQVksQ0FBQyxDQUFDO0lBQ3hHLENBQUM7Ozs7OztJQUVPLCtDQUF1Qjs7Ozs7SUFBL0IsVUFBZ0MsWUFBaUI7UUFDN0MsT0FBTyxJQUFJLFVBQVUsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQztJQUNyRixDQUFDOzs7Ozs7SUFFTyxzQ0FBYzs7Ozs7SUFBdEIsVUFBdUIsT0FBZ0I7UUFBdkMsaUJBWUM7O1lBWFMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxhQUFhO1FBQ3BDLElBQUksTUFBTSxJQUFJLE9BQU8sS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRTtZQUMzQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQSxPQUFPO2dCQUN2QyxJQUFJLE9BQU8sS0FBSyxPQUFPLElBQUksT0FBTyxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7b0JBQ3RELEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztvQkFDekUsT0FBTyxDQUFDLFlBQVksQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDLENBQUM7aUJBQy9DO1lBQ0wsQ0FBQyxFQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQy9CO0lBQ0wsQ0FBQzs7Ozs7SUFFTyx5Q0FBaUI7Ozs7SUFBekI7UUFDSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTzs7Ozs7UUFBQyxVQUFDLEtBQUssRUFBRSxPQUFPO1lBQzFDLElBQUksS0FBSyxFQUFFO2dCQUNQLE9BQU8sQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQzlDO2lCQUFNO2dCQUNILE9BQU8sQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLENBQUM7YUFDMUM7UUFDTCxDQUFDLEVBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNuQyxDQUFDOzs7Ozs7SUFFTyx5Q0FBaUI7Ozs7O0lBQXpCLFVBQTBCLFdBQXdCO1FBQWxELGlCQVNDOztZQVJTLGtCQUFrQjs7O1FBQUc7O2dCQUNqQixLQUFLLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO1lBQ2xELElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUNaLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQzthQUNwQztRQUNMLENBQUMsQ0FBQTtRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2xDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFLGtCQUFrQixDQUFDLENBQUM7SUFDcEUsQ0FBQzs7Ozs7O0lBRU8sMkNBQW1COzs7OztJQUEzQixVQUE0QixhQUFvRDtRQUFoRixpQkFXQztRQVZHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUV4QyxhQUFhLENBQUMsU0FBUzs7O1FBQUM7O2dCQUNkLEtBQUssR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7WUFDdEQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQ1osS0FBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxLQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDM0M7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7O2dCQWhPSixVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7O2dCQW5CNUIsY0FBYztnQkFLZCxRQUFRO2dEQTRCSCxNQUFNLFNBQUMsUUFBUTtnQkFyQmYsU0FBUztnQkFOZCxnQkFBZ0I7Ozt3QkFQcEI7Q0FxUEMsQUFqT0QsSUFpT0M7U0FoT1ksYUFBYTs7Ozs7O0lBQ3RCLG9EQUFvRDs7Ozs7SUFDcEQsMENBQTREOzs7OztJQUM1RCxtQ0FBdUM7Ozs7O0lBQ3ZDLDRDQUFnRDs7Ozs7SUFDaEQsMENBQTZHOzs7OztJQUM3Ryx3Q0FBcUk7Ozs7O0lBQ3JJLHNDQUF1SDs7Ozs7SUFDdkgscUNBQW1FOzs7OztJQUcvRCx3Q0FBdUM7Ozs7O0lBQ3ZDLGtDQUEyQjs7Ozs7SUFDM0Isa0NBQXdDOzs7OztJQUN4QyxtQ0FBNkI7Ozs7O0lBQzdCLHlDQUEwQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBBcHBsaWNhdGlvblJlZixcclxuICAgIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcclxuICAgIENvbXBvbmVudFJlZixcclxuICAgIEluamVjdCxcclxuICAgIEluamVjdGFibGUsXHJcbiAgICBJbmplY3RvcixcclxuICAgIFJlbmRlcmVyRmFjdG9yeTIsXHJcbiAgICBUZW1wbGF0ZVJlZlxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBET0NVTUVOVCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTmd0QWN0aXZlTW9kYWwsIE5ndE1vZGFsUmVmIH0gZnJvbSAnLi9tb2RhbC1yZWYnO1xyXG5pbXBvcnQgeyBTY3JvbGxCYXIgfSBmcm9tICcuLi91dGlsL3Njcm9sbGJhcic7XHJcbmltcG9ydCB7IGlzRGVmaW5lZCwgaXNTdHJpbmcgfSBmcm9tICcuLi91dGlsL3V0aWwnO1xyXG5pbXBvcnQgeyBuZ3RGb2N1c1RyYXAgfSBmcm9tICcuLi91dGlsL2ZvY3VzLXRyYXAnO1xyXG5pbXBvcnQgeyBOZ3RNb2RhbFdpbmRvd0NvbXBvbmVudCB9IGZyb20gJy4vbW9kYWwtd2luZG93LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE5ndE1vZGFsQmFja2Ryb3BDb21wb25lbnQgfSBmcm9tICcuL21vZGFsLWJhY2tkcm9wLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENvbnRlbnRSZWYgfSBmcm9tICcuLi91dGlsL3BvcHVwJztcclxuXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTmd0TW9kYWxTdGFjayB7XHJcbiAgICBwcml2YXRlIF9hY3RpdmVXaW5kb3dDbXB0SGFzQ2hhbmdlZCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBwcml2YXRlIF9hcmlhSGlkZGVuVmFsdWVzOiBNYXA8RWxlbWVudCwgc3RyaW5nPiA9IG5ldyBNYXAoKTtcclxuICAgIHByaXZhdGUgX21vZGFsUmVmczogTmd0TW9kYWxSZWZbXSA9IFtdO1xyXG4gICAgcHJpdmF0ZSBfYmFja2Ryb3BBdHRyaWJ1dGVzID0gWydiYWNrZHJvcENsYXNzJ107XHJcbiAgICBwcml2YXRlIF93aW5kb3dBdHRyaWJ1dGVzID0gWydiYWNrZHJvcCcsICdjZW50ZXJlZCcsICdrZXlib2FyZCcsICdzaXplJywgJ3Njcm9sbGFibGVDb250ZW50JywgJ3dpbmRvd0NsYXNzJ107XHJcbiAgICBwcml2YXRlIF9iYWNrZHJvcEV2ZW50cyA9IFsnYmFja2Ryb3BPcGVuaW5nRGlkU3RhcnQnLCAnYmFja2Ryb3BPcGVuaW5nRGlkRG9uZScsICdiYWNrZHJvcENsb3NpbmdEaWRTdGFydCcsICdiYWNrZHJvcENsb3NpbmdEaWREb25lJ107XHJcbiAgICBwcml2YXRlIF93aW5kb3dFdmVudHMgPSBbJ21vZGFsT3BlbmluZ0RpZFN0YXJ0JywgJ21vZGFsT3BlbmluZ0RpZERvbmUnLCAnbW9kYWxDbG9zaW5nRGlkU3RhcnQnLCAnbW9kYWxDbG9zaW5nRGlkRG9uZSddO1xyXG4gICAgcHJpdmF0ZSBfd2luZG93Q21wdHM6IENvbXBvbmVudFJlZjxOZ3RNb2RhbFdpbmRvd0NvbXBvbmVudD5bXSA9IFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgX2FwcGxpY2F0aW9uUmVmOiBBcHBsaWNhdGlvblJlZixcclxuICAgICAgICBwcml2YXRlIF9pbmplY3RvcjogSW5qZWN0b3IsXHJcbiAgICAgICAgQEluamVjdChET0NVTUVOVCkgcHJpdmF0ZSBfZG9jdW1lbnQ6IGFueSxcclxuICAgICAgICBwcml2YXRlIF9zY3JvbGxCYXI6IFNjcm9sbEJhcixcclxuICAgICAgICBwcml2YXRlIF9yZW5kZXJlckZhY3Rvcnk6IFJlbmRlcmVyRmFjdG9yeTIpIHtcclxuICAgICAgICAvLyBUcmFwIGZvY3VzIG9uIGFjdGl2ZSBXaW5kb3dDbXB0XHJcbiAgICAgICAgdGhpcy5fYWN0aXZlV2luZG93Q21wdEhhc0NoYW5nZWQuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuX3dpbmRvd0NtcHRzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgYWN0aXZlV2luZG93Q21wdCA9IHRoaXMuX3dpbmRvd0NtcHRzW3RoaXMuX3dpbmRvd0NtcHRzLmxlbmd0aCAtIDFdO1xyXG4gICAgICAgICAgICAgICAgbmd0Rm9jdXNUcmFwKGFjdGl2ZVdpbmRvd0NtcHQubG9jYXRpb24ubmF0aXZlRWxlbWVudCwgdGhpcy5fYWN0aXZlV2luZG93Q21wdEhhc0NoYW5nZWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fcmV2ZXJ0QXJpYUhpZGRlbigpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fc2V0QXJpYUhpZGRlbihhY3RpdmVXaW5kb3dDbXB0LmxvY2F0aW9uLm5hdGl2ZUVsZW1lbnQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb3Blbihtb2R1bGVDRlI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29udGVudEluamVjdG9yOiBJbmplY3RvciwgY29udGVudDogYW55LCBvcHRpb25zKSB7XHJcbiAgICAgICAgY29uc3QgY29udGFpbmVyRWwgPSBpc0RlZmluZWQob3B0aW9ucy5jb250YWluZXIpID8gdGhpcy5fZG9jdW1lbnQucXVlcnlTZWxlY3RvcihvcHRpb25zLmNvbnRhaW5lcikgOiB0aGlzLl9kb2N1bWVudC5ib2R5O1xyXG4gICAgICAgIGNvbnN0IHJlbmRlcmVyID0gdGhpcy5fcmVuZGVyZXJGYWN0b3J5LmNyZWF0ZVJlbmRlcmVyKG51bGwsIG51bGwpO1xyXG5cclxuICAgICAgICBjb25zdCByZXZlcnRQYWRkaW5nRm9yU2Nyb2xsQmFyID0gdGhpcy5fc2Nyb2xsQmFyLmNvbXBlbnNhdGUoKTtcclxuICAgICAgICBjb25zdCByZW1vdmVCb2R5Q2xhc3MgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5fbW9kYWxSZWZzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgcmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5fZG9jdW1lbnQuYm9keSwgJ21vZGFsLW9wZW4nKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGlmICghY29udGFpbmVyRWwpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBUaGUgc3BlY2lmaWVkIG1vZGFsIGNvbnRhaW5lciBcIiR7b3B0aW9ucy5jb250YWluZXIgfHwgJ2JvZHknfVwiIHdhcyBub3QgZm91bmQgaW4gdGhlIERPTS5gKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHtjb250ZW50UmVmLCBhY3RpdmVNb2RhbH0gPSB0aGlzLl9nZXRDb250ZW50UmVmKG1vZHVsZUNGUiwgb3B0aW9ucy5pbmplY3RvciB8fCBjb250ZW50SW5qZWN0b3IsIGNvbnRlbnQpO1xyXG5cclxuICAgICAgICBjb25zdCBiYWNrZHJvcENtcHRSZWY6IENvbXBvbmVudFJlZjxOZ3RNb2RhbEJhY2tkcm9wQ29tcG9uZW50PiA9XHJcbiAgICAgICAgICAgIG9wdGlvbnMuYmFja2Ryb3AgIT09IGZhbHNlID8gdGhpcy5fYXR0YWNoQmFja2Ryb3AobW9kdWxlQ0ZSLCBjb250YWluZXJFbCkgOiBudWxsO1xyXG4gICAgICAgIGNvbnN0IHdpbmRvd0NtcHRSZWY6IENvbXBvbmVudFJlZjxOZ3RNb2RhbFdpbmRvd0NvbXBvbmVudD4gPSB0aGlzLl9hdHRhY2hXaW5kb3dDb21wb25lbnQobW9kdWxlQ0ZSLCBjb250YWluZXJFbCwgY29udGVudFJlZik7XHJcbiAgICAgICAgY29uc3Qgbmd0TW9kYWxSZWY6IE5ndE1vZGFsUmVmID0gbmV3IE5ndE1vZGFsUmVmKHdpbmRvd0NtcHRSZWYsIGNvbnRlbnRSZWYsIGJhY2tkcm9wQ21wdFJlZiwgb3B0aW9ucy5iZWZvcmVEaXNtaXNzKTtcclxuXHJcbiAgICAgICAgdGhpcy5fcmVnaXN0ZXJNb2RhbFJlZihuZ3RNb2RhbFJlZik7XHJcbiAgICAgICAgdGhpcy5fcmVnaXN0ZXJXaW5kb3dDbXB0KHdpbmRvd0NtcHRSZWYpO1xyXG4gICAgICAgIG5ndE1vZGFsUmVmLnJlc3VsdC50aGVuKHJldmVydFBhZGRpbmdGb3JTY3JvbGxCYXIsIHJldmVydFBhZGRpbmdGb3JTY3JvbGxCYXIpO1xyXG4gICAgICAgIG5ndE1vZGFsUmVmLnJlc3VsdC50aGVuKHJlbW92ZUJvZHlDbGFzcywgcmVtb3ZlQm9keUNsYXNzKTtcclxuXHJcbiAgICAgICAgYWN0aXZlTW9kYWwuY2xvc2UgPSAocmVzdWx0OiBhbnkpID0+IHtcclxuICAgICAgICAgICAgbmd0TW9kYWxSZWYuY2xvc2UocmVzdWx0KTtcclxuICAgICAgICB9O1xyXG4gICAgICAgIGFjdGl2ZU1vZGFsLmRpc21pc3MgPSAocmVhc29uOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgbmd0TW9kYWxSZWYuZGlzbWlzcyhyZWFzb24pO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMuX2FwcGx5V2luZG93T3B0aW9ucyh3aW5kb3dDbXB0UmVmLmluc3RhbmNlLCBvcHRpb25zKTtcclxuICAgICAgICB0aGlzLl9hcHBseUV2ZW50cyhuZ3RNb2RhbFJlZiwgYWN0aXZlTW9kYWwsIHRoaXMuX3dpbmRvd0V2ZW50cyk7XHJcbiAgICAgICAgaWYgKHRoaXMuX21vZGFsUmVmcy5sZW5ndGggPT09IDEpIHtcclxuICAgICAgICAgICAgcmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5fZG9jdW1lbnQuYm9keSwgJ21vZGFsLW9wZW4nKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChiYWNrZHJvcENtcHRSZWYgJiYgYmFja2Ryb3BDbXB0UmVmLmluc3RhbmNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2FwcGx5QmFja2Ryb3BPcHRpb25zKGJhY2tkcm9wQ21wdFJlZi5pbnN0YW5jZSwgb3B0aW9ucyk7XHJcbiAgICAgICAgICAgIHRoaXMuX2FwcGx5RXZlbnRzKG5ndE1vZGFsUmVmLCBhY3RpdmVNb2RhbCwgdGhpcy5fYmFja2Ryb3BFdmVudHMpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbmd0TW9kYWxSZWY7XHJcbiAgICB9XHJcblxyXG4gICAgZGlzbWlzc0FsbChyZWFzb24/OiBhbnkpIHtcclxuICAgICAgICB0aGlzLl9tb2RhbFJlZnMuZm9yRWFjaChuZ3RNb2RhbFJlZiA9PiBuZ3RNb2RhbFJlZi5kaXNtaXNzKHJlYXNvbikpO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc09wZW5Nb2RhbHMoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX21vZGFsUmVmcy5sZW5ndGggPiAwO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2F0dGFjaEJhY2tkcm9wKG1vZHVsZUNGUjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb250YWluZXJFbDogYW55KTogQ29tcG9uZW50UmVmPE5ndE1vZGFsQmFja2Ryb3BDb21wb25lbnQ+IHtcclxuICAgICAgICBjb25zdCBiYWNrZHJvcEZhY3RvcnkgPSBtb2R1bGVDRlIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkoTmd0TW9kYWxCYWNrZHJvcENvbXBvbmVudCk7XHJcbiAgICAgICAgY29uc3QgYmFja2Ryb3BDbXB0UmVmID0gYmFja2Ryb3BGYWN0b3J5LmNyZWF0ZSh0aGlzLl9pbmplY3Rvcik7XHJcbiAgICAgICAgdGhpcy5fYXBwbGljYXRpb25SZWYuYXR0YWNoVmlldyhiYWNrZHJvcENtcHRSZWYuaG9zdFZpZXcpO1xyXG4gICAgICAgIGNvbnRhaW5lckVsLmFwcGVuZENoaWxkKGJhY2tkcm9wQ21wdFJlZi5sb2NhdGlvbi5uYXRpdmVFbGVtZW50KTtcclxuICAgICAgICByZXR1cm4gYmFja2Ryb3BDbXB0UmVmO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2F0dGFjaFdpbmRvd0NvbXBvbmVudChtb2R1bGVDRlI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29udGFpbmVyRWw6IGFueSwgY29udGVudFJlZjogYW55KTpcclxuICAgICAgICBDb21wb25lbnRSZWY8Tmd0TW9kYWxXaW5kb3dDb21wb25lbnQ+IHtcclxuICAgICAgICBjb25zdCB3aW5kb3dGYWN0b3J5ID0gbW9kdWxlQ0ZSLnJlc29sdmVDb21wb25lbnRGYWN0b3J5KE5ndE1vZGFsV2luZG93Q29tcG9uZW50KTtcclxuICAgICAgICBjb25zdCB3aW5kb3dDbXB0UmVmID0gd2luZG93RmFjdG9yeS5jcmVhdGUodGhpcy5faW5qZWN0b3IsIGNvbnRlbnRSZWYubm9kZXMpO1xyXG4gICAgICAgIHRoaXMuX2FwcGxpY2F0aW9uUmVmLmF0dGFjaFZpZXcod2luZG93Q21wdFJlZi5ob3N0Vmlldyk7XHJcbiAgICAgICAgY29udGFpbmVyRWwuYXBwZW5kQ2hpbGQod2luZG93Q21wdFJlZi5sb2NhdGlvbi5uYXRpdmVFbGVtZW50KTtcclxuICAgICAgICByZXR1cm4gd2luZG93Q21wdFJlZjtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9hcHBseVdpbmRvd09wdGlvbnMod2luZG93SW5zdGFuY2U6IE5ndE1vZGFsV2luZG93Q29tcG9uZW50LCBvcHRpb25zOiBPYmplY3QpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl93aW5kb3dBdHRyaWJ1dGVzLmZvckVhY2goKG9wdGlvbk5hbWU6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICBpZiAoaXNEZWZpbmVkKG9wdGlvbnNbb3B0aW9uTmFtZV0pKSB7XHJcbiAgICAgICAgICAgICAgICB3aW5kb3dJbnN0YW5jZVtvcHRpb25OYW1lXSA9IG9wdGlvbnNbb3B0aW9uTmFtZV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9hcHBseUJhY2tkcm9wT3B0aW9ucyhiYWNrZHJvcEluc3RhbmNlOiBOZ3RNb2RhbEJhY2tkcm9wQ29tcG9uZW50LCBvcHRpb25zOiBPYmplY3QpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9iYWNrZHJvcEF0dHJpYnV0ZXMuZm9yRWFjaCgob3B0aW9uTmFtZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChpc0RlZmluZWQob3B0aW9uc1tvcHRpb25OYW1lXSkpIHtcclxuICAgICAgICAgICAgICAgIGJhY2tkcm9wSW5zdGFuY2Vbb3B0aW9uTmFtZV0gPSBvcHRpb25zW29wdGlvbk5hbWVdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfYXBwbHlFdmVudHMoaW5zdGFuY2VUb1N1YnNjcmliZTogYW55LCBpbnN0YW5jZVRvVHJpZ2dlcjogYW55LCBldmVudHMpOiB2b2lkIHtcclxuICAgICAgICBldmVudHMuZm9yRWFjaCgoZXZlbnROYW1lOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgaWYgKGlzRGVmaW5lZChpbnN0YW5jZVRvU3Vic2NyaWJlW2V2ZW50TmFtZV0pICYmIGlzRGVmaW5lZChpbnN0YW5jZVRvVHJpZ2dlcltldmVudE5hbWVdKSkge1xyXG4gICAgICAgICAgICAgICAgaW5zdGFuY2VUb1N1YnNjcmliZVtldmVudE5hbWVdLnN1YnNjcmliZSgoKSA9PiBpbnN0YW5jZVRvVHJpZ2dlcltldmVudE5hbWVdLm5leHQoKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9nZXRDb250ZW50UmVmKG1vZHVsZUNGUjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb250ZW50SW5qZWN0b3I6IEluamVjdG9yLCBjb250ZW50OiBhbnkpOiB7IGNvbnRlbnRSZWY6IENvbnRlbnRSZWYsIGFjdGl2ZU1vZGFsOiBOZ3RBY3RpdmVNb2RhbCB9IHtcclxuICAgICAgICBsZXQgYWN0aXZlTW9kYWwgPSBuZXcgTmd0QWN0aXZlTW9kYWwoKTtcclxuICAgICAgICBsZXQgY29udGVudFJlZjtcclxuICAgICAgICBpZiAoIWNvbnRlbnQpIHtcclxuICAgICAgICAgICAgY29udGVudFJlZiA9IG5ldyBDb250ZW50UmVmKFtdKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGNvbnRlbnQgaW5zdGFuY2VvZiBUZW1wbGF0ZVJlZikge1xyXG4gICAgICAgICAgICBjb250ZW50UmVmID0gdGhpcy5fY3JlYXRlRnJvbVRlbXBsYXRlUmVmKGNvbnRlbnQsIGFjdGl2ZU1vZGFsKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGlzU3RyaW5nKGNvbnRlbnQpKSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnRSZWYgPSB0aGlzLl9jcmVhdGVGcm9tU3RyaW5nKGNvbnRlbnQpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGNvbnRlbnQgPT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICAgICAgY29udGVudFJlZiA9IHRoaXMuX2NyZWF0ZUZyb21Db21wb25lbnRDb25zdHJ1Y3Rvcihtb2R1bGVDRlIsIGNvbnRlbnRJbmplY3RvciwgY29udGVudCwgYWN0aXZlTW9kYWwpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnRSZWYgPSB0aGlzLl9jcmVhdGVGcm9tQ29tcG9uZW50UmVmKGNvbnRlbnQpO1xyXG4gICAgICAgICAgICBhY3RpdmVNb2RhbCA9IGNvbnRlbnRSZWYuY29tcG9uZW50UmVmLmFjdGl2ZU1vZGFsIHx8IGFjdGl2ZU1vZGFsO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4ge2NvbnRlbnRSZWYsIGFjdGl2ZU1vZGFsfTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9jcmVhdGVGcm9tVGVtcGxhdGVSZWYoY29udGVudDogVGVtcGxhdGVSZWY8YW55PiwgYWN0aXZlTW9kYWw6IE5ndEFjdGl2ZU1vZGFsKTogQ29udGVudFJlZiB7XHJcbiAgICAgICAgY29uc3QgY29udGV4dCA9IHtcclxuICAgICAgICAgICAgJGltcGxpY2l0OiBhY3RpdmVNb2RhbCxcclxuICAgICAgICAgICAgY2xvc2UocmVzdWx0KSB7XHJcbiAgICAgICAgICAgICAgICBhY3RpdmVNb2RhbC5jbG9zZShyZXN1bHQpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBkaXNtaXNzKHJlYXNvbikge1xyXG4gICAgICAgICAgICAgICAgYWN0aXZlTW9kYWwuZGlzbWlzcyhyZWFzb24pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCB2aWV3UmVmID0gY29udGVudC5jcmVhdGVFbWJlZGRlZFZpZXcoY29udGV4dCk7XHJcbiAgICAgICAgdGhpcy5fYXBwbGljYXRpb25SZWYuYXR0YWNoVmlldyh2aWV3UmVmKTtcclxuICAgICAgICByZXR1cm4gbmV3IENvbnRlbnRSZWYoW3ZpZXdSZWYucm9vdE5vZGVzXSwgdmlld1JlZik7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfY3JlYXRlRnJvbVN0cmluZyhjb250ZW50OiBzdHJpbmcpOiBDb250ZW50UmVmIHtcclxuICAgICAgICBjb25zdCBjb21wb25lbnQgPSB0aGlzLl9kb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShgJHtjb250ZW50fWApO1xyXG4gICAgICAgIHJldHVybiBuZXcgQ29udGVudFJlZihbW2NvbXBvbmVudF1dKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9jcmVhdGVGcm9tQ29tcG9uZW50Q29uc3RydWN0b3IoXHJcbiAgICAgICAgbW9kdWxlQ0ZSOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIGNvbnRlbnRJbmplY3RvcjogSW5qZWN0b3IsIGNvbnRlbnQ6IGFueSxcclxuICAgICAgICBjb250ZXh0OiBOZ3RBY3RpdmVNb2RhbCk6IENvbnRlbnRSZWYge1xyXG4gICAgICAgIGNvbnN0IGNvbnRlbnRDbXB0RmFjdG9yeSA9IG1vZHVsZUNGUi5yZXNvbHZlQ29tcG9uZW50RmFjdG9yeShjb250ZW50KTtcclxuICAgICAgICBjb25zdCBtb2RhbENvbnRlbnRJbmplY3RvciA9XHJcbiAgICAgICAgICAgIEluamVjdG9yLmNyZWF0ZSh7cHJvdmlkZXJzOiBbe3Byb3ZpZGU6IE5ndEFjdGl2ZU1vZGFsLCB1c2VWYWx1ZTogY29udGV4dH1dLCBwYXJlbnQ6IGNvbnRlbnRJbmplY3Rvcn0pO1xyXG4gICAgICAgIGNvbnN0IGNvbXBvbmVudFJlZiA9IGNvbnRlbnRDbXB0RmFjdG9yeS5jcmVhdGUobW9kYWxDb250ZW50SW5qZWN0b3IpO1xyXG4gICAgICAgIHRoaXMuX2FwcGxpY2F0aW9uUmVmLmF0dGFjaFZpZXcoY29tcG9uZW50UmVmLmhvc3RWaWV3KTtcclxuICAgICAgICByZXR1cm4gbmV3IENvbnRlbnRSZWYoW1tjb21wb25lbnRSZWYubG9jYXRpb24ubmF0aXZlRWxlbWVudF1dLCBjb21wb25lbnRSZWYuaG9zdFZpZXcsIGNvbXBvbmVudFJlZik7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfY3JlYXRlRnJvbUNvbXBvbmVudFJlZihjb21wb25lbnRSZWY6IGFueSkge1xyXG4gICAgICAgIHJldHVybiBuZXcgQ29udGVudFJlZihbW2NvbXBvbmVudFJlZi5fZWxSZWYubmF0aXZlRWxlbWVudF1dLCBudWxsLCBjb21wb25lbnRSZWYpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX3NldEFyaWFIaWRkZW4oZWxlbWVudDogRWxlbWVudCkge1xyXG4gICAgICAgIGNvbnN0IHBhcmVudCA9IGVsZW1lbnQucGFyZW50RWxlbWVudDtcclxuICAgICAgICBpZiAocGFyZW50ICYmIGVsZW1lbnQgIT09IHRoaXMuX2RvY3VtZW50LmJvZHkpIHtcclxuICAgICAgICAgICAgQXJyYXkuZnJvbShwYXJlbnQuY2hpbGRyZW4pLmZvckVhY2goc2libGluZyA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoc2libGluZyAhPT0gZWxlbWVudCAmJiBzaWJsaW5nLm5vZGVOYW1lICE9PSAnU0NSSVBUJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2FyaWFIaWRkZW5WYWx1ZXMuc2V0KHNpYmxpbmcsIHNpYmxpbmcuZ2V0QXR0cmlidXRlKCdhcmlhLWhpZGRlbicpKTtcclxuICAgICAgICAgICAgICAgICAgICBzaWJsaW5nLnNldEF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nLCAndHJ1ZScpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuX3NldEFyaWFIaWRkZW4ocGFyZW50KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfcmV2ZXJ0QXJpYUhpZGRlbigpIHtcclxuICAgICAgICB0aGlzLl9hcmlhSGlkZGVuVmFsdWVzLmZvckVhY2goKHZhbHVlLCBlbGVtZW50KSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgZWxlbWVudC5zZXRBdHRyaWJ1dGUoJ2FyaWEtaGlkZGVuJywgdmFsdWUpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZWxlbWVudC5yZW1vdmVBdHRyaWJ1dGUoJ2FyaWEtaGlkZGVuJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLl9hcmlhSGlkZGVuVmFsdWVzLmNsZWFyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfcmVnaXN0ZXJNb2RhbFJlZihuZ3RNb2RhbFJlZjogTmd0TW9kYWxSZWYpIHtcclxuICAgICAgICBjb25zdCB1bnJlZ2lzdGVyTW9kYWxSZWYgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5fbW9kYWxSZWZzLmluZGV4T2Yobmd0TW9kYWxSZWYpO1xyXG4gICAgICAgICAgICBpZiAoaW5kZXggPiAtMSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fbW9kYWxSZWZzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuX21vZGFsUmVmcy5wdXNoKG5ndE1vZGFsUmVmKTtcclxuICAgICAgICBuZ3RNb2RhbFJlZi5yZXN1bHQudGhlbih1bnJlZ2lzdGVyTW9kYWxSZWYsIHVucmVnaXN0ZXJNb2RhbFJlZik7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfcmVnaXN0ZXJXaW5kb3dDbXB0KG5ndFdpbmRvd0NtcHQ6IENvbXBvbmVudFJlZjxOZ3RNb2RhbFdpbmRvd0NvbXBvbmVudD4pIHtcclxuICAgICAgICB0aGlzLl93aW5kb3dDbXB0cy5wdXNoKG5ndFdpbmRvd0NtcHQpO1xyXG4gICAgICAgIHRoaXMuX2FjdGl2ZVdpbmRvd0NtcHRIYXNDaGFuZ2VkLm5leHQoKTtcclxuXHJcbiAgICAgICAgbmd0V2luZG93Q21wdC5vbkRlc3Ryb3koKCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMuX3dpbmRvd0NtcHRzLmluZGV4T2Yobmd0V2luZG93Q21wdCk7XHJcbiAgICAgICAgICAgIGlmIChpbmRleCA+IC0xKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl93aW5kb3dDbXB0cy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fYWN0aXZlV2luZG93Q21wdEhhc0NoYW5nZWQubmV4dCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19