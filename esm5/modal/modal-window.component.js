/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, HostBinding, HostListener, Inject, Input, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalDismissReasons } from './modal-dismiss-reasons';
import { animate, state, style, transition, trigger } from '@angular/animations';
var NgtModalWindowComponent = /** @class */ (function () {
    function NgtModalWindowComponent(_document, _elRef) {
        this._document = _document;
        this._elRef = _elRef;
        this.backdrop = true;
        this.keyboard = true;
        this.dismissEvent = new Subject();
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.animation = 'open';
        this.tabindex = '-1';
        this.ariaModal = true;
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtModalWindowComponent.prototype.onBackdropClick = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        if (this.backdrop === true && this._elRef.nativeElement === $event.target) {
            this.dismiss(ModalDismissReasons.BACKDROP_CLICK);
        }
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtModalWindowComponent.prototype.onEscKey = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        if (this.keyboard && !$event.defaultPrevented) {
            this.dismiss(ModalDismissReasons.ESC);
        }
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtModalWindowComponent.prototype.onAnimationStart = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.animationAction($event);
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtModalWindowComponent.prototype.onAnimationDone = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.animationAction($event);
    };
    /**
     * @return {?}
     */
    NgtModalWindowComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.class = 'modal fade show d-block' + (this.windowClass ? ' ' + this.windowClass : '');
    };
    /**
     * @param {?} reason
     * @return {?}
     */
    NgtModalWindowComponent.prototype.dismiss = /**
     * @param {?} reason
     * @return {?}
     */
    function (reason) {
        this.dismissEvent.next(reason);
    };
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtModalWindowComponent.prototype.animationAction = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        switch ($event.phaseName) {
            case 'start':
                switch ($event.toState) {
                    case 'open':
                        this.modalOpeningDidStart.next();
                        break;
                    case 'close':
                        this.modalClosingDidStart.next();
                        break;
                }
                break;
            case 'done':
                switch ($event.toState) {
                    case 'open':
                        this.modalOpeningDidDone.next();
                        break;
                    case 'close':
                        this.modalClosingDidDone.next();
                        break;
                }
                break;
        }
    };
    NgtModalWindowComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-modal-window',
                    template: "\n        <div [class]=\"'modal-dialog' + (size ? ' modal-' + size : '') + (centered ? ' modal-dialog-centered' : '') + (scrollableContent ? ' modal-dialog-scrollable' : '')\" role=\"document\">\n            <div class=\"modal-content\">\n                <ng-content></ng-content>\n            </div>\n        </div>\n    ",
                    animations: [
                        trigger('animation', [
                            state('close', style({ opacity: 0, transform: 'scale(0, 0)' })),
                            transition('void => *', [
                                style({ opacity: 0, transform: 'scale(0, 0)' }),
                                animate('0.3s cubic-bezier(0.680, -0.550, 0.265, 1.550)')
                            ]),
                            transition('* => close', animate('0.3s cubic-bezier(0.680, -0.550, 0.265, 1.550)'))
                        ])
                    ]
                }] }
    ];
    /** @nocollapse */
    NgtModalWindowComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
        { type: ElementRef }
    ]; };
    NgtModalWindowComponent.propDecorators = {
        backdrop: [{ type: Input }],
        centered: [{ type: Input }],
        keyboard: [{ type: Input }],
        size: [{ type: Input }],
        scrollableContent: [{ type: Input }],
        windowClass: [{ type: Input }],
        dialogClass: [{ type: Input }],
        dismissEvent: [{ type: Output }],
        modalClosingDidStart: [{ type: Output }],
        modalClosingDidDone: [{ type: Output }],
        modalOpeningDidStart: [{ type: Output }],
        modalOpeningDidDone: [{ type: Output }],
        animation: [{ type: HostBinding, args: ['@animation',] }],
        class: [{ type: HostBinding, args: ['class',] }],
        tabindex: [{ type: HostBinding, args: ['tabindex',] }],
        ariaModal: [{ type: HostBinding, args: ['attr.aria-modal.true',] }],
        onBackdropClick: [{ type: HostListener, args: ['click', ['$event'],] }],
        onEscKey: [{ type: HostListener, args: ['document:keyup.esc', ['$event'],] }],
        onAnimationStart: [{ type: HostListener, args: ['@animation.start', ['$event'],] }],
        onAnimationDone: [{ type: HostListener, args: ['@animation.done', ['$event'],] }]
    };
    return NgtModalWindowComponent;
}());
export { NgtModalWindowComponent };
if (false) {
    /** @type {?} */
    NgtModalWindowComponent.prototype.backdrop;
    /** @type {?} */
    NgtModalWindowComponent.prototype.centered;
    /** @type {?} */
    NgtModalWindowComponent.prototype.keyboard;
    /** @type {?} */
    NgtModalWindowComponent.prototype.size;
    /** @type {?} */
    NgtModalWindowComponent.prototype.scrollableContent;
    /** @type {?} */
    NgtModalWindowComponent.prototype.windowClass;
    /** @type {?} */
    NgtModalWindowComponent.prototype.dialogClass;
    /** @type {?} */
    NgtModalWindowComponent.prototype.dismissEvent;
    /** @type {?} */
    NgtModalWindowComponent.prototype.modalClosingDidStart;
    /** @type {?} */
    NgtModalWindowComponent.prototype.modalClosingDidDone;
    /** @type {?} */
    NgtModalWindowComponent.prototype.modalOpeningDidStart;
    /** @type {?} */
    NgtModalWindowComponent.prototype.modalOpeningDidDone;
    /** @type {?} */
    NgtModalWindowComponent.prototype.animation;
    /** @type {?} */
    NgtModalWindowComponent.prototype.class;
    /** @type {?} */
    NgtModalWindowComponent.prototype.tabindex;
    /** @type {?} */
    NgtModalWindowComponent.prototype.ariaModal;
    /**
     * @type {?}
     * @private
     */
    NgtModalWindowComponent.prototype._document;
    /**
     * @type {?}
     * @private
     */
    NgtModalWindowComponent.prototype._elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtd2luZG93LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibW9kYWwvbW9kYWwtd2luZG93LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFDSCxTQUFTLEVBQ1QsVUFBVSxFQUNWLFdBQVcsRUFBRSxZQUFZLEVBQ3pCLE1BQU0sRUFDTixLQUFLLEVBQ0wsTUFBTSxFQUNULE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDOUQsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVqRjtJQTZESSxpQ0FBc0MsU0FBYyxFQUNoQyxNQUErQjtRQURiLGNBQVMsR0FBVCxTQUFTLENBQUs7UUFDaEMsV0FBTSxHQUFOLE1BQU0sQ0FBeUI7UUF4QzFDLGFBQVEsR0FBcUIsSUFBSSxDQUFDO1FBRWxDLGFBQVEsR0FBRyxJQUFJLENBQUM7UUFNZixpQkFBWSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDN0IseUJBQW9CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNyQyx3QkFBbUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3BDLHlCQUFvQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDckMsd0JBQW1CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUVuQixjQUFTLEdBQUcsTUFBTSxDQUFDO1FBRXJCLGFBQVEsR0FBRyxJQUFJLENBQUM7UUFDSixjQUFTLEdBQUcsSUFBSSxDQUFDO0lBd0J0RCxDQUFDOzs7OztJQXRCa0MsaURBQWU7Ozs7SUFBbEQsVUFBbUQsTUFBTTtRQUNyRCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxLQUFLLE1BQU0sQ0FBQyxNQUFNLEVBQUU7WUFDdkUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUNwRDtJQUNMLENBQUM7Ozs7O0lBRStDLDBDQUFROzs7O0lBQXhELFVBQXlELE1BQU07UUFDM0QsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFO1lBQzNDLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDekM7SUFDTCxDQUFDOzs7OztJQUU2QyxrREFBZ0I7Ozs7SUFBOUQsVUFBK0QsTUFBTTtRQUNqRSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBRTRDLGlEQUFlOzs7O0lBQTVELFVBQTZELE1BQU07UUFDL0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7O0lBTUQsMENBQVE7OztJQUFSO1FBQ0ksSUFBSSxDQUFDLEtBQUssR0FBRyx5QkFBeUIsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUM5RixDQUFDOzs7OztJQUVELHlDQUFPOzs7O0lBQVAsVUFBUSxNQUFNO1FBQ1YsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7SUFFRCxpREFBZTs7OztJQUFmLFVBQWdCLE1BQU07UUFDbEIsUUFBUSxNQUFNLENBQUMsU0FBUyxFQUFFO1lBQ3RCLEtBQUssT0FBTztnQkFDUixRQUFRLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0JBQ3BCLEtBQUssTUFBTTt3QkFDUCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQ2pDLE1BQU07b0JBQ1YsS0FBSyxPQUFPO3dCQUNSLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDakMsTUFBTTtpQkFDYjtnQkFDRCxNQUFNO1lBQ1YsS0FBSyxNQUFNO2dCQUNQLFFBQVEsTUFBTSxDQUFDLE9BQU8sRUFBRTtvQkFDcEIsS0FBSyxNQUFNO3dCQUNQLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDaEMsTUFBTTtvQkFDVixLQUFLLE9BQU87d0JBQ1IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDO3dCQUNoQyxNQUFNO2lCQUNiO2dCQUNELE1BQU07U0FDYjtJQUNMLENBQUM7O2dCQWhHSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsUUFBUSxFQUFFLG9VQU1UO29CQUNELFVBQVUsRUFBRTt3QkFDUixPQUFPLENBQUMsV0FBVyxFQUFFOzRCQUNqQixLQUFLLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxFQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBQyxDQUFDLENBQUM7NEJBQzdELFVBQVUsQ0FBQyxXQUFXLEVBQUU7Z0NBQ3BCLEtBQUssQ0FBQyxFQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBQyxDQUFDO2dDQUM3QyxPQUFPLENBQUMsZ0RBQWdELENBQUM7NkJBQzVELENBQUM7NEJBQ0YsVUFBVSxDQUFDLFlBQVksRUFBRSxPQUFPLENBQUMsZ0RBQWdELENBQUMsQ0FBQzt5QkFDdEYsQ0FBQztxQkFDTDtpQkFDSjs7OztnREEwQ2dCLE1BQU0sU0FBQyxRQUFRO2dCQXZFNUIsVUFBVTs7OzJCQWdDVCxLQUFLOzJCQUNMLEtBQUs7MkJBQ0wsS0FBSzt1QkFDTCxLQUFLO29DQUNMLEtBQUs7OEJBQ0wsS0FBSzs4QkFDTCxLQUFLOytCQUVMLE1BQU07dUNBQ04sTUFBTTtzQ0FDTixNQUFNO3VDQUNOLE1BQU07c0NBQ04sTUFBTTs0QkFFTixXQUFXLFNBQUMsWUFBWTt3QkFDeEIsV0FBVyxTQUFDLE9BQU87MkJBQ25CLFdBQVcsU0FBQyxVQUFVOzRCQUN0QixXQUFXLFNBQUMsc0JBQXNCO2tDQUVsQyxZQUFZLFNBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDOzJCQU1oQyxZQUFZLFNBQUMsb0JBQW9CLEVBQUUsQ0FBQyxRQUFRLENBQUM7bUNBTTdDLFlBQVksU0FBQyxrQkFBa0IsRUFBRSxDQUFDLFFBQVEsQ0FBQztrQ0FJM0MsWUFBWSxTQUFDLGlCQUFpQixFQUFFLENBQUMsUUFBUSxDQUFDOztJQXdDL0MsOEJBQUM7Q0FBQSxBQWpHRCxJQWlHQztTQTdFWSx1QkFBdUI7OztJQUVoQywyQ0FBMkM7O0lBQzNDLDJDQUEwQjs7SUFDMUIsMkNBQXlCOztJQUN6Qix1Q0FBc0I7O0lBQ3RCLG9EQUFtQzs7SUFDbkMsOENBQTZCOztJQUM3Qiw4Q0FBNkI7O0lBRTdCLCtDQUF1Qzs7SUFDdkMsdURBQStDOztJQUMvQyxzREFBOEM7O0lBQzlDLHVEQUErQzs7SUFDL0Msc0RBQThDOztJQUU5Qyw0Q0FBOEM7O0lBQzlDLHdDQUE0Qjs7SUFDNUIsMkNBQXlDOztJQUN6Qyw0Q0FBc0Q7Ozs7O0lBc0IxQyw0Q0FBd0M7Ozs7O0lBQ3hDLHlDQUF1QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERPQ1VNRU5UIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHtcclxuICAgIENvbXBvbmVudCxcclxuICAgIEVsZW1lbnRSZWYsXHJcbiAgICBIb3N0QmluZGluZywgSG9zdExpc3RlbmVyLFxyXG4gICAgSW5qZWN0LFxyXG4gICAgSW5wdXQsIE9uSW5pdCxcclxuICAgIE91dHB1dFxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IE1vZGFsRGlzbWlzc1JlYXNvbnMgfSBmcm9tICcuL21vZGFsLWRpc21pc3MtcmVhc29ucyc7XHJcbmltcG9ydCB7IGFuaW1hdGUsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlciB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC1tb2RhbC13aW5kb3cnLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8ZGl2IFtjbGFzc109XCInbW9kYWwtZGlhbG9nJyArIChzaXplID8gJyBtb2RhbC0nICsgc2l6ZSA6ICcnKSArIChjZW50ZXJlZCA/ICcgbW9kYWwtZGlhbG9nLWNlbnRlcmVkJyA6ICcnKSArIChzY3JvbGxhYmxlQ29udGVudCA/ICcgbW9kYWwtZGlhbG9nLXNjcm9sbGFibGUnIDogJycpXCIgcm9sZT1cImRvY3VtZW50XCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJtb2RhbC1jb250ZW50XCI+XHJcbiAgICAgICAgICAgICAgICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgYCxcclxuICAgIGFuaW1hdGlvbnM6IFtcclxuICAgICAgICB0cmlnZ2VyKCdhbmltYXRpb24nLCBbXHJcbiAgICAgICAgICAgIHN0YXRlKCdjbG9zZScsIHN0eWxlKHtvcGFjaXR5OiAwLCB0cmFuc2Zvcm06ICdzY2FsZSgwLCAwKSd9KSksXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb24oJ3ZvaWQgPT4gKicsIFtcclxuICAgICAgICAgICAgICAgIHN0eWxlKHtvcGFjaXR5OiAwLCB0cmFuc2Zvcm06ICdzY2FsZSgwLCAwKSd9KSxcclxuICAgICAgICAgICAgICAgIGFuaW1hdGUoJzAuM3MgY3ViaWMtYmV6aWVyKDAuNjgwLCAtMC41NTAsIDAuMjY1LCAxLjU1MCknKVxyXG4gICAgICAgICAgICBdKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbignKiA9PiBjbG9zZScsIGFuaW1hdGUoJzAuM3MgY3ViaWMtYmV6aWVyKDAuNjgwLCAtMC41NTAsIDAuMjY1LCAxLjU1MCknKSlcclxuICAgICAgICBdKVxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0TW9kYWxXaW5kb3dDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpIGJhY2tkcm9wOiBib29sZWFuIHwgc3RyaW5nID0gdHJ1ZTtcclxuICAgIEBJbnB1dCgpIGNlbnRlcmVkOiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKSBrZXlib2FyZCA9IHRydWU7XHJcbiAgICBASW5wdXQoKSBzaXplOiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKSBzY3JvbGxhYmxlQ29udGVudDogc3RyaW5nO1xyXG4gICAgQElucHV0KCkgd2luZG93Q2xhc3M6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIGRpYWxvZ0NsYXNzOiBzdHJpbmc7XHJcblxyXG4gICAgQE91dHB1dCgpIGRpc21pc3NFdmVudCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBAT3V0cHV0KCkgbW9kYWxDbG9zaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgQE91dHB1dCgpIG1vZGFsQ2xvc2luZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgQE91dHB1dCgpIG1vZGFsT3BlbmluZ0RpZFN0YXJ0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIEBPdXRwdXQoKSBtb2RhbE9wZW5pbmdEaWREb25lID0gbmV3IFN1YmplY3QoKTtcclxuXHJcbiAgICBASG9zdEJpbmRpbmcoJ0BhbmltYXRpb24nKSBhbmltYXRpb24gPSAnb3Blbic7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzJykgY2xhc3M7XHJcbiAgICBASG9zdEJpbmRpbmcoJ3RhYmluZGV4JykgdGFiaW5kZXggPSAnLTEnO1xyXG4gICAgQEhvc3RCaW5kaW5nKCdhdHRyLmFyaWEtbW9kYWwudHJ1ZScpIGFyaWFNb2RhbCA9IHRydWU7XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snLCBbJyRldmVudCddKSBvbkJhY2tkcm9wQ2xpY2soJGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuYmFja2Ryb3AgPT09IHRydWUgJiYgdGhpcy5fZWxSZWYubmF0aXZlRWxlbWVudCA9PT0gJGV2ZW50LnRhcmdldCkge1xyXG4gICAgICAgICAgICB0aGlzLmRpc21pc3MoTW9kYWxEaXNtaXNzUmVhc29ucy5CQUNLRFJPUF9DTElDSyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmtleXVwLmVzYycsIFsnJGV2ZW50J10pIG9uRXNjS2V5KCRldmVudCkge1xyXG4gICAgICAgIGlmICh0aGlzLmtleWJvYXJkICYmICEkZXZlbnQuZGVmYXVsdFByZXZlbnRlZCkge1xyXG4gICAgICAgICAgICB0aGlzLmRpc21pc3MoTW9kYWxEaXNtaXNzUmVhc29ucy5FU0MpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdAYW5pbWF0aW9uLnN0YXJ0JywgWyckZXZlbnQnXSkgb25BbmltYXRpb25TdGFydCgkZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmFuaW1hdGlvbkFjdGlvbigkZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ0BhbmltYXRpb24uZG9uZScsIFsnJGV2ZW50J10pIG9uQW5pbWF0aW9uRG9uZSgkZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmFuaW1hdGlvbkFjdGlvbigkZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKEBJbmplY3QoRE9DVU1FTlQpIHByaXZhdGUgX2RvY3VtZW50OiBhbnksXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9lbFJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4pIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmNsYXNzID0gJ21vZGFsIGZhZGUgc2hvdyBkLWJsb2NrJyArICh0aGlzLndpbmRvd0NsYXNzID8gJyAnICsgdGhpcy53aW5kb3dDbGFzcyA6ICcnKTtcclxuICAgIH1cclxuXHJcbiAgICBkaXNtaXNzKHJlYXNvbik6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZGlzbWlzc0V2ZW50Lm5leHQocmVhc29uKTtcclxuICAgIH1cclxuXHJcbiAgICBhbmltYXRpb25BY3Rpb24oJGV2ZW50KSB7XHJcbiAgICAgICAgc3dpdGNoICgkZXZlbnQucGhhc2VOYW1lKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ3N0YXJ0JzpcclxuICAgICAgICAgICAgICAgIHN3aXRjaCAoJGV2ZW50LnRvU3RhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdvcGVuJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tb2RhbE9wZW5pbmdEaWRTdGFydC5uZXh0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2Nsb3NlJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tb2RhbENsb3NpbmdEaWRTdGFydC5uZXh0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJ2RvbmUnOlxyXG4gICAgICAgICAgICAgICAgc3dpdGNoICgkZXZlbnQudG9TdGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ29wZW4nOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZGFsT3BlbmluZ0RpZERvbmUubmV4dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdjbG9zZSc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubW9kYWxDbG9zaW5nRGlkRG9uZS5uZXh0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==