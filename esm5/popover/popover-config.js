/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtPopover directive.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the popovers used in the application.
 */
var NgtPopoverConfig = /** @class */ (function () {
    function NgtPopoverConfig() {
        this.autoClose = true;
        this.placement = 'auto';
        this.triggers = 'click';
        this.disablePopover = false;
        this.openDelay = 0;
        this.closeDelay = 0;
    }
    NgtPopoverConfig.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */ NgtPopoverConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtPopoverConfig_Factory() { return new NgtPopoverConfig(); }, token: NgtPopoverConfig, providedIn: "root" });
    return NgtPopoverConfig;
}());
export { NgtPopoverConfig };
if (false) {
    /** @type {?} */
    NgtPopoverConfig.prototype.autoClose;
    /** @type {?} */
    NgtPopoverConfig.prototype.placement;
    /** @type {?} */
    NgtPopoverConfig.prototype.triggers;
    /** @type {?} */
    NgtPopoverConfig.prototype.container;
    /** @type {?} */
    NgtPopoverConfig.prototype.disablePopover;
    /** @type {?} */
    NgtPopoverConfig.prototype.popoverClass;
    /** @type {?} */
    NgtPopoverConfig.prototype.openDelay;
    /** @type {?} */
    NgtPopoverConfig.prototype.closeDelay;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9wb3Zlci1jb25maWcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInBvcG92ZXIvcG9wb3Zlci1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFRM0M7SUFBQTtRQUVJLGNBQVMsR0FBbUMsSUFBSSxDQUFDO1FBQ2pELGNBQVMsR0FBbUIsTUFBTSxDQUFDO1FBQ25DLGFBQVEsR0FBRyxPQUFPLENBQUM7UUFFbkIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFFdkIsY0FBUyxHQUFHLENBQUMsQ0FBQztRQUNkLGVBQVUsR0FBRyxDQUFDLENBQUM7S0FDbEI7O2dCQVZBLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7OzsyQkFSaEM7Q0FrQkMsQUFWRCxJQVVDO1NBVFksZ0JBQWdCOzs7SUFDekIscUNBQWlEOztJQUNqRCxxQ0FBbUM7O0lBQ25DLG9DQUFtQjs7SUFDbkIscUNBQWtCOztJQUNsQiwwQ0FBdUI7O0lBQ3ZCLHdDQUFxQjs7SUFDckIscUNBQWM7O0lBQ2Qsc0NBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFBsYWNlbWVudEFycmF5IH0gZnJvbSAnLi4vdXRpbC9wb3NpdGlvbmluZyc7XHJcblxyXG4vKipcclxuICogQ29uZmlndXJhdGlvbiBzZXJ2aWNlIGZvciB0aGUgTmd0UG9wb3ZlciBkaXJlY3RpdmUuXHJcbiAqIFlvdSBjYW4gaW5qZWN0IHRoaXMgc2VydmljZSwgdHlwaWNhbGx5IGluIHlvdXIgcm9vdCBjb21wb25lbnQsIGFuZCBjdXN0b21pemUgdGhlIHZhbHVlcyBvZiBpdHMgcHJvcGVydGllcyBpblxyXG4gKiBvcmRlciB0byBwcm92aWRlIGRlZmF1bHQgdmFsdWVzIGZvciBhbGwgdGhlIHBvcG92ZXJzIHVzZWQgaW4gdGhlIGFwcGxpY2F0aW9uLlxyXG4gKi9cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQb3BvdmVyQ29uZmlnIHtcclxuICAgIGF1dG9DbG9zZTogYm9vbGVhbiB8ICdpbnNpZGUnIHwgJ291dHNpZGUnID0gdHJ1ZTtcclxuICAgIHBsYWNlbWVudDogUGxhY2VtZW50QXJyYXkgPSAnYXV0byc7XHJcbiAgICB0cmlnZ2VycyA9ICdjbGljayc7XHJcbiAgICBjb250YWluZXI6IHN0cmluZztcclxuICAgIGRpc2FibGVQb3BvdmVyID0gZmFsc2U7XHJcbiAgICBwb3BvdmVyQ2xhc3M6IHN0cmluZztcclxuICAgIG9wZW5EZWxheSA9IDA7XHJcbiAgICBjbG9zZURlbGF5ID0gMDtcclxufVxyXG4iXX0=