/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { NgtPopover, NgtPopoverWindow } from './popover';
import { CommonModule } from '@angular/common';
export { NgtPopover } from './popover';
export { NgtPopoverConfig } from './popover-config';
var NgtPopoverModule = /** @class */ (function () {
    function NgtPopoverModule() {
    }
    /**
     * @return {?}
     */
    NgtPopoverModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtPopoverModule };
    };
    NgtPopoverModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [NgtPopover, NgtPopoverWindow],
                    exports: [NgtPopover],
                    imports: [CommonModule],
                    entryComponents: [NgtPopoverWindow]
                },] }
    ];
    return NgtPopoverModule;
}());
export { NgtPopoverModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9wb3Zlci5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInBvcG92ZXIvcG9wb3Zlci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTlELE9BQU8sRUFBRSxVQUFVLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxXQUFXLENBQUM7QUFDekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxXQUFXLENBQUM7QUFDdkMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFHcEQ7SUFBQTtJQVVBLENBQUM7Ozs7SUFIVSx3QkFBTzs7O0lBQWQ7UUFDSSxPQUFPLEVBQUMsUUFBUSxFQUFFLGdCQUFnQixFQUFDLENBQUM7SUFDeEMsQ0FBQzs7Z0JBVEosUUFBUSxTQUFDO29CQUNOLFlBQVksRUFBRSxDQUFDLFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQztvQkFDNUMsT0FBTyxFQUFFLENBQUMsVUFBVSxDQUFDO29CQUNyQixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7b0JBQ3ZCLGVBQWUsRUFBRSxDQUFDLGdCQUFnQixDQUFDO2lCQUN0Qzs7SUFLRCx1QkFBQztDQUFBLEFBVkQsSUFVQztTQUpZLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBOZ3RQb3BvdmVyLCBOZ3RQb3BvdmVyV2luZG93IH0gZnJvbSAnLi9wb3BvdmVyJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbmV4cG9ydCB7IE5ndFBvcG92ZXIgfSBmcm9tICcuL3BvcG92ZXInO1xyXG5leHBvcnQgeyBOZ3RQb3BvdmVyQ29uZmlnIH0gZnJvbSAnLi9wb3BvdmVyLWNvbmZpZyc7XHJcbmV4cG9ydCB7IFBsYWNlbWVudCB9IGZyb20gJy4uL3V0aWwvcG9zaXRpb25pbmcnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW05ndFBvcG92ZXIsIE5ndFBvcG92ZXJXaW5kb3ddLFxyXG4gICAgZXhwb3J0czogW05ndFBvcG92ZXJdLFxyXG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV0sXHJcbiAgICBlbnRyeUNvbXBvbmVudHM6IFtOZ3RQb3BvdmVyV2luZG93XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0UG9wb3Zlck1vZHVsZSB7XHJcbiAgICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge25nTW9kdWxlOiBOZ3RQb3BvdmVyTW9kdWxlfTtcclxuICAgIH1cclxufVxyXG4iXX0=