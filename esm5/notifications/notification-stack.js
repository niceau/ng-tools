/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ApplicationRef, Inject, Injectable, Injector, RendererFactory2 } from '@angular/core';
import { NgtNotificationComponent } from './notification.component';
import { DOCUMENT } from '@angular/common';
import { isDefined } from '../util/util';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
var NgtNotificationStack = /** @class */ (function () {
    function NgtNotificationStack(_injector, _appRef, _rendererFactory, _document) {
        this._injector = _injector;
        this._appRef = _appRef;
        this._rendererFactory = _rendererFactory;
        this._document = _document;
        this.containerEl = this._document.body;
        this._notificationRefs = [];
        this._notificationAttributes = ['type', 'message', 'timeout', 'typeClass', 'aside', 'title', '_ref'];
        this.initContainer();
    }
    /**
     * @return {?}
     */
    NgtNotificationStack.prototype.initContainer = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var renderer = this._rendererFactory.createRenderer(null, null);
        /** @type {?} */
        var containerEl = renderer.createElement('div');
        /** @type {?} */
        var wrapperEl = renderer.createElement('div');
        renderer.addClass(containerEl, 'notification-overlay-container');
        renderer.addClass(wrapperEl, 'notification-wrapper');
        renderer.appendChild(containerEl, wrapperEl);
        renderer.appendChild(this._document.body, containerEl);
        this.containerEl = wrapperEl;
    };
    /**
     * @param {?} moduleCFR
     * @param {?} options
     * @return {?}
     */
    NgtNotificationStack.prototype.show = /**
     * @param {?} moduleCFR
     * @param {?} options
     * @return {?}
     */
    function (moduleCFR, options) {
        /** @type {?} */
        var notificationCmptRef = this._attachNotificationComponent(moduleCFR, this.containerEl);
        this._registerNotificationRef(notificationCmptRef.instance);
        this._applyNotificationOptions(notificationCmptRef.instance, (/** @type {?} */ (Object.assign({}, { _ref: notificationCmptRef }, options))));
    };
    /**
     * @return {?}
     */
    NgtNotificationStack.prototype.clearAll = /**
     * @return {?}
     */
    function () {
        this._notificationRefs.forEach((/**
         * @param {?} ngtNotificationRef
         * @return {?}
         */
        function (ngtNotificationRef) { return ngtNotificationRef.closeNotification(); }));
    };
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @return {?}
     */
    NgtNotificationStack.prototype._attachNotificationComponent = /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @return {?}
     */
    function (moduleCFR, containerEl) {
        /** @type {?} */
        var notificationFactory = moduleCFR.resolveComponentFactory(NgtNotificationComponent);
        /** @type {?} */
        var notificationCmptRef = notificationFactory.create(this._injector);
        // Attach component to the appRef so that it's inside the ng component tree
        this._appRef.attachView(notificationCmptRef.hostView);
        // Append DOM element to the body
        containerEl.appendChild(notificationCmptRef.location.nativeElement);
        return notificationCmptRef;
    };
    /**
     * @private
     * @param {?} notificationInstance
     * @param {?} options
     * @return {?}
     */
    NgtNotificationStack.prototype._applyNotificationOptions = /**
     * @private
     * @param {?} notificationInstance
     * @param {?} options
     * @return {?}
     */
    function (notificationInstance, options) {
        this._notificationAttributes.forEach((/**
         * @param {?} optionName
         * @return {?}
         */
        function (optionName) {
            if (isDefined(options[optionName])) {
                notificationInstance[optionName] = options[optionName];
            }
        }));
    };
    /**
     * @private
     * @param {?} ngtNotificationComponent
     * @return {?}
     */
    NgtNotificationStack.prototype._registerNotificationRef = /**
     * @private
     * @param {?} ngtNotificationComponent
     * @return {?}
     */
    function (ngtNotificationComponent) {
        var _this = this;
        /** @type {?} */
        var _unregisterNotificationRef = (/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var index = _this._notificationRefs.indexOf(ngtNotificationComponent);
            if (index > -1) {
                _this._notificationRefs.splice(index, 1);
            }
        });
        this._notificationRefs.push(ngtNotificationComponent);
        ngtNotificationComponent.result.then(_unregisterNotificationRef, _unregisterNotificationRef);
    };
    NgtNotificationStack.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    NgtNotificationStack.ctorParameters = function () { return [
        { type: Injector },
        { type: ApplicationRef },
        { type: RendererFactory2 },
        { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] }
    ]; };
    /** @nocollapse */ NgtNotificationStack.ngInjectableDef = i0.defineInjectable({ factory: function NgtNotificationStack_Factory() { return new NgtNotificationStack(i0.inject(i0.INJECTOR), i0.inject(i0.ApplicationRef), i0.inject(i0.RendererFactory2), i0.inject(i1.DOCUMENT)); }, token: NgtNotificationStack, providedIn: "root" });
    return NgtNotificationStack;
}());
export { NgtNotificationStack };
if (false) {
    /** @type {?} */
    NgtNotificationStack.prototype.containerEl;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._notificationRefs;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._notificationAttributes;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._injector;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._appRef;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._rendererFactory;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._document;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLXN0YWNrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbi1zdGFjay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUNILGNBQWMsRUFHZCxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFDakQsTUFBTSxlQUFlLENBQUM7QUFHdkIsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDcEUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxjQUFjLENBQUM7OztBQUV6QztJQU1JLDhCQUFvQixTQUFtQixFQUNuQixPQUF1QixFQUN2QixnQkFBa0MsRUFDaEIsU0FBYztRQUhoQyxjQUFTLEdBQVQsU0FBUyxDQUFVO1FBQ25CLFlBQU8sR0FBUCxPQUFPLENBQWdCO1FBQ3ZCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDaEIsY0FBUyxHQUFULFNBQVMsQ0FBSztRQVBwRCxnQkFBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1FBQzFCLHNCQUFpQixHQUErQixFQUFFLENBQUM7UUFDbkQsNEJBQXVCLEdBQUcsQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztRQU1wRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELDRDQUFhOzs7SUFBYjs7WUFDVSxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDOztZQUMzRCxXQUFXLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7O1lBQzNDLFNBQVMsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQztRQUMvQyxRQUFRLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxnQ0FBZ0MsQ0FBQyxDQUFDO1FBQ2pFLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLHNCQUFzQixDQUFDLENBQUM7UUFDckQsUUFBUSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDN0MsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQztJQUNqQyxDQUFDOzs7Ozs7SUFFRCxtQ0FBSTs7Ozs7SUFBSixVQUFLLFNBQW1DLEVBQUUsT0FBd0I7O1lBQ3hELG1CQUFtQixHQUEyQyxJQUFJLENBQUMsNEJBQTRCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDbEksSUFBSSxDQUFDLHdCQUF3QixDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsbUJBQWlCLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEVBQUMsSUFBSSxFQUFFLG1CQUFtQixFQUFDLEVBQUUsT0FBTyxDQUFDLEVBQUEsQ0FBQyxDQUFDO0lBQzNJLENBQUM7Ozs7SUFFRCx1Q0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTzs7OztRQUFDLFVBQUEsa0JBQWtCLElBQUksT0FBQSxrQkFBa0IsQ0FBQyxpQkFBaUIsRUFBRSxFQUF0QyxDQUFzQyxFQUFDLENBQUM7SUFDakcsQ0FBQzs7Ozs7OztJQUVPLDJEQUE0Qjs7Ozs7O0lBQXBDLFVBQXFDLFNBQW1DLEVBQUUsV0FBZ0I7O1lBRWhGLG1CQUFtQixHQUFHLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyx3QkFBd0IsQ0FBQzs7WUFDakYsbUJBQW1CLEdBQUcsbUJBQW1CLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDdEUsMkVBQTJFO1FBQzNFLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RELGlDQUFpQztRQUNqQyxXQUFXLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwRSxPQUFPLG1CQUFtQixDQUFDO0lBQy9CLENBQUM7Ozs7Ozs7SUFFTyx3REFBeUI7Ozs7OztJQUFqQyxVQUFrQyxvQkFBcUMsRUFBRSxPQUFlO1FBQ3BGLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQyxVQUFrQjtZQUNwRCxJQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBRTtnQkFDaEMsb0JBQW9CLENBQUMsVUFBVSxDQUFDLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQzFEO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFTyx1REFBd0I7Ozs7O0lBQWhDLFVBQWlDLHdCQUFrRDtRQUFuRixpQkFTQzs7WUFSUywwQkFBMEI7OztRQUFHOztnQkFDekIsS0FBSyxHQUFHLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUM7WUFDdEUsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQ1osS0FBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDM0M7UUFDTCxDQUFDLENBQUE7UUFDRCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLENBQUM7UUFDdEQsd0JBQXdCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwQkFBMEIsRUFBRSwwQkFBMEIsQ0FBQyxDQUFDO0lBQ2pHLENBQUM7O2dCQTlESixVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7O2dCQVJSLFFBQVE7Z0JBSDVCLGNBQWM7Z0JBR2dCLGdCQUFnQjtnREFpQmpDLE1BQU0sU0FBQyxRQUFROzs7K0JBckJoQztDQTJFQyxBQS9ERCxJQStEQztTQTlEWSxvQkFBb0I7OztJQUM3QiwyQ0FBa0M7Ozs7O0lBQ2xDLGlEQUEyRDs7Ozs7SUFDM0QsdURBQXdHOzs7OztJQUU1Rix5Q0FBMkI7Ozs7O0lBQzNCLHVDQUErQjs7Ozs7SUFDL0IsZ0RBQTBDOzs7OztJQUMxQyx5Q0FBd0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgQXBwbGljYXRpb25SZWYsXHJcbiAgICBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXHJcbiAgICBDb21wb25lbnRSZWYsXHJcbiAgICBJbmplY3QsIEluamVjdGFibGUsIEluamVjdG9yLCBSZW5kZXJlckZhY3RvcnkyXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBOZ3ROb3RpZmljYXRpb24gfSBmcm9tICcuL25vdGlmaWNhdGlvbi5tb2RlbCc7XHJcbmltcG9ydCB7IE5ndE5vdGlmaWNhdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vbm90aWZpY2F0aW9uLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERPQ1VNRU5UIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgaXNEZWZpbmVkIH0gZnJvbSAnLi4vdXRpbC91dGlsJztcclxuXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTmd0Tm90aWZpY2F0aW9uU3RhY2sge1xyXG4gICAgY29udGFpbmVyRWwgPSB0aGlzLl9kb2N1bWVudC5ib2R5O1xyXG4gICAgcHJpdmF0ZSBfbm90aWZpY2F0aW9uUmVmczogTmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50W10gPSBbXTtcclxuICAgIHByaXZhdGUgX25vdGlmaWNhdGlvbkF0dHJpYnV0ZXMgPSBbJ3R5cGUnLCAnbWVzc2FnZScsICd0aW1lb3V0JywgJ3R5cGVDbGFzcycsICdhc2lkZScsICd0aXRsZScsICdfcmVmJ107XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfaW5qZWN0b3I6IEluamVjdG9yLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfYXBwUmVmOiBBcHBsaWNhdGlvblJlZixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX3JlbmRlcmVyRmFjdG9yeTogUmVuZGVyZXJGYWN0b3J5MixcclxuICAgICAgICAgICAgICAgIEBJbmplY3QoRE9DVU1FTlQpIHByaXZhdGUgX2RvY3VtZW50OiBhbnkpIHtcclxuICAgICAgICB0aGlzLmluaXRDb250YWluZXIoKTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0Q29udGFpbmVyKCkge1xyXG4gICAgICAgIGNvbnN0IHJlbmRlcmVyID0gdGhpcy5fcmVuZGVyZXJGYWN0b3J5LmNyZWF0ZVJlbmRlcmVyKG51bGwsIG51bGwpO1xyXG4gICAgICAgIGNvbnN0IGNvbnRhaW5lckVsID0gcmVuZGVyZXIuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XHJcbiAgICAgICAgY29uc3Qgd3JhcHBlckVsID0gcmVuZGVyZXIuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XHJcbiAgICAgICAgcmVuZGVyZXIuYWRkQ2xhc3MoY29udGFpbmVyRWwsICdub3RpZmljYXRpb24tb3ZlcmxheS1jb250YWluZXInKTtcclxuICAgICAgICByZW5kZXJlci5hZGRDbGFzcyh3cmFwcGVyRWwsICdub3RpZmljYXRpb24td3JhcHBlcicpO1xyXG4gICAgICAgIHJlbmRlcmVyLmFwcGVuZENoaWxkKGNvbnRhaW5lckVsLCB3cmFwcGVyRWwpO1xyXG4gICAgICAgIHJlbmRlcmVyLmFwcGVuZENoaWxkKHRoaXMuX2RvY3VtZW50LmJvZHksIGNvbnRhaW5lckVsKTtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lckVsID0gd3JhcHBlckVsO1xyXG4gICAgfVxyXG5cclxuICAgIHNob3cobW9kdWxlQ0ZSOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIG9wdGlvbnM6IE5ndE5vdGlmaWNhdGlvbikge1xyXG4gICAgICAgIGNvbnN0IG5vdGlmaWNhdGlvbkNtcHRSZWY6IENvbXBvbmVudFJlZjxOZ3ROb3RpZmljYXRpb25Db21wb25lbnQ+ID0gdGhpcy5fYXR0YWNoTm90aWZpY2F0aW9uQ29tcG9uZW50KG1vZHVsZUNGUiwgdGhpcy5jb250YWluZXJFbCk7XHJcbiAgICAgICAgdGhpcy5fcmVnaXN0ZXJOb3RpZmljYXRpb25SZWYobm90aWZpY2F0aW9uQ21wdFJlZi5pbnN0YW5jZSk7XHJcbiAgICAgICAgdGhpcy5fYXBwbHlOb3RpZmljYXRpb25PcHRpb25zKG5vdGlmaWNhdGlvbkNtcHRSZWYuaW5zdGFuY2UsIDxOZ3ROb3RpZmljYXRpb24+T2JqZWN0LmFzc2lnbih7fSwge19yZWY6IG5vdGlmaWNhdGlvbkNtcHRSZWZ9LCBvcHRpb25zKSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2xlYXJBbGwoKSB7XHJcbiAgICAgICAgdGhpcy5fbm90aWZpY2F0aW9uUmVmcy5mb3JFYWNoKG5ndE5vdGlmaWNhdGlvblJlZiA9PiBuZ3ROb3RpZmljYXRpb25SZWYuY2xvc2VOb3RpZmljYXRpb24oKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfYXR0YWNoTm90aWZpY2F0aW9uQ29tcG9uZW50KG1vZHVsZUNGUjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb250YWluZXJFbDogYW55KTpcclxuICAgICAgICBDb21wb25lbnRSZWY8Tmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50PiB7XHJcbiAgICAgICAgY29uc3Qgbm90aWZpY2F0aW9uRmFjdG9yeSA9IG1vZHVsZUNGUi5yZXNvbHZlQ29tcG9uZW50RmFjdG9yeShOZ3ROb3RpZmljYXRpb25Db21wb25lbnQpO1xyXG4gICAgICAgIGNvbnN0IG5vdGlmaWNhdGlvbkNtcHRSZWYgPSBub3RpZmljYXRpb25GYWN0b3J5LmNyZWF0ZSh0aGlzLl9pbmplY3Rvcik7XHJcbiAgICAgICAgLy8gQXR0YWNoIGNvbXBvbmVudCB0byB0aGUgYXBwUmVmIHNvIHRoYXQgaXQncyBpbnNpZGUgdGhlIG5nIGNvbXBvbmVudCB0cmVlXHJcbiAgICAgICAgdGhpcy5fYXBwUmVmLmF0dGFjaFZpZXcobm90aWZpY2F0aW9uQ21wdFJlZi5ob3N0Vmlldyk7XHJcbiAgICAgICAgLy8gQXBwZW5kIERPTSBlbGVtZW50IHRvIHRoZSBib2R5XHJcbiAgICAgICAgY29udGFpbmVyRWwuYXBwZW5kQ2hpbGQobm90aWZpY2F0aW9uQ21wdFJlZi5sb2NhdGlvbi5uYXRpdmVFbGVtZW50KTtcclxuICAgICAgICByZXR1cm4gbm90aWZpY2F0aW9uQ21wdFJlZjtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9hcHBseU5vdGlmaWNhdGlvbk9wdGlvbnMobm90aWZpY2F0aW9uSW5zdGFuY2U6IE5ndE5vdGlmaWNhdGlvbiwgb3B0aW9uczogT2JqZWN0KTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fbm90aWZpY2F0aW9uQXR0cmlidXRlcy5mb3JFYWNoKChvcHRpb25OYW1lOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgaWYgKGlzRGVmaW5lZChvcHRpb25zW29wdGlvbk5hbWVdKSkge1xyXG4gICAgICAgICAgICAgICAgbm90aWZpY2F0aW9uSW5zdGFuY2Vbb3B0aW9uTmFtZV0gPSBvcHRpb25zW29wdGlvbk5hbWVdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfcmVnaXN0ZXJOb3RpZmljYXRpb25SZWYobmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50OiBOZ3ROb3RpZmljYXRpb25Db21wb25lbnQpIHtcclxuICAgICAgICBjb25zdCBfdW5yZWdpc3Rlck5vdGlmaWNhdGlvblJlZiA9ICgpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLl9ub3RpZmljYXRpb25SZWZzLmluZGV4T2Yobmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50KTtcclxuICAgICAgICAgICAgaWYgKGluZGV4ID4gLTEpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX25vdGlmaWNhdGlvblJlZnMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5fbm90aWZpY2F0aW9uUmVmcy5wdXNoKG5ndE5vdGlmaWNhdGlvbkNvbXBvbmVudCk7XHJcbiAgICAgICAgbmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50LnJlc3VsdC50aGVuKF91bnJlZ2lzdGVyTm90aWZpY2F0aW9uUmVmLCBfdW5yZWdpc3Rlck5vdGlmaWNhdGlvblJlZik7XHJcbiAgICB9XHJcbn1cclxuIl19