/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, HostBinding, Input } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { Subject } from 'rxjs';
var NgtNotificationComponent = /** @class */ (function () {
    function NgtNotificationComponent(elRef) {
        var _this = this;
        this.elRef = elRef;
        this.animation = true;
        this.display = 'block';
        this.aside = false;
        this.timer = new Subject();
        this.progress = 0;
        this.lastProgress = 0;
        this.result = new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            _this._resolve = resolve;
            _this._reject = reject;
        }));
    }
    /**
     * @return {?}
     */
    NgtNotificationComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.timeout > 0) {
            this.initProgressBar();
        }
    };
    /**
     * @return {?}
     */
    NgtNotificationComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        this.timeout && this.timer.next();
    };
    /**
     * @return {?}
     */
    NgtNotificationComponent.prototype.initProgressBar = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var k = this.timeout / 100;
        this.progressBar = this.elRef.nativeElement.querySelector('.notification-progress-bar');
        /** @type {?} */
        var sub = this.timer.subscribe((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var started = new Date().getTime();
            _this.interval = setInterval((/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var curTime = new Date().getTime();
                _this.progress = _this.lastProgress + (curTime - started) / k;
                if (_this.progress >= 100) {
                    clearInterval(_this.interval);
                    sub.unsubscribe();
                    _this._ref.destroy();
                }
                else {
                    _this.progressBar['style'].width = _this.progress + '%';
                }
            }), 0);
        }));
    };
    /**
     * @return {?}
     */
    NgtNotificationComponent.prototype.closeNotification = /**
     * @return {?}
     */
    function () {
        this._resolve();
        clearInterval(this.interval);
        this._ref.destroy();
    };
    /**
     * @return {?}
     */
    NgtNotificationComponent.prototype.onMouseenter = /**
     * @return {?}
     */
    function () {
        clearInterval(this.interval);
        this.lastProgress = this.progress;
    };
    /**
     * @return {?}
     */
    NgtNotificationComponent.prototype.onMouseleave = /**
     * @return {?}
     */
    function () {
        this.timer.next();
    };
    NgtNotificationComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-notification',
                    template: "\n        <div\n            (mouseenter)=\"onMouseenter()\" (mouseleave)=\"onMouseleave()\"\n            class=\"notification show {{ typeClass }} {{ aside ? 'with-aside' : '' }}\">\n            <div class=\"notification-progress-bar\"></div>\n            <div *ngIf=\"aside\" class=\"notification-aside\" [innerHtml]=\"aside\"></div>\n            <div class=\"notification-inner\">\n                <div class=\"notification-header\">\n                    <span class=\"notification-title\" [innerHtml]=\"title\"></span>\n                    <button (click)=\"closeNotification()\" class=\"notification-close\"><i class=\"ft-x\"></i></button>\n                </div>\n                <div class=\"notification-body\" [innerHtml]=\"message\"></div>\n            </div>\n        </div>\n\n    ",
                    animations: [
                        trigger('flyInOut', [
                            transition('void => *', [
                                style({ transform: 'translateX(-100%)', opacity: 0 }),
                                animate(150)
                            ]),
                            transition('* => void', [
                                animate(250, style({ transform: 'translateX(100%)', opacity: 0 }))
                            ])
                        ])
                    ]
                }] }
    ];
    /** @nocollapse */
    NgtNotificationComponent.ctorParameters = function () { return [
        { type: ElementRef }
    ]; };
    NgtNotificationComponent.propDecorators = {
        animation: [{ type: HostBinding, args: ['@flyInOut',] }],
        display: [{ type: HostBinding, args: ['style.display',] }],
        type: [{ type: Input }],
        typeClass: [{ type: Input }],
        title: [{ type: Input }],
        message: [{ type: Input }],
        aside: [{ type: Input }],
        timeout: [{ type: Input }],
        _ref: [{ type: Input }]
    };
    return NgtNotificationComponent;
}());
export { NgtNotificationComponent };
if (false) {
    /** @type {?} */
    NgtNotificationComponent.prototype.animation;
    /** @type {?} */
    NgtNotificationComponent.prototype.display;
    /** @type {?} */
    NgtNotificationComponent.prototype.type;
    /** @type {?} */
    NgtNotificationComponent.prototype.typeClass;
    /** @type {?} */
    NgtNotificationComponent.prototype.title;
    /** @type {?} */
    NgtNotificationComponent.prototype.message;
    /** @type {?} */
    NgtNotificationComponent.prototype.aside;
    /** @type {?} */
    NgtNotificationComponent.prototype.timeout;
    /** @type {?} */
    NgtNotificationComponent.prototype._ref;
    /** @type {?} */
    NgtNotificationComponent.prototype.timer;
    /** @type {?} */
    NgtNotificationComponent.prototype.progressBar;
    /** @type {?} */
    NgtNotificationComponent.prototype.interval;
    /** @type {?} */
    NgtNotificationComponent.prototype.progress;
    /** @type {?} */
    NgtNotificationComponent.prototype.lastProgress;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationComponent.prototype._resolve;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationComponent.prototype._reject;
    /**
     * A promise that is resolved when the notification is closed
     * @type {?}
     */
    NgtNotificationComponent.prototype.result;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationComponent.prototype.elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQWlCLFNBQVMsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUNqRyxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDMUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUkvQjtJQXFESSxrQ0FBb0IsS0FBaUI7UUFBckMsaUJBS0M7UUFMbUIsVUFBSyxHQUFMLEtBQUssQ0FBWTtRQXRCWCxjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2IsWUFBTyxHQUFHLE9BQU8sQ0FBQztRQUt2QyxVQUFLLEdBQUcsS0FBSyxDQUFDO1FBR3ZCLFVBQUssR0FBRyxJQUFJLE9BQU8sRUFBVSxDQUFDO1FBRzlCLGFBQVEsR0FBRyxDQUFDLENBQUM7UUFDYixpQkFBWSxHQUFHLENBQUMsQ0FBQztRQVViLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxPQUFPOzs7OztRQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDdEMsS0FBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7WUFDeEIsS0FBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDMUIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsMkNBQVE7OztJQUFSO1FBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsRUFBRTtZQUNsQixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDMUI7SUFDTCxDQUFDOzs7O0lBRUQsa0RBQWU7OztJQUFmO1FBQ0ksSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCxrREFBZTs7O0lBQWY7UUFBQSxpQkFtQkM7O1lBbEJTLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUc7UUFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsNEJBQTRCLENBQUMsQ0FBQzs7WUFDbEYsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUzs7O1FBQzVCOztnQkFDVSxPQUFPLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUU7WUFDcEMsS0FBSSxDQUFDLFFBQVEsR0FBRyxXQUFXOzs7WUFBQzs7b0JBQ2xCLE9BQU8sR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLE9BQU8sRUFBRTtnQkFDcEMsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDNUQsSUFBSSxLQUFJLENBQUMsUUFBUSxJQUFJLEdBQUcsRUFBRTtvQkFDdEIsYUFBYSxDQUFDLEtBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDN0IsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUNsQixLQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO2lCQUN2QjtxQkFBTTtvQkFDSCxLQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQztpQkFDekQ7WUFDTCxDQUFDLEdBQUUsQ0FBQyxDQUFDLENBQUM7UUFDVixDQUFDLEVBQ0o7SUFDTCxDQUFDOzs7O0lBRUQsb0RBQWlCOzs7SUFBakI7UUFDSSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEIsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3hCLENBQUM7Ozs7SUFFRCwrQ0FBWTs7O0lBQVo7UUFDSSxhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN0QyxDQUFDOzs7O0lBRUQsK0NBQVk7OztJQUFaO1FBQ0ksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN0QixDQUFDOztnQkF4R0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLFFBQVEsRUFBRSwweEJBZVQ7b0JBQ0QsVUFBVSxFQUFFO3dCQUNSLE9BQU8sQ0FBQyxVQUFVLEVBQUU7NEJBQ2hCLFVBQVUsQ0FBQyxXQUFXLEVBQUU7Z0NBQ3BCLEtBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSxtQkFBbUIsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUM7Z0NBQ25ELE9BQU8sQ0FBQyxHQUFHLENBQUM7NkJBQ2YsQ0FBQzs0QkFDRixVQUFVLENBQUMsV0FBVyxFQUFFO2dDQUNwQixPQUFPLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSxrQkFBa0IsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUMsQ0FBQzs2QkFDbkUsQ0FBQzt5QkFDTCxDQUFDO3FCQUNMO2lCQUNKOzs7O2dCQW5Da0MsVUFBVTs7OzRCQXFDeEMsV0FBVyxTQUFDLFdBQVc7MEJBQ3ZCLFdBQVcsU0FBQyxlQUFlO3VCQUMzQixLQUFLOzRCQUNMLEtBQUs7d0JBQ0wsS0FBSzswQkFDTCxLQUFLO3dCQUNMLEtBQUs7MEJBQ0wsS0FBSzt1QkFDTCxLQUFLOztJQWtFViwrQkFBQztDQUFBLEFBekdELElBeUdDO1NBM0VZLHdCQUF3Qjs7O0lBQ2pDLDZDQUEyQzs7SUFDM0MsMkNBQWdEOztJQUNoRCx3Q0FBbUI7O0lBQ25CLDZDQUEyQjs7SUFDM0IseUNBQXVCOztJQUN2QiwyQ0FBeUI7O0lBQ3pCLHlDQUF1Qjs7SUFDdkIsMkNBQXNCOztJQUN0Qix3Q0FBbUI7O0lBQ25CLHlDQUE4Qjs7SUFDOUIsK0NBQXdCOztJQUN4Qiw0Q0FBUzs7SUFDVCw0Q0FBYTs7SUFDYixnREFBaUI7Ozs7O0lBRWpCLDRDQUF5Qzs7Ozs7SUFDekMsMkNBQXdDOzs7OztJQUl4QywwQ0FBcUI7Ozs7O0lBRVQseUNBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWZ0ZXJWaWV3SW5pdCwgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBIb3N0QmluZGluZywgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBhbmltYXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlciB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcblxyXG5pbXBvcnQgeyBOZ3ROb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbmd0LW5vdGlmaWNhdGlvbicsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxkaXZcclxuICAgICAgICAgICAgKG1vdXNlZW50ZXIpPVwib25Nb3VzZWVudGVyKClcIiAobW91c2VsZWF2ZSk9XCJvbk1vdXNlbGVhdmUoKVwiXHJcbiAgICAgICAgICAgIGNsYXNzPVwibm90aWZpY2F0aW9uIHNob3cge3sgdHlwZUNsYXNzIH19IHt7IGFzaWRlID8gJ3dpdGgtYXNpZGUnIDogJycgfX1cIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm5vdGlmaWNhdGlvbi1wcm9ncmVzcy1iYXJcIj48L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiAqbmdJZj1cImFzaWRlXCIgY2xhc3M9XCJub3RpZmljYXRpb24tYXNpZGVcIiBbaW5uZXJIdG1sXT1cImFzaWRlXCI+PC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJub3RpZmljYXRpb24taW5uZXJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJub3RpZmljYXRpb24taGVhZGVyXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJub3RpZmljYXRpb24tdGl0bGVcIiBbaW5uZXJIdG1sXT1cInRpdGxlXCI+PC9zcGFuPlxyXG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gKGNsaWNrKT1cImNsb3NlTm90aWZpY2F0aW9uKClcIiBjbGFzcz1cIm5vdGlmaWNhdGlvbi1jbG9zZVwiPjxpIGNsYXNzPVwiZnQteFwiPjwvaT48L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm5vdGlmaWNhdGlvbi1ib2R5XCIgW2lubmVySHRtbF09XCJtZXNzYWdlXCI+PC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG5cclxuICAgIGAsXHJcbiAgICBhbmltYXRpb25zOiBbXHJcbiAgICAgICAgdHJpZ2dlcignZmx5SW5PdXQnLCBbXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb24oJ3ZvaWQgPT4gKicsIFtcclxuICAgICAgICAgICAgICAgIHN0eWxlKHt0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKC0xMDAlKScsIG9wYWNpdHk6IDB9KSxcclxuICAgICAgICAgICAgICAgIGFuaW1hdGUoMTUwKVxyXG4gICAgICAgICAgICBdKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbignKiA9PiB2b2lkJywgW1xyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZSgyNTAsIHN0eWxlKHt0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDEwMCUpJywgb3BhY2l0eTogMH0pKVxyXG4gICAgICAgICAgICBdKVxyXG4gICAgICAgIF0pXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3ROb3RpZmljYXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQge1xyXG4gICAgQEhvc3RCaW5kaW5nKCdAZmx5SW5PdXQnKSBhbmltYXRpb24gPSB0cnVlO1xyXG4gICAgQEhvc3RCaW5kaW5nKCdzdHlsZS5kaXNwbGF5JykgZGlzcGxheSA9ICdibG9jayc7XHJcbiAgICBASW5wdXQoKSB0eXBlOiBhbnk7XHJcbiAgICBASW5wdXQoKSB0eXBlQ2xhc3M6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIHRpdGxlOiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKSBtZXNzYWdlOiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKSBhc2lkZSA9IGZhbHNlO1xyXG4gICAgQElucHV0KCkgdGltZW91dDogYW55O1xyXG4gICAgQElucHV0KCkgX3JlZjogYW55O1xyXG4gICAgdGltZXIgPSBuZXcgU3ViamVjdDxudW1iZXI+KCk7XHJcbiAgICBwcm9ncmVzc0JhcjogRWxlbWVudFJlZjtcclxuICAgIGludGVydmFsO1xyXG4gICAgcHJvZ3Jlc3MgPSAwO1xyXG4gICAgbGFzdFByb2dyZXNzID0gMDtcclxuXHJcbiAgICBwcml2YXRlIF9yZXNvbHZlOiAocmVzdWx0PzogYW55KSA9PiB2b2lkO1xyXG4gICAgcHJpdmF0ZSBfcmVqZWN0OiAocmVhc29uPzogYW55KSA9PiB2b2lkO1xyXG4gICAgLyoqXHJcbiAgICAgKiBBIHByb21pc2UgdGhhdCBpcyByZXNvbHZlZCB3aGVuIHRoZSBub3RpZmljYXRpb24gaXMgY2xvc2VkXHJcbiAgICAgKi9cclxuICAgIHJlc3VsdDogUHJvbWlzZTxhbnk+O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZWxSZWY6IEVsZW1lbnRSZWYpIHtcclxuICAgICAgICB0aGlzLnJlc3VsdCA9IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fcmVzb2x2ZSA9IHJlc29sdmU7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlamVjdCA9IHJlamVjdDtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy50aW1lb3V0ID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLmluaXRQcm9ncmVzc0JhcigpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICAgICAgdGhpcy50aW1lb3V0ICYmIHRoaXMudGltZXIubmV4dCgpO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXRQcm9ncmVzc0JhcigpIHtcclxuICAgICAgICBjb25zdCBrID0gdGhpcy50aW1lb3V0IC8gMTAwO1xyXG4gICAgICAgIHRoaXMucHJvZ3Jlc3NCYXIgPSB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignLm5vdGlmaWNhdGlvbi1wcm9ncmVzcy1iYXInKTtcclxuICAgICAgICBjb25zdCBzdWIgPSB0aGlzLnRpbWVyLnN1YnNjcmliZShcclxuICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc3RhcnRlZCA9IG5ldyBEYXRlKCkuZ2V0VGltZSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnRlcnZhbCA9IHNldEludGVydmFsKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjdXJUaW1lID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9ncmVzcyA9IHRoaXMubGFzdFByb2dyZXNzICsgKGN1clRpbWUgLSBzdGFydGVkKSAvIGs7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMucHJvZ3Jlc3MgPj0gMTAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5pbnRlcnZhbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1Yi51bnN1YnNjcmliZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9yZWYuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucHJvZ3Jlc3NCYXJbJ3N0eWxlJ10ud2lkdGggPSB0aGlzLnByb2dyZXNzICsgJyUnO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sIDApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBjbG9zZU5vdGlmaWNhdGlvbigpIHtcclxuICAgICAgICB0aGlzLl9yZXNvbHZlKCk7XHJcbiAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLmludGVydmFsKTtcclxuICAgICAgICB0aGlzLl9yZWYuZGVzdHJveSgpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uTW91c2VlbnRlcigpIHtcclxuICAgICAgICBjbGVhckludGVydmFsKHRoaXMuaW50ZXJ2YWwpO1xyXG4gICAgICAgIHRoaXMubGFzdFByb2dyZXNzID0gdGhpcy5wcm9ncmVzcztcclxuICAgIH1cclxuXHJcbiAgICBvbk1vdXNlbGVhdmUoKSB7XHJcbiAgICAgICAgdGhpcy50aW1lci5uZXh0KCk7XHJcbiAgICB9XHJcbn1cclxuIl19