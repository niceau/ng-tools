/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ComponentFactoryResolver, Injectable } from '@angular/core';
import { NgtNotificationType } from './notification.model';
import { NgtNotificationStack } from './notification-stack';
import * as i0 from "@angular/core";
import * as i1 from "./notification-stack";
var NgtNotificationService = /** @class */ (function () {
    function NgtNotificationService(stack, _moduleCFR) {
        this.stack = stack;
        this._moduleCFR = _moduleCFR;
        this.timeout = 2500;
        this.notificationsData = {
            success: {
                typeClass: 'notification-success',
                aside: '<i class="ft-check"></i>'
            },
            error: {
                typeClass: 'notification-danger',
                aside: '<i class="ft-x"></i>'
            },
            info: {
                typeClass: 'notification-info',
                aside: '<i class="ft-info"></i>'
            },
            warning: {
                typeClass: 'notification-warning',
                aside: '<i class="ft-alert-triangle"></i>'
            },
        };
    }
    /**
     * @param {?} type
     * @param {?} kind
     * @return {?}
     */
    NgtNotificationService.prototype.getInfo = /**
     * @param {?} type
     * @param {?} kind
     * @return {?}
     */
    function (type, kind) {
        if (!kind) {
            return;
        }
        // return css class based on notification type
        switch (type) {
            case NgtNotificationType.Success:
                return this.notificationsData.success[kind];
            case NgtNotificationType.Error:
                return this.notificationsData.error[kind];
            case NgtNotificationType.Info:
                return this.notificationsData.info[kind];
            case NgtNotificationType.Warning:
                return this.notificationsData.warning[kind];
        }
    };
    /**
     * @param {?} data
     * @return {?}
     */
    NgtNotificationService.prototype.success = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Success }, data));
    };
    /**
     * @param {?} data
     * @return {?}
     */
    NgtNotificationService.prototype.error = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Error }, data));
    };
    /**
     * @param {?} data
     * @return {?}
     */
    NgtNotificationService.prototype.info = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Info }, data));
    };
    /**
     * @param {?} data
     * @return {?}
     */
    NgtNotificationService.prototype.warn = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Warning }, data));
    };
    /**
     * @param {?} data
     * @return {?}
     */
    NgtNotificationService.prototype.notification = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        if (data.type || data.type === 0) {
            data.typeClass = this.getInfo(data.type, 'typeClass');
            if (!data.aside) {
                data.aside = this.getInfo(data.type, 'aside');
            }
        }
        /** @type {?} */
        var combinedOptions = (/** @type {?} */ (Object.assign({}, { timeout: this.timeout }, data)));
        this.stack.show(this._moduleCFR, combinedOptions);
    };
    /**
     * @return {?}
     */
    NgtNotificationService.prototype.clear = /**
     * @return {?}
     */
    function () {
        // clear alerts
        this.stack.clearAll();
    };
    NgtNotificationService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    NgtNotificationService.ctorParameters = function () { return [
        { type: NgtNotificationStack },
        { type: ComponentFactoryResolver }
    ]; };
    /** @nocollapse */ NgtNotificationService.ngInjectableDef = i0.defineInjectable({ factory: function NgtNotificationService_Factory() { return new NgtNotificationService(i0.inject(i1.NgtNotificationStack), i0.inject(i0.ComponentFactoryResolver)); }, token: NgtNotificationService, providedIn: "root" });
    return NgtNotificationService;
}());
export { NgtNotificationService };
if (false) {
    /** @type {?} */
    NgtNotificationService.prototype.timeout;
    /** @type {?} */
    NgtNotificationService.prototype.notificationsData;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationService.prototype.stack;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationService.prototype._moduleCFR;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbIm5vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFckUsT0FBTyxFQUFtQixtQkFBbUIsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQzVFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDOzs7QUFFNUQ7SUF3QkksZ0NBQW9CLEtBQTJCLEVBQzNCLFVBQW9DO1FBRHBDLFVBQUssR0FBTCxLQUFLLENBQXNCO1FBQzNCLGVBQVUsR0FBVixVQUFVLENBQTBCO1FBckJ4RCxZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2Ysc0JBQWlCLEdBQUc7WUFDaEIsT0FBTyxFQUFFO2dCQUNMLFNBQVMsRUFBRSxzQkFBc0I7Z0JBQ2pDLEtBQUssRUFBRSwwQkFBMEI7YUFDcEM7WUFDRCxLQUFLLEVBQUU7Z0JBQ0gsU0FBUyxFQUFFLHFCQUFxQjtnQkFDaEMsS0FBSyxFQUFFLHNCQUFzQjthQUNoQztZQUNELElBQUksRUFBRTtnQkFDRixTQUFTLEVBQUUsbUJBQW1CO2dCQUM5QixLQUFLLEVBQUUseUJBQXlCO2FBQ25DO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLFNBQVMsRUFBRSxzQkFBc0I7Z0JBQ2pDLEtBQUssRUFBRSxtQ0FBbUM7YUFDN0M7U0FDSixDQUFDO0lBSUYsQ0FBQzs7Ozs7O0lBRUQsd0NBQU87Ozs7O0lBQVAsVUFBUSxJQUFJLEVBQUUsSUFBSTtRQUNkLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDUCxPQUFPO1NBQ1Y7UUFFRCw4Q0FBOEM7UUFDOUMsUUFBUSxJQUFJLEVBQUU7WUFDVixLQUFLLG1CQUFtQixDQUFDLE9BQU87Z0JBQzVCLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNoRCxLQUFLLG1CQUFtQixDQUFDLEtBQUs7Z0JBQzFCLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QyxLQUFLLG1CQUFtQixDQUFDLElBQUk7Z0JBQ3pCLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM3QyxLQUFLLG1CQUFtQixDQUFDLE9BQU87Z0JBQzVCLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNuRDtJQUNMLENBQUM7Ozs7O0lBRUQsd0NBQU87Ozs7SUFBUCxVQUFRLElBQUk7UUFDUixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEVBQUMsSUFBSSxFQUFFLG1CQUFtQixDQUFDLE9BQU8sRUFBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDcEYsQ0FBQzs7Ozs7SUFFRCxzQ0FBSzs7OztJQUFMLFVBQU0sSUFBSTtRQUNOLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsRUFBQyxJQUFJLEVBQUUsbUJBQW1CLENBQUMsS0FBSyxFQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUNsRixDQUFDOzs7OztJQUVELHFDQUFJOzs7O0lBQUosVUFBSyxJQUFJO1FBQ0wsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxFQUFDLElBQUksRUFBRSxtQkFBbUIsQ0FBQyxJQUFJLEVBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ2pGLENBQUM7Ozs7O0lBRUQscUNBQUk7Ozs7SUFBSixVQUFLLElBQUk7UUFDTCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEVBQUMsSUFBSSxFQUFFLG1CQUFtQixDQUFDLE9BQU8sRUFBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDcEYsQ0FBQzs7Ozs7SUFFRCw2Q0FBWTs7OztJQUFaLFVBQWEsSUFBSTtRQUNiLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLENBQUMsRUFBRTtZQUM5QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQztZQUV0RCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtnQkFDYixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQzthQUNqRDtTQUNKOztZQUNLLGVBQWUsR0FBRyxtQkFBaUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsRUFBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBQyxFQUFFLElBQUksQ0FBQyxFQUFBO1FBQ3pGLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsZUFBZSxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7OztJQUVELHNDQUFLOzs7SUFBTDtRQUNJLGVBQWU7UUFDZixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzFCLENBQUM7O2dCQTdFSixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQUpRLG9CQUFvQjtnQkFIcEIsd0JBQXdCOzs7aUNBQWpDO0NBbUZDLEFBOUVELElBOEVDO1NBM0VZLHNCQUFzQjs7O0lBQy9CLHlDQUFlOztJQUNmLG1EQWlCRTs7Ozs7SUFFVSx1Q0FBbUM7Ozs7O0lBQ25DLDRDQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTmd0Tm90aWZpY2F0aW9uLCBOZ3ROb3RpZmljYXRpb25UeXBlIH0gZnJvbSAnLi9ub3RpZmljYXRpb24ubW9kZWwnO1xyXG5pbXBvcnQgeyBOZ3ROb3RpZmljYXRpb25TdGFjayB9IGZyb20gJy4vbm90aWZpY2F0aW9uLXN0YWNrJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0Tm90aWZpY2F0aW9uU2VydmljZSB7XHJcbiAgICB0aW1lb3V0ID0gMjUwMDtcclxuICAgIG5vdGlmaWNhdGlvbnNEYXRhID0ge1xyXG4gICAgICAgIHN1Y2Nlc3M6IHtcclxuICAgICAgICAgICAgdHlwZUNsYXNzOiAnbm90aWZpY2F0aW9uLXN1Y2Nlc3MnLFxyXG4gICAgICAgICAgICBhc2lkZTogJzxpIGNsYXNzPVwiZnQtY2hlY2tcIj48L2k+J1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZXJyb3I6IHtcclxuICAgICAgICAgICAgdHlwZUNsYXNzOiAnbm90aWZpY2F0aW9uLWRhbmdlcicsXHJcbiAgICAgICAgICAgIGFzaWRlOiAnPGkgY2xhc3M9XCJmdC14XCI+PC9pPidcclxuICAgICAgICB9LFxyXG4gICAgICAgIGluZm86IHtcclxuICAgICAgICAgICAgdHlwZUNsYXNzOiAnbm90aWZpY2F0aW9uLWluZm8nLFxyXG4gICAgICAgICAgICBhc2lkZTogJzxpIGNsYXNzPVwiZnQtaW5mb1wiPjwvaT4nXHJcbiAgICAgICAgfSxcclxuICAgICAgICB3YXJuaW5nOiB7XHJcbiAgICAgICAgICAgIHR5cGVDbGFzczogJ25vdGlmaWNhdGlvbi13YXJuaW5nJyxcclxuICAgICAgICAgICAgYXNpZGU6ICc8aSBjbGFzcz1cImZ0LWFsZXJ0LXRyaWFuZ2xlXCI+PC9pPidcclxuICAgICAgICB9LFxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHN0YWNrOiBOZ3ROb3RpZmljYXRpb25TdGFjayxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX21vZHVsZUNGUjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyKSB7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0SW5mbyh0eXBlLCBraW5kKSB7XHJcbiAgICAgICAgaWYgKCFraW5kKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIHJldHVybiBjc3MgY2xhc3MgYmFzZWQgb24gbm90aWZpY2F0aW9uIHR5cGVcclxuICAgICAgICBzd2l0Y2ggKHR5cGUpIHtcclxuICAgICAgICAgICAgY2FzZSBOZ3ROb3RpZmljYXRpb25UeXBlLlN1Y2Nlc3M6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5ub3RpZmljYXRpb25zRGF0YS5zdWNjZXNzW2tpbmRdO1xyXG4gICAgICAgICAgICBjYXNlIE5ndE5vdGlmaWNhdGlvblR5cGUuRXJyb3I6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5ub3RpZmljYXRpb25zRGF0YS5lcnJvcltraW5kXTtcclxuICAgICAgICAgICAgY2FzZSBOZ3ROb3RpZmljYXRpb25UeXBlLkluZm86XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5ub3RpZmljYXRpb25zRGF0YS5pbmZvW2tpbmRdO1xyXG4gICAgICAgICAgICBjYXNlIE5ndE5vdGlmaWNhdGlvblR5cGUuV2FybmluZzpcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLm5vdGlmaWNhdGlvbnNEYXRhLndhcm5pbmdba2luZF07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHN1Y2Nlc3MoZGF0YSkge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uKE9iamVjdC5hc3NpZ24oe30sIHt0eXBlOiBOZ3ROb3RpZmljYXRpb25UeXBlLlN1Y2Nlc3N9LCBkYXRhKSk7XHJcbiAgICB9XHJcblxyXG4gICAgZXJyb3IoZGF0YSkge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uKE9iamVjdC5hc3NpZ24oe30sIHt0eXBlOiBOZ3ROb3RpZmljYXRpb25UeXBlLkVycm9yfSwgZGF0YSkpO1xyXG4gICAgfVxyXG5cclxuICAgIGluZm8oZGF0YSkge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uKE9iamVjdC5hc3NpZ24oe30sIHt0eXBlOiBOZ3ROb3RpZmljYXRpb25UeXBlLkluZm99LCBkYXRhKSk7XHJcbiAgICB9XHJcblxyXG4gICAgd2FybihkYXRhKSB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb24oT2JqZWN0LmFzc2lnbih7fSwge3R5cGU6IE5ndE5vdGlmaWNhdGlvblR5cGUuV2FybmluZ30sIGRhdGEpKTtcclxuICAgIH1cclxuXHJcbiAgICBub3RpZmljYXRpb24oZGF0YSkge1xyXG4gICAgICAgIGlmIChkYXRhLnR5cGUgfHwgZGF0YS50eXBlID09PSAwKSB7XHJcbiAgICAgICAgICAgIGRhdGEudHlwZUNsYXNzID0gdGhpcy5nZXRJbmZvKGRhdGEudHlwZSwgJ3R5cGVDbGFzcycpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCFkYXRhLmFzaWRlKSB7XHJcbiAgICAgICAgICAgICAgICBkYXRhLmFzaWRlID0gdGhpcy5nZXRJbmZvKGRhdGEudHlwZSwgJ2FzaWRlJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgY29tYmluZWRPcHRpb25zID0gPE5ndE5vdGlmaWNhdGlvbj5PYmplY3QuYXNzaWduKHt9LCB7dGltZW91dDogdGhpcy50aW1lb3V0fSwgZGF0YSk7XHJcbiAgICAgICAgdGhpcy5zdGFjay5zaG93KHRoaXMuX21vZHVsZUNGUiwgY29tYmluZWRPcHRpb25zKTtcclxuICAgIH1cclxuXHJcbiAgICBjbGVhcigpIHtcclxuICAgICAgICAvLyBjbGVhciBhbGVydHNcclxuICAgICAgICB0aGlzLnN0YWNrLmNsZWFyQWxsKCk7XHJcbiAgICB9XHJcbn1cclxuIl19