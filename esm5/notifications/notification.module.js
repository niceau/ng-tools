/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtNotificationComponent } from './notification.component';
export { NgtNotificationService } from './notification.service';
export { NgtNotificationStack } from './notification-stack';
export { NgtNotificationComponent } from './notification.component';
export { NgtNotification } from './notification.model';
/** @type {?} */
var NGC_MODAL_DIRECTIVES = [
    NgtNotificationComponent
];
var NgtNotificationModule = /** @class */ (function () {
    function NgtNotificationModule() {
    }
    /**
     * @return {?}
     */
    NgtNotificationModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtNotificationModule };
    };
    NgtNotificationModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [NGC_MODAL_DIRECTIVES],
                    exports: [NGC_MODAL_DIRECTIVES],
                    imports: [CommonModule],
                    entryComponents: [NgtNotificationComponent]
                },] }
    ];
    return NgtNotificationModule;
}());
export { NgtNotificationModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQXVCLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFJL0MsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFcEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDaEUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDNUQsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDcEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHNCQUFzQixDQUFDOztJQUVqRCxvQkFBb0IsR0FBRztJQUN6Qix3QkFBd0I7Q0FDM0I7QUFFRDtJQUFBO0lBVUEsQ0FBQzs7OztJQUhVLDZCQUFPOzs7SUFBZDtRQUNJLE9BQU8sRUFBQyxRQUFRLEVBQUUscUJBQXFCLEVBQUMsQ0FBQztJQUM3QyxDQUFDOztnQkFUSixRQUFRLFNBQUM7b0JBQ04sWUFBWSxFQUFFLENBQUMsb0JBQW9CLENBQUM7b0JBQ3BDLE9BQU8sRUFBRSxDQUFDLG9CQUFvQixDQUFDO29CQUMvQixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7b0JBQ3ZCLGVBQWUsRUFBRSxDQUFDLHdCQUF3QixDQUFDO2lCQUM5Qzs7SUFLRCw0QkFBQztDQUFBLEFBVkQsSUFVQztTQUpZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5pbXBvcnQgeyBOZ3ROb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9ub3RpZmljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE5ndE5vdGlmaWNhdGlvblN0YWNrIH0gZnJvbSAnLi9ub3RpZmljYXRpb24tc3RhY2snO1xyXG5pbXBvcnQgeyBOZ3ROb3RpZmljYXRpb25Db21wb25lbnQgfSBmcm9tICcuL25vdGlmaWNhdGlvbi5jb21wb25lbnQnO1xyXG5cclxuZXhwb3J0IHsgTmd0Tm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5leHBvcnQgeyBOZ3ROb3RpZmljYXRpb25TdGFjayB9IGZyb20gJy4vbm90aWZpY2F0aW9uLXN0YWNrJztcclxuZXhwb3J0IHsgTmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9ub3RpZmljYXRpb24uY29tcG9uZW50JztcclxuZXhwb3J0IHsgTmd0Tm90aWZpY2F0aW9uIH0gZnJvbSAnLi9ub3RpZmljYXRpb24ubW9kZWwnO1xyXG5cclxuY29uc3QgTkdDX01PREFMX0RJUkVDVElWRVMgPSBbXHJcbiAgICBOZ3ROb3RpZmljYXRpb25Db21wb25lbnRcclxuXTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtOR0NfTU9EQUxfRElSRUNUSVZFU10sXHJcbiAgICBleHBvcnRzOiBbTkdDX01PREFMX0RJUkVDVElWRVNdLFxyXG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV0sXHJcbiAgICBlbnRyeUNvbXBvbmVudHM6IFtOZ3ROb3RpZmljYXRpb25Db21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3ROb3RpZmljYXRpb25Nb2R1bGUge1xyXG4gICAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICAgICAgcmV0dXJuIHtuZ01vZHVsZTogTmd0Tm90aWZpY2F0aW9uTW9kdWxlfTtcclxuICAgIH1cclxufVxyXG4iXX0=