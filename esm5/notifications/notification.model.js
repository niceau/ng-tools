/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NgtNotification = /** @class */ (function () {
    function NgtNotification() {
    }
    return NgtNotification;
}());
export { NgtNotification };
if (false) {
    /** @type {?} */
    NgtNotification.prototype.type;
    /** @type {?} */
    NgtNotification.prototype.message;
    /** @type {?} */
    NgtNotification.prototype.timeout;
    /** @type {?} */
    NgtNotification.prototype.typeClass;
    /** @type {?} */
    NgtNotification.prototype.aside;
    /** @type {?} */
    NgtNotification.prototype.title;
    /** @type {?} */
    NgtNotification.prototype._ref;
}
/** @enum {number} */
var NgtNotificationType = {
    Success: 0,
    Error: 1,
    Info: 2,
    Warning: 3,
};
export { NgtNotificationType };
NgtNotificationType[NgtNotificationType.Success] = 'Success';
NgtNotificationType[NgtNotificationType.Error] = 'Error';
NgtNotificationType[NgtNotificationType.Info] = 'Info';
NgtNotificationType[NgtNotificationType.Warning] = 'Warning';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBRUE7SUFBQTtJQVFBLENBQUM7SUFBRCxzQkFBQztBQUFELENBQUMsQUFSRCxJQVFDOzs7O0lBUEcsK0JBQTBCOztJQUMxQixrQ0FBZ0I7O0lBQ2hCLGtDQUFnQjs7SUFDaEIsb0NBQWtCOztJQUNsQixnQ0FBZTs7SUFDZixnQ0FBYzs7SUFDZCwrQkFBb0M7Ozs7SUFJcEMsVUFBTztJQUNQLFFBQUs7SUFDTCxPQUFJO0lBQ0osVUFBTyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuZXhwb3J0IGNsYXNzIE5ndE5vdGlmaWNhdGlvbiB7XHJcbiAgICB0eXBlOiBOZ3ROb3RpZmljYXRpb25UeXBlO1xyXG4gICAgbWVzc2FnZTogc3RyaW5nO1xyXG4gICAgdGltZW91dDogbnVtYmVyO1xyXG4gICAgdHlwZUNsYXNzOiBzdHJpbmc7XHJcbiAgICBhc2lkZTogYm9vbGVhbjtcclxuICAgIHRpdGxlOiBzdHJpbmc7XHJcbiAgICBfcmVmOiBDb21wb25lbnRSZWY8Tmd0Tm90aWZpY2F0aW9uPjtcclxufVxyXG5cclxuZXhwb3J0IGVudW0gTmd0Tm90aWZpY2F0aW9uVHlwZSB7XHJcbiAgICBTdWNjZXNzLFxyXG4gICAgRXJyb3IsXHJcbiAgICBJbmZvLFxyXG4gICAgV2FybmluZ1xyXG59XHJcbiJdfQ==