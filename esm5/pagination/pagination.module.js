/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtPagination, NgtPaginationEllipsis, NgtPaginationFirst, NgtPaginationLast, NgtPaginationNext, NgtPaginationNumber, NgtPaginationPrevious } from './pagination';
export { NgtPagination, NgtPaginationEllipsis, NgtPaginationFirst, NgtPaginationLast, NgtPaginationNext, NgtPaginationNumber, NgtPaginationPrevious } from './pagination';
export { NgtPaginationConfig } from './pagination-config';
/** @type {?} */
var DIRECTIVES = [
    NgtPagination,
    NgtPaginationEllipsis,
    NgtPaginationFirst,
    NgtPaginationLast,
    NgtPaginationNext,
    NgtPaginationNumber,
    NgtPaginationPrevious
];
var NgtPaginationModule = /** @class */ (function () {
    function NgtPaginationModule() {
    }
    /**
     * @return {?}
     */
    NgtPaginationModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtPaginationModule };
    };
    NgtPaginationModule.decorators = [
        { type: NgModule, args: [{
                    declarations: DIRECTIVES,
                    exports: DIRECTIVES,
                    imports: [CommonModule]
                },] }
    ];
    return NgtPaginationModule;
}());
export { NgtPaginationModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInBhZ2luYXRpb24vcGFnaW5hdGlvbi5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQ0gsYUFBYSxFQUNiLHFCQUFxQixFQUNyQixrQkFBa0IsRUFDbEIsaUJBQWlCLEVBQ2pCLGlCQUFpQixFQUNqQixtQkFBbUIsRUFDbkIscUJBQXFCLEVBQ3hCLE1BQU0sY0FBYyxDQUFDO0FBRXRCLE9BQU8sRUFDSCxhQUFhLEVBQ2IscUJBQXFCLEVBQ3JCLGtCQUFrQixFQUNsQixpQkFBaUIsRUFDakIsaUJBQWlCLEVBQ2pCLG1CQUFtQixFQUNuQixxQkFBcUIsRUFDeEIsTUFBTSxjQUFjLENBQUM7QUFDdEIsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0scUJBQXFCLENBQUM7O0lBRXBELFVBQVUsR0FBRztJQUNmLGFBQWE7SUFDYixxQkFBcUI7SUFDckIsa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixpQkFBaUI7SUFDakIsbUJBQW1CO0lBQ25CLHFCQUFxQjtDQUN4QjtBQUVEO0lBQUE7SUFTQSxDQUFDOzs7O0lBSFUsMkJBQU87OztJQUFkO1FBQ0ksT0FBTyxFQUFDLFFBQVEsRUFBRSxtQkFBbUIsRUFBQyxDQUFDO0lBQzNDLENBQUM7O2dCQVJKLFFBQVEsU0FBQztvQkFDTixZQUFZLEVBQUUsVUFBVTtvQkFDeEIsT0FBTyxFQUFFLFVBQVU7b0JBQ25CLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztpQkFDMUI7O0lBS0QsMEJBQUM7Q0FBQSxBQVRELElBU0M7U0FKWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHtcclxuICAgIE5ndFBhZ2luYXRpb24sXHJcbiAgICBOZ3RQYWdpbmF0aW9uRWxsaXBzaXMsXHJcbiAgICBOZ3RQYWdpbmF0aW9uRmlyc3QsXHJcbiAgICBOZ3RQYWdpbmF0aW9uTGFzdCxcclxuICAgIE5ndFBhZ2luYXRpb25OZXh0LFxyXG4gICAgTmd0UGFnaW5hdGlvbk51bWJlcixcclxuICAgIE5ndFBhZ2luYXRpb25QcmV2aW91c1xyXG59IGZyb20gJy4vcGFnaW5hdGlvbic7XHJcblxyXG5leHBvcnQge1xyXG4gICAgTmd0UGFnaW5hdGlvbixcclxuICAgIE5ndFBhZ2luYXRpb25FbGxpcHNpcyxcclxuICAgIE5ndFBhZ2luYXRpb25GaXJzdCxcclxuICAgIE5ndFBhZ2luYXRpb25MYXN0LFxyXG4gICAgTmd0UGFnaW5hdGlvbk5leHQsXHJcbiAgICBOZ3RQYWdpbmF0aW9uTnVtYmVyLFxyXG4gICAgTmd0UGFnaW5hdGlvblByZXZpb3VzXHJcbn0gZnJvbSAnLi9wYWdpbmF0aW9uJztcclxuZXhwb3J0IHsgTmd0UGFnaW5hdGlvbkNvbmZpZyB9IGZyb20gJy4vcGFnaW5hdGlvbi1jb25maWcnO1xyXG5cclxuY29uc3QgRElSRUNUSVZFUyA9IFtcclxuICAgIE5ndFBhZ2luYXRpb24sXHJcbiAgICBOZ3RQYWdpbmF0aW9uRWxsaXBzaXMsXHJcbiAgICBOZ3RQYWdpbmF0aW9uRmlyc3QsXHJcbiAgICBOZ3RQYWdpbmF0aW9uTGFzdCxcclxuICAgIE5ndFBhZ2luYXRpb25OZXh0LFxyXG4gICAgTmd0UGFnaW5hdGlvbk51bWJlcixcclxuICAgIE5ndFBhZ2luYXRpb25QcmV2aW91c1xyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogRElSRUNUSVZFUyxcclxuICAgIGV4cG9ydHM6IERJUkVDVElWRVMsXHJcbiAgICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0UGFnaW5hdGlvbk1vZHVsZSB7XHJcbiAgICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge25nTW9kdWxlOiBOZ3RQYWdpbmF0aW9uTW9kdWxlfTtcclxuICAgIH1cclxufVxyXG4iXX0=