/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, ContentChild, Directive, EventEmitter, Input, Output, ChangeDetectionStrategy, TemplateRef } from '@angular/core';
import { getValueInRange, isNumber } from '../util/util';
import { NgtPaginationConfig } from './pagination-config';
/**
 * Context for the pagination 'first', 'previous', 'next', 'last' or 'ellipsis' cell
 * in case you want to override one
 *
 * \@since 4.1.0
 * @record
 */
export function NgtPaginationLinkContext() { }
if (false) {
    /**
     * Page number currently selected
     * @type {?}
     */
    NgtPaginationLinkContext.prototype.currentPage;
    /**
     * If true the link in question is disabled
     * @type {?}
     */
    NgtPaginationLinkContext.prototype.disabled;
}
/**
 * Context for the pagination 'number' cell in case you want to override one.
 * Extends 'NgtPaginationLinkContext'
 *
 * \@since 4.1.0
 * @record
 */
export function NgtPaginationNumberContext() { }
if (false) {
    /**
     * Page number displayed by the current cell
     * @type {?}
     */
    NgtPaginationNumberContext.prototype.$implicit;
}
/**
 * The directive to match the 'ellipsis' cell template
 *
 * \@since 4.1.0
 */
var NgtPaginationEllipsis = /** @class */ (function () {
    function NgtPaginationEllipsis(templateRef) {
        this.templateRef = templateRef;
    }
    NgtPaginationEllipsis.decorators = [
        { type: Directive, args: [{ selector: 'ng-template[ngtPaginationEllipsis]' },] }
    ];
    /** @nocollapse */
    NgtPaginationEllipsis.ctorParameters = function () { return [
        { type: TemplateRef }
    ]; };
    return NgtPaginationEllipsis;
}());
export { NgtPaginationEllipsis };
if (false) {
    /** @type {?} */
    NgtPaginationEllipsis.prototype.templateRef;
}
/**
 * The directive to match the 'first' cell template
 *
 * \@since 4.1.0
 */
var NgtPaginationFirst = /** @class */ (function () {
    function NgtPaginationFirst(templateRef) {
        this.templateRef = templateRef;
    }
    NgtPaginationFirst.decorators = [
        { type: Directive, args: [{ selector: 'ng-template[ngtPaginationFirst]' },] }
    ];
    /** @nocollapse */
    NgtPaginationFirst.ctorParameters = function () { return [
        { type: TemplateRef }
    ]; };
    return NgtPaginationFirst;
}());
export { NgtPaginationFirst };
if (false) {
    /** @type {?} */
    NgtPaginationFirst.prototype.templateRef;
}
/**
 * The directive to match the 'last' cell template
 *
 * \@since 4.1.0
 */
var NgtPaginationLast = /** @class */ (function () {
    function NgtPaginationLast(templateRef) {
        this.templateRef = templateRef;
    }
    NgtPaginationLast.decorators = [
        { type: Directive, args: [{ selector: 'ng-template[ngtPaginationLast]' },] }
    ];
    /** @nocollapse */
    NgtPaginationLast.ctorParameters = function () { return [
        { type: TemplateRef }
    ]; };
    return NgtPaginationLast;
}());
export { NgtPaginationLast };
if (false) {
    /** @type {?} */
    NgtPaginationLast.prototype.templateRef;
}
/**
 * The directive to match the 'next' cell template
 *
 * \@since 4.1.0
 */
var NgtPaginationNext = /** @class */ (function () {
    function NgtPaginationNext(templateRef) {
        this.templateRef = templateRef;
    }
    NgtPaginationNext.decorators = [
        { type: Directive, args: [{ selector: 'ng-template[ngtPaginationNext]' },] }
    ];
    /** @nocollapse */
    NgtPaginationNext.ctorParameters = function () { return [
        { type: TemplateRef }
    ]; };
    return NgtPaginationNext;
}());
export { NgtPaginationNext };
if (false) {
    /** @type {?} */
    NgtPaginationNext.prototype.templateRef;
}
/**
 * The directive to match the page 'number' cell template
 *
 * \@since 4.1.0
 */
var NgtPaginationNumber = /** @class */ (function () {
    function NgtPaginationNumber(templateRef) {
        this.templateRef = templateRef;
    }
    NgtPaginationNumber.decorators = [
        { type: Directive, args: [{ selector: 'ng-template[ngtPaginationNumber]' },] }
    ];
    /** @nocollapse */
    NgtPaginationNumber.ctorParameters = function () { return [
        { type: TemplateRef }
    ]; };
    return NgtPaginationNumber;
}());
export { NgtPaginationNumber };
if (false) {
    /** @type {?} */
    NgtPaginationNumber.prototype.templateRef;
}
/**
 * The directive to match the 'previous' cell template
 *
 * \@since 4.1.0
 */
var NgtPaginationPrevious = /** @class */ (function () {
    function NgtPaginationPrevious(templateRef) {
        this.templateRef = templateRef;
    }
    NgtPaginationPrevious.decorators = [
        { type: Directive, args: [{ selector: 'ng-template[ngtPaginationPrevious]' },] }
    ];
    /** @nocollapse */
    NgtPaginationPrevious.ctorParameters = function () { return [
        { type: TemplateRef }
    ]; };
    return NgtPaginationPrevious;
}());
export { NgtPaginationPrevious };
if (false) {
    /** @type {?} */
    NgtPaginationPrevious.prototype.templateRef;
}
/**
 * A component that displays page numbers and allows to customize them in several ways
 */
var NgtPagination = /** @class */ (function () {
    function NgtPagination(config) {
        this.pageCount = 0;
        this.pages = [];
        /**
         *  Current page. Page numbers start with 1
         */
        this.page = 1;
        /**
         *  An event fired when the page is changed.
         *  Event's payload equals to the newly selected page.
         *  Will fire only if collection size is set and all values are valid.
         *  Page numbers start with 1
         */
        this.pageChange = new EventEmitter(true);
        this.disabled = config.disabled;
        this.boundaryLinks = config.boundaryLinks;
        this.directionLinks = config.directionLinks;
        this.ellipses = config.ellipses;
        this.maxSize = config.maxSize;
        this.pageSize = config.pageSize;
        this.rotate = config.rotate;
        this.size = config.size;
    }
    /**
     * @return {?}
     */
    NgtPagination.prototype.hasPrevious = /**
     * @return {?}
     */
    function () {
        return this.page > 1;
    };
    /**
     * @return {?}
     */
    NgtPagination.prototype.hasNext = /**
     * @return {?}
     */
    function () {
        return this.page < this.pageCount;
    };
    /**
     * @return {?}
     */
    NgtPagination.prototype.nextDisabled = /**
     * @return {?}
     */
    function () {
        return !this.hasNext() || this.disabled;
    };
    /**
     * @return {?}
     */
    NgtPagination.prototype.previousDisabled = /**
     * @return {?}
     */
    function () {
        return !this.hasPrevious() || this.disabled;
    };
    /**
     * @param {?} pageNumber
     * @return {?}
     */
    NgtPagination.prototype.selectPage = /**
     * @param {?} pageNumber
     * @return {?}
     */
    function (pageNumber) {
        this._updatePages(pageNumber);
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    NgtPagination.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        this._updatePages(this.page);
    };
    /**
     * @param {?} pageNumber
     * @return {?}
     */
    NgtPagination.prototype.isEllipsis = /**
     * @param {?} pageNumber
     * @return {?}
     */
    function (pageNumber) {
        return pageNumber === -1;
    };
    /**
     * Appends ellipses and first/last page number to the displayed pages
     */
    /**
     * Appends ellipses and first/last page number to the displayed pages
     * @private
     * @param {?} start
     * @param {?} end
     * @return {?}
     */
    NgtPagination.prototype._applyEllipses = /**
     * Appends ellipses and first/last page number to the displayed pages
     * @private
     * @param {?} start
     * @param {?} end
     * @return {?}
     */
    function (start, end) {
        if (this.ellipses) {
            if (start > 0) {
                if (start > 1) {
                    this.pages.unshift(-1);
                }
                this.pages.unshift(1);
            }
            if (end < this.pageCount) {
                if (end < (this.pageCount - 1)) {
                    this.pages.push(-1);
                }
                this.pages.push(this.pageCount);
            }
        }
    };
    /**
     * Rotates page numbers based on maxSize items visible.
     * Currently selected page stays in the middle:
     *
     * Ex. for selected page = 6:
     * [5,*6*,7] for maxSize = 3
     * [4,5,*6*,7] for maxSize = 4
     */
    /**
     * Rotates page numbers based on maxSize items visible.
     * Currently selected page stays in the middle:
     *
     * Ex. for selected page = 6:
     * [5,*6*,7] for maxSize = 3
     * [4,5,*6*,7] for maxSize = 4
     * @private
     * @return {?}
     */
    NgtPagination.prototype._applyRotation = /**
     * Rotates page numbers based on maxSize items visible.
     * Currently selected page stays in the middle:
     *
     * Ex. for selected page = 6:
     * [5,*6*,7] for maxSize = 3
     * [4,5,*6*,7] for maxSize = 4
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var start = 0;
        /** @type {?} */
        var end = this.pageCount;
        /** @type {?} */
        var leftOffset = Math.floor(this.maxSize / 2);
        /** @type {?} */
        var rightOffset = this.maxSize % 2 === 0 ? leftOffset - 1 : leftOffset;
        if (this.page <= leftOffset) {
            // very beginning, no rotation -> [0..maxSize]
            end = this.maxSize;
        }
        else if (this.pageCount - this.page < leftOffset) {
            // very end, no rotation -> [len-maxSize..len]
            start = this.pageCount - this.maxSize;
        }
        else {
            // rotate
            start = this.page - leftOffset - 1;
            end = this.page + rightOffset;
        }
        return [start, end];
    };
    /**
     * Paginates page numbers based on maxSize items per page
     */
    /**
     * Paginates page numbers based on maxSize items per page
     * @private
     * @return {?}
     */
    NgtPagination.prototype._applyPagination = /**
     * Paginates page numbers based on maxSize items per page
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var page = Math.ceil(this.page / this.maxSize) - 1;
        /** @type {?} */
        var start = page * this.maxSize;
        /** @type {?} */
        var end = start + this.maxSize;
        return [start, end];
    };
    /**
     * @private
     * @param {?} newPageNo
     * @return {?}
     */
    NgtPagination.prototype._setPageInRange = /**
     * @private
     * @param {?} newPageNo
     * @return {?}
     */
    function (newPageNo) {
        /** @type {?} */
        var prevPageNo = this.page;
        this.page = getValueInRange(newPageNo, this.pageCount, 1);
        if (this.page !== prevPageNo && isNumber(this.collectionSize)) {
            this.pageChange.emit(this.page);
        }
    };
    /**
     * @private
     * @param {?} newPage
     * @return {?}
     */
    NgtPagination.prototype._updatePages = /**
     * @private
     * @param {?} newPage
     * @return {?}
     */
    function (newPage) {
        var _a, _b;
        this.pageCount = Math.ceil(this.collectionSize / this.pageSize);
        if (!isNumber(this.pageCount)) {
            this.pageCount = 0;
        }
        // fill-in model needed to render pages
        this.pages.length = 0;
        for (var i = 1; i <= this.pageCount; i++) {
            this.pages.push(i);
        }
        // set page within 1..max range
        this._setPageInRange(newPage);
        // apply maxSize if necessary
        if (this.maxSize > 0 && this.pageCount > this.maxSize) {
            /** @type {?} */
            var start = 0;
            /** @type {?} */
            var end = this.pageCount;
            // either paginating or rotating page numbers
            if (this.rotate) {
                _a = tslib_1.__read(this._applyRotation(), 2), start = _a[0], end = _a[1];
            }
            else {
                _b = tslib_1.__read(this._applyPagination(), 2), start = _b[0], end = _b[1];
            }
            this.pages = this.pages.slice(start, end);
            // adding ellipses
            this._applyEllipses(start, end);
        }
    };
    NgtPagination.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-pagination',
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    host: { 'role': 'navigation' },
                    template: "\n    <ng-template #first><span aria-hidden=\"true\" i18n=\"@@ngt.pagination.first\">&laquo;&laquo;</span></ng-template>\n    <ng-template #previous><span aria-hidden=\"true\" i18n=\"@@ngt.pagination.previous\">&laquo;</span></ng-template>\n    <ng-template #next><span aria-hidden=\"true\" i18n=\"@@ngt.pagination.next\">&raquo;</span></ng-template>\n    <ng-template #last><span aria-hidden=\"true\" i18n=\"@@ngt.pagination.last\">&raquo;&raquo;</span></ng-template>\n    <ng-template #ellipsis>...</ng-template>\n    <ng-template #defaultNumber let-page let-currentPage=\"currentPage\">\n      {{ page }}\n      <span *ngIf=\"page === currentPage\" class=\"sr-only\">(current)</span>\n    </ng-template>\n    <ul [class]=\"'pagination' + (size ? ' pagination-' + size : '')\">\n      <li *ngIf=\"boundaryLinks\" class=\"page-item\"\n        [class.disabled]=\"previousDisabled()\">\n        <a aria-label=\"First\" i18n-aria-label=\"@@ngt.pagination.first-aria\" class=\"page-link\" href\n          (click)=\"selectPage(1); $event.preventDefault()\" [attr.tabindex]=\"(hasPrevious() ? null : '-1')\">\n          <ng-template [ngTemplateOutlet]=\"tplFirst?.templateRef || first\"\n                       [ngTemplateOutletContext]=\"{disabled: previousDisabled(), currentPage: page}\"></ng-template>\n        </a>\n      </li>\n      <li *ngIf=\"directionLinks\" class=\"page-item\"\n        [class.disabled]=\"previousDisabled()\">\n        <a aria-label=\"Previous\" i18n-aria-label=\"@@ngt.pagination.previous-aria\" class=\"page-link\" href\n          (click)=\"selectPage(page-1); $event.preventDefault()\" [attr.tabindex]=\"(hasPrevious() ? null : '-1')\">\n          <ng-template [ngTemplateOutlet]=\"tplPrevious?.templateRef || previous\"\n                       [ngTemplateOutletContext]=\"{disabled: previousDisabled()}\"></ng-template>\n        </a>\n      </li>\n      <li *ngFor=\"let pageNumber of pages\" class=\"page-item\" [class.active]=\"pageNumber === page\"\n        [class.disabled]=\"isEllipsis(pageNumber) || disabled\">\n        <a *ngIf=\"isEllipsis(pageNumber)\" class=\"page-link\">\n          <ng-template [ngTemplateOutlet]=\"tplEllipsis?.templateRef || ellipsis\"\n                       [ngTemplateOutletContext]=\"{disabled: true, currentPage: page}\"></ng-template>\n        </a>\n        <a *ngIf=\"!isEllipsis(pageNumber)\" class=\"page-link\" href (click)=\"selectPage(pageNumber); $event.preventDefault()\">\n          <ng-template [ngTemplateOutlet]=\"tplNumber?.templateRef || defaultNumber\"\n                       [ngTemplateOutletContext]=\"{disabled: disabled, $implicit: pageNumber, currentPage: page}\"></ng-template>\n        </a>\n      </li>\n      <li *ngIf=\"directionLinks\" class=\"page-item\" [class.disabled]=\"nextDisabled()\">\n        <a aria-label=\"Next\" i18n-aria-label=\"@@ngt.pagination.next-aria\" class=\"page-link\" href\n          (click)=\"selectPage(page+1); $event.preventDefault()\" [attr.tabindex]=\"(hasNext() ? null : '-1')\">\n          <ng-template [ngTemplateOutlet]=\"tplNext?.templateRef || next\"\n                       [ngTemplateOutletContext]=\"{disabled: nextDisabled(), currentPage: page}\"></ng-template>\n        </a>\n      </li>\n      <li *ngIf=\"boundaryLinks\" class=\"page-item\" [class.disabled]=\"nextDisabled()\">\n        <a aria-label=\"Last\" i18n-aria-label=\"@@ngt.pagination.last-aria\" class=\"page-link\" href\n          (click)=\"selectPage(pageCount); $event.preventDefault()\" [attr.tabindex]=\"(hasNext() ? null : '-1')\">\n          <ng-template [ngTemplateOutlet]=\"tplLast?.templateRef || last\"\n                       [ngTemplateOutletContext]=\"{disabled: nextDisabled(), currentPage: page}\"></ng-template>\n        </a>\n      </li>\n    </ul>\n  "
                }] }
    ];
    /** @nocollapse */
    NgtPagination.ctorParameters = function () { return [
        { type: NgtPaginationConfig }
    ]; };
    NgtPagination.propDecorators = {
        tplEllipsis: [{ type: ContentChild, args: [NgtPaginationEllipsis,] }],
        tplFirst: [{ type: ContentChild, args: [NgtPaginationFirst,] }],
        tplLast: [{ type: ContentChild, args: [NgtPaginationLast,] }],
        tplNext: [{ type: ContentChild, args: [NgtPaginationNext,] }],
        tplNumber: [{ type: ContentChild, args: [NgtPaginationNumber,] }],
        tplPrevious: [{ type: ContentChild, args: [NgtPaginationPrevious,] }],
        disabled: [{ type: Input }],
        boundaryLinks: [{ type: Input }],
        directionLinks: [{ type: Input }],
        ellipses: [{ type: Input }],
        rotate: [{ type: Input }],
        collectionSize: [{ type: Input }],
        maxSize: [{ type: Input }],
        page: [{ type: Input }],
        pageSize: [{ type: Input }],
        pageChange: [{ type: Output }],
        size: [{ type: Input }]
    };
    return NgtPagination;
}());
export { NgtPagination };
if (false) {
    /** @type {?} */
    NgtPagination.prototype.pageCount;
    /** @type {?} */
    NgtPagination.prototype.pages;
    /** @type {?} */
    NgtPagination.prototype.tplEllipsis;
    /** @type {?} */
    NgtPagination.prototype.tplFirst;
    /** @type {?} */
    NgtPagination.prototype.tplLast;
    /** @type {?} */
    NgtPagination.prototype.tplNext;
    /** @type {?} */
    NgtPagination.prototype.tplNumber;
    /** @type {?} */
    NgtPagination.prototype.tplPrevious;
    /**
     * Whether to disable buttons from user input
     * @type {?}
     */
    NgtPagination.prototype.disabled;
    /**
     *  Whether to show the "First" and "Last" page links
     * @type {?}
     */
    NgtPagination.prototype.boundaryLinks;
    /**
     *  Whether to show the "Next" and "Previous" page links
     * @type {?}
     */
    NgtPagination.prototype.directionLinks;
    /**
     *  Whether to show ellipsis symbols and first/last page numbers when maxSize > number of pages
     * @type {?}
     */
    NgtPagination.prototype.ellipses;
    /**
     *  Whether to rotate pages when maxSize > number of pages.
     *  Current page will be in the middle
     * @type {?}
     */
    NgtPagination.prototype.rotate;
    /**
     *  Number of items in collection.
     * @type {?}
     */
    NgtPagination.prototype.collectionSize;
    /**
     *  Maximum number of pages to display.
     * @type {?}
     */
    NgtPagination.prototype.maxSize;
    /**
     *  Current page. Page numbers start with 1
     * @type {?}
     */
    NgtPagination.prototype.page;
    /**
     *  Number of items per page.
     * @type {?}
     */
    NgtPagination.prototype.pageSize;
    /**
     *  An event fired when the page is changed.
     *  Event's payload equals to the newly selected page.
     *  Will fire only if collection size is set and all values are valid.
     *  Page numbers start with 1
     * @type {?}
     */
    NgtPagination.prototype.pageChange;
    /**
     * Pagination display size: small or large
     * @type {?}
     */
    NgtPagination.prototype.size;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsicGFnaW5hdGlvbi9wYWdpbmF0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUNILFNBQVMsRUFDVCxZQUFZLEVBQ1osU0FBUyxFQUNULFlBQVksRUFDWixLQUFLLEVBQ0wsTUFBTSxFQUVOLHVCQUF1QixFQUV2QixXQUFXLEVBQ2QsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLGVBQWUsRUFBRSxRQUFRLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDekQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7Ozs7O0FBUTFELDhDQVVDOzs7Ozs7SUFORywrQ0FBb0I7Ozs7O0lBS3BCLDRDQUFrQjs7Ozs7Ozs7O0FBU3RCLGdEQUtDOzs7Ozs7SUFERywrQ0FBa0I7Ozs7Ozs7QUFRdEI7SUFFSSwrQkFBbUIsV0FBa0Q7UUFBbEQsZ0JBQVcsR0FBWCxXQUFXLENBQXVDO0lBQ3JFLENBQUM7O2dCQUhKLFNBQVMsU0FBQyxFQUFDLFFBQVEsRUFBRSxvQ0FBb0MsRUFBQzs7OztnQkF6Q3ZELFdBQVc7O0lBNkNmLDRCQUFDO0NBQUEsQUFKRCxJQUlDO1NBSFkscUJBQXFCOzs7SUFDbEIsNENBQXlEOzs7Ozs7O0FBU3pFO0lBRUksNEJBQW1CLFdBQWtEO1FBQWxELGdCQUFXLEdBQVgsV0FBVyxDQUF1QztJQUNyRSxDQUFDOztnQkFISixTQUFTLFNBQUMsRUFBQyxRQUFRLEVBQUUsaUNBQWlDLEVBQUM7Ozs7Z0JBcERwRCxXQUFXOztJQXdEZix5QkFBQztDQUFBLEFBSkQsSUFJQztTQUhZLGtCQUFrQjs7O0lBQ2YseUNBQXlEOzs7Ozs7O0FBU3pFO0lBRUksMkJBQW1CLFdBQWtEO1FBQWxELGdCQUFXLEdBQVgsV0FBVyxDQUF1QztJQUNyRSxDQUFDOztnQkFISixTQUFTLFNBQUMsRUFBQyxRQUFRLEVBQUUsZ0NBQWdDLEVBQUM7Ozs7Z0JBL0RuRCxXQUFXOztJQW1FZix3QkFBQztDQUFBLEFBSkQsSUFJQztTQUhZLGlCQUFpQjs7O0lBQ2Qsd0NBQXlEOzs7Ozs7O0FBU3pFO0lBRUksMkJBQW1CLFdBQWtEO1FBQWxELGdCQUFXLEdBQVgsV0FBVyxDQUF1QztJQUNyRSxDQUFDOztnQkFISixTQUFTLFNBQUMsRUFBQyxRQUFRLEVBQUUsZ0NBQWdDLEVBQUM7Ozs7Z0JBMUVuRCxXQUFXOztJQThFZix3QkFBQztDQUFBLEFBSkQsSUFJQztTQUhZLGlCQUFpQjs7O0lBQ2Qsd0NBQXlEOzs7Ozs7O0FBU3pFO0lBRUksNkJBQW1CLFdBQW9EO1FBQXBELGdCQUFXLEdBQVgsV0FBVyxDQUF5QztJQUN2RSxDQUFDOztnQkFISixTQUFTLFNBQUMsRUFBQyxRQUFRLEVBQUUsa0NBQWtDLEVBQUM7Ozs7Z0JBckZyRCxXQUFXOztJQXlGZiwwQkFBQztDQUFBLEFBSkQsSUFJQztTQUhZLG1CQUFtQjs7O0lBQ2hCLDBDQUEyRDs7Ozs7OztBQVMzRTtJQUVJLCtCQUFtQixXQUFrRDtRQUFsRCxnQkFBVyxHQUFYLFdBQVcsQ0FBdUM7SUFDckUsQ0FBQzs7Z0JBSEosU0FBUyxTQUFDLEVBQUMsUUFBUSxFQUFFLG9DQUFvQyxFQUFDOzs7O2dCQWhHdkQsV0FBVzs7SUFvR2YsNEJBQUM7Q0FBQSxBQUpELElBSUM7U0FIWSxxQkFBcUI7OztJQUNsQiw0Q0FBeUQ7Ozs7O0FBT3pFO0lBaUlJLHVCQUFZLE1BQTJCO1FBckV2QyxjQUFTLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsVUFBSyxHQUFhLEVBQUUsQ0FBQzs7OztRQWdEWixTQUFJLEdBQUcsQ0FBQyxDQUFDOzs7Ozs7O1FBYVIsZUFBVSxHQUFHLElBQUksWUFBWSxDQUFTLElBQUksQ0FBQyxDQUFDO1FBUWxELElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNoQyxJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUM7UUFDMUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLENBQUMsY0FBYyxDQUFDO1FBQzVDLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNoQyxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUM7UUFDOUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUM1QixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDNUIsQ0FBQzs7OztJQUVELG1DQUFXOzs7SUFBWDtRQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELCtCQUFPOzs7SUFBUDtRQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCxvQ0FBWTs7O0lBQVo7UUFDSSxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDNUMsQ0FBQzs7OztJQUVELHdDQUFnQjs7O0lBQWhCO1FBQ0ksT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ2hELENBQUM7Ozs7O0lBRUQsa0NBQVU7Ozs7SUFBVixVQUFXLFVBQWtCO1FBQ3pCLElBQUksQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7Ozs7SUFFRCxtQ0FBVzs7OztJQUFYLFVBQVksT0FBc0I7UUFDOUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCxrQ0FBVTs7OztJQUFWLFVBQVcsVUFBVTtRQUNqQixPQUFPLFVBQVUsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUM3QixDQUFDO0lBRUQ7O09BRUc7Ozs7Ozs7O0lBQ0ssc0NBQWM7Ozs7Ozs7SUFBdEIsVUFBdUIsS0FBYSxFQUFFLEdBQVc7UUFDN0MsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFO2dCQUNYLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRTtvQkFDWCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMxQjtnQkFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN6QjtZQUNELElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ3RCLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdkI7Z0JBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ25DO1NBQ0o7SUFDTCxDQUFDO0lBRUQ7Ozs7Ozs7T0FPRzs7Ozs7Ozs7Ozs7SUFDSyxzQ0FBYzs7Ozs7Ozs7OztJQUF0Qjs7WUFDUSxLQUFLLEdBQUcsQ0FBQzs7WUFDVCxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVM7O1lBQ3BCLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDOztZQUN6QyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVO1FBRXRFLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxVQUFVLEVBQUU7WUFDekIsOENBQThDO1lBQzlDLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1NBQ3RCO2FBQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsVUFBVSxFQUFFO1lBQ2hELDhDQUE4QztZQUM5QyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1NBQ3pDO2FBQU07WUFDSCxTQUFTO1lBQ1QsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsVUFBVSxHQUFHLENBQUMsQ0FBQztZQUNuQyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksR0FBRyxXQUFXLENBQUM7U0FDakM7UUFFRCxPQUFPLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFFRDs7T0FFRzs7Ozs7O0lBQ0ssd0NBQWdCOzs7OztJQUF4Qjs7WUFDUSxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDOztZQUM5QyxLQUFLLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQyxPQUFPOztZQUMzQixHQUFHLEdBQUcsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPO1FBRTlCLE9BQU8sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDeEIsQ0FBQzs7Ozs7O0lBRU8sdUNBQWU7Ozs7O0lBQXZCLFVBQXdCLFNBQVM7O1lBQ3ZCLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSTtRQUM1QixJQUFJLENBQUMsSUFBSSxHQUFHLGVBQWUsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUUxRCxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssVUFBVSxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUU7WUFDM0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ25DO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sb0NBQVk7Ozs7O0lBQXBCLFVBQXFCLE9BQWU7O1FBQ2hDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUVoRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztTQUN0QjtRQUVELHVDQUF1QztRQUN2QyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDdEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDdEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdEI7UUFFRCwrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUU5Qiw2QkFBNkI7UUFDN0IsSUFBSSxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUU7O2dCQUMvQyxLQUFLLEdBQUcsQ0FBQzs7Z0JBQ1QsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTO1lBRXhCLDZDQUE2QztZQUM3QyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2IsNkNBQW9DLEVBQW5DLGFBQUssRUFBRSxXQUFHLENBQTBCO2FBQ3hDO2lCQUFNO2dCQUNILCtDQUFzQyxFQUFyQyxhQUFLLEVBQUUsV0FBRyxDQUE0QjthQUMxQztZQUVELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBRTFDLGtCQUFrQjtZQUNsQixJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztTQUNuQztJQUNMLENBQUM7O2dCQTlRSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGdCQUFnQjtvQkFDMUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLElBQUksRUFBRSxFQUFDLE1BQU0sRUFBRSxZQUFZLEVBQUM7b0JBQzVCLFFBQVEsRUFBRSx5ckhBcURYO2lCQUNGOzs7O2dCQWhLUSxtQkFBbUI7Ozs4QkFxS3ZCLFlBQVksU0FBQyxxQkFBcUI7MkJBQ2xDLFlBQVksU0FBQyxrQkFBa0I7MEJBQy9CLFlBQVksU0FBQyxpQkFBaUI7MEJBQzlCLFlBQVksU0FBQyxpQkFBaUI7NEJBQzlCLFlBQVksU0FBQyxtQkFBbUI7OEJBQ2hDLFlBQVksU0FBQyxxQkFBcUI7MkJBS2xDLEtBQUs7Z0NBS0wsS0FBSztpQ0FLTCxLQUFLOzJCQUtMLEtBQUs7eUJBTUwsS0FBSztpQ0FLTCxLQUFLOzBCQUtMLEtBQUs7dUJBS0wsS0FBSzsyQkFLTCxLQUFLOzZCQVFMLE1BQU07dUJBS04sS0FBSzs7SUFnSlYsb0JBQUM7Q0FBQSxBQS9RRCxJQStRQztTQXBOWSxhQUFhOzs7SUFDdEIsa0NBQWM7O0lBQ2QsOEJBQXFCOztJQUVyQixvQ0FBd0U7O0lBQ3hFLGlDQUErRDs7SUFDL0QsZ0NBQTREOztJQUM1RCxnQ0FBNEQ7O0lBQzVELGtDQUFrRTs7SUFDbEUsb0NBQXdFOzs7OztJQUt4RSxpQ0FBMkI7Ozs7O0lBSzNCLHNDQUFnQzs7Ozs7SUFLaEMsdUNBQWlDOzs7OztJQUtqQyxpQ0FBMkI7Ozs7OztJQU0zQiwrQkFBeUI7Ozs7O0lBS3pCLHVDQUFnQzs7Ozs7SUFLaEMsZ0NBQXlCOzs7OztJQUt6Qiw2QkFBa0I7Ozs7O0lBS2xCLGlDQUEwQjs7Ozs7Ozs7SUFRMUIsbUNBQXNEOzs7OztJQUt0RCw2QkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgQ29udGVudENoaWxkLFxyXG4gICAgRGlyZWN0aXZlLFxyXG4gICAgRXZlbnRFbWl0dGVyLFxyXG4gICAgSW5wdXQsXHJcbiAgICBPdXRwdXQsXHJcbiAgICBPbkNoYW5nZXMsXHJcbiAgICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSxcclxuICAgIFNpbXBsZUNoYW5nZXMsXHJcbiAgICBUZW1wbGF0ZVJlZlxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBnZXRWYWx1ZUluUmFuZ2UsIGlzTnVtYmVyIH0gZnJvbSAnLi4vdXRpbC91dGlsJztcclxuaW1wb3J0IHsgTmd0UGFnaW5hdGlvbkNvbmZpZyB9IGZyb20gJy4vcGFnaW5hdGlvbi1jb25maWcnO1xyXG5cclxuLyoqXHJcbiAqIENvbnRleHQgZm9yIHRoZSBwYWdpbmF0aW9uICdmaXJzdCcsICdwcmV2aW91cycsICduZXh0JywgJ2xhc3QnIG9yICdlbGxpcHNpcycgY2VsbFxyXG4gKiBpbiBjYXNlIHlvdSB3YW50IHRvIG92ZXJyaWRlIG9uZVxyXG4gKlxyXG4gKiBAc2luY2UgNC4xLjBcclxuICovXHJcbmV4cG9ydCBpbnRlcmZhY2UgTmd0UGFnaW5hdGlvbkxpbmtDb250ZXh0IHtcclxuICAgIC8qKlxyXG4gICAgICogUGFnZSBudW1iZXIgY3VycmVudGx5IHNlbGVjdGVkXHJcbiAgICAgKi9cclxuICAgIGN1cnJlbnRQYWdlOiBudW1iZXI7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJZiB0cnVlIHRoZSBsaW5rIGluIHF1ZXN0aW9uIGlzIGRpc2FibGVkXHJcbiAgICAgKi9cclxuICAgIGRpc2FibGVkOiBib29sZWFuO1xyXG59XHJcblxyXG4vKipcclxuICogQ29udGV4dCBmb3IgdGhlIHBhZ2luYXRpb24gJ251bWJlcicgY2VsbCBpbiBjYXNlIHlvdSB3YW50IHRvIG92ZXJyaWRlIG9uZS5cclxuICogRXh0ZW5kcyAnTmd0UGFnaW5hdGlvbkxpbmtDb250ZXh0J1xyXG4gKlxyXG4gKiBAc2luY2UgNC4xLjBcclxuICovXHJcbmV4cG9ydCBpbnRlcmZhY2UgTmd0UGFnaW5hdGlvbk51bWJlckNvbnRleHQgZXh0ZW5kcyBOZ3RQYWdpbmF0aW9uTGlua0NvbnRleHQge1xyXG4gICAgLyoqXHJcbiAgICAgKiBQYWdlIG51bWJlciBkaXNwbGF5ZWQgYnkgdGhlIGN1cnJlbnQgY2VsbFxyXG4gICAgICovXHJcbiAgICAkaW1wbGljaXQ6IG51bWJlcjtcclxufVxyXG5cclxuLyoqXHJcbiAqIFRoZSBkaXJlY3RpdmUgdG8gbWF0Y2ggdGhlICdlbGxpcHNpcycgY2VsbCB0ZW1wbGF0ZVxyXG4gKlxyXG4gKiBAc2luY2UgNC4xLjBcclxuICovXHJcbkBEaXJlY3RpdmUoe3NlbGVjdG9yOiAnbmctdGVtcGxhdGVbbmd0UGFnaW5hdGlvbkVsbGlwc2lzXSd9KVxyXG5leHBvcnQgY2xhc3MgTmd0UGFnaW5hdGlvbkVsbGlwc2lzIHtcclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyB0ZW1wbGF0ZVJlZjogVGVtcGxhdGVSZWY8Tmd0UGFnaW5hdGlvbkxpbmtDb250ZXh0Pikge1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogVGhlIGRpcmVjdGl2ZSB0byBtYXRjaCB0aGUgJ2ZpcnN0JyBjZWxsIHRlbXBsYXRlXHJcbiAqXHJcbiAqIEBzaW5jZSA0LjEuMFxyXG4gKi9cclxuQERpcmVjdGl2ZSh7c2VsZWN0b3I6ICduZy10ZW1wbGF0ZVtuZ3RQYWdpbmF0aW9uRmlyc3RdJ30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYWdpbmF0aW9uRmlyc3Qge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHRlbXBsYXRlUmVmOiBUZW1wbGF0ZVJlZjxOZ3RQYWdpbmF0aW9uTGlua0NvbnRleHQ+KSB7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUaGUgZGlyZWN0aXZlIHRvIG1hdGNoIHRoZSAnbGFzdCcgY2VsbCB0ZW1wbGF0ZVxyXG4gKlxyXG4gKiBAc2luY2UgNC4xLjBcclxuICovXHJcbkBEaXJlY3RpdmUoe3NlbGVjdG9yOiAnbmctdGVtcGxhdGVbbmd0UGFnaW5hdGlvbkxhc3RdJ30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYWdpbmF0aW9uTGFzdCB7XHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdGVtcGxhdGVSZWY6IFRlbXBsYXRlUmVmPE5ndFBhZ2luYXRpb25MaW5rQ29udGV4dD4pIHtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIFRoZSBkaXJlY3RpdmUgdG8gbWF0Y2ggdGhlICduZXh0JyBjZWxsIHRlbXBsYXRlXHJcbiAqXHJcbiAqIEBzaW5jZSA0LjEuMFxyXG4gKi9cclxuQERpcmVjdGl2ZSh7c2VsZWN0b3I6ICduZy10ZW1wbGF0ZVtuZ3RQYWdpbmF0aW9uTmV4dF0nfSlcclxuZXhwb3J0IGNsYXNzIE5ndFBhZ2luYXRpb25OZXh0IHtcclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyB0ZW1wbGF0ZVJlZjogVGVtcGxhdGVSZWY8Tmd0UGFnaW5hdGlvbkxpbmtDb250ZXh0Pikge1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogVGhlIGRpcmVjdGl2ZSB0byBtYXRjaCB0aGUgcGFnZSAnbnVtYmVyJyBjZWxsIHRlbXBsYXRlXHJcbiAqXHJcbiAqIEBzaW5jZSA0LjEuMFxyXG4gKi9cclxuQERpcmVjdGl2ZSh7c2VsZWN0b3I6ICduZy10ZW1wbGF0ZVtuZ3RQYWdpbmF0aW9uTnVtYmVyXSd9KVxyXG5leHBvcnQgY2xhc3MgTmd0UGFnaW5hdGlvbk51bWJlciB7XHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdGVtcGxhdGVSZWY6IFRlbXBsYXRlUmVmPE5ndFBhZ2luYXRpb25OdW1iZXJDb250ZXh0Pikge1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogVGhlIGRpcmVjdGl2ZSB0byBtYXRjaCB0aGUgJ3ByZXZpb3VzJyBjZWxsIHRlbXBsYXRlXHJcbiAqXHJcbiAqIEBzaW5jZSA0LjEuMFxyXG4gKi9cclxuQERpcmVjdGl2ZSh7c2VsZWN0b3I6ICduZy10ZW1wbGF0ZVtuZ3RQYWdpbmF0aW9uUHJldmlvdXNdJ30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYWdpbmF0aW9uUHJldmlvdXMge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHRlbXBsYXRlUmVmOiBUZW1wbGF0ZVJlZjxOZ3RQYWdpbmF0aW9uTGlua0NvbnRleHQ+KSB7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBBIGNvbXBvbmVudCB0aGF0IGRpc3BsYXlzIHBhZ2UgbnVtYmVycyBhbmQgYWxsb3dzIHRvIGN1c3RvbWl6ZSB0aGVtIGluIHNldmVyYWwgd2F5c1xyXG4gKi9cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC1wYWdpbmF0aW9uJyxcclxuICAgIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxyXG4gICAgaG9zdDogeydyb2xlJzogJ25hdmlnYXRpb24nfSxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICA8bmctdGVtcGxhdGUgI2ZpcnN0PjxzcGFuIGFyaWEtaGlkZGVuPVwidHJ1ZVwiIGkxOG49XCJAQG5ndC5wYWdpbmF0aW9uLmZpcnN0XCI+JmxhcXVvOyZsYXF1bzs8L3NwYW4+PC9uZy10ZW1wbGF0ZT5cclxuICAgIDxuZy10ZW1wbGF0ZSAjcHJldmlvdXM+PHNwYW4gYXJpYS1oaWRkZW49XCJ0cnVlXCIgaTE4bj1cIkBAbmd0LnBhZ2luYXRpb24ucHJldmlvdXNcIj4mbGFxdW87PC9zcGFuPjwvbmctdGVtcGxhdGU+XHJcbiAgICA8bmctdGVtcGxhdGUgI25leHQ+PHNwYW4gYXJpYS1oaWRkZW49XCJ0cnVlXCIgaTE4bj1cIkBAbmd0LnBhZ2luYXRpb24ubmV4dFwiPiZyYXF1bzs8L3NwYW4+PC9uZy10ZW1wbGF0ZT5cclxuICAgIDxuZy10ZW1wbGF0ZSAjbGFzdD48c3BhbiBhcmlhLWhpZGRlbj1cInRydWVcIiBpMThuPVwiQEBuZ3QucGFnaW5hdGlvbi5sYXN0XCI+JnJhcXVvOyZyYXF1bzs8L3NwYW4+PC9uZy10ZW1wbGF0ZT5cclxuICAgIDxuZy10ZW1wbGF0ZSAjZWxsaXBzaXM+Li4uPC9uZy10ZW1wbGF0ZT5cclxuICAgIDxuZy10ZW1wbGF0ZSAjZGVmYXVsdE51bWJlciBsZXQtcGFnZSBsZXQtY3VycmVudFBhZ2U9XCJjdXJyZW50UGFnZVwiPlxyXG4gICAgICB7eyBwYWdlIH19XHJcbiAgICAgIDxzcGFuICpuZ0lmPVwicGFnZSA9PT0gY3VycmVudFBhZ2VcIiBjbGFzcz1cInNyLW9ubHlcIj4oY3VycmVudCk8L3NwYW4+XHJcbiAgICA8L25nLXRlbXBsYXRlPlxyXG4gICAgPHVsIFtjbGFzc109XCIncGFnaW5hdGlvbicgKyAoc2l6ZSA/ICcgcGFnaW5hdGlvbi0nICsgc2l6ZSA6ICcnKVwiPlxyXG4gICAgICA8bGkgKm5nSWY9XCJib3VuZGFyeUxpbmtzXCIgY2xhc3M9XCJwYWdlLWl0ZW1cIlxyXG4gICAgICAgIFtjbGFzcy5kaXNhYmxlZF09XCJwcmV2aW91c0Rpc2FibGVkKClcIj5cclxuICAgICAgICA8YSBhcmlhLWxhYmVsPVwiRmlyc3RcIiBpMThuLWFyaWEtbGFiZWw9XCJAQG5ndC5wYWdpbmF0aW9uLmZpcnN0LWFyaWFcIiBjbGFzcz1cInBhZ2UtbGlua1wiIGhyZWZcclxuICAgICAgICAgIChjbGljayk9XCJzZWxlY3RQYWdlKDEpOyAkZXZlbnQucHJldmVudERlZmF1bHQoKVwiIFthdHRyLnRhYmluZGV4XT1cIihoYXNQcmV2aW91cygpID8gbnVsbCA6ICctMScpXCI+XHJcbiAgICAgICAgICA8bmctdGVtcGxhdGUgW25nVGVtcGxhdGVPdXRsZXRdPVwidHBsRmlyc3Q/LnRlbXBsYXRlUmVmIHx8IGZpcnN0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldENvbnRleHRdPVwie2Rpc2FibGVkOiBwcmV2aW91c0Rpc2FibGVkKCksIGN1cnJlbnRQYWdlOiBwYWdlfVwiPjwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgPC9hPlxyXG4gICAgICA8L2xpPlxyXG4gICAgICA8bGkgKm5nSWY9XCJkaXJlY3Rpb25MaW5rc1wiIGNsYXNzPVwicGFnZS1pdGVtXCJcclxuICAgICAgICBbY2xhc3MuZGlzYWJsZWRdPVwicHJldmlvdXNEaXNhYmxlZCgpXCI+XHJcbiAgICAgICAgPGEgYXJpYS1sYWJlbD1cIlByZXZpb3VzXCIgaTE4bi1hcmlhLWxhYmVsPVwiQEBuZ3QucGFnaW5hdGlvbi5wcmV2aW91cy1hcmlhXCIgY2xhc3M9XCJwYWdlLWxpbmtcIiBocmVmXHJcbiAgICAgICAgICAoY2xpY2spPVwic2VsZWN0UGFnZShwYWdlLTEpOyAkZXZlbnQucHJldmVudERlZmF1bHQoKVwiIFthdHRyLnRhYmluZGV4XT1cIihoYXNQcmV2aW91cygpID8gbnVsbCA6ICctMScpXCI+XHJcbiAgICAgICAgICA8bmctdGVtcGxhdGUgW25nVGVtcGxhdGVPdXRsZXRdPVwidHBsUHJldmlvdXM/LnRlbXBsYXRlUmVmIHx8IHByZXZpb3VzXCJcclxuICAgICAgICAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldENvbnRleHRdPVwie2Rpc2FibGVkOiBwcmV2aW91c0Rpc2FibGVkKCl9XCI+PC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICA8L2E+XHJcbiAgICAgIDwvbGk+XHJcbiAgICAgIDxsaSAqbmdGb3I9XCJsZXQgcGFnZU51bWJlciBvZiBwYWdlc1wiIGNsYXNzPVwicGFnZS1pdGVtXCIgW2NsYXNzLmFjdGl2ZV09XCJwYWdlTnVtYmVyID09PSBwYWdlXCJcclxuICAgICAgICBbY2xhc3MuZGlzYWJsZWRdPVwiaXNFbGxpcHNpcyhwYWdlTnVtYmVyKSB8fCBkaXNhYmxlZFwiPlxyXG4gICAgICAgIDxhICpuZ0lmPVwiaXNFbGxpcHNpcyhwYWdlTnVtYmVyKVwiIGNsYXNzPVwicGFnZS1saW5rXCI+XHJcbiAgICAgICAgICA8bmctdGVtcGxhdGUgW25nVGVtcGxhdGVPdXRsZXRdPVwidHBsRWxsaXBzaXM/LnRlbXBsYXRlUmVmIHx8IGVsbGlwc2lzXCJcclxuICAgICAgICAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldENvbnRleHRdPVwie2Rpc2FibGVkOiB0cnVlLCBjdXJyZW50UGFnZTogcGFnZX1cIj48L25nLXRlbXBsYXRlPlxyXG4gICAgICAgIDwvYT5cclxuICAgICAgICA8YSAqbmdJZj1cIiFpc0VsbGlwc2lzKHBhZ2VOdW1iZXIpXCIgY2xhc3M9XCJwYWdlLWxpbmtcIiBocmVmIChjbGljayk9XCJzZWxlY3RQYWdlKHBhZ2VOdW1iZXIpOyAkZXZlbnQucHJldmVudERlZmF1bHQoKVwiPlxyXG4gICAgICAgICAgPG5nLXRlbXBsYXRlIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInRwbE51bWJlcj8udGVtcGxhdGVSZWYgfHwgZGVmYXVsdE51bWJlclwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRDb250ZXh0XT1cIntkaXNhYmxlZDogZGlzYWJsZWQsICRpbXBsaWNpdDogcGFnZU51bWJlciwgY3VycmVudFBhZ2U6IHBhZ2V9XCI+PC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICA8L2E+XHJcbiAgICAgIDwvbGk+XHJcbiAgICAgIDxsaSAqbmdJZj1cImRpcmVjdGlvbkxpbmtzXCIgY2xhc3M9XCJwYWdlLWl0ZW1cIiBbY2xhc3MuZGlzYWJsZWRdPVwibmV4dERpc2FibGVkKClcIj5cclxuICAgICAgICA8YSBhcmlhLWxhYmVsPVwiTmV4dFwiIGkxOG4tYXJpYS1sYWJlbD1cIkBAbmd0LnBhZ2luYXRpb24ubmV4dC1hcmlhXCIgY2xhc3M9XCJwYWdlLWxpbmtcIiBocmVmXHJcbiAgICAgICAgICAoY2xpY2spPVwic2VsZWN0UGFnZShwYWdlKzEpOyAkZXZlbnQucHJldmVudERlZmF1bHQoKVwiIFthdHRyLnRhYmluZGV4XT1cIihoYXNOZXh0KCkgPyBudWxsIDogJy0xJylcIj5cclxuICAgICAgICAgIDxuZy10ZW1wbGF0ZSBbbmdUZW1wbGF0ZU91dGxldF09XCJ0cGxOZXh0Py50ZW1wbGF0ZVJlZiB8fCBuZXh0XCJcclxuICAgICAgICAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldENvbnRleHRdPVwie2Rpc2FibGVkOiBuZXh0RGlzYWJsZWQoKSwgY3VycmVudFBhZ2U6IHBhZ2V9XCI+PC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICA8L2E+XHJcbiAgICAgIDwvbGk+XHJcbiAgICAgIDxsaSAqbmdJZj1cImJvdW5kYXJ5TGlua3NcIiBjbGFzcz1cInBhZ2UtaXRlbVwiIFtjbGFzcy5kaXNhYmxlZF09XCJuZXh0RGlzYWJsZWQoKVwiPlxyXG4gICAgICAgIDxhIGFyaWEtbGFiZWw9XCJMYXN0XCIgaTE4bi1hcmlhLWxhYmVsPVwiQEBuZ3QucGFnaW5hdGlvbi5sYXN0LWFyaWFcIiBjbGFzcz1cInBhZ2UtbGlua1wiIGhyZWZcclxuICAgICAgICAgIChjbGljayk9XCJzZWxlY3RQYWdlKHBhZ2VDb3VudCk7ICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXCIgW2F0dHIudGFiaW5kZXhdPVwiKGhhc05leHQoKSA/IG51bGwgOiAnLTEnKVwiPlxyXG4gICAgICAgICAgPG5nLXRlbXBsYXRlIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInRwbExhc3Q/LnRlbXBsYXRlUmVmIHx8IGxhc3RcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgIFtuZ1RlbXBsYXRlT3V0bGV0Q29udGV4dF09XCJ7ZGlzYWJsZWQ6IG5leHREaXNhYmxlZCgpLCBjdXJyZW50UGFnZTogcGFnZX1cIj48L25nLXRlbXBsYXRlPlxyXG4gICAgICAgIDwvYT5cclxuICAgICAgPC9saT5cclxuICAgIDwvdWw+XHJcbiAgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0UGFnaW5hdGlvbiBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcbiAgICBwYWdlQ291bnQgPSAwO1xyXG4gICAgcGFnZXM6IG51bWJlcltdID0gW107XHJcblxyXG4gICAgQENvbnRlbnRDaGlsZChOZ3RQYWdpbmF0aW9uRWxsaXBzaXMpIHRwbEVsbGlwc2lzOiBOZ3RQYWdpbmF0aW9uRWxsaXBzaXM7XHJcbiAgICBAQ29udGVudENoaWxkKE5ndFBhZ2luYXRpb25GaXJzdCkgdHBsRmlyc3Q6IE5ndFBhZ2luYXRpb25GaXJzdDtcclxuICAgIEBDb250ZW50Q2hpbGQoTmd0UGFnaW5hdGlvbkxhc3QpIHRwbExhc3Q6IE5ndFBhZ2luYXRpb25MYXN0O1xyXG4gICAgQENvbnRlbnRDaGlsZChOZ3RQYWdpbmF0aW9uTmV4dCkgdHBsTmV4dDogTmd0UGFnaW5hdGlvbk5leHQ7XHJcbiAgICBAQ29udGVudENoaWxkKE5ndFBhZ2luYXRpb25OdW1iZXIpIHRwbE51bWJlcjogTmd0UGFnaW5hdGlvbk51bWJlcjtcclxuICAgIEBDb250ZW50Q2hpbGQoTmd0UGFnaW5hdGlvblByZXZpb3VzKSB0cGxQcmV2aW91czogTmd0UGFnaW5hdGlvblByZXZpb3VzO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogV2hldGhlciB0byBkaXNhYmxlIGJ1dHRvbnMgZnJvbSB1c2VyIGlucHV0XHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGRpc2FibGVkOiBib29sZWFuO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogIFdoZXRoZXIgdG8gc2hvdyB0aGUgXCJGaXJzdFwiIGFuZCBcIkxhc3RcIiBwYWdlIGxpbmtzXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGJvdW5kYXJ5TGlua3M6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgV2hldGhlciB0byBzaG93IHRoZSBcIk5leHRcIiBhbmQgXCJQcmV2aW91c1wiIHBhZ2UgbGlua3NcclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgZGlyZWN0aW9uTGlua3M6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgV2hldGhlciB0byBzaG93IGVsbGlwc2lzIHN5bWJvbHMgYW5kIGZpcnN0L2xhc3QgcGFnZSBudW1iZXJzIHdoZW4gbWF4U2l6ZSA+IG51bWJlciBvZiBwYWdlc1xyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBlbGxpcHNlczogYm9vbGVhbjtcclxuXHJcbiAgICAvKipcclxuICAgICAqICBXaGV0aGVyIHRvIHJvdGF0ZSBwYWdlcyB3aGVuIG1heFNpemUgPiBudW1iZXIgb2YgcGFnZXMuXHJcbiAgICAgKiAgQ3VycmVudCBwYWdlIHdpbGwgYmUgaW4gdGhlIG1pZGRsZVxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSByb3RhdGU6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgTnVtYmVyIG9mIGl0ZW1zIGluIGNvbGxlY3Rpb24uXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGNvbGxlY3Rpb25TaXplOiBudW1iZXI7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgTWF4aW11bSBudW1iZXIgb2YgcGFnZXMgdG8gZGlzcGxheS5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgbWF4U2l6ZTogbnVtYmVyO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogIEN1cnJlbnQgcGFnZS4gUGFnZSBudW1iZXJzIHN0YXJ0IHdpdGggMVxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBwYWdlID0gMTtcclxuXHJcbiAgICAvKipcclxuICAgICAqICBOdW1iZXIgb2YgaXRlbXMgcGVyIHBhZ2UuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHBhZ2VTaXplOiBudW1iZXI7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgQW4gZXZlbnQgZmlyZWQgd2hlbiB0aGUgcGFnZSBpcyBjaGFuZ2VkLlxyXG4gICAgICogIEV2ZW50J3MgcGF5bG9hZCBlcXVhbHMgdG8gdGhlIG5ld2x5IHNlbGVjdGVkIHBhZ2UuXHJcbiAgICAgKiAgV2lsbCBmaXJlIG9ubHkgaWYgY29sbGVjdGlvbiBzaXplIGlzIHNldCBhbmQgYWxsIHZhbHVlcyBhcmUgdmFsaWQuXHJcbiAgICAgKiAgUGFnZSBudW1iZXJzIHN0YXJ0IHdpdGggMVxyXG4gICAgICovXHJcbiAgICBAT3V0cHV0KCkgcGFnZUNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8bnVtYmVyPih0cnVlKTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFBhZ2luYXRpb24gZGlzcGxheSBzaXplOiBzbWFsbCBvciBsYXJnZVxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBzaXplOiAnc20nIHwgJ2xnJztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb25maWc6IE5ndFBhZ2luYXRpb25Db25maWcpIHtcclxuICAgICAgICB0aGlzLmRpc2FibGVkID0gY29uZmlnLmRpc2FibGVkO1xyXG4gICAgICAgIHRoaXMuYm91bmRhcnlMaW5rcyA9IGNvbmZpZy5ib3VuZGFyeUxpbmtzO1xyXG4gICAgICAgIHRoaXMuZGlyZWN0aW9uTGlua3MgPSBjb25maWcuZGlyZWN0aW9uTGlua3M7XHJcbiAgICAgICAgdGhpcy5lbGxpcHNlcyA9IGNvbmZpZy5lbGxpcHNlcztcclxuICAgICAgICB0aGlzLm1heFNpemUgPSBjb25maWcubWF4U2l6ZTtcclxuICAgICAgICB0aGlzLnBhZ2VTaXplID0gY29uZmlnLnBhZ2VTaXplO1xyXG4gICAgICAgIHRoaXMucm90YXRlID0gY29uZmlnLnJvdGF0ZTtcclxuICAgICAgICB0aGlzLnNpemUgPSBjb25maWcuc2l6ZTtcclxuICAgIH1cclxuXHJcbiAgICBoYXNQcmV2aW91cygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wYWdlID4gMTtcclxuICAgIH1cclxuXHJcbiAgICBoYXNOZXh0KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnBhZ2UgPCB0aGlzLnBhZ2VDb3VudDtcclxuICAgIH1cclxuXHJcbiAgICBuZXh0RGlzYWJsZWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICF0aGlzLmhhc05leHQoKSB8fCB0aGlzLmRpc2FibGVkO1xyXG4gICAgfVxyXG5cclxuICAgIHByZXZpb3VzRGlzYWJsZWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICF0aGlzLmhhc1ByZXZpb3VzKCkgfHwgdGhpcy5kaXNhYmxlZDtcclxuICAgIH1cclxuXHJcbiAgICBzZWxlY3RQYWdlKHBhZ2VOdW1iZXI6IG51bWJlcik6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX3VwZGF0ZVBhZ2VzKHBhZ2VOdW1iZXIpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl91cGRhdGVQYWdlcyh0aGlzLnBhZ2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGlzRWxsaXBzaXMocGFnZU51bWJlcik6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBwYWdlTnVtYmVyID09PSAtMTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFwcGVuZHMgZWxsaXBzZXMgYW5kIGZpcnN0L2xhc3QgcGFnZSBudW1iZXIgdG8gdGhlIGRpc3BsYXllZCBwYWdlc1xyXG4gICAgICovXHJcbiAgICBwcml2YXRlIF9hcHBseUVsbGlwc2VzKHN0YXJ0OiBudW1iZXIsIGVuZDogbnVtYmVyKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZWxsaXBzZXMpIHtcclxuICAgICAgICAgICAgaWYgKHN0YXJ0ID4gMCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHN0YXJ0ID4gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucGFnZXMudW5zaGlmdCgtMSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLnBhZ2VzLnVuc2hpZnQoMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKGVuZCA8IHRoaXMucGFnZUNvdW50KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZW5kIDwgKHRoaXMucGFnZUNvdW50IC0gMSkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnBhZ2VzLnB1c2goLTEpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdGhpcy5wYWdlcy5wdXNoKHRoaXMucGFnZUNvdW50KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJvdGF0ZXMgcGFnZSBudW1iZXJzIGJhc2VkIG9uIG1heFNpemUgaXRlbXMgdmlzaWJsZS5cclxuICAgICAqIEN1cnJlbnRseSBzZWxlY3RlZCBwYWdlIHN0YXlzIGluIHRoZSBtaWRkbGU6XHJcbiAgICAgKlxyXG4gICAgICogRXguIGZvciBzZWxlY3RlZCBwYWdlID0gNjpcclxuICAgICAqIFs1LCo2Kiw3XSBmb3IgbWF4U2l6ZSA9IDNcclxuICAgICAqIFs0LDUsKjYqLDddIGZvciBtYXhTaXplID0gNFxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIF9hcHBseVJvdGF0aW9uKCk6IFtudW1iZXIsIG51bWJlcl0ge1xyXG4gICAgICAgIGxldCBzdGFydCA9IDA7XHJcbiAgICAgICAgbGV0IGVuZCA9IHRoaXMucGFnZUNvdW50O1xyXG4gICAgICAgIGxldCBsZWZ0T2Zmc2V0ID0gTWF0aC5mbG9vcih0aGlzLm1heFNpemUgLyAyKTtcclxuICAgICAgICBsZXQgcmlnaHRPZmZzZXQgPSB0aGlzLm1heFNpemUgJSAyID09PSAwID8gbGVmdE9mZnNldCAtIDEgOiBsZWZ0T2Zmc2V0O1xyXG5cclxuICAgICAgICBpZiAodGhpcy5wYWdlIDw9IGxlZnRPZmZzZXQpIHtcclxuICAgICAgICAgICAgLy8gdmVyeSBiZWdpbm5pbmcsIG5vIHJvdGF0aW9uIC0+IFswLi5tYXhTaXplXVxyXG4gICAgICAgICAgICBlbmQgPSB0aGlzLm1heFNpemU7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLnBhZ2VDb3VudCAtIHRoaXMucGFnZSA8IGxlZnRPZmZzZXQpIHtcclxuICAgICAgICAgICAgLy8gdmVyeSBlbmQsIG5vIHJvdGF0aW9uIC0+IFtsZW4tbWF4U2l6ZS4ubGVuXVxyXG4gICAgICAgICAgICBzdGFydCA9IHRoaXMucGFnZUNvdW50IC0gdGhpcy5tYXhTaXplO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIC8vIHJvdGF0ZVxyXG4gICAgICAgICAgICBzdGFydCA9IHRoaXMucGFnZSAtIGxlZnRPZmZzZXQgLSAxO1xyXG4gICAgICAgICAgICBlbmQgPSB0aGlzLnBhZ2UgKyByaWdodE9mZnNldDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBbc3RhcnQsIGVuZF07XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQYWdpbmF0ZXMgcGFnZSBudW1iZXJzIGJhc2VkIG9uIG1heFNpemUgaXRlbXMgcGVyIHBhZ2VcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBfYXBwbHlQYWdpbmF0aW9uKCk6IFtudW1iZXIsIG51bWJlcl0ge1xyXG4gICAgICAgIGxldCBwYWdlID0gTWF0aC5jZWlsKHRoaXMucGFnZSAvIHRoaXMubWF4U2l6ZSkgLSAxO1xyXG4gICAgICAgIGxldCBzdGFydCA9IHBhZ2UgKiB0aGlzLm1heFNpemU7XHJcbiAgICAgICAgbGV0IGVuZCA9IHN0YXJ0ICsgdGhpcy5tYXhTaXplO1xyXG5cclxuICAgICAgICByZXR1cm4gW3N0YXJ0LCBlbmRdO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX3NldFBhZ2VJblJhbmdlKG5ld1BhZ2VObykge1xyXG4gICAgICAgIGNvbnN0IHByZXZQYWdlTm8gPSB0aGlzLnBhZ2U7XHJcbiAgICAgICAgdGhpcy5wYWdlID0gZ2V0VmFsdWVJblJhbmdlKG5ld1BhZ2VObywgdGhpcy5wYWdlQ291bnQsIDEpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5wYWdlICE9PSBwcmV2UGFnZU5vICYmIGlzTnVtYmVyKHRoaXMuY29sbGVjdGlvblNpemUpKSB7XHJcbiAgICAgICAgICAgIHRoaXMucGFnZUNoYW5nZS5lbWl0KHRoaXMucGFnZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX3VwZGF0ZVBhZ2VzKG5ld1BhZ2U6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMucGFnZUNvdW50ID0gTWF0aC5jZWlsKHRoaXMuY29sbGVjdGlvblNpemUgLyB0aGlzLnBhZ2VTaXplKTtcclxuXHJcbiAgICAgICAgaWYgKCFpc051bWJlcih0aGlzLnBhZ2VDb3VudCkpIHtcclxuICAgICAgICAgICAgdGhpcy5wYWdlQ291bnQgPSAwO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gZmlsbC1pbiBtb2RlbCBuZWVkZWQgdG8gcmVuZGVyIHBhZ2VzXHJcbiAgICAgICAgdGhpcy5wYWdlcy5sZW5ndGggPSAwO1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAxOyBpIDw9IHRoaXMucGFnZUNvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgdGhpcy5wYWdlcy5wdXNoKGkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gc2V0IHBhZ2Ugd2l0aGluIDEuLm1heCByYW5nZVxyXG4gICAgICAgIHRoaXMuX3NldFBhZ2VJblJhbmdlKG5ld1BhZ2UpO1xyXG5cclxuICAgICAgICAvLyBhcHBseSBtYXhTaXplIGlmIG5lY2Vzc2FyeVxyXG4gICAgICAgIGlmICh0aGlzLm1heFNpemUgPiAwICYmIHRoaXMucGFnZUNvdW50ID4gdGhpcy5tYXhTaXplKSB7XHJcbiAgICAgICAgICAgIGxldCBzdGFydCA9IDA7XHJcbiAgICAgICAgICAgIGxldCBlbmQgPSB0aGlzLnBhZ2VDb3VudDtcclxuXHJcbiAgICAgICAgICAgIC8vIGVpdGhlciBwYWdpbmF0aW5nIG9yIHJvdGF0aW5nIHBhZ2UgbnVtYmVyc1xyXG4gICAgICAgICAgICBpZiAodGhpcy5yb3RhdGUpIHtcclxuICAgICAgICAgICAgICAgIFtzdGFydCwgZW5kXSA9IHRoaXMuX2FwcGx5Um90YXRpb24oKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIFtzdGFydCwgZW5kXSA9IHRoaXMuX2FwcGx5UGFnaW5hdGlvbigpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnBhZ2VzID0gdGhpcy5wYWdlcy5zbGljZShzdGFydCwgZW5kKTtcclxuXHJcbiAgICAgICAgICAgIC8vIGFkZGluZyBlbGxpcHNlc1xyXG4gICAgICAgICAgICB0aGlzLl9hcHBseUVsbGlwc2VzKHN0YXJ0LCBlbmQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=