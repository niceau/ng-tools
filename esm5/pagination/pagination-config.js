/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtPagination component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the paginations used in the application.
 */
var NgtPaginationConfig = /** @class */ (function () {
    function NgtPaginationConfig() {
        this.disabled = false;
        this.boundaryLinks = false;
        this.directionLinks = true;
        this.ellipses = true;
        this.maxSize = 0;
        this.pageSize = 10;
        this.rotate = false;
    }
    NgtPaginationConfig.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */ NgtPaginationConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtPaginationConfig_Factory() { return new NgtPaginationConfig(); }, token: NgtPaginationConfig, providedIn: "root" });
    return NgtPaginationConfig;
}());
export { NgtPaginationConfig };
if (false) {
    /** @type {?} */
    NgtPaginationConfig.prototype.disabled;
    /** @type {?} */
    NgtPaginationConfig.prototype.boundaryLinks;
    /** @type {?} */
    NgtPaginationConfig.prototype.directionLinks;
    /** @type {?} */
    NgtPaginationConfig.prototype.ellipses;
    /** @type {?} */
    NgtPaginationConfig.prototype.maxSize;
    /** @type {?} */
    NgtPaginationConfig.prototype.pageSize;
    /** @type {?} */
    NgtPaginationConfig.prototype.rotate;
    /** @type {?} */
    NgtPaginationConfig.prototype.size;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi1jb25maWcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInBhZ2luYXRpb24vcGFnaW5hdGlvbi1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFPM0M7SUFBQTtRQUVJLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsYUFBUSxHQUFHLElBQUksQ0FBQztRQUNoQixZQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQ1osYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNkLFdBQU0sR0FBRyxLQUFLLENBQUM7S0FFbEI7O2dCQVZBLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7Ozs4QkFQaEM7Q0FpQkMsQUFWRCxJQVVDO1NBVFksbUJBQW1COzs7SUFDNUIsdUNBQWlCOztJQUNqQiw0Q0FBc0I7O0lBQ3RCLDZDQUFzQjs7SUFDdEIsdUNBQWdCOztJQUNoQixzQ0FBWTs7SUFDWix1Q0FBYzs7SUFDZCxxQ0FBZTs7SUFDZixtQ0FBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vKipcclxuICogQ29uZmlndXJhdGlvbiBzZXJ2aWNlIGZvciB0aGUgTmd0UGFnaW5hdGlvbiBjb21wb25lbnQuXHJcbiAqIFlvdSBjYW4gaW5qZWN0IHRoaXMgc2VydmljZSwgdHlwaWNhbGx5IGluIHlvdXIgcm9vdCBjb21wb25lbnQsIGFuZCBjdXN0b21pemUgdGhlIHZhbHVlcyBvZiBpdHMgcHJvcGVydGllcyBpblxyXG4gKiBvcmRlciB0byBwcm92aWRlIGRlZmF1bHQgdmFsdWVzIGZvciBhbGwgdGhlIHBhZ2luYXRpb25zIHVzZWQgaW4gdGhlIGFwcGxpY2F0aW9uLlxyXG4gKi9cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYWdpbmF0aW9uQ29uZmlnIHtcclxuICAgIGRpc2FibGVkID0gZmFsc2U7XHJcbiAgICBib3VuZGFyeUxpbmtzID0gZmFsc2U7XHJcbiAgICBkaXJlY3Rpb25MaW5rcyA9IHRydWU7XHJcbiAgICBlbGxpcHNlcyA9IHRydWU7XHJcbiAgICBtYXhTaXplID0gMDtcclxuICAgIHBhZ2VTaXplID0gMTA7XHJcbiAgICByb3RhdGUgPSBmYWxzZTtcclxuICAgIHNpemU6ICdzbScgfCAnbGcnO1xyXG59XHJcbiJdfQ==