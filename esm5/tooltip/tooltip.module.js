/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { NgtTooltip, NgtTooltipWindow } from './tooltip';
export { NgtTooltipConfig } from './tooltip-config';
export { NgtTooltip } from './tooltip';
var NgtTooltipModule = /** @class */ (function () {
    function NgtTooltipModule() {
    }
    /**
     * @return {?}
     */
    NgtTooltipModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtTooltipModule };
    };
    NgtTooltipModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [NgtTooltip, NgtTooltipWindow],
                    exports: [NgtTooltip],
                    entryComponents: [NgtTooltipWindow]
                },] }
    ];
    return NgtTooltipModule;
}());
export { NgtTooltipModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInRvb2x0aXAvdG9vbHRpcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBRTlELE9BQU8sRUFBRSxVQUFVLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxXQUFXLENBQUM7QUFFekQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDcEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUd2QztJQUFBO0lBU0EsQ0FBQzs7OztJQUhVLHdCQUFPOzs7SUFBZDtRQUNJLE9BQU8sRUFBQyxRQUFRLEVBQUUsZ0JBQWdCLEVBQUMsQ0FBQztJQUN4QyxDQUFDOztnQkFSSixRQUFRLFNBQUM7b0JBQ04sWUFBWSxFQUFFLENBQUMsVUFBVSxFQUFFLGdCQUFnQixDQUFDO29CQUM1QyxPQUFPLEVBQUUsQ0FBQyxVQUFVLENBQUM7b0JBQ3JCLGVBQWUsRUFBRSxDQUFDLGdCQUFnQixDQUFDO2lCQUN0Qzs7SUFLRCx1QkFBQztDQUFBLEFBVEQsSUFTQztTQUpZLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBOZ3RUb29sdGlwLCBOZ3RUb29sdGlwV2luZG93IH0gZnJvbSAnLi90b29sdGlwJztcclxuXHJcbmV4cG9ydCB7IE5ndFRvb2x0aXBDb25maWcgfSBmcm9tICcuL3Rvb2x0aXAtY29uZmlnJztcclxuZXhwb3J0IHsgTmd0VG9vbHRpcCB9IGZyb20gJy4vdG9vbHRpcCc7XHJcbmV4cG9ydCB7IFBsYWNlbWVudCB9IGZyb20gJy4uL3V0aWwvcG9zaXRpb25pbmcnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW05ndFRvb2x0aXAsIE5ndFRvb2x0aXBXaW5kb3ddLFxyXG4gICAgZXhwb3J0czogW05ndFRvb2x0aXBdLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBbTmd0VG9vbHRpcFdpbmRvd11cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndFRvb2x0aXBNb2R1bGUge1xyXG4gICAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICAgICAgcmV0dXJuIHtuZ01vZHVsZTogTmd0VG9vbHRpcE1vZHVsZX07XHJcbiAgICB9XHJcbn1cclxuIl19