/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtTooltip directive.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the tooltips used in the application.
 */
var NgtTooltipConfig = /** @class */ (function () {
    function NgtTooltipConfig() {
        this.autoClose = true;
        this.placement = 'auto';
        this.triggers = 'hover focus';
        this.disableTooltip = false;
        this.openDelay = 0;
        this.closeDelay = 0;
    }
    NgtTooltipConfig.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */ NgtTooltipConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtTooltipConfig_Factory() { return new NgtTooltipConfig(); }, token: NgtTooltipConfig, providedIn: "root" });
    return NgtTooltipConfig;
}());
export { NgtTooltipConfig };
if (false) {
    /** @type {?} */
    NgtTooltipConfig.prototype.autoClose;
    /** @type {?} */
    NgtTooltipConfig.prototype.placement;
    /** @type {?} */
    NgtTooltipConfig.prototype.triggers;
    /** @type {?} */
    NgtTooltipConfig.prototype.container;
    /** @type {?} */
    NgtTooltipConfig.prototype.disableTooltip;
    /** @type {?} */
    NgtTooltipConfig.prototype.tooltipClass;
    /** @type {?} */
    NgtTooltipConfig.prototype.openDelay;
    /** @type {?} */
    NgtTooltipConfig.prototype.closeDelay;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC1jb25maWcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInRvb2x0aXAvdG9vbHRpcC1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFRM0M7SUFBQTtRQUVJLGNBQVMsR0FBbUMsSUFBSSxDQUFDO1FBQ2pELGNBQVMsR0FBbUIsTUFBTSxDQUFDO1FBQ25DLGFBQVEsR0FBRyxhQUFhLENBQUM7UUFFekIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFFdkIsY0FBUyxHQUFHLENBQUMsQ0FBQztRQUNkLGVBQVUsR0FBRyxDQUFDLENBQUM7S0FDbEI7O2dCQVZBLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7OzsyQkFSaEM7Q0FrQkMsQUFWRCxJQVVDO1NBVFksZ0JBQWdCOzs7SUFDekIscUNBQWlEOztJQUNqRCxxQ0FBbUM7O0lBQ25DLG9DQUF5Qjs7SUFDekIscUNBQWtCOztJQUNsQiwwQ0FBdUI7O0lBQ3ZCLHdDQUFxQjs7SUFDckIscUNBQWM7O0lBQ2Qsc0NBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFBsYWNlbWVudEFycmF5IH0gZnJvbSAnLi4vdXRpbC9wb3NpdGlvbmluZyc7XHJcblxyXG4vKipcclxuICogQ29uZmlndXJhdGlvbiBzZXJ2aWNlIGZvciB0aGUgTmd0VG9vbHRpcCBkaXJlY3RpdmUuXHJcbiAqIFlvdSBjYW4gaW5qZWN0IHRoaXMgc2VydmljZSwgdHlwaWNhbGx5IGluIHlvdXIgcm9vdCBjb21wb25lbnQsIGFuZCBjdXN0b21pemUgdGhlIHZhbHVlcyBvZiBpdHMgcHJvcGVydGllcyBpblxyXG4gKiBvcmRlciB0byBwcm92aWRlIGRlZmF1bHQgdmFsdWVzIGZvciBhbGwgdGhlIHRvb2x0aXBzIHVzZWQgaW4gdGhlIGFwcGxpY2F0aW9uLlxyXG4gKi9cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RUb29sdGlwQ29uZmlnIHtcclxuICAgIGF1dG9DbG9zZTogYm9vbGVhbiB8ICdpbnNpZGUnIHwgJ291dHNpZGUnID0gdHJ1ZTtcclxuICAgIHBsYWNlbWVudDogUGxhY2VtZW50QXJyYXkgPSAnYXV0byc7XHJcbiAgICB0cmlnZ2VycyA9ICdob3ZlciBmb2N1cyc7XHJcbiAgICBjb250YWluZXI6IHN0cmluZztcclxuICAgIGRpc2FibGVUb29sdGlwID0gZmFsc2U7XHJcbiAgICB0b29sdGlwQ2xhc3M6IHN0cmluZztcclxuICAgIG9wZW5EZWxheSA9IDA7XHJcbiAgICBjbG9zZURlbGF5ID0gMDtcclxufVxyXG4iXX0=