/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtAlert component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the alerts used in the application.
 */
var NgtAlertConfig = /** @class */ (function () {
    function NgtAlertConfig() {
        this.icon = '';
        this.dismissible = false;
        this.type = 'warning';
    }
    NgtAlertConfig.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */ NgtAlertConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtAlertConfig_Factory() { return new NgtAlertConfig(); }, token: NgtAlertConfig, providedIn: "root" });
    return NgtAlertConfig;
}());
export { NgtAlertConfig };
if (false) {
    /** @type {?} */
    NgtAlertConfig.prototype.icon;
    /** @type {?} */
    NgtAlertConfig.prototype.dismissible;
    /** @type {?} */
    NgtAlertConfig.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQtY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJhbGVydC9hbGVydC1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFPM0M7SUFBQTtRQUVJLFNBQUksR0FBRyxFQUFFLENBQUM7UUFDVixnQkFBVyxHQUFHLEtBQUssQ0FBQztRQUNwQixTQUFJLEdBQUcsU0FBUyxDQUFDO0tBQ3BCOztnQkFMQSxVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7eUJBUGhDO0NBWUMsQUFMRCxJQUtDO1NBSlksY0FBYzs7O0lBQ3ZCLDhCQUFVOztJQUNWLHFDQUFvQjs7SUFDcEIsOEJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuLyoqXHJcbiAqIENvbmZpZ3VyYXRpb24gc2VydmljZSBmb3IgdGhlIE5ndEFsZXJ0IGNvbXBvbmVudC5cclxuICogWW91IGNhbiBpbmplY3QgdGhpcyBzZXJ2aWNlLCB0eXBpY2FsbHkgaW4geW91ciByb290IGNvbXBvbmVudCwgYW5kIGN1c3RvbWl6ZSB0aGUgdmFsdWVzIG9mIGl0cyBwcm9wZXJ0aWVzIGluXHJcbiAqIG9yZGVyIHRvIHByb3ZpZGUgZGVmYXVsdCB2YWx1ZXMgZm9yIGFsbCB0aGUgYWxlcnRzIHVzZWQgaW4gdGhlIGFwcGxpY2F0aW9uLlxyXG4gKi9cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RBbGVydENvbmZpZyB7XHJcbiAgICBpY29uID0gJyc7XHJcbiAgICBkaXNtaXNzaWJsZSA9IGZhbHNlO1xyXG4gICAgdHlwZSA9ICd3YXJuaW5nJztcclxufVxyXG4iXX0=