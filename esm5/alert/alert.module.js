/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtAlertComponent } from './alert.component';
export { NgtAlertComponent } from './alert.component';
export { NgtAlertConfig } from './alert-config';
var NgtAlertModule = /** @class */ (function () {
    function NgtAlertModule() {
    }
    /**
     * @return {?}
     */
    NgtAlertModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtAlertModule };
    };
    NgtAlertModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [NgtAlertComponent],
                    exports: [NgtAlertComponent],
                    imports: [CommonModule],
                    entryComponents: [NgtAlertComponent]
                },] }
    ];
    return NgtAlertModule;
}());
export { NgtAlertModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJhbGVydC9hbGVydC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUV0RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFaEQ7SUFBQTtJQVVBLENBQUM7Ozs7SUFIVSxzQkFBTzs7O0lBQWQ7UUFDSSxPQUFPLEVBQUMsUUFBUSxFQUFFLGNBQWMsRUFBQyxDQUFDO0lBQ3RDLENBQUM7O2dCQVRKLFFBQVEsU0FBQztvQkFDTixZQUFZLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztvQkFDakMsT0FBTyxFQUFFLENBQUMsaUJBQWlCLENBQUM7b0JBQzVCLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztvQkFDdkIsZUFBZSxFQUFFLENBQUMsaUJBQWlCLENBQUM7aUJBQ3ZDOztJQUtELHFCQUFDO0NBQUEsQUFWRCxJQVVDO1NBSlksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5pbXBvcnQgeyBOZ3RBbGVydENvbXBvbmVudCB9IGZyb20gJy4vYWxlcnQuY29tcG9uZW50JztcclxuXHJcbmV4cG9ydCB7IE5ndEFsZXJ0Q29tcG9uZW50IH0gZnJvbSAnLi9hbGVydC5jb21wb25lbnQnO1xyXG5leHBvcnQgeyBOZ3RBbGVydENvbmZpZyB9IGZyb20gJy4vYWxlcnQtY29uZmlnJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtOZ3RBbGVydENvbXBvbmVudF0sXHJcbiAgICBleHBvcnRzOiBbTmd0QWxlcnRDb21wb25lbnRdLFxyXG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV0sXHJcbiAgICBlbnRyeUNvbXBvbmVudHM6IFtOZ3RBbGVydENvbXBvbmVudF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndEFsZXJ0TW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgICAgIHJldHVybiB7bmdNb2R1bGU6IE5ndEFsZXJ0TW9kdWxlfTtcclxuICAgIH1cclxufVxyXG4iXX0=