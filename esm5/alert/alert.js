/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, HostBinding, Input, Output, Renderer2, ViewEncapsulation } from '@angular/core';
import { NgtAlertConfig } from './alert-config';
/**
 * Alerts can be used to provide feedback messages.
 */
var NgtAlert = /** @class */ (function () {
    function NgtAlert(config, _renderer, _element) {
        this._renderer = _renderer;
        this._element = _element;
        /**
         * An event emitted when the close button is clicked. This event has no payload. Only relevant for dismissible alerts.
         */
        this.close = new EventEmitter();
        this.class = true;
        this.dismissible = config.dismissible;
        this.type = config.type;
        this.icon = config.icon;
    }
    /**
     * @return {?}
     */
    NgtAlert.prototype.closeHandler = /**
     * @return {?}
     */
    function () {
        this.close.emit(null);
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    NgtAlert.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        /** @type {?} */
        var typeChange = changes['type'];
        if (typeChange && !typeChange.firstChange) {
            this._renderer.removeClass(this._element.nativeElement, "alert-" + typeChange.previousValue);
            this._renderer.addClass(this._element.nativeElement, "alert-" + typeChange.currentValue);
        }
    };
    /**
     * @return {?}
     */
    NgtAlert.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this._renderer.addClass(this._element.nativeElement, "alert-" + this.type);
    };
    NgtAlert.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-alert',
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    encapsulation: ViewEncapsulation.None,
                    template: "\n        <div *ngIf=\"icon\" class=\"alert-icon\">\n            <i class=\"{{ icon }}\"></i>\n        </div>\n        <div class=\"alert-text\">\n            <ng-content></ng-content>\n        </div>\n        <div *ngIf=\"dismissible\" class=\"alert-close\">\n            <button type=\"button\" class=\"close\" (click)=\"closeHandler()\">\n                <span aria-hidden=\"true\">\n                    <i class=\"ft-x\"></i>\n                </span>\n            </button>\n        </div>\n    ",
                    styles: ["ngt-alert{display:block}"]
                }] }
    ];
    /** @nocollapse */
    NgtAlert.ctorParameters = function () { return [
        { type: NgtAlertConfig },
        { type: Renderer2 },
        { type: ElementRef }
    ]; };
    NgtAlert.propDecorators = {
        dismissible: [{ type: Input }],
        icon: [{ type: Input }],
        type: [{ type: Input }],
        close: [{ type: Output }],
        class: [{ type: HostBinding, args: ['class.alert',] }]
    };
    return NgtAlert;
}());
export { NgtAlert };
if (false) {
    /**
     * A flag indicating if a given alert can be dismissed (closed) by a user. If this flag is set, a close button (in a
     * form of an ×) will be displayed.
     * @type {?}
     */
    NgtAlert.prototype.dismissible;
    /**
     * Class of icon that will be used in alert
     * @type {?}
     */
    NgtAlert.prototype.icon;
    /**
     * Alert type (CSS class). System recognizes the following bg's: "primary", "secondary", "success", "danger", "warning", "info", "light" , "dark"
     *  and other utilities bg's.
     *  And also outline version: "outline-primary", "outline-secondary", "outline-success" etc.
     * @type {?}
     */
    NgtAlert.prototype.type;
    /**
     * An event emitted when the close button is clicked. This event has no payload. Only relevant for dismissible alerts.
     * @type {?}
     */
    NgtAlert.prototype.close;
    /** @type {?} */
    NgtAlert.prototype.class;
    /**
     * @type {?}
     * @private
     */
    NgtAlert.prototype._renderer;
    /**
     * @type {?}
     * @private
     */
    NgtAlert.prototype._element;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImFsZXJ0L2FsZXJ0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0gsdUJBQXVCLEVBQ3ZCLFNBQVMsRUFDVCxVQUFVLEVBQ1YsWUFBWSxFQUNaLFdBQVcsRUFDWCxLQUFLLEVBR0wsTUFBTSxFQUNOLFNBQVMsRUFFVCxpQkFBaUIsRUFDcEIsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBS2hEO0lBNENJLGtCQUFZLE1BQXNCLEVBQVUsU0FBb0IsRUFBVSxRQUFvQjtRQUFsRCxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBWTs7OztRQUpwRixVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQVEsQ0FBQztRQUVmLFVBQUssR0FBRyxJQUFJLENBQUM7UUFHckMsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDNUIsQ0FBQzs7OztJQUVELCtCQUFZOzs7SUFBWjtRQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBRUQsOEJBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCOztZQUN4QixVQUFVLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQztRQUNsQyxJQUFJLFVBQVUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUU7WUFDdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsV0FBUyxVQUFVLENBQUMsYUFBZSxDQUFDLENBQUM7WUFDN0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsV0FBUyxVQUFVLENBQUMsWUFBYyxDQUFDLENBQUM7U0FDNUY7SUFDTCxDQUFDOzs7O0lBRUQsMkJBQVE7OztJQUFSO1FBQ0ksSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsV0FBUyxJQUFJLENBQUMsSUFBTSxDQUFDLENBQUM7SUFDL0UsQ0FBQzs7Z0JBaEVKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsV0FBVztvQkFDckIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO29CQUNyQyxRQUFRLEVBQUUscWZBY1Q7O2lCQUVKOzs7O2dCQXpCUSxjQUFjO2dCQUxuQixTQUFTO2dCQVBULFVBQVU7Ozs4QkEyQ1QsS0FBSzt1QkFJTCxLQUFLO3VCQU1MLEtBQUs7d0JBSUwsTUFBTTt3QkFFTixXQUFXLFNBQUMsYUFBYTs7SUF1QjlCLGVBQUM7Q0FBQSxBQWpFRCxJQWlFQztTQTVDWSxRQUFROzs7Ozs7O0lBS2pCLCtCQUE4Qjs7Ozs7SUFJOUIsd0JBQXNCOzs7Ozs7O0lBTXRCLHdCQUFzQjs7Ozs7SUFJdEIseUJBQTJDOztJQUUzQyx5QkFBeUM7Ozs7O0lBRUwsNkJBQTRCOzs7OztJQUFFLDRCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSxcclxuICAgIENvbXBvbmVudCxcclxuICAgIEVsZW1lbnRSZWYsXHJcbiAgICBFdmVudEVtaXR0ZXIsXHJcbiAgICBIb3N0QmluZGluZyxcclxuICAgIElucHV0LFxyXG4gICAgT25DaGFuZ2VzLFxyXG4gICAgT25Jbml0LFxyXG4gICAgT3V0cHV0LFxyXG4gICAgUmVuZGVyZXIyLFxyXG4gICAgU2ltcGxlQ2hhbmdlcyxcclxuICAgIFZpZXdFbmNhcHN1bGF0aW9uXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBOZ3RBbGVydENvbmZpZyB9IGZyb20gJy4vYWxlcnQtY29uZmlnJztcclxuXHJcbi8qKlxyXG4gKiBBbGVydHMgY2FuIGJlIHVzZWQgdG8gcHJvdmlkZSBmZWVkYmFjayBtZXNzYWdlcy5cclxuICovXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICduZ3QtYWxlcnQnLFxyXG4gICAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8ZGl2ICpuZ0lmPVwiaWNvblwiIGNsYXNzPVwiYWxlcnQtaWNvblwiPlxyXG4gICAgICAgICAgICA8aSBjbGFzcz1cInt7IGljb24gfX1cIj48L2k+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiBjbGFzcz1cImFsZXJ0LXRleHRcIj5cclxuICAgICAgICAgICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgKm5nSWY9XCJkaXNtaXNzaWJsZVwiIGNsYXNzPVwiYWxlcnQtY2xvc2VcIj5cclxuICAgICAgICAgICAgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJjbG9zZVwiIChjbGljayk9XCJjbG9zZUhhbmRsZXIoKVwiPlxyXG4gICAgICAgICAgICAgICAgPHNwYW4gYXJpYS1oaWRkZW49XCJ0cnVlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJmdC14XCI+PC9pPlxyXG4gICAgICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIGAsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9hbGVydC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndEFsZXJ0IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMge1xyXG4gICAgLyoqXHJcbiAgICAgKiBBIGZsYWcgaW5kaWNhdGluZyBpZiBhIGdpdmVuIGFsZXJ0IGNhbiBiZSBkaXNtaXNzZWQgKGNsb3NlZCkgYnkgYSB1c2VyLiBJZiB0aGlzIGZsYWcgaXMgc2V0LCBhIGNsb3NlIGJ1dHRvbiAoaW4gYVxyXG4gICAgICogZm9ybSBvZiBhbiDDlykgd2lsbCBiZSBkaXNwbGF5ZWQuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGRpc21pc3NpYmxlOiBib29sZWFuO1xyXG4gICAgLyoqXHJcbiAgICAgKiBDbGFzcyBvZiBpY29uIHRoYXQgd2lsbCBiZSB1c2VkIGluIGFsZXJ0XHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGljb246IHN0cmluZztcclxuICAgIC8qKlxyXG4gICAgICogQWxlcnQgdHlwZSAoQ1NTIGNsYXNzKS4gU3lzdGVtIHJlY29nbml6ZXMgdGhlIGZvbGxvd2luZyBiZydzOiBcInByaW1hcnlcIiwgXCJzZWNvbmRhcnlcIiwgXCJzdWNjZXNzXCIsIFwiZGFuZ2VyXCIsIFwid2FybmluZ1wiLCBcImluZm9cIiwgXCJsaWdodFwiICwgXCJkYXJrXCJcclxuICAgICAqICBhbmQgb3RoZXIgdXRpbGl0aWVzIGJnJ3MuXHJcbiAgICAgKiAgQW5kIGFsc28gb3V0bGluZSB2ZXJzaW9uOiBcIm91dGxpbmUtcHJpbWFyeVwiLCBcIm91dGxpbmUtc2Vjb25kYXJ5XCIsIFwib3V0bGluZS1zdWNjZXNzXCIgZXRjLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSB0eXBlOiBzdHJpbmc7XHJcbiAgICAvKipcclxuICAgICAqIEFuIGV2ZW50IGVtaXR0ZWQgd2hlbiB0aGUgY2xvc2UgYnV0dG9uIGlzIGNsaWNrZWQuIFRoaXMgZXZlbnQgaGFzIG5vIHBheWxvYWQuIE9ubHkgcmVsZXZhbnQgZm9yIGRpc21pc3NpYmxlIGFsZXJ0cy5cclxuICAgICAqL1xyXG4gICAgQE91dHB1dCgpIGNsb3NlID0gbmV3IEV2ZW50RW1pdHRlcjx2b2lkPigpO1xyXG5cclxuICAgIEBIb3N0QmluZGluZygnY2xhc3MuYWxlcnQnKSBjbGFzcyA9IHRydWU7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29uZmlnOiBOZ3RBbGVydENvbmZpZywgcHJpdmF0ZSBfcmVuZGVyZXI6IFJlbmRlcmVyMiwgcHJpdmF0ZSBfZWxlbWVudDogRWxlbWVudFJlZikge1xyXG4gICAgICAgIHRoaXMuZGlzbWlzc2libGUgPSBjb25maWcuZGlzbWlzc2libGU7XHJcbiAgICAgICAgdGhpcy50eXBlID0gY29uZmlnLnR5cGU7XHJcbiAgICAgICAgdGhpcy5pY29uID0gY29uZmlnLmljb247XHJcbiAgICB9XHJcblxyXG4gICAgY2xvc2VIYW5kbGVyKCkge1xyXG4gICAgICAgIHRoaXMuY2xvc2UuZW1pdChudWxsKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAgICAgY29uc3QgdHlwZUNoYW5nZSA9IGNoYW5nZXNbJ3R5cGUnXTtcclxuICAgICAgICBpZiAodHlwZUNoYW5nZSAmJiAhdHlwZUNoYW5nZS5maXJzdENoYW5nZSkge1xyXG4gICAgICAgICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLl9lbGVtZW50Lm5hdGl2ZUVsZW1lbnQsIGBhbGVydC0ke3R5cGVDaGFuZ2UucHJldmlvdXNWYWx1ZX1gKTtcclxuICAgICAgICAgICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5fZWxlbWVudC5uYXRpdmVFbGVtZW50LCBgYWxlcnQtJHt0eXBlQ2hhbmdlLmN1cnJlbnRWYWx1ZX1gKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5fZWxlbWVudC5uYXRpdmVFbGVtZW50LCBgYWxlcnQtJHt0aGlzLnR5cGV9YCk7XHJcbiAgICB9XHJcbn1cclxuIl19