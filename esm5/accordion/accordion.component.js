/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ContentChildren, EventEmitter, HostBinding, Input, Output, QueryList, } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NgtAccordionPanelDirective } from './accordion-panel.directive';
import { NgtAccordionService } from './accordion.service';
import { NgtAccordionConfig } from './accordion-config';
import { isString } from '../util/util';
/**
 * The NgtAccordion directive is a collection of panels.
 * It can assure that only one panel can be opened at a time.
 */
var NgtAccordionComponent = /** @class */ (function () {
    function NgtAccordionComponent(config) {
        /**
         *  Whether the other panels should be closed when a panel is opened
         */
        this.closeOtherPanels = true;
        /**
         * An array or comma separated strings of panel identifiers that should be opened
         */
        this.activeIds = [];
        /**
         * A panel change event fired right before the panel toggle happens. See NgtPanelChangeEvent for payload details
         */
        this.panelChange = new EventEmitter();
        this.class = 'accordion';
        this.type = config.type;
        this.bg = config.bg;
        this.closeOtherPanels = config.closeOthers;
    }
    /**
     * @return {?}
     */
    NgtAccordionComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.class = this.class + (this.type ? ' accordion-' + this.type : '');
    };
    /**
     * Checks if a panel with a given id is expanded or not.
     */
    /**
     * Checks if a panel with a given id is expanded or not.
     * @param {?} panelId
     * @return {?}
     */
    NgtAccordionComponent.prototype.isExpanded = /**
     * Checks if a panel with a given id is expanded or not.
     * @param {?} panelId
     * @return {?}
     */
    function (panelId) {
        return this.activeIds.indexOf(panelId) > -1;
    };
    /**
     * Expands a panel with a given id. Has no effect if the panel is already expanded or disabled.
     */
    /**
     * Expands a panel with a given id. Has no effect if the panel is already expanded or disabled.
     * @param {?} panelId
     * @return {?}
     */
    NgtAccordionComponent.prototype.expand = /**
     * Expands a panel with a given id. Has no effect if the panel is already expanded or disabled.
     * @param {?} panelId
     * @return {?}
     */
    function (panelId) {
        this._changeOpenState(this._findPanelById(panelId), true);
    };
    /**
     * Expands all panels if [closeOthers]="false". For the [closeOthers]="true" case will have no effect if there is an
     * open panel, otherwise the first panel will be expanded.
     */
    /**
     * Expands all panels if [closeOthers]="false". For the [closeOthers]="true" case will have no effect if there is an
     * open panel, otherwise the first panel will be expanded.
     * @return {?}
     */
    NgtAccordionComponent.prototype.expandAll = /**
     * Expands all panels if [closeOthers]="false". For the [closeOthers]="true" case will have no effect if there is an
     * open panel, otherwise the first panel will be expanded.
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.closeOtherPanels) {
            if (this.activeIds.length === 0 && this.panels.length) {
                this._changeOpenState(this.panels.first, true);
            }
        }
        else {
            this.panels.forEach((/**
             * @param {?} panel
             * @return {?}
             */
            function (panel) { return _this._changeOpenState(panel, true); }));
        }
    };
    /**
     * Collapses a panel with a given id. Has no effect if the panel is already collapsed or disabled.
     */
    /**
     * Collapses a panel with a given id. Has no effect if the panel is already collapsed or disabled.
     * @param {?} panelId
     * @return {?}
     */
    NgtAccordionComponent.prototype.collapse = /**
     * Collapses a panel with a given id. Has no effect if the panel is already collapsed or disabled.
     * @param {?} panelId
     * @return {?}
     */
    function (panelId) {
        this._changeOpenState(this._findPanelById(panelId), false);
    };
    /**
     * Collapses all open panels.
     */
    /**
     * Collapses all open panels.
     * @return {?}
     */
    NgtAccordionComponent.prototype.collapseAll = /**
     * Collapses all open panels.
     * @return {?}
     */
    function () {
        var _this = this;
        this.panels.forEach((/**
         * @param {?} panel
         * @return {?}
         */
        function (panel) {
            _this._changeOpenState(panel, false);
        }));
    };
    /**
     * Programmatically toggle a panel with a given id. Has no effect if the panel is disabled.
     */
    /**
     * Programmatically toggle a panel with a given id. Has no effect if the panel is disabled.
     * @param {?} panelId
     * @return {?}
     */
    NgtAccordionComponent.prototype.toggle = /**
     * Programmatically toggle a panel with a given id. Has no effect if the panel is disabled.
     * @param {?} panelId
     * @return {?}
     */
    function (panelId) {
        /** @type {?} */
        var panel = this._findPanelById(panelId);
        if (panel) {
            this._changeOpenState(panel, !panel.isOpen);
        }
    };
    /**
     * Toggle all panels.
     */
    /**
     * Toggle all panels.
     * @param {?=} nextState
     * @return {?}
     */
    NgtAccordionComponent.prototype.toggleAll = /**
     * Toggle all panels.
     * @param {?=} nextState
     * @return {?}
     */
    function (nextState) {
        var _this = this;
        this.panels.forEach((/**
         * @param {?} panel
         * @return {?}
         */
        function (panel) {
            _this._changeOpenState(panel, typeof nextState === 'boolean' ? nextState : !panel.isOpen);
        }));
    };
    /**
     * @return {?}
     */
    NgtAccordionComponent.prototype.ngAfterContentChecked = /**
     * @return {?}
     */
    function () {
        var _this = this;
        // active id updates
        if (isString(this.activeIds)) {
            this.activeIds = this.activeIds.split(/\s*,\s*/);
        }
        // update panels open states
        this.panels.forEach((/**
         * @param {?} panel
         * @return {?}
         */
        function (panel) { return panel.isOpen = !panel.disabled && _this.activeIds.indexOf(panel.id) > -1; }));
        // closeOthers updates
        if (this.activeIds.length > 1 && this.closeOtherPanels) {
            this._closeOthers(this.activeIds[0]);
            this._updateActiveIds();
        }
    };
    /**
     * @private
     * @param {?} panel
     * @param {?} nextState
     * @return {?}
     */
    NgtAccordionComponent.prototype._changeOpenState = /**
     * @private
     * @param {?} panel
     * @param {?} nextState
     * @return {?}
     */
    function (panel, nextState) {
        if (panel && !panel.disabled && panel.isOpen !== nextState) {
            /** @type {?} */
            var defaultPrevented_1 = false;
            this.panelChange.emit({
                panelId: panel.id,
                nextState: nextState,
                preventDefault: (/**
                 * @return {?}
                 */
                function () {
                    defaultPrevented_1 = true;
                })
            });
            if (!defaultPrevented_1) {
                panel.isOpen = nextState;
                if (nextState && this.closeOtherPanels) {
                    this._closeOthers(panel.id);
                }
                this._updateActiveIds();
            }
        }
    };
    /**
     * @private
     * @param {?} panelId
     * @return {?}
     */
    NgtAccordionComponent.prototype._closeOthers = /**
     * @private
     * @param {?} panelId
     * @return {?}
     */
    function (panelId) {
        this.panels.forEach((/**
         * @param {?} panel
         * @return {?}
         */
        function (panel) {
            if (panel.id !== panelId) {
                panel.isOpen = false;
            }
        }));
    };
    /**
     * @private
     * @param {?} panelId
     * @return {?}
     */
    NgtAccordionComponent.prototype._findPanelById = /**
     * @private
     * @param {?} panelId
     * @return {?}
     */
    function (panelId) {
        return this.panels.find((/**
         * @param {?} p
         * @return {?}
         */
        function (p) { return p.id === panelId; }));
    };
    /**
     * @private
     * @return {?}
     */
    NgtAccordionComponent.prototype._updateActiveIds = /**
     * @private
     * @return {?}
     */
    function () {
        this.activeIds = this.panels.filter((/**
         * @param {?} panel
         * @return {?}
         */
        function (panel) { return panel.isOpen && !panel.disabled; })).map((/**
         * @param {?} panel
         * @return {?}
         */
        function (panel) { return panel.id; }));
    };
    NgtAccordionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-accordion',
                    template: "\n        <ng-template #t ngtAccordionPanelHeader let-panel>\n            <div [class]=\"'accordion-panel_header ' + (panel.bg ? 'bg-'+panel.bg: bg ? 'bg-'+bg : '') + ' ' + (panel.isOpen ? 'expanded' : '')\"\n                 [ngtAccordionPanelToggle]=\"panel\">\n                <div *ngIf=\"panel.title\" class=\"accordion-panel_title\">\n                    {{panel.title}}\n                </div>\n                <div *ngIf=\"!panel.title\">\n                    <ng-template [ngTemplateOutlet]=\"panel.titleTpl?.templateRef\"></ng-template>\n                </div>\n            </div>\n        </ng-template>\n        <ng-template ngFor let-panel [ngForOf]=\"panels\">\n            <div [class]=\"'accordion-panel ' + (panel.isOpen ? 'expanded' : '')\">\n                <ng-template [ngTemplateOutlet]=\"panel.headerTpl?.templateRef || t\"\n                             [ngTemplateOutletContext]=\"{$implicit: panel, opened: panel.isOpen}\"></ng-template>\n                <div [@slide]=\"panel.isOpen ? 'down' : 'up'\"\n                     class=\"accordion-panel_body\">\n                    <div *ngIf=\"panel.contentTpl\">\n                        <ng-template [ngTemplateOutlet]=\"panel.contentTpl?.templateRef\"></ng-template>\n                    </div>\n                    <ng-content></ng-content>\n                </div>\n            </div>\n        </ng-template>\n    ",
                    animations: [
                        trigger('slide', [
                            state('down', style({ height: '*', paddingTop: '*', paddingBottom: '*' })),
                            state('up', style({ height: 0, paddingTop: 0, paddingBottom: 0 })),
                            transition('up => down', animate('350ms ease-out')),
                            transition('down => up', animate('350ms ease-out'))
                        ])
                    ],
                    providers: [NgtAccordionService]
                }] }
    ];
    /** @nocollapse */
    NgtAccordionComponent.ctorParameters = function () { return [
        { type: NgtAccordionConfig }
    ]; };
    NgtAccordionComponent.propDecorators = {
        panels: [{ type: ContentChildren, args: [NgtAccordionPanelDirective,] }],
        closeOtherPanels: [{ type: Input, args: ['closeOthers',] }],
        activeIds: [{ type: Input }],
        type: [{ type: Input }],
        bg: [{ type: Input }],
        panelChange: [{ type: Output }],
        class: [{ type: HostBinding, args: ['class',] }]
    };
    return NgtAccordionComponent;
}());
export { NgtAccordionComponent };
if (false) {
    /** @type {?} */
    NgtAccordionComponent.prototype.panels;
    /**
     *  Whether the other panels should be closed when a panel is opened
     * @type {?}
     */
    NgtAccordionComponent.prototype.closeOtherPanels;
    /**
     * An array or comma separated strings of panel identifiers that should be opened
     * @type {?}
     */
    NgtAccordionComponent.prototype.activeIds;
    /**
     *  Accordion's types of panels.
     *  System recognizes the following types: "light" and "outline"
     * @type {?}
     */
    NgtAccordionComponent.prototype.type;
    /**
     *  Accordion's bg's of panels to be applied globally.
     *  System recognizes the following bg's: "primary", "secondary", "success", "danger", "warning", "info", "light" , "dark"
     *  and other utilities bg's
     * @type {?}
     */
    NgtAccordionComponent.prototype.bg;
    /**
     * A panel change event fired right before the panel toggle happens. See NgtPanelChangeEvent for payload details
     * @type {?}
     */
    NgtAccordionComponent.prototype.panelChange;
    /** @type {?} */
    NgtAccordionComponent.prototype.class;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3JkaW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiYWNjb3JkaW9uL2FjY29yZGlvbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFFSCxTQUFTLEVBQ1QsZUFBZSxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQzFDLEtBQUssRUFBVSxNQUFNLEVBQ3JCLFNBQVMsR0FDWixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRWpGLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBRXpFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQzFELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxjQUFjLENBQUM7Ozs7O0FBTXhDO0lBd0VJLCtCQUFZLE1BQTBCOzs7O1FBM0JoQixxQkFBZ0IsR0FBRyxJQUFJLENBQUM7Ozs7UUFLckMsY0FBUyxHQUFzQixFQUFFLENBQUM7Ozs7UUFrQmpDLGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQWdDLENBQUM7UUFFbkQsVUFBSyxHQUFHLFdBQVcsQ0FBQztRQUd0QyxJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLEVBQUUsR0FBRyxNQUFNLENBQUMsRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO0lBQy9DLENBQUM7Ozs7SUFFRCx3Q0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDM0UsQ0FBQztJQUVEOztPQUVHOzs7Ozs7SUFDSCwwQ0FBVTs7Ozs7SUFBVixVQUFXLE9BQWU7UUFDdEIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQ7O09BRUc7Ozs7OztJQUNILHNDQUFNOzs7OztJQUFOLFVBQU8sT0FBZTtRQUNsQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7SUFDSCx5Q0FBUzs7Ozs7SUFBVDtRQUFBLGlCQVFDO1FBUEcsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDdkIsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUU7Z0JBQ25ELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQzthQUNsRDtTQUNKO2FBQU07WUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU87Ozs7WUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEVBQWxDLENBQWtDLEVBQUMsQ0FBQztTQUNwRTtJQUNMLENBQUM7SUFFRDs7T0FFRzs7Ozs7O0lBQ0gsd0NBQVE7Ozs7O0lBQVIsVUFBUyxPQUFlO1FBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCwyQ0FBVzs7OztJQUFYO1FBQUEsaUJBSUM7UUFIRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU87Ozs7UUFBQyxVQUFDLEtBQUs7WUFDdEIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN4QyxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7T0FFRzs7Ozs7O0lBQ0gsc0NBQU07Ozs7O0lBQU4sVUFBTyxPQUFlOztZQUNaLEtBQUssR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQztRQUMxQyxJQUFJLEtBQUssRUFBRTtZQUNQLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDL0M7SUFDTCxDQUFDO0lBRUQ7O09BRUc7Ozs7OztJQUNILHlDQUFTOzs7OztJQUFULFVBQVUsU0FBbUI7UUFBN0IsaUJBSUM7UUFIRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU87Ozs7UUFBQyxVQUFDLEtBQUs7WUFDdEIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxPQUFPLFNBQVMsS0FBSyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDN0YsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQscURBQXFCOzs7SUFBckI7UUFBQSxpQkFjQztRQWJHLG9CQUFvQjtRQUNwQixJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUNwRDtRQUVELDRCQUE0QjtRQUM1QixJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU87Ozs7UUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLEtBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBdkUsQ0FBdUUsRUFBQyxDQUFDO1FBRXRHLHNCQUFzQjtRQUN0QixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDcEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDckMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDM0I7SUFDTCxDQUFDOzs7Ozs7O0lBRU8sZ0RBQWdCOzs7Ozs7SUFBeEIsVUFBeUIsS0FBaUMsRUFBRSxTQUFrQjtRQUMxRSxJQUFJLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLE1BQU0sS0FBSyxTQUFTLEVBQUU7O2dCQUNwRCxrQkFBZ0IsR0FBRyxLQUFLO1lBRTVCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO2dCQUNsQixPQUFPLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQ2pCLFNBQVMsRUFBRSxTQUFTO2dCQUNwQixjQUFjOzs7Z0JBQUU7b0JBQ1osa0JBQWdCLEdBQUcsSUFBSSxDQUFDO2dCQUM1QixDQUFDLENBQUE7YUFDSixDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsa0JBQWdCLEVBQUU7Z0JBQ25CLEtBQUssQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDO2dCQUV6QixJQUFJLFNBQVMsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7b0JBQ3BDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUMvQjtnQkFDRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzthQUMzQjtTQUVKO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sNENBQVk7Ozs7O0lBQXBCLFVBQXFCLE9BQWU7UUFDaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQSxLQUFLO1lBQ3JCLElBQUksS0FBSyxDQUFDLEVBQUUsS0FBSyxPQUFPLEVBQUU7Z0JBQ3RCLEtBQUssQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO2FBQ3hCO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFTyw4Q0FBYzs7Ozs7SUFBdEIsVUFBdUIsT0FBZTtRQUNsQyxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSTs7OztRQUFDLFVBQUEsQ0FBQyxJQUFJLE9BQUEsQ0FBQyxDQUFDLEVBQUUsS0FBSyxPQUFPLEVBQWhCLENBQWdCLEVBQUMsQ0FBQztJQUNuRCxDQUFDOzs7OztJQUVPLGdEQUFnQjs7OztJQUF4QjtRQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBL0IsQ0FBK0IsRUFBQyxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxFQUFFLEVBQVIsQ0FBUSxFQUFDLENBQUM7SUFDekcsQ0FBQzs7Z0JBdE1KLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsZUFBZTtvQkFDekIsUUFBUSxFQUFFLCsyQ0F5QlQ7b0JBQ0QsVUFBVSxFQUFFO3dCQUNSLE9BQU8sQ0FBQyxPQUFPLEVBQUU7NEJBQ2IsS0FBSyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsRUFBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLEdBQUcsRUFBQyxDQUFDLENBQUM7NEJBQ3hFLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDOzRCQUNoRSxVQUFVLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOzRCQUNuRCxVQUFVLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO3lCQUN0RCxDQUFDO3FCQUNMO29CQUNELFNBQVMsRUFBRSxDQUFDLG1CQUFtQixDQUFDO2lCQUNuQzs7OztnQkE1Q1Esa0JBQWtCOzs7eUJBK0N0QixlQUFlLFNBQUMsMEJBQTBCO21DQUsxQyxLQUFLLFNBQUMsYUFBYTs0QkFLbkIsS0FBSzt1QkFNTCxLQUFLO3FCQU9MLEtBQUs7OEJBS0wsTUFBTTt3QkFFTixXQUFXLFNBQUMsT0FBTzs7SUFpSXhCLDRCQUFDO0NBQUEsQUF2TUQsSUF1TUM7U0FoS1kscUJBQXFCOzs7SUFDOUIsdUNBQTJGOzs7OztJQUszRixpREFBOEM7Ozs7O0lBSzlDLDBDQUEyQzs7Ozs7O0lBTTNDLHFDQUFzQjs7Ozs7OztJQU90QixtQ0FBb0I7Ozs7O0lBS3BCLDRDQUF5RTs7SUFFekUsc0NBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICAgIEFmdGVyQ29udGVudENoZWNrZWQsXHJcbiAgICBDb21wb25lbnQsXHJcbiAgICBDb250ZW50Q2hpbGRyZW4sIEV2ZW50RW1pdHRlciwgSG9zdEJpbmRpbmcsXHJcbiAgICBJbnB1dCwgT25Jbml0LCBPdXRwdXQsXHJcbiAgICBRdWVyeUxpc3QsXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IGFuaW1hdGUsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlciB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5cclxuaW1wb3J0IHsgTmd0QWNjb3JkaW9uUGFuZWxEaXJlY3RpdmUgfSBmcm9tICcuL2FjY29yZGlvbi1wYW5lbC5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBOZ3RBY2NvcmRpb25QYW5lbENoYW5nZUV2ZW50IH0gZnJvbSAnLi9hY2NvcmRpb24tcGFuZWwtY2hhbmdlLWV2ZW50JztcclxuaW1wb3J0IHsgTmd0QWNjb3JkaW9uU2VydmljZSB9IGZyb20gJy4vYWNjb3JkaW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOZ3RBY2NvcmRpb25Db25maWcgfSBmcm9tICcuL2FjY29yZGlvbi1jb25maWcnO1xyXG5pbXBvcnQgeyBpc1N0cmluZyB9IGZyb20gJy4uL3V0aWwvdXRpbCc7XHJcblxyXG4vKipcclxuICogVGhlIE5ndEFjY29yZGlvbiBkaXJlY3RpdmUgaXMgYSBjb2xsZWN0aW9uIG9mIHBhbmVscy5cclxuICogSXQgY2FuIGFzc3VyZSB0aGF0IG9ubHkgb25lIHBhbmVsIGNhbiBiZSBvcGVuZWQgYXQgYSB0aW1lLlxyXG4gKi9cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC1hY2NvcmRpb24nLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8bmctdGVtcGxhdGUgI3Qgbmd0QWNjb3JkaW9uUGFuZWxIZWFkZXIgbGV0LXBhbmVsPlxyXG4gICAgICAgICAgICA8ZGl2IFtjbGFzc109XCInYWNjb3JkaW9uLXBhbmVsX2hlYWRlciAnICsgKHBhbmVsLmJnID8gJ2JnLScrcGFuZWwuYmc6IGJnID8gJ2JnLScrYmcgOiAnJykgKyAnICcgKyAocGFuZWwuaXNPcGVuID8gJ2V4cGFuZGVkJyA6ICcnKVwiXHJcbiAgICAgICAgICAgICAgICAgW25ndEFjY29yZGlvblBhbmVsVG9nZ2xlXT1cInBhbmVsXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2ICpuZ0lmPVwicGFuZWwudGl0bGVcIiBjbGFzcz1cImFjY29yZGlvbi1wYW5lbF90aXRsZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIHt7cGFuZWwudGl0bGV9fVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2ICpuZ0lmPVwiIXBhbmVsLnRpdGxlXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPG5nLXRlbXBsYXRlIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInBhbmVsLnRpdGxlVHBsPy50ZW1wbGF0ZVJlZlwiPjwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICA8bmctdGVtcGxhdGUgbmdGb3IgbGV0LXBhbmVsIFtuZ0Zvck9mXT1cInBhbmVsc1wiPlxyXG4gICAgICAgICAgICA8ZGl2IFtjbGFzc109XCInYWNjb3JkaW9uLXBhbmVsICcgKyAocGFuZWwuaXNPcGVuID8gJ2V4cGFuZGVkJyA6ICcnKVwiPlxyXG4gICAgICAgICAgICAgICAgPG5nLXRlbXBsYXRlIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInBhbmVsLmhlYWRlclRwbD8udGVtcGxhdGVSZWYgfHwgdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRDb250ZXh0XT1cInskaW1wbGljaXQ6IHBhbmVsLCBvcGVuZWQ6IHBhbmVsLmlzT3Blbn1cIj48L25nLXRlbXBsYXRlPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBbQHNsaWRlXT1cInBhbmVsLmlzT3BlbiA/ICdkb3duJyA6ICd1cCdcIlxyXG4gICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImFjY29yZGlvbi1wYW5lbF9ib2R5XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiAqbmdJZj1cInBhbmVsLmNvbnRlbnRUcGxcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPG5nLXRlbXBsYXRlIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInBhbmVsLmNvbnRlbnRUcGw/LnRlbXBsYXRlUmVmXCI+PC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgICAgICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9uZy10ZW1wbGF0ZT5cclxuICAgIGAsXHJcbiAgICBhbmltYXRpb25zOiBbXHJcbiAgICAgICAgdHJpZ2dlcignc2xpZGUnLCBbXHJcbiAgICAgICAgICAgIHN0YXRlKCdkb3duJywgc3R5bGUoe2hlaWdodDogJyonLCBwYWRkaW5nVG9wOiAnKicsIHBhZGRpbmdCb3R0b206ICcqJ30pKSxcclxuICAgICAgICAgICAgc3RhdGUoJ3VwJywgc3R5bGUoe2hlaWdodDogMCwgcGFkZGluZ1RvcDogMCwgcGFkZGluZ0JvdHRvbTogMH0pKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbigndXAgPT4gZG93bicsIGFuaW1hdGUoJzM1MG1zIGVhc2Utb3V0JykpLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCdkb3duID0+IHVwJywgYW5pbWF0ZSgnMzUwbXMgZWFzZS1vdXQnKSlcclxuICAgICAgICBdKVxyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW05ndEFjY29yZGlvblNlcnZpY2VdXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgTmd0QWNjb3JkaW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBBZnRlckNvbnRlbnRDaGVja2VkIHtcclxuICAgIEBDb250ZW50Q2hpbGRyZW4oTmd0QWNjb3JkaW9uUGFuZWxEaXJlY3RpdmUpIHBhbmVsczogUXVlcnlMaXN0PE5ndEFjY29yZGlvblBhbmVsRGlyZWN0aXZlPjtcclxuXHJcbiAgICAvKipcclxuICAgICAqICBXaGV0aGVyIHRoZSBvdGhlciBwYW5lbHMgc2hvdWxkIGJlIGNsb3NlZCB3aGVuIGEgcGFuZWwgaXMgb3BlbmVkXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgnY2xvc2VPdGhlcnMnKSBjbG9zZU90aGVyUGFuZWxzID0gdHJ1ZTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEFuIGFycmF5IG9yIGNvbW1hIHNlcGFyYXRlZCBzdHJpbmdzIG9mIHBhbmVsIGlkZW50aWZpZXJzIHRoYXQgc2hvdWxkIGJlIG9wZW5lZFxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBhY3RpdmVJZHM6IHN0cmluZyB8IHN0cmluZ1tdID0gW107XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgQWNjb3JkaW9uJ3MgdHlwZXMgb2YgcGFuZWxzLlxyXG4gICAgICogIFN5c3RlbSByZWNvZ25pemVzIHRoZSBmb2xsb3dpbmcgdHlwZXM6IFwibGlnaHRcIiBhbmQgXCJvdXRsaW5lXCJcclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgdHlwZTogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogIEFjY29yZGlvbidzIGJnJ3Mgb2YgcGFuZWxzIHRvIGJlIGFwcGxpZWQgZ2xvYmFsbHkuXHJcbiAgICAgKiAgU3lzdGVtIHJlY29nbml6ZXMgdGhlIGZvbGxvd2luZyBiZydzOiBcInByaW1hcnlcIiwgXCJzZWNvbmRhcnlcIiwgXCJzdWNjZXNzXCIsIFwiZGFuZ2VyXCIsIFwid2FybmluZ1wiLCBcImluZm9cIiwgXCJsaWdodFwiICwgXCJkYXJrXCJcclxuICAgICAqICBhbmQgb3RoZXIgdXRpbGl0aWVzIGJnJ3NcclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgYmc6IHN0cmluZztcclxuXHJcbiAgICAvKipcclxuICAgICAqIEEgcGFuZWwgY2hhbmdlIGV2ZW50IGZpcmVkIHJpZ2h0IGJlZm9yZSB0aGUgcGFuZWwgdG9nZ2xlIGhhcHBlbnMuIFNlZSBOZ3RQYW5lbENoYW5nZUV2ZW50IGZvciBwYXlsb2FkIGRldGFpbHNcclxuICAgICAqL1xyXG4gICAgQE91dHB1dCgpIHBhbmVsQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxOZ3RBY2NvcmRpb25QYW5lbENoYW5nZUV2ZW50PigpO1xyXG5cclxuICAgIEBIb3N0QmluZGluZygnY2xhc3MnKSBjbGFzcyA9ICdhY2NvcmRpb24nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbmZpZzogTmd0QWNjb3JkaW9uQ29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy50eXBlID0gY29uZmlnLnR5cGU7XHJcbiAgICAgICAgdGhpcy5iZyA9IGNvbmZpZy5iZztcclxuICAgICAgICB0aGlzLmNsb3NlT3RoZXJQYW5lbHMgPSBjb25maWcuY2xvc2VPdGhlcnM7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jbGFzcyA9IHRoaXMuY2xhc3MgKyAodGhpcy50eXBlID8gJyBhY2NvcmRpb24tJyArIHRoaXMudHlwZSA6ICcnKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBpZiBhIHBhbmVsIHdpdGggYSBnaXZlbiBpZCBpcyBleHBhbmRlZCBvciBub3QuXHJcbiAgICAgKi9cclxuICAgIGlzRXhwYW5kZWQocGFuZWxJZDogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYWN0aXZlSWRzLmluZGV4T2YocGFuZWxJZCkgPiAtMTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEV4cGFuZHMgYSBwYW5lbCB3aXRoIGEgZ2l2ZW4gaWQuIEhhcyBubyBlZmZlY3QgaWYgdGhlIHBhbmVsIGlzIGFscmVhZHkgZXhwYW5kZWQgb3IgZGlzYWJsZWQuXHJcbiAgICAgKi9cclxuICAgIGV4cGFuZChwYW5lbElkOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9jaGFuZ2VPcGVuU3RhdGUodGhpcy5fZmluZFBhbmVsQnlJZChwYW5lbElkKSwgdHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBFeHBhbmRzIGFsbCBwYW5lbHMgaWYgW2Nsb3NlT3RoZXJzXT1cImZhbHNlXCIuIEZvciB0aGUgW2Nsb3NlT3RoZXJzXT1cInRydWVcIiBjYXNlIHdpbGwgaGF2ZSBubyBlZmZlY3QgaWYgdGhlcmUgaXMgYW5cclxuICAgICAqIG9wZW4gcGFuZWwsIG90aGVyd2lzZSB0aGUgZmlyc3QgcGFuZWwgd2lsbCBiZSBleHBhbmRlZC5cclxuICAgICAqL1xyXG4gICAgZXhwYW5kQWxsKCk6IHZvaWQge1xyXG4gICAgICAgIGlmICh0aGlzLmNsb3NlT3RoZXJQYW5lbHMpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuYWN0aXZlSWRzLmxlbmd0aCA9PT0gMCAmJiB0aGlzLnBhbmVscy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX2NoYW5nZU9wZW5TdGF0ZSh0aGlzLnBhbmVscy5maXJzdCwgdHJ1ZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnBhbmVscy5mb3JFYWNoKHBhbmVsID0+IHRoaXMuX2NoYW5nZU9wZW5TdGF0ZShwYW5lbCwgdHJ1ZSkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbGxhcHNlcyBhIHBhbmVsIHdpdGggYSBnaXZlbiBpZC4gSGFzIG5vIGVmZmVjdCBpZiB0aGUgcGFuZWwgaXMgYWxyZWFkeSBjb2xsYXBzZWQgb3IgZGlzYWJsZWQuXHJcbiAgICAgKi9cclxuICAgIGNvbGxhcHNlKHBhbmVsSWQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX2NoYW5nZU9wZW5TdGF0ZSh0aGlzLl9maW5kUGFuZWxCeUlkKHBhbmVsSWQpLCBmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb2xsYXBzZXMgYWxsIG9wZW4gcGFuZWxzLlxyXG4gICAgICovXHJcbiAgICBjb2xsYXBzZUFsbCgpIHtcclxuICAgICAgICB0aGlzLnBhbmVscy5mb3JFYWNoKChwYW5lbCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9jaGFuZ2VPcGVuU3RhdGUocGFuZWwsIGZhbHNlKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFByb2dyYW1tYXRpY2FsbHkgdG9nZ2xlIGEgcGFuZWwgd2l0aCBhIGdpdmVuIGlkLiBIYXMgbm8gZWZmZWN0IGlmIHRoZSBwYW5lbCBpcyBkaXNhYmxlZC5cclxuICAgICAqL1xyXG4gICAgdG9nZ2xlKHBhbmVsSWQ6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IHBhbmVsID0gdGhpcy5fZmluZFBhbmVsQnlJZChwYW5lbElkKTtcclxuICAgICAgICBpZiAocGFuZWwpIHtcclxuICAgICAgICAgICAgdGhpcy5fY2hhbmdlT3BlblN0YXRlKHBhbmVsLCAhcGFuZWwuaXNPcGVuKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUb2dnbGUgYWxsIHBhbmVscy5cclxuICAgICAqL1xyXG4gICAgdG9nZ2xlQWxsKG5leHRTdGF0ZT86IGJvb2xlYW4pIHtcclxuICAgICAgICB0aGlzLnBhbmVscy5mb3JFYWNoKChwYW5lbCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9jaGFuZ2VPcGVuU3RhdGUocGFuZWwsIHR5cGVvZiBuZXh0U3RhdGUgPT09ICdib29sZWFuJyA/IG5leHRTdGF0ZSA6ICFwYW5lbC5pc09wZW4pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nQWZ0ZXJDb250ZW50Q2hlY2tlZCgpIHtcclxuICAgICAgICAvLyBhY3RpdmUgaWQgdXBkYXRlc1xyXG4gICAgICAgIGlmIChpc1N0cmluZyh0aGlzLmFjdGl2ZUlkcykpIHtcclxuICAgICAgICAgICAgdGhpcy5hY3RpdmVJZHMgPSB0aGlzLmFjdGl2ZUlkcy5zcGxpdCgvXFxzKixcXHMqLyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyB1cGRhdGUgcGFuZWxzIG9wZW4gc3RhdGVzXHJcbiAgICAgICAgdGhpcy5wYW5lbHMuZm9yRWFjaChwYW5lbCA9PiBwYW5lbC5pc09wZW4gPSAhcGFuZWwuZGlzYWJsZWQgJiYgdGhpcy5hY3RpdmVJZHMuaW5kZXhPZihwYW5lbC5pZCkgPiAtMSk7XHJcblxyXG4gICAgICAgIC8vIGNsb3NlT3RoZXJzIHVwZGF0ZXNcclxuICAgICAgICBpZiAodGhpcy5hY3RpdmVJZHMubGVuZ3RoID4gMSAmJiB0aGlzLmNsb3NlT3RoZXJQYW5lbHMpIHtcclxuICAgICAgICAgICAgdGhpcy5fY2xvc2VPdGhlcnModGhpcy5hY3RpdmVJZHNbMF0pO1xyXG4gICAgICAgICAgICB0aGlzLl91cGRhdGVBY3RpdmVJZHMoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfY2hhbmdlT3BlblN0YXRlKHBhbmVsOiBOZ3RBY2NvcmRpb25QYW5lbERpcmVjdGl2ZSwgbmV4dFN0YXRlOiBib29sZWFuKSB7XHJcbiAgICAgICAgaWYgKHBhbmVsICYmICFwYW5lbC5kaXNhYmxlZCAmJiBwYW5lbC5pc09wZW4gIT09IG5leHRTdGF0ZSkge1xyXG4gICAgICAgICAgICBsZXQgZGVmYXVsdFByZXZlbnRlZCA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5wYW5lbENoYW5nZS5lbWl0KHtcclxuICAgICAgICAgICAgICAgIHBhbmVsSWQ6IHBhbmVsLmlkLFxyXG4gICAgICAgICAgICAgICAgbmV4dFN0YXRlOiBuZXh0U3RhdGUsXHJcbiAgICAgICAgICAgICAgICBwcmV2ZW50RGVmYXVsdDogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHRQcmV2ZW50ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgaWYgKCFkZWZhdWx0UHJldmVudGVkKSB7XHJcbiAgICAgICAgICAgICAgICBwYW5lbC5pc09wZW4gPSBuZXh0U3RhdGU7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKG5leHRTdGF0ZSAmJiB0aGlzLmNsb3NlT3RoZXJQYW5lbHMpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9jbG9zZU90aGVycyhwYW5lbC5pZCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLl91cGRhdGVBY3RpdmVJZHMoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfY2xvc2VPdGhlcnMocGFuZWxJZDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5wYW5lbHMuZm9yRWFjaChwYW5lbCA9PiB7XHJcbiAgICAgICAgICAgIGlmIChwYW5lbC5pZCAhPT0gcGFuZWxJZCkge1xyXG4gICAgICAgICAgICAgICAgcGFuZWwuaXNPcGVuID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9maW5kUGFuZWxCeUlkKHBhbmVsSWQ6IHN0cmluZyk6IE5ndEFjY29yZGlvblBhbmVsRGlyZWN0aXZlIHwgbnVsbCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucGFuZWxzLmZpbmQocCA9PiBwLmlkID09PSBwYW5lbElkKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF91cGRhdGVBY3RpdmVJZHMoKSB7XHJcbiAgICAgICAgdGhpcy5hY3RpdmVJZHMgPSB0aGlzLnBhbmVscy5maWx0ZXIocGFuZWwgPT4gcGFuZWwuaXNPcGVuICYmICFwYW5lbC5kaXNhYmxlZCkubWFwKHBhbmVsID0+IHBhbmVsLmlkKTtcclxuICAgIH1cclxufVxyXG4iXX0=