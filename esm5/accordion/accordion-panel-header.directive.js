/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, TemplateRef } from '@angular/core';
/**
 * A directive to wrap an accordion panel header to contain any HTML markup and a toggling button with `NgtAccordionPanelToggleDirective`
 */
var NgtAccordionPanelHeaderDirective = /** @class */ (function () {
    function NgtAccordionPanelHeaderDirective(templateRef) {
        this.templateRef = templateRef;
    }
    NgtAccordionPanelHeaderDirective.decorators = [
        { type: Directive, args: [{
                    selector: 'ng-template[ngtAccordionPanelHeader]'
                },] }
    ];
    /** @nocollapse */
    NgtAccordionPanelHeaderDirective.ctorParameters = function () { return [
        { type: TemplateRef }
    ]; };
    return NgtAccordionPanelHeaderDirective;
}());
export { NgtAccordionPanelHeaderDirective };
if (false) {
    /** @type {?} */
    NgtAccordionPanelHeaderDirective.prototype.templateRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3JkaW9uLXBhbmVsLWhlYWRlci5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImFjY29yZGlvbi9hY2NvcmRpb24tcGFuZWwtaGVhZGVyLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7QUFLdkQ7SUFLSSwwQ0FBbUIsV0FBNkI7UUFBN0IsZ0JBQVcsR0FBWCxXQUFXLENBQWtCO0lBQ2hELENBQUM7O2dCQU5KLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsc0NBQXNDO2lCQUNuRDs7OztnQkFQbUIsV0FBVzs7SUFZL0IsdUNBQUM7Q0FBQSxBQVBELElBT0M7U0FIWSxnQ0FBZ0M7OztJQUM3Qix1REFBb0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIFRlbXBsYXRlUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vKipcclxuICogQSBkaXJlY3RpdmUgdG8gd3JhcCBhbiBhY2NvcmRpb24gcGFuZWwgaGVhZGVyIHRvIGNvbnRhaW4gYW55IEhUTUwgbWFya3VwIGFuZCBhIHRvZ2dsaW5nIGJ1dHRvbiB3aXRoIGBOZ3RBY2NvcmRpb25QYW5lbFRvZ2dsZURpcmVjdGl2ZWBcclxuICovXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICduZy10ZW1wbGF0ZVtuZ3RBY2NvcmRpb25QYW5lbEhlYWRlcl0nXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgTmd0QWNjb3JkaW9uUGFuZWxIZWFkZXJEaXJlY3RpdmUge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHRlbXBsYXRlUmVmOiBUZW1wbGF0ZVJlZjxhbnk+KSB7XHJcbiAgICB9XHJcbn1cclxuIl19