/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * The payload of the change event fired right before toggling an accordion panel
 * @record
 */
export function NgtAccordionPanelChangeEvent() { }
if (false) {
    /**
     * Id of the accordion panel that is toggled
     * @type {?}
     */
    NgtAccordionPanelChangeEvent.prototype.panelId;
    /**
     * Whether the panel will be opened (true) or closed (false)
     * @type {?}
     */
    NgtAccordionPanelChangeEvent.prototype.nextState;
    /**
     * Function that will prevent panel toggling if called
     * @type {?}
     */
    NgtAccordionPanelChangeEvent.prototype.preventDefault;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3JkaW9uLXBhbmVsLWNoYW5nZS1ldmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiYWNjb3JkaW9uL2FjY29yZGlvbi1wYW5lbC1jaGFuZ2UtZXZlbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFHQSxrREFlQzs7Ozs7O0lBWEcsK0NBQWdCOzs7OztJQUtoQixpREFBbUI7Ozs7O0lBS25CLHNEQUEyQiIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxyXG4gKiBUaGUgcGF5bG9hZCBvZiB0aGUgY2hhbmdlIGV2ZW50IGZpcmVkIHJpZ2h0IGJlZm9yZSB0b2dnbGluZyBhbiBhY2NvcmRpb24gcGFuZWxcclxuICovXHJcbmV4cG9ydCBpbnRlcmZhY2UgTmd0QWNjb3JkaW9uUGFuZWxDaGFuZ2VFdmVudCB7XHJcbiAgICAvKipcclxuICAgICAqIElkIG9mIHRoZSBhY2NvcmRpb24gcGFuZWwgdGhhdCBpcyB0b2dnbGVkXHJcbiAgICAgKi9cclxuICAgIHBhbmVsSWQ6IHN0cmluZztcclxuXHJcbiAgICAvKipcclxuICAgICAqIFdoZXRoZXIgdGhlIHBhbmVsIHdpbGwgYmUgb3BlbmVkICh0cnVlKSBvciBjbG9zZWQgKGZhbHNlKVxyXG4gICAgICovXHJcbiAgICBuZXh0U3RhdGU6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBGdW5jdGlvbiB0aGF0IHdpbGwgcHJldmVudCBwYW5lbCB0b2dnbGluZyBpZiBjYWxsZWRcclxuICAgICAqL1xyXG4gICAgcHJldmVudERlZmF1bHQ6ICgpID0+IHZvaWQ7XHJcbn1cclxuIl19