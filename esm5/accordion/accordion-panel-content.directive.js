/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, TemplateRef } from '@angular/core';
/**
 * This directive must be used to wrap accordion panel content.
 */
var NgtAccordionPanelContentDirective = /** @class */ (function () {
    function NgtAccordionPanelContentDirective(templateRef) {
        this.templateRef = templateRef;
    }
    NgtAccordionPanelContentDirective.decorators = [
        { type: Directive, args: [{
                    selector: 'ng-template[ngtAccordionPanelContent]'
                },] }
    ];
    /** @nocollapse */
    NgtAccordionPanelContentDirective.ctorParameters = function () { return [
        { type: TemplateRef }
    ]; };
    return NgtAccordionPanelContentDirective;
}());
export { NgtAccordionPanelContentDirective };
if (false) {
    /** @type {?} */
    NgtAccordionPanelContentDirective.prototype.templateRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3JkaW9uLXBhbmVsLWNvbnRlbnQuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJhY2NvcmRpb24vYWNjb3JkaW9uLXBhbmVsLWNvbnRlbnQuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7OztBQUt2RDtJQUtJLDJDQUFtQixXQUE2QjtRQUE3QixnQkFBVyxHQUFYLFdBQVcsQ0FBa0I7SUFDaEQsQ0FBQzs7Z0JBTkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSx1Q0FBdUM7aUJBQ3BEOzs7O2dCQVBtQixXQUFXOztJQVkvQix3Q0FBQztDQUFBLEFBUEQsSUFPQztTQUhZLGlDQUFpQzs7O0lBQzlCLHdEQUFvQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgVGVtcGxhdGVSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbi8qKlxyXG4gKiBUaGlzIGRpcmVjdGl2ZSBtdXN0IGJlIHVzZWQgdG8gd3JhcCBhY2NvcmRpb24gcGFuZWwgY29udGVudC5cclxuICovXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICduZy10ZW1wbGF0ZVtuZ3RBY2NvcmRpb25QYW5lbENvbnRlbnRdJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIE5ndEFjY29yZGlvblBhbmVsQ29udGVudERpcmVjdGl2ZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdGVtcGxhdGVSZWY6IFRlbXBsYXRlUmVmPGFueT4pIHtcclxuICAgIH1cclxufVxyXG4iXX0=