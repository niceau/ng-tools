/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, TemplateRef } from '@angular/core';
/**
 * This directive should be used to wrap accordion panel titles that need to contain HTML markup or other directives.
 */
var NgtAccordionPanelTitleDirective = /** @class */ (function () {
    function NgtAccordionPanelTitleDirective(templateRef) {
        this.templateRef = templateRef;
    }
    NgtAccordionPanelTitleDirective.decorators = [
        { type: Directive, args: [{
                    selector: 'ng-template[ngtAccordionPanelTitle]'
                },] }
    ];
    /** @nocollapse */
    NgtAccordionPanelTitleDirective.ctorParameters = function () { return [
        { type: TemplateRef }
    ]; };
    return NgtAccordionPanelTitleDirective;
}());
export { NgtAccordionPanelTitleDirective };
if (false) {
    /** @type {?} */
    NgtAccordionPanelTitleDirective.prototype.templateRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3JkaW9uLXBhbmVsLXRpdGxlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiYWNjb3JkaW9uL2FjY29yZGlvbi1wYW5lbC10aXRsZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7O0FBS3ZEO0lBS0kseUNBQW1CLFdBQTZCO1FBQTdCLGdCQUFXLEdBQVgsV0FBVyxDQUFrQjtJQUNoRCxDQUFDOztnQkFOSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLHFDQUFxQztpQkFDbEQ7Ozs7Z0JBUG1CLFdBQVc7O0lBWS9CLHNDQUFDO0NBQUEsQUFQRCxJQU9DO1NBSFksK0JBQStCOzs7SUFDNUIsc0RBQW9DIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBUZW1wbGF0ZVJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuLyoqXHJcbiAqIFRoaXMgZGlyZWN0aXZlIHNob3VsZCBiZSB1c2VkIHRvIHdyYXAgYWNjb3JkaW9uIHBhbmVsIHRpdGxlcyB0aGF0IG5lZWQgdG8gY29udGFpbiBIVE1MIG1hcmt1cCBvciBvdGhlciBkaXJlY3RpdmVzLlxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ25nLXRlbXBsYXRlW25ndEFjY29yZGlvblBhbmVsVGl0bGVdJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIE5ndEFjY29yZGlvblBhbmVsVGl0bGVEaXJlY3RpdmUge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHRlbXBsYXRlUmVmOiBUZW1wbGF0ZVJlZjxhbnk+KSB7XHJcbiAgICB9XHJcbn1cclxuIl19