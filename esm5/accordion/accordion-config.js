/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtAccordion component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the accordions used in the application.
 */
var NgtAccordionConfig = /** @class */ (function () {
    function NgtAccordionConfig() {
        this.closeOthers = true;
    }
    NgtAccordionConfig.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */ NgtAccordionConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtAccordionConfig_Factory() { return new NgtAccordionConfig(); }, token: NgtAccordionConfig, providedIn: "root" });
    return NgtAccordionConfig;
}());
export { NgtAccordionConfig };
if (false) {
    /** @type {?} */
    NgtAccordionConfig.prototype.closeOthers;
    /** @type {?} */
    NgtAccordionConfig.prototype.type;
    /** @type {?} */
    NgtAccordionConfig.prototype.bg;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3JkaW9uLWNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiYWNjb3JkaW9uL2FjY29yZGlvbi1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFPM0M7SUFBQTtRQUVJLGdCQUFXLEdBQUcsSUFBSSxDQUFDO0tBR3RCOztnQkFMQSxVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7NkJBUGhDO0NBWUMsQUFMRCxJQUtDO1NBSlksa0JBQWtCOzs7SUFDM0IseUNBQW1COztJQUNuQixrQ0FBYTs7SUFDYixnQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbi8qKlxyXG4gKiBDb25maWd1cmF0aW9uIHNlcnZpY2UgZm9yIHRoZSBOZ3RBY2NvcmRpb24gY29tcG9uZW50LlxyXG4gKiBZb3UgY2FuIGluamVjdCB0aGlzIHNlcnZpY2UsIHR5cGljYWxseSBpbiB5b3VyIHJvb3QgY29tcG9uZW50LCBhbmQgY3VzdG9taXplIHRoZSB2YWx1ZXMgb2YgaXRzIHByb3BlcnRpZXMgaW5cclxuICogb3JkZXIgdG8gcHJvdmlkZSBkZWZhdWx0IHZhbHVlcyBmb3IgYWxsIHRoZSBhY2NvcmRpb25zIHVzZWQgaW4gdGhlIGFwcGxpY2F0aW9uLlxyXG4gKi9cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RBY2NvcmRpb25Db25maWcge1xyXG4gICAgY2xvc2VPdGhlcnMgPSB0cnVlO1xyXG4gICAgdHlwZTogc3RyaW5nO1xyXG4gICAgYmc6IHN0cmluZztcclxufVxyXG4iXX0=