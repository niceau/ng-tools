/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, forwardRef, Host, HostListener, Inject, Input, Optional } from '@angular/core';
import { NgtAccordionPanelDirective } from './accordion-panel.directive';
import { NgtAccordionComponent } from './accordion.component';
/**
 * A directive to put on a button that toggles panel opening and closing.
 * To be used inside the `NgtAccordionPanelHeaderDirective`
 */
var NgtAccordionPanelToggleDirective = /** @class */ (function () {
    function NgtAccordionPanelToggleDirective(accordion, panel) {
        this.accordion = accordion;
        this.panel = panel;
    }
    Object.defineProperty(NgtAccordionPanelToggleDirective.prototype, "ngtAccordionPanelToggle", {
        set: /**
         * @param {?} panel
         * @return {?}
         */
        function (panel) {
            if (panel) {
                this.panel = panel;
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    NgtAccordionPanelToggleDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this.accordion.toggle(this.panel.id);
    };
    NgtAccordionPanelToggleDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtAccordionPanelToggle]',
                    host: {
                        '[class.disabled]': 'panel.disabled',
                        '[class.collapsed]': '!panel.isOpen',
                        '[attr.aria-expanded]': 'panel.isOpen',
                        '[attr.aria-controls]': 'panel.id',
                    }
                },] }
    ];
    /** @nocollapse */
    NgtAccordionPanelToggleDirective.ctorParameters = function () { return [
        { type: NgtAccordionComponent, decorators: [{ type: Inject, args: [forwardRef((/**
                         * @return {?}
                         */
                        function () { return NgtAccordionComponent; })),] }] },
        { type: NgtAccordionPanelDirective, decorators: [{ type: Optional }, { type: Host }, { type: Inject, args: [forwardRef((/**
                         * @return {?}
                         */
                        function () { return NgtAccordionPanelDirective; })),] }] }
    ]; };
    NgtAccordionPanelToggleDirective.propDecorators = {
        ngtAccordionPanelToggle: [{ type: Input }],
        onClick: [{ type: HostListener, args: ['click',] }]
    };
    return NgtAccordionPanelToggleDirective;
}());
export { NgtAccordionPanelToggleDirective };
if (false) {
    /** @type {?} */
    NgtAccordionPanelToggleDirective.prototype.accordion;
    /** @type {?} */
    NgtAccordionPanelToggleDirective.prototype.panel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3JkaW9uLXBhbmVsLXRvZ2dsZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImFjY29yZGlvbi9hY2NvcmRpb24tcGFuZWwtdG9nZ2xlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRyxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUN6RSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQzs7Ozs7QUFNOUQ7SUFxQkksMENBQzRELFNBQWdDLEVBQ1AsS0FBaUM7UUFEMUQsY0FBUyxHQUFULFNBQVMsQ0FBdUI7UUFDUCxVQUFLLEdBQUwsS0FBSyxDQUE0QjtJQUFHLENBQUM7SUFiMUgsc0JBQ0kscUVBQXVCOzs7OztRQUQzQixVQUM0QixLQUFpQztZQUN6RCxJQUFJLEtBQUssRUFBRTtnQkFDUCxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQzthQUN0QjtRQUNMLENBQUM7OztPQUFBOzs7O0lBRUQsa0RBQU87OztJQURQO1FBRUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN6QyxDQUFDOztnQkFuQkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSwyQkFBMkI7b0JBQ3JDLElBQUksRUFBRTt3QkFDRixrQkFBa0IsRUFBRSxnQkFBZ0I7d0JBQ3BDLG1CQUFtQixFQUFFLGVBQWU7d0JBQ3BDLHNCQUFzQixFQUFFLGNBQWM7d0JBQ3RDLHNCQUFzQixFQUFFLFVBQVU7cUJBQ3JDO2lCQUNKOzs7O2dCQWRRLHFCQUFxQix1QkE0QnJCLE1BQU0sU0FBQyxVQUFVOzs7d0JBQUMsY0FBTSxPQUFBLHFCQUFxQixFQUFyQixDQUFxQixFQUFDO2dCQTdCOUMsMEJBQTBCLHVCQThCMUIsUUFBUSxZQUFJLElBQUksWUFBSSxNQUFNLFNBQUMsVUFBVTs7O3dCQUFDLGNBQU0sT0FBQSwwQkFBMEIsRUFBMUIsQ0FBMEIsRUFBQzs7OzBDQWIzRSxLQUFLOzBCQU1MLFlBQVksU0FBQyxPQUFPOztJQVF6Qix1Q0FBQztDQUFBLEFBeEJELElBd0JDO1NBZlksZ0NBQWdDOzs7SUFhckMscURBQXdGOztJQUN4RixpREFBa0giLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIGZvcndhcmRSZWYsIEhvc3QsIEhvc3RMaXN0ZW5lciwgSW5qZWN0LCBJbnB1dCwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmd0QWNjb3JkaW9uUGFuZWxEaXJlY3RpdmUgfSBmcm9tICcuL2FjY29yZGlvbi1wYW5lbC5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBOZ3RBY2NvcmRpb25Db21wb25lbnQgfSBmcm9tICcuL2FjY29yZGlvbi5jb21wb25lbnQnO1xyXG5cclxuLyoqXHJcbiAqIEEgZGlyZWN0aXZlIHRvIHB1dCBvbiBhIGJ1dHRvbiB0aGF0IHRvZ2dsZXMgcGFuZWwgb3BlbmluZyBhbmQgY2xvc2luZy5cclxuICogVG8gYmUgdXNlZCBpbnNpZGUgdGhlIGBOZ3RBY2NvcmRpb25QYW5lbEhlYWRlckRpcmVjdGl2ZWBcclxuICovXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbbmd0QWNjb3JkaW9uUGFuZWxUb2dnbGVdJyxcclxuICAgIGhvc3Q6IHtcclxuICAgICAgICAnW2NsYXNzLmRpc2FibGVkXSc6ICdwYW5lbC5kaXNhYmxlZCcsXHJcbiAgICAgICAgJ1tjbGFzcy5jb2xsYXBzZWRdJzogJyFwYW5lbC5pc09wZW4nLFxyXG4gICAgICAgICdbYXR0ci5hcmlhLWV4cGFuZGVkXSc6ICdwYW5lbC5pc09wZW4nLFxyXG4gICAgICAgICdbYXR0ci5hcmlhLWNvbnRyb2xzXSc6ICdwYW5lbC5pZCcsXHJcbiAgICB9XHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RBY2NvcmRpb25QYW5lbFRvZ2dsZURpcmVjdGl2ZSB7XHJcbiAgICBASW5wdXQoKVxyXG4gICAgc2V0IG5ndEFjY29yZGlvblBhbmVsVG9nZ2xlKHBhbmVsOiBOZ3RBY2NvcmRpb25QYW5lbERpcmVjdGl2ZSkge1xyXG4gICAgICAgIGlmIChwYW5lbCkge1xyXG4gICAgICAgICAgICB0aGlzLnBhbmVsID0gcGFuZWw7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snKVxyXG4gICAgb25DbGljaygpIHtcclxuICAgICAgICB0aGlzLmFjY29yZGlvbi50b2dnbGUodGhpcy5wYW5lbC5pZCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgQEluamVjdChmb3J3YXJkUmVmKCgpID0+IE5ndEFjY29yZGlvbkNvbXBvbmVudCkpIHB1YmxpYyBhY2NvcmRpb246IE5ndEFjY29yZGlvbkNvbXBvbmVudCxcclxuICAgICAgICBAT3B0aW9uYWwoKSBASG9zdCgpIEBJbmplY3QoZm9yd2FyZFJlZigoKSA9PiBOZ3RBY2NvcmRpb25QYW5lbERpcmVjdGl2ZSkpIHB1YmxpYyBwYW5lbDogTmd0QWNjb3JkaW9uUGFuZWxEaXJlY3RpdmUpIHt9XHJcbn1cclxuIl19