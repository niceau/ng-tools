/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtPanelService } from './panel.service';
var NgtPanelOpenDirective = /** @class */ (function () {
    function NgtPanelOpenDirective(_panelService) {
        this._panelService = _panelService;
    }
    /**
     * @param {?} e
     * @return {?}
     */
    NgtPanelOpenDirective.prototype.onClick = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        e.stopPropagation();
        this._panelService.openPanel(this.id);
    };
    NgtPanelOpenDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtPanelOpen]'
                },] }
    ];
    /** @nocollapse */
    NgtPanelOpenDirective.ctorParameters = function () { return [
        { type: NgtPanelService }
    ]; };
    NgtPanelOpenDirective.propDecorators = {
        id: [{ type: Input, args: ['ngtPanelOpen',] }],
        onClick: [{ type: HostListener, args: ['click', ['$event'],] }]
    };
    return NgtPanelOpenDirective;
}());
export { NgtPanelOpenDirective };
if (false) {
    /** @type {?} */
    NgtPanelOpenDirective.prototype.id;
    /**
     * @type {?}
     * @private
     */
    NgtPanelOpenDirective.prototype._panelService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtb3Blbi5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInBhbmVscy9wYW5lbC1vcGVuLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9ELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUVsRDtJQU1JLCtCQUFvQixhQUE4QjtRQUE5QixrQkFBYSxHQUFiLGFBQWEsQ0FBaUI7SUFDbEQsQ0FBQzs7Ozs7SUFHRCx1Q0FBTzs7OztJQURQLFVBQ1EsQ0FBQztRQUNMLENBQUMsQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUNwQixJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Z0JBYkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxnQkFBZ0I7aUJBQzdCOzs7O2dCQUpRLGVBQWU7OztxQkFNbkIsS0FBSyxTQUFDLGNBQWM7MEJBS3BCLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7O0lBS3JDLDRCQUFDO0NBQUEsQUFkRCxJQWNDO1NBWFkscUJBQXFCOzs7SUFDOUIsbUNBQWtDOzs7OztJQUV0Qiw4Q0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IE5ndFBhbmVsU2VydmljZSB9IGZyb20gJy4vcGFuZWwuc2VydmljZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndFBhbmVsT3Blbl0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYW5lbE9wZW5EaXJlY3RpdmUge1xyXG4gICAgQElucHV0KCduZ3RQYW5lbE9wZW4nKSBpZDogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX3BhbmVsU2VydmljZTogTmd0UGFuZWxTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snLCBbJyRldmVudCddKVxyXG4gICAgb25DbGljayhlKSB7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB0aGlzLl9wYW5lbFNlcnZpY2Uub3BlblBhbmVsKHRoaXMuaWQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==