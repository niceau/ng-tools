/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtPanelService } from './panel.service';
var NgtPanelCloseDirective = /** @class */ (function () {
    function NgtPanelCloseDirective(_panelService) {
        this._panelService = _panelService;
    }
    /**
     * @param {?} e
     * @return {?}
     */
    NgtPanelCloseDirective.prototype.onClick = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        e.stopPropagation();
        this._panelService.closePanel(this.id);
    };
    NgtPanelCloseDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtPanelClose]'
                },] }
    ];
    /** @nocollapse */
    NgtPanelCloseDirective.ctorParameters = function () { return [
        { type: NgtPanelService }
    ]; };
    NgtPanelCloseDirective.propDecorators = {
        id: [{ type: Input, args: ['ngtPanelClose',] }],
        onClick: [{ type: HostListener, args: ['click', ['$event'],] }]
    };
    return NgtPanelCloseDirective;
}());
export { NgtPanelCloseDirective };
if (false) {
    /** @type {?} */
    NgtPanelCloseDirective.prototype.id;
    /**
     * @type {?}
     * @private
     */
    NgtPanelCloseDirective.prototype._panelService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtY2xvc2UuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJwYW5lbHMvcGFuZWwtY2xvc2UuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRWxEO0lBTUksZ0NBQW9CLGFBQThCO1FBQTlCLGtCQUFhLEdBQWIsYUFBYSxDQUFpQjtJQUNsRCxDQUFDOzs7OztJQUdELHdDQUFPOzs7O0lBRFAsVUFDUSxDQUFDO1FBQ0wsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUMzQyxDQUFDOztnQkFiSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGlCQUFpQjtpQkFDOUI7Ozs7Z0JBSlEsZUFBZTs7O3FCQU1uQixLQUFLLFNBQUMsZUFBZTswQkFLckIsWUFBWSxTQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQzs7SUFLckMsNkJBQUM7Q0FBQSxBQWRELElBY0M7U0FYWSxzQkFBc0I7OztJQUMvQixvQ0FBbUM7Ozs7O0lBRXZCLCtDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTmd0UGFuZWxTZXJ2aWNlIH0gZnJvbSAnLi9wYW5lbC5zZXJ2aWNlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbbmd0UGFuZWxDbG9zZV0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYW5lbENsb3NlRGlyZWN0aXZlIHtcclxuICAgIEBJbnB1dCgnbmd0UGFuZWxDbG9zZScpIGlkOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfcGFuZWxTZXJ2aWNlOiBOZ3RQYW5lbFNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdjbGljaycsIFsnJGV2ZW50J10pXHJcbiAgICBvbkNsaWNrKGUpIHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIHRoaXMuX3BhbmVsU2VydmljZS5jbG9zZVBhbmVsKHRoaXMuaWQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==