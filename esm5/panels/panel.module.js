/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtPanelComponent } from './panel.component';
import { NgtPanelsComponent } from './panels.component';
import { NgtPanelOpenDirective } from './panel-open.directive';
import { NgtPanelCloseDirective } from './panel-close.directive';
export { NgtPanelService } from './panel.service';
export { NgtPanelComponent } from './panel.component';
export { NgtPanelsComponent } from './panels.component';
export { NgtPanelOpenDirective } from './panel-open.directive';
export { NgtPanelCloseDirective } from './panel-close.directive';
export { NgtPanel } from './panel.model';
export { NGC_PANEL_CONFIG, DEFAULT_NGC_PANEL_CONFIG } from './panel.config';
/** @type {?} */
var NGC_PANEL_DIRECTIVES = [
    NgtPanelComponent, NgtPanelsComponent, NgtPanelOpenDirective, NgtPanelCloseDirective
];
var NgtPanelModule = /** @class */ (function () {
    function NgtPanelModule() {
    }
    /**
     * @return {?}
     */
    NgtPanelModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtPanelModule };
    };
    NgtPanelModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [NGC_PANEL_DIRECTIVES],
                    exports: [NGC_PANEL_DIRECTIVES],
                    imports: [CommonModule]
                },] }
    ];
    return NgtPanelModule;
}());
export { NgtPanelModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJwYW5lbHMvcGFuZWwubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQXVCLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFHL0MsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDdEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDeEQsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDL0QsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFakUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLHdCQUF3QixFQUEyQixNQUFNLGdCQUFnQixDQUFDOztJQUUvRixvQkFBb0IsR0FBRztJQUN6QixpQkFBaUIsRUFBRSxrQkFBa0IsRUFBRSxxQkFBcUIsRUFBRSxzQkFBc0I7Q0FDdkY7QUFFRDtJQUFBO0lBU0EsQ0FBQzs7OztJQUhVLHNCQUFPOzs7SUFBZDtRQUNJLE9BQU8sRUFBQyxRQUFRLEVBQUUsY0FBYyxFQUFDLENBQUM7SUFDdEMsQ0FBQzs7Z0JBUkosUUFBUSxTQUFDO29CQUNOLFlBQVksRUFBRSxDQUFDLG9CQUFvQixDQUFDO29CQUNwQyxPQUFPLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztvQkFDL0IsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO2lCQUMxQjs7SUFLRCxxQkFBQztDQUFBLEFBVEQsSUFTQztTQUpZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgTmd0UGFuZWxTZXJ2aWNlIH0gZnJvbSAnLi9wYW5lbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTmd0UGFuZWxDb21wb25lbnQgfSBmcm9tICcuL3BhbmVsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE5ndFBhbmVsc0NvbXBvbmVudCB9IGZyb20gJy4vcGFuZWxzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE5ndFBhbmVsT3BlbkRpcmVjdGl2ZSB9IGZyb20gJy4vcGFuZWwtb3Blbi5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBOZ3RQYW5lbENsb3NlRGlyZWN0aXZlIH0gZnJvbSAnLi9wYW5lbC1jbG9zZS5kaXJlY3RpdmUnO1xyXG5cclxuZXhwb3J0IHsgTmd0UGFuZWxTZXJ2aWNlIH0gZnJvbSAnLi9wYW5lbC5zZXJ2aWNlJztcclxuZXhwb3J0IHsgTmd0UGFuZWxDb21wb25lbnQgfSBmcm9tICcuL3BhbmVsLmNvbXBvbmVudCc7XHJcbmV4cG9ydCB7IE5ndFBhbmVsc0NvbXBvbmVudCB9IGZyb20gJy4vcGFuZWxzLmNvbXBvbmVudCc7XHJcbmV4cG9ydCB7IE5ndFBhbmVsT3BlbkRpcmVjdGl2ZSB9IGZyb20gJy4vcGFuZWwtb3Blbi5kaXJlY3RpdmUnO1xyXG5leHBvcnQgeyBOZ3RQYW5lbENsb3NlRGlyZWN0aXZlIH0gZnJvbSAnLi9wYW5lbC1jbG9zZS5kaXJlY3RpdmUnO1xyXG5leHBvcnQgeyBOZ3RQYW5lbCB9IGZyb20gJy4vcGFuZWwubW9kZWwnO1xyXG5leHBvcnQgeyBOR0NfUEFORUxfQ09ORklHLCBERUZBVUxUX05HQ19QQU5FTF9DT05GSUcsIE5ndFBhbmVsQ29uZmlnSW50ZXJmYWNlIH0gZnJvbSAnLi9wYW5lbC5jb25maWcnO1xyXG5cclxuY29uc3QgTkdDX1BBTkVMX0RJUkVDVElWRVMgPSBbXHJcbiAgICBOZ3RQYW5lbENvbXBvbmVudCwgTmd0UGFuZWxzQ29tcG9uZW50LCBOZ3RQYW5lbE9wZW5EaXJlY3RpdmUsIE5ndFBhbmVsQ2xvc2VEaXJlY3RpdmVcclxuXTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtOR0NfUEFORUxfRElSRUNUSVZFU10sXHJcbiAgICBleHBvcnRzOiBbTkdDX1BBTkVMX0RJUkVDVElWRVNdLFxyXG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndFBhbmVsTW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgICAgIHJldHVybiB7bmdNb2R1bGU6IE5ndFBhbmVsTW9kdWxlfTtcclxuICAgIH1cclxufVxyXG4iXX0=