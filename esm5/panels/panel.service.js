/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import * as i0 from "@angular/core";
var NgtPanelService = /** @class */ (function () {
    function NgtPanelService() {
        var _this = this;
        this.panels = [];
        this.activePanels = [];
        this.panelWillOpened = new Subject();
        this.panelWillClosed = new Subject();
        this.panelClosingDidStart = new Subject();
        this.panelClosingDidDone = new Subject();
        this.panelOpeningDidStart = new Subject();
        this.panelOpeningDidDone = new Subject();
        this.isPanelsChanged = new Subject();
        this.isPanelHide = false;
        this.isPanelExpand = true;
        this.panelState = 'expanded';
        this.stateEvents = {
            left: {
                expand: new Subject(),
                hide: new Subject()
            },
            right: {
                expand: new Subject(),
                hide: new Subject()
            }
        };
        // Subscribe state events and update state
        this.stateEvents.left.expand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            _this.isPanelExpand = status;
            _this.updateState();
        }));
        this.stateEvents.left.hide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            _this.isPanelHide = status;
            _this.updateState();
        }));
        this.stateEvents.right.expand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            _this.isPanelExpand = status;
            _this.updateState();
        }));
        this.stateEvents.right.hide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            _this.isPanelHide = status;
            _this.updateState();
        }));
    }
    /**
     * @param {?} panel
     * @return {?}
     */
    NgtPanelService.prototype.add = /**
     * @param {?} panel
     * @return {?}
     */
    function (panel) {
        // add panel to array of active panels-page
        this.panels.push(panel);
        this.isPanelsChanged.next(this.panels.slice());
    };
    /**
     * @param {?} id
     * @return {?}
     */
    NgtPanelService.prototype.remove = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        // remove panel from array of active panels-page
        /** @type {?} */
        var panelToRemove = _.find(this.panels, { id: id });
        this.panels = _.without(this.panels, panelToRemove);
        this.isPanelsChanged.next(this.panels.slice());
    };
    /**
     * @param {?} id
     * @return {?}
     */
    NgtPanelService.prototype.addToActive = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        this.activePanels.push(id);
    };
    /**
     * @param {?} id
     * @return {?}
     */
    NgtPanelService.prototype.removeFromActive = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        /** @type {?} */
        var index = this.activePanels.indexOf(id);
        if (index !== -1) {
            this.activePanels.splice(index, 1);
        }
    };
    /**
     * @param {?} id
     * @return {?}
     */
    NgtPanelService.prototype.openPanel = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        this.panelWillOpened.next(id);
    };
    /**
     * @param {?=} id
     * @return {?}
     */
    NgtPanelService.prototype.closePanel = /**
     * @param {?=} id
     * @return {?}
     */
    function (id) {
        this.panelWillClosed.next(id || this.activePanels[this.activePanels.length - 1]);
    };
    /**
     * @return {?}
     */
    NgtPanelService.prototype.updateState = /**
     * @return {?}
     */
    function () {
        this.panelState = this.isPanelHide ? 'hidden' : (this.isPanelExpand ? 'expanded' : 'collapsed');
    };
    NgtPanelService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    NgtPanelService.ctorParameters = function () { return []; };
    /** @nocollapse */ NgtPanelService.ngInjectableDef = i0.defineInjectable({ factory: function NgtPanelService_Factory() { return new NgtPanelService(); }, token: NgtPanelService, providedIn: "root" });
    return NgtPanelService;
}());
export { NgtPanelService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtPanelService.prototype.panels;
    /** @type {?} */
    NgtPanelService.prototype.activePanels;
    /** @type {?} */
    NgtPanelService.prototype.panelWillOpened;
    /** @type {?} */
    NgtPanelService.prototype.panelWillClosed;
    /** @type {?} */
    NgtPanelService.prototype.panelClosingDidStart;
    /** @type {?} */
    NgtPanelService.prototype.panelClosingDidDone;
    /** @type {?} */
    NgtPanelService.prototype.panelOpeningDidStart;
    /** @type {?} */
    NgtPanelService.prototype.panelOpeningDidDone;
    /** @type {?} */
    NgtPanelService.prototype.isPanelsChanged;
    /** @type {?} */
    NgtPanelService.prototype.isPanelHide;
    /** @type {?} */
    NgtPanelService.prototype.isPanelExpand;
    /** @type {?} */
    NgtPanelService.prototype.panelState;
    /** @type {?} */
    NgtPanelService.prototype.stateEvents;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsicGFuZWxzL3BhbmVsLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMvQixPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQzs7QUFJNUI7SUEyQkk7UUFBQSxpQkFrQkM7UUF6Q08sV0FBTSxHQUFVLEVBQUUsQ0FBQztRQUMzQixpQkFBWSxHQUFVLEVBQUUsQ0FBQztRQUN6QixvQkFBZSxHQUFHLElBQUksT0FBTyxFQUFVLENBQUM7UUFDeEMsb0JBQWUsR0FBRyxJQUFJLE9BQU8sRUFBVSxDQUFDO1FBQ3hDLHlCQUFvQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDckMsd0JBQW1CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNwQyx5QkFBb0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3JDLHdCQUFtQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDcEMsb0JBQWUsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ2hDLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLGtCQUFhLEdBQVksSUFBSSxDQUFDO1FBQzlCLGVBQVUsR0FBRyxVQUFVLENBQUM7UUFDeEIsZ0JBQVcsR0FBRztZQUNWLElBQUksRUFBRTtnQkFDRixNQUFNLEVBQUUsSUFBSSxPQUFPLEVBQVc7Z0JBQzlCLElBQUksRUFBRSxJQUFJLE9BQU8sRUFBVzthQUMvQjtZQUNELEtBQUssRUFBRTtnQkFDSCxNQUFNLEVBQUUsSUFBSSxPQUFPLEVBQVc7Z0JBQzlCLElBQUksRUFBRSxJQUFJLE9BQU8sRUFBVzthQUMvQjtTQUNKLENBQUM7UUFHRSwwQ0FBMEM7UUFDMUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLE1BQU07WUFDekMsS0FBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7WUFDNUIsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLE1BQU07WUFDdkMsS0FBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7WUFDMUIsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLE1BQU07WUFDMUMsS0FBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7WUFDNUIsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLE1BQU07WUFDeEMsS0FBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7WUFDMUIsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCw2QkFBRzs7OztJQUFILFVBQUksS0FBZTtRQUNmLDJDQUEyQztRQUMzQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7Ozs7SUFFRCxnQ0FBTTs7OztJQUFOLFVBQU8sRUFBVTs7O1lBRVAsYUFBYSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxhQUFhLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7Ozs7SUFFRCxxQ0FBVzs7OztJQUFYLFVBQVksRUFBVTtRQUNsQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUMvQixDQUFDOzs7OztJQUVELDBDQUFnQjs7OztJQUFoQixVQUFpQixFQUFVOztZQUNqQixLQUFLLEdBQVcsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDO1FBQ25ELElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ2QsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3RDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxtQ0FBUzs7OztJQUFULFVBQVUsRUFBVTtRQUNoQixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNsQyxDQUFDOzs7OztJQUVELG9DQUFVOzs7O0lBQVYsVUFBVyxFQUFXO1FBQ2xCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDckYsQ0FBQzs7OztJQUVELHFDQUFXOzs7SUFBWDtRQUNJLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDcEcsQ0FBQzs7Z0JBakZKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7OzBCQVJEO0NBd0ZDLEFBbEZELElBa0ZDO1NBL0VZLGVBQWU7Ozs7OztJQUN4QixpQ0FBMkI7O0lBQzNCLHVDQUF5Qjs7SUFDekIsMENBQXdDOztJQUN4QywwQ0FBd0M7O0lBQ3hDLCtDQUFxQzs7SUFDckMsOENBQW9DOztJQUNwQywrQ0FBcUM7O0lBQ3JDLDhDQUFvQzs7SUFDcEMsMENBQWdDOztJQUNoQyxzQ0FBNkI7O0lBQzdCLHdDQUE4Qjs7SUFDOUIscUNBQXdCOztJQUN4QixzQ0FTRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcblxyXG5pbXBvcnQgeyBOZ3RQYW5lbCB9IGZyb20gJy4vcGFuZWwubW9kZWwnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYW5lbFNlcnZpY2Uge1xyXG4gICAgcHJpdmF0ZSBwYW5lbHM6IGFueVtdID0gW107XHJcbiAgICBhY3RpdmVQYW5lbHM6IGFueVtdID0gW107XHJcbiAgICBwYW5lbFdpbGxPcGVuZWQgPSBuZXcgU3ViamVjdDxzdHJpbmc+KCk7XHJcbiAgICBwYW5lbFdpbGxDbG9zZWQgPSBuZXcgU3ViamVjdDxzdHJpbmc+KCk7XHJcbiAgICBwYW5lbENsb3NpbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBwYW5lbENsb3NpbmdEaWREb25lID0gbmV3IFN1YmplY3QoKTtcclxuICAgIHBhbmVsT3BlbmluZ0RpZFN0YXJ0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIHBhbmVsT3BlbmluZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgaXNQYW5lbHNDaGFuZ2VkID0gbmV3IFN1YmplY3QoKTtcclxuICAgIGlzUGFuZWxIaWRlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBpc1BhbmVsRXhwYW5kOiBib29sZWFuID0gdHJ1ZTtcclxuICAgIHBhbmVsU3RhdGUgPSAnZXhwYW5kZWQnO1xyXG4gICAgc3RhdGVFdmVudHMgPSB7XHJcbiAgICAgICAgbGVmdDoge1xyXG4gICAgICAgICAgICBleHBhbmQ6IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCksXHJcbiAgICAgICAgICAgIGhpZGU6IG5ldyBTdWJqZWN0PGJvb2xlYW4+KClcclxuICAgICAgICB9LFxyXG4gICAgICAgIHJpZ2h0OiB7XHJcbiAgICAgICAgICAgIGV4cGFuZDogbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKSxcclxuICAgICAgICAgICAgaGlkZTogbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKVxyXG4gICAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgLy8gU3Vic2NyaWJlIHN0YXRlIGV2ZW50cyBhbmQgdXBkYXRlIHN0YXRlXHJcbiAgICAgICAgdGhpcy5zdGF0ZUV2ZW50cy5sZWZ0LmV4cGFuZC5zdWJzY3JpYmUoc3RhdHVzID0+IHtcclxuICAgICAgICAgICAgdGhpcy5pc1BhbmVsRXhwYW5kID0gc3RhdHVzO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVN0YXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zdGF0ZUV2ZW50cy5sZWZ0LmhpZGUuc3Vic2NyaWJlKHN0YXR1cyA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNQYW5lbEhpZGUgPSBzdGF0dXM7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlU3RhdGUoKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN0YXRlRXZlbnRzLnJpZ2h0LmV4cGFuZC5zdWJzY3JpYmUoc3RhdHVzID0+IHtcclxuICAgICAgICAgICAgdGhpcy5pc1BhbmVsRXhwYW5kID0gc3RhdHVzO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVN0YXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5zdGF0ZUV2ZW50cy5yaWdodC5oaWRlLnN1YnNjcmliZShzdGF0dXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmlzUGFuZWxIaWRlID0gc3RhdHVzO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVN0YXRlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkKHBhbmVsOiBOZ3RQYW5lbCkge1xyXG4gICAgICAgIC8vIGFkZCBwYW5lbCB0byBhcnJheSBvZiBhY3RpdmUgcGFuZWxzLXBhZ2VcclxuICAgICAgICB0aGlzLnBhbmVscy5wdXNoKHBhbmVsKTtcclxuICAgICAgICB0aGlzLmlzUGFuZWxzQ2hhbmdlZC5uZXh0KHRoaXMucGFuZWxzLnNsaWNlKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZShpZDogc3RyaW5nKSB7XHJcbiAgICAgICAgLy8gcmVtb3ZlIHBhbmVsIGZyb20gYXJyYXkgb2YgYWN0aXZlIHBhbmVscy1wYWdlXHJcbiAgICAgICAgY29uc3QgcGFuZWxUb1JlbW92ZSA9IF8uZmluZCh0aGlzLnBhbmVscywge2lkOiBpZH0pO1xyXG4gICAgICAgIHRoaXMucGFuZWxzID0gXy53aXRob3V0KHRoaXMucGFuZWxzLCBwYW5lbFRvUmVtb3ZlKTtcclxuICAgICAgICB0aGlzLmlzUGFuZWxzQ2hhbmdlZC5uZXh0KHRoaXMucGFuZWxzLnNsaWNlKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZFRvQWN0aXZlKGlkOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLmFjdGl2ZVBhbmVscy5wdXNoKGlkKTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmVGcm9tQWN0aXZlKGlkOiBzdHJpbmcpIHtcclxuICAgICAgICBjb25zdCBpbmRleDogbnVtYmVyID0gdGhpcy5hY3RpdmVQYW5lbHMuaW5kZXhPZihpZCk7XHJcbiAgICAgICAgaWYgKGluZGV4ICE9PSAtMSkge1xyXG4gICAgICAgICAgICB0aGlzLmFjdGl2ZVBhbmVscy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvcGVuUGFuZWwoaWQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMucGFuZWxXaWxsT3BlbmVkLm5leHQoaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGNsb3NlUGFuZWwoaWQ/OiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLnBhbmVsV2lsbENsb3NlZC5uZXh0KGlkIHx8IHRoaXMuYWN0aXZlUGFuZWxzW3RoaXMuYWN0aXZlUGFuZWxzLmxlbmd0aCAtIDFdKTtcclxuICAgIH1cclxuXHJcbiAgICB1cGRhdGVTdGF0ZSgpIHtcclxuICAgICAgICB0aGlzLnBhbmVsU3RhdGUgPSB0aGlzLmlzUGFuZWxIaWRlID8gJ2hpZGRlbicgOiAodGhpcy5pc1BhbmVsRXhwYW5kID8gJ2V4cGFuZGVkJyA6ICdjb2xsYXBzZWQnKTtcclxuICAgIH1cclxufVxyXG4iXX0=