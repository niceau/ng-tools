/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
import { Component, ElementRef, HostListener, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Subject } from 'rxjs';
import { NgtPanelService } from './panel.service';
var NgtPanelsComponent = /** @class */ (function () {
    function NgtPanelsComponent(_panelService, elRef) {
        var _this = this;
        this._panelService = _panelService;
        this.elRef = elRef;
        this.overlay = true;
        this.leftPanelExpand = new Subject();
        this.leftPanelHide = new Subject();
        this.rightPanelExpand = new Subject();
        this.rightPanelHide = new Subject();
        this.subscriptions = [];
        this._isOpen = false;
        this.subscriptions.push(this._panelService.isPanelsChanged.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            var e_1, _a;
            (data.length < 1) && (_this.isOpen = false);
            console.log(data);
            try {
                for (var data_1 = tslib_1.__values(data), data_1_1 = data_1.next(); !data_1_1.done; data_1_1 = data_1.next()) {
                    var panel = data_1_1.value;
                    _this.elRef.nativeElement.querySelector('.panel-container').appendChild(panel.el.nativeElement);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (data_1_1 && !data_1_1.done && (_a = data_1.return)) _a.call(data_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        })));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    NgtPanelsComponent.prototype.onClick = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.onClickOutside(event);
    };
    Object.defineProperty(NgtPanelsComponent.prototype, "isOpen", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isOpen;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._isOpen = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    NgtPanelsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.subscriptions.push(this._panelService.panelOpeningDidStart.subscribe((/**
         * @param {?} _
         * @return {?}
         */
        function (_) { return _this.isOpen = true; })));
        this.subscriptions.push(this._panelService.panelClosingDidStart.subscribe((/**
         * @return {?}
         */
        function () {
            !_this._panelService.activePanels.length && (_this.isOpen = false);
        })));
        // Subscribe state events
        this.subscriptions.push(this.leftPanelExpand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) { return _this._panelService.stateEvents.left.expand.next(status); })));
        this.subscriptions.push(this.leftPanelHide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) { return _this._panelService.stateEvents.left.hide.next(status); })));
        this.subscriptions.push(this.rightPanelExpand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) { return _this._panelService.stateEvents.right.expand.next(status); })));
        this.subscriptions.push(this.rightPanelHide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        function (status) { return _this._panelService.stateEvents.right.hide.next(status); })));
    };
    /**
     * @return {?}
     */
    NgtPanelsComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.subscriptions.forEach((/**
         * @param {?} subscription
         * @return {?}
         */
        function (subscription) {
            subscription.unsubscribe();
        }));
    };
    /**
     * @param {?} e
     * @return {?}
     */
    NgtPanelsComponent.prototype.onClickOutside = /**
     * @param {?} e
     * @return {?}
     */
    function (e) {
        if (!e.target.closest('ngt-panel')) {
            this._panelService.closePanel();
        }
    };
    NgtPanelsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-panels',
                    template: "\n        <div class=\"panel-container\"></div>\n        <div\n            *ngIf=\"overlay\"\n            (click)=\"onClickOutside($event)\"\n            class=\"panel-overlay\"\n            [@overlay]='isOpen ? \"open\" : \"close\"'>\n        </div>\n    ",
                    animations: [
                        trigger('overlay', [
                            state('open', style({ opacity: 1 })),
                            state('close', style({ opacity: 0, display: 'none' })),
                            transition('close => open', animate('300ms')),
                            transition('open => close', animate('300ms'))
                        ])
                    ]
                }] }
    ];
    /** @nocollapse */
    NgtPanelsComponent.ctorParameters = function () { return [
        { type: NgtPanelService },
        { type: ElementRef }
    ]; };
    NgtPanelsComponent.propDecorators = {
        overlay: [{ type: Input }],
        leftPanelExpand: [{ type: Input }],
        leftPanelHide: [{ type: Input }],
        rightPanelExpand: [{ type: Input }],
        rightPanelHide: [{ type: Input }],
        onClick: [{ type: HostListener, args: ['document:click', ['$event'],] }],
        isOpen: [{ type: Input }]
    };
    return NgtPanelsComponent;
}());
export { NgtPanelsComponent };
if (false) {
    /** @type {?} */
    NgtPanelsComponent.prototype.overlay;
    /** @type {?} */
    NgtPanelsComponent.prototype.leftPanelExpand;
    /** @type {?} */
    NgtPanelsComponent.prototype.leftPanelHide;
    /** @type {?} */
    NgtPanelsComponent.prototype.rightPanelExpand;
    /** @type {?} */
    NgtPanelsComponent.prototype.rightPanelHide;
    /** @type {?} */
    NgtPanelsComponent.prototype.subscriptions;
    /**
     * @type {?}
     * @private
     */
    NgtPanelsComponent.prototype._isOpen;
    /**
     * @type {?}
     * @private
     */
    NgtPanelsComponent.prototype._panelService;
    /**
     * @type {?}
     * @private
     */
    NgtPanelsComponent.prototype.elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWxzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsicGFuZWxzL3BhbmVscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUM5RixPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ2pGLE9BQU8sRUFBRSxPQUFPLEVBQWdCLE1BQU0sTUFBTSxDQUFDO0FBRTdDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUdsRDtJQTJDSSw0QkFBb0IsYUFBOEIsRUFDOUIsS0FBaUI7UUFEckMsaUJBWUM7UUFabUIsa0JBQWEsR0FBYixhQUFhLENBQWlCO1FBQzlCLFVBQUssR0FBTCxLQUFLLENBQVk7UUF2QjVCLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixvQkFBZSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFDekMsa0JBQWEsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBQ3ZDLHFCQUFnQixHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFDMUMsbUJBQWMsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBQ2pELGtCQUFhLEdBQXdCLEVBQUUsQ0FBQztRQUNoQyxZQUFPLEdBQUcsS0FBSyxDQUFDO1FBa0JwQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxTQUFTOzs7O1FBQ2hFLFVBQUMsSUFBOEI7O1lBQzNCLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUM7WUFDM0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzs7Z0JBQ2xCLEtBQW9CLElBQUEsU0FBQSxpQkFBQSxJQUFJLENBQUEsMEJBQUEsNENBQUU7b0JBQXJCLElBQU0sS0FBSyxpQkFBQTtvQkFDWixLQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsQ0FBQztpQkFDbEc7Ozs7Ozs7OztRQUNMLENBQUMsRUFDQSxDQUNKLENBQUM7SUFDTixDQUFDOzs7OztJQXpCRCxvQ0FBTzs7OztJQURQLFVBQ1EsS0FBSztRQUNULElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELHNCQUNJLHNDQUFNOzs7O1FBSVY7WUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDeEIsQ0FBQzs7Ozs7UUFQRCxVQUNXLEtBQWM7WUFDckIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDekIsQ0FBQzs7O09BQUE7Ozs7SUFvQkQscUNBQVE7OztJQUFSO1FBQUEsaUJBY0M7UUFiRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLFNBQVM7Ozs7UUFBQyxVQUFBLENBQUMsSUFBSSxPQUFBLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxFQUFsQixDQUFrQixFQUFDLENBQUMsQ0FBQztRQUNwRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLFNBQVM7OztRQUNyRTtZQUNJLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsTUFBTSxJQUFJLENBQUMsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQztRQUNyRSxDQUFDLEVBQ0EsQ0FDSixDQUFDO1FBRUYseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQXZELENBQXVELEVBQUMsQ0FBQyxDQUFDO1FBQzNILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQXJELENBQXFELEVBQUMsQ0FBQyxDQUFDO1FBQ3ZILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBeEQsQ0FBd0QsRUFBQyxDQUFDLENBQUM7UUFDN0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBdEQsQ0FBc0QsRUFBQyxDQUFDLENBQUM7SUFDN0gsQ0FBQzs7OztJQUVELHdDQUFXOzs7SUFBWDtRQUNJLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTzs7OztRQUFDLFVBQUMsWUFBMEI7WUFDbEQsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCwyQ0FBYzs7OztJQUFkLFVBQWUsQ0FBQztRQUNaLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUNoQyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQ25DO0lBQ0wsQ0FBQzs7Z0JBbkZKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsWUFBWTtvQkFDdEIsUUFBUSxFQUFFLGtRQVFUO29CQUNELFVBQVUsRUFBRTt3QkFDUixPQUFPLENBQUMsU0FBUyxFQUFFOzRCQUNmLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7NEJBQ2xDLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQzs0QkFDcEQsVUFBVSxDQUFDLGVBQWUsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7NEJBQzdDLFVBQVUsQ0FBQyxlQUFlLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO3lCQUNoRCxDQUFDO3FCQUNMO2lCQUNKOzs7O2dCQXRCUSxlQUFlO2dCQUpKLFVBQVU7OzswQkE0QnpCLEtBQUs7a0NBQ0wsS0FBSztnQ0FDTCxLQUFLO21DQUNMLEtBQUs7aUNBQ0wsS0FBSzswQkFJTCxZQUFZLFNBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxRQUFRLENBQUM7eUJBS3pDLEtBQUs7O0lBa0RWLHlCQUFDO0NBQUEsQUFwRkQsSUFvRkM7U0FoRVksa0JBQWtCOzs7SUFDM0IscUNBQXdCOztJQUN4Qiw2Q0FBa0Q7O0lBQ2xELDJDQUFnRDs7SUFDaEQsOENBQW1EOztJQUNuRCw0Q0FBaUQ7O0lBQ2pELDJDQUF3Qzs7Ozs7SUFDeEMscUNBQXdCOzs7OztJQWdCWiwyQ0FBc0M7Ozs7O0lBQ3RDLG1DQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRWxlbWVudFJlZiwgSG9zdExpc3RlbmVyLCBJbnB1dCwgT25EZXN0cm95LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgYW5pbWF0ZSwgc3RhdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCB0cmlnZ2VyIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCB7IFN1YmplY3QsIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgTmd0UGFuZWxTZXJ2aWNlIH0gZnJvbSAnLi9wYW5lbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTmd0UGFuZWxDb21wb25lbnQgfSBmcm9tICcuL3BhbmVsLmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbmd0LXBhbmVscycsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJwYW5lbC1jb250YWluZXJcIj48L2Rpdj5cclxuICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICpuZ0lmPVwib3ZlcmxheVwiXHJcbiAgICAgICAgICAgIChjbGljayk9XCJvbkNsaWNrT3V0c2lkZSgkZXZlbnQpXCJcclxuICAgICAgICAgICAgY2xhc3M9XCJwYW5lbC1vdmVybGF5XCJcclxuICAgICAgICAgICAgW0BvdmVybGF5XT0naXNPcGVuID8gXCJvcGVuXCIgOiBcImNsb3NlXCInPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgYCxcclxuICAgIGFuaW1hdGlvbnM6IFtcclxuICAgICAgICB0cmlnZ2VyKCdvdmVybGF5JywgW1xyXG4gICAgICAgICAgICBzdGF0ZSgnb3BlbicsIHN0eWxlKHtvcGFjaXR5OiAxfSkpLFxyXG4gICAgICAgICAgICBzdGF0ZSgnY2xvc2UnLCBzdHlsZSh7b3BhY2l0eTogMCwgZGlzcGxheTogJ25vbmUnfSkpLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCdjbG9zZSA9PiBvcGVuJywgYW5pbWF0ZSgnMzAwbXMnKSksXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb24oJ29wZW4gPT4gY2xvc2UnLCBhbmltYXRlKCczMDBtcycpKVxyXG4gICAgICAgIF0pXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYW5lbHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgICBASW5wdXQoKSBvdmVybGF5ID0gdHJ1ZTtcclxuICAgIEBJbnB1dCgpIGxlZnRQYW5lbEV4cGFuZCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcbiAgICBASW5wdXQoKSBsZWZ0UGFuZWxIaWRlID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcclxuICAgIEBJbnB1dCgpIHJpZ2h0UGFuZWxFeHBhbmQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG4gICAgQElucHV0KCkgcmlnaHRQYW5lbEhpZGUgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG4gICAgc3Vic2NyaXB0aW9uczogQXJyYXk8U3Vic2NyaXB0aW9uPiA9IFtdO1xyXG4gICAgcHJpdmF0ZSBfaXNPcGVuID0gZmFsc2U7XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6Y2xpY2snLCBbJyRldmVudCddKVxyXG4gICAgb25DbGljayhldmVudCkge1xyXG4gICAgICAgIHRoaXMub25DbGlja091dHNpZGUoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzZXQgaXNPcGVuKHZhbHVlOiBib29sZWFuKSB7XHJcbiAgICAgICAgdGhpcy5faXNPcGVuID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGlzT3BlbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faXNPcGVuO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX3BhbmVsU2VydmljZTogTmd0UGFuZWxTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZikge1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKHRoaXMuX3BhbmVsU2VydmljZS5pc1BhbmVsc0NoYW5nZWQuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAoZGF0YTogQXJyYXk8Tmd0UGFuZWxDb21wb25lbnQ+KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAoZGF0YS5sZW5ndGggPCAxKSAmJiAodGhpcy5pc09wZW4gPSBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhKTtcclxuICAgICAgICAgICAgICAgIGZvciAoY29uc3QgcGFuZWwgb2YgZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWxSZWYubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKCcucGFuZWwtY29udGFpbmVyJykuYXBwZW5kQ2hpbGQocGFuZWwuZWwubmF0aXZlRWxlbWVudCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb25zLnB1c2godGhpcy5fcGFuZWxTZXJ2aWNlLnBhbmVsT3BlbmluZ0RpZFN0YXJ0LnN1YnNjcmliZShfID0+IHRoaXMuaXNPcGVuID0gdHJ1ZSkpO1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKHRoaXMuX3BhbmVsU2VydmljZS5wYW5lbENsb3NpbmdEaWRTdGFydC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICgpID0+IHtcclxuICAgICAgICAgICAgICAgICF0aGlzLl9wYW5lbFNlcnZpY2UuYWN0aXZlUGFuZWxzLmxlbmd0aCAmJiAodGhpcy5pc09wZW4gPSBmYWxzZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIC8vIFN1YnNjcmliZSBzdGF0ZSBldmVudHNcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaCh0aGlzLmxlZnRQYW5lbEV4cGFuZC5zdWJzY3JpYmUoc3RhdHVzID0+IHRoaXMuX3BhbmVsU2VydmljZS5zdGF0ZUV2ZW50cy5sZWZ0LmV4cGFuZC5uZXh0KHN0YXR1cykpKTtcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaCh0aGlzLmxlZnRQYW5lbEhpZGUuc3Vic2NyaWJlKHN0YXR1cyA9PiB0aGlzLl9wYW5lbFNlcnZpY2Uuc3RhdGVFdmVudHMubGVmdC5oaWRlLm5leHQoc3RhdHVzKSkpO1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKHRoaXMucmlnaHRQYW5lbEV4cGFuZC5zdWJzY3JpYmUoc3RhdHVzID0+IHRoaXMuX3BhbmVsU2VydmljZS5zdGF0ZUV2ZW50cy5yaWdodC5leHBhbmQubmV4dChzdGF0dXMpKSk7XHJcbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb25zLnB1c2godGhpcy5yaWdodFBhbmVsSGlkZS5zdWJzY3JpYmUoc3RhdHVzID0+IHRoaXMuX3BhbmVsU2VydmljZS5zdGF0ZUV2ZW50cy5yaWdodC5oaWRlLm5leHQoc3RhdHVzKSkpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5mb3JFYWNoKChzdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbikgPT4ge1xyXG4gICAgICAgICAgICBzdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvbkNsaWNrT3V0c2lkZShlKSB7XHJcbiAgICAgICAgaWYgKCFlLnRhcmdldC5jbG9zZXN0KCduZ3QtcGFuZWwnKSkge1xyXG4gICAgICAgICAgICB0aGlzLl9wYW5lbFNlcnZpY2UuY2xvc2VQYW5lbCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=