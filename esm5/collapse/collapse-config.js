/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtAlert component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the alerts used in the application.
 */
var NgtCollapseConfig = /** @class */ (function () {
    function NgtCollapseConfig() {
        this.animated = true;
        this.animationSpeed = 300;
        this.animationFunction = '';
    }
    NgtCollapseConfig.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */ NgtCollapseConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtCollapseConfig_Factory() { return new NgtCollapseConfig(); }, token: NgtCollapseConfig, providedIn: "root" });
    return NgtCollapseConfig;
}());
export { NgtCollapseConfig };
if (false) {
    /** @type {?} */
    NgtCollapseConfig.prototype.animated;
    /** @type {?} */
    NgtCollapseConfig.prototype.animationSpeed;
    /** @type {?} */
    NgtCollapseConfig.prototype.animationFunction;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2UtY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJjb2xsYXBzZS9jb2xsYXBzZS1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFPM0M7SUFBQTtRQUVJLGFBQVEsR0FBRyxJQUFJLENBQUM7UUFDaEIsbUJBQWMsR0FBRyxHQUFHLENBQUM7UUFDckIsc0JBQWlCLEdBQUcsRUFBRSxDQUFDO0tBQzFCOztnQkFMQSxVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7NEJBUGhDO0NBWUMsQUFMRCxJQUtDO1NBSlksaUJBQWlCOzs7SUFDMUIscUNBQWdCOztJQUNoQiwyQ0FBcUI7O0lBQ3JCLDhDQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbi8qKlxyXG4gKiBDb25maWd1cmF0aW9uIHNlcnZpY2UgZm9yIHRoZSBOZ3RBbGVydCBjb21wb25lbnQuXHJcbiAqIFlvdSBjYW4gaW5qZWN0IHRoaXMgc2VydmljZSwgdHlwaWNhbGx5IGluIHlvdXIgcm9vdCBjb21wb25lbnQsIGFuZCBjdXN0b21pemUgdGhlIHZhbHVlcyBvZiBpdHMgcHJvcGVydGllcyBpblxyXG4gKiBvcmRlciB0byBwcm92aWRlIGRlZmF1bHQgdmFsdWVzIGZvciBhbGwgdGhlIGFsZXJ0cyB1c2VkIGluIHRoZSBhcHBsaWNhdGlvbi5cclxuICovXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTmd0Q29sbGFwc2VDb25maWcge1xyXG4gICAgYW5pbWF0ZWQgPSB0cnVlO1xyXG4gICAgYW5pbWF0aW9uU3BlZWQgPSAzMDA7XHJcbiAgICBhbmltYXRpb25GdW5jdGlvbiA9ICcnO1xyXG59XHJcbiJdfQ==