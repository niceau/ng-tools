/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtCollapseService } from './collapse.service';
import { NgtCollapseDirective } from './collapse.directive';
import { NgtCollapseTriggerDirective } from './collapse-trigger.directive';
export { NgtCollapseService } from './collapse.service';
export { NgtCollapseDirective } from './collapse.directive';
export { NgtCollapseTriggerDirective } from './collapse-trigger.directive';
/** @type {?} */
var NGC_COLLAPSE_DIRECTIVES = [NgtCollapseDirective, NgtCollapseTriggerDirective];
var NgtCollapseModule = /** @class */ (function () {
    function NgtCollapseModule() {
    }
    /**
     * @return {?}
     */
    NgtCollapseModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtCollapseModule };
    };
    NgtCollapseModule.decorators = [
        { type: NgModule, args: [{
                    declarations: NGC_COLLAPSE_DIRECTIVES,
                    exports: NGC_COLLAPSE_DIRECTIVES,
                    imports: [CommonModule],
                    providers: [NgtCollapseService],
                },] }
    ];
    return NgtCollapseModule;
}());
export { NgtCollapseModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2UubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJjb2xsYXBzZS9jb2xsYXBzZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUUzRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQzs7SUFFckUsdUJBQXVCLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSwyQkFBMkIsQ0FBQztBQUVuRjtJQUFBO0lBVUEsQ0FBQzs7OztJQUhVLHlCQUFPOzs7SUFBZDtRQUNJLE9BQU8sRUFBQyxRQUFRLEVBQUUsaUJBQWlCLEVBQUMsQ0FBQztJQUN6QyxDQUFDOztnQkFUSixRQUFRLFNBQUM7b0JBQ04sWUFBWSxFQUFFLHVCQUF1QjtvQkFDckMsT0FBTyxFQUFFLHVCQUF1QjtvQkFDaEMsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO29CQUN2QixTQUFTLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztpQkFDbEM7O0lBS0Qsd0JBQUM7Q0FBQSxBQVZELElBVUM7U0FKWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgTmd0Q29sbGFwc2VTZXJ2aWNlIH0gZnJvbSAnLi9jb2xsYXBzZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTmd0Q29sbGFwc2VEaXJlY3RpdmUgfSBmcm9tICcuL2NvbGxhcHNlLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IE5ndENvbGxhcHNlVHJpZ2dlckRpcmVjdGl2ZSB9IGZyb20gJy4vY29sbGFwc2UtdHJpZ2dlci5kaXJlY3RpdmUnO1xyXG5cclxuZXhwb3J0IHsgTmd0Q29sbGFwc2VTZXJ2aWNlIH0gZnJvbSAnLi9jb2xsYXBzZS5zZXJ2aWNlJztcclxuZXhwb3J0IHsgTmd0Q29sbGFwc2VEaXJlY3RpdmUgfSBmcm9tICcuL2NvbGxhcHNlLmRpcmVjdGl2ZSc7XHJcbmV4cG9ydCB7IE5ndENvbGxhcHNlVHJpZ2dlckRpcmVjdGl2ZSB9IGZyb20gJy4vY29sbGFwc2UtdHJpZ2dlci5kaXJlY3RpdmUnO1xyXG5cclxuY29uc3QgTkdDX0NPTExBUFNFX0RJUkVDVElWRVMgPSBbTmd0Q29sbGFwc2VEaXJlY3RpdmUsIE5ndENvbGxhcHNlVHJpZ2dlckRpcmVjdGl2ZV07XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBOR0NfQ09MTEFQU0VfRElSRUNUSVZFUyxcclxuICAgIGV4cG9ydHM6IE5HQ19DT0xMQVBTRV9ESVJFQ1RJVkVTLFxyXG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV0sXHJcbiAgICBwcm92aWRlcnM6IFtOZ3RDb2xsYXBzZVNlcnZpY2VdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0Q29sbGFwc2VNb2R1bGUge1xyXG4gICAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICAgICAgcmV0dXJuIHtuZ01vZHVsZTogTmd0Q29sbGFwc2VNb2R1bGV9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==