/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, HostBinding, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NgtCollapseService } from './collapse.service';
import { NgtCollapseConfig } from './collapse-config';
var NgtCollapseDirective = /** @class */ (function () {
    function NgtCollapseDirective(config, elRef, $collapseService) {
        var _this = this;
        this.elRef = elRef;
        this.$collapseService = $collapseService;
        /**
         * A flag indicating show/hide with animation.
         */
        this.animated = true;
        /**
         * Speed of animation. No effect if [animated]=false.
         */
        this.animationSpeed = 300;
        /**
         * Animation function. No effect if [animated]=false.
         */
        this.animationFunction = '';
        /**
         * A flag indicating default opened status when component is load.
         */
        this.opened = false;
        this.show = this.status;
        this.animated = config.animated;
        this.animationSpeed = config.animationSpeed;
        this.animationFunction = config.animationFunction;
        this.status = this.opened;
        this.$collapseService.isCollapseTriggered.subscribe((/**
         * @param {?} id
         * @return {?}
         */
        function (id) {
            if (id === _this.id) {
                _this.toggle();
            }
        }));
    }
    Object.defineProperty(NgtCollapseDirective.prototype, "status", {
        get: /**
         * @return {?}
         */
        function () {
            return this._status;
        },
        set: /**
         * @param {?} status
         * @return {?}
         */
        function (status) {
            this._status = status;
            this.show = status;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(NgtCollapseDirective.prototype, "triggerShow", {
        get: /**
         * @return {?}
         */
        function () {
            return {
                value: this.animated ? (this.status ? 'down' : 'up') : (this.status ? 'show' : 'hide'),
                params: {
                    animationSpeed: this.animationSpeed,
                    animationFunction: this.animationFunction ? ' ' + this.animationFunction : ''
                }
            };
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    NgtCollapseDirective.prototype.toggle = /**
     * @return {?}
     */
    function () {
        this.status = !this.status;
    };
    NgtCollapseDirective.decorators = [
        { type: Component, args: [{
                    selector: '[ngtCollapse]',
                    exportAs: 'ngtCollapse',
                    animations: [
                        trigger('show', [
                            state('down', style({ overflow: 'hidden', height: '*', paddingTop: '*', paddingBottom: '*' })),
                            state('up', style({ overflow: 'hidden', height: 0, paddingTop: 0, paddingBottom: 0 })),
                            state('show', style({ overflow: 'hidden', height: '*', paddingTop: '*', paddingBottom: '*' })),
                            state('hide', style({ overflow: 'hidden', height: 0, paddingTop: 0, paddingBottom: 0 })),
                            transition('up => down', animate('{{animationSpeed}}ms{{animationFunction}}')),
                            transition('down => up', animate('{{animationSpeed}}ms{{animationFunction}}')),
                            transition('hide => show', animate('0ms')),
                            transition('show => hide', animate('0ms'))
                        ]),
                    ],
                    template: "\n        <ng-content></ng-content>"
                }] }
    ];
    /** @nocollapse */
    NgtCollapseDirective.ctorParameters = function () { return [
        { type: NgtCollapseConfig },
        { type: ElementRef },
        { type: NgtCollapseService }
    ]; };
    NgtCollapseDirective.propDecorators = {
        id: [{ type: Input, args: ['ngtCollapse',] }],
        animated: [{ type: Input }],
        animationSpeed: [{ type: Input }],
        animationFunction: [{ type: Input }],
        opened: [{ type: Input }],
        triggerShow: [{ type: HostBinding, args: ['@show',] }],
        show: [{ type: HostBinding, args: ['class.show',] }]
    };
    return NgtCollapseDirective;
}());
export { NgtCollapseDirective };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtCollapseDirective.prototype._status;
    /**
     * A id of element for collapsing.
     * @type {?}
     */
    NgtCollapseDirective.prototype.id;
    /**
     * A flag indicating show/hide with animation.
     * @type {?}
     */
    NgtCollapseDirective.prototype.animated;
    /**
     * Speed of animation. No effect if [animated]=false.
     * @type {?}
     */
    NgtCollapseDirective.prototype.animationSpeed;
    /**
     * Animation function. No effect if [animated]=false.
     * @type {?}
     */
    NgtCollapseDirective.prototype.animationFunction;
    /**
     * A flag indicating default opened status when component is load.
     * @type {?}
     */
    NgtCollapseDirective.prototype.opened;
    /** @type {?} */
    NgtCollapseDirective.prototype.show;
    /**
     * @type {?}
     * @private
     */
    NgtCollapseDirective.prototype.elRef;
    /**
     * @type {?}
     * @private
     */
    NgtCollapseDirective.prototype.$collapseService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2UuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJjb2xsYXBzZS9jb2xsYXBzZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVqRixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUV0RDtJQWtFSSw4QkFBWSxNQUF5QixFQUNqQixLQUFpQixFQUNqQixnQkFBb0M7UUFGeEQsaUJBWUM7UUFYbUIsVUFBSyxHQUFMLEtBQUssQ0FBWTtRQUNqQixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQW9COzs7O1FBL0IvQyxhQUFRLEdBQUcsSUFBSSxDQUFDOzs7O1FBS2hCLG1CQUFjLEdBQUcsR0FBRyxDQUFDOzs7O1FBS3JCLHNCQUFpQixHQUFHLEVBQUUsQ0FBQzs7OztRQUt2QixXQUFNLEdBQUcsS0FBSyxDQUFDO1FBWUcsU0FBSSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFLMUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxjQUFjLEdBQUcsTUFBTSxDQUFDLGNBQWMsQ0FBQztRQUM1QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsTUFBTSxDQUFDLGlCQUFpQixDQUFDO1FBQ2xELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUMxQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsU0FBUzs7OztRQUFDLFVBQUEsRUFBRTtZQUNsRCxJQUFJLEVBQUUsS0FBSyxLQUFJLENBQUMsRUFBRSxFQUFFO2dCQUNoQixLQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDakI7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7SUExREQsc0JBQUksd0NBQU07Ozs7UUFBVjtZQUNJLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUN4QixDQUFDOzs7OztRQUVELFVBQVcsTUFBZTtZQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztZQUN0QixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztRQUN2QixDQUFDOzs7T0FMQTtJQWdDRCxzQkFDSSw2Q0FBVzs7OztRQURmO1lBRUksT0FBTztnQkFDSCxLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO2dCQUN0RixNQUFNLEVBQUU7b0JBQ0osY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjO29CQUNuQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEVBQUU7aUJBQ2hGO2FBQ0osQ0FBQztRQUNOLENBQUM7OztPQUFBOzs7O0lBaUJELHFDQUFNOzs7SUFBTjtRQUNJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQy9CLENBQUM7O2dCQWxGSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGVBQWU7b0JBQ3pCLFFBQVEsRUFBRSxhQUFhO29CQUN2QixVQUFVLEVBQUU7d0JBQ1IsT0FBTyxDQUFDLE1BQU0sRUFBRTs0QkFDWixLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxFQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxHQUFHLEVBQUMsQ0FBQyxDQUFDOzRCQUM1RixLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxFQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDOzRCQUNwRixLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxFQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxHQUFHLEVBQUMsQ0FBQyxDQUFDOzRCQUM1RixLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxFQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsQ0FBQyxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDOzRCQUN0RixVQUFVLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQywyQ0FBMkMsQ0FBQyxDQUFDOzRCQUM5RSxVQUFVLENBQUMsWUFBWSxFQUFFLE9BQU8sQ0FBQywyQ0FBMkMsQ0FBQyxDQUFDOzRCQUM5RSxVQUFVLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDMUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7eUJBQzdDLENBQUM7cUJBQ0w7b0JBQ0QsUUFBUSxFQUFFLHFDQUNvQjtpQkFDakM7Ozs7Z0JBbkJRLGlCQUFpQjtnQkFKTixVQUFVO2dCQUdyQixrQkFBa0I7OztxQkFtQ3RCLEtBQUssU0FBQyxhQUFhOzJCQUtuQixLQUFLO2lDQUtMLEtBQUs7b0NBS0wsS0FBSzt5QkFLTCxLQUFLOzhCQUVMLFdBQVcsU0FBQyxPQUFPO3VCQVVuQixXQUFXLFNBQUMsWUFBWTs7SUFtQjdCLDJCQUFDO0NBQUEsQUFuRkQsSUFtRkM7U0FqRVksb0JBQW9COzs7Ozs7SUFDN0IsdUNBQXlCOzs7OztJQWF6QixrQ0FBaUM7Ozs7O0lBS2pDLHdDQUF5Qjs7Ozs7SUFLekIsOENBQThCOzs7OztJQUs5QixpREFBZ0M7Ozs7O0lBS2hDLHNDQUF3Qjs7SUFZeEIsb0NBQThDOzs7OztJQUdsQyxxQ0FBeUI7Ozs7O0lBQ3pCLGdEQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRWxlbWVudFJlZiwgSG9zdEJpbmRpbmcsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IGFuaW1hdGUsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlciB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5cclxuaW1wb3J0IHsgTmd0Q29sbGFwc2VTZXJ2aWNlIH0gZnJvbSAnLi9jb2xsYXBzZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTmd0Q29sbGFwc2VDb25maWcgfSBmcm9tICcuL2NvbGxhcHNlLWNvbmZpZyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnW25ndENvbGxhcHNlXScsXHJcbiAgICBleHBvcnRBczogJ25ndENvbGxhcHNlJyxcclxuICAgIGFuaW1hdGlvbnM6IFtcclxuICAgICAgICB0cmlnZ2VyKCdzaG93JywgW1xyXG4gICAgICAgICAgICBzdGF0ZSgnZG93bicsIHN0eWxlKHtvdmVyZmxvdzogJ2hpZGRlbicsIGhlaWdodDogJyonLCBwYWRkaW5nVG9wOiAnKicsIHBhZGRpbmdCb3R0b206ICcqJ30pKSxcclxuICAgICAgICAgICAgc3RhdGUoJ3VwJywgc3R5bGUoe292ZXJmbG93OiAnaGlkZGVuJywgaGVpZ2h0OiAwLCBwYWRkaW5nVG9wOiAwLCBwYWRkaW5nQm90dG9tOiAwfSkpLFxyXG4gICAgICAgICAgICBzdGF0ZSgnc2hvdycsIHN0eWxlKHtvdmVyZmxvdzogJ2hpZGRlbicsIGhlaWdodDogJyonLCBwYWRkaW5nVG9wOiAnKicsIHBhZGRpbmdCb3R0b206ICcqJ30pKSxcclxuICAgICAgICAgICAgc3RhdGUoJ2hpZGUnLCBzdHlsZSh7b3ZlcmZsb3c6ICdoaWRkZW4nLCBoZWlnaHQ6IDAsIHBhZGRpbmdUb3A6IDAsIHBhZGRpbmdCb3R0b206IDB9KSksXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb24oJ3VwID0+IGRvd24nLCBhbmltYXRlKCd7e2FuaW1hdGlvblNwZWVkfX1tc3t7YW5pbWF0aW9uRnVuY3Rpb259fScpKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbignZG93biA9PiB1cCcsIGFuaW1hdGUoJ3t7YW5pbWF0aW9uU3BlZWR9fW1ze3thbmltYXRpb25GdW5jdGlvbn19JykpLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCdoaWRlID0+IHNob3cnLCBhbmltYXRlKCcwbXMnKSksXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb24oJ3Nob3cgPT4gaGlkZScsIGFuaW1hdGUoJzBtcycpKVxyXG4gICAgICAgIF0pLFxyXG4gICAgXSxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PmAsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RDb2xsYXBzZURpcmVjdGl2ZSB7XHJcbiAgICBwcml2YXRlIF9zdGF0dXM6IGJvb2xlYW47XHJcbiAgICBnZXQgc3RhdHVzKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zdGF0dXM7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IHN0YXR1cyhzdGF0dXM6IGJvb2xlYW4pIHtcclxuICAgICAgICB0aGlzLl9zdGF0dXMgPSBzdGF0dXM7XHJcbiAgICAgICAgdGhpcy5zaG93ID0gc3RhdHVzO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQSBpZCBvZiBlbGVtZW50IGZvciBjb2xsYXBzaW5nLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoJ25ndENvbGxhcHNlJykgaWQ6IHN0cmluZztcclxuXHJcbiAgICAvKipcclxuICAgICAqIEEgZmxhZyBpbmRpY2F0aW5nIHNob3cvaGlkZSB3aXRoIGFuaW1hdGlvbi5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgYW5pbWF0ZWQgPSB0cnVlO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3BlZWQgb2YgYW5pbWF0aW9uLiBObyBlZmZlY3QgaWYgW2FuaW1hdGVkXT1mYWxzZS5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgYW5pbWF0aW9uU3BlZWQgPSAzMDA7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBbmltYXRpb24gZnVuY3Rpb24uIE5vIGVmZmVjdCBpZiBbYW5pbWF0ZWRdPWZhbHNlLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBhbmltYXRpb25GdW5jdGlvbiA9ICcnO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQSBmbGFnIGluZGljYXRpbmcgZGVmYXVsdCBvcGVuZWQgc3RhdHVzIHdoZW4gY29tcG9uZW50IGlzIGxvYWQuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIG9wZW5lZCA9IGZhbHNlO1xyXG5cclxuICAgIEBIb3N0QmluZGluZygnQHNob3cnKVxyXG4gICAgZ2V0IHRyaWdnZXJTaG93KCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHZhbHVlOiB0aGlzLmFuaW1hdGVkID8gKHRoaXMuc3RhdHVzID8gJ2Rvd24nIDogJ3VwJykgOiAodGhpcy5zdGF0dXMgPyAnc2hvdycgOiAnaGlkZScpLFxyXG4gICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgIGFuaW1hdGlvblNwZWVkOiB0aGlzLmFuaW1hdGlvblNwZWVkLFxyXG4gICAgICAgICAgICAgICAgYW5pbWF0aW9uRnVuY3Rpb246IHRoaXMuYW5pbWF0aW9uRnVuY3Rpb24gPyAnICcgKyB0aGlzLmFuaW1hdGlvbkZ1bmN0aW9uIDogJydcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLnNob3cnKSBzaG93ID0gdGhpcy5zdGF0dXM7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY29uZmlnOiBOZ3RDb2xsYXBzZUNvbmZpZyxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgZWxSZWY6IEVsZW1lbnRSZWYsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlICRjb2xsYXBzZVNlcnZpY2U6IE5ndENvbGxhcHNlU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMuYW5pbWF0ZWQgPSBjb25maWcuYW5pbWF0ZWQ7XHJcbiAgICAgICAgdGhpcy5hbmltYXRpb25TcGVlZCA9IGNvbmZpZy5hbmltYXRpb25TcGVlZDtcclxuICAgICAgICB0aGlzLmFuaW1hdGlvbkZ1bmN0aW9uID0gY29uZmlnLmFuaW1hdGlvbkZ1bmN0aW9uO1xyXG4gICAgICAgIHRoaXMuc3RhdHVzID0gdGhpcy5vcGVuZWQ7XHJcbiAgICAgICAgdGhpcy4kY29sbGFwc2VTZXJ2aWNlLmlzQ29sbGFwc2VUcmlnZ2VyZWQuc3Vic2NyaWJlKGlkID0+IHtcclxuICAgICAgICAgICAgaWYgKGlkID09PSB0aGlzLmlkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRvZ2dsZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdG9nZ2xlKCkge1xyXG4gICAgICAgIHRoaXMuc3RhdHVzID0gIXRoaXMuc3RhdHVzO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==