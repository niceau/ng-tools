/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtCollapseService } from './collapse.service';
var NgtCollapseTriggerDirective = /** @class */ (function () {
    function NgtCollapseTriggerDirective($collapseService) {
        this.$collapseService = $collapseService;
        /**
         * A flag to disable collapsing on click.
         */
        this.disabled = false;
    }
    /**
     * @return {?}
     */
    NgtCollapseTriggerDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        event.preventDefault();
        event.stopPropagation();
        this.toggle();
    };
    /**
     * @return {?}
     */
    NgtCollapseTriggerDirective.prototype.toggle = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.disabled) {
            return;
        }
        if (Array.isArray(this.id)) {
            this.id.forEach((/**
             * @param {?} id
             * @return {?}
             */
            function (id) {
                _this.$collapseService.toggle(id);
            }));
        }
        else {
            this.$collapseService.toggle(this.id);
        }
    };
    NgtCollapseTriggerDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtCollapseTrigger]'
                },] }
    ];
    /** @nocollapse */
    NgtCollapseTriggerDirective.ctorParameters = function () { return [
        { type: NgtCollapseService }
    ]; };
    NgtCollapseTriggerDirective.propDecorators = {
        id: [{ type: Input, args: ['ngtCollapseTrigger',] }],
        disabled: [{ type: Input }],
        onClick: [{ type: HostListener, args: ['click',] }]
    };
    return NgtCollapseTriggerDirective;
}());
export { NgtCollapseTriggerDirective };
if (false) {
    /**
     * A id of element to collapse.
     * @type {?}
     */
    NgtCollapseTriggerDirective.prototype.id;
    /**
     * A flag to disable collapsing on click.
     * @type {?}
     */
    NgtCollapseTriggerDirective.prototype.disabled;
    /**
     * @type {?}
     * @private
     */
    NgtCollapseTriggerDirective.prototype.$collapseService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2UtdHJpZ2dlci5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImNvbGxhcHNlL2NvbGxhcHNlLXRyaWdnZXIuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0QsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFHeEQ7SUFzQkkscUNBQW9CLGdCQUFvQztRQUFwQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQW9COzs7O1FBVi9DLGFBQVEsR0FBRyxLQUFLLENBQUM7SUFXMUIsQ0FBQzs7OztJQVJELDZDQUFPOzs7SUFEUDtRQUVJLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFeEIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2xCLENBQUM7Ozs7SUFLRCw0Q0FBTTs7O0lBQU47UUFBQSxpQkFZQztRQVhHLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNmLE9BQU87U0FDVjtRQUVELElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEVBQUU7WUFDeEIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQSxFQUFFO2dCQUNkLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDckMsQ0FBQyxFQUFDLENBQUM7U0FDTjthQUFNO1lBQ0gsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDekM7SUFDTCxDQUFDOztnQkFyQ0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxzQkFBc0I7aUJBQ25DOzs7O2dCQUxRLGtCQUFrQjs7O3FCQVV0QixLQUFLLFNBQUMsb0JBQW9COzJCQUsxQixLQUFLOzBCQUVMLFlBQVksU0FBQyxPQUFPOztJQXdCekIsa0NBQUM7Q0FBQSxBQXRDRCxJQXNDQztTQW5DWSwyQkFBMkI7Ozs7OztJQUlwQyx5Q0FBcUM7Ozs7O0lBS3JDLCtDQUEwQjs7Ozs7SUFVZCx1REFBNEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IE5ndENvbGxhcHNlU2VydmljZSB9IGZyb20gJy4vY29sbGFwc2Uuc2VydmljZSc7XHJcblxyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3RDb2xsYXBzZVRyaWdnZXJdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0Q29sbGFwc2VUcmlnZ2VyRGlyZWN0aXZlIHtcclxuICAgIC8qKlxyXG4gICAgICogQSBpZCBvZiBlbGVtZW50IHRvIGNvbGxhcHNlLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoJ25ndENvbGxhcHNlVHJpZ2dlcicpIGlkOiBhbnk7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBIGZsYWcgdG8gZGlzYWJsZSBjb2xsYXBzaW5nIG9uIGNsaWNrLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBkaXNhYmxlZCA9IGZhbHNlO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJylcclxuICAgIG9uQ2xpY2soKSB7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgdGhpcy50b2dnbGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlICRjb2xsYXBzZVNlcnZpY2U6IE5ndENvbGxhcHNlU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5kaXNhYmxlZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSh0aGlzLmlkKSkge1xyXG4gICAgICAgICAgICB0aGlzLmlkLmZvckVhY2goaWQgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy4kY29sbGFwc2VTZXJ2aWNlLnRvZ2dsZShpZCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuJGNvbGxhcHNlU2VydmljZS50b2dnbGUodGhpcy5pZCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==