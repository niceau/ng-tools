/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ComponentFactoryResolver, Directive, ElementRef, Input, ViewContainerRef } from '@angular/core';
import { NgtLoaderComponent } from './loader.component';
/**
 * Loader instance
 */
var NgtLoaderDirective = /** @class */ (function () {
    function NgtLoaderDirective(resolver, viewContainerRef, elRef) {
        this.resolver = resolver;
        this.viewContainerRef = viewContainerRef;
        this.elRef = elRef;
    }
    /**
     * @return {?}
     */
    NgtLoaderDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (!this.id) {
            console.error('loader must have an id');
            return;
        }
        /** @type {?} */
        var factory = this.resolver.resolveComponentFactory(NgtLoaderComponent);
        this.viewContainerRef.clear();
        this.componentRef = this.viewContainerRef.createComponent(factory);
        this.componentRef.instance.id = this.id;
        this.elRef.nativeElement.appendChild(this.componentRef.location.nativeElement);
    };
    NgtLoaderDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtLoader]'
                },] }
    ];
    /** @nocollapse */
    NgtLoaderDirective.ctorParameters = function () { return [
        { type: ComponentFactoryResolver },
        { type: ViewContainerRef },
        { type: ElementRef }
    ]; };
    NgtLoaderDirective.propDecorators = {
        id: [{ type: Input, args: ['ngtLoader',] }]
    };
    return NgtLoaderDirective;
}());
export { NgtLoaderDirective };
if (false) {
    /** @type {?} */
    NgtLoaderDirective.prototype.id;
    /** @type {?} */
    NgtLoaderDirective.prototype.container;
    /** @type {?} */
    NgtLoaderDirective.prototype.componentRef;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderDirective.prototype.resolver;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderDirective.prototype.viewContainerRef;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderDirective.prototype.elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibG9hZGVyL2xvYWRlci5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFFSCx3QkFBd0IsRUFFeEIsU0FBUyxFQUNULFVBQVUsRUFDVixLQUFLLEVBRUwsZ0JBQWdCLEVBQ25CLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9CQUFvQixDQUFDOzs7O0FBTXhEO0lBUUksNEJBQW9CLFFBQWtDLEVBQ2xDLGdCQUFrQyxFQUNsQyxLQUFpQjtRQUZqQixhQUFRLEdBQVIsUUFBUSxDQUEwQjtRQUNsQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLFVBQUssR0FBTCxLQUFLLENBQVk7SUFDckMsQ0FBQzs7OztJQUVELHFDQUFROzs7SUFBUjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ1YsT0FBTyxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQ3hDLE9BQU87U0FDVjs7WUFFSyxPQUFPLEdBQWdDLElBQUksQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsa0JBQWtCLENBQUM7UUFDdEcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxDQUFDO1FBRTlCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztRQUV4QyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDbkYsQ0FBQzs7Z0JBMUJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsYUFBYTtpQkFDMUI7Ozs7Z0JBaEJHLHdCQUF3QjtnQkFNeEIsZ0JBQWdCO2dCQUhoQixVQUFVOzs7cUJBZVQsS0FBSyxTQUFDLFdBQVc7O0lBdUJ0Qix5QkFBQztDQUFBLEFBM0JELElBMkJDO1NBeEJZLGtCQUFrQjs7O0lBQzNCLGdDQUErQjs7SUFDL0IsdUNBQTRCOztJQUM1QiwwQ0FBc0M7Ozs7O0lBRTFCLHNDQUEwQzs7Ozs7SUFDMUMsOENBQTBDOzs7OztJQUMxQyxtQ0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgQ29tcG9uZW50RmFjdG9yeSxcclxuICAgIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcclxuICAgIENvbXBvbmVudFJlZixcclxuICAgIERpcmVjdGl2ZSxcclxuICAgIEVsZW1lbnRSZWYsXHJcbiAgICBJbnB1dCxcclxuICAgIE9uSW5pdCxcclxuICAgIFZpZXdDb250YWluZXJSZWZcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmd0TG9hZGVyQ29tcG9uZW50IH0gZnJvbSAnLi9sb2FkZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTmd0TG9hZGVyIH0gZnJvbSAnLi9sb2FkZXIubW9kZWwnO1xyXG5cclxuLyoqXHJcbiAqIExvYWRlciBpbnN0YW5jZVxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3RMb2FkZXJdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0TG9hZGVyRGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgnbmd0TG9hZGVyJykgaWQ6IHN0cmluZztcclxuICAgIGNvbnRhaW5lcjogVmlld0NvbnRhaW5lclJlZjtcclxuICAgIGNvbXBvbmVudFJlZjogQ29tcG9uZW50UmVmPE5ndExvYWRlcj47XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByZXNvbHZlcjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSB2aWV3Q29udGFpbmVyUmVmOiBWaWV3Q29udGFpbmVyUmVmLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZikge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICghdGhpcy5pZCkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdsb2FkZXIgbXVzdCBoYXZlIGFuIGlkJyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGZhY3Rvcnk6IENvbXBvbmVudEZhY3Rvcnk8Tmd0TG9hZGVyPiA9IHRoaXMucmVzb2x2ZXIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkoTmd0TG9hZGVyQ29tcG9uZW50KTtcclxuICAgICAgICB0aGlzLnZpZXdDb250YWluZXJSZWYuY2xlYXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5jb21wb25lbnRSZWYgPSB0aGlzLnZpZXdDb250YWluZXJSZWYuY3JlYXRlQ29tcG9uZW50KGZhY3RvcnkpO1xyXG4gICAgICAgIHRoaXMuY29tcG9uZW50UmVmLmluc3RhbmNlLmlkID0gdGhpcy5pZDtcclxuXHJcbiAgICAgICAgdGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LmFwcGVuZENoaWxkKHRoaXMuY29tcG9uZW50UmVmLmxvY2F0aW9uLm5hdGl2ZUVsZW1lbnQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==