/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NgtLoaderService } from './loader.service';
var NgtLoaderComponent = /** @class */ (function () {
    function NgtLoaderComponent(_loaderService, _cdr) {
        this._loaderService = _loaderService;
        this._cdr = _cdr;
        this.content = null;
        /**
         *  Spinner type: 'border' | 'grow';
         */
        this.type = 'border';
        this._isPresent = false;
    }
    Object.defineProperty(NgtLoaderComponent.prototype, "isPresent", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isPresent;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._isPresent = value;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    NgtLoaderComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        // ensure id attribute exists
        if (!this.id) {
            console.error('loader must have an id');
            return;
        }
        // add self (this loader instance) to the loader service so it's accessible from controllers
        this._loaderService.add(this);
    };
    // remove self from loader service when directive is destroyed
    // remove self from loader service when directive is destroyed
    /**
     * @return {?}
     */
    NgtLoaderComponent.prototype.ngOnDestroy = 
    // remove self from loader service when directive is destroyed
    /**
     * @return {?}
     */
    function () {
        this._loaderService.remove(this.id);
    };
    // open loader
    // open loader
    /**
     * @return {?}
     */
    NgtLoaderComponent.prototype.present = 
    // open loader
    /**
     * @return {?}
     */
    function () {
        this.isPresent = true;
        this._cdr.detectChanges();
        return this._loaderService.isloaderOpened;
    };
    // close loader
    // close loader
    /**
     * @return {?}
     */
    NgtLoaderComponent.prototype.dismiss = 
    // close loader
    /**
     * @return {?}
     */
    function () {
        this.isPresent = false;
        this._cdr.detectChanges();
        return this._loaderService.isloaderClosed;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    NgtLoaderComponent.prototype.animationDone = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (event.toState === 'close') {
            this._loaderService.isloaderClosed.next();
        }
        if (event.toState === 'open') {
            this._loaderService.isloaderOpened.next();
        }
    };
    NgtLoaderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-loader-component',
                    template: "\n        <div\n            class=\"loader\"\n            [ngClass]=\"{'-single': !content}\"\n            [@loading]='isPresent ? \"open\" : \"close\"'\n            (@loading.done)=\"animationDone($event)\">\n            <div class=\"loader_overlay\"></div>\n            <div class=\"loader_wrapper\">\n                <div class=\"loader_spinner-wrap\">\n                    <div class=\"spinner-{{ type }} {{ spinnerClass }}\" role=\"status\">\n                        <span class=\"sr-only\">Loading...</span>\n                    </div>\n                </div>\n                <p *ngIf=\"content\">{{ content }}</p>\n            </div>\n        </div>\n\n    ",
                    animations: [
                        trigger('loading', [
                            state('open', style({ opacity: 1, visibility: 'visible' })),
                            state('close', style({ opacity: 0, visibility: 'hidden' })),
                            transition('close => open', animate('0.3s linear')),
                            transition('open => close', animate('0.2s linear'))
                        ])
                    ]
                }] }
    ];
    /** @nocollapse */
    NgtLoaderComponent.ctorParameters = function () { return [
        { type: NgtLoaderService },
        { type: ChangeDetectorRef }
    ]; };
    NgtLoaderComponent.propDecorators = {
        id: [{ type: Input }],
        type: [{ type: Input }],
        spinnerClass: [{ type: Input }],
        isPresent: [{ type: Input }]
    };
    return NgtLoaderComponent;
}());
export { NgtLoaderComponent };
if (false) {
    /** @type {?} */
    NgtLoaderComponent.prototype.id;
    /** @type {?} */
    NgtLoaderComponent.prototype.content;
    /**
     *  Spinner type: 'border' | 'grow';
     * @type {?}
     */
    NgtLoaderComponent.prototype.type;
    /**
     *  Spinner extra class;
     * @type {?}
     */
    NgtLoaderComponent.prototype.spinnerClass;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderComponent.prototype._isPresent;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderComponent.prototype._loaderService;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderComponent.prototype._cdr;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibG9hZGVyL2xvYWRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFxQixpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN2RixPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBR2pGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRXBEO0lBdURJLDRCQUFvQixjQUFnQyxFQUFVLElBQXVCO1FBQWpFLG1CQUFjLEdBQWQsY0FBYyxDQUFrQjtRQUFVLFNBQUksR0FBSixJQUFJLENBQW1CO1FBdkJyRixZQUFPLEdBQVcsSUFBSSxDQUFDOzs7O1FBS2QsU0FBSSxHQUFHLFFBQVEsQ0FBQztRQU9qQixlQUFVLEdBQUcsS0FBSyxDQUFDO0lBWTNCLENBQUM7SUFWRCxzQkFDSSx5Q0FBUzs7OztRQUliO1lBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzNCLENBQUM7Ozs7O1FBUEQsVUFDYyxLQUFjO1lBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQzVCLENBQUM7OztPQUFBOzs7O0lBU0QscUNBQVE7OztJQUFSO1FBQ0ksNkJBQTZCO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ1YsT0FBTyxDQUFDLEtBQUssQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1lBQ3hDLE9BQU87U0FDVjtRQUVELDRGQUE0RjtRQUM1RixJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQsOERBQThEOzs7OztJQUM5RCx3Q0FBVzs7Ozs7SUFBWDtRQUNJLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQsY0FBYzs7Ozs7SUFDZCxvQ0FBTzs7Ozs7SUFBUDtRQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDMUIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsZUFBZTs7Ozs7SUFDZixvQ0FBTzs7Ozs7SUFBUDtRQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDMUIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQztJQUM5QyxDQUFDOzs7OztJQUVELDBDQUFhOzs7O0lBQWIsVUFBYyxLQUFLO1FBQ2YsSUFBSSxLQUFLLENBQUMsT0FBTyxLQUFLLE9BQU8sRUFBRTtZQUMzQixJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUM3QztRQUNELElBQUksS0FBSyxDQUFDLE9BQU8sS0FBSyxNQUFNLEVBQUU7WUFDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDN0M7SUFDTCxDQUFDOztnQkEvRkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxzQkFBc0I7b0JBQ2hDLFFBQVEsRUFBRSwycEJBaUJUO29CQUNELFVBQVUsRUFBRTt3QkFDUixPQUFPLENBQUMsU0FBUyxFQUFFOzRCQUNmLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFDLENBQUMsQ0FBQzs0QkFDekQsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsRUFBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUMsQ0FBQyxDQUFDOzRCQUN6RCxVQUFVLENBQUMsZUFBZSxFQUFFLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQzs0QkFDbkQsVUFBVSxDQUFDLGVBQWUsRUFBRSxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7eUJBQ3RELENBQUM7cUJBQ0w7aUJBQ0o7Ozs7Z0JBOUJRLGdCQUFnQjtnQkFKcUIsaUJBQWlCOzs7cUJBcUMxRCxLQUFLO3VCQU1MLEtBQUs7K0JBS0wsS0FBSzs0QkFJTCxLQUFLOztJQWtEVix5QkFBQztDQUFBLEFBaEdELElBZ0dDO1NBbEVZLGtCQUFrQjs7O0lBQzNCLGdDQUFvQjs7SUFDcEIscUNBQXVCOzs7OztJQUt2QixrQ0FBeUI7Ozs7O0lBS3pCLDBDQUE4Qjs7Ozs7SUFFOUIsd0NBQTJCOzs7OztJQVdmLDRDQUF3Qzs7Ozs7SUFBRSxrQ0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSwgQ2hhbmdlRGV0ZWN0b3JSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgYW5pbWF0ZSwgc3RhdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCB0cmlnZ2VyIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IE5ndExvYWRlclNlcnZpY2UgfSBmcm9tICcuL2xvYWRlci5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICduZ3QtbG9hZGVyLWNvbXBvbmVudCcsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxkaXZcclxuICAgICAgICAgICAgY2xhc3M9XCJsb2FkZXJcIlxyXG4gICAgICAgICAgICBbbmdDbGFzc109XCJ7Jy1zaW5nbGUnOiAhY29udGVudH1cIlxyXG4gICAgICAgICAgICBbQGxvYWRpbmddPSdpc1ByZXNlbnQgPyBcIm9wZW5cIiA6IFwiY2xvc2VcIidcclxuICAgICAgICAgICAgKEBsb2FkaW5nLmRvbmUpPVwiYW5pbWF0aW9uRG9uZSgkZXZlbnQpXCI+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsb2FkZXJfb3ZlcmxheVwiPjwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibG9hZGVyX3dyYXBwZXJcIj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsb2FkZXJfc3Bpbm5lci13cmFwXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInNwaW5uZXIte3sgdHlwZSB9fSB7eyBzcGlubmVyQ2xhc3MgfX1cIiByb2xlPVwic3RhdHVzXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwic3Itb25seVwiPkxvYWRpbmcuLi48L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDxwICpuZ0lmPVwiY29udGVudFwiPnt7IGNvbnRlbnQgfX08L3A+XHJcbiAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvZGl2PlxyXG5cclxuICAgIGAsXHJcbiAgICBhbmltYXRpb25zOiBbXHJcbiAgICAgICAgdHJpZ2dlcignbG9hZGluZycsIFtcclxuICAgICAgICAgICAgc3RhdGUoJ29wZW4nLCBzdHlsZSh7b3BhY2l0eTogMSwgdmlzaWJpbGl0eTogJ3Zpc2libGUnfSkpLFxyXG4gICAgICAgICAgICBzdGF0ZSgnY2xvc2UnLCBzdHlsZSh7b3BhY2l0eTogMCwgdmlzaWJpbGl0eTogJ2hpZGRlbid9KSksXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb24oJ2Nsb3NlID0+IG9wZW4nLCBhbmltYXRlKCcwLjNzIGxpbmVhcicpKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbignb3BlbiA9PiBjbG9zZScsIGFuaW1hdGUoJzAuMnMgbGluZWFyJykpXHJcbiAgICAgICAgXSlcclxuICAgIF1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBOZ3RMb2FkZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgICBASW5wdXQoKSBpZDogc3RyaW5nO1xyXG4gICAgY29udGVudDogc3RyaW5nID0gbnVsbDtcclxuXHJcbiAgICAvKipcclxuICAgICAqICBTcGlubmVyIHR5cGU6ICdib3JkZXInIHwgJ2dyb3cnO1xyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSB0eXBlID0gJ2JvcmRlcic7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgU3Bpbm5lciBleHRyYSBjbGFzcztcclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgc3Bpbm5lckNsYXNzOiBzdHJpbmc7XHJcblxyXG4gICAgcHJpdmF0ZSBfaXNQcmVzZW50ID0gZmFsc2U7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHNldCBpc1ByZXNlbnQodmFsdWU6IGJvb2xlYW4pIHtcclxuICAgICAgICB0aGlzLl9pc1ByZXNlbnQgPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaXNQcmVzZW50KCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pc1ByZXNlbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfbG9hZGVyU2VydmljZTogTmd0TG9hZGVyU2VydmljZSwgcHJpdmF0ZSBfY2RyOiBDaGFuZ2VEZXRlY3RvclJlZikge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIC8vIGVuc3VyZSBpZCBhdHRyaWJ1dGUgZXhpc3RzXHJcbiAgICAgICAgaWYgKCF0aGlzLmlkKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ2xvYWRlciBtdXN0IGhhdmUgYW4gaWQnKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gYWRkIHNlbGYgKHRoaXMgbG9hZGVyIGluc3RhbmNlKSB0byB0aGUgbG9hZGVyIHNlcnZpY2Ugc28gaXQncyBhY2Nlc3NpYmxlIGZyb20gY29udHJvbGxlcnNcclxuICAgICAgICB0aGlzLl9sb2FkZXJTZXJ2aWNlLmFkZCh0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyByZW1vdmUgc2VsZiBmcm9tIGxvYWRlciBzZXJ2aWNlIHdoZW4gZGlyZWN0aXZlIGlzIGRlc3Ryb3llZFxyXG4gICAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fbG9hZGVyU2VydmljZS5yZW1vdmUodGhpcy5pZCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gb3BlbiBsb2FkZXJcclxuICAgIHByZXNlbnQoKTogT2JzZXJ2YWJsZTxib29sZWFuPiB8IFByb21pc2U8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHRoaXMuaXNQcmVzZW50ID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLl9jZHIuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9sb2FkZXJTZXJ2aWNlLmlzbG9hZGVyT3BlbmVkO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGNsb3NlIGxvYWRlclxyXG4gICAgZGlzbWlzcygpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHwgUHJvbWlzZTxib29sZWFuPiB7XHJcbiAgICAgICAgdGhpcy5pc1ByZXNlbnQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLl9jZHIuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9sb2FkZXJTZXJ2aWNlLmlzbG9hZGVyQ2xvc2VkO1xyXG4gICAgfVxyXG5cclxuICAgIGFuaW1hdGlvbkRvbmUoZXZlbnQpIHtcclxuICAgICAgICBpZiAoZXZlbnQudG9TdGF0ZSA9PT0gJ2Nsb3NlJykge1xyXG4gICAgICAgICAgICB0aGlzLl9sb2FkZXJTZXJ2aWNlLmlzbG9hZGVyQ2xvc2VkLm5leHQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGV2ZW50LnRvU3RhdGUgPT09ICdvcGVuJykge1xyXG4gICAgICAgICAgICB0aGlzLl9sb2FkZXJTZXJ2aWNlLmlzbG9hZGVyT3BlbmVkLm5leHQoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19