/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtLoaderComponent } from './loader.component';
import { NgtLoaderDirective } from './loader.directive';
export { NgtLoaderService } from './loader.service';
export { NgtLoaderComponent } from './loader.component';
export { NgtLoaderDirective } from './loader.directive';
export { NgtLoader } from './loader.model';
/** @type {?} */
var NGC_LOADER_DIRECTIVES = [
    NgtLoaderComponent, NgtLoaderDirective
];
var NgtLoaderModule = /** @class */ (function () {
    function NgtLoaderModule() {
    }
    /**
     * @return {?}
     */
    NgtLoaderModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtLoaderModule };
    };
    NgtLoaderModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [NGC_LOADER_DIRECTIVES],
                    exports: [NGC_LOADER_DIRECTIVES],
                    imports: [CommonModule],
                    entryComponents: [NgtLoaderComponent]
                },] }
    ];
    return NgtLoaderModule;
}());
export { NgtLoaderModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibG9hZGVyL2xvYWRlci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUcvQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUV4RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7O0lBRXJDLHFCQUFxQixHQUFHO0lBQzFCLGtCQUFrQixFQUFFLGtCQUFrQjtDQUN6QztBQUVEO0lBQUE7SUFVQSxDQUFDOzs7O0lBSFUsdUJBQU87OztJQUFkO1FBQ0ksT0FBTyxFQUFDLFFBQVEsRUFBRSxlQUFlLEVBQUMsQ0FBQztJQUN2QyxDQUFDOztnQkFUSixRQUFRLFNBQUM7b0JBQ04sWUFBWSxFQUFFLENBQUMscUJBQXFCLENBQUM7b0JBQ3JDLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDO29CQUNoQyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7b0JBQ3ZCLGVBQWUsRUFBRSxDQUFDLGtCQUFrQixDQUFDO2lCQUN4Qzs7SUFLRCxzQkFBQztDQUFBLEFBVkQsSUFVQztTQUpZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgTmd0TG9hZGVyU2VydmljZSB9IGZyb20gJy4vbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOZ3RMb2FkZXJDb21wb25lbnQgfSBmcm9tICcuL2xvYWRlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBOZ3RMb2FkZXJEaXJlY3RpdmUgfSBmcm9tICcuL2xvYWRlci5kaXJlY3RpdmUnO1xyXG5cclxuZXhwb3J0IHsgTmd0TG9hZGVyU2VydmljZSB9IGZyb20gJy4vbG9hZGVyLnNlcnZpY2UnO1xyXG5leHBvcnQgeyBOZ3RMb2FkZXJDb21wb25lbnQgfSBmcm9tICcuL2xvYWRlci5jb21wb25lbnQnO1xyXG5leHBvcnQgeyBOZ3RMb2FkZXJEaXJlY3RpdmUgfSBmcm9tICcuL2xvYWRlci5kaXJlY3RpdmUnO1xyXG5leHBvcnQgeyBOZ3RMb2FkZXIgfSBmcm9tICcuL2xvYWRlci5tb2RlbCc7XHJcblxyXG5jb25zdCBOR0NfTE9BREVSX0RJUkVDVElWRVMgPSBbXHJcbiAgICBOZ3RMb2FkZXJDb21wb25lbnQsIE5ndExvYWRlckRpcmVjdGl2ZVxyXG5dO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW05HQ19MT0FERVJfRElSRUNUSVZFU10sXHJcbiAgICBleHBvcnRzOiBbTkdDX0xPQURFUl9ESVJFQ1RJVkVTXSxcclxuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBbTmd0TG9hZGVyQ29tcG9uZW50XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0TG9hZGVyTW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgICAgIHJldHVybiB7bmdNb2R1bGU6IE5ndExvYWRlck1vZHVsZX07XHJcbiAgICB9XHJcbn1cclxuIl19