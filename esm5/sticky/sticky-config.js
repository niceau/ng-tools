/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtSticky component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the stickies used in the application.
 */
var NgtStickyConfig = /** @class */ (function () {
    function NgtStickyConfig() {
        this.zIndex = 10;
        this.width = 'auto';
        this.offsetTop = 0;
        this.offsetBottom = 0;
        this.start = 0;
        this.stickClass = 'sticky';
        this.endStickClass = 'sticky-end';
        this.mediaQuery = '';
        this.parentMode = true;
        this.orientation = 'none';
    }
    NgtStickyConfig.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */ NgtStickyConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtStickyConfig_Factory() { return new NgtStickyConfig(); }, token: NgtStickyConfig, providedIn: "root" });
    return NgtStickyConfig;
}());
export { NgtStickyConfig };
if (false) {
    /** @type {?} */
    NgtStickyConfig.prototype.sticky;
    /** @type {?} */
    NgtStickyConfig.prototype.zIndex;
    /** @type {?} */
    NgtStickyConfig.prototype.width;
    /** @type {?} */
    NgtStickyConfig.prototype.offsetTop;
    /** @type {?} */
    NgtStickyConfig.prototype.offsetBottom;
    /** @type {?} */
    NgtStickyConfig.prototype.start;
    /** @type {?} */
    NgtStickyConfig.prototype.stickClass;
    /** @type {?} */
    NgtStickyConfig.prototype.endStickClass;
    /** @type {?} */
    NgtStickyConfig.prototype.mediaQuery;
    /** @type {?} */
    NgtStickyConfig.prototype.parentMode;
    /** @type {?} */
    NgtStickyConfig.prototype.orientation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RpY2t5LWNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsic3RpY2t5L3N0aWNreS1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFPM0M7SUFBQTtRQUdJLFdBQU0sR0FBRyxFQUFFLENBQUM7UUFDWixVQUFLLEdBQUcsTUFBTSxDQUFDO1FBQ2YsY0FBUyxHQUFHLENBQUMsQ0FBQztRQUNkLGlCQUFZLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLFVBQUssR0FBRyxDQUFDLENBQUM7UUFDVixlQUFVLEdBQUcsUUFBUSxDQUFDO1FBQ3RCLGtCQUFhLEdBQUcsWUFBWSxDQUFDO1FBQzdCLGVBQVUsR0FBRyxFQUFFLENBQUM7UUFDaEIsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixnQkFBVyxHQUE4QixNQUFNLENBQUM7S0FDbkQ7O2dCQWJBLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7OzswQkFQaEM7Q0FvQkMsQUFiRCxJQWFDO1NBWlksZUFBZTs7O0lBQ3hCLGlDQUFlOztJQUNmLGlDQUFZOztJQUNaLGdDQUFlOztJQUNmLG9DQUFjOztJQUNkLHVDQUFpQjs7SUFDakIsZ0NBQVU7O0lBQ1YscUNBQXNCOztJQUN0Qix3Q0FBNkI7O0lBQzdCLHFDQUFnQjs7SUFDaEIscUNBQWtCOztJQUNsQixzQ0FBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vKipcclxuICogQ29uZmlndXJhdGlvbiBzZXJ2aWNlIGZvciB0aGUgTmd0U3RpY2t5IGNvbXBvbmVudC5cclxuICogWW91IGNhbiBpbmplY3QgdGhpcyBzZXJ2aWNlLCB0eXBpY2FsbHkgaW4geW91ciByb290IGNvbXBvbmVudCwgYW5kIGN1c3RvbWl6ZSB0aGUgdmFsdWVzIG9mIGl0cyBwcm9wZXJ0aWVzIGluXHJcbiAqIG9yZGVyIHRvIHByb3ZpZGUgZGVmYXVsdCB2YWx1ZXMgZm9yIGFsbCB0aGUgc3RpY2tpZXMgdXNlZCBpbiB0aGUgYXBwbGljYXRpb24uXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcclxuZXhwb3J0IGNsYXNzIE5ndFN0aWNreUNvbmZpZyB7XHJcbiAgICBzdGlja3k6IHN0cmluZztcclxuICAgIHpJbmRleCA9IDEwO1xyXG4gICAgd2lkdGggPSAnYXV0byc7XHJcbiAgICBvZmZzZXRUb3AgPSAwO1xyXG4gICAgb2Zmc2V0Qm90dG9tID0gMDtcclxuICAgIHN0YXJ0ID0gMDtcclxuICAgIHN0aWNrQ2xhc3MgPSAnc3RpY2t5JztcclxuICAgIGVuZFN0aWNrQ2xhc3MgPSAnc3RpY2t5LWVuZCc7XHJcbiAgICBtZWRpYVF1ZXJ5ID0gJyc7XHJcbiAgICBwYXJlbnRNb2RlID0gdHJ1ZTtcclxuICAgIG9yaWVudGF0aW9uOiAnbGVmdCcgfCAncmlnaHQnIHwgJ25vbmUnID0gJ25vbmUnO1xyXG59XHJcbiJdfQ==