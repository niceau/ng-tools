/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtSticky } from './sticky';
export { NgtSticky } from './sticky';
export { NgtStickyConfig } from './sticky-config';
/** @type {?} */
var NGT_STICKY_DIRECTIVES = [NgtSticky];
var NgtStickyModule = /** @class */ (function () {
    function NgtStickyModule() {
    }
    /**
     * @return {?}
     */
    NgtStickyModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtStickyModule };
    };
    NgtStickyModule.decorators = [
        { type: NgModule, args: [{
                    declarations: NGT_STICKY_DIRECTIVES,
                    exports: NGT_STICKY_DIRECTIVES,
                    imports: [CommonModule]
                },] }
    ];
    return NgtStickyModule;
}());
export { NgtStickyModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RpY2t5Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsic3RpY2t5L3N0aWNreS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sVUFBVSxDQUFDO0FBRXJDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxVQUFVLENBQUM7QUFDckMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlCQUFpQixDQUFDOztJQUU1QyxxQkFBcUIsR0FBRyxDQUFDLFNBQVMsQ0FBQztBQUV6QztJQUFBO0lBU0EsQ0FBQzs7OztJQUhVLHVCQUFPOzs7SUFBZDtRQUNJLE9BQU8sRUFBQyxRQUFRLEVBQUUsZUFBZSxFQUFDLENBQUM7SUFDdkMsQ0FBQzs7Z0JBUkosUUFBUSxTQUFDO29CQUNOLFlBQVksRUFBRSxxQkFBcUI7b0JBQ25DLE9BQU8sRUFBRSxxQkFBcUI7b0JBQzlCLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztpQkFDMUI7O0lBS0Qsc0JBQUM7Q0FBQSxBQVRELElBU0M7U0FKWSxlQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbmltcG9ydCB7IE5ndFN0aWNreSB9IGZyb20gJy4vc3RpY2t5JztcclxuXHJcbmV4cG9ydCB7IE5ndFN0aWNreSB9IGZyb20gJy4vc3RpY2t5JztcclxuZXhwb3J0IHsgTmd0U3RpY2t5Q29uZmlnIH0gZnJvbSAnLi9zdGlja3ktY29uZmlnJztcclxuXHJcbmNvbnN0IE5HVF9TVElDS1lfRElSRUNUSVZFUyA9IFtOZ3RTdGlja3ldO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogTkdUX1NUSUNLWV9ESVJFQ1RJVkVTLFxyXG4gICAgZXhwb3J0czogTkdUX1NUSUNLWV9ESVJFQ1RJVkVTLFxyXG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndFN0aWNreU1vZHVsZSB7XHJcbiAgICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge25nTW9kdWxlOiBOZ3RTdGlja3lNb2R1bGV9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==