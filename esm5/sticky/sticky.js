/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, Input, Output, EventEmitter, HostListener, Renderer2 } from '@angular/core';
import { NgtStickyConfig } from './sticky-config';
var NgtSticky = /** @class */ (function () {
    function NgtSticky(element, _renderer, config) {
        this.element = element;
        this._renderer = _renderer;
        this.activated = new EventEmitter();
        this.deactivated = new EventEmitter();
        this.reset = new EventEmitter();
        this.isStuck = false;
        this.orientation = config.orientation;
        this.sticky = config.sticky;
        this.zIndex = config.zIndex;
        this.width = config.width;
        this.offsetTop = config.offsetTop;
        this.offsetBottom = config.offsetBottom;
        this.start = config.start;
        this.stickClass = config.stickClass;
        this.endStickClass = config.endStickClass;
        this.mediaQuery = config.mediaQuery;
        this.parentMode = config.parentMode;
        this.orientation = config.orientation;
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    NgtSticky.prototype.onChange = /**
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        this.sticker();
    };
    /**
     * @return {?}
     */
    NgtSticky.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.elem = this.element.nativeElement;
    };
    /**
     * @return {?}
     */
    NgtSticky.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        // define scroll container as parent element
        this.container = this._renderer.parentNode(this.elem);
        this.defineOriginalDimensions();
        this.sticker();
    };
    /**
     * @return {?}
     */
    NgtSticky.prototype.defineOriginalDimensions = /**
     * @return {?}
     */
    function () {
        this.originalCss = {
            zIndex: this.getCssValue(this.elem, 'zIndex'),
            position: this.getCssValue(this.elem, 'position'),
            top: this.getCssValue(this.elem, 'top'),
            right: this.getCssValue(this.elem, 'right'),
            left: this.getCssValue(this.elem, 'left'),
            bottom: this.getCssValue(this.elem, 'bottom'),
            width: this.getCssValue(this.elem, 'width'),
        };
    };
    /**
     * @return {?}
     */
    NgtSticky.prototype.defineYDimensions = /**
     * @return {?}
     */
    function () {
        this.containerTop = this.getBoundingClientRectValue(this.container, 'top');
        this.windowHeight = window.innerHeight;
        this.elemHeight = this.getCssNumber(this.elem, 'height');
        this.containerHeight = this.getCssNumber(this.container, 'height');
        this.containerStart = this.containerTop + this.getCssNumber(this.container, 'padding-top') + this.scrollbarYPos() - this.offsetTop + this.start;
        if (this.parentMode) {
            this.scrollFinish = this.containerStart - this.start - this.offsetBottom + (this.containerHeight - this.elemHeight);
        }
        else {
            this.scrollFinish = document.body.offsetHeight;
        }
    };
    /**
     * @return {?}
     */
    NgtSticky.prototype.defineXDimensions = /**
     * @return {?}
     */
    function () {
        this.containerWidth = this.getCssNumber(this.container, 'width');
        this.setStyles(this.elem, {
            display: 'block',
            position: 'static',
            width: this.width
        });
        this.originalCss.width = this.getCssValue(this.elem, 'width');
    };
    /**
     * @return {?}
     */
    NgtSticky.prototype.resetElement = /**
     * @return {?}
     */
    function () {
        this.elem.classList.remove(this.stickClass);
        this.setStyles(this.elem, this.originalCss);
        this.reset.next(this.elem);
    };
    /**
     * @return {?}
     */
    NgtSticky.prototype.stuckElement = /**
     * @return {?}
     */
    function () {
        this.isStuck = true;
        this.elem.classList.remove(this.endStickClass);
        this.elem.classList.add(this.stickClass);
        this.setStyles(this.elem, {
            zIndex: this.zIndex,
            position: 'fixed',
            top: this.offsetTop + 'px',
            right: 'auto',
            bottom: 'auto',
            left: this.getBoundingClientRectValue(this.elem, 'left') + 'px',
            width: this.getCssValue(this.elem, 'width')
        });
        this.activated.next(this.elem);
    };
    /**
     * @return {?}
     */
    NgtSticky.prototype.unstuckElement = /**
     * @return {?}
     */
    function () {
        this.isStuck = false;
        this.elem.classList.add(this.endStickClass);
        this.container.style.position = 'relative';
        this.setStyles(this.elem, {
            position: 'absolute',
            top: 'auto',
            left: 'auto',
            right: this.getCssValue(this.elem, 'float') === 'right' || this.orientation === 'right' ? 0 : 'auto',
            bottom: this.offsetBottom + 'px',
            width: this.getCssValue(this.elem, 'width')
        });
        this.deactivated.next(this.elem);
    };
    /**
     * @return {?}
     */
    NgtSticky.prototype.matchMediaQuery = /**
     * @return {?}
     */
    function () {
        if (!this.mediaQuery) {
            return true;
        }
        return (window.matchMedia('(' + this.mediaQuery + ')').matches ||
            window.matchMedia(this.mediaQuery).matches);
    };
    /**
     * @return {?}
     */
    NgtSticky.prototype.sticker = /**
     * @return {?}
     */
    function () {
        // check media query
        if (this.isStuck && !this.matchMediaQuery()) {
            this.resetElement();
            return;
        }
        // detecting when a container's height, width or top position changes
        /** @type {?} */
        var currentContainerHeight = this.getCssNumber(this.container, 'height');
        /** @type {?} */
        var currentContainerWidth = this.getCssNumber(this.container, 'width');
        /** @type {?} */
        var currentContainerTop = this.getBoundingClientRectValue(this.container, 'top');
        if (currentContainerHeight !== this.containerHeight) {
            this.defineYDimensions();
        }
        if (currentContainerTop !== this.containerTop) {
            this.defineYDimensions();
        }
        if (currentContainerWidth !== this.containerWidth) {
            this.defineXDimensions();
        }
        // check if the sticky element is above the container
        if (this.elemHeight >= currentContainerHeight) {
            return;
        }
        /** @type {?} */
        var position = this.scrollbarYPos();
        if (this.isStuck && (position < this.containerStart || position > this.scrollFinish) || position > this.scrollFinish) { // unstick
            this.resetElement();
            if (position > this.scrollFinish) {
                this.unstuckElement();
            }
            this.isStuck = false;
        }
        else if (position > this.containerStart && position < this.scrollFinish) { // stick
            this.stuckElement();
        }
    };
    /**
     * @private
     * @return {?}
     */
    NgtSticky.prototype.scrollbarYPos = /**
     * @private
     * @return {?}
     */
    function () {
        return window.pageYOffset || document.documentElement.scrollTop;
    };
    /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    NgtSticky.prototype.getBoundingClientRectValue = /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    function (element, property) {
        /** @type {?} */
        var result = 0;
        if (element && element.getBoundingClientRect) {
            /** @type {?} */
            var rect = element.getBoundingClientRect();
            result = (typeof rect[property] !== 'undefined') ? rect[property] : 0;
        }
        return result;
    };
    /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    NgtSticky.prototype.getCssValue = /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    function (element, property) {
        /** @type {?} */
        var result = '';
        if (typeof window.getComputedStyle !== 'undefined') {
            result = window.getComputedStyle(element, '').getPropertyValue(property);
        }
        else if (typeof element.currentStyle !== 'undefined') {
            result = element.currentStyle[property];
        }
        return result;
    };
    /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    NgtSticky.prototype.getCssNumber = /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    function (element, property) {
        if (typeof element === 'undefined') {
            return 0;
        }
        return parseInt(this.getCssValue(element, property), 10) || 0;
    };
    /**
     * @private
     * @param {?} element
     * @param {?} styles
     * @return {?}
     */
    NgtSticky.prototype.setStyles = /**
     * @private
     * @param {?} element
     * @param {?} styles
     * @return {?}
     */
    function (element, styles) {
        if (typeof styles === 'object') {
            for (var property in styles) {
                if (styles.hasOwnProperty(property)) {
                    this._renderer.setStyle(element, property, styles[property]);
                }
            }
        }
    };
    NgtSticky.decorators = [
        { type: Component, args: [{
                    selector: 'ngt-sticky,[ngtSticky]',
                    template: '<ng-content></ng-content>'
                }] }
    ];
    /** @nocollapse */
    NgtSticky.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 },
        { type: NgtStickyConfig }
    ]; };
    NgtSticky.propDecorators = {
        sticky: [{ type: Input, args: ['sticky',] }],
        zIndex: [{ type: Input, args: ['sticky-zIndex',] }],
        width: [{ type: Input, args: ['sticky-width',] }],
        offsetTop: [{ type: Input, args: ['sticky-offset-top',] }],
        offsetBottom: [{ type: Input, args: ['sticky-offset-bottom',] }],
        start: [{ type: Input, args: ['sticky-start',] }],
        stickClass: [{ type: Input, args: ['sticky-class',] }],
        endStickClass: [{ type: Input, args: ['sticky-end-class',] }],
        mediaQuery: [{ type: Input, args: ['sticky-media-query',] }],
        parentMode: [{ type: Input, args: ['sticky-parent',] }],
        orientation: [{ type: Input, args: ['sticky-orientation',] }],
        activated: [{ type: Output }],
        deactivated: [{ type: Output }],
        reset: [{ type: Output }],
        onChange: [{ type: HostListener, args: ['window:scroll', ['$event'],] }, { type: HostListener, args: ['window:resize', ['$event'],] }, { type: HostListener, args: ['window:orientationchange', ['$event'],] }]
    };
    return NgtSticky;
}());
export { NgtSticky };
if (false) {
    /**
     * Makes an element sticky
     *
     *  <sticky>Sticky element</sticky>
     *  <div sticky>Sticky div</div>
     * @type {?}
     */
    NgtSticky.prototype.sticky;
    /**
     * Controls z-index CSS parameter of the sticky element
     * @type {?}
     */
    NgtSticky.prototype.zIndex;
    /**
     * Width of the sticky element
     * @type {?}
     */
    NgtSticky.prototype.width;
    /**
     * Pixels between the top of the page or container and the element
     * @type {?}
     */
    NgtSticky.prototype.offsetTop;
    /**
     *  Pixels between the bottom of the page or container and the element
     * @type {?}
     */
    NgtSticky.prototype.offsetBottom;
    /**
     * Position where the element should start to stick
     * @type {?}
     */
    NgtSticky.prototype.start;
    /**
     * CSS class that will be added after the element starts sticking
     * @type {?}
     */
    NgtSticky.prototype.stickClass;
    /**
     * CSS class that will be added to the sticky element after it ends sticking
     * @type {?}
     */
    NgtSticky.prototype.endStickClass;
    /**
     * Media query that allows to use sticky
     * @type {?}
     */
    NgtSticky.prototype.mediaQuery;
    /**
     * If true, then the sticky element will be stuck relatively to the parent containers. Otherwise, window will be used as the parent container.
     * NOTE: the "position: relative" styling is added to the parent element automatically in order to use the absolute positioning
     * @type {?}
     */
    NgtSticky.prototype.parentMode;
    /**
     * Orientation for sticky element ("left", "right", "none")
     * @type {?}
     */
    NgtSticky.prototype.orientation;
    /** @type {?} */
    NgtSticky.prototype.activated;
    /** @type {?} */
    NgtSticky.prototype.deactivated;
    /** @type {?} */
    NgtSticky.prototype.reset;
    /** @type {?} */
    NgtSticky.prototype.isStuck;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.elem;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.container;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.originalCss;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.windowHeight;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.containerHeight;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.containerWidth;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.containerTop;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.elemHeight;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.containerStart;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.scrollFinish;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.element;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype._renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RpY2t5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJzdGlja3kvc3RpY2t5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0gsU0FBUyxFQUNULFVBQVUsRUFDVixLQUFLLEVBQ0wsTUFBTSxFQUNOLFlBQVksRUFHWixZQUFZLEVBQ1osU0FBUyxFQUNaLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUVsRDtJQXlGSSxtQkFBb0IsT0FBbUIsRUFDbkIsU0FBb0IsRUFDNUIsTUFBdUI7UUFGZixZQUFPLEdBQVAsT0FBTyxDQUFZO1FBQ25CLGNBQVMsR0FBVCxTQUFTLENBQVc7UUExQjlCLGNBQVMsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQy9CLGdCQUFXLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUNqQyxVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUVyQyxZQUFPLEdBQVksS0FBSyxDQUFDO1FBd0JyQixJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUM7UUFDdEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQzVCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUM1QixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQztRQUN4QyxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLGFBQWEsQ0FBQztRQUMxQyxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFDcEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUMsVUFBVSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztJQUMxQyxDQUFDOzs7OztJQW5CRCw0QkFBUTs7OztJQUhSLFVBR1MsTUFBTTtRQUNYLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNuQixDQUFDOzs7O0lBbUJELDRCQUFROzs7SUFBUjtRQUNJLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7SUFDM0MsQ0FBQzs7OztJQUVELG1DQUFlOzs7SUFBZjtRQUNJLDRDQUE0QztRQUM1QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkIsQ0FBQzs7OztJQUVELDRDQUF3Qjs7O0lBQXhCO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRztZQUNmLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDO1lBQzdDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDO1lBQ2pELEdBQUcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDO1lBQ3ZDLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDO1lBQzNDLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDO1lBQ3pDLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDO1lBQzdDLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDO1NBQzlDLENBQUM7SUFDTixDQUFDOzs7O0lBRUQscUNBQWlCOzs7SUFBakI7UUFDSSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUN2QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLGFBQWEsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLEVBQUUsR0FBRyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDaEosSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUN2SDthQUFNO1lBQ0gsSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztTQUNsRDtJQUNMLENBQUM7Ozs7SUFFRCxxQ0FBaUI7OztJQUFqQjtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2pFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUN0QixPQUFPLEVBQUUsT0FBTztZQUNoQixRQUFRLEVBQUUsUUFBUTtZQUNsQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDcEIsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ2xFLENBQUM7Ozs7SUFFRCxnQ0FBWTs7O0lBQVo7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFNUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7Ozs7SUFFRCxnQ0FBWTs7O0lBQVo7UUFDSSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUVwQixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFekMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ3RCLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtZQUNuQixRQUFRLEVBQUUsT0FBTztZQUNqQixHQUFHLEVBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJO1lBQzFCLEtBQUssRUFBRSxNQUFNO1lBQ2IsTUFBTSxFQUFFLE1BQU07WUFDZCxJQUFJLEVBQUUsSUFBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLEdBQUcsSUFBSTtZQUMvRCxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQztTQUM5QyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7OztJQUdELGtDQUFjOzs7SUFBZDtRQUNJLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBRXJCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFNUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLFVBQVUsQ0FBQztRQUUzQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDdEIsUUFBUSxFQUFFLFVBQVU7WUFDcEIsR0FBRyxFQUFFLE1BQU07WUFDWCxJQUFJLEVBQUUsTUFBTTtZQUNaLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLEtBQUssT0FBTyxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU07WUFDcEcsTUFBTSxFQUFFLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSTtZQUNoQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQztTQUM5QyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDckMsQ0FBQzs7OztJQUVELG1DQUFlOzs7SUFBZjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2xCLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxPQUFPLENBQ0gsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPO1lBQ3RELE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FDN0MsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCwyQkFBTzs7O0lBQVA7UUFDSSxvQkFBb0I7UUFDcEIsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxFQUFFO1lBQ3pDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUNwQixPQUFPO1NBQ1Y7OztZQUdLLHNCQUFzQixHQUFXLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUM7O1lBQzVFLHFCQUFxQixHQUFXLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUM7O1lBQzFFLG1CQUFtQixHQUFXLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEtBQUssQ0FBQztRQUMxRixJQUFJLHNCQUFzQixLQUFLLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDakQsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7U0FDNUI7UUFDRCxJQUFJLG1CQUFtQixLQUFLLElBQUksQ0FBQyxZQUFZLEVBQUU7WUFDM0MsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7U0FDNUI7UUFDRCxJQUFJLHFCQUFxQixLQUFLLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDL0MsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7U0FDNUI7UUFFRCxxREFBcUQ7UUFDckQsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLHNCQUFzQixFQUFFO1lBQzNDLE9BQU87U0FDVjs7WUFFSyxRQUFRLEdBQVcsSUFBSSxDQUFDLGFBQWEsRUFBRTtRQUU3QyxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUUsVUFBVTtZQUM5SCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDcEIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDOUIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3pCO1lBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDeEI7YUFBTSxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsY0FBYyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUUsUUFBUTtZQUNqRixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7U0FDdkI7SUFDTCxDQUFDOzs7OztJQUVPLGlDQUFhOzs7O0lBQXJCO1FBQ0ksT0FBTyxNQUFNLENBQUMsV0FBVyxJQUFJLFFBQVEsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDO0lBQ3BFLENBQUM7Ozs7Ozs7SUFFTyw4Q0FBMEI7Ozs7OztJQUFsQyxVQUFtQyxPQUFZLEVBQUUsUUFBZ0I7O1lBQ3pELE1BQU0sR0FBRyxDQUFDO1FBQ2QsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLHFCQUFxQixFQUFFOztnQkFDcEMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRTtZQUM1QyxNQUFNLEdBQUcsQ0FBQyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDekU7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOzs7Ozs7O0lBRU8sK0JBQVc7Ozs7OztJQUFuQixVQUFvQixPQUFZLEVBQUUsUUFBZ0I7O1lBQzFDLE1BQU0sR0FBUSxFQUFFO1FBQ3BCLElBQUksT0FBTyxNQUFNLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxFQUFFO1lBQ2hELE1BQU0sR0FBRyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQzVFO2FBQU0sSUFBSSxPQUFPLE9BQU8sQ0FBQyxZQUFZLEtBQUssV0FBVyxFQUFFO1lBQ3BELE1BQU0sR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQzNDO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7Ozs7OztJQUVPLGdDQUFZOzs7Ozs7SUFBcEIsVUFBcUIsT0FBWSxFQUFFLFFBQWdCO1FBQy9DLElBQUksT0FBTyxPQUFPLEtBQUssV0FBVyxFQUFFO1lBQ2hDLE9BQU8sQ0FBQyxDQUFDO1NBQ1o7UUFDRCxPQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEUsQ0FBQzs7Ozs7OztJQUVPLDZCQUFTOzs7Ozs7SUFBakIsVUFBa0IsT0FBWSxFQUFFLE1BQVc7UUFDdkMsSUFBSSxPQUFPLE1BQU0sS0FBSyxRQUFRLEVBQUU7WUFDNUIsS0FBSyxJQUFNLFFBQVEsSUFBSSxNQUFNLEVBQUU7Z0JBQzNCLElBQUksTUFBTSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDakMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztpQkFDaEU7YUFDSjtTQUNKO0lBQ0wsQ0FBQzs7Z0JBN1JKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsd0JBQXdCO29CQUNsQyxRQUFRLEVBQUUsMkJBQTJCO2lCQUN4Qzs7OztnQkFkRyxVQUFVO2dCQU9WLFNBQVM7Z0JBRUosZUFBZTs7O3lCQWFuQixLQUFLLFNBQUMsUUFBUTt5QkFLZCxLQUFLLFNBQUMsZUFBZTt3QkFLckIsS0FBSyxTQUFDLGNBQWM7NEJBS3BCLEtBQUssU0FBQyxtQkFBbUI7K0JBS3pCLEtBQUssU0FBQyxzQkFBc0I7d0JBSzVCLEtBQUssU0FBQyxjQUFjOzZCQUtwQixLQUFLLFNBQUMsY0FBYztnQ0FLcEIsS0FBSyxTQUFDLGtCQUFrQjs2QkFLeEIsS0FBSyxTQUFDLG9CQUFvQjs2QkFNMUIsS0FBSyxTQUFDLGVBQWU7OEJBS3JCLEtBQUssU0FBQyxvQkFBb0I7NEJBRTFCLE1BQU07OEJBQ04sTUFBTTt3QkFDTixNQUFNOzJCQWdCTixZQUFZLFNBQUMsZUFBZSxFQUFFLENBQUMsUUFBUSxDQUFDLGNBQ3hDLFlBQVksU0FBQyxlQUFlLEVBQUUsQ0FBQyxRQUFRLENBQUMsY0FDeEMsWUFBWSxTQUFDLDBCQUEwQixFQUFFLENBQUMsUUFBUSxDQUFDOztJQTBNeEQsZ0JBQUM7Q0FBQSxBQTlSRCxJQThSQztTQTFSWSxTQUFTOzs7Ozs7Ozs7SUFPbEIsMkJBQWdDOzs7OztJQUtoQywyQkFBdUM7Ozs7O0lBS3ZDLDBCQUFxQzs7Ozs7SUFLckMsOEJBQThDOzs7OztJQUs5QyxpQ0FBb0Q7Ozs7O0lBS3BELDBCQUFxQzs7Ozs7SUFLckMsK0JBQTBDOzs7OztJQUsxQyxrQ0FBaUQ7Ozs7O0lBS2pELCtCQUFnRDs7Ozs7O0lBTWhELCtCQUE0Qzs7Ozs7SUFLNUMsZ0NBQWlEOztJQUVqRCw4QkFBeUM7O0lBQ3pDLGdDQUEyQzs7SUFDM0MsMEJBQXFDOztJQUVyQyw0QkFBeUI7Ozs7O0lBRXpCLHlCQUFrQjs7Ozs7SUFDbEIsOEJBQXVCOzs7OztJQUN2QixnQ0FBeUI7Ozs7O0lBRXpCLGlDQUE2Qjs7Ozs7SUFDN0Isb0NBQWdDOzs7OztJQUNoQyxtQ0FBK0I7Ozs7O0lBQy9CLGlDQUE2Qjs7Ozs7SUFDN0IsK0JBQTJCOzs7OztJQUMzQixtQ0FBK0I7Ozs7O0lBQy9CLGlDQUE2Qjs7Ozs7SUFTakIsNEJBQTJCOzs7OztJQUMzQiw4QkFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgRWxlbWVudFJlZixcclxuICAgIElucHV0LFxyXG4gICAgT3V0cHV0LFxyXG4gICAgRXZlbnRFbWl0dGVyLFxyXG4gICAgT25Jbml0LFxyXG4gICAgQWZ0ZXJWaWV3SW5pdCxcclxuICAgIEhvc3RMaXN0ZW5lcixcclxuICAgIFJlbmRlcmVyMlxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOZ3RTdGlja3lDb25maWcgfSBmcm9tICcuL3N0aWNreS1jb25maWcnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC1zdGlja3ksW25ndFN0aWNreV0nLFxyXG4gICAgdGVtcGxhdGU6ICc8bmctY29udGVudD48L25nLWNvbnRlbnQ+J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0U3RpY2t5IGltcGxlbWVudHMgT25Jbml0LCBBZnRlclZpZXdJbml0IHtcclxuICAgIC8qKlxyXG4gICAgICogTWFrZXMgYW4gZWxlbWVudCBzdGlja3lcclxuICAgICAqXHJcbiAgICAgKiAgPHN0aWNreT5TdGlja3kgZWxlbWVudDwvc3RpY2t5PlxyXG4gICAgICogIDxkaXYgc3RpY2t5PlN0aWNreSBkaXY8L2Rpdj5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCdzdGlja3knKSBzdGlja3k6IHN0cmluZztcclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnRyb2xzIHotaW5kZXggQ1NTIHBhcmFtZXRlciBvZiB0aGUgc3RpY2t5IGVsZW1lbnRcclxuICAgICAqL1xyXG4gICAgQElucHV0KCdzdGlja3ktekluZGV4JykgekluZGV4OiBudW1iZXI7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBXaWR0aCBvZiB0aGUgc3RpY2t5IGVsZW1lbnRcclxuICAgICAqL1xyXG4gICAgQElucHV0KCdzdGlja3ktd2lkdGgnKSB3aWR0aDogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogUGl4ZWxzIGJldHdlZW4gdGhlIHRvcCBvZiB0aGUgcGFnZSBvciBjb250YWluZXIgYW5kIHRoZSBlbGVtZW50XHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgnc3RpY2t5LW9mZnNldC10b3AnKSBvZmZzZXRUb3A6IG51bWJlcjtcclxuXHJcbiAgICAvKipcclxuICAgICAqICBQaXhlbHMgYmV0d2VlbiB0aGUgYm90dG9tIG9mIHRoZSBwYWdlIG9yIGNvbnRhaW5lciBhbmQgdGhlIGVsZW1lbnRcclxuICAgICAqL1xyXG4gICAgQElucHV0KCdzdGlja3ktb2Zmc2V0LWJvdHRvbScpIG9mZnNldEJvdHRvbTogbnVtYmVyO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogUG9zaXRpb24gd2hlcmUgdGhlIGVsZW1lbnQgc2hvdWxkIHN0YXJ0IHRvIHN0aWNrXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgnc3RpY2t5LXN0YXJ0Jykgc3RhcnQ6IG51bWJlcjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENTUyBjbGFzcyB0aGF0IHdpbGwgYmUgYWRkZWQgYWZ0ZXIgdGhlIGVsZW1lbnQgc3RhcnRzIHN0aWNraW5nXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgnc3RpY2t5LWNsYXNzJykgc3RpY2tDbGFzczogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ1NTIGNsYXNzIHRoYXQgd2lsbCBiZSBhZGRlZCB0byB0aGUgc3RpY2t5IGVsZW1lbnQgYWZ0ZXIgaXQgZW5kcyBzdGlja2luZ1xyXG4gICAgICovXHJcbiAgICBASW5wdXQoJ3N0aWNreS1lbmQtY2xhc3MnKSBlbmRTdGlja0NsYXNzOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBNZWRpYSBxdWVyeSB0aGF0IGFsbG93cyB0byB1c2Ugc3RpY2t5XHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgnc3RpY2t5LW1lZGlhLXF1ZXJ5JykgbWVkaWFRdWVyeTogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogSWYgdHJ1ZSwgdGhlbiB0aGUgc3RpY2t5IGVsZW1lbnQgd2lsbCBiZSBzdHVjayByZWxhdGl2ZWx5IHRvIHRoZSBwYXJlbnQgY29udGFpbmVycy4gT3RoZXJ3aXNlLCB3aW5kb3cgd2lsbCBiZSB1c2VkIGFzIHRoZSBwYXJlbnQgY29udGFpbmVyLlxyXG4gICAgICogTk9URTogdGhlIFwicG9zaXRpb246IHJlbGF0aXZlXCIgc3R5bGluZyBpcyBhZGRlZCB0byB0aGUgcGFyZW50IGVsZW1lbnQgYXV0b21hdGljYWxseSBpbiBvcmRlciB0byB1c2UgdGhlIGFic29sdXRlIHBvc2l0aW9uaW5nXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgnc3RpY2t5LXBhcmVudCcpIHBhcmVudE1vZGU6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBPcmllbnRhdGlvbiBmb3Igc3RpY2t5IGVsZW1lbnQgKFwibGVmdFwiLCBcInJpZ2h0XCIsIFwibm9uZVwiKVxyXG4gICAgICovXHJcbiAgICBASW5wdXQoJ3N0aWNreS1vcmllbnRhdGlvbicpIG9yaWVudGF0aW9uOiBzdHJpbmc7XHJcblxyXG4gICAgQE91dHB1dCgpIGFjdGl2YXRlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICAgIEBPdXRwdXQoKSBkZWFjdGl2YXRlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICAgIEBPdXRwdXQoKSByZXNldCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgICBpc1N0dWNrOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgcHJpdmF0ZSBlbGVtOiBhbnk7XHJcbiAgICBwcml2YXRlIGNvbnRhaW5lcjogYW55O1xyXG4gICAgcHJpdmF0ZSBvcmlnaW5hbENzczogYW55O1xyXG5cclxuICAgIHByaXZhdGUgd2luZG93SGVpZ2h0OiBudW1iZXI7XHJcbiAgICBwcml2YXRlIGNvbnRhaW5lckhlaWdodDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBjb250YWluZXJXaWR0aDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBjb250YWluZXJUb3A6IG51bWJlcjtcclxuICAgIHByaXZhdGUgZWxlbUhlaWdodDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBjb250YWluZXJTdGFydDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBzY3JvbGxGaW5pc2g6IG51bWJlcjtcclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCd3aW5kb3c6c2Nyb2xsJywgWyckZXZlbnQnXSlcclxuICAgIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpyZXNpemUnLCBbJyRldmVudCddKVxyXG4gICAgQEhvc3RMaXN0ZW5lcignd2luZG93Om9yaWVudGF0aW9uY2hhbmdlJywgWyckZXZlbnQnXSlcclxuICAgIG9uQ2hhbmdlKCRldmVudCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuc3RpY2tlcigpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZWxlbWVudDogRWxlbWVudFJlZixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX3JlbmRlcmVyOiBSZW5kZXJlcjIsXHJcbiAgICAgICAgICAgICAgICBjb25maWc6IE5ndFN0aWNreUNvbmZpZykge1xyXG4gICAgICAgIHRoaXMub3JpZW50YXRpb24gPSBjb25maWcub3JpZW50YXRpb247XHJcbiAgICAgICAgdGhpcy5zdGlja3kgPSBjb25maWcuc3RpY2t5O1xyXG4gICAgICAgIHRoaXMuekluZGV4ID0gY29uZmlnLnpJbmRleDtcclxuICAgICAgICB0aGlzLndpZHRoID0gY29uZmlnLndpZHRoO1xyXG4gICAgICAgIHRoaXMub2Zmc2V0VG9wID0gY29uZmlnLm9mZnNldFRvcDtcclxuICAgICAgICB0aGlzLm9mZnNldEJvdHRvbSA9IGNvbmZpZy5vZmZzZXRCb3R0b207XHJcbiAgICAgICAgdGhpcy5zdGFydCA9IGNvbmZpZy5zdGFydDtcclxuICAgICAgICB0aGlzLnN0aWNrQ2xhc3MgPSBjb25maWcuc3RpY2tDbGFzcztcclxuICAgICAgICB0aGlzLmVuZFN0aWNrQ2xhc3MgPSBjb25maWcuZW5kU3RpY2tDbGFzcztcclxuICAgICAgICB0aGlzLm1lZGlhUXVlcnkgPSBjb25maWcubWVkaWFRdWVyeTtcclxuICAgICAgICB0aGlzLnBhcmVudE1vZGUgPSBjb25maWcucGFyZW50TW9kZTtcclxuICAgICAgICB0aGlzLm9yaWVudGF0aW9uID0gY29uZmlnLm9yaWVudGF0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZWxlbSA9IHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIG5nQWZ0ZXJWaWV3SW5pdCgpOiB2b2lkIHtcclxuICAgICAgICAvLyBkZWZpbmUgc2Nyb2xsIGNvbnRhaW5lciBhcyBwYXJlbnQgZWxlbWVudFxyXG4gICAgICAgIHRoaXMuY29udGFpbmVyID0gdGhpcy5fcmVuZGVyZXIucGFyZW50Tm9kZSh0aGlzLmVsZW0pO1xyXG4gICAgICAgIHRoaXMuZGVmaW5lT3JpZ2luYWxEaW1lbnNpb25zKCk7XHJcbiAgICAgICAgdGhpcy5zdGlja2VyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZGVmaW5lT3JpZ2luYWxEaW1lbnNpb25zKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMub3JpZ2luYWxDc3MgPSB7XHJcbiAgICAgICAgICAgIHpJbmRleDogdGhpcy5nZXRDc3NWYWx1ZSh0aGlzLmVsZW0sICd6SW5kZXgnKSxcclxuICAgICAgICAgICAgcG9zaXRpb246IHRoaXMuZ2V0Q3NzVmFsdWUodGhpcy5lbGVtLCAncG9zaXRpb24nKSxcclxuICAgICAgICAgICAgdG9wOiB0aGlzLmdldENzc1ZhbHVlKHRoaXMuZWxlbSwgJ3RvcCcpLFxyXG4gICAgICAgICAgICByaWdodDogdGhpcy5nZXRDc3NWYWx1ZSh0aGlzLmVsZW0sICdyaWdodCcpLFxyXG4gICAgICAgICAgICBsZWZ0OiB0aGlzLmdldENzc1ZhbHVlKHRoaXMuZWxlbSwgJ2xlZnQnKSxcclxuICAgICAgICAgICAgYm90dG9tOiB0aGlzLmdldENzc1ZhbHVlKHRoaXMuZWxlbSwgJ2JvdHRvbScpLFxyXG4gICAgICAgICAgICB3aWR0aDogdGhpcy5nZXRDc3NWYWx1ZSh0aGlzLmVsZW0sICd3aWR0aCcpLFxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgZGVmaW5lWURpbWVuc2lvbnMoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXJUb3AgPSB0aGlzLmdldEJvdW5kaW5nQ2xpZW50UmVjdFZhbHVlKHRoaXMuY29udGFpbmVyLCAndG9wJyk7XHJcbiAgICAgICAgdGhpcy53aW5kb3dIZWlnaHQgPSB3aW5kb3cuaW5uZXJIZWlnaHQ7XHJcbiAgICAgICAgdGhpcy5lbGVtSGVpZ2h0ID0gdGhpcy5nZXRDc3NOdW1iZXIodGhpcy5lbGVtLCAnaGVpZ2h0Jyk7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXJIZWlnaHQgPSB0aGlzLmdldENzc051bWJlcih0aGlzLmNvbnRhaW5lciwgJ2hlaWdodCcpO1xyXG4gICAgICAgIHRoaXMuY29udGFpbmVyU3RhcnQgPSB0aGlzLmNvbnRhaW5lclRvcCArIHRoaXMuZ2V0Q3NzTnVtYmVyKHRoaXMuY29udGFpbmVyLCAncGFkZGluZy10b3AnKSArIHRoaXMuc2Nyb2xsYmFyWVBvcygpIC0gdGhpcy5vZmZzZXRUb3AgKyB0aGlzLnN0YXJ0O1xyXG4gICAgICAgIGlmICh0aGlzLnBhcmVudE1vZGUpIHtcclxuICAgICAgICAgICAgdGhpcy5zY3JvbGxGaW5pc2ggPSB0aGlzLmNvbnRhaW5lclN0YXJ0IC0gdGhpcy5zdGFydCAtIHRoaXMub2Zmc2V0Qm90dG9tICsgKHRoaXMuY29udGFpbmVySGVpZ2h0IC0gdGhpcy5lbGVtSGVpZ2h0KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnNjcm9sbEZpbmlzaCA9IGRvY3VtZW50LmJvZHkub2Zmc2V0SGVpZ2h0O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkZWZpbmVYRGltZW5zaW9ucygpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lcldpZHRoID0gdGhpcy5nZXRDc3NOdW1iZXIodGhpcy5jb250YWluZXIsICd3aWR0aCcpO1xyXG4gICAgICAgIHRoaXMuc2V0U3R5bGVzKHRoaXMuZWxlbSwge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiAnYmxvY2snLFxyXG4gICAgICAgICAgICBwb3NpdGlvbjogJ3N0YXRpYycsXHJcbiAgICAgICAgICAgIHdpZHRoOiB0aGlzLndpZHRoXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5vcmlnaW5hbENzcy53aWR0aCA9IHRoaXMuZ2V0Q3NzVmFsdWUodGhpcy5lbGVtLCAnd2lkdGgnKTtcclxuICAgIH1cclxuXHJcbiAgICByZXNldEVsZW1lbnQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5lbGVtLmNsYXNzTGlzdC5yZW1vdmUodGhpcy5zdGlja0NsYXNzKTtcclxuICAgICAgICB0aGlzLnNldFN0eWxlcyh0aGlzLmVsZW0sIHRoaXMub3JpZ2luYWxDc3MpO1xyXG5cclxuICAgICAgICB0aGlzLnJlc2V0Lm5leHQodGhpcy5lbGVtKTtcclxuICAgIH1cclxuXHJcbiAgICBzdHVja0VsZW1lbnQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5pc1N0dWNrID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgdGhpcy5lbGVtLmNsYXNzTGlzdC5yZW1vdmUodGhpcy5lbmRTdGlja0NsYXNzKTtcclxuICAgICAgICB0aGlzLmVsZW0uY2xhc3NMaXN0LmFkZCh0aGlzLnN0aWNrQ2xhc3MpO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0eWxlcyh0aGlzLmVsZW0sIHtcclxuICAgICAgICAgICAgekluZGV4OiB0aGlzLnpJbmRleCxcclxuICAgICAgICAgICAgcG9zaXRpb246ICdmaXhlZCcsXHJcbiAgICAgICAgICAgIHRvcDogdGhpcy5vZmZzZXRUb3AgKyAncHgnLFxyXG4gICAgICAgICAgICByaWdodDogJ2F1dG8nLFxyXG4gICAgICAgICAgICBib3R0b206ICdhdXRvJyxcclxuICAgICAgICAgICAgbGVmdDogdGhpcy5nZXRCb3VuZGluZ0NsaWVudFJlY3RWYWx1ZSh0aGlzLmVsZW0sICdsZWZ0JykgKyAncHgnLFxyXG4gICAgICAgICAgICB3aWR0aDogdGhpcy5nZXRDc3NWYWx1ZSh0aGlzLmVsZW0sICd3aWR0aCcpXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuYWN0aXZhdGVkLm5leHQodGhpcy5lbGVtKTtcclxuICAgIH1cclxuXHJcblxyXG4gICAgdW5zdHVja0VsZW1lbnQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5pc1N0dWNrID0gZmFsc2U7XHJcblxyXG4gICAgICAgIHRoaXMuZWxlbS5jbGFzc0xpc3QuYWRkKHRoaXMuZW5kU3RpY2tDbGFzcyk7XHJcblxyXG4gICAgICAgIHRoaXMuY29udGFpbmVyLnN0eWxlLnBvc2l0aW9uID0gJ3JlbGF0aXZlJztcclxuXHJcbiAgICAgICAgdGhpcy5zZXRTdHlsZXModGhpcy5lbGVtLCB7XHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiAnYWJzb2x1dGUnLFxyXG4gICAgICAgICAgICB0b3A6ICdhdXRvJyxcclxuICAgICAgICAgICAgbGVmdDogJ2F1dG8nLFxyXG4gICAgICAgICAgICByaWdodDogdGhpcy5nZXRDc3NWYWx1ZSh0aGlzLmVsZW0sICdmbG9hdCcpID09PSAncmlnaHQnIHx8IHRoaXMub3JpZW50YXRpb24gPT09ICdyaWdodCcgPyAwIDogJ2F1dG8nLFxyXG4gICAgICAgICAgICBib3R0b206IHRoaXMub2Zmc2V0Qm90dG9tICsgJ3B4JyxcclxuICAgICAgICAgICAgd2lkdGg6IHRoaXMuZ2V0Q3NzVmFsdWUodGhpcy5lbGVtLCAnd2lkdGgnKVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmRlYWN0aXZhdGVkLm5leHQodGhpcy5lbGVtKTtcclxuICAgIH1cclxuXHJcbiAgICBtYXRjaE1lZGlhUXVlcnkoKTogYW55IHtcclxuICAgICAgICBpZiAoIXRoaXMubWVkaWFRdWVyeSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgd2luZG93Lm1hdGNoTWVkaWEoJygnICsgdGhpcy5tZWRpYVF1ZXJ5ICsgJyknKS5tYXRjaGVzIHx8XHJcbiAgICAgICAgICAgIHdpbmRvdy5tYXRjaE1lZGlhKHRoaXMubWVkaWFRdWVyeSkubWF0Y2hlc1xyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RpY2tlcigpOiB2b2lkIHtcclxuICAgICAgICAvLyBjaGVjayBtZWRpYSBxdWVyeVxyXG4gICAgICAgIGlmICh0aGlzLmlzU3R1Y2sgJiYgIXRoaXMubWF0Y2hNZWRpYVF1ZXJ5KCkpIHtcclxuICAgICAgICAgICAgdGhpcy5yZXNldEVsZW1lbnQoKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gZGV0ZWN0aW5nIHdoZW4gYSBjb250YWluZXIncyBoZWlnaHQsIHdpZHRoIG9yIHRvcCBwb3NpdGlvbiBjaGFuZ2VzXHJcbiAgICAgICAgY29uc3QgY3VycmVudENvbnRhaW5lckhlaWdodDogbnVtYmVyID0gdGhpcy5nZXRDc3NOdW1iZXIodGhpcy5jb250YWluZXIsICdoZWlnaHQnKTtcclxuICAgICAgICBjb25zdCBjdXJyZW50Q29udGFpbmVyV2lkdGg6IG51bWJlciA9IHRoaXMuZ2V0Q3NzTnVtYmVyKHRoaXMuY29udGFpbmVyLCAnd2lkdGgnKTtcclxuICAgICAgICBjb25zdCBjdXJyZW50Q29udGFpbmVyVG9wOiBudW1iZXIgPSB0aGlzLmdldEJvdW5kaW5nQ2xpZW50UmVjdFZhbHVlKHRoaXMuY29udGFpbmVyLCAndG9wJyk7XHJcbiAgICAgICAgaWYgKGN1cnJlbnRDb250YWluZXJIZWlnaHQgIT09IHRoaXMuY29udGFpbmVySGVpZ2h0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVmaW5lWURpbWVuc2lvbnMoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGN1cnJlbnRDb250YWluZXJUb3AgIT09IHRoaXMuY29udGFpbmVyVG9wKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVmaW5lWURpbWVuc2lvbnMoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGN1cnJlbnRDb250YWluZXJXaWR0aCAhPT0gdGhpcy5jb250YWluZXJXaWR0aCkge1xyXG4gICAgICAgICAgICB0aGlzLmRlZmluZVhEaW1lbnNpb25zKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBjaGVjayBpZiB0aGUgc3RpY2t5IGVsZW1lbnQgaXMgYWJvdmUgdGhlIGNvbnRhaW5lclxyXG4gICAgICAgIGlmICh0aGlzLmVsZW1IZWlnaHQgPj0gY3VycmVudENvbnRhaW5lckhlaWdodCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBwb3NpdGlvbjogbnVtYmVyID0gdGhpcy5zY3JvbGxiYXJZUG9zKCk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmlzU3R1Y2sgJiYgKHBvc2l0aW9uIDwgdGhpcy5jb250YWluZXJTdGFydCB8fCBwb3NpdGlvbiA+IHRoaXMuc2Nyb2xsRmluaXNoKSB8fCBwb3NpdGlvbiA+IHRoaXMuc2Nyb2xsRmluaXNoKSB7IC8vIHVuc3RpY2tcclxuICAgICAgICAgICAgdGhpcy5yZXNldEVsZW1lbnQoKTtcclxuICAgICAgICAgICAgaWYgKHBvc2l0aW9uID4gdGhpcy5zY3JvbGxGaW5pc2gpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudW5zdHVja0VsZW1lbnQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmlzU3R1Y2sgPSBmYWxzZTtcclxuICAgICAgICB9IGVsc2UgaWYgKHBvc2l0aW9uID4gdGhpcy5jb250YWluZXJTdGFydCAmJiBwb3NpdGlvbiA8IHRoaXMuc2Nyb2xsRmluaXNoKSB7IC8vIHN0aWNrXHJcbiAgICAgICAgICAgIHRoaXMuc3R1Y2tFbGVtZW50KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc2Nyb2xsYmFyWVBvcygpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB3aW5kb3cucGFnZVlPZmZzZXQgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldEJvdW5kaW5nQ2xpZW50UmVjdFZhbHVlKGVsZW1lbnQ6IGFueSwgcHJvcGVydHk6IHN0cmluZyk6IG51bWJlciB7XHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IDA7XHJcbiAgICAgICAgaWYgKGVsZW1lbnQgJiYgZWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QpIHtcclxuICAgICAgICAgICAgY29uc3QgcmVjdCA9IGVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XHJcbiAgICAgICAgICAgIHJlc3VsdCA9ICh0eXBlb2YgcmVjdFtwcm9wZXJ0eV0gIT09ICd1bmRlZmluZWQnKSA/IHJlY3RbcHJvcGVydHldIDogMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldENzc1ZhbHVlKGVsZW1lbnQ6IGFueSwgcHJvcGVydHk6IHN0cmluZyk6IGFueSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdDogYW55ID0gJyc7XHJcbiAgICAgICAgaWYgKHR5cGVvZiB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZSAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgcmVzdWx0ID0gd2luZG93LmdldENvbXB1dGVkU3R5bGUoZWxlbWVudCwgJycpLmdldFByb3BlcnR5VmFsdWUocHJvcGVydHkpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodHlwZW9mIGVsZW1lbnQuY3VycmVudFN0eWxlICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICByZXN1bHQgPSBlbGVtZW50LmN1cnJlbnRTdHlsZVtwcm9wZXJ0eV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRDc3NOdW1iZXIoZWxlbWVudDogYW55LCBwcm9wZXJ0eTogc3RyaW5nKTogbnVtYmVyIHtcclxuICAgICAgICBpZiAodHlwZW9mIGVsZW1lbnQgPT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcGFyc2VJbnQodGhpcy5nZXRDc3NWYWx1ZShlbGVtZW50LCBwcm9wZXJ0eSksIDEwKSB8fCAwO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc2V0U3R5bGVzKGVsZW1lbnQ6IGFueSwgc3R5bGVzOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBpZiAodHlwZW9mIHN0eWxlcyA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBwcm9wZXJ0eSBpbiBzdHlsZXMpIHtcclxuICAgICAgICAgICAgICAgIGlmIChzdHlsZXMuaGFzT3duUHJvcGVydHkocHJvcGVydHkpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUoZWxlbWVudCwgcHJvcGVydHksIHN0eWxlc1twcm9wZXJ0eV0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==