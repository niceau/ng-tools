/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { merge, Observable } from 'rxjs';
import { delay, filter, map, share } from 'rxjs/operators';
var Trigger = /** @class */ (function () {
    function Trigger(open, close) {
        this.open = open;
        this.close = close;
        if (!close) {
            this.close = open;
        }
    }
    /**
     * @return {?}
     */
    Trigger.prototype.isManual = /**
     * @return {?}
     */
    function () {
        return this.open === 'manual' || this.close === 'manual';
    };
    return Trigger;
}());
export { Trigger };
if (false) {
    /** @type {?} */
    Trigger.prototype.open;
    /** @type {?} */
    Trigger.prototype.close;
}
/** @type {?} */
var DEFAULT_ALIASES = {
    'hover': ['mouseenter', 'mouseleave'],
    'focus': ['focusin', 'focusout'],
};
/**
 * @param {?} triggers
 * @param {?=} aliases
 * @return {?}
 */
export function parseTriggers(triggers, aliases) {
    if (aliases === void 0) { aliases = DEFAULT_ALIASES; }
    /** @type {?} */
    var trimmedTriggers = (triggers || '').trim();
    if (trimmedTriggers.length === 0) {
        return [];
    }
    /** @type {?} */
    var parsedTriggers = trimmedTriggers.split(/\s+/).map((/**
     * @param {?} trigger
     * @return {?}
     */
    function (trigger) { return trigger.split(':'); })).map((/**
     * @param {?} triggerPair
     * @return {?}
     */
    function (triggerPair) {
        /** @type {?} */
        var alias = aliases[triggerPair[0]] || triggerPair;
        return new Trigger(alias[0], alias[1]);
    }));
    /** @type {?} */
    var manualTriggers = parsedTriggers.filter((/**
     * @param {?} triggerPair
     * @return {?}
     */
    function (triggerPair) { return triggerPair.isManual(); }));
    if (manualTriggers.length > 1) {
        throw 'Triggers parse error: only one manual trigger is allowed';
    }
    if (manualTriggers.length === 1 && parsedTriggers.length > 1) {
        throw 'Triggers parse error: manual trigger can\'t be mixed with other triggers';
    }
    return parsedTriggers;
}
/**
 * @param {?} renderer
 * @param {?} nativeElement
 * @param {?} triggers
 * @param {?} isOpenedFn
 * @return {?}
 */
export function observeTriggers(renderer, nativeElement, triggers, isOpenedFn) {
    return new Observable((/**
     * @param {?} subscriber
     * @return {?}
     */
    function (subscriber) {
        /** @type {?} */
        var listeners = [];
        /** @type {?} */
        var openFn = (/**
         * @return {?}
         */
        function () { return subscriber.next(true); });
        /** @type {?} */
        var closeFn = (/**
         * @return {?}
         */
        function () { return subscriber.next(false); });
        /** @type {?} */
        var toggleFn = (/**
         * @return {?}
         */
        function () { return subscriber.next(!isOpenedFn()); });
        triggers.forEach((/**
         * @param {?} trigger
         * @return {?}
         */
        function (trigger) {
            if (trigger.open === trigger.close) {
                listeners.push(renderer.listen(nativeElement, trigger.open, toggleFn));
            }
            else {
                listeners.push(renderer.listen(nativeElement, trigger.open, openFn), renderer.listen(nativeElement, trigger.close, closeFn));
            }
        }));
        return (/**
         * @return {?}
         */
        function () {
            listeners.forEach((/**
             * @param {?} unsubscribeFn
             * @return {?}
             */
            function (unsubscribeFn) { return unsubscribeFn(); }));
        });
    }));
}
/** @type {?} */
var delayOrNoop = (/**
 * @template T
 * @param {?} time
 * @return {?}
 */
function (time) { return time > 0 ? delay(time) : (/**
 * @param {?} a
 * @return {?}
 */
function (a) { return a; }); });
var ɵ0 = delayOrNoop;
/**
 * @param {?} openDelay
 * @param {?} closeDelay
 * @param {?} isOpenedFn
 * @return {?}
 */
export function triggerDelay(openDelay, closeDelay, isOpenedFn) {
    return (/**
     * @param {?} input$
     * @return {?}
     */
    function (input$) {
        /** @type {?} */
        var pending = null;
        /** @type {?} */
        var filteredInput$ = input$.pipe(map((/**
         * @param {?} open
         * @return {?}
         */
        function (open) { return ({ open: open }); })), filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            /** @type {?} */
            var currentlyOpen = isOpenedFn();
            if (currentlyOpen !== event.open && (!pending || pending.open === currentlyOpen)) {
                pending = event;
                return true;
            }
            if (pending && pending.open !== event.open) {
                pending = null;
            }
            return false;
        })), share());
        /** @type {?} */
        var delayedOpen$ = filteredInput$.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) { return event.open; })), delayOrNoop(openDelay));
        /** @type {?} */
        var delayedClose$ = filteredInput$.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) { return !event.open; })), delayOrNoop(closeDelay));
        return merge(delayedOpen$, delayedClose$)
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        function (event) {
            if (event === pending) {
                pending = null;
                return event.open !== isOpenedFn();
            }
            return false;
        })), map((/**
         * @param {?} event
         * @return {?}
         */
        function (event) { return event.open; })));
    });
}
/**
 * @param {?} renderer
 * @param {?} nativeElement
 * @param {?} triggers
 * @param {?} isOpenedFn
 * @param {?} openFn
 * @param {?} closeFn
 * @param {?=} openDelay
 * @param {?=} closeDelay
 * @return {?}
 */
export function listenToTriggers(renderer, nativeElement, triggers, isOpenedFn, openFn, closeFn, openDelay, closeDelay) {
    if (openDelay === void 0) { openDelay = 0; }
    if (closeDelay === void 0) { closeDelay = 0; }
    /** @type {?} */
    var parsedTriggers = parseTriggers(triggers);
    if (parsedTriggers.length === 1 && parsedTriggers[0].isManual()) {
        return (/**
         * @return {?}
         */
        function () {
        });
    }
    /** @type {?} */
    var subscription = observeTriggers(renderer, nativeElement, parsedTriggers, isOpenedFn)
        .pipe(triggerDelay(openDelay, closeDelay, isOpenedFn))
        .subscribe((/**
     * @param {?} open
     * @return {?}
     */
    function (open) { return (open ? openFn() : closeFn()); }));
    return (/**
     * @return {?}
     */
    function () { return subscription.unsubscribe(); });
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJpZ2dlcnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInV0aWwvdHJpZ2dlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUUzRDtJQUNJLGlCQUFtQixJQUFZLEVBQVMsS0FBYztRQUFuQyxTQUFJLEdBQUosSUFBSSxDQUFRO1FBQVMsVUFBSyxHQUFMLEtBQUssQ0FBUztRQUNsRCxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1IsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7U0FDckI7SUFDTCxDQUFDOzs7O0lBRUQsMEJBQVE7OztJQUFSO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxLQUFLLFFBQVEsSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLFFBQVEsQ0FBQztJQUM3RCxDQUFDO0lBQ0wsY0FBQztBQUFELENBQUMsQUFWRCxJQVVDOzs7O0lBVGUsdUJBQW1COztJQUFFLHdCQUFxQjs7O0lBV3BELGVBQWUsR0FBRztJQUNwQixPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDO0lBQ3JDLE9BQU8sRUFBRSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUM7Q0FDbkM7Ozs7OztBQUVELE1BQU0sVUFBVSxhQUFhLENBQUMsUUFBZ0IsRUFBRSxPQUF5QjtJQUF6Qix3QkFBQSxFQUFBLHlCQUF5Qjs7UUFDL0QsZUFBZSxHQUFHLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRTtJQUUvQyxJQUFJLGVBQWUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1FBQzlCLE9BQU8sRUFBRSxDQUFDO0tBQ2I7O1FBRUssY0FBYyxHQUFHLGVBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRzs7OztJQUFDLFVBQUEsT0FBTyxJQUFJLE9BQUEsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBbEIsQ0FBa0IsRUFBQyxDQUFDLEdBQUc7Ozs7SUFBQyxVQUFDLFdBQVc7O1lBQy9GLEtBQUssR0FBRyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksV0FBVztRQUNsRCxPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUMzQyxDQUFDLEVBQUM7O1FBRUksY0FBYyxHQUFHLGNBQWMsQ0FBQyxNQUFNOzs7O0lBQUMsVUFBQSxXQUFXLElBQUksT0FBQSxXQUFXLENBQUMsUUFBUSxFQUFFLEVBQXRCLENBQXNCLEVBQUM7SUFFbkYsSUFBSSxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtRQUMzQixNQUFNLDBEQUEwRCxDQUFDO0tBQ3BFO0lBRUQsSUFBSSxjQUFjLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxjQUFjLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtRQUMxRCxNQUFNLDBFQUEwRSxDQUFDO0tBQ3BGO0lBRUQsT0FBTyxjQUFjLENBQUM7QUFDMUIsQ0FBQzs7Ozs7Ozs7QUFFRCxNQUFNLFVBQVUsZUFBZSxDQUFDLFFBQWEsRUFBRSxhQUFrQixFQUFFLFFBQW1CLEVBQUUsVUFBeUI7SUFDN0csT0FBTyxJQUFJLFVBQVU7Ozs7SUFBVSxVQUFBLFVBQVU7O1lBQy9CLFNBQVMsR0FBRyxFQUFFOztZQUNkLE1BQU07OztRQUFHLGNBQU0sT0FBQSxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFyQixDQUFxQixDQUFBOztZQUNwQyxPQUFPOzs7UUFBRyxjQUFNLE9BQUEsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBdEIsQ0FBc0IsQ0FBQTs7WUFDdEMsUUFBUTs7O1FBQUcsY0FBTSxPQUFBLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxFQUE5QixDQUE4QixDQUFBO1FBRXJELFFBQVEsQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQyxPQUFnQjtZQUM5QixJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssT0FBTyxDQUFDLEtBQUssRUFBRTtnQkFDaEMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUM7YUFDMUU7aUJBQU07Z0JBQ0gsU0FBUyxDQUFDLElBQUksQ0FDVixRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxFQUNwRCxRQUFRLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUMsS0FBSyxFQUFFLE9BQU8sQ0FBQyxDQUFDLENBQUM7YUFDL0Q7UUFDTCxDQUFDLEVBQUMsQ0FBQztRQUVIOzs7UUFBTztZQUNILFNBQVMsQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQSxhQUFhLElBQUksT0FBQSxhQUFhLEVBQUUsRUFBZixDQUFlLEVBQUMsQ0FBQztRQUN4RCxDQUFDLEVBQUM7SUFDTixDQUFDLEVBQUMsQ0FBQztBQUNQLENBQUM7O0lBRUssV0FBVzs7Ozs7QUFBRyxVQUFJLElBQVksSUFBSyxPQUFBLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBSSxJQUFJLENBQUMsQ0FBQyxDQUFDOzs7O0FBQUMsVUFBQyxDQUFnQixJQUFLLE9BQUEsQ0FBQyxFQUFELENBQUMsQ0FBQSxFQUFuRCxDQUFtRCxDQUFBOzs7Ozs7OztBQUU1RixNQUFNLFVBQVUsWUFBWSxDQUFDLFNBQWlCLEVBQUUsVUFBa0IsRUFBRSxVQUF5QjtJQUN6Rjs7OztJQUFPLFVBQUMsTUFBMkI7O1lBQzNCLE9BQU8sR0FBRyxJQUFJOztZQUNaLGNBQWMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUM5QixHQUFHOzs7O1FBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxDQUFDLEVBQUMsSUFBSSxNQUFBLEVBQUMsQ0FBQyxFQUFSLENBQVEsRUFBQyxFQUFFLE1BQU07Ozs7UUFBQyxVQUFBLEtBQUs7O2dCQUN6QixhQUFhLEdBQUcsVUFBVSxFQUFFO1lBQ2xDLElBQUksYUFBYSxLQUFLLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxDQUFDLE9BQU8sSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLGFBQWEsQ0FBQyxFQUFFO2dCQUM5RSxPQUFPLEdBQUcsS0FBSyxDQUFDO2dCQUNoQixPQUFPLElBQUksQ0FBQzthQUNmO1lBQ0QsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxLQUFLLENBQUMsSUFBSSxFQUFFO2dCQUN4QyxPQUFPLEdBQUcsSUFBSSxDQUFDO2FBQ2xCO1lBQ0QsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQyxFQUFDLEVBQ0YsS0FBSyxFQUFFLENBQUM7O1lBQ04sWUFBWSxHQUFHLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxDQUFDLElBQUksRUFBVixDQUFVLEVBQUMsRUFBRSxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7O1lBQ3ZGLGFBQWEsR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLEtBQUssSUFBSSxPQUFBLENBQUMsS0FBSyxDQUFDLElBQUksRUFBWCxDQUFXLEVBQUMsRUFBRSxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDaEcsT0FBTyxLQUFLLENBQUMsWUFBWSxFQUFFLGFBQWEsQ0FBQzthQUNwQyxJQUFJLENBQ0QsTUFBTTs7OztRQUFDLFVBQUEsS0FBSztZQUNSLElBQUksS0FBSyxLQUFLLE9BQU8sRUFBRTtnQkFDbkIsT0FBTyxHQUFHLElBQUksQ0FBQztnQkFDZixPQUFPLEtBQUssQ0FBQyxJQUFJLEtBQUssVUFBVSxFQUFFLENBQUM7YUFDdEM7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDLEVBQUMsRUFDRixHQUFHOzs7O1FBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLENBQUMsSUFBSSxFQUFWLENBQVUsRUFBQyxDQUFDLENBQUM7SUFDdEMsQ0FBQyxFQUFDO0FBQ04sQ0FBQzs7Ozs7Ozs7Ozs7O0FBRUQsTUFBTSxVQUFVLGdCQUFnQixDQUM1QixRQUFhLEVBQUUsYUFBa0IsRUFBRSxRQUFnQixFQUFFLFVBQXlCLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxTQUFhLEVBQzlHLFVBQWM7SUFEbUYsMEJBQUEsRUFBQSxhQUFhO0lBQzlHLDJCQUFBLEVBQUEsY0FBYzs7UUFDUixjQUFjLEdBQUcsYUFBYSxDQUFDLFFBQVEsQ0FBQztJQUU5QyxJQUFJLGNBQWMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtRQUM3RDs7O1FBQU87UUFDUCxDQUFDLEVBQUM7S0FDTDs7UUFFSyxZQUFZLEdBQUcsZUFBZSxDQUFDLFFBQVEsRUFBRSxhQUFhLEVBQUUsY0FBYyxFQUFFLFVBQVUsQ0FBQztTQUNwRixJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7U0FDckQsU0FBUzs7OztJQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQyxFQUE3QixDQUE2QixFQUFDO0lBRXJEOzs7SUFBTyxjQUFNLE9BQUEsWUFBWSxDQUFDLFdBQVcsRUFBRSxFQUExQixDQUEwQixFQUFDO0FBQzVDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBtZXJnZSwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBkZWxheSwgZmlsdGVyLCBtYXAsIHNoYXJlIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFRyaWdnZXIge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIG9wZW46IHN0cmluZywgcHVibGljIGNsb3NlPzogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKCFjbG9zZSkge1xyXG4gICAgICAgICAgICB0aGlzLmNsb3NlID0gb3BlbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaXNNYW51YWwoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub3BlbiA9PT0gJ21hbnVhbCcgfHwgdGhpcy5jbG9zZSA9PT0gJ21hbnVhbCc7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNvbnN0IERFRkFVTFRfQUxJQVNFUyA9IHtcclxuICAgICdob3Zlcic6IFsnbW91c2VlbnRlcicsICdtb3VzZWxlYXZlJ10sXHJcbiAgICAnZm9jdXMnOiBbJ2ZvY3VzaW4nLCAnZm9jdXNvdXQnXSxcclxufTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBwYXJzZVRyaWdnZXJzKHRyaWdnZXJzOiBzdHJpbmcsIGFsaWFzZXMgPSBERUZBVUxUX0FMSUFTRVMpOiBUcmlnZ2VyW10ge1xyXG4gICAgY29uc3QgdHJpbW1lZFRyaWdnZXJzID0gKHRyaWdnZXJzIHx8ICcnKS50cmltKCk7XHJcblxyXG4gICAgaWYgKHRyaW1tZWRUcmlnZ2Vycy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICByZXR1cm4gW107XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgcGFyc2VkVHJpZ2dlcnMgPSB0cmltbWVkVHJpZ2dlcnMuc3BsaXQoL1xccysvKS5tYXAodHJpZ2dlciA9PiB0cmlnZ2VyLnNwbGl0KCc6JykpLm1hcCgodHJpZ2dlclBhaXIpID0+IHtcclxuICAgICAgICBsZXQgYWxpYXMgPSBhbGlhc2VzW3RyaWdnZXJQYWlyWzBdXSB8fCB0cmlnZ2VyUGFpcjtcclxuICAgICAgICByZXR1cm4gbmV3IFRyaWdnZXIoYWxpYXNbMF0sIGFsaWFzWzFdKTtcclxuICAgIH0pO1xyXG5cclxuICAgIGNvbnN0IG1hbnVhbFRyaWdnZXJzID0gcGFyc2VkVHJpZ2dlcnMuZmlsdGVyKHRyaWdnZXJQYWlyID0+IHRyaWdnZXJQYWlyLmlzTWFudWFsKCkpO1xyXG5cclxuICAgIGlmIChtYW51YWxUcmlnZ2Vycy5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgdGhyb3cgJ1RyaWdnZXJzIHBhcnNlIGVycm9yOiBvbmx5IG9uZSBtYW51YWwgdHJpZ2dlciBpcyBhbGxvd2VkJztcclxuICAgIH1cclxuXHJcbiAgICBpZiAobWFudWFsVHJpZ2dlcnMubGVuZ3RoID09PSAxICYmIHBhcnNlZFRyaWdnZXJzLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICB0aHJvdyAnVHJpZ2dlcnMgcGFyc2UgZXJyb3I6IG1hbnVhbCB0cmlnZ2VyIGNhblxcJ3QgYmUgbWl4ZWQgd2l0aCBvdGhlciB0cmlnZ2Vycyc7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHBhcnNlZFRyaWdnZXJzO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gb2JzZXJ2ZVRyaWdnZXJzKHJlbmRlcmVyOiBhbnksIG5hdGl2ZUVsZW1lbnQ6IGFueSwgdHJpZ2dlcnM6IFRyaWdnZXJbXSwgaXNPcGVuZWRGbjogKCkgPT4gYm9vbGVhbikge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlPGJvb2xlYW4+KHN1YnNjcmliZXIgPT4ge1xyXG4gICAgICAgIGNvbnN0IGxpc3RlbmVycyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IG9wZW5GbiA9ICgpID0+IHN1YnNjcmliZXIubmV4dCh0cnVlKTtcclxuICAgICAgICBjb25zdCBjbG9zZUZuID0gKCkgPT4gc3Vic2NyaWJlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICBjb25zdCB0b2dnbGVGbiA9ICgpID0+IHN1YnNjcmliZXIubmV4dCghaXNPcGVuZWRGbigpKTtcclxuXHJcbiAgICAgICAgdHJpZ2dlcnMuZm9yRWFjaCgodHJpZ2dlcjogVHJpZ2dlcikgPT4ge1xyXG4gICAgICAgICAgICBpZiAodHJpZ2dlci5vcGVuID09PSB0cmlnZ2VyLmNsb3NlKSB7XHJcbiAgICAgICAgICAgICAgICBsaXN0ZW5lcnMucHVzaChyZW5kZXJlci5saXN0ZW4obmF0aXZlRWxlbWVudCwgdHJpZ2dlci5vcGVuLCB0b2dnbGVGbikpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGlzdGVuZXJzLnB1c2goXHJcbiAgICAgICAgICAgICAgICAgICAgcmVuZGVyZXIubGlzdGVuKG5hdGl2ZUVsZW1lbnQsIHRyaWdnZXIub3Blbiwgb3BlbkZuKSxcclxuICAgICAgICAgICAgICAgICAgICByZW5kZXJlci5saXN0ZW4obmF0aXZlRWxlbWVudCwgdHJpZ2dlci5jbG9zZSwgY2xvc2VGbikpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgICAgIGxpc3RlbmVycy5mb3JFYWNoKHVuc3Vic2NyaWJlRm4gPT4gdW5zdWJzY3JpYmVGbigpKTtcclxuICAgICAgICB9O1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbmNvbnN0IGRlbGF5T3JOb29wID0gPFQ+KHRpbWU6IG51bWJlcikgPT4gdGltZSA+IDAgPyBkZWxheTxUPih0aW1lKSA6IChhOiBPYnNlcnZhYmxlPFQ+KSA9PiBhO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHRyaWdnZXJEZWxheShvcGVuRGVsYXk6IG51bWJlciwgY2xvc2VEZWxheTogbnVtYmVyLCBpc09wZW5lZEZuOiAoKSA9PiBib29sZWFuKSB7XHJcbiAgICByZXR1cm4gKGlucHV0JDogT2JzZXJ2YWJsZTxib29sZWFuPikgPT4ge1xyXG4gICAgICAgIGxldCBwZW5kaW5nID0gbnVsbDtcclxuICAgICAgICBjb25zdCBmaWx0ZXJlZElucHV0JCA9IGlucHV0JC5waXBlKFxyXG4gICAgICAgICAgICBtYXAob3BlbiA9PiAoe29wZW59KSksIGZpbHRlcihldmVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjdXJyZW50bHlPcGVuID0gaXNPcGVuZWRGbigpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGN1cnJlbnRseU9wZW4gIT09IGV2ZW50Lm9wZW4gJiYgKCFwZW5kaW5nIHx8IHBlbmRpbmcub3BlbiA9PT0gY3VycmVudGx5T3BlbikpIHtcclxuICAgICAgICAgICAgICAgICAgICBwZW5kaW5nID0gZXZlbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAocGVuZGluZyAmJiBwZW5kaW5nLm9wZW4gIT09IGV2ZW50Lm9wZW4pIHtcclxuICAgICAgICAgICAgICAgICAgICBwZW5kaW5nID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgIHNoYXJlKCkpO1xyXG4gICAgICAgIGNvbnN0IGRlbGF5ZWRPcGVuJCA9IGZpbHRlcmVkSW5wdXQkLnBpcGUoZmlsdGVyKGV2ZW50ID0+IGV2ZW50Lm9wZW4pLCBkZWxheU9yTm9vcChvcGVuRGVsYXkpKTtcclxuICAgICAgICBjb25zdCBkZWxheWVkQ2xvc2UkID0gZmlsdGVyZWRJbnB1dCQucGlwZShmaWx0ZXIoZXZlbnQgPT4gIWV2ZW50Lm9wZW4pLCBkZWxheU9yTm9vcChjbG9zZURlbGF5KSk7XHJcbiAgICAgICAgcmV0dXJuIG1lcmdlKGRlbGF5ZWRPcGVuJCwgZGVsYXllZENsb3NlJClcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBmaWx0ZXIoZXZlbnQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChldmVudCA9PT0gcGVuZGluZykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwZW5kaW5nID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGV2ZW50Lm9wZW4gIT09IGlzT3BlbmVkRm4oKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBtYXAoZXZlbnQgPT4gZXZlbnQub3BlbikpO1xyXG4gICAgfTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGxpc3RlblRvVHJpZ2dlcnMoXHJcbiAgICByZW5kZXJlcjogYW55LCBuYXRpdmVFbGVtZW50OiBhbnksIHRyaWdnZXJzOiBzdHJpbmcsIGlzT3BlbmVkRm46ICgpID0+IGJvb2xlYW4sIG9wZW5GbiwgY2xvc2VGbiwgb3BlbkRlbGF5ID0gMCxcclxuICAgIGNsb3NlRGVsYXkgPSAwKSB7XHJcbiAgICBjb25zdCBwYXJzZWRUcmlnZ2VycyA9IHBhcnNlVHJpZ2dlcnModHJpZ2dlcnMpO1xyXG5cclxuICAgIGlmIChwYXJzZWRUcmlnZ2Vycy5sZW5ndGggPT09IDEgJiYgcGFyc2VkVHJpZ2dlcnNbMF0uaXNNYW51YWwoKSkge1xyXG4gICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBzdWJzY3JpcHRpb24gPSBvYnNlcnZlVHJpZ2dlcnMocmVuZGVyZXIsIG5hdGl2ZUVsZW1lbnQsIHBhcnNlZFRyaWdnZXJzLCBpc09wZW5lZEZuKVxyXG4gICAgICAgIC5waXBlKHRyaWdnZXJEZWxheShvcGVuRGVsYXksIGNsb3NlRGVsYXksIGlzT3BlbmVkRm4pKVxyXG4gICAgICAgIC5zdWJzY3JpYmUob3BlbiA9PiAob3BlbiA/IG9wZW5GbigpIDogY2xvc2VGbigpKSk7XHJcblxyXG4gICAgcmV0dXJuICgpID0+IHN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG59XHJcbiJdfQ==