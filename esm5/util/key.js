/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
var Key = {
    Tab: 9,
    Enter: 13,
    Escape: 27,
    Space: 32,
    PageUp: 33,
    PageDown: 34,
    End: 35,
    Home: 36,
    ArrowLeft: 37,
    ArrowUp: 38,
    ArrowRight: 39,
    ArrowDown: 40,
};
export { Key };
Key[Key.Tab] = 'Tab';
Key[Key.Enter] = 'Enter';
Key[Key.Escape] = 'Escape';
Key[Key.Space] = 'Space';
Key[Key.PageUp] = 'PageUp';
Key[Key.PageDown] = 'PageDown';
Key[Key.End] = 'End';
Key[Key.Home] = 'Home';
Key[Key.ArrowLeft] = 'ArrowLeft';
Key[Key.ArrowUp] = 'ArrowUp';
Key[Key.ArrowRight] = 'ArrowRight';
Key[Key.ArrowDown] = 'ArrowDown';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2V5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJ1dGlsL2tleS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7SUFDSSxNQUFPO0lBQ1AsU0FBVTtJQUNWLFVBQVc7SUFDWCxTQUFVO0lBQ1YsVUFBVztJQUNYLFlBQWE7SUFDYixPQUFRO0lBQ1IsUUFBUztJQUNULGFBQWM7SUFDZCxXQUFZO0lBQ1osY0FBZTtJQUNmLGFBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgZW51bSBLZXkge1xyXG4gICAgVGFiID0gOSxcclxuICAgIEVudGVyID0gMTMsXHJcbiAgICBFc2NhcGUgPSAyNyxcclxuICAgIFNwYWNlID0gMzIsXHJcbiAgICBQYWdlVXAgPSAzMyxcclxuICAgIFBhZ2VEb3duID0gMzQsXHJcbiAgICBFbmQgPSAzNSxcclxuICAgIEhvbWUgPSAzNixcclxuICAgIEFycm93TGVmdCA9IDM3LFxyXG4gICAgQXJyb3dVcCA9IDM4LFxyXG4gICAgQXJyb3dSaWdodCA9IDM5LFxyXG4gICAgQXJyb3dEb3duID0gNDBcclxufVxyXG4iXX0=