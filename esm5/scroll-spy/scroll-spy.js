/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, HostListener, Inject, Input, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
var NgtScrollSpy = /** @class */ (function () {
    function NgtScrollSpy(_document, _router, _elem, _renderer) {
        this._document = _document;
        this._router = _router;
        this._elem = _elem;
        this._renderer = _renderer;
    }
    /**
     * @private
     * @param {?} spTarget
     * @return {?}
     */
    NgtScrollSpy.prototype._getTarget = /**
     * @private
     * @param {?} spTarget
     * @return {?}
     */
    function (spTarget) {
        if (typeof spTarget === 'string') {
            spTarget = spTarget.split('');
            /** @type {?} */
            var type = spTarget.shift();
            switch (type) {
                case '#':
                    return this._document.getElementById(spTarget.join(''));
                case '.':
                    return this._document.getElementsByClassName(spTarget.join(''))[0];
                default:
                    return this._document.getElementsByTagName(this.ngtScrollSpy)[0];
            }
        }
        else {
            return spTarget;
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    NgtScrollSpy.prototype.onWindowScroll = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var offset = this.spyOffset ? this.spyOffset : 0;
        /** @type {?} */
        var scrollTarget = this._getTarget(this.ngtScrollSpy);
        /** @type {?} */
        var elemDim = scrollTarget ? scrollTarget.getBoundingClientRect() : null;
        if (!elemDim) {
            console.warn("There is no element " + this.ngtScrollSpy);
        }
        else if (this.spyAnchor && elemDim && elemDim.top < offset && elemDim.bottom > offset) {
            if (this.spyAnchor !== 'none') {
                this._router.navigate([], { fragment: this.spyAnchor });
            }
            else {
                this._router.navigate([]);
            }
            this._renderer.addClass(this._elem.nativeElement, 'active');
        }
        else {
            this._renderer.removeClass(this._elem.nativeElement, 'active');
        }
    };
    NgtScrollSpy.decorators = [
        { type: Directive, args: [{
                    selector: '[ngtScrollSpy]',
                },] }
    ];
    /** @nocollapse */
    NgtScrollSpy.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
        { type: Router },
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    NgtScrollSpy.propDecorators = {
        ngtScrollSpy: [{ type: Input }],
        spyAnchor: [{ type: Input }],
        spyOffset: [{ type: Input }],
        onWindowScroll: [{ type: HostListener, args: ['window:scroll', ['$event'],] }]
    };
    return NgtScrollSpy;
}());
export { NgtScrollSpy };
if (false) {
    /** @type {?} */
    NgtScrollSpy.prototype.ngtScrollSpy;
    /** @type {?} */
    NgtScrollSpy.prototype.spyAnchor;
    /** @type {?} */
    NgtScrollSpy.prototype.spyOffset;
    /**
     * @type {?}
     * @private
     */
    NgtScrollSpy.prototype._document;
    /**
     * @type {?}
     * @private
     */
    NgtScrollSpy.prototype._router;
    /**
     * @type {?}
     * @private
     */
    NgtScrollSpy.prototype._elem;
    /**
     * @type {?}
     * @private
     */
    NgtScrollSpy.prototype._renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Nyb2xsLXNweS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsic2Nyb2xsLXNweS9zY3JvbGwtc3B5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDOUYsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUV6QztJQVFJLHNCQUM4QixTQUFjLEVBQ2hDLE9BQWUsRUFDZixLQUFpQixFQUNqQixTQUFvQjtRQUhGLGNBQVMsR0FBVCxTQUFTLENBQUs7UUFDaEMsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUNmLFVBQUssR0FBTCxLQUFLLENBQVk7UUFDakIsY0FBUyxHQUFULFNBQVMsQ0FBVztJQUM3QixDQUFDOzs7Ozs7SUFFSSxpQ0FBVTs7Ozs7SUFBbEIsVUFBbUIsUUFBUTtRQUN2QixJQUFLLE9BQU8sUUFBUSxLQUFLLFFBQVEsRUFBRTtZQUMvQixRQUFRLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQzs7Z0JBQ3hCLElBQUksR0FBRyxRQUFRLENBQUMsS0FBSyxFQUFFO1lBQzdCLFFBQVEsSUFBSSxFQUFFO2dCQUNWLEtBQUssR0FBRztvQkFDSixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDNUQsS0FBSyxHQUFHO29CQUNKLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZFO29CQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDeEU7U0FDSjthQUFNO1lBQ0gsT0FBTyxRQUFRLENBQUM7U0FDbkI7SUFDTCxDQUFDOzs7OztJQUdELHFDQUFjOzs7O0lBRGQsVUFDZSxLQUFZOztZQUNqQixNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7WUFDNUMsWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQzs7WUFDakQsT0FBTyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHFCQUFxQixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUk7UUFDMUUsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNWLE9BQU8sQ0FBQyxJQUFJLENBQUMseUJBQXVCLElBQUksQ0FBQyxZQUFjLENBQUMsQ0FBQztTQUM1RDthQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLEdBQUcsR0FBRyxNQUFNLElBQUksT0FBTyxDQUFDLE1BQU0sR0FBRyxNQUFNLEVBQUU7WUFDckYsSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLE1BQU0sRUFBRTtnQkFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO2FBQzNEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQzdCO1lBRUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FDL0Q7YUFBTTtZQUNILElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ2xFO0lBQ0wsQ0FBQzs7Z0JBbERKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsZ0JBQWdCO2lCQUM3Qjs7OztnREFPUSxNQUFNLFNBQUMsUUFBUTtnQkFYZixNQUFNO2dCQUZLLFVBQVU7Z0JBQStCLFNBQVM7OzsrQkFRakUsS0FBSzs0QkFDTCxLQUFLOzRCQUNMLEtBQUs7aUNBMEJMLFlBQVksU0FBQyxlQUFlLEVBQUUsQ0FBQyxRQUFRLENBQUM7O0lBbUI3QyxtQkFBQztDQUFBLEFBbkRELElBbURDO1NBaERZLFlBQVk7OztJQUNyQixvQ0FBMkI7O0lBQzNCLGlDQUE0Qjs7SUFDNUIsaUNBQTRCOzs7OztJQUd4QixpQ0FBd0M7Ozs7O0lBQ3hDLCtCQUF1Qjs7Ozs7SUFDdkIsNkJBQXlCOzs7OztJQUN6QixpQ0FBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIEhvc3RMaXN0ZW5lciwgSW5qZWN0LCBJbnB1dCwgUmVuZGVyZXIyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERPQ1VNRU5UIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbbmd0U2Nyb2xsU3B5XScsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RTY3JvbGxTcHkge1xyXG4gICAgQElucHV0KCkgbmd0U2Nyb2xsU3B5OiBhbnk7XHJcbiAgICBASW5wdXQoKSBzcHlBbmNob3I/OiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKSBzcHlPZmZzZXQ/OiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgQEluamVjdChET0NVTUVOVCkgcHJpdmF0ZSBfZG9jdW1lbnQ6IGFueSxcclxuICAgICAgICBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICBwcml2YXRlIF9lbGVtOiBFbGVtZW50UmVmLFxyXG4gICAgICAgIHByaXZhdGUgX3JlbmRlcmVyOiBSZW5kZXJlcjJcclxuICAgICkge31cclxuXHJcbiAgICBwcml2YXRlIF9nZXRUYXJnZXQoc3BUYXJnZXQpOiBhbnkge1xyXG4gICAgICAgIGlmICggdHlwZW9mIHNwVGFyZ2V0ID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBzcFRhcmdldCA9IHNwVGFyZ2V0LnNwbGl0KCcnKTtcclxuICAgICAgICAgICAgY29uc3QgdHlwZSA9IHNwVGFyZ2V0LnNoaWZ0KCk7XHJcbiAgICAgICAgICAgIHN3aXRjaCAodHlwZSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnIyc6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKHNwVGFyZ2V0LmpvaW4oJycpKTtcclxuICAgICAgICAgICAgICAgIGNhc2UgJy4nOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLl9kb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKHNwVGFyZ2V0LmpvaW4oJycpKVswXTtcclxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2RvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKHRoaXMubmd0U2Nyb2xsU3B5KVswXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBzcFRhcmdldDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignd2luZG93OnNjcm9sbCcsIFsnJGV2ZW50J10pXHJcbiAgICBvbldpbmRvd1Njcm9sbChldmVudDogRXZlbnQpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCBvZmZzZXQgPSB0aGlzLnNweU9mZnNldCA/IHRoaXMuc3B5T2Zmc2V0IDogMDtcclxuICAgICAgICBjb25zdCBzY3JvbGxUYXJnZXQgPSB0aGlzLl9nZXRUYXJnZXQodGhpcy5uZ3RTY3JvbGxTcHkpO1xyXG4gICAgICAgIGNvbnN0IGVsZW1EaW0gPSBzY3JvbGxUYXJnZXQgPyBzY3JvbGxUYXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkgOiBudWxsO1xyXG4gICAgICAgIGlmICghZWxlbURpbSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oYFRoZXJlIGlzIG5vIGVsZW1lbnQgJHt0aGlzLm5ndFNjcm9sbFNweX1gKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3B5QW5jaG9yICYmIGVsZW1EaW0gJiYgZWxlbURpbS50b3AgPCBvZmZzZXQgJiYgZWxlbURpbS5ib3R0b20gPiBvZmZzZXQpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3B5QW5jaG9yICE9PSAnbm9uZScpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbXSwgeyBmcmFnbWVudDogdGhpcy5zcHlBbmNob3IgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoW10pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLl9lbGVtLm5hdGl2ZUVsZW1lbnQsICdhY3RpdmUnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLl9lbGVtLm5hdGl2ZUVsZW1lbnQsICdhY3RpdmUnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19