/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtScrollSpy } from './scroll-spy';
export { NgtScrollSpy } from './scroll-spy';
/** @type {?} */
var NGT_SCROLL_SPY_DIRECTIVES = [NgtScrollSpy];
var NgtScrollSpyModule = /** @class */ (function () {
    function NgtScrollSpyModule() {
    }
    /**
     * @return {?}
     */
    NgtScrollSpyModule.forRoot = /**
     * @return {?}
     */
    function () {
        return { ngModule: NgtScrollSpyModule };
    };
    NgtScrollSpyModule.decorators = [
        { type: NgModule, args: [{
                    declarations: NGT_SCROLL_SPY_DIRECTIVES,
                    exports: NGT_SCROLL_SPY_DIRECTIVES,
                    imports: [CommonModule]
                },] }
    ];
    return NgtScrollSpyModule;
}());
export { NgtScrollSpyModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Nyb2xsLXNweS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInNjcm9sbC1zcHkvc2Nyb2xsLXNweS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBRTVDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxjQUFjLENBQUM7O0lBRXRDLHlCQUF5QixHQUFHLENBQUMsWUFBWSxDQUFDO0FBRWhEO0lBQUE7SUFTQSxDQUFDOzs7O0lBSFUsMEJBQU87OztJQUFkO1FBQ0ksT0FBTyxFQUFDLFFBQVEsRUFBRSxrQkFBa0IsRUFBQyxDQUFDO0lBQzFDLENBQUM7O2dCQVJKLFFBQVEsU0FBQztvQkFDTixZQUFZLEVBQUUseUJBQXlCO29CQUN2QyxPQUFPLEVBQUUseUJBQXlCO29CQUNsQyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7aUJBQzFCOztJQUtELHlCQUFDO0NBQUEsQUFURCxJQVNDO1NBSlksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbmltcG9ydCB7IE5ndFNjcm9sbFNweSB9IGZyb20gJy4vc2Nyb2xsLXNweSc7XHJcblxyXG5leHBvcnQgeyBOZ3RTY3JvbGxTcHkgfSBmcm9tICcuL3Njcm9sbC1zcHknO1xyXG5cclxuY29uc3QgTkdUX1NDUk9MTF9TUFlfRElSRUNUSVZFUyA9IFtOZ3RTY3JvbGxTcHldO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogTkdUX1NDUk9MTF9TUFlfRElSRUNUSVZFUyxcclxuICAgIGV4cG9ydHM6IE5HVF9TQ1JPTExfU1BZX0RJUkVDVElWRVMsXHJcbiAgICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0U2Nyb2xsU3B5TW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgICAgIHJldHVybiB7bmdNb2R1bGU6IE5ndFNjcm9sbFNweU1vZHVsZX07XHJcbiAgICB9XHJcbn1cclxuIl19