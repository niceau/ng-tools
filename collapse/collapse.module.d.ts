import { ModuleWithProviders } from '@angular/core';
export { NgtCollapseService } from './collapse.service';
export { NgtCollapseDirective } from './collapse.directive';
export { NgtCollapseTriggerDirective } from './collapse-trigger.directive';
export declare class NgtCollapseModule {
    static forRoot(): ModuleWithProviders;
}
