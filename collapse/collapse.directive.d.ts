import { ElementRef } from '@angular/core';
import { NgtCollapseService } from './collapse.service';
import { NgtCollapseConfig } from './collapse-config';
export declare class NgtCollapseDirective {
    private elRef;
    private $collapseService;
    private _status;
    status: boolean;
    /**
     * A id of element for collapsing.
     */
    id: string;
    /**
     * A flag indicating show/hide with animation.
     */
    animated: boolean;
    /**
     * Speed of animation. No effect if [animated]=false.
     */
    animationSpeed: number;
    /**
     * Animation function. No effect if [animated]=false.
     */
    animationFunction: string;
    /**
     * A flag indicating default opened status when component is load.
     */
    opened: boolean;
    readonly triggerShow: {
        value: string;
        params: {
            animationSpeed: number;
            animationFunction: string;
        };
    };
    show: boolean;
    constructor(config: NgtCollapseConfig, elRef: ElementRef, $collapseService: NgtCollapseService);
    toggle(): void;
}
