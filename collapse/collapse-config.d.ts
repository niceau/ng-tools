/**
 * Configuration service for the NgtAlert component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the alerts used in the application.
 */
export declare class NgtCollapseConfig {
    animated: boolean;
    animationSpeed: number;
    animationFunction: string;
}
