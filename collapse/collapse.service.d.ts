import { Subject } from 'rxjs';
export declare class NgtCollapseService {
    isCollapseTriggered: Subject<string>;
    constructor();
    toggle(id: string): void;
}
