import { NgtCollapseService } from './collapse.service';
export declare class NgtCollapseTriggerDirective {
    private $collapseService;
    /**
     * A id of element to collapse.
     */
    id: any;
    /**
     * A flag to disable collapsing on click.
     */
    disabled: boolean;
    onClick(): void;
    constructor($collapseService: NgtCollapseService);
    toggle(): void;
}
