import { ModuleWithProviders } from '@angular/core';
export { NgtAlertComponent } from './alert.component';
export { NgtAlertConfig } from './alert-config';
export declare class NgtAlertModule {
    static forRoot(): ModuleWithProviders;
}
