import { ElementRef, EventEmitter, OnChanges, OnInit, Renderer2, SimpleChanges } from '@angular/core';
import { NgtAlertConfig } from './alert-config';
/**
 * Alerts can be used to provide feedback messages.
 */
export declare class NgtAlertComponent implements OnInit, OnChanges {
    private _renderer;
    private _element;
    /**
     * A flag indicating if a given alert can be dismissed (closed) by a user. If this flag is set, a close button (in a
     * form of an ×) will be displayed.
     */
    dismissible: boolean;
    /**
     * Class of icon that will be used in alert
     */
    icon: string;
    /**
     * Alert type (CSS class). System recognizes the following bg's: "primary", "secondary", "success", "danger", "warning", "info", "light" , "dark"
     *  and other utilities bg's.
     *  And also outline version: "outline-primary", "outline-secondary", "outline-success" etc.
     */
    type: string;
    /**
     * An event emitted when the close button is clicked. This event has no payload. Only relevant for dismissible alerts.
     */
    close: EventEmitter<void>;
    class: boolean;
    constructor(config: NgtAlertConfig, _renderer: Renderer2, _element: ElementRef);
    closeHandler(): void;
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
}
