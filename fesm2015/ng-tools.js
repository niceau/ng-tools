import { find, without } from 'lodash';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Subject, fromEvent, race, merge, Observable } from 'rxjs';
import { delay, filter, map, takeUntil, withLatestFrom, share } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CommonModule, DOCUMENT } from '@angular/common';
import { Directive, TemplateRef, Injectable, ContentChildren, Input, NgModule, HostListener, Component, HostBinding, Output, ApplicationRef, Inject, Injector, RendererFactory2, ChangeDetectionStrategy, ChangeDetectorRef, ComponentFactoryResolver, ElementRef, EventEmitter, NgZone, Renderer2, ViewContainerRef, ViewEncapsulation, forwardRef, Host, Optional, ContentChild, InjectionToken, defineInjectable, inject, INJECTOR } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * This directive should be used to wrap accordion panel titles that need to contain HTML markup or other directives.
 */
class NgtAccordionPanelTitleDirective {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtAccordionPanelTitleDirective.decorators = [
    { type: Directive, args: [{
                selector: 'ng-template[ngtAccordionPanelTitle]'
            },] }
];
/** @nocollapse */
NgtAccordionPanelTitleDirective.ctorParameters = () => [
    { type: TemplateRef }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A directive to wrap an accordion panel header to contain any HTML markup and a toggling button with `NgtAccordionPanelToggleDirective`
 */
class NgtAccordionPanelHeaderDirective {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtAccordionPanelHeaderDirective.decorators = [
    { type: Directive, args: [{
                selector: 'ng-template[ngtAccordionPanelHeader]'
            },] }
];
/** @nocollapse */
NgtAccordionPanelHeaderDirective.ctorParameters = () => [
    { type: TemplateRef }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * This directive must be used to wrap accordion panel content.
 */
class NgtAccordionPanelContentDirective {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtAccordionPanelContentDirective.decorators = [
    { type: Directive, args: [{
                selector: 'ng-template[ngtAccordionPanelContent]'
            },] }
];
/** @nocollapse */
NgtAccordionPanelContentDirective.ctorParameters = () => [
    { type: TemplateRef }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtAccordionService {
    constructor() {
        this.nextId = 0;
    }
}
NgtAccordionService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
NgtAccordionService.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtAccordionPanelDirective {
    /**
     * @param {?} $accordionService
     */
    constructor($accordionService) {
        this.$accordionService = $accordionService;
        this._isOpen = false;
        this.disabled = false;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set isOpen(value) {
        this._isOpen = value;
    }
    /**
     * @return {?}
     */
    get isOpen() {
        return this._isOpen;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.id) {
            this.id = `accordion-panel-${this.$accordionService.nextId++}`;
        }
    }
    /**
     * @return {?}
     */
    ngAfterContentChecked() {
        // We are using @ContentChildren instead of @ContentChild as in the Angular version being used
        // only @ContentChildren allows us to specify the {descendants: false} option.
        // Without {descendants: false} we are hitting bugs described in:
        // https://github.com/ng-bootstrap/ng-bootstrap/issues/2240
        this.titleTpl = this.titleTpls.first;
        this.headerTpl = this.headerTpls.first;
        this.contentTpl = this.contentTpls.first;
    }
}
NgtAccordionPanelDirective.decorators = [
    { type: Directive, args: [{
                selector: 'ngt-accordion-panel',
            },] }
];
/** @nocollapse */
NgtAccordionPanelDirective.ctorParameters = () => [
    { type: NgtAccordionService }
];
NgtAccordionPanelDirective.propDecorators = {
    id: [{ type: Input }],
    title: [{ type: Input }],
    bg: [{ type: Input }],
    disabled: [{ type: Input }],
    isOpen: [{ type: Input }],
    titleTpls: [{ type: ContentChildren, args: [NgtAccordionPanelTitleDirective, { descendants: false },] }],
    headerTpls: [{ type: ContentChildren, args: [NgtAccordionPanelHeaderDirective, { descendants: false },] }],
    contentTpls: [{ type: ContentChildren, args: [NgtAccordionPanelContentDirective, { descendants: false },] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Configuration service for the NgtAccordion component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the accordions used in the application.
 */
class NgtAccordionConfig {
    constructor() {
        this.closeOthers = true;
    }
}
NgtAccordionConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtAccordionConfig.ngInjectableDef = defineInjectable({ factory: function NgtAccordionConfig_Factory() { return new NgtAccordionConfig(); }, token: NgtAccordionConfig, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @param {?} value
 * @return {?}
 */
function toInteger(value) {
    return parseInt(`${value}`, 10);
}
/**
 * @param {?} value
 * @param {?} max
 * @param {?=} min
 * @return {?}
 */
function getValueInRange(value, max, min = 0) {
    return Math.max(Math.min(value, max), min);
}
/**
 * @param {?} value
 * @return {?}
 */
function isString(value) {
    return typeof value === 'string';
}
/**
 * @param {?} value
 * @return {?}
 */
function isNumber(value) {
    return !isNaN(toInteger(value));
}
/**
 * @param {?} value
 * @return {?}
 */
function isDefined(value) {
    return value !== undefined && value !== null;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * The NgtAccordion directive is a collection of panels.
 * It can assure that only one panel can be opened at a time.
 */
class NgtAccordionComponent {
    /**
     * @param {?} config
     */
    constructor(config) {
        /**
         *  Whether the other panels should be closed when a panel is opened
         */
        this.closeOtherPanels = true;
        /**
         * An array or comma separated strings of panel identifiers that should be opened
         */
        this.activeIds = [];
        /**
         * A panel change event fired right before the panel toggle happens. See NgtPanelChangeEvent for payload details
         */
        this.panelChange = new EventEmitter();
        this.class = 'accordion';
        this.type = config.type;
        this.bg = config.bg;
        this.closeOtherPanels = config.closeOthers;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.class = this.class + (this.type ? ' accordion-' + this.type : '');
    }
    /**
     * Checks if a panel with a given id is expanded or not.
     * @param {?} panelId
     * @return {?}
     */
    isExpanded(panelId) {
        return this.activeIds.indexOf(panelId) > -1;
    }
    /**
     * Expands a panel with a given id. Has no effect if the panel is already expanded or disabled.
     * @param {?} panelId
     * @return {?}
     */
    expand(panelId) {
        this._changeOpenState(this._findPanelById(panelId), true);
    }
    /**
     * Expands all panels if [closeOthers]="false". For the [closeOthers]="true" case will have no effect if there is an
     * open panel, otherwise the first panel will be expanded.
     * @return {?}
     */
    expandAll() {
        if (this.closeOtherPanels) {
            if (this.activeIds.length === 0 && this.panels.length) {
                this._changeOpenState(this.panels.first, true);
            }
        }
        else {
            this.panels.forEach((/**
             * @param {?} panel
             * @return {?}
             */
            panel => this._changeOpenState(panel, true)));
        }
    }
    /**
     * Collapses a panel with a given id. Has no effect if the panel is already collapsed or disabled.
     * @param {?} panelId
     * @return {?}
     */
    collapse(panelId) {
        this._changeOpenState(this._findPanelById(panelId), false);
    }
    /**
     * Collapses all open panels.
     * @return {?}
     */
    collapseAll() {
        this.panels.forEach((/**
         * @param {?} panel
         * @return {?}
         */
        (panel) => {
            this._changeOpenState(panel, false);
        }));
    }
    /**
     * Programmatically toggle a panel with a given id. Has no effect if the panel is disabled.
     * @param {?} panelId
     * @return {?}
     */
    toggle(panelId) {
        /** @type {?} */
        const panel = this._findPanelById(panelId);
        if (panel) {
            this._changeOpenState(panel, !panel.isOpen);
        }
    }
    /**
     * Toggle all panels.
     * @param {?=} nextState
     * @return {?}
     */
    toggleAll(nextState) {
        this.panels.forEach((/**
         * @param {?} panel
         * @return {?}
         */
        (panel) => {
            this._changeOpenState(panel, typeof nextState === 'boolean' ? nextState : !panel.isOpen);
        }));
    }
    /**
     * @return {?}
     */
    ngAfterContentChecked() {
        // active id updates
        if (isString(this.activeIds)) {
            this.activeIds = this.activeIds.split(/\s*,\s*/);
        }
        // update panels open states
        this.panels.forEach((/**
         * @param {?} panel
         * @return {?}
         */
        panel => panel.isOpen = !panel.disabled && this.activeIds.indexOf(panel.id) > -1));
        // closeOthers updates
        if (this.activeIds.length > 1 && this.closeOtherPanels) {
            this._closeOthers(this.activeIds[0]);
            this._updateActiveIds();
        }
    }
    /**
     * @private
     * @param {?} panel
     * @param {?} nextState
     * @return {?}
     */
    _changeOpenState(panel, nextState) {
        if (panel && !panel.disabled && panel.isOpen !== nextState) {
            /** @type {?} */
            let defaultPrevented = false;
            this.panelChange.emit({
                panelId: panel.id,
                nextState: nextState,
                preventDefault: (/**
                 * @return {?}
                 */
                () => {
                    defaultPrevented = true;
                })
            });
            if (!defaultPrevented) {
                panel.isOpen = nextState;
                if (nextState && this.closeOtherPanels) {
                    this._closeOthers(panel.id);
                }
                this._updateActiveIds();
            }
        }
    }
    /**
     * @private
     * @param {?} panelId
     * @return {?}
     */
    _closeOthers(panelId) {
        this.panels.forEach((/**
         * @param {?} panel
         * @return {?}
         */
        panel => {
            if (panel.id !== panelId) {
                panel.isOpen = false;
            }
        }));
    }
    /**
     * @private
     * @param {?} panelId
     * @return {?}
     */
    _findPanelById(panelId) {
        return this.panels.find((/**
         * @param {?} p
         * @return {?}
         */
        p => p.id === panelId));
    }
    /**
     * @private
     * @return {?}
     */
    _updateActiveIds() {
        this.activeIds = this.panels.filter((/**
         * @param {?} panel
         * @return {?}
         */
        panel => panel.isOpen && !panel.disabled)).map((/**
         * @param {?} panel
         * @return {?}
         */
        panel => panel.id));
    }
}
NgtAccordionComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-accordion',
                template: `
        <ng-template #t ngtAccordionPanelHeader let-panel>
            <div [class]="'accordion-panel_header ' + (panel.bg ? 'bg-'+panel.bg: bg ? 'bg-'+bg : '') + ' ' + (panel.isOpen ? 'expanded' : '')"
                 [ngtAccordionPanelToggle]="panel">
                <div *ngIf="panel.title" class="accordion-panel_title">
                    {{panel.title}}
                </div>
                <div *ngIf="!panel.title">
                    <ng-template [ngTemplateOutlet]="panel.titleTpl?.templateRef"></ng-template>
                </div>
            </div>
        </ng-template>
        <ng-template ngFor let-panel [ngForOf]="panels">
            <div [class]="'accordion-panel ' + (panel.isOpen ? 'expanded' : '')">
                <ng-template [ngTemplateOutlet]="panel.headerTpl?.templateRef || t"
                             [ngTemplateOutletContext]="{$implicit: panel, opened: panel.isOpen}"></ng-template>
                <div [@slide]="panel.isOpen ? 'down' : 'up'"
                     class="accordion-panel_body">
                    <div *ngIf="panel.contentTpl">
                        <ng-template [ngTemplateOutlet]="panel.contentTpl?.templateRef"></ng-template>
                    </div>
                    <ng-content></ng-content>
                </div>
            </div>
        </ng-template>
    `,
                animations: [
                    trigger('slide', [
                        state('down', style({ height: '*', paddingTop: '*', paddingBottom: '*' })),
                        state('up', style({ height: 0, paddingTop: 0, paddingBottom: 0 })),
                        transition('up => down', animate('350ms ease-out')),
                        transition('down => up', animate('350ms ease-out'))
                    ])
                ],
                providers: [NgtAccordionService]
            }] }
];
/** @nocollapse */
NgtAccordionComponent.ctorParameters = () => [
    { type: NgtAccordionConfig }
];
NgtAccordionComponent.propDecorators = {
    panels: [{ type: ContentChildren, args: [NgtAccordionPanelDirective,] }],
    closeOtherPanels: [{ type: Input, args: ['closeOthers',] }],
    activeIds: [{ type: Input }],
    type: [{ type: Input }],
    bg: [{ type: Input }],
    panelChange: [{ type: Output }],
    class: [{ type: HostBinding, args: ['class',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A directive to put on a button that toggles panel opening and closing.
 * To be used inside the `NgtAccordionPanelHeaderDirective`
 */
class NgtAccordionPanelToggleDirective {
    /**
     * @param {?} accordion
     * @param {?} panel
     */
    constructor(accordion, panel) {
        this.accordion = accordion;
        this.panel = panel;
    }
    /**
     * @param {?} panel
     * @return {?}
     */
    set ngtAccordionPanelToggle(panel) {
        if (panel) {
            this.panel = panel;
        }
    }
    /**
     * @return {?}
     */
    onClick() {
        this.accordion.toggle(this.panel.id);
    }
}
NgtAccordionPanelToggleDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtAccordionPanelToggle]',
                host: {
                    '[class.disabled]': 'panel.disabled',
                    '[class.collapsed]': '!panel.isOpen',
                    '[attr.aria-expanded]': 'panel.isOpen',
                    '[attr.aria-controls]': 'panel.id',
                }
            },] }
];
/** @nocollapse */
NgtAccordionPanelToggleDirective.ctorParameters = () => [
    { type: NgtAccordionComponent, decorators: [{ type: Inject, args: [forwardRef((/**
                     * @return {?}
                     */
                    () => NgtAccordionComponent)),] }] },
    { type: NgtAccordionPanelDirective, decorators: [{ type: Optional }, { type: Host }, { type: Inject, args: [forwardRef((/**
                     * @return {?}
                     */
                    () => NgtAccordionPanelDirective)),] }] }
];
NgtAccordionPanelToggleDirective.propDecorators = {
    ngtAccordionPanelToggle: [{ type: Input }],
    onClick: [{ type: HostListener, args: ['click',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGC_ACCORDION_DIRECTIVES = [NgtAccordionComponent, NgtAccordionPanelDirective, NgtAccordionPanelTitleDirective, NgtAccordionPanelContentDirective,
    NgtAccordionPanelHeaderDirective, NgtAccordionPanelToggleDirective];
class NgtAccordionModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtAccordionModule };
    }
}
NgtAccordionModule.decorators = [
    { type: NgModule, args: [{
                declarations: NGC_ACCORDION_DIRECTIVES,
                exports: NGC_ACCORDION_DIRECTIVES,
                imports: [CommonModule]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Configuration service for the NgtAlert component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the alerts used in the application.
 */
class NgtAlertConfig {
    constructor() {
        this.icon = '';
        this.dismissible = false;
        this.type = 'warning';
    }
}
NgtAlertConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtAlertConfig.ngInjectableDef = defineInjectable({ factory: function NgtAlertConfig_Factory() { return new NgtAlertConfig(); }, token: NgtAlertConfig, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Alerts can be used to provide feedback messages.
 */
class NgtAlertComponent {
    /**
     * @param {?} config
     * @param {?} _renderer
     * @param {?} _element
     */
    constructor(config, _renderer, _element) {
        this._renderer = _renderer;
        this._element = _element;
        /**
         * An event emitted when the close button is clicked. This event has no payload. Only relevant for dismissible alerts.
         */
        this.close = new EventEmitter();
        this.class = true;
        this.dismissible = config.dismissible;
        this.type = config.type;
        this.icon = config.icon;
    }
    /**
     * @return {?}
     */
    closeHandler() {
        this.close.emit(null);
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        /** @type {?} */
        const typeChange = changes['type'];
        if (typeChange && !typeChange.firstChange) {
            this._renderer.removeClass(this._element.nativeElement, `alert-${typeChange.previousValue}`);
            this._renderer.addClass(this._element.nativeElement, `alert-${typeChange.currentValue}`);
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._renderer.addClass(this._element.nativeElement, `alert-${this.type}`);
    }
}
NgtAlertComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-alert',
                changeDetection: ChangeDetectionStrategy.OnPush,
                encapsulation: ViewEncapsulation.None,
                template: `
        <div *ngIf="icon" class="alert-icon">
            <i class="{{ icon }}"></i>
        </div>
        <div class="alert-text">
            <ng-content></ng-content>
        </div>
        <div *ngIf="dismissible" class="alert-close">
            <button type="button" class="close" (click)="closeHandler()">
                <span aria-hidden="true">
                    <i class="ft-x"></i>
                </span>
            </button>
        </div>
    `,
                styles: ["ngt-alert{display:block}"]
            }] }
];
/** @nocollapse */
NgtAlertComponent.ctorParameters = () => [
    { type: NgtAlertConfig },
    { type: Renderer2 },
    { type: ElementRef }
];
NgtAlertComponent.propDecorators = {
    dismissible: [{ type: Input }],
    icon: [{ type: Input }],
    type: [{ type: Input }],
    close: [{ type: Output }],
    class: [{ type: HostBinding, args: ['class.alert',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtAlertModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtAlertModule };
    }
}
NgtAlertModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NgtAlertComponent],
                exports: [NgtAlertComponent],
                imports: [CommonModule],
                entryComponents: [NgtAlertComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtCollapseService {
    constructor() {
        this.isCollapseTriggered = new Subject();
    }
    /**
     * @param {?} id
     * @return {?}
     */
    toggle(id) {
        this.isCollapseTriggered.next(id);
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Configuration service for the NgtAlert component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the alerts used in the application.
 */
class NgtCollapseConfig {
    constructor() {
        this.animated = true;
        this.animationSpeed = 300;
        this.animationFunction = '';
    }
}
NgtCollapseConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtCollapseConfig.ngInjectableDef = defineInjectable({ factory: function NgtCollapseConfig_Factory() { return new NgtCollapseConfig(); }, token: NgtCollapseConfig, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtCollapseDirective {
    /**
     * @param {?} config
     * @param {?} elRef
     * @param {?} $collapseService
     */
    constructor(config, elRef, $collapseService) {
        this.elRef = elRef;
        this.$collapseService = $collapseService;
        /**
         * A flag indicating show/hide with animation.
         */
        this.animated = true;
        /**
         * Speed of animation. No effect if [animated]=false.
         */
        this.animationSpeed = 300;
        /**
         * Animation function. No effect if [animated]=false.
         */
        this.animationFunction = '';
        /**
         * A flag indicating default opened status when component is load.
         */
        this.opened = false;
        this.show = this.status;
        this.animated = config.animated;
        this.animationSpeed = config.animationSpeed;
        this.animationFunction = config.animationFunction;
        this.status = this.opened;
        this.$collapseService.isCollapseTriggered.subscribe((/**
         * @param {?} id
         * @return {?}
         */
        id => {
            if (id === this.id) {
                this.toggle();
            }
        }));
    }
    /**
     * @return {?}
     */
    get status() {
        return this._status;
    }
    /**
     * @param {?} status
     * @return {?}
     */
    set status(status) {
        this._status = status;
        this.show = status;
    }
    /**
     * @return {?}
     */
    get triggerShow() {
        return {
            value: this.animated ? (this.status ? 'down' : 'up') : (this.status ? 'show' : 'hide'),
            params: {
                animationSpeed: this.animationSpeed,
                animationFunction: this.animationFunction ? ' ' + this.animationFunction : ''
            }
        };
    }
    /**
     * @return {?}
     */
    toggle() {
        this.status = !this.status;
    }
}
NgtCollapseDirective.decorators = [
    { type: Component, args: [{
                selector: '[ngtCollapse]',
                exportAs: 'ngtCollapse',
                animations: [
                    trigger('show', [
                        state('down', style({ overflow: 'hidden', height: '*', paddingTop: '*', paddingBottom: '*' })),
                        state('up', style({ overflow: 'hidden', height: 0, paddingTop: 0, paddingBottom: 0 })),
                        state('show', style({ overflow: 'hidden', height: '*', paddingTop: '*', paddingBottom: '*' })),
                        state('hide', style({ overflow: 'hidden', height: 0, paddingTop: 0, paddingBottom: 0 })),
                        transition('up => down', animate('{{animationSpeed}}ms{{animationFunction}}')),
                        transition('down => up', animate('{{animationSpeed}}ms{{animationFunction}}')),
                        transition('hide => show', animate('0ms')),
                        transition('show => hide', animate('0ms'))
                    ]),
                ],
                template: `
        <ng-content></ng-content>`
            }] }
];
/** @nocollapse */
NgtCollapseDirective.ctorParameters = () => [
    { type: NgtCollapseConfig },
    { type: ElementRef },
    { type: NgtCollapseService }
];
NgtCollapseDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtCollapse',] }],
    animated: [{ type: Input }],
    animationSpeed: [{ type: Input }],
    animationFunction: [{ type: Input }],
    opened: [{ type: Input }],
    triggerShow: [{ type: HostBinding, args: ['@show',] }],
    show: [{ type: HostBinding, args: ['class.show',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtCollapseTriggerDirective {
    /**
     * @param {?} $collapseService
     */
    constructor($collapseService) {
        this.$collapseService = $collapseService;
        /**
         * A flag to disable collapsing on click.
         */
        this.disabled = false;
    }
    /**
     * @return {?}
     */
    onClick() {
        event.preventDefault();
        event.stopPropagation();
        this.toggle();
    }
    /**
     * @return {?}
     */
    toggle() {
        if (this.disabled) {
            return;
        }
        if (Array.isArray(this.id)) {
            this.id.forEach((/**
             * @param {?} id
             * @return {?}
             */
            id => {
                this.$collapseService.toggle(id);
            }));
        }
        else {
            this.$collapseService.toggle(this.id);
        }
    }
}
NgtCollapseTriggerDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtCollapseTrigger]'
            },] }
];
/** @nocollapse */
NgtCollapseTriggerDirective.ctorParameters = () => [
    { type: NgtCollapseService }
];
NgtCollapseTriggerDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtCollapseTrigger',] }],
    disabled: [{ type: Input }],
    onClick: [{ type: HostListener, args: ['click',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGC_COLLAPSE_DIRECTIVES = [NgtCollapseDirective, NgtCollapseTriggerDirective];
class NgtCollapseModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtCollapseModule };
    }
}
NgtCollapseModule.decorators = [
    { type: NgModule, args: [{
                declarations: NGC_COLLAPSE_DIRECTIVES,
                exports: NGC_COLLAPSE_DIRECTIVES,
                imports: [CommonModule],
                providers: [NgtCollapseService],
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// previous version:
// https://github.com/angular-ui/bootstrap/blob/07c31d0731f7cb068a1932b8e01d2312b796b4ec/src/position/position.js
class Positioning {
    /**
     * @private
     * @param {?} element
     * @return {?}
     */
    getAllStyles(element) {
        return window.getComputedStyle(element);
    }
    /**
     * @private
     * @param {?} element
     * @param {?} prop
     * @return {?}
     */
    getStyle(element, prop) {
        return this.getAllStyles(element)[prop];
    }
    /**
     * @private
     * @param {?} element
     * @return {?}
     */
    isStaticPositioned(element) {
        return (this.getStyle(element, 'position') || 'static') === 'static';
    }
    /**
     * @private
     * @param {?} element
     * @return {?}
     */
    offsetParent(element) {
        /** @type {?} */
        let offsetParentEl = (/** @type {?} */ (element.offsetParent)) || document.documentElement;
        while (offsetParentEl && offsetParentEl !== document.documentElement && this.isStaticPositioned(offsetParentEl)) {
            offsetParentEl = (/** @type {?} */ (offsetParentEl.offsetParent));
        }
        return offsetParentEl || document.documentElement;
    }
    /**
     * @param {?} element
     * @param {?=} round
     * @return {?}
     */
    position(element, round = true) {
        /** @type {?} */
        let elPosition;
        /** @type {?} */
        let parentOffset = { width: 0, height: 0, top: 0, bottom: 0, left: 0, right: 0 };
        if (this.getStyle(element, 'position') === 'fixed') {
            elPosition = element.getBoundingClientRect();
            elPosition = {
                top: elPosition.top,
                bottom: elPosition.bottom,
                left: elPosition.left,
                right: elPosition.right,
                height: elPosition.height,
                width: elPosition.width
            };
        }
        else {
            /** @type {?} */
            const offsetParentEl = this.offsetParent(element);
            elPosition = this.offset(element, false);
            if (offsetParentEl !== document.documentElement) {
                parentOffset = this.offset(offsetParentEl, false);
            }
            parentOffset.top += offsetParentEl.clientTop;
            parentOffset.left += offsetParentEl.clientLeft;
        }
        elPosition.top -= parentOffset.top;
        elPosition.bottom -= parentOffset.top;
        elPosition.left -= parentOffset.left;
        elPosition.right -= parentOffset.left;
        if (round) {
            elPosition.top = Math.round(elPosition.top);
            elPosition.bottom = Math.round(elPosition.bottom);
            elPosition.left = Math.round(elPosition.left);
            elPosition.right = Math.round(elPosition.right);
        }
        return elPosition;
    }
    /**
     * @param {?} element
     * @param {?=} round
     * @return {?}
     */
    offset(element, round = true) {
        /** @type {?} */
        const elBcr = element.getBoundingClientRect();
        /** @type {?} */
        const viewportOffset = {
            top: window.pageYOffset - document.documentElement.clientTop,
            left: window.pageXOffset - document.documentElement.clientLeft
        };
        /** @type {?} */
        let elOffset = {
            height: elBcr.height || element.offsetHeight,
            width: elBcr.width || element.offsetWidth,
            top: elBcr.top + viewportOffset.top,
            bottom: elBcr.bottom + viewportOffset.top,
            left: elBcr.left + viewportOffset.left,
            right: elBcr.right + viewportOffset.left
        };
        if (round) {
            elOffset.height = Math.round(elOffset.height);
            elOffset.width = Math.round(elOffset.width);
            elOffset.top = Math.round(elOffset.top);
            elOffset.bottom = Math.round(elOffset.bottom);
            elOffset.left = Math.round(elOffset.left);
            elOffset.right = Math.round(elOffset.right);
        }
        return elOffset;
    }
    /*
          Return false if the element to position is outside the viewport
        */
    /**
     * @param {?} hostElement
     * @param {?} targetElement
     * @param {?} placement
     * @param {?=} appendToBody
     * @return {?}
     */
    positionElements(hostElement, targetElement, placement, appendToBody) {
        const [placementPrimary = 'top', placementSecondary = 'center'] = placement.split('-');
        /** @type {?} */
        const hostElPosition = appendToBody ? this.offset(hostElement, false) : this.position(hostElement, false);
        /** @type {?} */
        const targetElStyles = this.getAllStyles(targetElement);
        /** @type {?} */
        const marginTop = parseFloat(targetElStyles.marginTop);
        /** @type {?} */
        const marginBottom = parseFloat(targetElStyles.marginBottom);
        /** @type {?} */
        const marginLeft = parseFloat(targetElStyles.marginLeft);
        /** @type {?} */
        const marginRight = parseFloat(targetElStyles.marginRight);
        /** @type {?} */
        let topPosition = 0;
        /** @type {?} */
        let leftPosition = 0;
        switch (placementPrimary) {
            case 'top':
                topPosition = (hostElPosition.top - (targetElement.offsetHeight + marginTop + marginBottom));
                break;
            case 'bottom':
                topPosition = (hostElPosition.top + hostElPosition.height);
                break;
            case 'left':
                leftPosition = (hostElPosition.left - (targetElement.offsetWidth + marginLeft + marginRight));
                break;
            case 'right':
                leftPosition = (hostElPosition.left + hostElPosition.width);
                break;
        }
        switch (placementSecondary) {
            case 'top':
                topPosition = hostElPosition.top;
                break;
            case 'bottom':
                topPosition = hostElPosition.top + hostElPosition.height - targetElement.offsetHeight;
                break;
            case 'left':
                leftPosition = hostElPosition.left;
                break;
            case 'right':
                leftPosition = hostElPosition.left + hostElPosition.width - targetElement.offsetWidth;
                break;
            case 'center':
                if (placementPrimary === 'top' || placementPrimary === 'bottom') {
                    leftPosition = (hostElPosition.left + hostElPosition.width / 2 - targetElement.offsetWidth / 2);
                }
                else {
                    topPosition = (hostElPosition.top + hostElPosition.height / 2 - targetElement.offsetHeight / 2);
                }
                break;
        }
        /// The translate3d/gpu acceleration render a blurry text on chrome, the next line is commented until a browser fix
        // targetElement.style.transform = `translate3d(${Math.round(leftPosition)}px, ${Math.floor(topPosition)}px, 0px)`;
        targetElement.style.transform = `translate(${leftPosition}px, ${topPosition}px)`;
        // Check if the targetElement is inside the viewport
        /** @type {?} */
        const targetElBCR = targetElement.getBoundingClientRect();
        /** @type {?} */
        const html = document.documentElement;
        /** @type {?} */
        const windowHeight = window.innerHeight || html.clientHeight;
        /** @type {?} */
        const windowWidth = window.innerWidth || html.clientWidth;
        return targetElBCR.left >= 0 && targetElBCR.top >= 0 && targetElBCR.right <= windowWidth &&
            targetElBCR.bottom <= windowHeight;
    }
}
/** @type {?} */
const placementSeparator = /\s+/;
/** @type {?} */
const positionService = new Positioning();
/*
 * Accept the placement array and applies the appropriate placement dependent on the viewport.
 * Returns the applied placement.
 * In case of auto placement, placements are selected in order
 *   'top', 'bottom', 'left', 'right',
 *   'top-left', 'top-right',
 *   'bottom-left', 'bottom-right',
 *   'left-top', 'left-bottom',
 *   'right-top', 'right-bottom'.
 * */
/**
 * @param {?} hostElement
 * @param {?} targetElement
 * @param {?} placement
 * @param {?=} appendToBody
 * @param {?=} baseClass
 * @return {?}
 */
function positionElements(hostElement, targetElement, placement, appendToBody, baseClass) {
    /** @type {?} */
    let placementVals = Array.isArray(placement) ? placement : (/** @type {?} */ (placement.split(placementSeparator)));
    /** @type {?} */
    const allowedPlacements = [
        'top', 'bottom', 'left', 'right', 'top-left', 'top-right', 'bottom-left', 'bottom-right', 'left-top', 'left-bottom',
        'right-top', 'right-bottom'
    ];
    /** @type {?} */
    const classList = targetElement.classList;
    /** @type {?} */
    const addClassesToTarget = (/**
     * @param {?} targetPlacement
     * @return {?}
     */
    (targetPlacement) => {
        const [primary, secondary] = targetPlacement.split('-');
        /** @type {?} */
        const classes = [];
        if (baseClass) {
            classes.push(`${baseClass}-${primary}`);
            if (secondary) {
                classes.push(`${baseClass}-${primary}-${secondary}`);
            }
            classes.forEach((/**
             * @param {?} classname
             * @return {?}
             */
            (classname) => {
                classList.add(classname);
            }));
        }
        return classes;
    });
    // Remove old placement classes to avoid issues
    if (baseClass) {
        allowedPlacements.forEach((/**
         * @param {?} placementToRemove
         * @return {?}
         */
        (placementToRemove) => {
            classList.remove(`${baseClass}-${placementToRemove}`);
        }));
    }
    // replace auto placement with other placements
    /** @type {?} */
    let hasAuto = placementVals.findIndex((/**
     * @param {?} val
     * @return {?}
     */
    val => val === 'auto'));
    if (hasAuto >= 0) {
        allowedPlacements.forEach((/**
         * @param {?} obj
         * @return {?}
         */
        function (obj) {
            if (placementVals.find((/**
             * @param {?} val
             * @return {?}
             */
            val => val.search('^' + obj) !== -1)) == null) {
                placementVals.splice(hasAuto++, 1, (/** @type {?} */ (obj)));
            }
        }));
    }
    // coordinates where to position
    // Required for transform:
    /** @type {?} */
    const style$$1 = targetElement.style;
    style$$1.position = 'absolute';
    style$$1.top = '0';
    style$$1.left = '0';
    // The translate3d/gpu acceleration render a blurry text on chrome, the next line is commented until a browser fix
    // style['will-change'] = 'transform';
    /** @type {?} */
    let testPlacement;
    /** @type {?} */
    let isInViewport = false;
    for (testPlacement of placementVals) {
        /** @type {?} */
        let addedClasses = addClassesToTarget(testPlacement);
        if (positionService.positionElements(hostElement, targetElement, testPlacement, appendToBody)) {
            isInViewport = true;
            break;
        }
        // Remove the baseClasses for further calculation
        if (baseClass) {
            addedClasses.forEach((/**
             * @param {?} classname
             * @return {?}
             */
            (classname) => {
                classList.remove(classname);
            }));
        }
    }
    if (!isInViewport) {
        // If nothing match, the first placement is the default one
        testPlacement = placementVals[0];
        addClassesToTarget(testPlacement);
        positionService.positionElements(hostElement, targetElement, testPlacement, appendToBody);
    }
    return testPlacement;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const Key = {
    Tab: 9,
    Enter: 13,
    Escape: 27,
    Space: 32,
    PageUp: 33,
    PageDown: 34,
    End: 35,
    Home: 36,
    ArrowLeft: 37,
    ArrowUp: 38,
    ArrowRight: 39,
    ArrowDown: 40,
};
Key[Key.Tab] = 'Tab';
Key[Key.Enter] = 'Enter';
Key[Key.Escape] = 'Escape';
Key[Key.Space] = 'Space';
Key[Key.PageUp] = 'PageUp';
Key[Key.PageDown] = 'PageDown';
Key[Key.End] = 'End';
Key[Key.Home] = 'Home';
Key[Key.ArrowLeft] = 'ArrowLeft';
Key[Key.ArrowUp] = 'ArrowUp';
Key[Key.ArrowRight] = 'ArrowRight';
Key[Key.ArrowDown] = 'ArrowDown';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const isHTMLElementContainedIn = (/**
 * @param {?} element
 * @param {?=} array
 * @return {?}
 */
(element, array) => array ? array.some((/**
 * @param {?} item
 * @return {?}
 */
item => item.contains(element))) : false);
// we'll have to use 'touch' events instead of 'mouse' events on iOS and add a more significant delay
// to avoid re-opening when handling (click) on a toggling element
// TODO: use proper Angular platform detection when NgtAutoClose becomes a service and we can inject PLATFORM_ID
/** @type {?} */
let iOS = false;
if (typeof navigator !== 'undefined') {
    iOS = !!navigator.userAgent && /iPad|iPhone|iPod/.test(navigator.userAgent);
}
/**
 * @param {?} zone
 * @param {?} document
 * @param {?} type
 * @param {?} close
 * @param {?} closed$
 * @param {?} insideElements
 * @param {?=} ignoreElements
 * @return {?}
 */
function ngtAutoClose(zone, document, type, close, closed$, insideElements, ignoreElements) {
    // closing on ESC and outside clicks
    if (type) {
        zone.runOutsideAngular((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const shouldCloseOnClick = (/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                /** @type {?} */
                const element = (/** @type {?} */ (event.target));
                if (event instanceof MouseEvent) {
                    if (event.button === 2 || isHTMLElementContainedIn(element, ignoreElements)) {
                        return false;
                    }
                }
                if (type === 'inside') {
                    return isHTMLElementContainedIn(element, insideElements);
                }
                else if (type === 'outside') {
                    return !isHTMLElementContainedIn(element, insideElements);
                }
                else /* if (type === true) */ {
                    return true;
                }
            });
            /** @type {?} */
            const escapes$ = fromEvent(document, 'keydown')
                .pipe(takeUntil(closed$), 
            // tslint:disable-next-line:deprecation
            filter((/**
             * @param {?} e
             * @return {?}
             */
            e => e.which === Key.Escape)));
            // we have to pre-calculate 'shouldCloseOnClick' on 'mousedown/touchstart',
            // because on 'mouseup/touchend' DOM nodes might be detached
            /** @type {?} */
            const mouseDowns$ = fromEvent(document, iOS ? 'touchstart' : 'mousedown')
                .pipe(map(shouldCloseOnClick), takeUntil(closed$));
            /** @type {?} */
            const closeableClicks$ = fromEvent(document, iOS ? 'touchend' : 'mouseup')
                .pipe(withLatestFrom(mouseDowns$), filter((/**
             * @param {?} __0
             * @return {?}
             */
            ([_, shouldClose]) => shouldClose)), delay(iOS ? 16 : 0), takeUntil(closed$));
            race([escapes$, closeableClicks$]).subscribe((/**
             * @return {?}
             */
            () => zone.run(close)));
        }));
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Configuration service for the NgtDropdown directive.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the dropdowns used in the application.
 */
class NgtDropdownConfig {
    constructor() {
        this.autoClose = true;
        this.placement = ['bottom-left', 'bottom-right', 'top-left', 'top-right'];
    }
}
NgtDropdownConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtDropdownConfig.ngInjectableDef = defineInjectable({ factory: function NgtDropdownConfig_Factory() { return new NgtDropdownConfig(); }, token: NgtDropdownConfig, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A directive you should put put on a dropdown item to enable keyboard navigation.
 * Keyboard navigation using arrow keys will move focus between items marked with this directive.
 */
class NgtDropdownItemDirective {
    /**
     * @param {?} elementRef
     */
    constructor(elementRef) {
        this.elementRef = elementRef;
        this._disabled = false;
        this.class = true;
        this.classDisabled = this.disabled;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set disabled(value) {
        this._disabled = (/** @type {?} */ (value)) === '' || value === true; // accept an empty attribute as true
        this.classDisabled = this._disabled;
    }
    /**
     * @return {?}
     */
    get disabled() {
        return this._disabled;
    }
}
NgtDropdownItemDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdownItem]'
            },] }
];
/** @nocollapse */
NgtDropdownItemDirective.ctorParameters = () => [
    { type: ElementRef }
];
NgtDropdownItemDirective.propDecorators = {
    class: [{ type: HostBinding, args: ['class.dropdown-item',] }],
    classDisabled: [{ type: HostBinding, args: ['class.disabled',] }],
    disabled: [{ type: Input }]
};
/**
 *
 */
class NgtDropdownMenuDirective {
    /**
     * @param {?} dropdown
     * @param {?} _elementRef
     * @param {?} _renderer
     */
    constructor(dropdown, _elementRef, _renderer) {
        this.dropdown = dropdown;
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this.placement = 'bottom';
        this.isOpen = false;
        this.dropdownMenu = true;
        this.show = this.dropdown.isOpen();
        this.xPlacement = this.placement;
        this.dropdown.openChange.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.show = status;
        }));
    }
    /**
     * @return {?}
     */
    getNativeElement() {
        return this._elementRef.nativeElement;
    }
    /**
     * @param {?} triggerEl
     * @param {?} placement
     * @return {?}
     */
    position(triggerEl, placement) {
        this.applyPlacement(positionElements(triggerEl, this._elementRef.nativeElement, placement));
    }
    /**
     * @param {?} _placement
     * @return {?}
     */
    applyPlacement(_placement) {
        // remove the current placement classes
        this._renderer.removeClass(this._elementRef.nativeElement.parentNode, 'dropup');
        this._renderer.removeClass(this._elementRef.nativeElement.parentNode, 'dropdown');
        this.placement = _placement;
        /**
         * apply the new placement
         * in case of top use up-arrow or down-arrow otherwise
         */
        if (_placement.search('^top') !== -1) {
            this._renderer.addClass(this._elementRef.nativeElement.parentNode, 'dropup');
        }
        else {
            this._renderer.addClass(this._elementRef.nativeElement.parentNode, 'dropdown');
        }
    }
}
NgtDropdownMenuDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdownMenu]'
            },] }
];
/** @nocollapse */
NgtDropdownMenuDirective.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [forwardRef((/**
                     * @return {?}
                     */
                    () => NgtDropdownDirective)),] }] },
    { type: ElementRef },
    { type: Renderer2 }
];
NgtDropdownMenuDirective.propDecorators = {
    menuItems: [{ type: ContentChildren, args: [NgtDropdownItemDirective,] }],
    dropdownMenu: [{ type: HostBinding, args: ['class.dropdown-menu',] }],
    show: [{ type: HostBinding, args: ['class.show',] }],
    xPlacement: [{ type: HostBinding, args: ['attr.x-placement',] }]
};
/**
 * Marks an element to which dropdown menu will be anchored. This is a simple version
 * of the NgtDropdownToggleDirective. It plays the same role as NgtDropdownToggleDirective but
 * doesn't listen to click events to toggle dropdown menu thus enabling support for
 * events other than click.
 */
class NgtDropdownAnchorDirective {
    /**
     * @param {?} dropdown
     * @param {?} _elementRef
     */
    constructor(dropdown, _elementRef) {
        this.dropdown = dropdown;
        this._elementRef = _elementRef;
        this.class = true;
        this.ariaHaspopup = true;
        this.ariaExpanded = this.dropdown.isOpen();
        this.anchorEl = _elementRef.nativeElement;
    }
    /**
     * @return {?}
     */
    getNativeElement() {
        return this._elementRef.nativeElement;
    }
}
NgtDropdownAnchorDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdownAnchor]'
            },] }
];
/** @nocollapse */
NgtDropdownAnchorDirective.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [forwardRef((/**
                     * @return {?}
                     */
                    () => NgtDropdownDirective)),] }] },
    { type: ElementRef }
];
NgtDropdownAnchorDirective.propDecorators = {
    class: [{ type: HostBinding, args: ['class.dropdown-toggle',] }],
    ariaHaspopup: [{ type: HostBinding, args: ['attr.aria-haspopup.true',] }],
    ariaExpanded: [{ type: HostBinding, args: ['attr.aria-expanded',] }]
};
/**
 * Allows the dropdown to be toggled via click. This directive is optional: you can use NgtDropdownAnchorDirective as an
 * alternative.
 */
class NgtDropdownToggleDirective extends NgtDropdownAnchorDirective {
    /**
     * @param {?} dropdown
     * @param {?} elementRef
     */
    constructor(dropdown, elementRef) {
        super(dropdown, elementRef);
        this.class = true;
        this.ariaHaspopup = true;
        this.ariaExpanded = this.dropdown.isOpen();
    }
    /**
     * @return {?}
     */
    onClick() {
        this.toggleOpen();
    }
    /**
     * @return {?}
     */
    toggleOpen() {
        this.dropdown.toggle();
    }
}
NgtDropdownToggleDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdownToggle]',
                providers: [{ provide: NgtDropdownAnchorDirective, useExisting: forwardRef((/**
                         * @return {?}
                         */
                        () => NgtDropdownToggleDirective)) }]
            },] }
];
/** @nocollapse */
NgtDropdownToggleDirective.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [forwardRef((/**
                     * @return {?}
                     */
                    () => NgtDropdownDirective)),] }] },
    { type: ElementRef }
];
NgtDropdownToggleDirective.propDecorators = {
    class: [{ type: HostBinding, args: ['class.dropdown-toggle',] }],
    ariaHaspopup: [{ type: HostBinding, args: ['attr.aria-haspopup.true',] }],
    ariaExpanded: [{ type: HostBinding, args: ['attr.aria-expanded',] }],
    onClick: [{ type: HostListener, args: ['click',] }]
};
/**
 * Transforms a node into a dropdown.
 */
class NgtDropdownDirective {
    /**
     * @param {?} _changeDetector
     * @param {?} config
     * @param {?} _document
     * @param {?} _ngZone
     * @param {?} _elementRef
     * @param {?} _renderer
     */
    constructor(_changeDetector, config, _document, _ngZone, _elementRef, _renderer) {
        this._changeDetector = _changeDetector;
        this._document = _document;
        this._ngZone = _ngZone;
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this._closed$ = new Subject();
        /**
         *  Defines whether or not the dropdown-menu is open initially.
         */
        this._open = false;
        /**
         *  An event fired when the dropdown is opened or closed.
         *  Event's payload equals whether dropdown is open.
         */
        this.openChange = new EventEmitter();
        this.show = this.isOpen();
        this.placement = config.placement;
        this.container = config.container;
        this.autoClose = config.autoClose;
        this._zoneSubscription = _ngZone.onStable.subscribe((/**
         * @return {?}
         */
        () => {
            this._positionMenu();
        }));
        this.openChange.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.show = status;
        }));
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onArrowUp($event) {
        this.onKeyDown($event);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onArrowArrowDown($event) {
        this.onKeyDown($event);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onArrowHome($event) {
        this.onKeyDown($event);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onArrowEnd($event) {
        this.onKeyDown($event);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._applyPlacementClasses();
        if (this._open) {
            this._setCloseHandlers();
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.container && this._open) {
            this._applyContainer(this.container);
        }
        if (changes.placement && !changes.placement.isFirstChange) {
            this._applyPlacementClasses();
        }
    }
    /**
     * Checks if the dropdown menu is open or not.
     * @return {?}
     */
    isOpen() {
        return this._open;
    }
    /**
     * Opens the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    open() {
        if (!this._open) {
            this._open = true;
            this._applyContainer(this.container);
            this._positionMenu();
            this.openChange.emit(true);
            this._setCloseHandlers();
        }
    }
    /**
     * @private
     * @return {?}
     */
    _setCloseHandlers() {
        ngtAutoClose(this._ngZone, this._document, this.autoClose, (/**
         * @return {?}
         */
        () => this.close()), this._closed$, this._menu ? [this._menu.getNativeElement()] : [], this._anchor ? [this._anchor.getNativeElement()] : []);
    }
    /**
     * Closes the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    close() {
        if (this._open) {
            this._open = false;
            this._resetContainer();
            this._closed$.next();
            this.openChange.emit(false);
            this._changeDetector.markForCheck();
        }
    }
    /**
     * Toggles the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    toggle() {
        if (this.isOpen()) {
            this.close();
        }
        else {
            this.open();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this._resetContainer();
        this._closed$.next();
        this._zoneSubscription.unsubscribe();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyDown(event) {
        /** @type {?} */
        const itemElements = this._getMenuElements();
        if (!itemElements.length) {
            return false;
        }
        /** @type {?} */
        let position = -1;
        /** @type {?} */
        let isEventFromItems = false;
        /** @type {?} */
        const isEventFromToggle = this._isEventFromToggle(event);
        if (!isEventFromToggle) {
            itemElements.forEach((/**
             * @param {?} itemElement
             * @param {?} index
             * @return {?}
             */
            (itemElement, index) => {
                if (itemElement.contains((/** @type {?} */ (event.target)))) {
                    isEventFromItems = true;
                }
                if (itemElement === this._document.activeElement) {
                    position = index;
                }
            }));
        }
        if (isEventFromToggle || isEventFromItems) {
            if (!this.isOpen()) {
                this.open();
            }
            // tslint:disable-next-line:deprecation
            switch (event.which) {
                case Key.ArrowDown:
                    position = Math.min(position + 1, itemElements.length - 1);
                    break;
                case Key.ArrowUp:
                    if (this._isDropup() && position === -1) {
                        position = itemElements.length - 1;
                        break;
                    }
                    position = Math.max(position - 1, 0);
                    break;
                case Key.Home:
                    position = 0;
                    break;
                case Key.End:
                    position = itemElements.length - 1;
                    break;
            }
            itemElements[position].focus();
            event.preventDefault();
        }
    }
    /**
     * @private
     * @return {?}
     */
    _isDropup() {
        return this._elementRef.nativeElement.classList.contains('dropup');
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _isEventFromToggle(event) {
        return this._anchor.getNativeElement().contains((/** @type {?} */ (event.target)));
    }
    /**
     * @private
     * @return {?}
     */
    _getMenuElements() {
        if (this._menu == null) {
            return [];
        }
        return this._menu.menuItems.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => !item.disabled)).map((/**
         * @param {?} item
         * @return {?}
         */
        item => item.elementRef.nativeElement));
    }
    /**
     * @private
     * @return {?}
     */
    _positionMenu() {
        if (this.isOpen() && this._menu) {
            this._applyPlacementClasses(positionElements(this._anchor.anchorEl, this._bodyContainer || this._menuElement.nativeElement, this.placement, this.container === 'body'));
        }
    }
    /**
     * @private
     * @return {?}
     */
    _resetContainer() {
        /** @type {?} */
        const renderer = this._renderer;
        if (this._menuElement) {
            /** @type {?} */
            const dropdownElement = this._elementRef.nativeElement;
            /** @type {?} */
            const dropdownMenuElement = this._menuElement.nativeElement;
            renderer.appendChild(dropdownElement, dropdownMenuElement);
            renderer.removeStyle(dropdownMenuElement, 'position');
            renderer.removeStyle(dropdownMenuElement, 'transform');
        }
        if (this._bodyContainer) {
            renderer.removeChild(this._document.body, this._bodyContainer);
            this._bodyContainer = null;
        }
    }
    /**
     * @private
     * @param {?=} container
     * @return {?}
     */
    _applyContainer(container = null) {
        this._resetContainer();
        if (container === 'body') {
            /** @type {?} */
            const renderer = this._renderer;
            /** @type {?} */
            const dropdownMenuElement = this._menuElement.nativeElement;
            /** @type {?} */
            const bodyContainer = this._bodyContainer = this._bodyContainer || renderer.createElement('div');
            // Override some styles to have the positionning working
            renderer.setStyle(bodyContainer, 'position', 'absolute');
            renderer.setStyle(dropdownMenuElement, 'position', 'static');
            renderer.appendChild(bodyContainer, dropdownMenuElement);
            renderer.appendChild(this._document.body, bodyContainer);
        }
    }
    /**
     * @private
     * @param {?=} placement
     * @return {?}
     */
    _applyPlacementClasses(placement) {
        if (this._menu) {
            if (!placement) {
                placement = Array.isArray(this.placement) ? this.placement[0] : (/** @type {?} */ (this.placement));
            }
            /** @type {?} */
            const renderer = this._renderer;
            /** @type {?} */
            const dropdownElement = this._elementRef.nativeElement;
            // remove the current placement classes
            renderer.removeClass(dropdownElement, 'dropup');
            renderer.removeClass(dropdownElement, 'dropdown');
            this.placement = placement;
            this._menu.placement = placement;
            /*
                        * apply the new placement
                        * in case of top use up-arrow or down-arrow otherwise
                        */
            /** @type {?} */
            const dropdownClass = placement.search('^top') !== -1 ? 'dropup' : 'dropdown';
            renderer.addClass(dropdownElement, dropdownClass);
            /** @type {?} */
            const bodyContainer = this._bodyContainer;
            if (bodyContainer) {
                renderer.removeClass(bodyContainer, 'dropup');
                renderer.removeClass(bodyContainer, 'dropdown');
                renderer.addClass(bodyContainer, dropdownClass);
            }
        }
    }
}
NgtDropdownDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdown]',
                exportAs: 'ngtDropdown'
            },] }
];
/** @nocollapse */
NgtDropdownDirective.ctorParameters = () => [
    { type: ChangeDetectorRef },
    { type: NgtDropdownConfig },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: NgZone },
    { type: ElementRef },
    { type: Renderer2 }
];
NgtDropdownDirective.propDecorators = {
    _menu: [{ type: ContentChild, args: [NgtDropdownMenuDirective,] }],
    _menuElement: [{ type: ContentChild, args: [NgtDropdownMenuDirective, { read: ElementRef },] }],
    _anchor: [{ type: ContentChild, args: [NgtDropdownAnchorDirective,] }],
    autoClose: [{ type: Input }],
    _open: [{ type: Input, args: ['open',] }],
    placement: [{ type: Input }],
    container: [{ type: Input }],
    openChange: [{ type: Output }],
    show: [{ type: HostBinding, args: ['class.show',] }],
    onArrowUp: [{ type: HostListener, args: ['document:keydown.ArrowUp', ['$event'],] }],
    onArrowArrowDown: [{ type: HostListener, args: ['document:keydown.ArrowDown', ['$event'],] }],
    onArrowHome: [{ type: HostListener, args: ['document:keydown.Home', ['$event'],] }],
    onArrowEnd: [{ type: HostListener, args: ['document:keydown.End', ['$event'],] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGC_DROPDOWN_DIRECTIVES = [
    NgtDropdownDirective, NgtDropdownAnchorDirective, NgtDropdownToggleDirective, NgtDropdownMenuDirective, NgtDropdownItemDirective
];
class NgtDropdownModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtDropdownModule };
    }
}
NgtDropdownModule.decorators = [
    { type: NgModule, args: [{ declarations: NGC_DROPDOWN_DIRECTIVES, exports: NGC_DROPDOWN_DIRECTIVES },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Configuration object token for the NgtModal service.
 * You can provide this configuration, typically in your root module in order to provide default option values for every
 * modal.
 */
class NgtModalConfig {
    constructor() {
        this.backdrop = true;
        this.keyboard = true;
    }
}
NgtModalConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtModalConfig.ngInjectableDef = defineInjectable({ factory: function NgtModalConfig_Factory() { return new NgtModalConfig(); }, token: NgtModalConfig, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A reference to an active (currently opened) modal. Instances of this class
 * can be injected into components passed as modal content.
 */
class NgtActiveModal {
    constructor() {
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
    }
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    close(result) { }
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    dismiss(reason) { }
}
/**
 * A reference to a newly opened modal returned by the 'NgtModalComponent.open()' method.
 */
class NgtModalRef {
    /**
     * @param {?} _windowCmptRef
     * @param {?} _contentRef
     * @param {?=} _backdropCmptRef
     * @param {?=} _beforeDismiss
     */
    constructor(_windowCmptRef, _contentRef, _backdropCmptRef, _beforeDismiss) {
        this._windowCmptRef = _windowCmptRef;
        this._contentRef = _contentRef;
        this._backdropCmptRef = _backdropCmptRef;
        this._beforeDismiss = _beforeDismiss;
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
        _windowCmptRef.instance.dismissEvent.subscribe((/**
         * @param {?} reason
         * @return {?}
         */
        (reason) => this.dismiss(reason)));
        _windowCmptRef.instance.modalOpeningDidStart.subscribe((/**
         * @return {?}
         */
        () => this.modalOpeningDidStart.next()));
        _windowCmptRef.instance.modalOpeningDidDone.subscribe((/**
         * @return {?}
         */
        () => this.modalOpeningDidDone.next()));
        _windowCmptRef.instance.modalClosingDidStart.subscribe((/**
         * @return {?}
         */
        () => this.modalClosingDidStart.next()));
        _windowCmptRef.instance.modalClosingDidDone.subscribe((/**
         * @return {?}
         */
        () => this.modalClosingDidDone.next()));
        _backdropCmptRef.instance.backdropOpeningDidStart.subscribe((/**
         * @return {?}
         */
        () => this.backdropOpeningDidStart.next()));
        _backdropCmptRef.instance.backdropOpeningDidDone.subscribe((/**
         * @return {?}
         */
        () => this.backdropOpeningDidDone.next()));
        _backdropCmptRef.instance.backdropClosingDidStart.subscribe((/**
         * @return {?}
         */
        () => this.backdropClosingDidStart.next()));
        _backdropCmptRef.instance.backdropClosingDidDone.subscribe((/**
         * @return {?}
         */
        () => this.backdropClosingDidDone.next()));
        this.result = new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this._resolve = resolve;
            this._reject = reject;
        }));
        this.result.then(null, (/**
         * @return {?}
         */
        () => { }));
    }
    /**
     * The instance of component used as modal's content.
     * Undefined when a TemplateRef is used as modal's content.
     * @return {?}
     */
    get componentInstance() {
        if (this._contentRef.componentRef) {
            return this._contentRef.componentRef.instance;
        }
    }
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    close(result) {
        if (this._windowCmptRef) {
            this._resolve(result);
            this._removeModalElements();
        }
    }
    /**
     * @private
     * @param {?=} reason
     * @return {?}
     */
    _dismiss(reason) {
        this._reject(reason);
        this._removeModalElements();
    }
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    dismiss(reason) {
        if (this._windowCmptRef) {
            if (!this._beforeDismiss) {
                this._dismiss(reason);
            }
            else {
                /** @type {?} */
                const dismiss = this._beforeDismiss();
                if (dismiss && dismiss.then) {
                    dismiss.then((/**
                     * @param {?} result
                     * @return {?}
                     */
                    result => {
                        if (result !== false) {
                            this._dismiss(reason);
                        }
                    }), (/**
                     * @return {?}
                     */
                    () => {
                    }));
                }
                else if (dismiss !== false) {
                    this._dismiss(reason);
                }
            }
        }
    }
    /**
     * @private
     * @return {?}
     */
    _removeModalElements() {
        this.modalClosingDidDone.subscribe((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const windowNativeEl = this._windowCmptRef.location.nativeElement;
            windowNativeEl.parentNode.removeChild(windowNativeEl);
            this._windowCmptRef.destroy();
            this._windowCmptRef = null;
            if (this._contentRef && this._contentRef.viewRef) {
                this._contentRef.viewRef.destroy();
            }
            this._contentRef = null;
        }));
        this._windowCmptRef.instance.animation = 'close';
        if (this._backdropCmptRef) {
            this.backdropClosingDidDone.subscribe((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const backdropNativeEl = this._backdropCmptRef.location.nativeElement;
                backdropNativeEl.parentNode.removeChild(backdropNativeEl);
                this._backdropCmptRef.destroy();
                this._backdropCmptRef = null;
            }));
            this._backdropCmptRef.instance.animation = 'close';
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const noop = (/**
 * @return {?}
 */
() => {
});
/**
 * Utility to handle the scrollbar.
 *
 * It allows to compensate the lack of a vertical scrollbar by adding an
 * equivalent padding on the right of the body, and to remove this compensation.
 */
class ScrollBar {
    /**
     * @param {?} _document
     */
    constructor(_document) {
        this._document = _document;
    }
    /**
     * Detects if a scrollbar is present and if yes, already compensates for its
     * removal by adding an equivalent padding on the right of the body.
     *
     * @return {?} a callback used to revert the compensation (noop if there was none,
     * otherwise a function removing the padding)
     */
    compensate() {
        return !this._isPresent() ? noop : this._adjustBody(this._getWidth());
    }
    /**
     * Adds a padding of the given width on the right of the body.
     *
     * @private
     * @param {?} width
     * @return {?} a callback used to revert the padding to its previous value
     */
    _adjustBody(width) {
        /** @type {?} */
        const body = this._document.body;
        /** @type {?} */
        const userSetPadding = body.style.paddingRight;
        /** @type {?} */
        const paddingAmount = parseFloat(window.getComputedStyle(body)['padding-right']);
        body.style['padding-right'] = `${paddingAmount + width}px`;
        return (/**
         * @return {?}
         */
        () => body.style['padding-right'] = userSetPadding);
    }
    /**
     * Tells whether a scrollbar is currently present on the body.
     *
     * @private
     * @return {?} true if scrollbar is present, false otherwise
     */
    _isPresent() {
        /** @type {?} */
        const rect = this._document.body.getBoundingClientRect();
        return rect.left + rect.right < window.innerWidth;
    }
    /**
     * Calculates and returns the width of a scrollbar.
     *
     * @private
     * @return {?} the width of a scrollbar on this page
     */
    _getWidth() {
        /** @type {?} */
        const measurer = this._document.createElement('div');
        measurer.className = 'modal-scrollbar-measure';
        /** @type {?} */
        const body = this._document.body;
        body.appendChild(measurer);
        /** @type {?} */
        const width = measurer.getBoundingClientRect().width - measurer.clientWidth;
        body.removeChild(measurer);
        return width;
    }
}
ScrollBar.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
ScrollBar.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] }
];
/** @nocollapse */ ScrollBar.ngInjectableDef = defineInjectable({ factory: function ScrollBar_Factory() { return new ScrollBar(inject(DOCUMENT)); }, token: ScrollBar, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const FOCUSABLE_ELEMENTS_SELECTOR = [
    'a[href]', 'button:not([disabled])', 'input:not([disabled]):not([type="hidden"])', 'select:not([disabled])',
    'textarea:not([disabled])', '[contenteditable]', '[tabindex]:not([tabindex="-1"])'
].join(', ');
/**
 * Returns first and last focusable elements inside of a given element based on specific CSS selector
 * @param {?} element
 * @return {?}
 */
function getFocusableBoundaryElements(element) {
    /** @type {?} */
    const list = Array.from((/** @type {?} */ (element.querySelectorAll(FOCUSABLE_ELEMENTS_SELECTOR))))
        .filter((/**
     * @param {?} el
     * @return {?}
     */
    el => el.tabIndex !== -1));
    return [list[0], list[list.length - 1]];
}
/**
 * Function that enforces browser focus to be trapped inside a DOM element.
 *
 * Works only for clicks inside the element and navigation with 'Tab', ignoring clicks outside of the element
 *
 * \@param element The element around which focus will be trapped inside
 * \@param stopFocusTrap$ The observable stream. When completed the focus trap will clean up listeners
 * and free internal resources
 * \@param refocusOnClick Put the focus back to the last focused element whenever a click occurs on element (default to
 * false)
 * @type {?}
 */
const ngtFocusTrap = (/**
 * @param {?} element
 * @param {?} stopFocusTrap$
 * @param {?=} refocusOnClick
 * @return {?}
 */
(element, stopFocusTrap$, refocusOnClick = false) => {
    // last focused element
    /** @type {?} */
    const lastFocusedElement$ = fromEvent(element, 'focusin').pipe(takeUntil(stopFocusTrap$), map((/**
     * @param {?} e
     * @return {?}
     */
    e => e.target)));
    // 'tab' / 'shift+tab' stream
    fromEvent(element, 'keydown')
        .pipe(takeUntil(stopFocusTrap$), 
    // tslint:disable:deprecation
    filter((/**
     * @param {?} e
     * @return {?}
     */
    e => e.which === Key.Tab)), 
    // tslint:enable:deprecation
    withLatestFrom(lastFocusedElement$))
        .subscribe((/**
     * @param {?} __0
     * @return {?}
     */
    ([tabEvent, focusedElement]) => {
        const [first, last] = getFocusableBoundaryElements(element);
        if ((focusedElement === first || focusedElement === element) && tabEvent.shiftKey) {
            last.focus();
            tabEvent.preventDefault();
        }
        if (focusedElement === last && !tabEvent.shiftKey) {
            first.focus();
            tabEvent.preventDefault();
        }
    }));
    // inside click
    if (refocusOnClick) {
        fromEvent(element, 'click')
            .pipe(takeUntil(stopFocusTrap$), withLatestFrom(lastFocusedElement$), map((/**
         * @param {?} arr
         * @return {?}
         */
        arr => (/** @type {?} */ (arr[1])))))
            .subscribe((/**
         * @param {?} lastFocusedElement
         * @return {?}
         */
        lastFocusedElement => lastFocusedElement.focus()));
    }
});

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const ModalDismissReasons = {
    BACKDROP_CLICK: 0,
    ESC: 1,
};
ModalDismissReasons[ModalDismissReasons.BACKDROP_CLICK] = 'BACKDROP_CLICK';
ModalDismissReasons[ModalDismissReasons.ESC] = 'ESC';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtModalWindowComponent {
    /**
     * @param {?} _document
     * @param {?} _elRef
     */
    constructor(_document, _elRef) {
        this._document = _document;
        this._elRef = _elRef;
        this.backdrop = true;
        this.keyboard = true;
        this.dismissEvent = new Subject();
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.animation = 'open';
        this.tabindex = '-1';
        this.ariaModal = true;
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onBackdropClick($event) {
        if (this.backdrop === true && this._elRef.nativeElement === $event.target) {
            this.dismiss(ModalDismissReasons.BACKDROP_CLICK);
        }
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onEscKey($event) {
        if (this.keyboard && !$event.defaultPrevented) {
            this.dismiss(ModalDismissReasons.ESC);
        }
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onAnimationStart($event) {
        this.animationAction($event);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onAnimationDone($event) {
        this.animationAction($event);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.class = 'modal fade show d-block' + (this.windowClass ? ' ' + this.windowClass : '');
    }
    /**
     * @param {?} reason
     * @return {?}
     */
    dismiss(reason) {
        this.dismissEvent.next(reason);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    animationAction($event) {
        switch ($event.phaseName) {
            case 'start':
                switch ($event.toState) {
                    case 'open':
                        this.modalOpeningDidStart.next();
                        break;
                    case 'close':
                        this.modalClosingDidStart.next();
                        break;
                }
                break;
            case 'done':
                switch ($event.toState) {
                    case 'open':
                        this.modalOpeningDidDone.next();
                        break;
                    case 'close':
                        this.modalClosingDidDone.next();
                        break;
                }
                break;
        }
    }
}
NgtModalWindowComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-modal-window',
                template: `
        <div [class]="'modal-dialog' + (size ? ' modal-' + size : '') + (centered ? ' modal-dialog-centered' : '') + (scrollableContent ? ' modal-dialog-scrollable' : '')" role="document">
            <div class="modal-content">
                <ng-content></ng-content>
            </div>
        </div>
    `,
                animations: [
                    trigger('animation', [
                        state('close', style({ opacity: 0, transform: 'scale(0, 0)' })),
                        transition('void => *', [
                            style({ opacity: 0, transform: 'scale(0, 0)' }),
                            animate('0.3s cubic-bezier(0.680, -0.550, 0.265, 1.550)')
                        ]),
                        transition('* => close', animate('0.3s cubic-bezier(0.680, -0.550, 0.265, 1.550)'))
                    ])
                ]
            }] }
];
/** @nocollapse */
NgtModalWindowComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: ElementRef }
];
NgtModalWindowComponent.propDecorators = {
    backdrop: [{ type: Input }],
    centered: [{ type: Input }],
    keyboard: [{ type: Input }],
    size: [{ type: Input }],
    scrollableContent: [{ type: Input }],
    windowClass: [{ type: Input }],
    dialogClass: [{ type: Input }],
    dismissEvent: [{ type: Output }],
    modalClosingDidStart: [{ type: Output }],
    modalClosingDidDone: [{ type: Output }],
    modalOpeningDidStart: [{ type: Output }],
    modalOpeningDidDone: [{ type: Output }],
    animation: [{ type: HostBinding, args: ['@animation',] }],
    class: [{ type: HostBinding, args: ['class',] }],
    tabindex: [{ type: HostBinding, args: ['tabindex',] }],
    ariaModal: [{ type: HostBinding, args: ['attr.aria-modal.true',] }],
    onBackdropClick: [{ type: HostListener, args: ['click', ['$event'],] }],
    onEscKey: [{ type: HostListener, args: ['document:keyup.esc', ['$event'],] }],
    onAnimationStart: [{ type: HostListener, args: ['@animation.start', ['$event'],] }],
    onAnimationDone: [{ type: HostListener, args: ['@animation.done', ['$event'],] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtModalBackdropComponent {
    constructor() {
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.zIndex = '1050';
        this.animation = 'start';
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onAnimationStart($event) {
        this.animationAction($event);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onAnimationDone($event) {
        this.animationAction($event);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.class = 'modal-backdrop fade show' + (this.backdropClass ? ' ' + this.backdropClass : '');
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    animationAction($event) {
        switch ($event.phaseName) {
            case 'start':
                switch ($event.toState) {
                    case 'start':
                        this.backdropOpeningDidStart.next();
                        break;
                    case 'close':
                        this.backdropClosingDidStart.next();
                        break;
                }
                break;
            case 'done':
                switch ($event.toState) {
                    case 'start':
                        this.backdropOpeningDidDone.next();
                        break;
                    case 'close':
                        this.backdropClosingDidDone.next();
                        break;
                }
                break;
        }
    }
}
NgtModalBackdropComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-modal-backdrop',
                template: '',
                animations: [
                    trigger('animation', [
                        state('close', style({ opacity: 0 })),
                        transition('void => *', [
                            style({ opacity: 0 }),
                            animate(200)
                        ]),
                        transition('* => void', [
                            animate(200, style({ opacity: 0 }))
                        ]),
                        transition('* => close', animate('0.3s'))
                    ])
                ]
            }] }
];
NgtModalBackdropComponent.propDecorators = {
    backdropClass: [{ type: Input }],
    backdropClosingDidStart: [{ type: Output, args: ['backdropClosingDidStart',] }],
    backdropClosingDidDone: [{ type: Output, args: ['backdropClosingDidDone',] }],
    backdropOpeningDidStart: [{ type: Output, args: ['backdropOpeningDidStart',] }],
    backdropOpeningDidDone: [{ type: Output, args: ['backdropOpeningDidDone',] }],
    class: [{ type: HostBinding, args: ['class',] }],
    zIndex: [{ type: HostBinding, args: ['style.z-index',] }],
    animation: [{ type: HostBinding, args: ['@animation',] }],
    onAnimationStart: [{ type: HostListener, args: ['@animation.start', ['$event'],] }],
    onAnimationDone: [{ type: HostListener, args: ['@animation.done', ['$event'],] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class ContentRef {
    /**
     * @param {?} nodes
     * @param {?=} viewRef
     * @param {?=} componentRef
     */
    constructor(nodes, viewRef, componentRef) {
        this.nodes = nodes;
        this.viewRef = viewRef;
        this.componentRef = componentRef;
    }
}
/**
 * @template T
 */
class PopupService {
    /**
     * @param {?} _type
     * @param {?} _injector
     * @param {?} _viewContainerRef
     * @param {?} _renderer
     * @param {?} _componentFactoryResolver
     */
    constructor(_type, _injector, _viewContainerRef, _renderer, _componentFactoryResolver) {
        this._type = _type;
        this._injector = _injector;
        this._viewContainerRef = _viewContainerRef;
        this._renderer = _renderer;
        this._componentFactoryResolver = _componentFactoryResolver;
    }
    /**
     * @param {?=} content
     * @param {?=} context
     * @return {?}
     */
    open(content, context) {
        if (!this._windowRef) {
            this._contentRef = this._getContentRef(content, context);
            this._windowRef = this._viewContainerRef.createComponent(this._componentFactoryResolver.resolveComponentFactory(this._type), 0, this._injector, this._contentRef.nodes);
        }
        return this._windowRef;
    }
    /**
     * @return {?}
     */
    close() {
        if (this._windowRef) {
            this._viewContainerRef.remove(this._viewContainerRef.indexOf(this._windowRef.hostView));
            this._windowRef = null;
            if (this._contentRef.viewRef) {
                this._viewContainerRef.remove(this._viewContainerRef.indexOf(this._contentRef.viewRef));
                this._contentRef = null;
            }
        }
    }
    /**
     * @private
     * @param {?} content
     * @param {?=} context
     * @return {?}
     */
    _getContentRef(content, context) {
        if (!content) {
            return new ContentRef([]);
        }
        else if (content instanceof TemplateRef) {
            /** @type {?} */
            const viewRef = this._viewContainerRef.createEmbeddedView((/** @type {?} */ (content)), context);
            return new ContentRef([viewRef.rootNodes], viewRef);
        }
        else {
            return new ContentRef([[this._renderer.createText(`${content}`)]]);
        }
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtModalStack {
    /**
     * @param {?} _applicationRef
     * @param {?} _injector
     * @param {?} _document
     * @param {?} _scrollBar
     * @param {?} _rendererFactory
     */
    constructor(_applicationRef, _injector, _document, _scrollBar, _rendererFactory) {
        this._applicationRef = _applicationRef;
        this._injector = _injector;
        this._document = _document;
        this._scrollBar = _scrollBar;
        this._rendererFactory = _rendererFactory;
        this._activeWindowCmptHasChanged = new Subject();
        this._ariaHiddenValues = new Map();
        this._modalRefs = [];
        this._backdropAttributes = ['backdropClass'];
        this._windowAttributes = ['backdrop', 'centered', 'keyboard', 'size', 'scrollableContent', 'windowClass'];
        this._backdropEvents = ['backdropOpeningDidStart', 'backdropOpeningDidDone', 'backdropClosingDidStart', 'backdropClosingDidDone'];
        this._windowEvents = ['modalOpeningDidStart', 'modalOpeningDidDone', 'modalClosingDidStart', 'modalClosingDidDone'];
        this._windowCmpts = [];
        // Trap focus on active WindowCmpt
        this._activeWindowCmptHasChanged.subscribe((/**
         * @return {?}
         */
        () => {
            if (this._windowCmpts.length) {
                /** @type {?} */
                const activeWindowCmpt = this._windowCmpts[this._windowCmpts.length - 1];
                ngtFocusTrap(activeWindowCmpt.location.nativeElement, this._activeWindowCmptHasChanged);
                this._revertAriaHidden();
                this._setAriaHidden(activeWindowCmpt.location.nativeElement);
            }
        }));
    }
    /**
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @param {?} options
     * @return {?}
     */
    open(moduleCFR, contentInjector, content, options) {
        /** @type {?} */
        const containerEl = isDefined(options.container) ? this._document.querySelector(options.container) : this._document.body;
        /** @type {?} */
        const renderer = this._rendererFactory.createRenderer(null, null);
        /** @type {?} */
        const revertPaddingForScrollBar = this._scrollBar.compensate();
        /** @type {?} */
        const removeBodyClass = (/**
         * @return {?}
         */
        () => {
            if (!this._modalRefs.length) {
                renderer.removeClass(this._document.body, 'modal-open');
            }
        });
        if (!containerEl) {
            throw new Error(`The specified modal container "${options.container || 'body'}" was not found in the DOM.`);
        }
        const { contentRef, activeModal } = this._getContentRef(moduleCFR, options.injector || contentInjector, content);
        /** @type {?} */
        const backdropCmptRef = options.backdrop !== false ? this._attachBackdrop(moduleCFR, containerEl) : null;
        /** @type {?} */
        const windowCmptRef = this._attachWindowComponent(moduleCFR, containerEl, contentRef);
        /** @type {?} */
        const ngtModalRef = new NgtModalRef(windowCmptRef, contentRef, backdropCmptRef, options.beforeDismiss);
        this._registerModalRef(ngtModalRef);
        this._registerWindowCmpt(windowCmptRef);
        ngtModalRef.result.then(revertPaddingForScrollBar, revertPaddingForScrollBar);
        ngtModalRef.result.then(removeBodyClass, removeBodyClass);
        activeModal.close = (/**
         * @param {?} result
         * @return {?}
         */
        (result) => {
            ngtModalRef.close(result);
        });
        activeModal.dismiss = (/**
         * @param {?} reason
         * @return {?}
         */
        (reason) => {
            ngtModalRef.dismiss(reason);
        });
        this._applyWindowOptions(windowCmptRef.instance, options);
        this._applyEvents(ngtModalRef, activeModal, this._windowEvents);
        if (this._modalRefs.length === 1) {
            renderer.addClass(this._document.body, 'modal-open');
        }
        if (backdropCmptRef && backdropCmptRef.instance) {
            this._applyBackdropOptions(backdropCmptRef.instance, options);
            this._applyEvents(ngtModalRef, activeModal, this._backdropEvents);
        }
        return ngtModalRef;
    }
    /**
     * @param {?=} reason
     * @return {?}
     */
    dismissAll(reason) {
        this._modalRefs.forEach((/**
         * @param {?} ngtModalRef
         * @return {?}
         */
        ngtModalRef => ngtModalRef.dismiss(reason)));
    }
    /**
     * @return {?}
     */
    hasOpenModals() {
        return this._modalRefs.length > 0;
    }
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @return {?}
     */
    _attachBackdrop(moduleCFR, containerEl) {
        /** @type {?} */
        const backdropFactory = moduleCFR.resolveComponentFactory(NgtModalBackdropComponent);
        /** @type {?} */
        const backdropCmptRef = backdropFactory.create(this._injector);
        this._applicationRef.attachView(backdropCmptRef.hostView);
        containerEl.appendChild(backdropCmptRef.location.nativeElement);
        return backdropCmptRef;
    }
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @param {?} contentRef
     * @return {?}
     */
    _attachWindowComponent(moduleCFR, containerEl, contentRef) {
        /** @type {?} */
        const windowFactory = moduleCFR.resolveComponentFactory(NgtModalWindowComponent);
        /** @type {?} */
        const windowCmptRef = windowFactory.create(this._injector, contentRef.nodes);
        this._applicationRef.attachView(windowCmptRef.hostView);
        containerEl.appendChild(windowCmptRef.location.nativeElement);
        return windowCmptRef;
    }
    /**
     * @private
     * @param {?} windowInstance
     * @param {?} options
     * @return {?}
     */
    _applyWindowOptions(windowInstance, options) {
        this._windowAttributes.forEach((/**
         * @param {?} optionName
         * @return {?}
         */
        (optionName) => {
            if (isDefined(options[optionName])) {
                windowInstance[optionName] = options[optionName];
            }
        }));
    }
    /**
     * @private
     * @param {?} backdropInstance
     * @param {?} options
     * @return {?}
     */
    _applyBackdropOptions(backdropInstance, options) {
        this._backdropAttributes.forEach((/**
         * @param {?} optionName
         * @return {?}
         */
        (optionName) => {
            if (isDefined(options[optionName])) {
                backdropInstance[optionName] = options[optionName];
            }
        }));
    }
    /**
     * @private
     * @param {?} instanceToSubscribe
     * @param {?} instanceToTrigger
     * @param {?} events
     * @return {?}
     */
    _applyEvents(instanceToSubscribe, instanceToTrigger, events) {
        events.forEach((/**
         * @param {?} eventName
         * @return {?}
         */
        (eventName) => {
            if (isDefined(instanceToSubscribe[eventName]) && isDefined(instanceToTrigger[eventName])) {
                instanceToSubscribe[eventName].subscribe((/**
                 * @return {?}
                 */
                () => instanceToTrigger[eventName].next()));
            }
        }));
    }
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @return {?}
     */
    _getContentRef(moduleCFR, contentInjector, content) {
        /** @type {?} */
        let activeModal = new NgtActiveModal();
        /** @type {?} */
        let contentRef;
        if (!content) {
            contentRef = new ContentRef([]);
        }
        else if (content instanceof TemplateRef) {
            contentRef = this._createFromTemplateRef(content, activeModal);
        }
        else if (isString(content)) {
            contentRef = this._createFromString(content);
        }
        else if (typeof content === 'function') {
            contentRef = this._createFromComponentConstructor(moduleCFR, contentInjector, content, activeModal);
        }
        else {
            contentRef = this._createFromComponentRef(content);
            activeModal = contentRef.componentRef.activeModal || activeModal;
        }
        return { contentRef, activeModal };
    }
    /**
     * @private
     * @param {?} content
     * @param {?} activeModal
     * @return {?}
     */
    _createFromTemplateRef(content, activeModal) {
        /** @type {?} */
        const context = {
            $implicit: activeModal,
            /**
             * @param {?} result
             * @return {?}
             */
            close(result) {
                activeModal.close(result);
            },
            /**
             * @param {?} reason
             * @return {?}
             */
            dismiss(reason) {
                activeModal.dismiss(reason);
            }
        };
        /** @type {?} */
        const viewRef = content.createEmbeddedView(context);
        this._applicationRef.attachView(viewRef);
        return new ContentRef([viewRef.rootNodes], viewRef);
    }
    /**
     * @private
     * @param {?} content
     * @return {?}
     */
    _createFromString(content) {
        /** @type {?} */
        const component = this._document.createTextNode(`${content}`);
        return new ContentRef([[component]]);
    }
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @param {?} context
     * @return {?}
     */
    _createFromComponentConstructor(moduleCFR, contentInjector, content, context) {
        /** @type {?} */
        const contentCmptFactory = moduleCFR.resolveComponentFactory(content);
        /** @type {?} */
        const modalContentInjector = Injector.create({ providers: [{ provide: NgtActiveModal, useValue: context }], parent: contentInjector });
        /** @type {?} */
        const componentRef = contentCmptFactory.create(modalContentInjector);
        this._applicationRef.attachView(componentRef.hostView);
        return new ContentRef([[componentRef.location.nativeElement]], componentRef.hostView, componentRef);
    }
    /**
     * @private
     * @param {?} componentRef
     * @return {?}
     */
    _createFromComponentRef(componentRef) {
        return new ContentRef([[componentRef._elRef.nativeElement]], null, componentRef);
    }
    /**
     * @private
     * @param {?} element
     * @return {?}
     */
    _setAriaHidden(element) {
        /** @type {?} */
        const parent = element.parentElement;
        if (parent && element !== this._document.body) {
            Array.from(parent.children).forEach((/**
             * @param {?} sibling
             * @return {?}
             */
            sibling => {
                if (sibling !== element && sibling.nodeName !== 'SCRIPT') {
                    this._ariaHiddenValues.set(sibling, sibling.getAttribute('aria-hidden'));
                    sibling.setAttribute('aria-hidden', 'true');
                }
            }));
            this._setAriaHidden(parent);
        }
    }
    /**
     * @private
     * @return {?}
     */
    _revertAriaHidden() {
        this._ariaHiddenValues.forEach((/**
         * @param {?} value
         * @param {?} element
         * @return {?}
         */
        (value, element) => {
            if (value) {
                element.setAttribute('aria-hidden', value);
            }
            else {
                element.removeAttribute('aria-hidden');
            }
        }));
        this._ariaHiddenValues.clear();
    }
    /**
     * @private
     * @param {?} ngtModalRef
     * @return {?}
     */
    _registerModalRef(ngtModalRef) {
        /** @type {?} */
        const unregisterModalRef = (/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const index = this._modalRefs.indexOf(ngtModalRef);
            if (index > -1) {
                this._modalRefs.splice(index, 1);
            }
        });
        this._modalRefs.push(ngtModalRef);
        ngtModalRef.result.then(unregisterModalRef, unregisterModalRef);
    }
    /**
     * @private
     * @param {?} ngtWindowCmpt
     * @return {?}
     */
    _registerWindowCmpt(ngtWindowCmpt) {
        this._windowCmpts.push(ngtWindowCmpt);
        this._activeWindowCmptHasChanged.next();
        ngtWindowCmpt.onDestroy((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const index = this._windowCmpts.indexOf(ngtWindowCmpt);
            if (index > -1) {
                this._windowCmpts.splice(index, 1);
                this._activeWindowCmptHasChanged.next();
            }
        }));
    }
}
NgtModalStack.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
NgtModalStack.ctorParameters = () => [
    { type: ApplicationRef },
    { type: Injector },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: ScrollBar },
    { type: RendererFactory2 }
];
/** @nocollapse */ NgtModalStack.ngInjectableDef = defineInjectable({ factory: function NgtModalStack_Factory() { return new NgtModalStack(inject(ApplicationRef), inject(INJECTOR), inject(DOCUMENT), inject(ScrollBar), inject(RendererFactory2)); }, token: NgtModalStack, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * A service to open modal windows. Creating a modal is straightforward: create a template and pass it as an argument to
 * the "open" method!
 */
class NgtModalService {
    /**
     * @param {?} _moduleCFR
     * @param {?} _injector
     * @param {?} _modalStack
     * @param {?} _config
     */
    constructor(_moduleCFR, _injector, _modalStack, _config) {
        this._moduleCFR = _moduleCFR;
        this._injector = _injector;
        this._modalStack = _modalStack;
        this._config = _config;
        this._components = [];
    }
    /**
     * Opens a new modal window with the specified content and using supplied options. Content can be provided
     * as a TemplateRef or a component type. If you pass a component type as content, then instances of those
     * components can be injected with an instance of the NgtActiveModal class. You can use methods on the
     * NgtActiveModal class to close / dismiss modals from "inside" of a component.
     * @param {?} content
     * @param {?=} options
     * @return {?}
     */
    open(content, options = {}) {
        /** @type {?} */
        const combinedOptions = Object.assign({}, this._config, options);
        return this._modalStack.open(this._moduleCFR, this._injector, content, combinedOptions);
    }
    /**
     * Dismiss all currently displayed modal windows with the supplied reason.
     * @param {?=} reason
     * @return {?}
     */
    dismissAll(reason) {
        this._modalStack.dismissAll(reason);
    }
    /**
     * Indicates if there are currently any open modal windows in the application.
     * @return {?}
     */
    hasOpenModals() {
        return this._modalStack.hasOpenModals();
    }
    /**
     * Add modal component instance to _components list.
     * !NOTE: modal must have id;
     * @param {?} componentRef
     * @return {?}
     */
    add(componentRef) {
        this._components.push(componentRef);
    }
    /**
     * Remove modal component instance from _components list.
     * @param {?} id
     * @return {?}
     */
    remove(id) {
        /** @type {?} */
        const modalToRemove = find(this._components, { id: id });
        this._components = without(this._components, modalToRemove);
    }
    /**
     * Opens a new modal window with the specified content and using supplied options founded in
     * _components list by specified id.
     * @param {?} id
     * @return {?}
     */
    openById(id) {
        /** @type {?} */
        const modalToOpen = find(this._components, { id: id });
        this.open(modalToOpen, modalToOpen.options);
    }
    /**
     * Call close method in modal component instance founded by specified id.
     * @param {?} id
     * @param {?=} result
     * @return {?}
     */
    closeById(id, result) {
        find(this._components, { id: id }).close(result);
    }
    /**
     * Call dismiss method in modal component instance founded by specified id.
     * @param {?} id
     * @param {?=} reason
     * @return {?}
     */
    dismissById(id, reason) {
        find(this._components, { id: id }).dismiss(reason);
    }
}
NgtModalService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
NgtModalService.ctorParameters = () => [
    { type: ComponentFactoryResolver },
    { type: Injector },
    { type: NgtModalStack },
    { type: NgtModalConfig }
];
/** @nocollapse */ NgtModalService.ngInjectableDef = defineInjectable({ factory: function NgtModalService_Factory() { return new NgtModalService(inject(ComponentFactoryResolver), inject(INJECTOR), inject(NgtModalStack), inject(NgtModalConfig)); }, token: NgtModalService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtModalComponent {
    /**
     * @param {?} activeModal
     * @param {?} _modalService
     * @param {?} _config
     * @param {?} _elRef
     */
    constructor(activeModal, _modalService, _config, _elRef) {
        this.activeModal = activeModal;
        this._modalService = _modalService;
        this._config = _config;
        this._elRef = _elRef;
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.options = {};
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        // set options
        if (isDefined(this.backdrop)) {
            this.options.backdrop = this.backdrop;
        }
        if (isDefined(this.beforeDismiss)) {
            this.options.beforeDismiss = this.beforeDismiss;
        }
        if (isDefined(this.centered)) {
            this.options.centered = this.centered;
        }
        if (isDefined(this.container)) {
            this.options.container = this.container;
        }
        if (isDefined(this.keyboard)) {
            this.options.keyboard = this.keyboard;
        }
        if (isDefined(this.size)) {
            this.options.size = this.size;
        }
        if (isDefined(this.scrollableContent)) {
            this.options.scrollableContent = this.scrollableContent;
        }
        if (isDefined(this.windowClass)) {
            this.options.windowClass = this.windowClass;
        }
        if (isDefined(this.backdropClass)) {
            this.options.backdropClass = this.backdropClass;
        }
        // apply events
        this.activeModal.modalOpeningDidStart.subscribe((/**
         * @return {?}
         */
        () => this.modalOpeningDidStart.next()));
        this.activeModal.modalOpeningDidDone.subscribe((/**
         * @return {?}
         */
        () => this.modalOpeningDidDone.next()));
        this.activeModal.modalClosingDidStart.subscribe((/**
         * @return {?}
         */
        () => this.modalClosingDidStart.next()));
        this.activeModal.modalClosingDidDone.subscribe((/**
         * @return {?}
         */
        () => this.modalClosingDidDone.next()));
        this.activeModal.backdropOpeningDidStart.subscribe((/**
         * @return {?}
         */
        () => this.backdropOpeningDidStart.next()));
        this.activeModal.backdropOpeningDidDone.subscribe((/**
         * @return {?}
         */
        () => this.backdropOpeningDidDone.next()));
        this.activeModal.backdropClosingDidStart.subscribe((/**
         * @return {?}
         */
        () => this.backdropClosingDidStart.next()));
        this.activeModal.backdropClosingDidDone.subscribe((/**
         * @return {?}
         */
        () => this.backdropClosingDidDone.next()));
        if (!this.id) {
            // console.error('modal must have an id');
            return;
        }
        this._modalService.add(this);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this._modalService.remove(this.id);
    }
    /**
     * Open the modal with an modal component reference without id.
     * @return {?}
     */
    open() {
        this._modalService.open(this, this.options);
    }
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    close(result) {
        this.activeModal.close(result);
    }
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    dismiss(reason) {
        this.activeModal.dismiss(reason);
    }
}
NgtModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-modal',
                template: `
        <ng-content></ng-content>
    `,
                providers: [NgtActiveModal]
            }] }
];
/** @nocollapse */
NgtModalComponent.ctorParameters = () => [
    { type: NgtActiveModal },
    { type: NgtModalService },
    { type: NgtModalConfig },
    { type: ElementRef }
];
NgtModalComponent.propDecorators = {
    id: [{ type: Input }],
    backdrop: [{ type: Input }],
    beforeDismiss: [{ type: Input }],
    centered: [{ type: Input }],
    container: [{ type: Input }],
    keyboard: [{ type: Input }],
    size: [{ type: Input }],
    scrollableContent: [{ type: Input }],
    windowClass: [{ type: Input }],
    backdropClass: [{ type: Input }],
    modalClosingDidStart: [{ type: Output }],
    modalClosingDidDone: [{ type: Output }],
    modalOpeningDidStart: [{ type: Output }],
    modalOpeningDidDone: [{ type: Output }],
    backdropClosingDidStart: [{ type: Output }],
    backdropClosingDidDone: [{ type: Output }],
    backdropOpeningDidStart: [{ type: Output }],
    backdropOpeningDidDone: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtModalOpenDirective {
    /**
     * @param {?} _modalService
     */
    constructor(_modalService) {
        this._modalService = _modalService;
    }
    /**
     * @return {?}
     */
    onClick() {
        this._modalService.openById(this.id);
    }
}
NgtModalOpenDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtModalOpen]'
            },] }
];
/** @nocollapse */
NgtModalOpenDirective.ctorParameters = () => [
    { type: NgtModalService }
];
NgtModalOpenDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtModalOpen',] }],
    onClick: [{ type: HostListener, args: ['click',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtModalCloseDirective {
    /**
     * @param {?} _modalService
     */
    constructor(_modalService) {
        this._modalService = _modalService;
    }
    /**
     * @return {?}
     */
    onClick() {
        this._modalService.closeById(this.id, this.result);
    }
}
NgtModalCloseDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtModalClose]'
            },] }
];
/** @nocollapse */
NgtModalCloseDirective.ctorParameters = () => [
    { type: NgtModalService }
];
NgtModalCloseDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtModalClose',] }],
    result: [{ type: Input }],
    onClick: [{ type: HostListener, args: ['click',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtModalDismissDirective {
    /**
     * @param {?} _modalService
     */
    constructor(_modalService) {
        this._modalService = _modalService;
    }
    /**
     * @return {?}
     */
    onClick() {
        this._modalService.dismissById(this.id, this.reason);
    }
}
NgtModalDismissDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtModalDismiss]'
            },] }
];
/** @nocollapse */
NgtModalDismissDirective.ctorParameters = () => [
    { type: NgtModalService }
];
NgtModalDismissDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtModalDismiss',] }],
    reason: [{ type: Input }],
    onClick: [{ type: HostListener, args: ['click',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGC_MODAL_DIRECTIVES = [
    NgtModalOpenDirective,
    NgtModalCloseDirective,
    NgtModalDismissDirective,
    NgtModalWindowComponent,
    NgtModalBackdropComponent,
    NgtModalComponent
];
class NgtModalModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtModalModule };
    }
}
NgtModalModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NGC_MODAL_DIRECTIVES],
                exports: [NGC_MODAL_DIRECTIVES],
                imports: [CommonModule],
                providers: [NgtModalService],
                entryComponents: [NgtModalComponent, NgtModalWindowComponent, NgtModalBackdropComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtLoaderService {
    constructor() {
        this.loaders = [];
        this.isloaderClosed = new Subject();
        this.isloaderOpened = new Subject();
    }
    /**
     * @param {?} loader
     * @return {?}
     */
    add(loader) {
        // add loader to array of active loaders-page
        this.loaders.push(loader);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    remove(id) {
        // remove loader from array of active loaders-page
        /** @type {?} */
        const loaderToRemove = find(this.loaders, { id: id });
        this.loaders = without(this.loaders, loaderToRemove);
    }
    /**
     * @param {?} data
     * @return {?}
     */
    create(data) {
        /** @type {?} */
        const loader = find(this.loaders, { id: data.id });
        if (!loader) {
            console.error('Loader instance with id "' + data.id + '" not defined!');
            return null;
        }
        else {
            loader.content = data.content ? data.content : null;
        }
        return loader;
    }
}
NgtLoaderService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ NgtLoaderService.ngInjectableDef = defineInjectable({ factory: function NgtLoaderService_Factory() { return new NgtLoaderService(); }, token: NgtLoaderService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtLoaderComponent {
    /**
     * @param {?} _loaderService
     * @param {?} _cdr
     */
    constructor(_loaderService, _cdr) {
        this._loaderService = _loaderService;
        this._cdr = _cdr;
        this.content = null;
        /**
         *  Spinner type: 'border' | 'grow';
         */
        this.type = 'border';
        this._isPresent = false;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set isPresent(value) {
        this._isPresent = value;
    }
    /**
     * @return {?}
     */
    get isPresent() {
        return this._isPresent;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        // ensure id attribute exists
        if (!this.id) {
            console.error('loader must have an id');
            return;
        }
        // add self (this loader instance) to the loader service so it's accessible from controllers
        this._loaderService.add(this);
    }
    // remove self from loader service when directive is destroyed
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this._loaderService.remove(this.id);
    }
    // open loader
    /**
     * @return {?}
     */
    present() {
        this.isPresent = true;
        this._cdr.detectChanges();
        return this._loaderService.isloaderOpened;
    }
    // close loader
    /**
     * @return {?}
     */
    dismiss() {
        this.isPresent = false;
        this._cdr.detectChanges();
        return this._loaderService.isloaderClosed;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    animationDone(event) {
        if (event.toState === 'close') {
            this._loaderService.isloaderClosed.next();
        }
        if (event.toState === 'open') {
            this._loaderService.isloaderOpened.next();
        }
    }
}
NgtLoaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-loader-component',
                template: `
        <div
            class="loader"
            [ngClass]="{'-single': !content}"
            [@loading]='isPresent ? "open" : "close"'
            (@loading.done)="animationDone($event)">
            <div class="loader_overlay"></div>
            <div class="loader_wrapper">
                <div class="loader_spinner-wrap">
                    <div class="spinner-{{ type }} {{ spinnerClass }}" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <p *ngIf="content">{{ content }}</p>
            </div>
        </div>

    `,
                animations: [
                    trigger('loading', [
                        state('open', style({ opacity: 1, visibility: 'visible' })),
                        state('close', style({ opacity: 0, visibility: 'hidden' })),
                        transition('close => open', animate('0.3s linear')),
                        transition('open => close', animate('0.2s linear'))
                    ])
                ]
            }] }
];
/** @nocollapse */
NgtLoaderComponent.ctorParameters = () => [
    { type: NgtLoaderService },
    { type: ChangeDetectorRef }
];
NgtLoaderComponent.propDecorators = {
    id: [{ type: Input }],
    type: [{ type: Input }],
    spinnerClass: [{ type: Input }],
    isPresent: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Loader instance
 */
class NgtLoaderDirective {
    /**
     * @param {?} resolver
     * @param {?} viewContainerRef
     * @param {?} elRef
     */
    constructor(resolver, viewContainerRef, elRef) {
        this.resolver = resolver;
        this.viewContainerRef = viewContainerRef;
        this.elRef = elRef;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.id) {
            console.error('loader must have an id');
            return;
        }
        /** @type {?} */
        const factory = this.resolver.resolveComponentFactory(NgtLoaderComponent);
        this.viewContainerRef.clear();
        this.componentRef = this.viewContainerRef.createComponent(factory);
        this.componentRef.instance.id = this.id;
        this.elRef.nativeElement.appendChild(this.componentRef.location.nativeElement);
    }
}
NgtLoaderDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtLoader]'
            },] }
];
/** @nocollapse */
NgtLoaderDirective.ctorParameters = () => [
    { type: ComponentFactoryResolver },
    { type: ViewContainerRef },
    { type: ElementRef }
];
NgtLoaderDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtLoader',] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtLoader {
    /**
     * @param {?} id
     * @param {?} content
     */
    constructor(id, content) {
        this.id = id;
        this.content = content;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGC_LOADER_DIRECTIVES = [
    NgtLoaderComponent, NgtLoaderDirective
];
class NgtLoaderModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtLoaderModule };
    }
}
NgtLoaderModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NGC_LOADER_DIRECTIVES],
                exports: [NGC_LOADER_DIRECTIVES],
                imports: [CommonModule],
                entryComponents: [NgtLoaderComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtNotificationComponent {
    /**
     * @param {?} elRef
     */
    constructor(elRef) {
        this.elRef = elRef;
        this.animation = true;
        this.display = 'block';
        this.aside = false;
        this.timer = new Subject();
        this.progress = 0;
        this.lastProgress = 0;
        this.result = new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this._resolve = resolve;
            this._reject = reject;
        }));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.timeout > 0) {
            this.initProgressBar();
        }
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.timeout && this.timer.next();
    }
    /**
     * @return {?}
     */
    initProgressBar() {
        /** @type {?} */
        const k = this.timeout / 100;
        this.progressBar = this.elRef.nativeElement.querySelector('.notification-progress-bar');
        /** @type {?} */
        const sub = this.timer.subscribe((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const started = new Date().getTime();
            this.interval = setInterval((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const curTime = new Date().getTime();
                this.progress = this.lastProgress + (curTime - started) / k;
                if (this.progress >= 100) {
                    clearInterval(this.interval);
                    sub.unsubscribe();
                    this._ref.destroy();
                }
                else {
                    this.progressBar['style'].width = this.progress + '%';
                }
            }), 0);
        }));
    }
    /**
     * @return {?}
     */
    closeNotification() {
        this._resolve();
        clearInterval(this.interval);
        this._ref.destroy();
    }
    /**
     * @return {?}
     */
    onMouseenter() {
        clearInterval(this.interval);
        this.lastProgress = this.progress;
    }
    /**
     * @return {?}
     */
    onMouseleave() {
        this.timer.next();
    }
}
NgtNotificationComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-notification',
                template: `
        <div
            (mouseenter)="onMouseenter()" (mouseleave)="onMouseleave()"
            class="notification show {{ typeClass }} {{ aside ? 'with-aside' : '' }}">
            <div class="notification-progress-bar"></div>
            <div *ngIf="aside" class="notification-aside" [innerHtml]="aside"></div>
            <div class="notification-inner">
                <div class="notification-header">
                    <span class="notification-title" [innerHtml]="title"></span>
                    <button (click)="closeNotification()" class="notification-close"><i class="ft-x"></i></button>
                </div>
                <div class="notification-body" [innerHtml]="message"></div>
            </div>
        </div>

    `,
                animations: [
                    trigger('flyInOut', [
                        transition('void => *', [
                            style({ transform: 'translateX(-100%)', opacity: 0 }),
                            animate(150)
                        ]),
                        transition('* => void', [
                            animate(250, style({ transform: 'translateX(100%)', opacity: 0 }))
                        ])
                    ])
                ]
            }] }
];
/** @nocollapse */
NgtNotificationComponent.ctorParameters = () => [
    { type: ElementRef }
];
NgtNotificationComponent.propDecorators = {
    animation: [{ type: HostBinding, args: ['@flyInOut',] }],
    display: [{ type: HostBinding, args: ['style.display',] }],
    type: [{ type: Input }],
    typeClass: [{ type: Input }],
    title: [{ type: Input }],
    message: [{ type: Input }],
    aside: [{ type: Input }],
    timeout: [{ type: Input }],
    _ref: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtNotification {
}
/** @enum {number} */
const NgtNotificationType = {
    Success: 0,
    Error: 1,
    Info: 2,
    Warning: 3,
};
NgtNotificationType[NgtNotificationType.Success] = 'Success';
NgtNotificationType[NgtNotificationType.Error] = 'Error';
NgtNotificationType[NgtNotificationType.Info] = 'Info';
NgtNotificationType[NgtNotificationType.Warning] = 'Warning';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtNotificationStack {
    /**
     * @param {?} _injector
     * @param {?} _appRef
     * @param {?} _rendererFactory
     * @param {?} _document
     */
    constructor(_injector, _appRef, _rendererFactory, _document) {
        this._injector = _injector;
        this._appRef = _appRef;
        this._rendererFactory = _rendererFactory;
        this._document = _document;
        this.containerEl = this._document.body;
        this._notificationRefs = [];
        this._notificationAttributes = ['type', 'message', 'timeout', 'typeClass', 'aside', 'title', '_ref'];
        this.initContainer();
    }
    /**
     * @return {?}
     */
    initContainer() {
        /** @type {?} */
        const renderer = this._rendererFactory.createRenderer(null, null);
        /** @type {?} */
        const containerEl = renderer.createElement('div');
        /** @type {?} */
        const wrapperEl = renderer.createElement('div');
        renderer.addClass(containerEl, 'notification-overlay-container');
        renderer.addClass(wrapperEl, 'notification-wrapper');
        renderer.appendChild(containerEl, wrapperEl);
        renderer.appendChild(this._document.body, containerEl);
        this.containerEl = wrapperEl;
    }
    /**
     * @param {?} moduleCFR
     * @param {?} options
     * @return {?}
     */
    show(moduleCFR, options) {
        /** @type {?} */
        const notificationCmptRef = this._attachNotificationComponent(moduleCFR, this.containerEl);
        this._registerNotificationRef(notificationCmptRef.instance);
        this._applyNotificationOptions(notificationCmptRef.instance, (/** @type {?} */ (Object.assign({}, { _ref: notificationCmptRef }, options))));
    }
    /**
     * @return {?}
     */
    clearAll() {
        this._notificationRefs.forEach((/**
         * @param {?} ngtNotificationRef
         * @return {?}
         */
        ngtNotificationRef => ngtNotificationRef.closeNotification()));
    }
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @return {?}
     */
    _attachNotificationComponent(moduleCFR, containerEl) {
        /** @type {?} */
        const notificationFactory = moduleCFR.resolveComponentFactory(NgtNotificationComponent);
        /** @type {?} */
        const notificationCmptRef = notificationFactory.create(this._injector);
        // Attach component to the appRef so that it's inside the ng component tree
        this._appRef.attachView(notificationCmptRef.hostView);
        // Append DOM element to the body
        containerEl.appendChild(notificationCmptRef.location.nativeElement);
        return notificationCmptRef;
    }
    /**
     * @private
     * @param {?} notificationInstance
     * @param {?} options
     * @return {?}
     */
    _applyNotificationOptions(notificationInstance, options) {
        this._notificationAttributes.forEach((/**
         * @param {?} optionName
         * @return {?}
         */
        (optionName) => {
            if (isDefined(options[optionName])) {
                notificationInstance[optionName] = options[optionName];
            }
        }));
    }
    /**
     * @private
     * @param {?} ngtNotificationComponent
     * @return {?}
     */
    _registerNotificationRef(ngtNotificationComponent) {
        /** @type {?} */
        const _unregisterNotificationRef = (/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const index = this._notificationRefs.indexOf(ngtNotificationComponent);
            if (index > -1) {
                this._notificationRefs.splice(index, 1);
            }
        });
        this._notificationRefs.push(ngtNotificationComponent);
        ngtNotificationComponent.result.then(_unregisterNotificationRef, _unregisterNotificationRef);
    }
}
NgtNotificationStack.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
NgtNotificationStack.ctorParameters = () => [
    { type: Injector },
    { type: ApplicationRef },
    { type: RendererFactory2 },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] }
];
/** @nocollapse */ NgtNotificationStack.ngInjectableDef = defineInjectable({ factory: function NgtNotificationStack_Factory() { return new NgtNotificationStack(inject(INJECTOR), inject(ApplicationRef), inject(RendererFactory2), inject(DOCUMENT)); }, token: NgtNotificationStack, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtNotificationService {
    /**
     * @param {?} stack
     * @param {?} _moduleCFR
     */
    constructor(stack, _moduleCFR) {
        this.stack = stack;
        this._moduleCFR = _moduleCFR;
        this.timeout = 2500;
        this.notificationsData = {
            success: {
                typeClass: 'notification-success',
                aside: '<i class="ft-check"></i>'
            },
            error: {
                typeClass: 'notification-danger',
                aside: '<i class="ft-x"></i>'
            },
            info: {
                typeClass: 'notification-info',
                aside: '<i class="ft-info"></i>'
            },
            warning: {
                typeClass: 'notification-warning',
                aside: '<i class="ft-alert-triangle"></i>'
            },
        };
    }
    /**
     * @param {?} type
     * @param {?} kind
     * @return {?}
     */
    getInfo(type, kind) {
        if (!kind) {
            return;
        }
        // return css class based on notification type
        switch (type) {
            case NgtNotificationType.Success:
                return this.notificationsData.success[kind];
            case NgtNotificationType.Error:
                return this.notificationsData.error[kind];
            case NgtNotificationType.Info:
                return this.notificationsData.info[kind];
            case NgtNotificationType.Warning:
                return this.notificationsData.warning[kind];
        }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    success(data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Success }, data));
    }
    /**
     * @param {?} data
     * @return {?}
     */
    error(data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Error }, data));
    }
    /**
     * @param {?} data
     * @return {?}
     */
    info(data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Info }, data));
    }
    /**
     * @param {?} data
     * @return {?}
     */
    warn(data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Warning }, data));
    }
    /**
     * @param {?} data
     * @return {?}
     */
    notification(data) {
        if (data.type || data.type === 0) {
            data.typeClass = this.getInfo(data.type, 'typeClass');
            if (!data.aside) {
                data.aside = this.getInfo(data.type, 'aside');
            }
        }
        /** @type {?} */
        const combinedOptions = (/** @type {?} */ (Object.assign({}, { timeout: this.timeout }, data)));
        this.stack.show(this._moduleCFR, combinedOptions);
    }
    /**
     * @return {?}
     */
    clear() {
        // clear alerts
        this.stack.clearAll();
    }
}
NgtNotificationService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NgtNotificationService.ctorParameters = () => [
    { type: NgtNotificationStack },
    { type: ComponentFactoryResolver }
];
/** @nocollapse */ NgtNotificationService.ngInjectableDef = defineInjectable({ factory: function NgtNotificationService_Factory() { return new NgtNotificationService(inject(NgtNotificationStack), inject(ComponentFactoryResolver)); }, token: NgtNotificationService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGC_MODAL_DIRECTIVES$1 = [
    NgtNotificationComponent
];
class NgtNotificationModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtNotificationModule };
    }
}
NgtNotificationModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NGC_MODAL_DIRECTIVES$1],
                exports: [NGC_MODAL_DIRECTIVES$1],
                imports: [CommonModule],
                entryComponents: [NgtNotificationComponent]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Configuration service for the NgtPagination component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the paginations used in the application.
 */
class NgtPaginationConfig {
    constructor() {
        this.disabled = false;
        this.boundaryLinks = false;
        this.directionLinks = true;
        this.ellipses = true;
        this.maxSize = 0;
        this.pageSize = 10;
        this.rotate = false;
    }
}
NgtPaginationConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtPaginationConfig.ngInjectableDef = defineInjectable({ factory: function NgtPaginationConfig_Factory() { return new NgtPaginationConfig(); }, token: NgtPaginationConfig, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * The directive to match the 'ellipsis' cell template
 *
 * \@since 4.1.0
 */
class NgtPaginationEllipsis {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationEllipsis.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationEllipsis]' },] }
];
/** @nocollapse */
NgtPaginationEllipsis.ctorParameters = () => [
    { type: TemplateRef }
];
/**
 * The directive to match the 'first' cell template
 *
 * \@since 4.1.0
 */
class NgtPaginationFirst {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationFirst.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationFirst]' },] }
];
/** @nocollapse */
NgtPaginationFirst.ctorParameters = () => [
    { type: TemplateRef }
];
/**
 * The directive to match the 'last' cell template
 *
 * \@since 4.1.0
 */
class NgtPaginationLast {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationLast.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationLast]' },] }
];
/** @nocollapse */
NgtPaginationLast.ctorParameters = () => [
    { type: TemplateRef }
];
/**
 * The directive to match the 'next' cell template
 *
 * \@since 4.1.0
 */
class NgtPaginationNext {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationNext.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationNext]' },] }
];
/** @nocollapse */
NgtPaginationNext.ctorParameters = () => [
    { type: TemplateRef }
];
/**
 * The directive to match the page 'number' cell template
 *
 * \@since 4.1.0
 */
class NgtPaginationNumber {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationNumber.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationNumber]' },] }
];
/** @nocollapse */
NgtPaginationNumber.ctorParameters = () => [
    { type: TemplateRef }
];
/**
 * The directive to match the 'previous' cell template
 *
 * \@since 4.1.0
 */
class NgtPaginationPrevious {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationPrevious.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationPrevious]' },] }
];
/** @nocollapse */
NgtPaginationPrevious.ctorParameters = () => [
    { type: TemplateRef }
];
/**
 * A component that displays page numbers and allows to customize them in several ways
 */
class NgtPagination {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.pageCount = 0;
        this.pages = [];
        /**
         *  Current page. Page numbers start with 1
         */
        this.page = 1;
        /**
         *  An event fired when the page is changed.
         *  Event's payload equals to the newly selected page.
         *  Will fire only if collection size is set and all values are valid.
         *  Page numbers start with 1
         */
        this.pageChange = new EventEmitter(true);
        this.disabled = config.disabled;
        this.boundaryLinks = config.boundaryLinks;
        this.directionLinks = config.directionLinks;
        this.ellipses = config.ellipses;
        this.maxSize = config.maxSize;
        this.pageSize = config.pageSize;
        this.rotate = config.rotate;
        this.size = config.size;
    }
    /**
     * @return {?}
     */
    hasPrevious() {
        return this.page > 1;
    }
    /**
     * @return {?}
     */
    hasNext() {
        return this.page < this.pageCount;
    }
    /**
     * @return {?}
     */
    nextDisabled() {
        return !this.hasNext() || this.disabled;
    }
    /**
     * @return {?}
     */
    previousDisabled() {
        return !this.hasPrevious() || this.disabled;
    }
    /**
     * @param {?} pageNumber
     * @return {?}
     */
    selectPage(pageNumber) {
        this._updatePages(pageNumber);
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        this._updatePages(this.page);
    }
    /**
     * @param {?} pageNumber
     * @return {?}
     */
    isEllipsis(pageNumber) {
        return pageNumber === -1;
    }
    /**
     * Appends ellipses and first/last page number to the displayed pages
     * @private
     * @param {?} start
     * @param {?} end
     * @return {?}
     */
    _applyEllipses(start, end) {
        if (this.ellipses) {
            if (start > 0) {
                if (start > 1) {
                    this.pages.unshift(-1);
                }
                this.pages.unshift(1);
            }
            if (end < this.pageCount) {
                if (end < (this.pageCount - 1)) {
                    this.pages.push(-1);
                }
                this.pages.push(this.pageCount);
            }
        }
    }
    /**
     * Rotates page numbers based on maxSize items visible.
     * Currently selected page stays in the middle:
     *
     * Ex. for selected page = 6:
     * [5,*6*,7] for maxSize = 3
     * [4,5,*6*,7] for maxSize = 4
     * @private
     * @return {?}
     */
    _applyRotation() {
        /** @type {?} */
        let start = 0;
        /** @type {?} */
        let end = this.pageCount;
        /** @type {?} */
        let leftOffset = Math.floor(this.maxSize / 2);
        /** @type {?} */
        let rightOffset = this.maxSize % 2 === 0 ? leftOffset - 1 : leftOffset;
        if (this.page <= leftOffset) {
            // very beginning, no rotation -> [0..maxSize]
            end = this.maxSize;
        }
        else if (this.pageCount - this.page < leftOffset) {
            // very end, no rotation -> [len-maxSize..len]
            start = this.pageCount - this.maxSize;
        }
        else {
            // rotate
            start = this.page - leftOffset - 1;
            end = this.page + rightOffset;
        }
        return [start, end];
    }
    /**
     * Paginates page numbers based on maxSize items per page
     * @private
     * @return {?}
     */
    _applyPagination() {
        /** @type {?} */
        let page = Math.ceil(this.page / this.maxSize) - 1;
        /** @type {?} */
        let start = page * this.maxSize;
        /** @type {?} */
        let end = start + this.maxSize;
        return [start, end];
    }
    /**
     * @private
     * @param {?} newPageNo
     * @return {?}
     */
    _setPageInRange(newPageNo) {
        /** @type {?} */
        const prevPageNo = this.page;
        this.page = getValueInRange(newPageNo, this.pageCount, 1);
        if (this.page !== prevPageNo && isNumber(this.collectionSize)) {
            this.pageChange.emit(this.page);
        }
    }
    /**
     * @private
     * @param {?} newPage
     * @return {?}
     */
    _updatePages(newPage) {
        this.pageCount = Math.ceil(this.collectionSize / this.pageSize);
        if (!isNumber(this.pageCount)) {
            this.pageCount = 0;
        }
        // fill-in model needed to render pages
        this.pages.length = 0;
        for (let i = 1; i <= this.pageCount; i++) {
            this.pages.push(i);
        }
        // set page within 1..max range
        this._setPageInRange(newPage);
        // apply maxSize if necessary
        if (this.maxSize > 0 && this.pageCount > this.maxSize) {
            /** @type {?} */
            let start = 0;
            /** @type {?} */
            let end = this.pageCount;
            // either paginating or rotating page numbers
            if (this.rotate) {
                [start, end] = this._applyRotation();
            }
            else {
                [start, end] = this._applyPagination();
            }
            this.pages = this.pages.slice(start, end);
            // adding ellipses
            this._applyEllipses(start, end);
        }
    }
}
NgtPagination.decorators = [
    { type: Component, args: [{
                selector: 'ngt-pagination',
                changeDetection: ChangeDetectionStrategy.OnPush,
                host: { 'role': 'navigation' },
                template: `
    <ng-template #first><span aria-hidden="true" i18n="@@ngt.pagination.first">&laquo;&laquo;</span></ng-template>
    <ng-template #previous><span aria-hidden="true" i18n="@@ngt.pagination.previous">&laquo;</span></ng-template>
    <ng-template #next><span aria-hidden="true" i18n="@@ngt.pagination.next">&raquo;</span></ng-template>
    <ng-template #last><span aria-hidden="true" i18n="@@ngt.pagination.last">&raquo;&raquo;</span></ng-template>
    <ng-template #ellipsis>...</ng-template>
    <ng-template #defaultNumber let-page let-currentPage="currentPage">
      {{ page }}
      <span *ngIf="page === currentPage" class="sr-only">(current)</span>
    </ng-template>
    <ul [class]="'pagination' + (size ? ' pagination-' + size : '')">
      <li *ngIf="boundaryLinks" class="page-item"
        [class.disabled]="previousDisabled()">
        <a aria-label="First" i18n-aria-label="@@ngt.pagination.first-aria" class="page-link" href
          (click)="selectPage(1); $event.preventDefault()" [attr.tabindex]="(hasPrevious() ? null : '-1')">
          <ng-template [ngTemplateOutlet]="tplFirst?.templateRef || first"
                       [ngTemplateOutletContext]="{disabled: previousDisabled(), currentPage: page}"></ng-template>
        </a>
      </li>
      <li *ngIf="directionLinks" class="page-item"
        [class.disabled]="previousDisabled()">
        <a aria-label="Previous" i18n-aria-label="@@ngt.pagination.previous-aria" class="page-link" href
          (click)="selectPage(page-1); $event.preventDefault()" [attr.tabindex]="(hasPrevious() ? null : '-1')">
          <ng-template [ngTemplateOutlet]="tplPrevious?.templateRef || previous"
                       [ngTemplateOutletContext]="{disabled: previousDisabled()}"></ng-template>
        </a>
      </li>
      <li *ngFor="let pageNumber of pages" class="page-item" [class.active]="pageNumber === page"
        [class.disabled]="isEllipsis(pageNumber) || disabled">
        <a *ngIf="isEllipsis(pageNumber)" class="page-link">
          <ng-template [ngTemplateOutlet]="tplEllipsis?.templateRef || ellipsis"
                       [ngTemplateOutletContext]="{disabled: true, currentPage: page}"></ng-template>
        </a>
        <a *ngIf="!isEllipsis(pageNumber)" class="page-link" href (click)="selectPage(pageNumber); $event.preventDefault()">
          <ng-template [ngTemplateOutlet]="tplNumber?.templateRef || defaultNumber"
                       [ngTemplateOutletContext]="{disabled: disabled, $implicit: pageNumber, currentPage: page}"></ng-template>
        </a>
      </li>
      <li *ngIf="directionLinks" class="page-item" [class.disabled]="nextDisabled()">
        <a aria-label="Next" i18n-aria-label="@@ngt.pagination.next-aria" class="page-link" href
          (click)="selectPage(page+1); $event.preventDefault()" [attr.tabindex]="(hasNext() ? null : '-1')">
          <ng-template [ngTemplateOutlet]="tplNext?.templateRef || next"
                       [ngTemplateOutletContext]="{disabled: nextDisabled(), currentPage: page}"></ng-template>
        </a>
      </li>
      <li *ngIf="boundaryLinks" class="page-item" [class.disabled]="nextDisabled()">
        <a aria-label="Last" i18n-aria-label="@@ngt.pagination.last-aria" class="page-link" href
          (click)="selectPage(pageCount); $event.preventDefault()" [attr.tabindex]="(hasNext() ? null : '-1')">
          <ng-template [ngTemplateOutlet]="tplLast?.templateRef || last"
                       [ngTemplateOutletContext]="{disabled: nextDisabled(), currentPage: page}"></ng-template>
        </a>
      </li>
    </ul>
  `
            }] }
];
/** @nocollapse */
NgtPagination.ctorParameters = () => [
    { type: NgtPaginationConfig }
];
NgtPagination.propDecorators = {
    tplEllipsis: [{ type: ContentChild, args: [NgtPaginationEllipsis,] }],
    tplFirst: [{ type: ContentChild, args: [NgtPaginationFirst,] }],
    tplLast: [{ type: ContentChild, args: [NgtPaginationLast,] }],
    tplNext: [{ type: ContentChild, args: [NgtPaginationNext,] }],
    tplNumber: [{ type: ContentChild, args: [NgtPaginationNumber,] }],
    tplPrevious: [{ type: ContentChild, args: [NgtPaginationPrevious,] }],
    disabled: [{ type: Input }],
    boundaryLinks: [{ type: Input }],
    directionLinks: [{ type: Input }],
    ellipses: [{ type: Input }],
    rotate: [{ type: Input }],
    collectionSize: [{ type: Input }],
    maxSize: [{ type: Input }],
    page: [{ type: Input }],
    pageSize: [{ type: Input }],
    pageChange: [{ type: Output }],
    size: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const DIRECTIVES = [
    NgtPagination,
    NgtPaginationEllipsis,
    NgtPaginationFirst,
    NgtPaginationLast,
    NgtPaginationNext,
    NgtPaginationNumber,
    NgtPaginationPrevious
];
class NgtPaginationModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtPaginationModule };
    }
}
NgtPaginationModule.decorators = [
    { type: NgModule, args: [{
                declarations: DIRECTIVES,
                exports: DIRECTIVES,
                imports: [CommonModule]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtPanelService {
    constructor() {
        this.panels = [];
        this.activePanels = [];
        this.panelWillOpened = new Subject();
        this.panelWillClosed = new Subject();
        this.panelClosingDidStart = new Subject();
        this.panelClosingDidDone = new Subject();
        this.panelOpeningDidStart = new Subject();
        this.panelOpeningDidDone = new Subject();
        this.isPanelsChanged = new Subject();
        this.isPanelHide = false;
        this.isPanelExpand = true;
        this.panelState = 'expanded';
        this.stateEvents = {
            left: {
                expand: new Subject(),
                hide: new Subject()
            },
            right: {
                expand: new Subject(),
                hide: new Subject()
            }
        };
        // Subscribe state events and update state
        this.stateEvents.left.expand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.isPanelExpand = status;
            this.updateState();
        }));
        this.stateEvents.left.hide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.isPanelHide = status;
            this.updateState();
        }));
        this.stateEvents.right.expand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.isPanelExpand = status;
            this.updateState();
        }));
        this.stateEvents.right.hide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.isPanelHide = status;
            this.updateState();
        }));
    }
    /**
     * @param {?} panel
     * @return {?}
     */
    add(panel) {
        // add panel to array of active panels-page
        this.panels.push(panel);
        this.isPanelsChanged.next(this.panels.slice());
    }
    /**
     * @param {?} id
     * @return {?}
     */
    remove(id) {
        // remove panel from array of active panels-page
        /** @type {?} */
        const panelToRemove = find(this.panels, { id: id });
        this.panels = without(this.panels, panelToRemove);
        this.isPanelsChanged.next(this.panels.slice());
    }
    /**
     * @param {?} id
     * @return {?}
     */
    addToActive(id) {
        this.activePanels.push(id);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    removeFromActive(id) {
        /** @type {?} */
        const index = this.activePanels.indexOf(id);
        if (index !== -1) {
            this.activePanels.splice(index, 1);
        }
    }
    /**
     * @param {?} id
     * @return {?}
     */
    openPanel(id) {
        this.panelWillOpened.next(id);
    }
    /**
     * @param {?=} id
     * @return {?}
     */
    closePanel(id) {
        this.panelWillClosed.next(id || this.activePanels[this.activePanels.length - 1]);
    }
    /**
     * @return {?}
     */
    updateState() {
        this.panelState = this.isPanelHide ? 'hidden' : (this.isPanelExpand ? 'expanded' : 'collapsed');
    }
}
NgtPanelService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NgtPanelService.ctorParameters = () => [];
/** @nocollapse */ NgtPanelService.ngInjectableDef = defineInjectable({ factory: function NgtPanelService_Factory() { return new NgtPanelService(); }, token: NgtPanelService, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGC_PANEL_CONFIG = new InjectionToken('Panel configuration');
/** @type {?} */
const DEFAULT_NGC_PANEL_CONFIG = {
    leftPanelExpandedShift: 0,
    leftPanelCollapsedShift: 0,
    rightPanelExpandedShift: 0,
    rightPanelCollapsedShift: 0
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtPanelComponent {
    /**
     * @param {?} _panelService
     * @param {?} _elRef
     * @param {?} injector
     */
    constructor(_panelService, _elRef, injector) {
        this._panelService = _panelService;
        this._elRef = _elRef;
        this.dir = 'left';
        this._isOpen = false;
        this.statuses = {
            left: {
                expanded: {
                    open: 'openLeftExpanded',
                    close: 'closeLeftExpanded'
                },
                collapsed: {
                    open: 'openLeftCollapsed',
                    close: 'closeLeftCollapsed'
                },
                hidden: {
                    open: 'openLeftHidden',
                    close: 'closeLeftHidden'
                }
            },
            right: {
                expanded: {
                    open: 'openRightExpanded',
                    close: 'closeRightExpanded'
                },
                collapsed: {
                    open: 'openRightCollapsed',
                    close: 'closeRightCollapsed'
                },
                hidden: {
                    open: 'openRightHidden',
                    close: 'closeRightHidden'
                }
            }
        };
        this.openStatuses = [
            this.statuses.left.expanded.open,
            this.statuses.left.collapsed.open,
            this.statuses.left.hidden.open,
            this.statuses.right.expanded.open,
            this.statuses.right.collapsed.open,
            this.statuses.right.hidden.open,
        ];
        this.closeStatuses = [
            this.statuses.left.expanded.open,
            this.statuses.left.collapsed.open,
            this.statuses.left.hidden.open,
            this.statuses.right.expanded.open,
            this.statuses.right.collapsed.open,
            this.statuses.right.hidden.open,
        ];
        this.subscriptions = [];
        this.styles = {};
        this.config = Object.assign({}, DEFAULT_NGC_PANEL_CONFIG, injector.get(NGC_PANEL_CONFIG));
        this.el = this._elRef;
        this.element = this._elRef.nativeElement;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set isOpen(value) {
        this._isOpen = value;
    }
    /**
     * @return {?}
     */
    get isOpen() {
        return this._isOpen;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        // direction
        this.updateStates(this.dir);
        this.subscriptions.push(this._panelService.stateEvents[this.dir].expand.subscribe((/**
         * @param {?} _
         * @return {?}
         */
        _ => this.updateStates(this.dir))));
        this.subscriptions.push(this._panelService.stateEvents[this.dir].hide.subscribe((/**
         * @param {?} _
         * @return {?}
         */
        _ => this.updateStates(this.dir))));
        // ensure id attribute exists
        if (!this.id) {
            console.error('panel must have an id');
            return;
        }
        // add self (this panel instance) to the panel service so it's accessible from controllers
        this._panelService.add(this);
        // subscribe events
        this.subscriptions.push(this._panelService.panelWillOpened.subscribe((/**
         * @param {?} id
         * @return {?}
         */
        (id) => {
            if (id === this.id) {
                this.open();
            }
        })));
        this.subscriptions.push(this._panelService.panelWillClosed.subscribe((/**
         * @param {?} id
         * @return {?}
         */
        (id) => {
            if (id === this.id) {
                this.close();
            }
        })));
    }
    // remove self from panel service when directive is destroyed
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this._panelService.remove(this.id);
        this._panelService.removeFromActive(this.id);
        this.element.parentNode.removeChild(this.element);
        this.subscriptions.forEach((/**
         * @param {?} subscription
         * @return {?}
         */
        (subscription) => {
            subscription.unsubscribe();
        }));
    }
    // open panel
    /**
     * @return {?}
     */
    open() {
        this._panelService.addToActive(this.id);
        this.setStyle(this._panelService.activePanels.length);
        this.isOpen = true;
    }
    // close panel
    /**
     * @return {?}
     */
    close() {
        this._panelService.removeFromActive(this.id);
        this.isOpen = false;
    }
    /**
     * @param {?} dir
     * @return {?}
     */
    updateStates(dir) {
        this.openState = this.statuses[dir][this._panelService.panelState].open;
        this.closeState = this.statuses[dir][this._panelService.panelState].close;
    }
    /**
     * @param {?} i
     * @return {?}
     */
    setStyle(i) {
        this.styles = {
            'zIndex': i
        };
    }
    /**
     * @param {?} event
     * @return {?}
     */
    animationAction(event) {
        switch (event.phaseName) {
            case 'start':
                this.openStatuses.includes(event.toState) && this._panelService.panelOpeningDidStart.next();
                this.closeStatuses.includes(event.toState) && this._panelService.panelClosingDidStart.next();
                break;
            case 'done':
                this.openStatuses.includes(event.toState) && this._panelService.panelOpeningDidDone.next();
                this.closeStatuses.includes(event.toState) && this._panelService.panelClosingDidDone.next();
                break;
        }
    }
}
NgtPanelComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-panel',
                template: `
        <div class="panel-wrap"
             [ngStyle]="styles"
             [@panel]='{
		     	value: isOpen ? this.openState : this.closeState,
		     	params: {
		     		leftCollapsedWidth: config.leftPanelCollapsedShift,
		     		leftExpandedWidth: config.leftPanelExpandedShift,
		     		rightCollapsedWidth: config.rightPanelCollapsedShift,
		     		rightExpandedWidth: config.rightPanelExpandedShift
		     	}
		     }'
             (@panel.start)="animationAction($event)"
             (@panel.done)="animationAction($event)">
            <ng-content></ng-content>
        </div>
    `,
                animations: [
                    trigger('panel', [
                        state('openLeftHidden', style({ left: 0, transform: 'none' })),
                        state('openLeftCollapsed', style({
                            left: 0,
                            transform: 'translateX({{ leftCollapsedWidth }}px)'
                        }), { params: { leftCollapsedWidth: DEFAULT_NGC_PANEL_CONFIG.leftPanelCollapsedShift } }),
                        state('openLeftExpanded', style({
                            left: 0,
                            transform: 'translateX({{ leftExpandedWidth }}px)'
                        }), { params: { leftExpandedWidth: DEFAULT_NGC_PANEL_CONFIG.leftPanelExpandedShift } }),
                        state('closeLeftHidden', style({ left: 0, transform: 'translateX(-100%)' })),
                        state('closeLeftCollapsed', style({
                            left: 0,
                            transform: 'translateX(calc(-100% + {{ leftCollapsedWidth }}px))'
                        }), { params: { leftCollapsedWidth: DEFAULT_NGC_PANEL_CONFIG.leftPanelCollapsedShift } }),
                        state('closeLeftExpanded', style({
                            left: 0,
                            transform: 'translateX(calc(-100% + {{ leftExpandedWidth }}px))'
                        }), { params: { leftExpandedWidth: DEFAULT_NGC_PANEL_CONFIG.leftPanelExpandedShift } }),
                        state('openRightHidden', style({ right: 0, transform: 'none' })),
                        state('openRightCollapsed', style({
                            right: 0,
                            transform: 'translateX(-{{ rightCollapsedWidth }}px)'
                        }), { params: { rightCollapsedWidth: DEFAULT_NGC_PANEL_CONFIG.rightPanelCollapsedShift } }),
                        state('openRightExpanded', style({
                            right: 0,
                            transform: 'translateX(-{{ rightExpandedWidth }}px)'
                        }), { params: { rightExpandedWidth: DEFAULT_NGC_PANEL_CONFIG.rightPanelExpandedShift } }),
                        state('closeRightHidden', style({ right: 0, transform: 'translateX(100%)' })),
                        state('closeRightCollapsed', style({
                            right: 0,
                            transform: 'translateX(calc(100% - {{ rightCollapsedWidth }}px))'
                        }), { params: { rightCollapsedWidth: DEFAULT_NGC_PANEL_CONFIG.rightPanelCollapsedShift } }),
                        state('closeRightExpanded', style({
                            right: 0,
                            transform: 'translateX(calc(100% - {{ rightExpandedWidth }}px))'
                        }), { params: { rightExpandedWidth: DEFAULT_NGC_PANEL_CONFIG.rightPanelExpandedShift } }),
                        transition('void => *', animate('0s')),
                        transition('* => *', animate('0.4s cubic-bezier(0.05, 0.74, 0.2, 0.99)'))
                    ])
                ]
            }] }
];
/** @nocollapse */
NgtPanelComponent.ctorParameters = () => [
    { type: NgtPanelService },
    { type: ElementRef },
    { type: Injector }
];
NgtPanelComponent.propDecorators = {
    id: [{ type: Input }],
    dir: [{ type: Input }],
    isOpen: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtPanelsComponent {
    /**
     * @param {?} _panelService
     * @param {?} elRef
     */
    constructor(_panelService, elRef) {
        this._panelService = _panelService;
        this.elRef = elRef;
        this.overlay = true;
        this.leftPanelExpand = new Subject();
        this.leftPanelHide = new Subject();
        this.rightPanelExpand = new Subject();
        this.rightPanelHide = new Subject();
        this.subscriptions = [];
        this._isOpen = false;
        this.subscriptions.push(this._panelService.isPanelsChanged.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        (data) => {
            (data.length < 1) && (this.isOpen = false);
            console.log(data);
            for (const panel of data) {
                this.elRef.nativeElement.querySelector('.panel-container').appendChild(panel.el.nativeElement);
            }
        })));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onClick(event) {
        this.onClickOutside(event);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set isOpen(value) {
        this._isOpen = value;
    }
    /**
     * @return {?}
     */
    get isOpen() {
        return this._isOpen;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscriptions.push(this._panelService.panelOpeningDidStart.subscribe((/**
         * @param {?} _
         * @return {?}
         */
        _ => this.isOpen = true)));
        this.subscriptions.push(this._panelService.panelClosingDidStart.subscribe((/**
         * @return {?}
         */
        () => {
            !this._panelService.activePanels.length && (this.isOpen = false);
        })));
        // Subscribe state events
        this.subscriptions.push(this.leftPanelExpand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => this._panelService.stateEvents.left.expand.next(status))));
        this.subscriptions.push(this.leftPanelHide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => this._panelService.stateEvents.left.hide.next(status))));
        this.subscriptions.push(this.rightPanelExpand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => this._panelService.stateEvents.right.expand.next(status))));
        this.subscriptions.push(this.rightPanelHide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => this._panelService.stateEvents.right.hide.next(status))));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscriptions.forEach((/**
         * @param {?} subscription
         * @return {?}
         */
        (subscription) => {
            subscription.unsubscribe();
        }));
    }
    /**
     * @param {?} e
     * @return {?}
     */
    onClickOutside(e) {
        if (!e.target.closest('ngt-panel')) {
            this._panelService.closePanel();
        }
    }
}
NgtPanelsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-panels',
                template: `
        <div class="panel-container"></div>
        <div
            *ngIf="overlay"
            (click)="onClickOutside($event)"
            class="panel-overlay"
            [@overlay]='isOpen ? "open" : "close"'>
        </div>
    `,
                animations: [
                    trigger('overlay', [
                        state('open', style({ opacity: 1 })),
                        state('close', style({ opacity: 0, display: 'none' })),
                        transition('close => open', animate('300ms')),
                        transition('open => close', animate('300ms'))
                    ])
                ]
            }] }
];
/** @nocollapse */
NgtPanelsComponent.ctorParameters = () => [
    { type: NgtPanelService },
    { type: ElementRef }
];
NgtPanelsComponent.propDecorators = {
    overlay: [{ type: Input }],
    leftPanelExpand: [{ type: Input }],
    leftPanelHide: [{ type: Input }],
    rightPanelExpand: [{ type: Input }],
    rightPanelHide: [{ type: Input }],
    onClick: [{ type: HostListener, args: ['document:click', ['$event'],] }],
    isOpen: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtPanelOpenDirective {
    /**
     * @param {?} _panelService
     */
    constructor(_panelService) {
        this._panelService = _panelService;
    }
    /**
     * @param {?} e
     * @return {?}
     */
    onClick(e) {
        e.stopPropagation();
        this._panelService.openPanel(this.id);
    }
}
NgtPanelOpenDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtPanelOpen]'
            },] }
];
/** @nocollapse */
NgtPanelOpenDirective.ctorParameters = () => [
    { type: NgtPanelService }
];
NgtPanelOpenDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtPanelOpen',] }],
    onClick: [{ type: HostListener, args: ['click', ['$event'],] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtPanelCloseDirective {
    /**
     * @param {?} _panelService
     */
    constructor(_panelService) {
        this._panelService = _panelService;
    }
    /**
     * @param {?} e
     * @return {?}
     */
    onClick(e) {
        e.stopPropagation();
        this._panelService.closePanel(this.id);
    }
}
NgtPanelCloseDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtPanelClose]'
            },] }
];
/** @nocollapse */
NgtPanelCloseDirective.ctorParameters = () => [
    { type: NgtPanelService }
];
NgtPanelCloseDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtPanelClose',] }],
    onClick: [{ type: HostListener, args: ['click', ['$event'],] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtPanel {
    /**
     * @param {?} id
     * @param {?} el
     */
    constructor(id, el) {
        this.id = id;
        this.el = el;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGC_PANEL_DIRECTIVES = [
    NgtPanelComponent, NgtPanelsComponent, NgtPanelOpenDirective, NgtPanelCloseDirective
];
class NgtPanelModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtPanelModule };
    }
}
NgtPanelModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NGC_PANEL_DIRECTIVES],
                exports: [NGC_PANEL_DIRECTIVES],
                imports: [CommonModule]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class Trigger {
    /**
     * @param {?} open
     * @param {?=} close
     */
    constructor(open, close) {
        this.open = open;
        this.close = close;
        if (!close) {
            this.close = open;
        }
    }
    /**
     * @return {?}
     */
    isManual() {
        return this.open === 'manual' || this.close === 'manual';
    }
}
/** @type {?} */
const DEFAULT_ALIASES = {
    'hover': ['mouseenter', 'mouseleave'],
    'focus': ['focusin', 'focusout'],
};
/**
 * @param {?} triggers
 * @param {?=} aliases
 * @return {?}
 */
function parseTriggers(triggers, aliases = DEFAULT_ALIASES) {
    /** @type {?} */
    const trimmedTriggers = (triggers || '').trim();
    if (trimmedTriggers.length === 0) {
        return [];
    }
    /** @type {?} */
    const parsedTriggers = trimmedTriggers.split(/\s+/).map((/**
     * @param {?} trigger
     * @return {?}
     */
    trigger$$1 => trigger$$1.split(':'))).map((/**
     * @param {?} triggerPair
     * @return {?}
     */
    (triggerPair) => {
        /** @type {?} */
        let alias = aliases[triggerPair[0]] || triggerPair;
        return new Trigger(alias[0], alias[1]);
    }));
    /** @type {?} */
    const manualTriggers = parsedTriggers.filter((/**
     * @param {?} triggerPair
     * @return {?}
     */
    triggerPair => triggerPair.isManual()));
    if (manualTriggers.length > 1) {
        throw 'Triggers parse error: only one manual trigger is allowed';
    }
    if (manualTriggers.length === 1 && parsedTriggers.length > 1) {
        throw 'Triggers parse error: manual trigger can\'t be mixed with other triggers';
    }
    return parsedTriggers;
}
/**
 * @param {?} renderer
 * @param {?} nativeElement
 * @param {?} triggers
 * @param {?} isOpenedFn
 * @return {?}
 */
function observeTriggers(renderer, nativeElement, triggers, isOpenedFn) {
    return new Observable((/**
     * @param {?} subscriber
     * @return {?}
     */
    subscriber => {
        /** @type {?} */
        const listeners = [];
        /** @type {?} */
        const openFn = (/**
         * @return {?}
         */
        () => subscriber.next(true));
        /** @type {?} */
        const closeFn = (/**
         * @return {?}
         */
        () => subscriber.next(false));
        /** @type {?} */
        const toggleFn = (/**
         * @return {?}
         */
        () => subscriber.next(!isOpenedFn()));
        triggers.forEach((/**
         * @param {?} trigger
         * @return {?}
         */
        (trigger$$1) => {
            if (trigger$$1.open === trigger$$1.close) {
                listeners.push(renderer.listen(nativeElement, trigger$$1.open, toggleFn));
            }
            else {
                listeners.push(renderer.listen(nativeElement, trigger$$1.open, openFn), renderer.listen(nativeElement, trigger$$1.close, closeFn));
            }
        }));
        return (/**
         * @return {?}
         */
        () => {
            listeners.forEach((/**
             * @param {?} unsubscribeFn
             * @return {?}
             */
            unsubscribeFn => unsubscribeFn()));
        });
    }));
}
/** @type {?} */
const delayOrNoop = (/**
 * @template T
 * @param {?} time
 * @return {?}
 */
(time) => time > 0 ? delay(time) : (/**
 * @param {?} a
 * @return {?}
 */
(a) => a));
/**
 * @param {?} openDelay
 * @param {?} closeDelay
 * @param {?} isOpenedFn
 * @return {?}
 */
function triggerDelay(openDelay, closeDelay, isOpenedFn) {
    return (/**
     * @param {?} input$
     * @return {?}
     */
    (input$) => {
        /** @type {?} */
        let pending = null;
        /** @type {?} */
        const filteredInput$ = input$.pipe(map((/**
         * @param {?} open
         * @return {?}
         */
        open => ({ open }))), filter((/**
         * @param {?} event
         * @return {?}
         */
        event => {
            /** @type {?} */
            const currentlyOpen = isOpenedFn();
            if (currentlyOpen !== event.open && (!pending || pending.open === currentlyOpen)) {
                pending = event;
                return true;
            }
            if (pending && pending.open !== event.open) {
                pending = null;
            }
            return false;
        })), share());
        /** @type {?} */
        const delayedOpen$ = filteredInput$.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event.open)), delayOrNoop(openDelay));
        /** @type {?} */
        const delayedClose$ = filteredInput$.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => !event.open)), delayOrNoop(closeDelay));
        return merge(delayedOpen$, delayedClose$)
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => {
            if (event === pending) {
                pending = null;
                return event.open !== isOpenedFn();
            }
            return false;
        })), map((/**
         * @param {?} event
         * @return {?}
         */
        event => event.open)));
    });
}
/**
 * @param {?} renderer
 * @param {?} nativeElement
 * @param {?} triggers
 * @param {?} isOpenedFn
 * @param {?} openFn
 * @param {?} closeFn
 * @param {?=} openDelay
 * @param {?=} closeDelay
 * @return {?}
 */
function listenToTriggers(renderer, nativeElement, triggers, isOpenedFn, openFn, closeFn, openDelay = 0, closeDelay = 0) {
    /** @type {?} */
    const parsedTriggers = parseTriggers(triggers);
    if (parsedTriggers.length === 1 && parsedTriggers[0].isManual()) {
        return (/**
         * @return {?}
         */
        () => {
        });
    }
    /** @type {?} */
    const subscription = observeTriggers(renderer, nativeElement, parsedTriggers, isOpenedFn)
        .pipe(triggerDelay(openDelay, closeDelay, isOpenedFn))
        .subscribe((/**
     * @param {?} open
     * @return {?}
     */
    open => (open ? openFn() : closeFn())));
    return (/**
     * @return {?}
     */
    () => subscription.unsubscribe());
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Configuration service for the NgtPopover directive.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the popovers used in the application.
 */
class NgtPopoverConfig {
    constructor() {
        this.autoClose = true;
        this.placement = 'auto';
        this.triggers = 'click';
        this.disablePopover = false;
        this.openDelay = 0;
        this.closeDelay = 0;
    }
}
NgtPopoverConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtPopoverConfig.ngInjectableDef = defineInjectable({ factory: function NgtPopoverConfig_Factory() { return new NgtPopoverConfig(); }, token: NgtPopoverConfig, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
let nextId = 0;
class NgtPopoverWindow {
    /**
     * @return {?}
     */
    isTitleTemplate() {
        return this.title instanceof TemplateRef;
    }
}
NgtPopoverWindow.decorators = [
    { type: Component, args: [{
                selector: 'ngt-popover-window',
                changeDetection: ChangeDetectionStrategy.OnPush,
                encapsulation: ViewEncapsulation.None,
                host: { '[class]': '"popover" + (popoverClass ? " " + popoverClass : "")', 'role': 'tooltip', '[id]': 'id' },
                template: `
        <div class="arrow"></div>
        <h3 class="popover-header" *ngIf="title != null">
            <ng-template #simpleTitle>{{title}}</ng-template>
            <ng-template [ngTemplateOutlet]="isTitleTemplate() ? title : simpleTitle" [ngTemplateOutletContext]="context"></ng-template>
        </h3>
        <div class="popover-body">
            <ng-content></ng-content>
        </div>`,
                styles: ["ngt-popover-window.bs-popover-bottom .arrow,ngt-popover-window.bs-popover-top .arrow{left:50%;margin-left:-.5rem}ngt-popover-window.bs-popover-bottom-left .arrow,ngt-popover-window.bs-popover-top-left .arrow{left:2em}ngt-popover-window.bs-popover-bottom-right .arrow,ngt-popover-window.bs-popover-top-right .arrow{left:auto;right:2em}ngt-popover-window.bs-popover-left .arrow,ngt-popover-window.bs-popover-right .arrow{top:50%;margin-top:-.5rem}ngt-popover-window.bs-popover-left-top .arrow,ngt-popover-window.bs-popover-right-top .arrow{top:.7em}ngt-popover-window.bs-popover-left-bottom .arrow,ngt-popover-window.bs-popover-right-bottom .arrow{top:auto;bottom:.7em}"]
            }] }
];
NgtPopoverWindow.propDecorators = {
    title: [{ type: Input }],
    id: [{ type: Input }],
    popoverClass: [{ type: Input }],
    context: [{ type: Input }]
};
/**
 * A lightweight, extensible directive for fancy popover creation.
 */
class NgtPopover {
    /**
     * @param {?} _elementRef
     * @param {?} _renderer
     * @param {?} injector
     * @param {?} componentFactoryResolver
     * @param {?} viewContainerRef
     * @param {?} config
     * @param {?} _ngZone
     * @param {?} _document
     * @param {?} _changeDetector
     */
    constructor(_elementRef, _renderer, injector, componentFactoryResolver, viewContainerRef, config, _ngZone, _document, _changeDetector) {
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this._ngZone = _ngZone;
        this._document = _document;
        this._changeDetector = _changeDetector;
        /**
         * Emits an event when the popover is shown
         */
        this.shown = new EventEmitter();
        /**
         * Emits an event when the popover is hidden
         */
        this.hidden = new EventEmitter();
        this._ngtPopoverWindowId = `ngt-popover-${nextId++}`;
        this.autoClose = config.autoClose;
        this.placement = config.placement;
        this.triggers = config.triggers;
        this.container = config.container;
        this.disablePopover = config.disablePopover;
        this.popoverClass = config.popoverClass;
        this.openDelay = config.openDelay;
        this.closeDelay = config.closeDelay;
        this._popupService = new PopupService(NgtPopoverWindow, injector, viewContainerRef, _renderer, componentFactoryResolver);
        this._zoneSubscription = _ngZone.onStable.subscribe((/**
         * @return {?}
         */
        () => {
            if (this._windowRef) {
                positionElements(this._elementRef.nativeElement, this._windowRef.location.nativeElement, this.placement, this.container === 'body', 'bs-popover');
            }
        }));
    }
    /**
     * @private
     * @return {?}
     */
    _isDisabled() {
        if (this.disablePopover) {
            return true;
        }
        if (!this.ngtPopover && !this.popoverTitle) {
            return true;
        }
        return false;
    }
    /**
     * Opens an element’s popover. This is considered a “manual” triggering of the popover.
     * The context is an optional value to be injected into the popover template when it is created.
     * @param {?=} context
     * @return {?}
     */
    open(context) {
        if (!this._windowRef && !this._isDisabled()) {
            this._windowRef = this._popupService.open(this.ngtPopover, context);
            this._windowRef.instance.title = this.popoverTitle;
            this._windowRef.instance.context = context;
            this._windowRef.instance.popoverClass = this.popoverClass;
            this._windowRef.instance.id = this._ngtPopoverWindowId;
            this._renderer.setAttribute(this._elementRef.nativeElement, 'aria-describedby', this._ngtPopoverWindowId);
            if (this.container === 'body') {
                this._document.querySelector(this.container).appendChild(this._windowRef.location.nativeElement);
            }
            // apply styling to set basic css-classes on target element, before going for positioning
            this._windowRef.changeDetectorRef.markForCheck();
            ngtAutoClose(this._ngZone, this._document, this.autoClose, (/**
             * @return {?}
             */
            () => this.close()), this.hidden, [this._windowRef.location.nativeElement]);
            this.shown.emit();
        }
    }
    /**
     * Closes an element’s popover. This is considered a “manual” triggering of the popover.
     * @return {?}
     */
    close() {
        if (this._windowRef) {
            this._renderer.removeAttribute(this._elementRef.nativeElement, 'aria-describedby');
            this._popupService.close();
            this._windowRef = null;
            this.hidden.emit();
            this._changeDetector.markForCheck();
        }
    }
    /**
     * Toggles an element’s popover. This is considered a “manual” triggering of the popover.
     * @return {?}
     */
    toggle() {
        if (this._windowRef) {
            this.close();
        }
        else {
            this.open();
        }
    }
    /**
     * Returns whether or not the popover is currently being shown
     * @return {?}
     */
    isOpen() {
        return this._windowRef != null;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._unregisterListenersFn = listenToTriggers(this._renderer, this._elementRef.nativeElement, this.triggers, this.isOpen.bind(this), this.open.bind(this), this.close.bind(this), +this.openDelay, +this.closeDelay);
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        // close popover if title and content become empty, or disablePopover set to true
        if ((changes['ngtPopover'] || changes['popoverTitle'] || changes['disablePopover']) && this._isDisabled()) {
            this.close();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.close();
        // This check is needed as it might happen that ngOnDestroy is called before ngOnInit
        // under certain conditions, see: https://github.com/ng-bootstrap/ng-bootstrap/issues/2199
        if (this._unregisterListenersFn) {
            this._unregisterListenersFn();
        }
        this._zoneSubscription.unsubscribe();
    }
}
NgtPopover.decorators = [
    { type: Directive, args: [{ selector: '[ngtPopover]', exportAs: 'ngtPopover' },] }
];
/** @nocollapse */
NgtPopover.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: Injector },
    { type: ComponentFactoryResolver },
    { type: ViewContainerRef },
    { type: NgtPopoverConfig },
    { type: NgZone },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: ChangeDetectorRef }
];
NgtPopover.propDecorators = {
    autoClose: [{ type: Input }],
    ngtPopover: [{ type: Input }],
    popoverTitle: [{ type: Input }],
    placement: [{ type: Input }],
    triggers: [{ type: Input }],
    container: [{ type: Input }],
    disablePopover: [{ type: Input }],
    popoverClass: [{ type: Input }],
    openDelay: [{ type: Input }],
    closeDelay: [{ type: Input }],
    shown: [{ type: Output }],
    hidden: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtPopoverModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtPopoverModule };
    }
}
NgtPopoverModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NgtPopover, NgtPopoverWindow],
                exports: [NgtPopover],
                imports: [CommonModule],
                entryComponents: [NgtPopoverWindow]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Configuration service for the NgtProgressbar component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the progress bars used in the application.
 */
class NgtProgressbarConfig {
    constructor() {
        this.max = 100;
        this.animated = false;
        this.striped = false;
        this.showValue = false;
    }
}
NgtProgressbarConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtProgressbarConfig.ngInjectableDef = defineInjectable({ factory: function NgtProgressbarConfig_Factory() { return new NgtProgressbarConfig(); }, token: NgtProgressbarConfig, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Directive that can be used to provide feedback on the progress of a workflow or an action.
 */
class NgtProgressbar {
    /**
     * @param {?} config
     */
    constructor(config) {
        /**
         * Current value to be displayed in the progressbar. Should be smaller or equal to "max" value.
         */
        this.value = 0;
        this.max = config.max;
        this.animated = config.animated;
        this.striped = config.striped;
        this.type = config.type;
        this.showValue = config.showValue;
        this.height = config.height;
    }
    /**
     * @return {?}
     */
    getValue() {
        return getValueInRange(this.value, this.max);
    }
    /**
     * @return {?}
     */
    getPercentValue() {
        return 100 * this.getValue() / this.max;
    }
}
NgtProgressbar.decorators = [
    { type: Component, args: [{
                selector: 'ngt-progressbar',
                changeDetection: ChangeDetectionStrategy.OnPush,
                template: `
        <div class="progress" [style.height]="height">
            <div class="progress-bar{{type ? ' bg-' + type : ''}}{{animated ? ' progress-bar-animated' : ''}}{{striped ?
    ' progress-bar-striped' : ''}}" role="progressbar" [style.width.%]="getPercentValue()"
                 [attr.aria-valuenow]="getValue()" aria-valuemin="0" [attr.aria-valuemax]="max">
                <span *ngIf="showValue" i18n="@@ngt.progressbar.value">{{getPercentValue()}}%</span>
                <ng-content></ng-content>
            </div>
        </div>
    `
            }] }
];
/** @nocollapse */
NgtProgressbar.ctorParameters = () => [
    { type: NgtProgressbarConfig }
];
NgtProgressbar.propDecorators = {
    max: [{ type: Input }],
    animated: [{ type: Input }],
    striped: [{ type: Input }],
    showValue: [{ type: Input }],
    type: [{ type: Input }],
    value: [{ type: Input }],
    height: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtProgressbarModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtProgressbarModule };
    }
}
NgtProgressbarModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NgtProgressbar],
                exports: [NgtProgressbar],
                imports: [CommonModule]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtScrollSpy {
    /**
     * @param {?} _document
     * @param {?} _router
     * @param {?} _elem
     * @param {?} _renderer
     */
    constructor(_document, _router, _elem, _renderer) {
        this._document = _document;
        this._router = _router;
        this._elem = _elem;
        this._renderer = _renderer;
    }
    /**
     * @private
     * @param {?} spTarget
     * @return {?}
     */
    _getTarget(spTarget) {
        if (typeof spTarget === 'string') {
            spTarget = spTarget.split('');
            /** @type {?} */
            const type = spTarget.shift();
            switch (type) {
                case '#':
                    return this._document.getElementById(spTarget.join(''));
                case '.':
                    return this._document.getElementsByClassName(spTarget.join(''))[0];
                default:
                    return this._document.getElementsByTagName(this.ngtScrollSpy)[0];
            }
        }
        else {
            return spTarget;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onWindowScroll(event) {
        /** @type {?} */
        const offset = this.spyOffset ? this.spyOffset : 0;
        /** @type {?} */
        const scrollTarget = this._getTarget(this.ngtScrollSpy);
        /** @type {?} */
        const elemDim = scrollTarget ? scrollTarget.getBoundingClientRect() : null;
        if (!elemDim) {
            console.warn(`There is no element ${this.ngtScrollSpy}`);
        }
        else if (this.spyAnchor && elemDim && elemDim.top < offset && elemDim.bottom > offset) {
            if (this.spyAnchor !== 'none') {
                this._router.navigate([], { fragment: this.spyAnchor });
            }
            else {
                this._router.navigate([]);
            }
            this._renderer.addClass(this._elem.nativeElement, 'active');
        }
        else {
            this._renderer.removeClass(this._elem.nativeElement, 'active');
        }
    }
}
NgtScrollSpy.decorators = [
    { type: Directive, args: [{
                selector: '[ngtScrollSpy]',
            },] }
];
/** @nocollapse */
NgtScrollSpy.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: Router },
    { type: ElementRef },
    { type: Renderer2 }
];
NgtScrollSpy.propDecorators = {
    ngtScrollSpy: [{ type: Input }],
    spyAnchor: [{ type: Input }],
    spyOffset: [{ type: Input }],
    onWindowScroll: [{ type: HostListener, args: ['window:scroll', ['$event'],] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGT_SCROLL_SPY_DIRECTIVES = [NgtScrollSpy];
class NgtScrollSpyModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtScrollSpyModule };
    }
}
NgtScrollSpyModule.decorators = [
    { type: NgModule, args: [{
                declarations: NGT_SCROLL_SPY_DIRECTIVES,
                exports: NGT_SCROLL_SPY_DIRECTIVES,
                imports: [CommonModule]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Configuration service for the NgtSticky component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the stickies used in the application.
 */
class NgtStickyConfig {
    constructor() {
        this.zIndex = 10;
        this.width = 'auto';
        this.offsetTop = 0;
        this.offsetBottom = 0;
        this.start = 0;
        this.stickClass = 'sticky';
        this.endStickClass = 'sticky-end';
        this.mediaQuery = '';
        this.parentMode = true;
        this.orientation = 'none';
    }
}
NgtStickyConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtStickyConfig.ngInjectableDef = defineInjectable({ factory: function NgtStickyConfig_Factory() { return new NgtStickyConfig(); }, token: NgtStickyConfig, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtSticky {
    /**
     * @param {?} element
     * @param {?} _renderer
     * @param {?} config
     */
    constructor(element, _renderer, config) {
        this.element = element;
        this._renderer = _renderer;
        this.activated = new EventEmitter();
        this.deactivated = new EventEmitter();
        this.reset = new EventEmitter();
        this.isStuck = false;
        this.orientation = config.orientation;
        this.sticky = config.sticky;
        this.zIndex = config.zIndex;
        this.width = config.width;
        this.offsetTop = config.offsetTop;
        this.offsetBottom = config.offsetBottom;
        this.start = config.start;
        this.stickClass = config.stickClass;
        this.endStickClass = config.endStickClass;
        this.mediaQuery = config.mediaQuery;
        this.parentMode = config.parentMode;
        this.orientation = config.orientation;
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onChange($event) {
        this.sticker();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.elem = this.element.nativeElement;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        // define scroll container as parent element
        this.container = this._renderer.parentNode(this.elem);
        this.defineOriginalDimensions();
        this.sticker();
    }
    /**
     * @return {?}
     */
    defineOriginalDimensions() {
        this.originalCss = {
            zIndex: this.getCssValue(this.elem, 'zIndex'),
            position: this.getCssValue(this.elem, 'position'),
            top: this.getCssValue(this.elem, 'top'),
            right: this.getCssValue(this.elem, 'right'),
            left: this.getCssValue(this.elem, 'left'),
            bottom: this.getCssValue(this.elem, 'bottom'),
            width: this.getCssValue(this.elem, 'width'),
        };
    }
    /**
     * @return {?}
     */
    defineYDimensions() {
        this.containerTop = this.getBoundingClientRectValue(this.container, 'top');
        this.windowHeight = window.innerHeight;
        this.elemHeight = this.getCssNumber(this.elem, 'height');
        this.containerHeight = this.getCssNumber(this.container, 'height');
        this.containerStart = this.containerTop + this.getCssNumber(this.container, 'padding-top') + this.scrollbarYPos() - this.offsetTop + this.start;
        if (this.parentMode) {
            this.scrollFinish = this.containerStart - this.start - this.offsetBottom + (this.containerHeight - this.elemHeight);
        }
        else {
            this.scrollFinish = document.body.offsetHeight;
        }
    }
    /**
     * @return {?}
     */
    defineXDimensions() {
        this.containerWidth = this.getCssNumber(this.container, 'width');
        this.setStyles(this.elem, {
            display: 'block',
            position: 'static',
            width: this.width
        });
        this.originalCss.width = this.getCssValue(this.elem, 'width');
    }
    /**
     * @return {?}
     */
    resetElement() {
        this.elem.classList.remove(this.stickClass);
        this.setStyles(this.elem, this.originalCss);
        this.reset.next(this.elem);
    }
    /**
     * @return {?}
     */
    stuckElement() {
        this.isStuck = true;
        this.elem.classList.remove(this.endStickClass);
        this.elem.classList.add(this.stickClass);
        this.setStyles(this.elem, {
            zIndex: this.zIndex,
            position: 'fixed',
            top: this.offsetTop + 'px',
            right: 'auto',
            bottom: 'auto',
            left: this.getBoundingClientRectValue(this.elem, 'left') + 'px',
            width: this.getCssValue(this.elem, 'width')
        });
        this.activated.next(this.elem);
    }
    /**
     * @return {?}
     */
    unstuckElement() {
        this.isStuck = false;
        this.elem.classList.add(this.endStickClass);
        this.container.style.position = 'relative';
        this.setStyles(this.elem, {
            position: 'absolute',
            top: 'auto',
            left: 'auto',
            right: this.getCssValue(this.elem, 'float') === 'right' || this.orientation === 'right' ? 0 : 'auto',
            bottom: this.offsetBottom + 'px',
            width: this.getCssValue(this.elem, 'width')
        });
        this.deactivated.next(this.elem);
    }
    /**
     * @return {?}
     */
    matchMediaQuery() {
        if (!this.mediaQuery) {
            return true;
        }
        return (window.matchMedia('(' + this.mediaQuery + ')').matches ||
            window.matchMedia(this.mediaQuery).matches);
    }
    /**
     * @return {?}
     */
    sticker() {
        // check media query
        if (this.isStuck && !this.matchMediaQuery()) {
            this.resetElement();
            return;
        }
        // detecting when a container's height, width or top position changes
        /** @type {?} */
        const currentContainerHeight = this.getCssNumber(this.container, 'height');
        /** @type {?} */
        const currentContainerWidth = this.getCssNumber(this.container, 'width');
        /** @type {?} */
        const currentContainerTop = this.getBoundingClientRectValue(this.container, 'top');
        if (currentContainerHeight !== this.containerHeight) {
            this.defineYDimensions();
        }
        if (currentContainerTop !== this.containerTop) {
            this.defineYDimensions();
        }
        if (currentContainerWidth !== this.containerWidth) {
            this.defineXDimensions();
        }
        // check if the sticky element is above the container
        if (this.elemHeight >= currentContainerHeight) {
            return;
        }
        /** @type {?} */
        const position = this.scrollbarYPos();
        if (this.isStuck && (position < this.containerStart || position > this.scrollFinish) || position > this.scrollFinish) { // unstick
            this.resetElement();
            if (position > this.scrollFinish) {
                this.unstuckElement();
            }
            this.isStuck = false;
        }
        else if (position > this.containerStart && position < this.scrollFinish) { // stick
            this.stuckElement();
        }
    }
    /**
     * @private
     * @return {?}
     */
    scrollbarYPos() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }
    /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    getBoundingClientRectValue(element, property) {
        /** @type {?} */
        let result = 0;
        if (element && element.getBoundingClientRect) {
            /** @type {?} */
            const rect = element.getBoundingClientRect();
            result = (typeof rect[property] !== 'undefined') ? rect[property] : 0;
        }
        return result;
    }
    /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    getCssValue(element, property) {
        /** @type {?} */
        let result = '';
        if (typeof window.getComputedStyle !== 'undefined') {
            result = window.getComputedStyle(element, '').getPropertyValue(property);
        }
        else if (typeof element.currentStyle !== 'undefined') {
            result = element.currentStyle[property];
        }
        return result;
    }
    /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    getCssNumber(element, property) {
        if (typeof element === 'undefined') {
            return 0;
        }
        return parseInt(this.getCssValue(element, property), 10) || 0;
    }
    /**
     * @private
     * @param {?} element
     * @param {?} styles
     * @return {?}
     */
    setStyles(element, styles) {
        if (typeof styles === 'object') {
            for (const property in styles) {
                if (styles.hasOwnProperty(property)) {
                    this._renderer.setStyle(element, property, styles[property]);
                }
            }
        }
    }
}
NgtSticky.decorators = [
    { type: Component, args: [{
                selector: 'ngt-sticky,[ngtSticky]',
                template: '<ng-content></ng-content>'
            }] }
];
/** @nocollapse */
NgtSticky.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: NgtStickyConfig }
];
NgtSticky.propDecorators = {
    sticky: [{ type: Input, args: ['sticky',] }],
    zIndex: [{ type: Input, args: ['sticky-zIndex',] }],
    width: [{ type: Input, args: ['sticky-width',] }],
    offsetTop: [{ type: Input, args: ['sticky-offset-top',] }],
    offsetBottom: [{ type: Input, args: ['sticky-offset-bottom',] }],
    start: [{ type: Input, args: ['sticky-start',] }],
    stickClass: [{ type: Input, args: ['sticky-class',] }],
    endStickClass: [{ type: Input, args: ['sticky-end-class',] }],
    mediaQuery: [{ type: Input, args: ['sticky-media-query',] }],
    parentMode: [{ type: Input, args: ['sticky-parent',] }],
    orientation: [{ type: Input, args: ['sticky-orientation',] }],
    activated: [{ type: Output }],
    deactivated: [{ type: Output }],
    reset: [{ type: Output }],
    onChange: [{ type: HostListener, args: ['window:scroll', ['$event'],] }, { type: HostListener, args: ['window:resize', ['$event'],] }, { type: HostListener, args: ['window:orientationchange', ['$event'],] }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGT_STICKY_DIRECTIVES = [NgtSticky];
class NgtStickyModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtStickyModule };
    }
}
NgtStickyModule.decorators = [
    { type: NgModule, args: [{
                declarations: NGT_STICKY_DIRECTIVES,
                exports: NGT_STICKY_DIRECTIVES,
                imports: [CommonModule]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Configuration service for the NgtTabset component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the tabsets used in the application.
 */
class NgtTabsetConfig {
    constructor() {
        this.justify = 'start';
        this.orientation = 'horizontal';
        this.type = 'tabs';
    }
}
NgtTabsetConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtTabsetConfig.ngInjectableDef = defineInjectable({ factory: function NgtTabsetConfig_Factory() { return new NgtTabsetConfig(); }, token: NgtTabsetConfig, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
let nextId$1 = 0;
/**
 * This directive should be used to wrap tab titles that need to contain HTML markup or other directives.
 */
class NgtTabTitle {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtTabTitle.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtTabTitle]' },] }
];
/** @nocollapse */
NgtTabTitle.ctorParameters = () => [
    { type: TemplateRef }
];
/**
 * This directive must be used to wrap content to be displayed in a tab.
 */
class NgtTabContent {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtTabContent.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtTabContent]' },] }
];
/** @nocollapse */
NgtTabContent.ctorParameters = () => [
    { type: TemplateRef }
];
/**
 * A directive representing an individual tab.
 */
class NgtTab {
    constructor() {
        /**
         * Unique tab identifier. Must be unique for the entire document for proper accessibility support.
         */
        this.id = `ngt-tab-${nextId$1++}`;
        /**
         * Allows toggling disabled state of a given state. Disabled tabs can't be selected.
         */
        this.disabled = false;
    }
    /**
     * @return {?}
     */
    ngAfterContentChecked() {
        // We are using @ContentChildren instead of @ContentChild as in the Angular version being used
        // only @ContentChildren allows us to specify the {descendants: false} option.
        // Without {descendants: false} we are hitting bugs described in:
        // https://github.com/ng-bootstrap/ng-bootstrap/issues/2240
        this.titleTpl = this.titleTpls.first;
        this.contentTpl = this.contentTpls.first;
    }
}
NgtTab.decorators = [
    { type: Directive, args: [{ selector: 'ngt-tab' },] }
];
NgtTab.propDecorators = {
    id: [{ type: Input }],
    title: [{ type: Input }],
    disabled: [{ type: Input }],
    titleTpls: [{ type: ContentChildren, args: [NgtTabTitle, { descendants: false },] }],
    contentTpls: [{ type: ContentChildren, args: [NgtTabContent, { descendants: false },] }]
};
/**
 * A component that makes it easy to create tabbed interface.
 */
class NgtTabset {
    /**
     * @param {?} config
     */
    constructor(config) {
        /**
         * Whether the closed tabs should be hidden without destroying them
         */
        this.destroyOnHide = true;
        /**
         * A tab change event fired right before the tab selection happens. See NgtTabChangeEvent for payload details
         */
        this.tabChange = new EventEmitter();
        this.type = config.type;
        this.justify = config.justify;
        this.orientation = config.orientation;
    }
    /**
     * The horizontal alignment of the nav with flexbox utilities. Can be one of 'start', 'center', 'end', 'fill' or
     * 'justified'
     * The default value is 'start'.
     * @param {?} className
     * @return {?}
     */
    set justify(className) {
        if (className === 'fill' || className === 'justified') {
            this.justifyClass = `nav-${className}`;
        }
        else {
            this.justifyClass = `justify-content-${className}`;
        }
    }
    /**
     * Selects the tab with the given id and shows its associated pane.
     * Any other tab that was previously selected becomes unselected and its associated pane is hidden.
     * @param {?} tabId
     * @return {?}
     */
    select(tabId) {
        /** @type {?} */
        let selectedTab = this._getTabById(tabId);
        if (selectedTab && !selectedTab.disabled && this.activeId !== selectedTab.id) {
            /** @type {?} */
            let defaultPrevented = false;
            this.tabChange.emit({
                activeId: this.activeId, nextId: selectedTab.id, preventDefault: (/**
                 * @return {?}
                 */
                () => {
                    defaultPrevented = true;
                })
            });
            if (!defaultPrevented) {
                this.activeId = selectedTab.id;
            }
        }
    }
    /**
     * @return {?}
     */
    ngAfterContentChecked() {
        // auto-correct activeId that might have been set incorrectly as input
        /** @type {?} */
        let activeTab = this._getTabById(this.activeId);
        this.activeId = activeTab ? activeTab.id : (this.tabs.length ? this.tabs.first.id : null);
    }
    /**
     * @private
     * @param {?} id
     * @return {?}
     */
    _getTabById(id) {
        /** @type {?} */
        let tabsWithId = this.tabs.filter((/**
         * @param {?} tab
         * @return {?}
         */
        tab => tab.id === id));
        return tabsWithId.length ? tabsWithId[0] : null;
    }
}
NgtTabset.decorators = [
    { type: Component, args: [{
                selector: 'ngt-tabset',
                exportAs: 'ngtTabset',
                template: `
        <ul [class]="'nav nav-' + type + (orientation == 'horizontal'?  ' ' + justifyClass : ' flex-column')" role="tablist">
            <li class="nav-item" *ngFor="let tab of tabs">
                <a [id]="tab.id" class="nav-link" [class.active]="tab.id === activeId" [class.disabled]="tab.disabled"
                   href (click)="select(tab.id); $event.preventDefault()" role="tab" [attr.tabindex]="(tab.disabled ? '-1': undefined)"
                   [attr.aria-controls]="(!destroyOnHide || tab.id === activeId ? tab.id + '-panel' : null)"
                   [attr.aria-expanded]="tab.id === activeId" [attr.aria-disabled]="tab.disabled">
                    {{tab.title}}
                    <ng-template [ngTemplateOutlet]="tab.titleTpl?.templateRef"></ng-template>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <ng-template ngFor let-tab [ngForOf]="tabs">
                <div
                    class="tab-pane {{tab.id === activeId ? 'active' : null}}"
                    *ngIf="!destroyOnHide || tab.id === activeId"
                    role="tabpanel"
                    [attr.aria-labelledby]="tab.id" id="{{tab.id}}-panel"
                    [attr.aria-expanded]="tab.id === activeId">
                    <ng-template [ngTemplateOutlet]="tab.contentTpl?.templateRef"></ng-template>
                </div>
            </ng-template>
        </div>
    `
            }] }
];
/** @nocollapse */
NgtTabset.ctorParameters = () => [
    { type: NgtTabsetConfig }
];
NgtTabset.propDecorators = {
    tabs: [{ type: ContentChildren, args: [NgtTab,] }],
    activeId: [{ type: Input }],
    destroyOnHide: [{ type: Input }],
    justify: [{ type: Input }],
    orientation: [{ type: Input }],
    type: [{ type: Input }],
    tabChange: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGT_TABSET_DIRECTIVES = [NgtTabset, NgtTab, NgtTabContent, NgtTabTitle];
class NgtTabsetModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtTabsetModule };
    }
}
NgtTabsetModule.decorators = [
    { type: NgModule, args: [{
                declarations: NGT_TABSET_DIRECTIVES,
                exports: NGT_TABSET_DIRECTIVES,
                imports: [CommonModule]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * Configuration service for the NgtTooltip directive.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the tooltips used in the application.
 */
class NgtTooltipConfig {
    constructor() {
        this.autoClose = true;
        this.placement = 'auto';
        this.triggers = 'hover focus';
        this.disableTooltip = false;
        this.openDelay = 0;
        this.closeDelay = 0;
    }
}
NgtTooltipConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtTooltipConfig.ngInjectableDef = defineInjectable({ factory: function NgtTooltipConfig_Factory() { return new NgtTooltipConfig(); }, token: NgtTooltipConfig, providedIn: "root" });

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
let nextId$2 = 0;
class NgtTooltipWindow {
}
NgtTooltipWindow.decorators = [
    { type: Component, args: [{
                selector: 'ngt-tooltip-window',
                changeDetection: ChangeDetectionStrategy.OnPush,
                encapsulation: ViewEncapsulation.None,
                host: { '[class]': '"tooltip show" + (tooltipClass ? " " + tooltipClass : "")', 'role': 'tooltip', '[id]': 'id' },
                template: `<div class="arrow"></div><div class="tooltip-inner"><ng-content></ng-content></div>`,
                styles: ["ngt-tooltip-window.bs-tooltip-bottom .arrow,ngt-tooltip-window.bs-tooltip-top .arrow{left:calc(50% - .4rem)}ngt-tooltip-window.bs-tooltip-bottom-left .arrow,ngt-tooltip-window.bs-tooltip-top-left .arrow{left:1em}ngt-tooltip-window.bs-tooltip-bottom-right .arrow,ngt-tooltip-window.bs-tooltip-top-right .arrow{left:auto;right:.8rem}ngt-tooltip-window.bs-tooltip-left .arrow,ngt-tooltip-window.bs-tooltip-right .arrow{top:calc(50% - .4rem)}ngt-tooltip-window.bs-tooltip-left-top .arrow,ngt-tooltip-window.bs-tooltip-right-top .arrow{top:.4rem}ngt-tooltip-window.bs-tooltip-left-bottom .arrow,ngt-tooltip-window.bs-tooltip-right-bottom .arrow{top:auto;bottom:.4rem}"]
            }] }
];
NgtTooltipWindow.propDecorators = {
    id: [{ type: Input }],
    tooltipClass: [{ type: Input }]
};
/**
 * A lightweight, extensible directive for fancy tooltip creation.
 */
class NgtTooltip {
    /**
     * @param {?} _elementRef
     * @param {?} _renderer
     * @param {?} injector
     * @param {?} componentFactoryResolver
     * @param {?} viewContainerRef
     * @param {?} config
     * @param {?} _ngZone
     * @param {?} _document
     * @param {?} _changeDetector
     */
    constructor(_elementRef, _renderer, injector, componentFactoryResolver, viewContainerRef, config, _ngZone, _document, _changeDetector) {
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this._ngZone = _ngZone;
        this._document = _document;
        this._changeDetector = _changeDetector;
        /**
         * Emits an event when the tooltip is shown
         */
        this.shown = new EventEmitter();
        /**
         * Emits an event when the tooltip is hidden
         */
        this.hidden = new EventEmitter();
        this._ngtTooltipWindowId = `ngt-tooltip-${nextId$2++}`;
        this.autoClose = config.autoClose;
        this.placement = config.placement;
        this.triggers = config.triggers;
        this.container = config.container;
        this.disableTooltip = config.disableTooltip;
        this.tooltipClass = config.tooltipClass;
        this.openDelay = config.openDelay;
        this.closeDelay = config.closeDelay;
        this._popupService = new PopupService(NgtTooltipWindow, injector, viewContainerRef, _renderer, componentFactoryResolver);
        this._zoneSubscription = _ngZone.onStable.subscribe((/**
         * @return {?}
         */
        () => {
            if (this._windowRef) {
                positionElements(this._elementRef.nativeElement, this._windowRef.location.nativeElement, this.placement, this.container === 'body', 'bs-tooltip');
            }
        }));
    }
    /**
     * Content to be displayed as tooltip. If falsy, the tooltip won't open.
     * @param {?} value
     * @return {?}
     */
    set ngtTooltip(value) {
        this._ngtTooltip = value;
        if (!value && this._windowRef) {
            this.close();
        }
    }
    /**
     * @return {?}
     */
    get ngtTooltip() {
        return this._ngtTooltip;
    }
    /**
     * Opens an element’s tooltip. This is considered a “manual” triggering of the tooltip.
     * The context is an optional value to be injected into the tooltip template when it is created.
     * @param {?=} context
     * @return {?}
     */
    open(context) {
        if (!this._windowRef && this._ngtTooltip && !this.disableTooltip) {
            this._windowRef = this._popupService.open(this._ngtTooltip, context);
            this._windowRef.instance.tooltipClass = this.tooltipClass;
            this._windowRef.instance.id = this._ngtTooltipWindowId;
            this._renderer.setAttribute(this._elementRef.nativeElement, 'aria-describedby', this._ngtTooltipWindowId);
            if (this.container === 'body') {
                this._document.querySelector(this.container).appendChild(this._windowRef.location.nativeElement);
            }
            // apply styling to set basic css-classes on target element, before going for positioning
            this._windowRef.changeDetectorRef.markForCheck();
            ngtAutoClose(this._ngZone, this._document, this.autoClose, (/**
             * @return {?}
             */
            () => this.close()), this.hidden, [this._windowRef.location.nativeElement]);
            this.shown.emit();
        }
    }
    /**
     * Closes an element’s tooltip. This is considered a “manual” triggering of the tooltip.
     * @return {?}
     */
    close() {
        if (this._windowRef != null) {
            this._renderer.removeAttribute(this._elementRef.nativeElement, 'aria-describedby');
            this._popupService.close();
            this._windowRef = null;
            this.hidden.emit();
            this._changeDetector.markForCheck();
        }
    }
    /**
     * Toggles an element’s tooltip. This is considered a “manual” triggering of the tooltip.
     * @return {?}
     */
    toggle() {
        if (this._windowRef) {
            this.close();
        }
        else {
            this.open();
        }
    }
    /**
     * Returns whether or not the tooltip is currently being shown
     * @return {?}
     */
    isOpen() {
        return this._windowRef != null;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._unregisterListenersFn = listenToTriggers(this._renderer, this._elementRef.nativeElement, this.triggers, this.isOpen.bind(this), this.open.bind(this), this.close.bind(this), +this.openDelay, +this.closeDelay);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.close();
        // This check is needed as it might happen that ngOnDestroy is called before ngOnInit
        // under certain conditions, see: https://github.com/ng-bootstrap/ng-bootstrap/issues/2199
        if (this._unregisterListenersFn) {
            this._unregisterListenersFn();
        }
        this._zoneSubscription.unsubscribe();
    }
}
NgtTooltip.decorators = [
    { type: Directive, args: [{ selector: '[ngtTooltip]', exportAs: 'ngtTooltip' },] }
];
/** @nocollapse */
NgtTooltip.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: Injector },
    { type: ComponentFactoryResolver },
    { type: ViewContainerRef },
    { type: NgtTooltipConfig },
    { type: NgZone },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: ChangeDetectorRef }
];
NgtTooltip.propDecorators = {
    autoClose: [{ type: Input }],
    placement: [{ type: Input }],
    triggers: [{ type: Input }],
    container: [{ type: Input }],
    disableTooltip: [{ type: Input }],
    tooltipClass: [{ type: Input }],
    openDelay: [{ type: Input }],
    closeDelay: [{ type: Input }],
    shown: [{ type: Output }],
    hidden: [{ type: Output }],
    ngtTooltip: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NgtTooltipModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtTooltipModule };
    }
}
NgtTooltipModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NgtTooltip, NgtTooltipWindow],
                exports: [NgtTooltip],
                entryComponents: [NgtTooltipWindow]
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NGC_MODULES = [
    NgtAccordionModule, NgtAlertModule, NgtCollapseModule, NgtDropdownModule, NgtLoaderModule, NgtModalModule, NgtNotificationModule,
    NgtPanelModule, NgtPaginationModule, NgtPopoverModule, NgtProgressbarModule, NgtScrollSpyModule, NgtStickyModule, NgtTabsetModule, NgtTooltipModule
];
class NgtModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtModule };
    }
}
NgtModule.decorators = [
    { type: NgModule, args: [{
                imports: NGC_MODULES,
                exports: NGC_MODULES
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { NgtAccordionModule, NgtAccordionConfig, NgtAccordionComponent, NgtAccordionPanelDirective, NgtAccordionPanelTitleDirective, NgtAccordionPanelContentDirective, NgtAccordionPanelHeaderDirective, NgtAccordionPanelToggleDirective, NgtAlertModule, NgtAlertComponent, NgtAlertConfig, NgtCollapseModule, NgtCollapseService, NgtCollapseDirective, NgtCollapseTriggerDirective, NgtDropdownModule, NgtDropdownConfig, NgtDropdownDirective, NgtDropdownMenuDirective, NgtDropdownItemDirective, NgtDropdownAnchorDirective, NgtDropdownToggleDirective, NgtLoaderModule, NgtLoader, NgtLoaderService, NgtLoaderComponent, NgtLoaderDirective, NgtModalModule, NgtModalService, NgtModalConfig, NgtActiveModal, NgtModalRef, ModalDismissReasons, NgtModalComponent, NgtModalOpenDirective, NgtModalCloseDirective, NgtNotificationModule, NgtNotification, NgtNotificationService, NgtNotificationStack, NgtNotificationComponent, NgtPaginationModule, NgtPagination, NgtPaginationFirst, NgtPaginationLast, NgtPaginationPrevious, NgtPaginationNext, NgtPaginationEllipsis, NgtPaginationNumber, NgtPaginationConfig, NgtPanelModule, NgtPanel, NgtPanelService, NgtPanelComponent, NgtPanelsComponent, NgtPanelOpenDirective, NgtPanelCloseDirective, NGC_PANEL_CONFIG, DEFAULT_NGC_PANEL_CONFIG, NgtPopoverModule, NgtPopover, NgtPopoverConfig, NgtProgressbarModule, NgtProgressbar, NgtProgressbarConfig, NgtScrollSpyModule, NgtScrollSpy, NgtStickyModule, NgtSticky, NgtStickyConfig, NgtTabsetModule, NgtTabset, NgtTab, NgtTabContent, NgtTabTitle, NgtTabsetConfig, NgtTooltipModule, NgtTooltip, NgtTooltipConfig, NgtModule, NgtAccordionService as ɵd, NgtCollapseConfig as ɵe, NgtModalBackdropComponent as ɵi, NgtModalDismissDirective as ɵa, NgtModalStack as ɵf, NgtModalWindowComponent as ɵh, NgtPopoverWindow as ɵb, NgtTooltipWindow as ɵc, ContentRef as ɵj, ScrollBar as ɵg };

//# sourceMappingURL=ng-tools.js.map