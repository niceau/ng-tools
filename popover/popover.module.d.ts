import { ModuleWithProviders } from '@angular/core';
export { NgtPopover } from './popover';
export { NgtPopoverConfig } from './popover-config';
export { Placement } from '../util/positioning';
export declare class NgtPopoverModule {
    static forRoot(): ModuleWithProviders;
}
