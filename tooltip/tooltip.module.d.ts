import { ModuleWithProviders } from '@angular/core';
export { NgtTooltipConfig } from './tooltip-config';
export { NgtTooltip } from './tooltip';
export { Placement } from '../util/positioning';
export declare class NgtTooltipModule {
    static forRoot(): ModuleWithProviders;
}
