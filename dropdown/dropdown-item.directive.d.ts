import { ElementRef } from '@angular/core';
/**
 * A directive you should put put on a dropdown item to enable keyboard navigation.
 * Keyboard navigation using arrow keys will move focus between items marked with this directive.
 */
export declare class NgtDropdownItemDirective {
    elementRef: ElementRef<HTMLElement>;
    private _disabled;
    class: boolean;
    classDisabled: boolean;
    disabled: boolean;
    constructor(elementRef: ElementRef<HTMLElement>);
}
