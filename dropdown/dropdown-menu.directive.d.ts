import { ElementRef, OnDestroy, QueryList, Renderer2 } from '@angular/core';
import { NgtDropdownItemDirective } from './dropdown-item.directive';
import { Placement } from '../util/positioning';
import { NgtDropdownService } from './dropdown.service';
import { Subscription } from 'rxjs';
/**
 */
export declare class NgtDropdownMenuDirective implements OnDestroy {
    private _elementRef;
    $dropdownService: NgtDropdownService;
    private _renderer;
    placement: Placement;
    isOpen: boolean;
    isOpenSubscription: Subscription;
    menuItems: QueryList<NgtDropdownItemDirective>;
    dropdownMenu: boolean;
    show: boolean;
    xPlacement: Placement;
    constructor(_elementRef: ElementRef<HTMLElement>, $dropdownService: NgtDropdownService, _renderer: Renderer2);
    ngOnDestroy(): void;
    getNativeElement(): HTMLElement;
    position(triggerEl: any, placement: any): void;
    applyPlacement(_placement: Placement): void;
}
