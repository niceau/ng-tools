import { ModuleWithProviders } from '@angular/core';
export { NgtDropdownDirective } from './dropdown';
export { NgtDropdownAnchorDirective } from './dropdown';
export { NgtDropdownToggleDirective } from './dropdown';
export { NgtDropdownMenuDirective } from './dropdown';
export { NgtDropdownItemDirective } from './dropdown';
export { NgtDropdownConfig } from './dropdown-config';
export declare class NgtDropdownModule {
    static forRoot(): ModuleWithProviders;
}
