import { ElementRef } from '@angular/core';
import { NgtDropdownAnchorDirective } from './dropdown-anchor.directive';
import { NgtDropdownService } from './dropdown.service';
/**
 * Allows the dropdown to be toggled via click. This directive is optional: you can use NgtDropdownAnchorDirective as an
 * alternative.
 */
export declare class NgtDropdownToggleDirective extends NgtDropdownAnchorDirective {
    class: boolean;
    ariaHaspopup: boolean;
    ariaExpanded: boolean;
    onClick(): void;
    constructor(elementRef: ElementRef<HTMLElement>, $dropdownService: NgtDropdownService);
    toggleOpen(): void;
}
