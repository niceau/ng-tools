import { BehaviorSubject, Subject } from 'rxjs';
export declare class NgtDropdownService {
    _isOpen: BehaviorSubject<boolean>;
    onToggleChange: Subject<{}>;
    constructor();
    isOpen(): boolean;
}
