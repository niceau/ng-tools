import { PlacementArray } from '../util/positioning';
/**
 * Configuration service for the NgtDropdown directive.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the dropdowns used in the application.
 */
export declare class NgtDropdownConfig {
    autoClose: boolean | 'outside' | 'inside';
    placement: PlacementArray;
    container: null | 'body';
}
