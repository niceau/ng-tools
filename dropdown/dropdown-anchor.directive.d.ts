import { ElementRef } from '@angular/core';
import { NgtDropdownService } from './dropdown.service';
/**
 * Marks an element to which dropdown menu will be anchored. This is a simple version
 * of the NgtDropdownToggleDirective directive. It plays the same role as NgtDropdownToggleDirective but
 * doesn't listen to click events to toggle dropdown menu thus enabling support for
 * events other than click.
 *
 * @since 1.1.0
 */
export declare class NgtDropdownAnchorDirective {
    private _elementRef;
    $dropdownService: NgtDropdownService;
    anchorEl: any;
    class: boolean;
    ariaHaspopup: boolean;
    ariaExpanded: boolean;
    constructor(_elementRef: ElementRef<HTMLElement>, $dropdownService: NgtDropdownService);
    getNativeElement(): HTMLElement;
}
