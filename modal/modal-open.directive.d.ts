import { NgtModalService } from './modal.service';
export declare class NgtModalOpenDirective {
    private _modalService;
    id: string;
    constructor(_modalService: NgtModalService);
    onClick(): void;
}
