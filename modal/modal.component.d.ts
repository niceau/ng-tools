import { OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Subject } from 'rxjs';
import { NgtModalService } from './modal.service';
import { NgtModalConfig, NgtModalOptions } from './modal-config';
import { NgtActiveModal } from './modal-ref';
export declare class NgtModalComponent implements OnInit, OnDestroy {
    activeModal: NgtActiveModal;
    private _modalService;
    private _config;
    private _elRef;
    id: string;
    backdrop: boolean | 'static';
    beforeDismiss: () => boolean | Promise<boolean>;
    centered: boolean;
    container: string;
    keyboard: boolean;
    size: 'sm' | 'lg';
    scrollableContent: boolean;
    windowClass: string;
    backdropClass: string;
    modalClosingDidStart: Subject<{}>;
    modalClosingDidDone: Subject<{}>;
    modalOpeningDidStart: Subject<{}>;
    modalOpeningDidDone: Subject<{}>;
    backdropClosingDidStart: Subject<{}>;
    backdropClosingDidDone: Subject<{}>;
    backdropOpeningDidStart: Subject<{}>;
    backdropOpeningDidDone: Subject<{}>;
    options: NgtModalOptions;
    constructor(activeModal: NgtActiveModal, _modalService: NgtModalService, _config: NgtModalConfig, _elRef: ElementRef);
    ngOnInit(): void;
    ngOnDestroy(): void;
    /**
     * Open the modal with an modal component reference without id.
     */
    open(): void;
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     */
    close(result?: any): void;
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     */
    dismiss(reason?: any): void;
}
