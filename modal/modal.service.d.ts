import { Injector, ComponentFactoryResolver } from '@angular/core';
import { NgtModalConfig, NgtModalOptions } from './modal-config';
import { NgtModalStack } from './modal-stack';
import { NgtModalRef } from './modal-ref';
import { NgtModalComponent } from './modal.component';
/**
 * A service to open modal windows. Creating a modal is straightforward: create a template and pass it as an argument to
 * the "open" method!
 */
export declare class NgtModalService {
    private _moduleCFR;
    private _injector;
    private _modalStack;
    private _config;
    private _components;
    constructor(_moduleCFR: ComponentFactoryResolver, _injector: Injector, _modalStack: NgtModalStack, _config: NgtModalConfig);
    /**
     * Opens a new modal window with the specified content and using supplied options. Content can be provided
     * as a TemplateRef or a component type. If you pass a component type as content, then instances of those
     * components can be injected with an instance of the NgtActiveModal class. You can use methods on the
     * NgtActiveModal class to close / dismiss modals from "inside" of a component.
     */
    open(content: any, options?: NgtModalOptions): NgtModalRef;
    /**
     * Dismiss all currently displayed modal windows with the supplied reason.
     */
    dismissAll(reason?: any): void;
    /**
     * Indicates if there are currently any open modal windows in the application.
     */
    hasOpenModals(): boolean;
    /**
     * Add modal component instance to _components list.
     * !NOTE: modal must have id;
     */
    add(componentRef: NgtModalComponent): void;
    /**
     * Remove modal component instance from _components list.
     */
    remove(id: string): void;
    /**
     * Opens a new modal window with the specified content and using supplied options founded in
     * _components list by specified id.
     */
    openById(id: string): void;
    /**
     * Call close method in modal component instance founded by specified id.
     */
    closeById(id: string, result?: any): void;
    /**
     * Call dismiss method in modal component instance founded by specified id.
     */
    dismissById(id: string, reason?: any): void;
}
