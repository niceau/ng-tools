import { NgtModalService } from './modal.service';
export declare class NgtModalCloseDirective {
    private _modalService;
    id: string;
    result: string;
    constructor(_modalService: NgtModalService);
    onClick(): void;
}
