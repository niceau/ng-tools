import { ModuleWithProviders } from '@angular/core';
export { NgtModalService } from './modal.service';
export { NgtModalRef, NgtActiveModal } from './modal-ref';
export { NgtModalComponent } from './modal.component';
export { NgtModalOpenDirective } from './modal-open.directive';
export { NgtModalCloseDirective } from './modal-close.directive';
export { NgtModalDismissDirective } from './modal-dismiss.directive';
export { NgtModalConfig, NgtModalOptions } from './modal-config';
export { ModalDismissReasons } from './modal-dismiss-reasons';
export declare class NgtModalModule {
    static forRoot(): ModuleWithProviders;
}
