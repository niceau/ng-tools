import { NgtModalService } from './modal.service';
export declare class NgtModalDismissDirective {
    private _modalService;
    id: string;
    reason: string;
    constructor(_modalService: NgtModalService);
    onClick(): void;
}
