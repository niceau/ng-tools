import { ComponentRef } from '@angular/core';
import { NgtModalWindowComponent } from './modal-window.component';
import { NgtModalBackdropComponent } from './modal-backdrop.component';
import { Subject } from 'rxjs';
import { ContentRef } from '../util/popup';
/**
 * A reference to an active (currently opened) modal. Instances of this class
 * can be injected into components passed as modal content.
 */
export declare class NgtActiveModal {
    modalOpeningDidStart: Subject<{}>;
    modalOpeningDidDone: Subject<{}>;
    modalClosingDidStart: Subject<{}>;
    modalClosingDidDone: Subject<{}>;
    backdropOpeningDidStart: Subject<{}>;
    backdropOpeningDidDone: Subject<{}>;
    backdropClosingDidStart: Subject<{}>;
    backdropClosingDidDone: Subject<{}>;
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     */
    close(result?: any): void;
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     */
    dismiss(reason?: any): void;
}
/**
 * A reference to a newly opened modal returned by the 'NgtModalComponent.open()' method.
 */
export declare class NgtModalRef {
    private _windowCmptRef;
    private _contentRef;
    private _backdropCmptRef?;
    private _beforeDismiss?;
    private _resolve;
    private _reject;
    modalOpeningDidStart: Subject<{}>;
    modalOpeningDidDone: Subject<{}>;
    modalClosingDidStart: Subject<{}>;
    modalClosingDidDone: Subject<{}>;
    backdropOpeningDidStart: Subject<{}>;
    backdropOpeningDidDone: Subject<{}>;
    backdropClosingDidStart: Subject<{}>;
    backdropClosingDidDone: Subject<{}>;
    /**
     * The instance of component used as modal's content.
     * Undefined when a TemplateRef is used as modal's content.
     */
    readonly componentInstance: any;
    /**
     * A promise that is resolved when the modal is closed and rejected when the modal is dismissed.
     */
    result: Promise<any>;
    constructor(_windowCmptRef: ComponentRef<NgtModalWindowComponent>, _contentRef: ContentRef, _backdropCmptRef?: ComponentRef<NgtModalBackdropComponent>, _beforeDismiss?: Function);
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     */
    close(result?: any): void;
    private _dismiss;
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     */
    dismiss(reason?: any): void;
    private _removeModalElements;
}
