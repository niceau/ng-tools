import { OnInit } from '@angular/core';
import { Subject } from 'rxjs';
export declare class NgtModalBackdropComponent implements OnInit {
    backdropClass: string;
    backdropClosingDidStart: Subject<{}>;
    backdropClosingDidDone: Subject<{}>;
    backdropOpeningDidStart: Subject<{}>;
    backdropOpeningDidDone: Subject<{}>;
    class: any;
    zIndex: string;
    animation: string;
    onAnimationStart($event: any): void;
    onAnimationDone($event: any): void;
    ngOnInit(): void;
    animationAction($event: any): void;
}
