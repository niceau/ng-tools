/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtAlertComponent } from './alert.component';
export { NgtAlertComponent } from './alert.component';
export { NgtAlertConfig } from './alert-config';
export class NgtAlertModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtAlertModule };
    }
}
NgtAlertModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NgtAlertComponent],
                exports: [NgtAlertComponent],
                imports: [CommonModule],
                entryComponents: [NgtAlertComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJhbGVydC9hbGVydC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUV0RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFRaEQsTUFBTSxPQUFPLGNBQWM7Ozs7SUFDdkIsTUFBTSxDQUFDLE9BQU87UUFDVixPQUFPLEVBQUMsUUFBUSxFQUFFLGNBQWMsRUFBQyxDQUFDO0lBQ3RDLENBQUM7OztZQVRKLFFBQVEsU0FBQztnQkFDTixZQUFZLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztnQkFDakMsT0FBTyxFQUFFLENBQUMsaUJBQWlCLENBQUM7Z0JBQzVCLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztnQkFDdkIsZUFBZSxFQUFFLENBQUMsaUJBQWlCLENBQUM7YUFDdkMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgTmd0QWxlcnRDb21wb25lbnQgfSBmcm9tICcuL2FsZXJ0LmNvbXBvbmVudCc7XHJcblxyXG5leHBvcnQgeyBOZ3RBbGVydENvbXBvbmVudCB9IGZyb20gJy4vYWxlcnQuY29tcG9uZW50JztcclxuZXhwb3J0IHsgTmd0QWxlcnRDb25maWcgfSBmcm9tICcuL2FsZXJ0LWNvbmZpZyc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbTmd0QWxlcnRDb21wb25lbnRdLFxyXG4gICAgZXhwb3J0czogW05ndEFsZXJ0Q29tcG9uZW50XSxcclxuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBbTmd0QWxlcnRDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RBbGVydE1vZHVsZSB7XHJcbiAgICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge25nTW9kdWxlOiBOZ3RBbGVydE1vZHVsZX07XHJcbiAgICB9XHJcbn1cclxuIl19