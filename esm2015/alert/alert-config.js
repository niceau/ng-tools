/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtAlert component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the alerts used in the application.
 */
export class NgtAlertConfig {
    constructor() {
        this.icon = '';
        this.dismissible = false;
        this.type = 'warning';
    }
}
NgtAlertConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtAlertConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtAlertConfig_Factory() { return new NgtAlertConfig(); }, token: NgtAlertConfig, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtAlertConfig.prototype.icon;
    /** @type {?} */
    NgtAlertConfig.prototype.dismissible;
    /** @type {?} */
    NgtAlertConfig.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQtY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJhbGVydC9hbGVydC1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFRM0MsTUFBTSxPQUFPLGNBQWM7SUFEM0I7UUFFSSxTQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ1YsZ0JBQVcsR0FBRyxLQUFLLENBQUM7UUFDcEIsU0FBSSxHQUFHLFNBQVMsQ0FBQztLQUNwQjs7O1lBTEEsVUFBVSxTQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQzs7Ozs7SUFFNUIsOEJBQVU7O0lBQ1YscUNBQW9COztJQUNwQiw4QkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vKipcclxuICogQ29uZmlndXJhdGlvbiBzZXJ2aWNlIGZvciB0aGUgTmd0QWxlcnQgY29tcG9uZW50LlxyXG4gKiBZb3UgY2FuIGluamVjdCB0aGlzIHNlcnZpY2UsIHR5cGljYWxseSBpbiB5b3VyIHJvb3QgY29tcG9uZW50LCBhbmQgY3VzdG9taXplIHRoZSB2YWx1ZXMgb2YgaXRzIHByb3BlcnRpZXMgaW5cclxuICogb3JkZXIgdG8gcHJvdmlkZSBkZWZhdWx0IHZhbHVlcyBmb3IgYWxsIHRoZSBhbGVydHMgdXNlZCBpbiB0aGUgYXBwbGljYXRpb24uXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcclxuZXhwb3J0IGNsYXNzIE5ndEFsZXJ0Q29uZmlnIHtcclxuICAgIGljb24gPSAnJztcclxuICAgIGRpc21pc3NpYmxlID0gZmFsc2U7XHJcbiAgICB0eXBlID0gJ3dhcm5pbmcnO1xyXG59XHJcbiJdfQ==