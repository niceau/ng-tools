/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, HostBinding, Input, Output, Renderer2, ViewEncapsulation } from '@angular/core';
import { NgtAlertConfig } from './alert-config';
/**
 * Alerts can be used to provide feedback messages.
 */
export class NgtAlert {
    /**
     * @param {?} config
     * @param {?} _renderer
     * @param {?} _element
     */
    constructor(config, _renderer, _element) {
        this._renderer = _renderer;
        this._element = _element;
        /**
         * An event emitted when the close button is clicked. This event has no payload. Only relevant for dismissible alerts.
         */
        this.close = new EventEmitter();
        this.class = true;
        this.dismissible = config.dismissible;
        this.type = config.type;
        this.icon = config.icon;
    }
    /**
     * @return {?}
     */
    closeHandler() {
        this.close.emit(null);
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        /** @type {?} */
        const typeChange = changes['type'];
        if (typeChange && !typeChange.firstChange) {
            this._renderer.removeClass(this._element.nativeElement, `alert-${typeChange.previousValue}`);
            this._renderer.addClass(this._element.nativeElement, `alert-${typeChange.currentValue}`);
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._renderer.addClass(this._element.nativeElement, `alert-${this.type}`);
    }
}
NgtAlert.decorators = [
    { type: Component, args: [{
                selector: 'ngt-alert',
                changeDetection: ChangeDetectionStrategy.OnPush,
                encapsulation: ViewEncapsulation.None,
                template: `
        <div *ngIf="icon" class="alert-icon">
            <i class="{{ icon }}"></i>
        </div>
        <div class="alert-text">
            <ng-content></ng-content>
        </div>
        <div *ngIf="dismissible" class="alert-close">
            <button type="button" class="close" (click)="closeHandler()">
                <span aria-hidden="true">
                    <i class="ft-x"></i>
                </span>
            </button>
        </div>
    `,
                styles: ["ngt-alert{display:block}"]
            }] }
];
/** @nocollapse */
NgtAlert.ctorParameters = () => [
    { type: NgtAlertConfig },
    { type: Renderer2 },
    { type: ElementRef }
];
NgtAlert.propDecorators = {
    dismissible: [{ type: Input }],
    icon: [{ type: Input }],
    type: [{ type: Input }],
    close: [{ type: Output }],
    class: [{ type: HostBinding, args: ['class.alert',] }]
};
if (false) {
    /**
     * A flag indicating if a given alert can be dismissed (closed) by a user. If this flag is set, a close button (in a
     * form of an ×) will be displayed.
     * @type {?}
     */
    NgtAlert.prototype.dismissible;
    /**
     * Class of icon that will be used in alert
     * @type {?}
     */
    NgtAlert.prototype.icon;
    /**
     * Alert type (CSS class). System recognizes the following bg's: "primary", "secondary", "success", "danger", "warning", "info", "light" , "dark"
     *  and other utilities bg's.
     *  And also outline version: "outline-primary", "outline-secondary", "outline-success" etc.
     * @type {?}
     */
    NgtAlert.prototype.type;
    /**
     * An event emitted when the close button is clicked. This event has no payload. Only relevant for dismissible alerts.
     * @type {?}
     */
    NgtAlert.prototype.close;
    /** @type {?} */
    NgtAlert.prototype.class;
    /**
     * @type {?}
     * @private
     */
    NgtAlert.prototype._renderer;
    /**
     * @type {?}
     * @private
     */
    NgtAlert.prototype._element;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImFsZXJ0L2FsZXJ0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0gsdUJBQXVCLEVBQ3ZCLFNBQVMsRUFDVCxVQUFVLEVBQ1YsWUFBWSxFQUNaLFdBQVcsRUFDWCxLQUFLLEVBR0wsTUFBTSxFQUNOLFNBQVMsRUFFVCxpQkFBaUIsRUFDcEIsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBMEJoRCxNQUFNLE9BQU8sUUFBUTs7Ozs7O0lBdUJqQixZQUFZLE1BQXNCLEVBQVUsU0FBb0IsRUFBVSxRQUFvQjtRQUFsRCxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBWTs7OztRQUpwRixVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQVEsQ0FBQztRQUVmLFVBQUssR0FBRyxJQUFJLENBQUM7UUFHckMsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDNUIsQ0FBQzs7OztJQUVELFlBQVk7UUFDUixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMxQixDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFzQjs7Y0FDeEIsVUFBVSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUM7UUFDbEMsSUFBSSxVQUFVLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLFNBQVMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUM7WUFDN0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsU0FBUyxVQUFVLENBQUMsWUFBWSxFQUFFLENBQUMsQ0FBQztTQUM1RjtJQUNMLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsU0FBUyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUMvRSxDQUFDOzs7WUFoRUosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxXQUFXO2dCQUNyQixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtnQkFDL0MsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7Z0JBQ3JDLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7S0FjVDs7YUFFSjs7OztZQXpCUSxjQUFjO1lBTG5CLFNBQVM7WUFQVCxVQUFVOzs7MEJBMkNULEtBQUs7bUJBSUwsS0FBSzttQkFNTCxLQUFLO29CQUlMLE1BQU07b0JBRU4sV0FBVyxTQUFDLGFBQWE7Ozs7Ozs7O0lBaEIxQiwrQkFBOEI7Ozs7O0lBSTlCLHdCQUFzQjs7Ozs7OztJQU10Qix3QkFBc0I7Ozs7O0lBSXRCLHlCQUEyQzs7SUFFM0MseUJBQXlDOzs7OztJQUVMLDZCQUE0Qjs7Ozs7SUFBRSw0QkFBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksXHJcbiAgICBDb21wb25lbnQsXHJcbiAgICBFbGVtZW50UmVmLFxyXG4gICAgRXZlbnRFbWl0dGVyLFxyXG4gICAgSG9zdEJpbmRpbmcsXHJcbiAgICBJbnB1dCxcclxuICAgIE9uQ2hhbmdlcyxcclxuICAgIE9uSW5pdCxcclxuICAgIE91dHB1dCxcclxuICAgIFJlbmRlcmVyMixcclxuICAgIFNpbXBsZUNoYW5nZXMsXHJcbiAgICBWaWV3RW5jYXBzdWxhdGlvblxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTmd0QWxlcnRDb25maWcgfSBmcm9tICcuL2FsZXJ0LWNvbmZpZyc7XHJcblxyXG4vKipcclxuICogQWxlcnRzIGNhbiBiZSB1c2VkIHRvIHByb3ZpZGUgZmVlZGJhY2sgbWVzc2FnZXMuXHJcbiAqL1xyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbmd0LWFsZXJ0JyxcclxuICAgIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPGRpdiAqbmdJZj1cImljb25cIiBjbGFzcz1cImFsZXJ0LWljb25cIj5cclxuICAgICAgICAgICAgPGkgY2xhc3M9XCJ7eyBpY29uIH19XCI+PC9pPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJhbGVydC10ZXh0XCI+XHJcbiAgICAgICAgICAgIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2ICpuZ0lmPVwiZGlzbWlzc2libGVcIiBjbGFzcz1cImFsZXJ0LWNsb3NlXCI+XHJcbiAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiY2xvc2VcIiAoY2xpY2spPVwiY2xvc2VIYW5kbGVyKClcIj5cclxuICAgICAgICAgICAgICAgIDxzcGFuIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwiZnQteFwiPjwvaT5cclxuICAgICAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICBgLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vYWxlcnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RBbGVydCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzIHtcclxuICAgIC8qKlxyXG4gICAgICogQSBmbGFnIGluZGljYXRpbmcgaWYgYSBnaXZlbiBhbGVydCBjYW4gYmUgZGlzbWlzc2VkIChjbG9zZWQpIGJ5IGEgdXNlci4gSWYgdGhpcyBmbGFnIGlzIHNldCwgYSBjbG9zZSBidXR0b24gKGluIGFcclxuICAgICAqIGZvcm0gb2YgYW4gw5cpIHdpbGwgYmUgZGlzcGxheWVkLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBkaXNtaXNzaWJsZTogYm9vbGVhbjtcclxuICAgIC8qKlxyXG4gICAgICogQ2xhc3Mgb2YgaWNvbiB0aGF0IHdpbGwgYmUgdXNlZCBpbiBhbGVydFxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBpY29uOiBzdHJpbmc7XHJcbiAgICAvKipcclxuICAgICAqIEFsZXJ0IHR5cGUgKENTUyBjbGFzcykuIFN5c3RlbSByZWNvZ25pemVzIHRoZSBmb2xsb3dpbmcgYmcnczogXCJwcmltYXJ5XCIsIFwic2Vjb25kYXJ5XCIsIFwic3VjY2Vzc1wiLCBcImRhbmdlclwiLCBcIndhcm5pbmdcIiwgXCJpbmZvXCIsIFwibGlnaHRcIiAsIFwiZGFya1wiXHJcbiAgICAgKiAgYW5kIG90aGVyIHV0aWxpdGllcyBiZydzLlxyXG4gICAgICogIEFuZCBhbHNvIG91dGxpbmUgdmVyc2lvbjogXCJvdXRsaW5lLXByaW1hcnlcIiwgXCJvdXRsaW5lLXNlY29uZGFyeVwiLCBcIm91dGxpbmUtc3VjY2Vzc1wiIGV0Yy5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgdHlwZTogc3RyaW5nO1xyXG4gICAgLyoqXHJcbiAgICAgKiBBbiBldmVudCBlbWl0dGVkIHdoZW4gdGhlIGNsb3NlIGJ1dHRvbiBpcyBjbGlja2VkLiBUaGlzIGV2ZW50IGhhcyBubyBwYXlsb2FkLiBPbmx5IHJlbGV2YW50IGZvciBkaXNtaXNzaWJsZSBhbGVydHMuXHJcbiAgICAgKi9cclxuICAgIEBPdXRwdXQoKSBjbG9zZSA9IG5ldyBFdmVudEVtaXR0ZXI8dm9pZD4oKTtcclxuXHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLmFsZXJ0JykgY2xhc3MgPSB0cnVlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbmZpZzogTmd0QWxlcnRDb25maWcsIHByaXZhdGUgX3JlbmRlcmVyOiBSZW5kZXJlcjIsIHByaXZhdGUgX2VsZW1lbnQ6IEVsZW1lbnRSZWYpIHtcclxuICAgICAgICB0aGlzLmRpc21pc3NpYmxlID0gY29uZmlnLmRpc21pc3NpYmxlO1xyXG4gICAgICAgIHRoaXMudHlwZSA9IGNvbmZpZy50eXBlO1xyXG4gICAgICAgIHRoaXMuaWNvbiA9IGNvbmZpZy5pY29uO1xyXG4gICAgfVxyXG5cclxuICAgIGNsb3NlSGFuZGxlcigpIHtcclxuICAgICAgICB0aGlzLmNsb3NlLmVtaXQobnVsbCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgICAgIGNvbnN0IHR5cGVDaGFuZ2UgPSBjaGFuZ2VzWyd0eXBlJ107XHJcbiAgICAgICAgaWYgKHR5cGVDaGFuZ2UgJiYgIXR5cGVDaGFuZ2UuZmlyc3RDaGFuZ2UpIHtcclxuICAgICAgICAgICAgdGhpcy5fcmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5fZWxlbWVudC5uYXRpdmVFbGVtZW50LCBgYWxlcnQtJHt0eXBlQ2hhbmdlLnByZXZpb3VzVmFsdWV9YCk7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMuX2VsZW1lbnQubmF0aXZlRWxlbWVudCwgYGFsZXJ0LSR7dHlwZUNoYW5nZS5jdXJyZW50VmFsdWV9YCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMuX2VsZW1lbnQubmF0aXZlRWxlbWVudCwgYGFsZXJ0LSR7dGhpcy50eXBlfWApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==