/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, HostBinding, Input, Output, Renderer2, ViewEncapsulation } from '@angular/core';
import { NgtAlertConfig } from './alert-config';
/**
 * Alerts can be used to provide feedback messages.
 */
export class NgtAlertComponent {
    /**
     * @param {?} config
     * @param {?} _renderer
     * @param {?} _element
     */
    constructor(config, _renderer, _element) {
        this._renderer = _renderer;
        this._element = _element;
        /**
         * An event emitted when the close button is clicked. This event has no payload. Only relevant for dismissible alerts.
         */
        this.close = new EventEmitter();
        this.class = true;
        this.dismissible = config.dismissible;
        this.type = config.type;
        this.icon = config.icon;
    }
    /**
     * @return {?}
     */
    closeHandler() {
        this.close.emit(null);
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        /** @type {?} */
        const typeChange = changes['type'];
        if (typeChange && !typeChange.firstChange) {
            this._renderer.removeClass(this._element.nativeElement, `alert-${typeChange.previousValue}`);
            this._renderer.addClass(this._element.nativeElement, `alert-${typeChange.currentValue}`);
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._renderer.addClass(this._element.nativeElement, `alert-${this.type}`);
    }
}
NgtAlertComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-alert',
                changeDetection: ChangeDetectionStrategy.OnPush,
                encapsulation: ViewEncapsulation.None,
                template: `
        <div *ngIf="icon" class="alert-icon">
            <i class="{{ icon }}"></i>
        </div>
        <div class="alert-text">
            <ng-content></ng-content>
        </div>
        <div *ngIf="dismissible" class="alert-close">
            <button type="button" class="close" (click)="closeHandler()">
                <span aria-hidden="true">
                    <i class="ft-x"></i>
                </span>
            </button>
        </div>
    `,
                styles: ["ngt-alert{display:block}"]
            }] }
];
/** @nocollapse */
NgtAlertComponent.ctorParameters = () => [
    { type: NgtAlertConfig },
    { type: Renderer2 },
    { type: ElementRef }
];
NgtAlertComponent.propDecorators = {
    dismissible: [{ type: Input }],
    icon: [{ type: Input }],
    type: [{ type: Input }],
    close: [{ type: Output }],
    class: [{ type: HostBinding, args: ['class.alert',] }]
};
if (false) {
    /**
     * A flag indicating if a given alert can be dismissed (closed) by a user. If this flag is set, a close button (in a
     * form of an ×) will be displayed.
     * @type {?}
     */
    NgtAlertComponent.prototype.dismissible;
    /**
     * Class of icon that will be used in alert
     * @type {?}
     */
    NgtAlertComponent.prototype.icon;
    /**
     * Alert type (CSS class). System recognizes the following bg's: "primary", "secondary", "success", "danger", "warning", "info", "light" , "dark"
     *  and other utilities bg's.
     *  And also outline version: "outline-primary", "outline-secondary", "outline-success" etc.
     * @type {?}
     */
    NgtAlertComponent.prototype.type;
    /**
     * An event emitted when the close button is clicked. This event has no payload. Only relevant for dismissible alerts.
     * @type {?}
     */
    NgtAlertComponent.prototype.close;
    /** @type {?} */
    NgtAlertComponent.prototype.class;
    /**
     * @type {?}
     * @private
     */
    NgtAlertComponent.prototype._renderer;
    /**
     * @type {?}
     * @private
     */
    NgtAlertComponent.prototype._element;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJhbGVydC9hbGVydC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDSCx1QkFBdUIsRUFDdkIsU0FBUyxFQUNULFVBQVUsRUFDVixZQUFZLEVBQ1osV0FBVyxFQUNYLEtBQUssRUFHTCxNQUFNLEVBQ04sU0FBUyxFQUVULGlCQUFpQixFQUNwQixNQUFNLGVBQWUsQ0FBQztBQUV2QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUEwQmhELE1BQU0sT0FBTyxpQkFBaUI7Ozs7OztJQXVCMUIsWUFBWSxNQUFzQixFQUFVLFNBQW9CLEVBQVUsUUFBb0I7UUFBbEQsY0FBUyxHQUFULFNBQVMsQ0FBVztRQUFVLGFBQVEsR0FBUixRQUFRLENBQVk7Ozs7UUFKcEYsVUFBSyxHQUFHLElBQUksWUFBWSxFQUFRLENBQUM7UUFFZixVQUFLLEdBQUcsSUFBSSxDQUFDO1FBR3JDLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUN0QyxJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1IsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBc0I7O2NBQ3hCLFVBQVUsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO1FBQ2xDLElBQUksVUFBVSxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRTtZQUN2QyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxTQUFTLFVBQVUsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDO1lBQzdGLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLFNBQVMsVUFBVSxDQUFDLFlBQVksRUFBRSxDQUFDLENBQUM7U0FDNUY7SUFDTCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLFNBQVMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7SUFDL0UsQ0FBQzs7O1lBaEVKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsV0FBVztnQkFDckIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07Z0JBQy9DLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2dCQUNyQyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7O0tBY1Q7O2FBRUo7Ozs7WUF6QlEsY0FBYztZQUxuQixTQUFTO1lBUFQsVUFBVTs7OzBCQTJDVCxLQUFLO21CQUlMLEtBQUs7bUJBTUwsS0FBSztvQkFJTCxNQUFNO29CQUVOLFdBQVcsU0FBQyxhQUFhOzs7Ozs7OztJQWhCMUIsd0NBQThCOzs7OztJQUk5QixpQ0FBc0I7Ozs7Ozs7SUFNdEIsaUNBQXNCOzs7OztJQUl0QixrQ0FBMkM7O0lBRTNDLGtDQUF5Qzs7Ozs7SUFFTCxzQ0FBNEI7Ozs7O0lBQUUscUNBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICAgIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LFxyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgRWxlbWVudFJlZixcclxuICAgIEV2ZW50RW1pdHRlcixcclxuICAgIEhvc3RCaW5kaW5nLFxyXG4gICAgSW5wdXQsXHJcbiAgICBPbkNoYW5nZXMsXHJcbiAgICBPbkluaXQsXHJcbiAgICBPdXRwdXQsXHJcbiAgICBSZW5kZXJlcjIsXHJcbiAgICBTaW1wbGVDaGFuZ2VzLFxyXG4gICAgVmlld0VuY2Fwc3VsYXRpb25cclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IE5ndEFsZXJ0Q29uZmlnIH0gZnJvbSAnLi9hbGVydC1jb25maWcnO1xyXG5cclxuLyoqXHJcbiAqIEFsZXJ0cyBjYW4gYmUgdXNlZCB0byBwcm92aWRlIGZlZWRiYWNrIG1lc3NhZ2VzLlxyXG4gKi9cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC1hbGVydCcsXHJcbiAgICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxkaXYgKm5nSWY9XCJpY29uXCIgY2xhc3M9XCJhbGVydC1pY29uXCI+XHJcbiAgICAgICAgICAgIDxpIGNsYXNzPVwie3sgaWNvbiB9fVwiPjwvaT5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2IGNsYXNzPVwiYWxlcnQtdGV4dFwiPlxyXG4gICAgICAgICAgICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdiAqbmdJZj1cImRpc21pc3NpYmxlXCIgY2xhc3M9XCJhbGVydC1jbG9zZVwiPlxyXG4gICAgICAgICAgICA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cImNsb3NlXCIgKGNsaWNrKT1cImNsb3NlSGFuZGxlcigpXCI+XHJcbiAgICAgICAgICAgICAgICA8c3BhbiBhcmlhLWhpZGRlbj1cInRydWVcIj5cclxuICAgICAgICAgICAgICAgICAgICA8aSBjbGFzcz1cImZ0LXhcIj48L2k+XHJcbiAgICAgICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgICAgIDwvYnV0dG9uPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgYCxcclxuICAgIHN0eWxlVXJsczogWycuL2FsZXJ0LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0QWxlcnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcbiAgICAvKipcclxuICAgICAqIEEgZmxhZyBpbmRpY2F0aW5nIGlmIGEgZ2l2ZW4gYWxlcnQgY2FuIGJlIGRpc21pc3NlZCAoY2xvc2VkKSBieSBhIHVzZXIuIElmIHRoaXMgZmxhZyBpcyBzZXQsIGEgY2xvc2UgYnV0dG9uIChpbiBhXHJcbiAgICAgKiBmb3JtIG9mIGFuIMOXKSB3aWxsIGJlIGRpc3BsYXllZC5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgZGlzbWlzc2libGU6IGJvb2xlYW47XHJcbiAgICAvKipcclxuICAgICAqIENsYXNzIG9mIGljb24gdGhhdCB3aWxsIGJlIHVzZWQgaW4gYWxlcnRcclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgaWNvbjogc3RyaW5nO1xyXG4gICAgLyoqXHJcbiAgICAgKiBBbGVydCB0eXBlIChDU1MgY2xhc3MpLiBTeXN0ZW0gcmVjb2duaXplcyB0aGUgZm9sbG93aW5nIGJnJ3M6IFwicHJpbWFyeVwiLCBcInNlY29uZGFyeVwiLCBcInN1Y2Nlc3NcIiwgXCJkYW5nZXJcIiwgXCJ3YXJuaW5nXCIsIFwiaW5mb1wiLCBcImxpZ2h0XCIgLCBcImRhcmtcIlxyXG4gICAgICogIGFuZCBvdGhlciB1dGlsaXRpZXMgYmcncy5cclxuICAgICAqICBBbmQgYWxzbyBvdXRsaW5lIHZlcnNpb246IFwib3V0bGluZS1wcmltYXJ5XCIsIFwib3V0bGluZS1zZWNvbmRhcnlcIiwgXCJvdXRsaW5lLXN1Y2Nlc3NcIiBldGMuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHR5cGU6IHN0cmluZztcclxuICAgIC8qKlxyXG4gICAgICogQW4gZXZlbnQgZW1pdHRlZCB3aGVuIHRoZSBjbG9zZSBidXR0b24gaXMgY2xpY2tlZC4gVGhpcyBldmVudCBoYXMgbm8gcGF5bG9hZC4gT25seSByZWxldmFudCBmb3IgZGlzbWlzc2libGUgYWxlcnRzLlxyXG4gICAgICovXHJcbiAgICBAT3V0cHV0KCkgY2xvc2UgPSBuZXcgRXZlbnRFbWl0dGVyPHZvaWQ+KCk7XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5hbGVydCcpIGNsYXNzID0gdHJ1ZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb25maWc6IE5ndEFsZXJ0Q29uZmlnLCBwcml2YXRlIF9yZW5kZXJlcjogUmVuZGVyZXIyLCBwcml2YXRlIF9lbGVtZW50OiBFbGVtZW50UmVmKSB7XHJcbiAgICAgICAgdGhpcy5kaXNtaXNzaWJsZSA9IGNvbmZpZy5kaXNtaXNzaWJsZTtcclxuICAgICAgICB0aGlzLnR5cGUgPSBjb25maWcudHlwZTtcclxuICAgICAgICB0aGlzLmljb24gPSBjb25maWcuaWNvbjtcclxuICAgIH1cclxuXHJcbiAgICBjbG9zZUhhbmRsZXIoKSB7XHJcbiAgICAgICAgdGhpcy5jbG9zZS5lbWl0KG51bGwpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgICAgICBjb25zdCB0eXBlQ2hhbmdlID0gY2hhbmdlc1sndHlwZSddO1xyXG4gICAgICAgIGlmICh0eXBlQ2hhbmdlICYmICF0eXBlQ2hhbmdlLmZpcnN0Q2hhbmdlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZUNsYXNzKHRoaXMuX2VsZW1lbnQubmF0aXZlRWxlbWVudCwgYGFsZXJ0LSR7dHlwZUNoYW5nZS5wcmV2aW91c1ZhbHVlfWApO1xyXG4gICAgICAgICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLl9lbGVtZW50Lm5hdGl2ZUVsZW1lbnQsIGBhbGVydC0ke3R5cGVDaGFuZ2UuY3VycmVudFZhbHVlfWApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLl9lbGVtZW50Lm5hdGl2ZUVsZW1lbnQsIGBhbGVydC0ke3RoaXMudHlwZX1gKTtcclxuICAgIH1cclxufVxyXG4iXX0=