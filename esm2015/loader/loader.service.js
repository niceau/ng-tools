/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class NgtLoaderService {
    constructor() {
        this.loaders = [];
        this.isloaderClosed = new Subject();
        this.isloaderOpened = new Subject();
    }
    /**
     * @param {?} loader
     * @return {?}
     */
    add(loader) {
        // add loader to array of active loaders-page
        this.loaders.push(loader);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    remove(id) {
        // remove loader from array of active loaders-page
        /** @type {?} */
        const loaderToRemove = _.find(this.loaders, { id: id });
        this.loaders = _.without(this.loaders, loaderToRemove);
    }
    /**
     * @param {?} data
     * @return {?}
     */
    create(data) {
        /** @type {?} */
        const loader = _.find(this.loaders, { id: data.id });
        if (!loader) {
            console.error('Loader instance with id "' + data.id + '" not defined!');
            return null;
        }
        else {
            loader.content = data.content ? data.content : null;
        }
        return loader;
    }
}
NgtLoaderService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ NgtLoaderService.ngInjectableDef = i0.defineInjectable({ factory: function NgtLoaderService_Factory() { return new NgtLoaderService(); }, token: NgtLoaderService, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtLoaderService.prototype.loaders;
    /** @type {?} */
    NgtLoaderService.prototype.isloaderClosed;
    /** @type {?} */
    NgtLoaderService.prototype.isloaderOpened;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImxvYWRlci9sb2FkZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMvQixPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUc1QixPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQUszQyxNQUFNLE9BQU8sZ0JBQWdCO0lBSDdCO1FBSUksWUFBTyxHQUFxQixFQUFFLENBQUM7UUFDL0IsbUJBQWMsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBQ3hDLG1CQUFjLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztLQXVCM0M7Ozs7O0lBckJHLEdBQUcsQ0FBQyxNQUFpQjtRQUNqQiw2Q0FBNkM7UUFDN0MsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsRUFBVTs7O2NBRVAsY0FBYyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxjQUFjLENBQUMsQ0FBQztJQUMzRCxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxJQUFzQzs7Y0FDbkMsTUFBTSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsRUFBRSxFQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNULE9BQU8sQ0FBQyxLQUFLLENBQUMsMkJBQTJCLEdBQUcsSUFBSSxDQUFDLEVBQUUsR0FBRyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ3hFLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7YUFBTTtZQUNILE1BQU0sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1NBQ3ZEO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7O1lBNUJKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7Ozs7SUFFRyxtQ0FBK0I7O0lBQy9CLDBDQUF3Qzs7SUFDeEMsMENBQXdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcblxyXG5pbXBvcnQgeyBOZ3RMb2FkZXIgfSBmcm9tICcuL2xvYWRlci5tb2RlbCc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0TG9hZGVyU2VydmljZSB7XHJcbiAgICBsb2FkZXJzOiBBcnJheTxOZ3RMb2FkZXI+ID0gW107XHJcbiAgICBpc2xvYWRlckNsb3NlZCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcbiAgICBpc2xvYWRlck9wZW5lZCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcblxyXG4gICAgYWRkKGxvYWRlcjogTmd0TG9hZGVyKSB7XHJcbiAgICAgICAgLy8gYWRkIGxvYWRlciB0byBhcnJheSBvZiBhY3RpdmUgbG9hZGVycy1wYWdlXHJcbiAgICAgICAgdGhpcy5sb2FkZXJzLnB1c2gobG9hZGVyKTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmUoaWQ6IHN0cmluZykge1xyXG4gICAgICAgIC8vIHJlbW92ZSBsb2FkZXIgZnJvbSBhcnJheSBvZiBhY3RpdmUgbG9hZGVycy1wYWdlXHJcbiAgICAgICAgY29uc3QgbG9hZGVyVG9SZW1vdmUgPSBfLmZpbmQodGhpcy5sb2FkZXJzLCB7aWQ6IGlkfSk7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJzID0gXy53aXRob3V0KHRoaXMubG9hZGVycywgbG9hZGVyVG9SZW1vdmUpO1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZShkYXRhOiB7IGlkOiBzdHJpbmcsIGNvbnRlbnQ/OiBzdHJpbmcgfSkge1xyXG4gICAgICAgIGNvbnN0IGxvYWRlciA9IF8uZmluZCh0aGlzLmxvYWRlcnMsIHtpZDogZGF0YS5pZH0pO1xyXG4gICAgICAgIGlmICghbG9hZGVyKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoJ0xvYWRlciBpbnN0YW5jZSB3aXRoIGlkIFwiJyArIGRhdGEuaWQgKyAnXCIgbm90IGRlZmluZWQhJyk7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGxvYWRlci5jb250ZW50ID0gZGF0YS5jb250ZW50ID8gZGF0YS5jb250ZW50IDogbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGxvYWRlcjtcclxuICAgIH1cclxufVxyXG4iXX0=