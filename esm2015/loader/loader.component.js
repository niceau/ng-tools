/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NgtLoaderService } from './loader.service';
export class NgtLoaderComponent {
    /**
     * @param {?} _loaderService
     * @param {?} _cdr
     */
    constructor(_loaderService, _cdr) {
        this._loaderService = _loaderService;
        this._cdr = _cdr;
        this.content = null;
        /**
         *  Spinner type: 'border' | 'grow';
         */
        this.type = 'border';
        this._isPresent = false;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set isPresent(value) {
        this._isPresent = value;
    }
    /**
     * @return {?}
     */
    get isPresent() {
        return this._isPresent;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        // ensure id attribute exists
        if (!this.id) {
            console.error('loader must have an id');
            return;
        }
        // add self (this loader instance) to the loader service so it's accessible from controllers
        this._loaderService.add(this);
    }
    // remove self from loader service when directive is destroyed
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this._loaderService.remove(this.id);
    }
    // open loader
    /**
     * @return {?}
     */
    present() {
        this.isPresent = true;
        this._cdr.detectChanges();
        return this._loaderService.isloaderOpened;
    }
    // close loader
    /**
     * @return {?}
     */
    dismiss() {
        this.isPresent = false;
        this._cdr.detectChanges();
        return this._loaderService.isloaderClosed;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    animationDone(event) {
        if (event.toState === 'close') {
            this._loaderService.isloaderClosed.next();
        }
        if (event.toState === 'open') {
            this._loaderService.isloaderOpened.next();
        }
    }
}
NgtLoaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-loader-component',
                template: `
        <div
            class="loader"
            [ngClass]="{'-single': !content}"
            [@loading]='isPresent ? "open" : "close"'
            (@loading.done)="animationDone($event)">
            <div class="loader_overlay"></div>
            <div class="loader_wrapper">
                <div class="loader_spinner-wrap">
                    <div class="spinner-{{ type }} {{ spinnerClass }}" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <p *ngIf="content">{{ content }}</p>
            </div>
        </div>

    `,
                animations: [
                    trigger('loading', [
                        state('open', style({ opacity: 1, visibility: 'visible' })),
                        state('close', style({ opacity: 0, visibility: 'hidden' })),
                        transition('close => open', animate('0.3s linear')),
                        transition('open => close', animate('0.2s linear'))
                    ])
                ]
            }] }
];
/** @nocollapse */
NgtLoaderComponent.ctorParameters = () => [
    { type: NgtLoaderService },
    { type: ChangeDetectorRef }
];
NgtLoaderComponent.propDecorators = {
    id: [{ type: Input }],
    type: [{ type: Input }],
    spinnerClass: [{ type: Input }],
    isPresent: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NgtLoaderComponent.prototype.id;
    /** @type {?} */
    NgtLoaderComponent.prototype.content;
    /**
     *  Spinner type: 'border' | 'grow';
     * @type {?}
     */
    NgtLoaderComponent.prototype.type;
    /**
     *  Spinner extra class;
     * @type {?}
     */
    NgtLoaderComponent.prototype.spinnerClass;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderComponent.prototype._isPresent;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderComponent.prototype._loaderService;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderComponent.prototype._cdr;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibG9hZGVyL2xvYWRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFxQixpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN2RixPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBR2pGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBZ0NwRCxNQUFNLE9BQU8sa0JBQWtCOzs7OztJQXlCM0IsWUFBb0IsY0FBZ0MsRUFBVSxJQUF1QjtRQUFqRSxtQkFBYyxHQUFkLGNBQWMsQ0FBa0I7UUFBVSxTQUFJLEdBQUosSUFBSSxDQUFtQjtRQXZCckYsWUFBTyxHQUFXLElBQUksQ0FBQzs7OztRQUtkLFNBQUksR0FBRyxRQUFRLENBQUM7UUFPakIsZUFBVSxHQUFHLEtBQUssQ0FBQztJQVkzQixDQUFDOzs7OztJQVZELElBQ0ksU0FBUyxDQUFDLEtBQWM7UUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7SUFDNUIsQ0FBQzs7OztJQUVELElBQUksU0FBUztRQUNULE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQztJQUMzQixDQUFDOzs7O0lBS0QsUUFBUTtRQUNKLDZCQUE2QjtRQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRTtZQUNWLE9BQU8sQ0FBQyxLQUFLLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUN4QyxPQUFPO1NBQ1Y7UUFFRCw0RkFBNEY7UUFDNUYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7Ozs7SUFHRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7Ozs7O0lBR0QsT0FBTztRQUNILElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDMUIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQztJQUM5QyxDQUFDOzs7OztJQUdELE9BQU87UUFDSCxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQzFCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUM7SUFDOUMsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsS0FBSztRQUNmLElBQUksS0FBSyxDQUFDLE9BQU8sS0FBSyxPQUFPLEVBQUU7WUFDM0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDN0M7UUFDRCxJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssTUFBTSxFQUFFO1lBQzFCLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQzdDO0lBQ0wsQ0FBQzs7O1lBL0ZKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsc0JBQXNCO2dCQUNoQyxRQUFRLEVBQUU7Ozs7Ozs7Ozs7Ozs7Ozs7O0tBaUJUO2dCQUNELFVBQVUsRUFBRTtvQkFDUixPQUFPLENBQUMsU0FBUyxFQUFFO3dCQUNmLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFDLENBQUMsQ0FBQzt3QkFDekQsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsRUFBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUMsQ0FBQyxDQUFDO3dCQUN6RCxVQUFVLENBQUMsZUFBZSxFQUFFLE9BQU8sQ0FBQyxhQUFhLENBQUMsQ0FBQzt3QkFDbkQsVUFBVSxDQUFDLGVBQWUsRUFBRSxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7cUJBQ3RELENBQUM7aUJBQ0w7YUFDSjs7OztZQTlCUSxnQkFBZ0I7WUFKcUIsaUJBQWlCOzs7aUJBcUMxRCxLQUFLO21CQU1MLEtBQUs7MkJBS0wsS0FBSzt3QkFJTCxLQUFLOzs7O0lBZk4sZ0NBQW9COztJQUNwQixxQ0FBdUI7Ozs7O0lBS3ZCLGtDQUF5Qjs7Ozs7SUFLekIsMENBQThCOzs7OztJQUU5Qix3Q0FBMkI7Ozs7O0lBV2YsNENBQXdDOzs7OztJQUFFLGtDQUErQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgT25EZXN0cm95LCBDaGFuZ2VEZXRlY3RvclJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBhbmltYXRlLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIHRyaWdnZXIgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgTmd0TG9hZGVyU2VydmljZSB9IGZyb20gJy4vbG9hZGVyLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC1sb2FkZXItY29tcG9uZW50JyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICBjbGFzcz1cImxvYWRlclwiXHJcbiAgICAgICAgICAgIFtuZ0NsYXNzXT1cInsnLXNpbmdsZSc6ICFjb250ZW50fVwiXHJcbiAgICAgICAgICAgIFtAbG9hZGluZ109J2lzUHJlc2VudCA/IFwib3BlblwiIDogXCJjbG9zZVwiJ1xyXG4gICAgICAgICAgICAoQGxvYWRpbmcuZG9uZSk9XCJhbmltYXRpb25Eb25lKCRldmVudClcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxvYWRlcl9vdmVybGF5XCI+PC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJsb2FkZXJfd3JhcHBlclwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImxvYWRlcl9zcGlubmVyLXdyYXBcIj5cclxuICAgICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwic3Bpbm5lci17eyB0eXBlIH19IHt7IHNwaW5uZXJDbGFzcyB9fVwiIHJvbGU9XCJzdGF0dXNcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJzci1vbmx5XCI+TG9hZGluZy4uLjwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgICAgICAgICAgPHAgKm5nSWY9XCJjb250ZW50XCI+e3sgY29udGVudCB9fTwvcD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgYCxcclxuICAgIGFuaW1hdGlvbnM6IFtcclxuICAgICAgICB0cmlnZ2VyKCdsb2FkaW5nJywgW1xyXG4gICAgICAgICAgICBzdGF0ZSgnb3BlbicsIHN0eWxlKHtvcGFjaXR5OiAxLCB2aXNpYmlsaXR5OiAndmlzaWJsZSd9KSksXHJcbiAgICAgICAgICAgIHN0YXRlKCdjbG9zZScsIHN0eWxlKHtvcGFjaXR5OiAwLCB2aXNpYmlsaXR5OiAnaGlkZGVuJ30pKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbignY2xvc2UgPT4gb3BlbicsIGFuaW1hdGUoJzAuM3MgbGluZWFyJykpLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCdvcGVuID0+IGNsb3NlJywgYW5pbWF0ZSgnMC4ycyBsaW5lYXInKSlcclxuICAgICAgICBdKVxyXG4gICAgXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIE5ndExvYWRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIEBJbnB1dCgpIGlkOiBzdHJpbmc7XHJcbiAgICBjb250ZW50OiBzdHJpbmcgPSBudWxsO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogIFNwaW5uZXIgdHlwZTogJ2JvcmRlcicgfCAnZ3Jvdyc7XHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHR5cGUgPSAnYm9yZGVyJztcclxuXHJcbiAgICAvKipcclxuICAgICAqICBTcGlubmVyIGV4dHJhIGNsYXNzO1xyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBzcGlubmVyQ2xhc3M6IHN0cmluZztcclxuXHJcbiAgICBwcml2YXRlIF9pc1ByZXNlbnQgPSBmYWxzZTtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgc2V0IGlzUHJlc2VudCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgICAgIHRoaXMuX2lzUHJlc2VudCA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpc1ByZXNlbnQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzUHJlc2VudDtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9sb2FkZXJTZXJ2aWNlOiBOZ3RMb2FkZXJTZXJ2aWNlLCBwcml2YXRlIF9jZHI6IENoYW5nZURldGVjdG9yUmVmKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgLy8gZW5zdXJlIGlkIGF0dHJpYnV0ZSBleGlzdHNcclxuICAgICAgICBpZiAoIXRoaXMuaWQpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcignbG9hZGVyIG11c3QgaGF2ZSBhbiBpZCcpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBhZGQgc2VsZiAodGhpcyBsb2FkZXIgaW5zdGFuY2UpIHRvIHRoZSBsb2FkZXIgc2VydmljZSBzbyBpdCdzIGFjY2Vzc2libGUgZnJvbSBjb250cm9sbGVyc1xyXG4gICAgICAgIHRoaXMuX2xvYWRlclNlcnZpY2UuYWRkKHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHJlbW92ZSBzZWxmIGZyb20gbG9hZGVyIHNlcnZpY2Ugd2hlbiBkaXJlY3RpdmUgaXMgZGVzdHJveWVkXHJcbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9sb2FkZXJTZXJ2aWNlLnJlbW92ZSh0aGlzLmlkKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBvcGVuIGxvYWRlclxyXG4gICAgcHJlc2VudCgpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHwgUHJvbWlzZTxib29sZWFuPiB7XHJcbiAgICAgICAgdGhpcy5pc1ByZXNlbnQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuX2Nkci5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2xvYWRlclNlcnZpY2UuaXNsb2FkZXJPcGVuZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gY2xvc2UgbG9hZGVyXHJcbiAgICBkaXNtaXNzKCk6IE9ic2VydmFibGU8Ym9vbGVhbj4gfCBQcm9taXNlPGJvb2xlYW4+IHtcclxuICAgICAgICB0aGlzLmlzUHJlc2VudCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuX2Nkci5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2xvYWRlclNlcnZpY2UuaXNsb2FkZXJDbG9zZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgYW5pbWF0aW9uRG9uZShldmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC50b1N0YXRlID09PSAnY2xvc2UnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2xvYWRlclNlcnZpY2UuaXNsb2FkZXJDbG9zZWQubmV4dCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZXZlbnQudG9TdGF0ZSA9PT0gJ29wZW4nKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2xvYWRlclNlcnZpY2UuaXNsb2FkZXJPcGVuZWQubmV4dCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=