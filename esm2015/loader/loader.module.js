/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtLoaderComponent } from './loader.component';
import { NgtLoaderDirective } from './loader.directive';
export { NgtLoaderService } from './loader.service';
export { NgtLoaderComponent } from './loader.component';
export { NgtLoaderDirective } from './loader.directive';
export { NgtLoader } from './loader.model';
/** @type {?} */
const NGC_LOADER_DIRECTIVES = [
    NgtLoaderComponent, NgtLoaderDirective
];
export class NgtLoaderModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtLoaderModule };
    }
}
NgtLoaderModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NGC_LOADER_DIRECTIVES],
                exports: [NGC_LOADER_DIRECTIVES],
                imports: [CommonModule],
                entryComponents: [NgtLoaderComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibG9hZGVyL2xvYWRlci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUcvQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUV4RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7O01BRXJDLHFCQUFxQixHQUFHO0lBQzFCLGtCQUFrQixFQUFFLGtCQUFrQjtDQUN6QztBQVFELE1BQU0sT0FBTyxlQUFlOzs7O0lBQ3hCLE1BQU0sQ0FBQyxPQUFPO1FBQ1YsT0FBTyxFQUFDLFFBQVEsRUFBRSxlQUFlLEVBQUMsQ0FBQztJQUN2QyxDQUFDOzs7WUFUSixRQUFRLFNBQUM7Z0JBQ04sWUFBWSxFQUFFLENBQUMscUJBQXFCLENBQUM7Z0JBQ3JDLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDO2dCQUNoQyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7Z0JBQ3ZCLGVBQWUsRUFBRSxDQUFDLGtCQUFrQixDQUFDO2FBQ3hDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbmltcG9ydCB7IE5ndExvYWRlclNlcnZpY2UgfSBmcm9tICcuL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTmd0TG9hZGVyQ29tcG9uZW50IH0gZnJvbSAnLi9sb2FkZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTmd0TG9hZGVyRGlyZWN0aXZlIH0gZnJvbSAnLi9sb2FkZXIuZGlyZWN0aXZlJztcclxuXHJcbmV4cG9ydCB7IE5ndExvYWRlclNlcnZpY2UgfSBmcm9tICcuL2xvYWRlci5zZXJ2aWNlJztcclxuZXhwb3J0IHsgTmd0TG9hZGVyQ29tcG9uZW50IH0gZnJvbSAnLi9sb2FkZXIuY29tcG9uZW50JztcclxuZXhwb3J0IHsgTmd0TG9hZGVyRGlyZWN0aXZlIH0gZnJvbSAnLi9sb2FkZXIuZGlyZWN0aXZlJztcclxuZXhwb3J0IHsgTmd0TG9hZGVyIH0gZnJvbSAnLi9sb2FkZXIubW9kZWwnO1xyXG5cclxuY29uc3QgTkdDX0xPQURFUl9ESVJFQ1RJVkVTID0gW1xyXG4gICAgTmd0TG9hZGVyQ29tcG9uZW50LCBOZ3RMb2FkZXJEaXJlY3RpdmVcclxuXTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtOR0NfTE9BREVSX0RJUkVDVElWRVNdLFxyXG4gICAgZXhwb3J0czogW05HQ19MT0FERVJfRElSRUNUSVZFU10sXHJcbiAgICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlXSxcclxuICAgIGVudHJ5Q29tcG9uZW50czogW05ndExvYWRlckNvbXBvbmVudF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndExvYWRlck1vZHVsZSB7XHJcbiAgICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge25nTW9kdWxlOiBOZ3RMb2FkZXJNb2R1bGV9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==