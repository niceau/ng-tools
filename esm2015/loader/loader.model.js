/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class NgtLoader {
    /**
     * @param {?} id
     * @param {?} content
     */
    constructor(id, content) {
        this.id = id;
        this.content = content;
    }
}
if (false) {
    /** @type {?} */
    NgtLoader.prototype.id;
    /** @type {?} */
    NgtLoader.prototype.content;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJsb2FkZXIvbG9hZGVyLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxNQUFNLE9BQU8sU0FBUzs7Ozs7SUFDbEIsWUFBbUIsRUFBVSxFQUFTLE9BQWU7UUFBbEMsT0FBRSxHQUFGLEVBQUUsQ0FBUTtRQUFTLFlBQU8sR0FBUCxPQUFPLENBQVE7SUFDckQsQ0FBQztDQUNKOzs7SUFGZSx1QkFBaUI7O0lBQUUsNEJBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIE5ndExvYWRlciB7XHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgaWQ6IHN0cmluZywgcHVibGljIGNvbnRlbnQ6IHN0cmluZykge1xyXG4gICAgfVxyXG59XHJcbiJdfQ==