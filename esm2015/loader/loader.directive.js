/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ComponentFactoryResolver, Directive, ElementRef, Input, ViewContainerRef } from '@angular/core';
import { NgtLoaderComponent } from './loader.component';
/**
 * Loader instance
 */
export class NgtLoaderDirective {
    /**
     * @param {?} resolver
     * @param {?} viewContainerRef
     * @param {?} elRef
     */
    constructor(resolver, viewContainerRef, elRef) {
        this.resolver = resolver;
        this.viewContainerRef = viewContainerRef;
        this.elRef = elRef;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.id) {
            console.error('loader must have an id');
            return;
        }
        /** @type {?} */
        const factory = this.resolver.resolveComponentFactory(NgtLoaderComponent);
        this.viewContainerRef.clear();
        this.componentRef = this.viewContainerRef.createComponent(factory);
        this.componentRef.instance.id = this.id;
        this.elRef.nativeElement.appendChild(this.componentRef.location.nativeElement);
    }
}
NgtLoaderDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtLoader]'
            },] }
];
/** @nocollapse */
NgtLoaderDirective.ctorParameters = () => [
    { type: ComponentFactoryResolver },
    { type: ViewContainerRef },
    { type: ElementRef }
];
NgtLoaderDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtLoader',] }]
};
if (false) {
    /** @type {?} */
    NgtLoaderDirective.prototype.id;
    /** @type {?} */
    NgtLoaderDirective.prototype.container;
    /** @type {?} */
    NgtLoaderDirective.prototype.componentRef;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderDirective.prototype.resolver;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderDirective.prototype.viewContainerRef;
    /**
     * @type {?}
     * @private
     */
    NgtLoaderDirective.prototype.elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibG9hZGVyL2xvYWRlci5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFFSCx3QkFBd0IsRUFFeEIsU0FBUyxFQUNULFVBQVUsRUFDVixLQUFLLEVBRUwsZ0JBQWdCLEVBQ25CLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9CQUFvQixDQUFDOzs7O0FBU3hELE1BQU0sT0FBTyxrQkFBa0I7Ozs7OztJQUszQixZQUFvQixRQUFrQyxFQUNsQyxnQkFBa0MsRUFDbEMsS0FBaUI7UUFGakIsYUFBUSxHQUFSLFFBQVEsQ0FBMEI7UUFDbEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxVQUFLLEdBQUwsS0FBSyxDQUFZO0lBQ3JDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUU7WUFDVixPQUFPLENBQUMsS0FBSyxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDeEMsT0FBTztTQUNWOztjQUVLLE9BQU8sR0FBZ0MsSUFBSSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxrQkFBa0IsQ0FBQztRQUN0RyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFLENBQUM7UUFFOUIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ25FLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBRXhDLElBQUksQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNuRixDQUFDOzs7WUExQkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxhQUFhO2FBQzFCOzs7O1lBaEJHLHdCQUF3QjtZQU14QixnQkFBZ0I7WUFIaEIsVUFBVTs7O2lCQWVULEtBQUssU0FBQyxXQUFXOzs7O0lBQWxCLGdDQUErQjs7SUFDL0IsdUNBQTRCOztJQUM1QiwwQ0FBc0M7Ozs7O0lBRTFCLHNDQUEwQzs7Ozs7SUFDMUMsOENBQTBDOzs7OztJQUMxQyxtQ0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgQ29tcG9uZW50RmFjdG9yeSxcclxuICAgIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcclxuICAgIENvbXBvbmVudFJlZixcclxuICAgIERpcmVjdGl2ZSxcclxuICAgIEVsZW1lbnRSZWYsXHJcbiAgICBJbnB1dCxcclxuICAgIE9uSW5pdCxcclxuICAgIFZpZXdDb250YWluZXJSZWZcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmd0TG9hZGVyQ29tcG9uZW50IH0gZnJvbSAnLi9sb2FkZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTmd0TG9hZGVyIH0gZnJvbSAnLi9sb2FkZXIubW9kZWwnO1xyXG5cclxuLyoqXHJcbiAqIExvYWRlciBpbnN0YW5jZVxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3RMb2FkZXJdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0TG9hZGVyRGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgnbmd0TG9hZGVyJykgaWQ6IHN0cmluZztcclxuICAgIGNvbnRhaW5lcjogVmlld0NvbnRhaW5lclJlZjtcclxuICAgIGNvbXBvbmVudFJlZjogQ29tcG9uZW50UmVmPE5ndExvYWRlcj47XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByZXNvbHZlcjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSB2aWV3Q29udGFpbmVyUmVmOiBWaWV3Q29udGFpbmVyUmVmLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZikge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICghdGhpcy5pZCkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKCdsb2FkZXIgbXVzdCBoYXZlIGFuIGlkJyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGZhY3Rvcnk6IENvbXBvbmVudEZhY3Rvcnk8Tmd0TG9hZGVyPiA9IHRoaXMucmVzb2x2ZXIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkoTmd0TG9hZGVyQ29tcG9uZW50KTtcclxuICAgICAgICB0aGlzLnZpZXdDb250YWluZXJSZWYuY2xlYXIoKTtcclxuXHJcbiAgICAgICAgdGhpcy5jb21wb25lbnRSZWYgPSB0aGlzLnZpZXdDb250YWluZXJSZWYuY3JlYXRlQ29tcG9uZW50KGZhY3RvcnkpO1xyXG4gICAgICAgIHRoaXMuY29tcG9uZW50UmVmLmluc3RhbmNlLmlkID0gdGhpcy5pZDtcclxuXHJcbiAgICAgICAgdGhpcy5lbFJlZi5uYXRpdmVFbGVtZW50LmFwcGVuZENoaWxkKHRoaXMuY29tcG9uZW50UmVmLmxvY2F0aW9uLm5hdGl2ZUVsZW1lbnQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==