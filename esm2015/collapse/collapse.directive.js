/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, HostBinding, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NgtCollapseService } from './collapse.service';
import { NgtCollapseConfig } from './collapse-config';
export class NgtCollapseDirective {
    /**
     * @param {?} config
     * @param {?} elRef
     * @param {?} $collapseService
     */
    constructor(config, elRef, $collapseService) {
        this.elRef = elRef;
        this.$collapseService = $collapseService;
        /**
         * A flag indicating show/hide with animation.
         */
        this.animated = true;
        /**
         * Speed of animation. No effect if [animated]=false.
         */
        this.animationSpeed = 300;
        /**
         * Animation function. No effect if [animated]=false.
         */
        this.animationFunction = '';
        /**
         * A flag indicating default opened status when component is load.
         */
        this.opened = false;
        this.show = this.status;
        this.animated = config.animated;
        this.animationSpeed = config.animationSpeed;
        this.animationFunction = config.animationFunction;
        this.status = this.opened;
        this.$collapseService.isCollapseTriggered.subscribe((/**
         * @param {?} id
         * @return {?}
         */
        id => {
            if (id === this.id) {
                this.toggle();
            }
        }));
    }
    /**
     * @return {?}
     */
    get status() {
        return this._status;
    }
    /**
     * @param {?} status
     * @return {?}
     */
    set status(status) {
        this._status = status;
        this.show = status;
    }
    /**
     * @return {?}
     */
    get triggerShow() {
        return {
            value: this.animated ? (this.status ? 'down' : 'up') : (this.status ? 'show' : 'hide'),
            params: {
                animationSpeed: this.animationSpeed,
                animationFunction: this.animationFunction ? ' ' + this.animationFunction : ''
            }
        };
    }
    /**
     * @return {?}
     */
    toggle() {
        this.status = !this.status;
    }
}
NgtCollapseDirective.decorators = [
    { type: Component, args: [{
                selector: '[ngtCollapse]',
                exportAs: 'ngtCollapse',
                animations: [
                    trigger('show', [
                        state('down', style({ overflow: 'hidden', height: '*', paddingTop: '*', paddingBottom: '*' })),
                        state('up', style({ overflow: 'hidden', height: 0, paddingTop: 0, paddingBottom: 0 })),
                        state('show', style({ overflow: 'hidden', height: '*', paddingTop: '*', paddingBottom: '*' })),
                        state('hide', style({ overflow: 'hidden', height: 0, paddingTop: 0, paddingBottom: 0 })),
                        transition('up => down', animate('{{animationSpeed}}ms{{animationFunction}}')),
                        transition('down => up', animate('{{animationSpeed}}ms{{animationFunction}}')),
                        transition('hide => show', animate('0ms')),
                        transition('show => hide', animate('0ms'))
                    ]),
                ],
                template: `
        <ng-content></ng-content>`
            }] }
];
/** @nocollapse */
NgtCollapseDirective.ctorParameters = () => [
    { type: NgtCollapseConfig },
    { type: ElementRef },
    { type: NgtCollapseService }
];
NgtCollapseDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtCollapse',] }],
    animated: [{ type: Input }],
    animationSpeed: [{ type: Input }],
    animationFunction: [{ type: Input }],
    opened: [{ type: Input }],
    triggerShow: [{ type: HostBinding, args: ['@show',] }],
    show: [{ type: HostBinding, args: ['class.show',] }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtCollapseDirective.prototype._status;
    /**
     * A id of element for collapsing.
     * @type {?}
     */
    NgtCollapseDirective.prototype.id;
    /**
     * A flag indicating show/hide with animation.
     * @type {?}
     */
    NgtCollapseDirective.prototype.animated;
    /**
     * Speed of animation. No effect if [animated]=false.
     * @type {?}
     */
    NgtCollapseDirective.prototype.animationSpeed;
    /**
     * Animation function. No effect if [animated]=false.
     * @type {?}
     */
    NgtCollapseDirective.prototype.animationFunction;
    /**
     * A flag indicating default opened status when component is load.
     * @type {?}
     */
    NgtCollapseDirective.prototype.opened;
    /** @type {?} */
    NgtCollapseDirective.prototype.show;
    /**
     * @type {?}
     * @private
     */
    NgtCollapseDirective.prototype.elRef;
    /**
     * @type {?}
     * @private
     */
    NgtCollapseDirective.prototype.$collapseService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2UuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJjb2xsYXBzZS9jb2xsYXBzZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDMUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVqRixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQW9CdEQsTUFBTSxPQUFPLG9CQUFvQjs7Ozs7O0lBZ0Q3QixZQUFZLE1BQXlCLEVBQ2pCLEtBQWlCLEVBQ2pCLGdCQUFvQztRQURwQyxVQUFLLEdBQUwsS0FBSyxDQUFZO1FBQ2pCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBb0I7Ozs7UUEvQi9DLGFBQVEsR0FBRyxJQUFJLENBQUM7Ozs7UUFLaEIsbUJBQWMsR0FBRyxHQUFHLENBQUM7Ozs7UUFLckIsc0JBQWlCLEdBQUcsRUFBRSxDQUFDOzs7O1FBS3ZCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFZRyxTQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUsxQyxJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUM7UUFDaEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLENBQUMsY0FBYyxDQUFDO1FBQzVDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFNLENBQUMsaUJBQWlCLENBQUM7UUFDbEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQzFCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTOzs7O1FBQUMsRUFBRSxDQUFDLEVBQUU7WUFDckQsSUFBSSxFQUFFLEtBQUssSUFBSSxDQUFDLEVBQUUsRUFBRTtnQkFDaEIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQ2pCO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBMURELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDOzs7OztJQUVELElBQUksTUFBTSxDQUFDLE1BQWU7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDdEIsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7SUFDdkIsQ0FBQzs7OztJQTJCRCxJQUNJLFdBQVc7UUFDWCxPQUFPO1lBQ0gsS0FBSyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUN0RixNQUFNLEVBQUU7Z0JBQ0osY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjO2dCQUNuQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLEVBQUU7YUFDaEY7U0FDSixDQUFDO0lBQ04sQ0FBQzs7OztJQWlCRCxNQUFNO1FBQ0YsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDL0IsQ0FBQzs7O1lBbEZKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsUUFBUSxFQUFFLGFBQWE7Z0JBQ3ZCLFVBQVUsRUFBRTtvQkFDUixPQUFPLENBQUMsTUFBTSxFQUFFO3dCQUNaLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLEdBQUcsRUFBQyxDQUFDLENBQUM7d0JBQzVGLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsYUFBYSxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7d0JBQ3BGLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLEdBQUcsRUFBQyxDQUFDLENBQUM7d0JBQzVGLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLEVBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxNQUFNLEVBQUUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxDQUFDLEVBQUUsYUFBYSxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7d0JBQ3RGLFVBQVUsQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDLDJDQUEyQyxDQUFDLENBQUM7d0JBQzlFLFVBQVUsQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDLDJDQUEyQyxDQUFDLENBQUM7d0JBQzlFLFVBQVUsQ0FBQyxjQUFjLEVBQUUsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUMxQyxVQUFVLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDN0MsQ0FBQztpQkFDTDtnQkFDRCxRQUFRLEVBQUU7a0NBQ29CO2FBQ2pDOzs7O1lBbkJRLGlCQUFpQjtZQUpOLFVBQVU7WUFHckIsa0JBQWtCOzs7aUJBbUN0QixLQUFLLFNBQUMsYUFBYTt1QkFLbkIsS0FBSzs2QkFLTCxLQUFLO2dDQUtMLEtBQUs7cUJBS0wsS0FBSzswQkFFTCxXQUFXLFNBQUMsT0FBTzttQkFVbkIsV0FBVyxTQUFDLFlBQVk7Ozs7Ozs7SUE3Q3pCLHVDQUF5Qjs7Ozs7SUFhekIsa0NBQWlDOzs7OztJQUtqQyx3Q0FBeUI7Ozs7O0lBS3pCLDhDQUE4Qjs7Ozs7SUFLOUIsaURBQWdDOzs7OztJQUtoQyxzQ0FBd0I7O0lBWXhCLG9DQUE4Qzs7Ozs7SUFHbEMscUNBQXlCOzs7OztJQUN6QixnREFBNEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEhvc3RCaW5kaW5nLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBhbmltYXRlLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIHRyaWdnZXIgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuXHJcbmltcG9ydCB7IE5ndENvbGxhcHNlU2VydmljZSB9IGZyb20gJy4vY29sbGFwc2Uuc2VydmljZSc7XHJcbmltcG9ydCB7IE5ndENvbGxhcHNlQ29uZmlnIH0gZnJvbSAnLi9jb2xsYXBzZS1jb25maWcnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3RDb2xsYXBzZV0nLFxyXG4gICAgZXhwb3J0QXM6ICduZ3RDb2xsYXBzZScsXHJcbiAgICBhbmltYXRpb25zOiBbXHJcbiAgICAgICAgdHJpZ2dlcignc2hvdycsIFtcclxuICAgICAgICAgICAgc3RhdGUoJ2Rvd24nLCBzdHlsZSh7b3ZlcmZsb3c6ICdoaWRkZW4nLCBoZWlnaHQ6ICcqJywgcGFkZGluZ1RvcDogJyonLCBwYWRkaW5nQm90dG9tOiAnKid9KSksXHJcbiAgICAgICAgICAgIHN0YXRlKCd1cCcsIHN0eWxlKHtvdmVyZmxvdzogJ2hpZGRlbicsIGhlaWdodDogMCwgcGFkZGluZ1RvcDogMCwgcGFkZGluZ0JvdHRvbTogMH0pKSxcclxuICAgICAgICAgICAgc3RhdGUoJ3Nob3cnLCBzdHlsZSh7b3ZlcmZsb3c6ICdoaWRkZW4nLCBoZWlnaHQ6ICcqJywgcGFkZGluZ1RvcDogJyonLCBwYWRkaW5nQm90dG9tOiAnKid9KSksXHJcbiAgICAgICAgICAgIHN0YXRlKCdoaWRlJywgc3R5bGUoe292ZXJmbG93OiAnaGlkZGVuJywgaGVpZ2h0OiAwLCBwYWRkaW5nVG9wOiAwLCBwYWRkaW5nQm90dG9tOiAwfSkpLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCd1cCA9PiBkb3duJywgYW5pbWF0ZSgne3thbmltYXRpb25TcGVlZH19bXN7e2FuaW1hdGlvbkZ1bmN0aW9ufX0nKSksXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb24oJ2Rvd24gPT4gdXAnLCBhbmltYXRlKCd7e2FuaW1hdGlvblNwZWVkfX1tc3t7YW5pbWF0aW9uRnVuY3Rpb259fScpKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbignaGlkZSA9PiBzaG93JywgYW5pbWF0ZSgnMG1zJykpLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCdzaG93ID0+IGhpZGUnLCBhbmltYXRlKCcwbXMnKSlcclxuICAgICAgICBdKSxcclxuICAgIF0sXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxuZy1jb250ZW50PjwvbmctY29udGVudD5gLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0Q29sbGFwc2VEaXJlY3RpdmUge1xyXG4gICAgcHJpdmF0ZSBfc3RhdHVzOiBib29sZWFuO1xyXG4gICAgZ2V0IHN0YXR1cygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc3RhdHVzO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBzdGF0dXMoc3RhdHVzOiBib29sZWFuKSB7XHJcbiAgICAgICAgdGhpcy5fc3RhdHVzID0gc3RhdHVzO1xyXG4gICAgICAgIHRoaXMuc2hvdyA9IHN0YXR1cztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEEgaWQgb2YgZWxlbWVudCBmb3IgY29sbGFwc2luZy5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCduZ3RDb2xsYXBzZScpIGlkOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBIGZsYWcgaW5kaWNhdGluZyBzaG93L2hpZGUgd2l0aCBhbmltYXRpb24uXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGFuaW1hdGVkID0gdHJ1ZTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFNwZWVkIG9mIGFuaW1hdGlvbi4gTm8gZWZmZWN0IGlmIFthbmltYXRlZF09ZmFsc2UuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGFuaW1hdGlvblNwZWVkID0gMzAwO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQW5pbWF0aW9uIGZ1bmN0aW9uLiBObyBlZmZlY3QgaWYgW2FuaW1hdGVkXT1mYWxzZS5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgYW5pbWF0aW9uRnVuY3Rpb24gPSAnJztcclxuXHJcbiAgICAvKipcclxuICAgICAqIEEgZmxhZyBpbmRpY2F0aW5nIGRlZmF1bHQgb3BlbmVkIHN0YXR1cyB3aGVuIGNvbXBvbmVudCBpcyBsb2FkLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBvcGVuZWQgPSBmYWxzZTtcclxuXHJcbiAgICBASG9zdEJpbmRpbmcoJ0BzaG93JylcclxuICAgIGdldCB0cmlnZ2VyU2hvdygpIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICB2YWx1ZTogdGhpcy5hbmltYXRlZCA/ICh0aGlzLnN0YXR1cyA/ICdkb3duJyA6ICd1cCcpIDogKHRoaXMuc3RhdHVzID8gJ3Nob3cnIDogJ2hpZGUnKSxcclxuICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICBhbmltYXRpb25TcGVlZDogdGhpcy5hbmltYXRpb25TcGVlZCxcclxuICAgICAgICAgICAgICAgIGFuaW1hdGlvbkZ1bmN0aW9uOiB0aGlzLmFuaW1hdGlvbkZ1bmN0aW9uID8gJyAnICsgdGhpcy5hbmltYXRpb25GdW5jdGlvbiA6ICcnXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5zaG93Jykgc2hvdyA9IHRoaXMuc3RhdHVzO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbmZpZzogTmd0Q29sbGFwc2VDb25maWcsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGVsUmVmOiBFbGVtZW50UmVmLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSAkY29sbGFwc2VTZXJ2aWNlOiBOZ3RDb2xsYXBzZVNlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLmFuaW1hdGVkID0gY29uZmlnLmFuaW1hdGVkO1xyXG4gICAgICAgIHRoaXMuYW5pbWF0aW9uU3BlZWQgPSBjb25maWcuYW5pbWF0aW9uU3BlZWQ7XHJcbiAgICAgICAgdGhpcy5hbmltYXRpb25GdW5jdGlvbiA9IGNvbmZpZy5hbmltYXRpb25GdW5jdGlvbjtcclxuICAgICAgICB0aGlzLnN0YXR1cyA9IHRoaXMub3BlbmVkO1xyXG4gICAgICAgIHRoaXMuJGNvbGxhcHNlU2VydmljZS5pc0NvbGxhcHNlVHJpZ2dlcmVkLnN1YnNjcmliZShpZCA9PiB7XHJcbiAgICAgICAgICAgIGlmIChpZCA9PT0gdGhpcy5pZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50b2dnbGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZSgpIHtcclxuICAgICAgICB0aGlzLnN0YXR1cyA9ICF0aGlzLnN0YXR1cztcclxuICAgIH1cclxufVxyXG4iXX0=