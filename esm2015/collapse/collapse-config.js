/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtAlert component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the alerts used in the application.
 */
export class NgtCollapseConfig {
    constructor() {
        this.animated = true;
        this.animationSpeed = 300;
        this.animationFunction = '';
    }
}
NgtCollapseConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtCollapseConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtCollapseConfig_Factory() { return new NgtCollapseConfig(); }, token: NgtCollapseConfig, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtCollapseConfig.prototype.animated;
    /** @type {?} */
    NgtCollapseConfig.prototype.animationSpeed;
    /** @type {?} */
    NgtCollapseConfig.prototype.animationFunction;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2UtY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJjb2xsYXBzZS9jb2xsYXBzZS1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFRM0MsTUFBTSxPQUFPLGlCQUFpQjtJQUQ5QjtRQUVJLGFBQVEsR0FBRyxJQUFJLENBQUM7UUFDaEIsbUJBQWMsR0FBRyxHQUFHLENBQUM7UUFDckIsc0JBQWlCLEdBQUcsRUFBRSxDQUFDO0tBQzFCOzs7WUFMQSxVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7OztJQUU1QixxQ0FBZ0I7O0lBQ2hCLDJDQUFxQjs7SUFDckIsOENBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuLyoqXHJcbiAqIENvbmZpZ3VyYXRpb24gc2VydmljZSBmb3IgdGhlIE5ndEFsZXJ0IGNvbXBvbmVudC5cclxuICogWW91IGNhbiBpbmplY3QgdGhpcyBzZXJ2aWNlLCB0eXBpY2FsbHkgaW4geW91ciByb290IGNvbXBvbmVudCwgYW5kIGN1c3RvbWl6ZSB0aGUgdmFsdWVzIG9mIGl0cyBwcm9wZXJ0aWVzIGluXHJcbiAqIG9yZGVyIHRvIHByb3ZpZGUgZGVmYXVsdCB2YWx1ZXMgZm9yIGFsbCB0aGUgYWxlcnRzIHVzZWQgaW4gdGhlIGFwcGxpY2F0aW9uLlxyXG4gKi9cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RDb2xsYXBzZUNvbmZpZyB7XHJcbiAgICBhbmltYXRlZCA9IHRydWU7XHJcbiAgICBhbmltYXRpb25TcGVlZCA9IDMwMDtcclxuICAgIGFuaW1hdGlvbkZ1bmN0aW9uID0gJyc7XHJcbn1cclxuIl19