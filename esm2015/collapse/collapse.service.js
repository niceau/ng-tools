/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Subject } from 'rxjs';
export class NgtCollapseService {
    constructor() {
        this.isCollapseTriggered = new Subject();
    }
    /**
     * @param {?} id
     * @return {?}
     */
    toggle(id) {
        this.isCollapseTriggered.next(id);
    }
}
if (false) {
    /** @type {?} */
    NgtCollapseService.prototype.isCollapseTriggered;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2Uuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiY29sbGFwc2UvY29sbGFwc2Uuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUUvQixNQUFNLE9BQU8sa0JBQWtCO0lBRzNCO1FBRkEsd0JBQW1CLEdBQUcsSUFBSSxPQUFPLEVBQVUsQ0FBQztJQUc1QyxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxFQUFVO1FBQ2IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUN0QyxDQUFDO0NBQ0o7OztJQVJHLGlEQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuXHJcbmV4cG9ydCBjbGFzcyBOZ3RDb2xsYXBzZVNlcnZpY2Uge1xyXG4gICAgaXNDb2xsYXBzZVRyaWdnZXJlZCA9IG5ldyBTdWJqZWN0PHN0cmluZz4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGUoaWQ6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuaXNDb2xsYXBzZVRyaWdnZXJlZC5uZXh0KGlkKTtcclxuICAgIH1cclxufVxyXG4iXX0=