/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtCollapseService } from './collapse.service';
export class NgtCollapseTriggerDirective {
    /**
     * @param {?} $collapseService
     */
    constructor($collapseService) {
        this.$collapseService = $collapseService;
        /**
         * A flag to disable collapsing on click.
         */
        this.disabled = false;
    }
    /**
     * @return {?}
     */
    onClick() {
        event.preventDefault();
        event.stopPropagation();
        this.toggle();
    }
    /**
     * @return {?}
     */
    toggle() {
        if (this.disabled) {
            return;
        }
        if (Array.isArray(this.id)) {
            this.id.forEach((/**
             * @param {?} id
             * @return {?}
             */
            id => {
                this.$collapseService.toggle(id);
            }));
        }
        else {
            this.$collapseService.toggle(this.id);
        }
    }
}
NgtCollapseTriggerDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtCollapseTrigger]'
            },] }
];
/** @nocollapse */
NgtCollapseTriggerDirective.ctorParameters = () => [
    { type: NgtCollapseService }
];
NgtCollapseTriggerDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtCollapseTrigger',] }],
    disabled: [{ type: Input }],
    onClick: [{ type: HostListener, args: ['click',] }]
};
if (false) {
    /**
     * A id of element to collapse.
     * @type {?}
     */
    NgtCollapseTriggerDirective.prototype.id;
    /**
     * A flag to disable collapsing on click.
     * @type {?}
     */
    NgtCollapseTriggerDirective.prototype.disabled;
    /**
     * @type {?}
     * @private
     */
    NgtCollapseTriggerDirective.prototype.$collapseService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2UtdHJpZ2dlci5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImNvbGxhcHNlL2NvbGxhcHNlLXRyaWdnZXIuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0QsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFNeEQsTUFBTSxPQUFPLDJCQUEyQjs7OztJQW1CcEMsWUFBb0IsZ0JBQW9DO1FBQXBDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBb0I7Ozs7UUFWL0MsYUFBUSxHQUFHLEtBQUssQ0FBQztJQVcxQixDQUFDOzs7O0lBUkQsT0FBTztRQUNILEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFeEIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2xCLENBQUM7Ozs7SUFLRCxNQUFNO1FBQ0YsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsT0FBTztTQUNWO1FBRUQsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBRTtZQUN4QixJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU87Ozs7WUFBQyxFQUFFLENBQUMsRUFBRTtnQkFDakIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNyQyxDQUFDLEVBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUN6QztJQUNMLENBQUM7OztZQXJDSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHNCQUFzQjthQUNuQzs7OztZQUxRLGtCQUFrQjs7O2lCQVV0QixLQUFLLFNBQUMsb0JBQW9CO3VCQUsxQixLQUFLO3NCQUVMLFlBQVksU0FBQyxPQUFPOzs7Ozs7O0lBUHJCLHlDQUFxQzs7Ozs7SUFLckMsK0NBQTBCOzs7OztJQVVkLHVEQUE0QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTmd0Q29sbGFwc2VTZXJ2aWNlIH0gZnJvbSAnLi9jb2xsYXBzZS5zZXJ2aWNlJztcclxuXHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndENvbGxhcHNlVHJpZ2dlcl0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RDb2xsYXBzZVRyaWdnZXJEaXJlY3RpdmUge1xyXG4gICAgLyoqXHJcbiAgICAgKiBBIGlkIG9mIGVsZW1lbnQgdG8gY29sbGFwc2UuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgnbmd0Q29sbGFwc2VUcmlnZ2VyJykgaWQ6IGFueTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEEgZmxhZyB0byBkaXNhYmxlIGNvbGxhcHNpbmcgb24gY2xpY2suXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGRpc2FibGVkID0gZmFsc2U7XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snKVxyXG4gICAgb25DbGljaygpIHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cclxuICAgICAgICB0aGlzLnRvZ2dsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgJGNvbGxhcHNlU2VydmljZTogTmd0Q29sbGFwc2VTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgdG9nZ2xlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmRpc2FibGVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChBcnJheS5pc0FycmF5KHRoaXMuaWQpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaWQuZm9yRWFjaChpZCA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLiRjb2xsYXBzZVNlcnZpY2UudG9nZ2xlKGlkKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy4kY29sbGFwc2VTZXJ2aWNlLnRvZ2dsZSh0aGlzLmlkKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19