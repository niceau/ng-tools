/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtCollapseService } from './collapse.service';
import { NgtCollapseDirective } from './collapse.directive';
import { NgtCollapseTriggerDirective } from './collapse-trigger.directive';
export { NgtCollapseService } from './collapse.service';
export { NgtCollapseDirective } from './collapse.directive';
export { NgtCollapseTriggerDirective } from './collapse-trigger.directive';
/** @type {?} */
const NGC_COLLAPSE_DIRECTIVES = [NgtCollapseDirective, NgtCollapseTriggerDirective];
export class NgtCollapseModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtCollapseModule };
    }
}
NgtCollapseModule.decorators = [
    { type: NgModule, args: [{
                declarations: NGC_COLLAPSE_DIRECTIVES,
                exports: NGC_COLLAPSE_DIRECTIVES,
                imports: [CommonModule],
                providers: [NgtCollapseService],
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2UubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJjb2xsYXBzZS9jb2xsYXBzZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUUzRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQzs7TUFFckUsdUJBQXVCLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSwyQkFBMkIsQ0FBQztBQVFuRixNQUFNLE9BQU8saUJBQWlCOzs7O0lBQzFCLE1BQU0sQ0FBQyxPQUFPO1FBQ1YsT0FBTyxFQUFDLFFBQVEsRUFBRSxpQkFBaUIsRUFBQyxDQUFDO0lBQ3pDLENBQUM7OztZQVRKLFFBQVEsU0FBQztnQkFDTixZQUFZLEVBQUUsdUJBQXVCO2dCQUNyQyxPQUFPLEVBQUUsdUJBQXVCO2dCQUNoQyxPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7Z0JBQ3ZCLFNBQVMsRUFBRSxDQUFDLGtCQUFrQixDQUFDO2FBQ2xDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbmltcG9ydCB7IE5ndENvbGxhcHNlU2VydmljZSB9IGZyb20gJy4vY29sbGFwc2Uuc2VydmljZSc7XHJcbmltcG9ydCB7IE5ndENvbGxhcHNlRGlyZWN0aXZlIH0gZnJvbSAnLi9jb2xsYXBzZS5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBOZ3RDb2xsYXBzZVRyaWdnZXJEaXJlY3RpdmUgfSBmcm9tICcuL2NvbGxhcHNlLXRyaWdnZXIuZGlyZWN0aXZlJztcclxuXHJcbmV4cG9ydCB7IE5ndENvbGxhcHNlU2VydmljZSB9IGZyb20gJy4vY29sbGFwc2Uuc2VydmljZSc7XHJcbmV4cG9ydCB7IE5ndENvbGxhcHNlRGlyZWN0aXZlIH0gZnJvbSAnLi9jb2xsYXBzZS5kaXJlY3RpdmUnO1xyXG5leHBvcnQgeyBOZ3RDb2xsYXBzZVRyaWdnZXJEaXJlY3RpdmUgfSBmcm9tICcuL2NvbGxhcHNlLXRyaWdnZXIuZGlyZWN0aXZlJztcclxuXHJcbmNvbnN0IE5HQ19DT0xMQVBTRV9ESVJFQ1RJVkVTID0gW05ndENvbGxhcHNlRGlyZWN0aXZlLCBOZ3RDb2xsYXBzZVRyaWdnZXJEaXJlY3RpdmVdO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogTkdDX0NPTExBUFNFX0RJUkVDVElWRVMsXHJcbiAgICBleHBvcnRzOiBOR0NfQ09MTEFQU0VfRElSRUNUSVZFUyxcclxuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdLFxyXG4gICAgcHJvdmlkZXJzOiBbTmd0Q29sbGFwc2VTZXJ2aWNlXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndENvbGxhcHNlTW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgICAgIHJldHVybiB7bmdNb2R1bGU6IE5ndENvbGxhcHNlTW9kdWxlfTtcclxuICAgIH1cclxufVxyXG4iXX0=