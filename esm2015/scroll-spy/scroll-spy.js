/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, HostListener, Inject, Input, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
export class NgtScrollSpy {
    /**
     * @param {?} _document
     * @param {?} _router
     * @param {?} _elem
     * @param {?} _renderer
     */
    constructor(_document, _router, _elem, _renderer) {
        this._document = _document;
        this._router = _router;
        this._elem = _elem;
        this._renderer = _renderer;
    }
    /**
     * @private
     * @param {?} spTarget
     * @return {?}
     */
    _getTarget(spTarget) {
        if (typeof spTarget === 'string') {
            spTarget = spTarget.split('');
            /** @type {?} */
            const type = spTarget.shift();
            switch (type) {
                case '#':
                    return this._document.getElementById(spTarget.join(''));
                case '.':
                    return this._document.getElementsByClassName(spTarget.join(''))[0];
                default:
                    return this._document.getElementsByTagName(this.ngtScrollSpy)[0];
            }
        }
        else {
            return spTarget;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onWindowScroll(event) {
        /** @type {?} */
        const offset = this.spyOffset ? this.spyOffset : 0;
        /** @type {?} */
        const scrollTarget = this._getTarget(this.ngtScrollSpy);
        /** @type {?} */
        const elemDim = scrollTarget ? scrollTarget.getBoundingClientRect() : null;
        if (!elemDim) {
            console.warn(`There is no element ${this.ngtScrollSpy}`);
        }
        else if (this.spyAnchor && elemDim && elemDim.top < offset && elemDim.bottom > offset) {
            if (this.spyAnchor !== 'none') {
                this._router.navigate([], { fragment: this.spyAnchor });
            }
            else {
                this._router.navigate([]);
            }
            this._renderer.addClass(this._elem.nativeElement, 'active');
        }
        else {
            this._renderer.removeClass(this._elem.nativeElement, 'active');
        }
    }
}
NgtScrollSpy.decorators = [
    { type: Directive, args: [{
                selector: '[ngtScrollSpy]',
            },] }
];
/** @nocollapse */
NgtScrollSpy.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: Router },
    { type: ElementRef },
    { type: Renderer2 }
];
NgtScrollSpy.propDecorators = {
    ngtScrollSpy: [{ type: Input }],
    spyAnchor: [{ type: Input }],
    spyOffset: [{ type: Input }],
    onWindowScroll: [{ type: HostListener, args: ['window:scroll', ['$event'],] }]
};
if (false) {
    /** @type {?} */
    NgtScrollSpy.prototype.ngtScrollSpy;
    /** @type {?} */
    NgtScrollSpy.prototype.spyAnchor;
    /** @type {?} */
    NgtScrollSpy.prototype.spyOffset;
    /**
     * @type {?}
     * @private
     */
    NgtScrollSpy.prototype._document;
    /**
     * @type {?}
     * @private
     */
    NgtScrollSpy.prototype._router;
    /**
     * @type {?}
     * @private
     */
    NgtScrollSpy.prototype._elem;
    /**
     * @type {?}
     * @private
     */
    NgtScrollSpy.prototype._renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Nyb2xsLXNweS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsic2Nyb2xsLXNweS9zY3JvbGwtc3B5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDOUYsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUt6QyxNQUFNLE9BQU8sWUFBWTs7Ozs7OztJQUtyQixZQUM4QixTQUFjLEVBQ2hDLE9BQWUsRUFDZixLQUFpQixFQUNqQixTQUFvQjtRQUhGLGNBQVMsR0FBVCxTQUFTLENBQUs7UUFDaEMsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUNmLFVBQUssR0FBTCxLQUFLLENBQVk7UUFDakIsY0FBUyxHQUFULFNBQVMsQ0FBVztJQUM3QixDQUFDOzs7Ozs7SUFFSSxVQUFVLENBQUMsUUFBUTtRQUN2QixJQUFLLE9BQU8sUUFBUSxLQUFLLFFBQVEsRUFBRTtZQUMvQixRQUFRLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQzs7a0JBQ3hCLElBQUksR0FBRyxRQUFRLENBQUMsS0FBSyxFQUFFO1lBQzdCLFFBQVEsSUFBSSxFQUFFO2dCQUNWLEtBQUssR0FBRztvQkFDSixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDNUQsS0FBSyxHQUFHO29CQUNKLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3ZFO29CQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDeEU7U0FDSjthQUFNO1lBQ0gsT0FBTyxRQUFRLENBQUM7U0FDbkI7SUFDTCxDQUFDOzs7OztJQUdELGNBQWMsQ0FBQyxLQUFZOztjQUNqQixNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7Y0FDNUMsWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQzs7Y0FDakQsT0FBTyxHQUFHLFlBQVksQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLHFCQUFxQixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUk7UUFDMUUsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNWLE9BQU8sQ0FBQyxJQUFJLENBQUMsdUJBQXVCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQyxDQUFDO1NBQzVEO2FBQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsR0FBRyxHQUFHLE1BQU0sSUFBSSxPQUFPLENBQUMsTUFBTSxHQUFHLE1BQU0sRUFBRTtZQUNyRixJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssTUFBTSxFQUFFO2dCQUMzQixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUM7YUFDM0Q7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDN0I7WUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUMvRDthQUFNO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FDbEU7SUFDTCxDQUFDOzs7WUFsREosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxnQkFBZ0I7YUFDN0I7Ozs7NENBT1EsTUFBTSxTQUFDLFFBQVE7WUFYZixNQUFNO1lBRkssVUFBVTtZQUErQixTQUFTOzs7MkJBUWpFLEtBQUs7d0JBQ0wsS0FBSzt3QkFDTCxLQUFLOzZCQTBCTCxZQUFZLFNBQUMsZUFBZSxFQUFFLENBQUMsUUFBUSxDQUFDOzs7O0lBNUJ6QyxvQ0FBMkI7O0lBQzNCLGlDQUE0Qjs7SUFDNUIsaUNBQTRCOzs7OztJQUd4QixpQ0FBd0M7Ozs7O0lBQ3hDLCtCQUF1Qjs7Ozs7SUFDdkIsNkJBQXlCOzs7OztJQUN6QixpQ0FBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIEhvc3RMaXN0ZW5lciwgSW5qZWN0LCBJbnB1dCwgUmVuZGVyZXIyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERPQ1VNRU5UIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbbmd0U2Nyb2xsU3B5XScsXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RTY3JvbGxTcHkge1xyXG4gICAgQElucHV0KCkgbmd0U2Nyb2xsU3B5OiBhbnk7XHJcbiAgICBASW5wdXQoKSBzcHlBbmNob3I/OiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKSBzcHlPZmZzZXQ/OiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgQEluamVjdChET0NVTUVOVCkgcHJpdmF0ZSBfZG9jdW1lbnQ6IGFueSxcclxuICAgICAgICBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICBwcml2YXRlIF9lbGVtOiBFbGVtZW50UmVmLFxyXG4gICAgICAgIHByaXZhdGUgX3JlbmRlcmVyOiBSZW5kZXJlcjJcclxuICAgICkge31cclxuXHJcbiAgICBwcml2YXRlIF9nZXRUYXJnZXQoc3BUYXJnZXQpOiBhbnkge1xyXG4gICAgICAgIGlmICggdHlwZW9mIHNwVGFyZ2V0ID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBzcFRhcmdldCA9IHNwVGFyZ2V0LnNwbGl0KCcnKTtcclxuICAgICAgICAgICAgY29uc3QgdHlwZSA9IHNwVGFyZ2V0LnNoaWZ0KCk7XHJcbiAgICAgICAgICAgIHN3aXRjaCAodHlwZSkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnIyc6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2RvY3VtZW50LmdldEVsZW1lbnRCeUlkKHNwVGFyZ2V0LmpvaW4oJycpKTtcclxuICAgICAgICAgICAgICAgIGNhc2UgJy4nOlxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLl9kb2N1bWVudC5nZXRFbGVtZW50c0J5Q2xhc3NOYW1lKHNwVGFyZ2V0LmpvaW4oJycpKVswXTtcclxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuX2RvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKHRoaXMubmd0U2Nyb2xsU3B5KVswXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBzcFRhcmdldDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignd2luZG93OnNjcm9sbCcsIFsnJGV2ZW50J10pXHJcbiAgICBvbldpbmRvd1Njcm9sbChldmVudDogRXZlbnQpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCBvZmZzZXQgPSB0aGlzLnNweU9mZnNldCA/IHRoaXMuc3B5T2Zmc2V0IDogMDtcclxuICAgICAgICBjb25zdCBzY3JvbGxUYXJnZXQgPSB0aGlzLl9nZXRUYXJnZXQodGhpcy5uZ3RTY3JvbGxTcHkpO1xyXG4gICAgICAgIGNvbnN0IGVsZW1EaW0gPSBzY3JvbGxUYXJnZXQgPyBzY3JvbGxUYXJnZXQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCkgOiBudWxsO1xyXG4gICAgICAgIGlmICghZWxlbURpbSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLndhcm4oYFRoZXJlIGlzIG5vIGVsZW1lbnQgJHt0aGlzLm5ndFNjcm9sbFNweX1gKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3B5QW5jaG9yICYmIGVsZW1EaW0gJiYgZWxlbURpbS50b3AgPCBvZmZzZXQgJiYgZWxlbURpbS5ib3R0b20gPiBvZmZzZXQpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3B5QW5jaG9yICE9PSAnbm9uZScpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3JvdXRlci5uYXZpZ2F0ZShbXSwgeyBmcmFnbWVudDogdGhpcy5zcHlBbmNob3IgfSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9yb3V0ZXIubmF2aWdhdGUoW10pO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLl9lbGVtLm5hdGl2ZUVsZW1lbnQsICdhY3RpdmUnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLl9lbGVtLm5hdGl2ZUVsZW1lbnQsICdhY3RpdmUnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19