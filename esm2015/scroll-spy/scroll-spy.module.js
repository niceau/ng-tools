/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtScrollSpy } from './scroll-spy';
export { NgtScrollSpy } from './scroll-spy';
/** @type {?} */
const NGT_SCROLL_SPY_DIRECTIVES = [NgtScrollSpy];
export class NgtScrollSpyModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtScrollSpyModule };
    }
}
NgtScrollSpyModule.decorators = [
    { type: NgModule, args: [{
                declarations: NGT_SCROLL_SPY_DIRECTIVES,
                exports: NGT_SCROLL_SPY_DIRECTIVES,
                imports: [CommonModule]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Nyb2xsLXNweS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInNjcm9sbC1zcHkvc2Nyb2xsLXNweS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBRTVDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxjQUFjLENBQUM7O01BRXRDLHlCQUF5QixHQUFHLENBQUMsWUFBWSxDQUFDO0FBT2hELE1BQU0sT0FBTyxrQkFBa0I7Ozs7SUFDM0IsTUFBTSxDQUFDLE9BQU87UUFDVixPQUFPLEVBQUMsUUFBUSxFQUFFLGtCQUFrQixFQUFDLENBQUM7SUFDMUMsQ0FBQzs7O1lBUkosUUFBUSxTQUFDO2dCQUNOLFlBQVksRUFBRSx5QkFBeUI7Z0JBQ3ZDLE9BQU8sRUFBRSx5QkFBeUI7Z0JBQ2xDLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQzthQUMxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5pbXBvcnQgeyBOZ3RTY3JvbGxTcHkgfSBmcm9tICcuL3Njcm9sbC1zcHknO1xyXG5cclxuZXhwb3J0IHsgTmd0U2Nyb2xsU3B5IH0gZnJvbSAnLi9zY3JvbGwtc3B5JztcclxuXHJcbmNvbnN0IE5HVF9TQ1JPTExfU1BZX0RJUkVDVElWRVMgPSBbTmd0U2Nyb2xsU3B5XTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IE5HVF9TQ1JPTExfU1BZX0RJUkVDVElWRVMsXHJcbiAgICBleHBvcnRzOiBOR1RfU0NST0xMX1NQWV9ESVJFQ1RJVkVTLFxyXG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndFNjcm9sbFNweU1vZHVsZSB7XHJcbiAgICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge25nTW9kdWxlOiBOZ3RTY3JvbGxTcHlNb2R1bGV9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==