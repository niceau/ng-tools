/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { NgtTooltip, NgtTooltipWindow } from './tooltip';
export { NgtTooltipConfig } from './tooltip-config';
export { NgtTooltip } from './tooltip';
export class NgtTooltipModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtTooltipModule };
    }
}
NgtTooltipModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NgtTooltip, NgtTooltipWindow],
                exports: [NgtTooltip],
                entryComponents: [NgtTooltipWindow]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInRvb2x0aXAvdG9vbHRpcC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBRTlELE9BQU8sRUFBRSxVQUFVLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxXQUFXLENBQUM7QUFFekQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDcEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQVF2QyxNQUFNLE9BQU8sZ0JBQWdCOzs7O0lBQ3pCLE1BQU0sQ0FBQyxPQUFPO1FBQ1YsT0FBTyxFQUFDLFFBQVEsRUFBRSxnQkFBZ0IsRUFBQyxDQUFDO0lBQ3hDLENBQUM7OztZQVJKLFFBQVEsU0FBQztnQkFDTixZQUFZLEVBQUUsQ0FBQyxVQUFVLEVBQUUsZ0JBQWdCLENBQUM7Z0JBQzVDLE9BQU8sRUFBRSxDQUFDLFVBQVUsQ0FBQztnQkFDckIsZUFBZSxFQUFFLENBQUMsZ0JBQWdCLENBQUM7YUFDdEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTmd0VG9vbHRpcCwgTmd0VG9vbHRpcFdpbmRvdyB9IGZyb20gJy4vdG9vbHRpcCc7XHJcblxyXG5leHBvcnQgeyBOZ3RUb29sdGlwQ29uZmlnIH0gZnJvbSAnLi90b29sdGlwLWNvbmZpZyc7XHJcbmV4cG9ydCB7IE5ndFRvb2x0aXAgfSBmcm9tICcuL3Rvb2x0aXAnO1xyXG5leHBvcnQgeyBQbGFjZW1lbnQgfSBmcm9tICcuLi91dGlsL3Bvc2l0aW9uaW5nJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtOZ3RUb29sdGlwLCBOZ3RUb29sdGlwV2luZG93XSxcclxuICAgIGV4cG9ydHM6IFtOZ3RUb29sdGlwXSxcclxuICAgIGVudHJ5Q29tcG9uZW50czogW05ndFRvb2x0aXBXaW5kb3ddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RUb29sdGlwTW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgICAgIHJldHVybiB7bmdNb2R1bGU6IE5ndFRvb2x0aXBNb2R1bGV9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==