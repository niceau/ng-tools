/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtTooltip directive.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the tooltips used in the application.
 */
export class NgtTooltipConfig {
    constructor() {
        this.autoClose = true;
        this.placement = 'auto';
        this.triggers = 'hover focus';
        this.disableTooltip = false;
        this.openDelay = 0;
        this.closeDelay = 0;
    }
}
NgtTooltipConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtTooltipConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtTooltipConfig_Factory() { return new NgtTooltipConfig(); }, token: NgtTooltipConfig, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtTooltipConfig.prototype.autoClose;
    /** @type {?} */
    NgtTooltipConfig.prototype.placement;
    /** @type {?} */
    NgtTooltipConfig.prototype.triggers;
    /** @type {?} */
    NgtTooltipConfig.prototype.container;
    /** @type {?} */
    NgtTooltipConfig.prototype.disableTooltip;
    /** @type {?} */
    NgtTooltipConfig.prototype.tooltipClass;
    /** @type {?} */
    NgtTooltipConfig.prototype.openDelay;
    /** @type {?} */
    NgtTooltipConfig.prototype.closeDelay;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbHRpcC1jb25maWcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInRvb2x0aXAvdG9vbHRpcC1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFTM0MsTUFBTSxPQUFPLGdCQUFnQjtJQUQ3QjtRQUVJLGNBQVMsR0FBbUMsSUFBSSxDQUFDO1FBQ2pELGNBQVMsR0FBbUIsTUFBTSxDQUFDO1FBQ25DLGFBQVEsR0FBRyxhQUFhLENBQUM7UUFFekIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFFdkIsY0FBUyxHQUFHLENBQUMsQ0FBQztRQUNkLGVBQVUsR0FBRyxDQUFDLENBQUM7S0FDbEI7OztZQVZBLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7Ozs7O0lBRTVCLHFDQUFpRDs7SUFDakQscUNBQW1DOztJQUNuQyxvQ0FBeUI7O0lBQ3pCLHFDQUFrQjs7SUFDbEIsMENBQXVCOztJQUN2Qix3Q0FBcUI7O0lBQ3JCLHFDQUFjOztJQUNkLHNDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQbGFjZW1lbnRBcnJheSB9IGZyb20gJy4uL3V0aWwvcG9zaXRpb25pbmcnO1xyXG5cclxuLyoqXHJcbiAqIENvbmZpZ3VyYXRpb24gc2VydmljZSBmb3IgdGhlIE5ndFRvb2x0aXAgZGlyZWN0aXZlLlxyXG4gKiBZb3UgY2FuIGluamVjdCB0aGlzIHNlcnZpY2UsIHR5cGljYWxseSBpbiB5b3VyIHJvb3QgY29tcG9uZW50LCBhbmQgY3VzdG9taXplIHRoZSB2YWx1ZXMgb2YgaXRzIHByb3BlcnRpZXMgaW5cclxuICogb3JkZXIgdG8gcHJvdmlkZSBkZWZhdWx0IHZhbHVlcyBmb3IgYWxsIHRoZSB0b29sdGlwcyB1c2VkIGluIHRoZSBhcHBsaWNhdGlvbi5cclxuICovXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTmd0VG9vbHRpcENvbmZpZyB7XHJcbiAgICBhdXRvQ2xvc2U6IGJvb2xlYW4gfCAnaW5zaWRlJyB8ICdvdXRzaWRlJyA9IHRydWU7XHJcbiAgICBwbGFjZW1lbnQ6IFBsYWNlbWVudEFycmF5ID0gJ2F1dG8nO1xyXG4gICAgdHJpZ2dlcnMgPSAnaG92ZXIgZm9jdXMnO1xyXG4gICAgY29udGFpbmVyOiBzdHJpbmc7XHJcbiAgICBkaXNhYmxlVG9vbHRpcCA9IGZhbHNlO1xyXG4gICAgdG9vbHRpcENsYXNzOiBzdHJpbmc7XHJcbiAgICBvcGVuRGVsYXkgPSAwO1xyXG4gICAgY2xvc2VEZWxheSA9IDA7XHJcbn1cclxuIl19