/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtProgressbar component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the progress bars used in the application.
 */
export class NgtProgressbarConfig {
    constructor() {
        this.max = 100;
        this.animated = false;
        this.striped = false;
        this.showValue = false;
    }
}
NgtProgressbarConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtProgressbarConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtProgressbarConfig_Factory() { return new NgtProgressbarConfig(); }, token: NgtProgressbarConfig, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtProgressbarConfig.prototype.max;
    /** @type {?} */
    NgtProgressbarConfig.prototype.animated;
    /** @type {?} */
    NgtProgressbarConfig.prototype.striped;
    /** @type {?} */
    NgtProgressbarConfig.prototype.type;
    /** @type {?} */
    NgtProgressbarConfig.prototype.showValue;
    /** @type {?} */
    NgtProgressbarConfig.prototype.height;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZ3Jlc3NiYXItY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJwcm9ncmVzc2Jhci9wcm9ncmVzc2Jhci1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFRM0MsTUFBTSxPQUFPLG9CQUFvQjtJQURqQztRQUVJLFFBQUcsR0FBRyxHQUFHLENBQUM7UUFDVixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFFaEIsY0FBUyxHQUFHLEtBQUssQ0FBQztLQUVyQjs7O1lBUkEsVUFBVSxTQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQzs7Ozs7SUFFNUIsbUNBQVU7O0lBQ1Ysd0NBQWlCOztJQUNqQix1Q0FBZ0I7O0lBQ2hCLG9DQUFhOztJQUNiLHlDQUFrQjs7SUFDbEIsc0NBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vKipcclxuICogQ29uZmlndXJhdGlvbiBzZXJ2aWNlIGZvciB0aGUgTmd0UHJvZ3Jlc3NiYXIgY29tcG9uZW50LlxyXG4gKiBZb3UgY2FuIGluamVjdCB0aGlzIHNlcnZpY2UsIHR5cGljYWxseSBpbiB5b3VyIHJvb3QgY29tcG9uZW50LCBhbmQgY3VzdG9taXplIHRoZSB2YWx1ZXMgb2YgaXRzIHByb3BlcnRpZXMgaW5cclxuICogb3JkZXIgdG8gcHJvdmlkZSBkZWZhdWx0IHZhbHVlcyBmb3IgYWxsIHRoZSBwcm9ncmVzcyBiYXJzIHVzZWQgaW4gdGhlIGFwcGxpY2F0aW9uLlxyXG4gKi9cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQcm9ncmVzc2JhckNvbmZpZyB7XHJcbiAgICBtYXggPSAxMDA7XHJcbiAgICBhbmltYXRlZCA9IGZhbHNlO1xyXG4gICAgc3RyaXBlZCA9IGZhbHNlO1xyXG4gICAgdHlwZTogc3RyaW5nO1xyXG4gICAgc2hvd1ZhbHVlID0gZmFsc2U7XHJcbiAgICBoZWlnaHQ6IHN0cmluZztcclxufVxyXG4iXX0=