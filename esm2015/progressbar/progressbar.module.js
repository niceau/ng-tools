/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtProgressbar } from './progressbar';
export { NgtProgressbar } from './progressbar';
export { NgtProgressbarConfig } from './progressbar-config';
export class NgtProgressbarModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtProgressbarModule };
    }
}
NgtProgressbarModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NgtProgressbar],
                exports: [NgtProgressbar],
                imports: [CommonModule]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZ3Jlc3NiYXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJwcm9ncmVzc2Jhci9wcm9ncmVzc2Jhci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9DLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0MsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFPNUQsTUFBTSxPQUFPLG9CQUFvQjs7OztJQUM3QixNQUFNLENBQUMsT0FBTztRQUNWLE9BQU8sRUFBQyxRQUFRLEVBQUUsb0JBQW9CLEVBQUMsQ0FBQztJQUM1QyxDQUFDOzs7WUFSSixRQUFRLFNBQUM7Z0JBQ04sWUFBWSxFQUFFLENBQUMsY0FBYyxDQUFDO2dCQUM5QixPQUFPLEVBQUUsQ0FBQyxjQUFjLENBQUM7Z0JBQ3pCLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQzthQUMxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5pbXBvcnQgeyBOZ3RQcm9ncmVzc2JhciB9IGZyb20gJy4vcHJvZ3Jlc3NiYXInO1xyXG5cclxuZXhwb3J0IHsgTmd0UHJvZ3Jlc3NiYXIgfSBmcm9tICcuL3Byb2dyZXNzYmFyJztcclxuZXhwb3J0IHsgTmd0UHJvZ3Jlc3NiYXJDb25maWcgfSBmcm9tICcuL3Byb2dyZXNzYmFyLWNvbmZpZyc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbTmd0UHJvZ3Jlc3NiYXJdLFxyXG4gICAgZXhwb3J0czogW05ndFByb2dyZXNzYmFyXSxcclxuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQcm9ncmVzc2Jhck1vZHVsZSB7XHJcbiAgICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge25nTW9kdWxlOiBOZ3RQcm9ncmVzc2Jhck1vZHVsZX07XHJcbiAgICB9XHJcbn1cclxuIl19