/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, HostListener, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Subject } from 'rxjs';
import { NgtPanelService } from './panel.service';
export class NgtPanelsComponent {
    /**
     * @param {?} _panelService
     * @param {?} elRef
     */
    constructor(_panelService, elRef) {
        this._panelService = _panelService;
        this.elRef = elRef;
        this.overlay = true;
        this.leftPanelExpand = new Subject();
        this.leftPanelHide = new Subject();
        this.rightPanelExpand = new Subject();
        this.rightPanelHide = new Subject();
        this.subscriptions = [];
        this._isOpen = false;
        this.subscriptions.push(this._panelService.isPanelsChanged.subscribe((/**
         * @param {?} data
         * @return {?}
         */
        (data) => {
            (data.length < 1) && (this.isOpen = false);
            console.log(data);
            for (const panel of data) {
                this.elRef.nativeElement.querySelector('.panel-container').appendChild(panel.el.nativeElement);
            }
        })));
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onClick(event) {
        this.onClickOutside(event);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set isOpen(value) {
        this._isOpen = value;
    }
    /**
     * @return {?}
     */
    get isOpen() {
        return this._isOpen;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscriptions.push(this._panelService.panelOpeningDidStart.subscribe((/**
         * @param {?} _
         * @return {?}
         */
        _ => this.isOpen = true)));
        this.subscriptions.push(this._panelService.panelClosingDidStart.subscribe((/**
         * @return {?}
         */
        () => {
            !this._panelService.activePanels.length && (this.isOpen = false);
        })));
        // Subscribe state events
        this.subscriptions.push(this.leftPanelExpand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => this._panelService.stateEvents.left.expand.next(status))));
        this.subscriptions.push(this.leftPanelHide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => this._panelService.stateEvents.left.hide.next(status))));
        this.subscriptions.push(this.rightPanelExpand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => this._panelService.stateEvents.right.expand.next(status))));
        this.subscriptions.push(this.rightPanelHide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => this._panelService.stateEvents.right.hide.next(status))));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscriptions.forEach((/**
         * @param {?} subscription
         * @return {?}
         */
        (subscription) => {
            subscription.unsubscribe();
        }));
    }
    /**
     * @param {?} e
     * @return {?}
     */
    onClickOutside(e) {
        if (!e.target.closest('ngt-panel')) {
            this._panelService.closePanel();
        }
    }
}
NgtPanelsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-panels',
                template: `
        <div class="panel-container"></div>
        <div
            *ngIf="overlay"
            (click)="onClickOutside($event)"
            class="panel-overlay"
            [@overlay]='isOpen ? "open" : "close"'>
        </div>
    `,
                animations: [
                    trigger('overlay', [
                        state('open', style({ opacity: 1 })),
                        state('close', style({ opacity: 0, display: 'none' })),
                        transition('close => open', animate('300ms')),
                        transition('open => close', animate('300ms'))
                    ])
                ]
            }] }
];
/** @nocollapse */
NgtPanelsComponent.ctorParameters = () => [
    { type: NgtPanelService },
    { type: ElementRef }
];
NgtPanelsComponent.propDecorators = {
    overlay: [{ type: Input }],
    leftPanelExpand: [{ type: Input }],
    leftPanelHide: [{ type: Input }],
    rightPanelExpand: [{ type: Input }],
    rightPanelHide: [{ type: Input }],
    onClick: [{ type: HostListener, args: ['document:click', ['$event'],] }],
    isOpen: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NgtPanelsComponent.prototype.overlay;
    /** @type {?} */
    NgtPanelsComponent.prototype.leftPanelExpand;
    /** @type {?} */
    NgtPanelsComponent.prototype.leftPanelHide;
    /** @type {?} */
    NgtPanelsComponent.prototype.rightPanelExpand;
    /** @type {?} */
    NgtPanelsComponent.prototype.rightPanelHide;
    /** @type {?} */
    NgtPanelsComponent.prototype.subscriptions;
    /**
     * @type {?}
     * @private
     */
    NgtPanelsComponent.prototype._isOpen;
    /**
     * @type {?}
     * @private
     */
    NgtPanelsComponent.prototype._panelService;
    /**
     * @type {?}
     * @private
     */
    NgtPanelsComponent.prototype.elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWxzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsicGFuZWxzL3BhbmVscy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLE1BQU0sZUFBZSxDQUFDO0FBQzlGLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDakYsT0FBTyxFQUFFLE9BQU8sRUFBZ0IsTUFBTSxNQUFNLENBQUM7QUFFN0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBdUJsRCxNQUFNLE9BQU8sa0JBQWtCOzs7OztJQXVCM0IsWUFBb0IsYUFBOEIsRUFDOUIsS0FBaUI7UUFEakIsa0JBQWEsR0FBYixhQUFhLENBQWlCO1FBQzlCLFVBQUssR0FBTCxLQUFLLENBQVk7UUF2QjVCLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixvQkFBZSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFDekMsa0JBQWEsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBQ3ZDLHFCQUFnQixHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFDMUMsbUJBQWMsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBQ2pELGtCQUFhLEdBQXdCLEVBQUUsQ0FBQztRQUNoQyxZQUFPLEdBQUcsS0FBSyxDQUFDO1FBa0JwQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxTQUFTOzs7O1FBQ2hFLENBQUMsSUFBOEIsRUFBRSxFQUFFO1lBQy9CLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLENBQUM7WUFDM0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNsQixLQUFLLE1BQU0sS0FBSyxJQUFJLElBQUksRUFBRTtnQkFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLENBQUM7YUFDbEc7UUFDTCxDQUFDLEVBQ0EsQ0FDSixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUF6QkQsT0FBTyxDQUFDLEtBQUs7UUFDVCxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRUQsSUFDSSxNQUFNLENBQUMsS0FBYztRQUNyQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsSUFBSSxNQUFNO1FBQ04sT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ3hCLENBQUM7Ozs7SUFnQkQsUUFBUTtRQUNKLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUMsU0FBUzs7OztRQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLEVBQUMsQ0FBQyxDQUFDO1FBQ3BHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUMsU0FBUzs7O1FBQ3JFLEdBQUcsRUFBRTtZQUNELENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsQ0FBQztRQUNyRSxDQUFDLEVBQ0EsQ0FDSixDQUFDO1FBRUYseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUzs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUMsQ0FBQyxDQUFDO1FBQzNILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUzs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUMsQ0FBQyxDQUFDO1FBQ3ZILElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQyxDQUFDLENBQUM7UUFDN0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBQyxDQUFDLENBQUM7SUFDN0gsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU87Ozs7UUFBQyxDQUFDLFlBQTBCLEVBQUUsRUFBRTtZQUN0RCxZQUFZLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDL0IsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxDQUFDO1FBQ1osSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ2hDLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDbkM7SUFDTCxDQUFDOzs7WUFuRkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxZQUFZO2dCQUN0QixRQUFRLEVBQUU7Ozs7Ozs7O0tBUVQ7Z0JBQ0QsVUFBVSxFQUFFO29CQUNSLE9BQU8sQ0FBQyxTQUFTLEVBQUU7d0JBQ2YsS0FBSyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsRUFBQyxPQUFPLEVBQUUsQ0FBQyxFQUFDLENBQUMsQ0FBQzt3QkFDbEMsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsRUFBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFDO3dCQUNwRCxVQUFVLENBQUMsZUFBZSxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDN0MsVUFBVSxDQUFDLGVBQWUsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ2hELENBQUM7aUJBQ0w7YUFDSjs7OztZQXRCUSxlQUFlO1lBSkosVUFBVTs7O3NCQTRCekIsS0FBSzs4QkFDTCxLQUFLOzRCQUNMLEtBQUs7K0JBQ0wsS0FBSzs2QkFDTCxLQUFLO3NCQUlMLFlBQVksU0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFFBQVEsQ0FBQztxQkFLekMsS0FBSzs7OztJQWJOLHFDQUF3Qjs7SUFDeEIsNkNBQWtEOztJQUNsRCwyQ0FBZ0Q7O0lBQ2hELDhDQUFtRDs7SUFDbkQsNENBQWlEOztJQUNqRCwyQ0FBd0M7Ozs7O0lBQ3hDLHFDQUF3Qjs7Ozs7SUFnQlosMkNBQXNDOzs7OztJQUN0QyxtQ0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEhvc3RMaXN0ZW5lciwgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IGFuaW1hdGUsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlciB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgeyBTdWJqZWN0LCBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IE5ndFBhbmVsU2VydmljZSB9IGZyb20gJy4vcGFuZWwuc2VydmljZSc7XHJcbmltcG9ydCB7IE5ndFBhbmVsQ29tcG9uZW50IH0gZnJvbSAnLi9wYW5lbC5jb21wb25lbnQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC1wYW5lbHMnLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8ZGl2IGNsYXNzPVwicGFuZWwtY29udGFpbmVyXCI+PC9kaXY+XHJcbiAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICAqbmdJZj1cIm92ZXJsYXlcIlxyXG4gICAgICAgICAgICAoY2xpY2spPVwib25DbGlja091dHNpZGUoJGV2ZW50KVwiXHJcbiAgICAgICAgICAgIGNsYXNzPVwicGFuZWwtb3ZlcmxheVwiXHJcbiAgICAgICAgICAgIFtAb3ZlcmxheV09J2lzT3BlbiA/IFwib3BlblwiIDogXCJjbG9zZVwiJz5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIGAsXHJcbiAgICBhbmltYXRpb25zOiBbXHJcbiAgICAgICAgdHJpZ2dlcignb3ZlcmxheScsIFtcclxuICAgICAgICAgICAgc3RhdGUoJ29wZW4nLCBzdHlsZSh7b3BhY2l0eTogMX0pKSxcclxuICAgICAgICAgICAgc3RhdGUoJ2Nsb3NlJywgc3R5bGUoe29wYWNpdHk6IDAsIGRpc3BsYXk6ICdub25lJ30pKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbignY2xvc2UgPT4gb3BlbicsIGFuaW1hdGUoJzMwMG1zJykpLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCdvcGVuID0+IGNsb3NlJywgYW5pbWF0ZSgnMzAwbXMnKSlcclxuICAgICAgICBdKVxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0UGFuZWxzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gICAgQElucHV0KCkgb3ZlcmxheSA9IHRydWU7XHJcbiAgICBASW5wdXQoKSBsZWZ0UGFuZWxFeHBhbmQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG4gICAgQElucHV0KCkgbGVmdFBhbmVsSGlkZSA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcbiAgICBASW5wdXQoKSByaWdodFBhbmVsRXhwYW5kID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcclxuICAgIEBJbnB1dCgpIHJpZ2h0UGFuZWxIaWRlID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcclxuICAgIHN1YnNjcmlwdGlvbnM6IEFycmF5PFN1YnNjcmlwdGlvbj4gPSBbXTtcclxuICAgIHByaXZhdGUgX2lzT3BlbiA9IGZhbHNlO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmNsaWNrJywgWyckZXZlbnQnXSlcclxuICAgIG9uQ2xpY2soZXZlbnQpIHtcclxuICAgICAgICB0aGlzLm9uQ2xpY2tPdXRzaWRlKGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgc2V0IGlzT3Blbih2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgICAgIHRoaXMuX2lzT3BlbiA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpc09wZW4oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzT3BlbjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9wYW5lbFNlcnZpY2U6IE5ndFBhbmVsU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgZWxSZWY6IEVsZW1lbnRSZWYpIHtcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaCh0aGlzLl9wYW5lbFNlcnZpY2UuaXNQYW5lbHNDaGFuZ2VkLnN1YnNjcmliZShcclxuICAgICAgICAgICAgKGRhdGE6IEFycmF5PE5ndFBhbmVsQ29tcG9uZW50PikgPT4ge1xyXG4gICAgICAgICAgICAgICAgKGRhdGEubGVuZ3RoIDwgMSkgJiYgKHRoaXMuaXNPcGVuID0gZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSk7XHJcbiAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IHBhbmVsIG9mIGRhdGEpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVsUmVmLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignLnBhbmVsLWNvbnRhaW5lcicpLmFwcGVuZENoaWxkKHBhbmVsLmVsLm5hdGl2ZUVsZW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKHRoaXMuX3BhbmVsU2VydmljZS5wYW5lbE9wZW5pbmdEaWRTdGFydC5zdWJzY3JpYmUoXyA9PiB0aGlzLmlzT3BlbiA9IHRydWUpKTtcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaCh0aGlzLl9wYW5lbFNlcnZpY2UucGFuZWxDbG9zaW5nRGlkU3RhcnQuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAhdGhpcy5fcGFuZWxTZXJ2aWNlLmFjdGl2ZVBhbmVscy5sZW5ndGggJiYgKHRoaXMuaXNPcGVuID0gZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIClcclxuICAgICAgICApO1xyXG5cclxuICAgICAgICAvLyBTdWJzY3JpYmUgc3RhdGUgZXZlbnRzXHJcbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb25zLnB1c2godGhpcy5sZWZ0UGFuZWxFeHBhbmQuc3Vic2NyaWJlKHN0YXR1cyA9PiB0aGlzLl9wYW5lbFNlcnZpY2Uuc3RhdGVFdmVudHMubGVmdC5leHBhbmQubmV4dChzdGF0dXMpKSk7XHJcbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb25zLnB1c2godGhpcy5sZWZ0UGFuZWxIaWRlLnN1YnNjcmliZShzdGF0dXMgPT4gdGhpcy5fcGFuZWxTZXJ2aWNlLnN0YXRlRXZlbnRzLmxlZnQuaGlkZS5uZXh0KHN0YXR1cykpKTtcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaCh0aGlzLnJpZ2h0UGFuZWxFeHBhbmQuc3Vic2NyaWJlKHN0YXR1cyA9PiB0aGlzLl9wYW5lbFNlcnZpY2Uuc3RhdGVFdmVudHMucmlnaHQuZXhwYW5kLm5leHQoc3RhdHVzKSkpO1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKHRoaXMucmlnaHRQYW5lbEhpZGUuc3Vic2NyaWJlKHN0YXR1cyA9PiB0aGlzLl9wYW5lbFNlcnZpY2Uuc3RhdGVFdmVudHMucmlnaHQuaGlkZS5uZXh0KHN0YXR1cykpKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMuZm9yRWFjaCgoc3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb24pID0+IHtcclxuICAgICAgICAgICAgc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25DbGlja091dHNpZGUoZSkge1xyXG4gICAgICAgIGlmICghZS50YXJnZXQuY2xvc2VzdCgnbmd0LXBhbmVsJykpIHtcclxuICAgICAgICAgICAgdGhpcy5fcGFuZWxTZXJ2aWNlLmNsb3NlUGFuZWwoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19