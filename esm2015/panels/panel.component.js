/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, Injector, Input } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NgtPanelService } from './panel.service';
import { DEFAULT_NGC_PANEL_CONFIG, NGC_PANEL_CONFIG } from './panel.config';
export class NgtPanelComponent {
    /**
     * @param {?} _panelService
     * @param {?} _elRef
     * @param {?} injector
     */
    constructor(_panelService, _elRef, injector) {
        this._panelService = _panelService;
        this._elRef = _elRef;
        this.dir = 'left';
        this._isOpen = false;
        this.statuses = {
            left: {
                expanded: {
                    open: 'openLeftExpanded',
                    close: 'closeLeftExpanded'
                },
                collapsed: {
                    open: 'openLeftCollapsed',
                    close: 'closeLeftCollapsed'
                },
                hidden: {
                    open: 'openLeftHidden',
                    close: 'closeLeftHidden'
                }
            },
            right: {
                expanded: {
                    open: 'openRightExpanded',
                    close: 'closeRightExpanded'
                },
                collapsed: {
                    open: 'openRightCollapsed',
                    close: 'closeRightCollapsed'
                },
                hidden: {
                    open: 'openRightHidden',
                    close: 'closeRightHidden'
                }
            }
        };
        this.openStatuses = [
            this.statuses.left.expanded.open,
            this.statuses.left.collapsed.open,
            this.statuses.left.hidden.open,
            this.statuses.right.expanded.open,
            this.statuses.right.collapsed.open,
            this.statuses.right.hidden.open,
        ];
        this.closeStatuses = [
            this.statuses.left.expanded.open,
            this.statuses.left.collapsed.open,
            this.statuses.left.hidden.open,
            this.statuses.right.expanded.open,
            this.statuses.right.collapsed.open,
            this.statuses.right.hidden.open,
        ];
        this.subscriptions = [];
        this.styles = {};
        this.config = Object.assign({}, DEFAULT_NGC_PANEL_CONFIG, injector.get(NGC_PANEL_CONFIG));
        this.el = this._elRef;
        this.element = this._elRef.nativeElement;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set isOpen(value) {
        this._isOpen = value;
    }
    /**
     * @return {?}
     */
    get isOpen() {
        return this._isOpen;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        // direction
        this.updateStates(this.dir);
        this.subscriptions.push(this._panelService.stateEvents[this.dir].expand.subscribe((/**
         * @param {?} _
         * @return {?}
         */
        _ => this.updateStates(this.dir))));
        this.subscriptions.push(this._panelService.stateEvents[this.dir].hide.subscribe((/**
         * @param {?} _
         * @return {?}
         */
        _ => this.updateStates(this.dir))));
        // ensure id attribute exists
        if (!this.id) {
            console.error('panel must have an id');
            return;
        }
        // add self (this panel instance) to the panel service so it's accessible from controllers
        this._panelService.add(this);
        // subscribe events
        this.subscriptions.push(this._panelService.panelWillOpened.subscribe((/**
         * @param {?} id
         * @return {?}
         */
        (id) => {
            if (id === this.id) {
                this.open();
            }
        })));
        this.subscriptions.push(this._panelService.panelWillClosed.subscribe((/**
         * @param {?} id
         * @return {?}
         */
        (id) => {
            if (id === this.id) {
                this.close();
            }
        })));
    }
    // remove self from panel service when directive is destroyed
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this._panelService.remove(this.id);
        this._panelService.removeFromActive(this.id);
        this.element.parentNode.removeChild(this.element);
        this.subscriptions.forEach((/**
         * @param {?} subscription
         * @return {?}
         */
        (subscription) => {
            subscription.unsubscribe();
        }));
    }
    // open panel
    /**
     * @return {?}
     */
    open() {
        this._panelService.addToActive(this.id);
        this.setStyle(this._panelService.activePanels.length);
        this.isOpen = true;
    }
    // close panel
    /**
     * @return {?}
     */
    close() {
        this._panelService.removeFromActive(this.id);
        this.isOpen = false;
    }
    /**
     * @param {?} dir
     * @return {?}
     */
    updateStates(dir) {
        this.openState = this.statuses[dir][this._panelService.panelState].open;
        this.closeState = this.statuses[dir][this._panelService.panelState].close;
    }
    /**
     * @param {?} i
     * @return {?}
     */
    setStyle(i) {
        this.styles = {
            'zIndex': i
        };
    }
    /**
     * @param {?} event
     * @return {?}
     */
    animationAction(event) {
        switch (event.phaseName) {
            case 'start':
                this.openStatuses.includes(event.toState) && this._panelService.panelOpeningDidStart.next();
                this.closeStatuses.includes(event.toState) && this._panelService.panelClosingDidStart.next();
                break;
            case 'done':
                this.openStatuses.includes(event.toState) && this._panelService.panelOpeningDidDone.next();
                this.closeStatuses.includes(event.toState) && this._panelService.panelClosingDidDone.next();
                break;
        }
    }
}
NgtPanelComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-panel',
                template: `
        <div class="panel-wrap"
             [ngStyle]="styles"
             [@panel]='{
		     	value: isOpen ? this.openState : this.closeState,
		     	params: {
		     		leftCollapsedWidth: config.leftPanelCollapsedShift,
		     		leftExpandedWidth: config.leftPanelExpandedShift,
		     		rightCollapsedWidth: config.rightPanelCollapsedShift,
		     		rightExpandedWidth: config.rightPanelExpandedShift
		     	}
		     }'
             (@panel.start)="animationAction($event)"
             (@panel.done)="animationAction($event)">
            <ng-content></ng-content>
        </div>
    `,
                animations: [
                    trigger('panel', [
                        state('openLeftHidden', style({ left: 0, transform: 'none' })),
                        state('openLeftCollapsed', style({
                            left: 0,
                            transform: 'translateX({{ leftCollapsedWidth }}px)'
                        }), { params: { leftCollapsedWidth: DEFAULT_NGC_PANEL_CONFIG.leftPanelCollapsedShift } }),
                        state('openLeftExpanded', style({
                            left: 0,
                            transform: 'translateX({{ leftExpandedWidth }}px)'
                        }), { params: { leftExpandedWidth: DEFAULT_NGC_PANEL_CONFIG.leftPanelExpandedShift } }),
                        state('closeLeftHidden', style({ left: 0, transform: 'translateX(-100%)' })),
                        state('closeLeftCollapsed', style({
                            left: 0,
                            transform: 'translateX(calc(-100% + {{ leftCollapsedWidth }}px))'
                        }), { params: { leftCollapsedWidth: DEFAULT_NGC_PANEL_CONFIG.leftPanelCollapsedShift } }),
                        state('closeLeftExpanded', style({
                            left: 0,
                            transform: 'translateX(calc(-100% + {{ leftExpandedWidth }}px))'
                        }), { params: { leftExpandedWidth: DEFAULT_NGC_PANEL_CONFIG.leftPanelExpandedShift } }),
                        state('openRightHidden', style({ right: 0, transform: 'none' })),
                        state('openRightCollapsed', style({
                            right: 0,
                            transform: 'translateX(-{{ rightCollapsedWidth }}px)'
                        }), { params: { rightCollapsedWidth: DEFAULT_NGC_PANEL_CONFIG.rightPanelCollapsedShift } }),
                        state('openRightExpanded', style({
                            right: 0,
                            transform: 'translateX(-{{ rightExpandedWidth }}px)'
                        }), { params: { rightExpandedWidth: DEFAULT_NGC_PANEL_CONFIG.rightPanelExpandedShift } }),
                        state('closeRightHidden', style({ right: 0, transform: 'translateX(100%)' })),
                        state('closeRightCollapsed', style({
                            right: 0,
                            transform: 'translateX(calc(100% - {{ rightCollapsedWidth }}px))'
                        }), { params: { rightCollapsedWidth: DEFAULT_NGC_PANEL_CONFIG.rightPanelCollapsedShift } }),
                        state('closeRightExpanded', style({
                            right: 0,
                            transform: 'translateX(calc(100% - {{ rightExpandedWidth }}px))'
                        }), { params: { rightExpandedWidth: DEFAULT_NGC_PANEL_CONFIG.rightPanelExpandedShift } }),
                        transition('void => *', animate('0s')),
                        transition('* => *', animate('0.4s cubic-bezier(0.05, 0.74, 0.2, 0.99)'))
                    ])
                ]
            }] }
];
/** @nocollapse */
NgtPanelComponent.ctorParameters = () => [
    { type: NgtPanelService },
    { type: ElementRef },
    { type: Injector }
];
NgtPanelComponent.propDecorators = {
    id: [{ type: Input }],
    dir: [{ type: Input }],
    isOpen: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NgtPanelComponent.prototype.id;
    /** @type {?} */
    NgtPanelComponent.prototype.dir;
    /**
     * @type {?}
     * @private
     */
    NgtPanelComponent.prototype._isOpen;
    /** @type {?} */
    NgtPanelComponent.prototype.statuses;
    /** @type {?} */
    NgtPanelComponent.prototype.openStatuses;
    /** @type {?} */
    NgtPanelComponent.prototype.closeStatuses;
    /** @type {?} */
    NgtPanelComponent.prototype.subscriptions;
    /** @type {?} */
    NgtPanelComponent.prototype.openState;
    /** @type {?} */
    NgtPanelComponent.prototype.closeState;
    /** @type {?} */
    NgtPanelComponent.prototype.styles;
    /** @type {?} */
    NgtPanelComponent.prototype.element;
    /** @type {?} */
    NgtPanelComponent.prototype.el;
    /** @type {?} */
    NgtPanelComponent.prototype.config;
    /**
     * @type {?}
     * @private
     */
    NgtPanelComponent.prototype._panelService;
    /**
     * @type {?}
     * @private
     */
    NgtPanelComponent.prototype._elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJwYW5lbHMvcGFuZWwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUMxRixPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBR2pGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsZ0JBQWdCLEVBQTJCLE1BQU0sZ0JBQWdCLENBQUM7QUFrRXJHLE1BQU0sT0FBTyxpQkFBaUI7Ozs7OztJQW1FMUIsWUFBb0IsYUFBOEIsRUFDOUIsTUFBa0IsRUFDMUIsUUFBa0I7UUFGVixrQkFBYSxHQUFiLGFBQWEsQ0FBaUI7UUFDOUIsV0FBTSxHQUFOLE1BQU0sQ0FBWTtRQWxFN0IsUUFBRyxHQUFHLE1BQU0sQ0FBQztRQUNkLFlBQU8sR0FBRyxLQUFLLENBQUM7UUFDeEIsYUFBUSxHQUFHO1lBQ1AsSUFBSSxFQUFFO2dCQUNGLFFBQVEsRUFBRTtvQkFDTixJQUFJLEVBQUUsa0JBQWtCO29CQUN4QixLQUFLLEVBQUUsbUJBQW1CO2lCQUM3QjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1AsSUFBSSxFQUFFLG1CQUFtQjtvQkFDekIsS0FBSyxFQUFFLG9CQUFvQjtpQkFDOUI7Z0JBQ0QsTUFBTSxFQUFFO29CQUNKLElBQUksRUFBRSxnQkFBZ0I7b0JBQ3RCLEtBQUssRUFBRSxpQkFBaUI7aUJBQzNCO2FBQ0o7WUFDRCxLQUFLLEVBQUU7Z0JBQ0gsUUFBUSxFQUFFO29CQUNOLElBQUksRUFBRSxtQkFBbUI7b0JBQ3pCLEtBQUssRUFBRSxvQkFBb0I7aUJBQzlCO2dCQUNELFNBQVMsRUFBRTtvQkFDUCxJQUFJLEVBQUUsb0JBQW9CO29CQUMxQixLQUFLLEVBQUUscUJBQXFCO2lCQUMvQjtnQkFDRCxNQUFNLEVBQUU7b0JBQ0osSUFBSSxFQUFFLGlCQUFpQjtvQkFDdkIsS0FBSyxFQUFFLGtCQUFrQjtpQkFDNUI7YUFDSjtTQUNKLENBQUM7UUFDRixpQkFBWSxHQUFHO1lBQ1gsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUk7WUFDaEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUk7WUFDakMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUk7WUFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUk7WUFDakMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLElBQUk7WUFDbEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUk7U0FDbEMsQ0FBQztRQUNGLGtCQUFhLEdBQUc7WUFDWixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSTtZQUNoQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSTtZQUNqQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSTtZQUM5QixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSTtZQUNqQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSTtZQUNsQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSTtTQUNsQyxDQUFDO1FBQ0Ysa0JBQWEsR0FBd0IsRUFBRSxDQUFDO1FBR3hDLFdBQU0sR0FBRyxFQUFFLENBQUM7UUFpQlIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSx3QkFBd0IsRUFBRSxRQUFRLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztRQUMxRixJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQztJQUM3QyxDQUFDOzs7OztJQWZELElBQ0ksTUFBTSxDQUFDLEtBQWM7UUFDckIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDOzs7O0lBVUQsUUFBUTtRQUNKLFlBQVk7UUFDWixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM1QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUMsQ0FBQztRQUNySCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUMsQ0FBQztRQUVuSCw2QkFBNkI7UUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUU7WUFDVixPQUFPLENBQUMsS0FBSyxDQUFDLHVCQUF1QixDQUFDLENBQUM7WUFDdkMsT0FBTztTQUNWO1FBRUQsMEZBQTBGO1FBQzFGLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRTdCLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxTQUFTOzs7O1FBQ2hFLENBQUMsRUFBVSxFQUFFLEVBQUU7WUFDWCxJQUFJLEVBQUUsS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUNoQixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDZjtRQUNMLENBQUMsRUFDQSxDQUNKLENBQUM7UUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxTQUFTOzs7O1FBQ2hFLENBQUMsRUFBVSxFQUFFLEVBQUU7WUFDWCxJQUFJLEVBQUUsS0FBSyxJQUFJLENBQUMsRUFBRSxFQUFFO2dCQUNoQixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDaEI7UUFDTCxDQUFDLEVBQ0EsQ0FDSixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFHRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPOzs7O1FBQUMsQ0FBQyxZQUEwQixFQUFFLEVBQUU7WUFDdEQsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFHRCxJQUFJO1FBQ0EsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFDdkIsQ0FBQzs7Ozs7SUFHRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsR0FBVztRQUNwQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDeEUsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQzlFLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLENBQVM7UUFDZCxJQUFJLENBQUMsTUFBTSxHQUFHO1lBQ1YsUUFBUSxFQUFFLENBQUM7U0FDZCxDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFRCxlQUFlLENBQUMsS0FBSztRQUNqQixRQUFRLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDckIsS0FBSyxPQUFPO2dCQUNSLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxDQUFDO2dCQUM1RixJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDN0YsTUFBTTtZQUNWLEtBQUssTUFBTTtnQkFDUCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDM0YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQzVGLE1BQU07U0FDYjtJQUNMLENBQUM7OztZQTFOSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7OztLQWdCVDtnQkFDRCxVQUFVLEVBQUU7b0JBQ1IsT0FBTyxDQUFDLE9BQU8sRUFBRTt3QkFDYixLQUFLLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxDQUFDLEVBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQzt3QkFDNUQsS0FBSyxDQUFDLG1CQUFtQixFQUFFLEtBQUssQ0FBQzs0QkFDN0IsSUFBSSxFQUFFLENBQUM7NEJBQ1AsU0FBUyxFQUFFLHdDQUF3Qzt5QkFDdEQsQ0FBQyxFQUFFLEVBQUMsTUFBTSxFQUFFLEVBQUMsa0JBQWtCLEVBQUUsd0JBQXdCLENBQUMsdUJBQXVCLEVBQUMsRUFBQyxDQUFDO3dCQUNyRixLQUFLLENBQUMsa0JBQWtCLEVBQUUsS0FBSyxDQUFDOzRCQUM1QixJQUFJLEVBQUUsQ0FBQzs0QkFDUCxTQUFTLEVBQUUsdUNBQXVDO3lCQUNyRCxDQUFDLEVBQUUsRUFBQyxNQUFNLEVBQUUsRUFBQyxpQkFBaUIsRUFBRSx3QkFBd0IsQ0FBQyxzQkFBc0IsRUFBQyxFQUFDLENBQUM7d0JBQ25GLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLENBQUMsRUFBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxtQkFBbUIsRUFBQyxDQUFDLENBQUM7d0JBQzFFLEtBQUssQ0FBQyxvQkFBb0IsRUFBRSxLQUFLLENBQUM7NEJBQzlCLElBQUksRUFBRSxDQUFDOzRCQUNQLFNBQVMsRUFBRSxzREFBc0Q7eUJBQ3BFLENBQUMsRUFBRSxFQUFDLE1BQU0sRUFBRSxFQUFDLGtCQUFrQixFQUFFLHdCQUF3QixDQUFDLHVCQUF1QixFQUFDLEVBQUMsQ0FBQzt3QkFDckYsS0FBSyxDQUFDLG1CQUFtQixFQUFFLEtBQUssQ0FBQzs0QkFDN0IsSUFBSSxFQUFFLENBQUM7NEJBQ1AsU0FBUyxFQUFFLHFEQUFxRDt5QkFDbkUsQ0FBQyxFQUFFLEVBQUMsTUFBTSxFQUFFLEVBQUMsaUJBQWlCLEVBQUUsd0JBQXdCLENBQUMsc0JBQXNCLEVBQUMsRUFBQyxDQUFDO3dCQUVuRixLQUFLLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxDQUFDLEVBQUMsS0FBSyxFQUFFLENBQUMsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFDLENBQUMsQ0FBQzt3QkFDOUQsS0FBSyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQzs0QkFDOUIsS0FBSyxFQUFFLENBQUM7NEJBQ1IsU0FBUyxFQUFFLDBDQUEwQzt5QkFDeEQsQ0FBQyxFQUFFLEVBQUMsTUFBTSxFQUFFLEVBQUMsbUJBQW1CLEVBQUUsd0JBQXdCLENBQUMsd0JBQXdCLEVBQUMsRUFBQyxDQUFDO3dCQUN2RixLQUFLLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDOzRCQUM3QixLQUFLLEVBQUUsQ0FBQzs0QkFDUixTQUFTLEVBQUUseUNBQXlDO3lCQUN2RCxDQUFDLEVBQUUsRUFBQyxNQUFNLEVBQUUsRUFBQyxrQkFBa0IsRUFBRSx3QkFBd0IsQ0FBQyx1QkFBdUIsRUFBQyxFQUFDLENBQUM7d0JBQ3JGLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxLQUFLLENBQUMsRUFBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxrQkFBa0IsRUFBQyxDQUFDLENBQUM7d0JBQzNFLEtBQUssQ0FBQyxxQkFBcUIsRUFBRSxLQUFLLENBQUM7NEJBQy9CLEtBQUssRUFBRSxDQUFDOzRCQUNSLFNBQVMsRUFBRSxzREFBc0Q7eUJBQ3BFLENBQUMsRUFBRSxFQUFDLE1BQU0sRUFBRSxFQUFDLG1CQUFtQixFQUFFLHdCQUF3QixDQUFDLHdCQUF3QixFQUFDLEVBQUMsQ0FBQzt3QkFDdkYsS0FBSyxDQUFDLG9CQUFvQixFQUFFLEtBQUssQ0FBQzs0QkFDOUIsS0FBSyxFQUFFLENBQUM7NEJBQ1IsU0FBUyxFQUFFLHFEQUFxRDt5QkFDbkUsQ0FBQyxFQUFFLEVBQUMsTUFBTSxFQUFFLEVBQUMsa0JBQWtCLEVBQUUsd0JBQXdCLENBQUMsdUJBQXVCLEVBQUMsRUFBQyxDQUFDO3dCQUVyRixVQUFVLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQzt3QkFDdEMsVUFBVSxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsMENBQTBDLENBQUMsQ0FBQztxQkFDNUUsQ0FBQztpQkFDTDthQUNKOzs7O1lBbEVRLGVBQWU7WUFKSixVQUFVO1lBQUUsUUFBUTs7O2lCQXdFbkMsS0FBSztrQkFDTCxLQUFLO3FCQXdETCxLQUFLOzs7O0lBekROLCtCQUFvQjs7SUFDcEIsZ0NBQXNCOzs7OztJQUN0QixvQ0FBd0I7O0lBQ3hCLHFDQTZCRTs7SUFDRix5Q0FPRTs7SUFDRiwwQ0FPRTs7SUFDRiwwQ0FBd0M7O0lBQ3hDLHNDQUFrQjs7SUFDbEIsdUNBQW1COztJQUNuQixtQ0FBWTs7SUFDWixvQ0FBUTs7SUFDUiwrQkFBZTs7SUFDZixtQ0FBZ0M7Ozs7O0lBV3BCLDBDQUFzQzs7Ozs7SUFDdEMsbUNBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBJbmplY3RvciwgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IGFuaW1hdGUsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlciB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IE5ndFBhbmVsU2VydmljZSB9IGZyb20gJy4vcGFuZWwuc2VydmljZSc7XHJcbmltcG9ydCB7IERFRkFVTFRfTkdDX1BBTkVMX0NPTkZJRywgTkdDX1BBTkVMX0NPTkZJRywgTmd0UGFuZWxDb25maWdJbnRlcmZhY2UgfSBmcm9tICcuL3BhbmVsLmNvbmZpZyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbmd0LXBhbmVsJyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPGRpdiBjbGFzcz1cInBhbmVsLXdyYXBcIlxyXG4gICAgICAgICAgICAgW25nU3R5bGVdPVwic3R5bGVzXCJcclxuICAgICAgICAgICAgIFtAcGFuZWxdPSd7XHJcblx0XHQgICAgIFx0dmFsdWU6IGlzT3BlbiA/IHRoaXMub3BlblN0YXRlIDogdGhpcy5jbG9zZVN0YXRlLFxyXG5cdFx0ICAgICBcdHBhcmFtczoge1xyXG5cdFx0ICAgICBcdFx0bGVmdENvbGxhcHNlZFdpZHRoOiBjb25maWcubGVmdFBhbmVsQ29sbGFwc2VkU2hpZnQsXHJcblx0XHQgICAgIFx0XHRsZWZ0RXhwYW5kZWRXaWR0aDogY29uZmlnLmxlZnRQYW5lbEV4cGFuZGVkU2hpZnQsXHJcblx0XHQgICAgIFx0XHRyaWdodENvbGxhcHNlZFdpZHRoOiBjb25maWcucmlnaHRQYW5lbENvbGxhcHNlZFNoaWZ0LFxyXG5cdFx0ICAgICBcdFx0cmlnaHRFeHBhbmRlZFdpZHRoOiBjb25maWcucmlnaHRQYW5lbEV4cGFuZGVkU2hpZnRcclxuXHRcdCAgICAgXHR9XHJcblx0XHQgICAgIH0nXHJcbiAgICAgICAgICAgICAoQHBhbmVsLnN0YXJ0KT1cImFuaW1hdGlvbkFjdGlvbigkZXZlbnQpXCJcclxuICAgICAgICAgICAgIChAcGFuZWwuZG9uZSk9XCJhbmltYXRpb25BY3Rpb24oJGV2ZW50KVwiPlxyXG4gICAgICAgICAgICA8bmctY29udGVudD48L25nLWNvbnRlbnQ+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICBgLFxyXG4gICAgYW5pbWF0aW9uczogW1xyXG4gICAgICAgIHRyaWdnZXIoJ3BhbmVsJywgW1xyXG4gICAgICAgICAgICBzdGF0ZSgnb3BlbkxlZnRIaWRkZW4nLCBzdHlsZSh7bGVmdDogMCwgdHJhbnNmb3JtOiAnbm9uZSd9KSksXHJcbiAgICAgICAgICAgIHN0YXRlKCdvcGVuTGVmdENvbGxhcHNlZCcsIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgIGxlZnQ6IDAsXHJcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKHt7IGxlZnRDb2xsYXBzZWRXaWR0aCB9fXB4KSdcclxuICAgICAgICAgICAgfSksIHtwYXJhbXM6IHtsZWZ0Q29sbGFwc2VkV2lkdGg6IERFRkFVTFRfTkdDX1BBTkVMX0NPTkZJRy5sZWZ0UGFuZWxDb2xsYXBzZWRTaGlmdH19KSxcclxuICAgICAgICAgICAgc3RhdGUoJ29wZW5MZWZ0RXhwYW5kZWQnLCBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICBsZWZ0OiAwLFxyXG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWCh7eyBsZWZ0RXhwYW5kZWRXaWR0aCB9fXB4KSdcclxuICAgICAgICAgICAgfSksIHtwYXJhbXM6IHtsZWZ0RXhwYW5kZWRXaWR0aDogREVGQVVMVF9OR0NfUEFORUxfQ09ORklHLmxlZnRQYW5lbEV4cGFuZGVkU2hpZnR9fSksXHJcbiAgICAgICAgICAgIHN0YXRlKCdjbG9zZUxlZnRIaWRkZW4nLCBzdHlsZSh7bGVmdDogMCwgdHJhbnNmb3JtOiAndHJhbnNsYXRlWCgtMTAwJSknfSkpLFxyXG4gICAgICAgICAgICBzdGF0ZSgnY2xvc2VMZWZ0Q29sbGFwc2VkJywgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgbGVmdDogMCxcclxuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoY2FsYygtMTAwJSArIHt7IGxlZnRDb2xsYXBzZWRXaWR0aCB9fXB4KSknXHJcbiAgICAgICAgICAgIH0pLCB7cGFyYW1zOiB7bGVmdENvbGxhcHNlZFdpZHRoOiBERUZBVUxUX05HQ19QQU5FTF9DT05GSUcubGVmdFBhbmVsQ29sbGFwc2VkU2hpZnR9fSksXHJcbiAgICAgICAgICAgIHN0YXRlKCdjbG9zZUxlZnRFeHBhbmRlZCcsIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgIGxlZnQ6IDAsXHJcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKGNhbGMoLTEwMCUgKyB7eyBsZWZ0RXhwYW5kZWRXaWR0aCB9fXB4KSknXHJcbiAgICAgICAgICAgIH0pLCB7cGFyYW1zOiB7bGVmdEV4cGFuZGVkV2lkdGg6IERFRkFVTFRfTkdDX1BBTkVMX0NPTkZJRy5sZWZ0UGFuZWxFeHBhbmRlZFNoaWZ0fX0pLFxyXG5cclxuICAgICAgICAgICAgc3RhdGUoJ29wZW5SaWdodEhpZGRlbicsIHN0eWxlKHtyaWdodDogMCwgdHJhbnNmb3JtOiAnbm9uZSd9KSksXHJcbiAgICAgICAgICAgIHN0YXRlKCdvcGVuUmlnaHRDb2xsYXBzZWQnLCBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICByaWdodDogMCxcclxuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoLXt7IHJpZ2h0Q29sbGFwc2VkV2lkdGggfX1weCknXHJcbiAgICAgICAgICAgIH0pLCB7cGFyYW1zOiB7cmlnaHRDb2xsYXBzZWRXaWR0aDogREVGQVVMVF9OR0NfUEFORUxfQ09ORklHLnJpZ2h0UGFuZWxDb2xsYXBzZWRTaGlmdH19KSxcclxuICAgICAgICAgICAgc3RhdGUoJ29wZW5SaWdodEV4cGFuZGVkJywgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgcmlnaHQ6IDAsXHJcbiAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKC17eyByaWdodEV4cGFuZGVkV2lkdGggfX1weCknXHJcbiAgICAgICAgICAgIH0pLCB7cGFyYW1zOiB7cmlnaHRFeHBhbmRlZFdpZHRoOiBERUZBVUxUX05HQ19QQU5FTF9DT05GSUcucmlnaHRQYW5lbEV4cGFuZGVkU2hpZnR9fSksXHJcbiAgICAgICAgICAgIHN0YXRlKCdjbG9zZVJpZ2h0SGlkZGVuJywgc3R5bGUoe3JpZ2h0OiAwLCB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDEwMCUpJ30pKSxcclxuICAgICAgICAgICAgc3RhdGUoJ2Nsb3NlUmlnaHRDb2xsYXBzZWQnLCBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICByaWdodDogMCxcclxuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoY2FsYygxMDAlIC0ge3sgcmlnaHRDb2xsYXBzZWRXaWR0aCB9fXB4KSknXHJcbiAgICAgICAgICAgIH0pLCB7cGFyYW1zOiB7cmlnaHRDb2xsYXBzZWRXaWR0aDogREVGQVVMVF9OR0NfUEFORUxfQ09ORklHLnJpZ2h0UGFuZWxDb2xsYXBzZWRTaGlmdH19KSxcclxuICAgICAgICAgICAgc3RhdGUoJ2Nsb3NlUmlnaHRFeHBhbmRlZCcsIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgIHJpZ2h0OiAwLFxyXG4gICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWChjYWxjKDEwMCUgLSB7eyByaWdodEV4cGFuZGVkV2lkdGggfX1weCkpJ1xyXG4gICAgICAgICAgICB9KSwge3BhcmFtczoge3JpZ2h0RXhwYW5kZWRXaWR0aDogREVGQVVMVF9OR0NfUEFORUxfQ09ORklHLnJpZ2h0UGFuZWxFeHBhbmRlZFNoaWZ0fX0pLFxyXG5cclxuICAgICAgICAgICAgdHJhbnNpdGlvbigndm9pZCA9PiAqJywgYW5pbWF0ZSgnMHMnKSksXHJcbiAgICAgICAgICAgIHRyYW5zaXRpb24oJyogPT4gKicsIGFuaW1hdGUoJzAuNHMgY3ViaWMtYmV6aWVyKDAuMDUsIDAuNzQsIDAuMiwgMC45OSknKSlcclxuICAgICAgICBdKVxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0UGFuZWxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgICBASW5wdXQoKSBpZDogc3RyaW5nO1xyXG4gICAgQElucHV0KCkgZGlyID0gJ2xlZnQnO1xyXG4gICAgcHJpdmF0ZSBfaXNPcGVuID0gZmFsc2U7XHJcbiAgICBzdGF0dXNlcyA9IHtcclxuICAgICAgICBsZWZ0OiB7XHJcbiAgICAgICAgICAgIGV4cGFuZGVkOiB7XHJcbiAgICAgICAgICAgICAgICBvcGVuOiAnb3BlbkxlZnRFeHBhbmRlZCcsXHJcbiAgICAgICAgICAgICAgICBjbG9zZTogJ2Nsb3NlTGVmdEV4cGFuZGVkJ1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBjb2xsYXBzZWQ6IHtcclxuICAgICAgICAgICAgICAgIG9wZW46ICdvcGVuTGVmdENvbGxhcHNlZCcsXHJcbiAgICAgICAgICAgICAgICBjbG9zZTogJ2Nsb3NlTGVmdENvbGxhcHNlZCdcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgaGlkZGVuOiB7XHJcbiAgICAgICAgICAgICAgICBvcGVuOiAnb3BlbkxlZnRIaWRkZW4nLFxyXG4gICAgICAgICAgICAgICAgY2xvc2U6ICdjbG9zZUxlZnRIaWRkZW4nXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHJpZ2h0OiB7XHJcbiAgICAgICAgICAgIGV4cGFuZGVkOiB7XHJcbiAgICAgICAgICAgICAgICBvcGVuOiAnb3BlblJpZ2h0RXhwYW5kZWQnLFxyXG4gICAgICAgICAgICAgICAgY2xvc2U6ICdjbG9zZVJpZ2h0RXhwYW5kZWQnXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGNvbGxhcHNlZDoge1xyXG4gICAgICAgICAgICAgICAgb3BlbjogJ29wZW5SaWdodENvbGxhcHNlZCcsXHJcbiAgICAgICAgICAgICAgICBjbG9zZTogJ2Nsb3NlUmlnaHRDb2xsYXBzZWQnXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGhpZGRlbjoge1xyXG4gICAgICAgICAgICAgICAgb3BlbjogJ29wZW5SaWdodEhpZGRlbicsXHJcbiAgICAgICAgICAgICAgICBjbG9zZTogJ2Nsb3NlUmlnaHRIaWRkZW4nXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9O1xyXG4gICAgb3BlblN0YXR1c2VzID0gW1xyXG4gICAgICAgIHRoaXMuc3RhdHVzZXMubGVmdC5leHBhbmRlZC5vcGVuLFxyXG4gICAgICAgIHRoaXMuc3RhdHVzZXMubGVmdC5jb2xsYXBzZWQub3BlbixcclxuICAgICAgICB0aGlzLnN0YXR1c2VzLmxlZnQuaGlkZGVuLm9wZW4sXHJcbiAgICAgICAgdGhpcy5zdGF0dXNlcy5yaWdodC5leHBhbmRlZC5vcGVuLFxyXG4gICAgICAgIHRoaXMuc3RhdHVzZXMucmlnaHQuY29sbGFwc2VkLm9wZW4sXHJcbiAgICAgICAgdGhpcy5zdGF0dXNlcy5yaWdodC5oaWRkZW4ub3BlbixcclxuICAgIF07XHJcbiAgICBjbG9zZVN0YXR1c2VzID0gW1xyXG4gICAgICAgIHRoaXMuc3RhdHVzZXMubGVmdC5leHBhbmRlZC5vcGVuLFxyXG4gICAgICAgIHRoaXMuc3RhdHVzZXMubGVmdC5jb2xsYXBzZWQub3BlbixcclxuICAgICAgICB0aGlzLnN0YXR1c2VzLmxlZnQuaGlkZGVuLm9wZW4sXHJcbiAgICAgICAgdGhpcy5zdGF0dXNlcy5yaWdodC5leHBhbmRlZC5vcGVuLFxyXG4gICAgICAgIHRoaXMuc3RhdHVzZXMucmlnaHQuY29sbGFwc2VkLm9wZW4sXHJcbiAgICAgICAgdGhpcy5zdGF0dXNlcy5yaWdodC5oaWRkZW4ub3BlbixcclxuICAgIF07XHJcbiAgICBzdWJzY3JpcHRpb25zOiBBcnJheTxTdWJzY3JpcHRpb24+ID0gW107XHJcbiAgICBvcGVuU3RhdGU6IHN0cmluZztcclxuICAgIGNsb3NlU3RhdGU6IHN0cmluZztcclxuICAgIHN0eWxlcyA9IHt9O1xyXG4gICAgZWxlbWVudDtcclxuICAgIGVsOiBFbGVtZW50UmVmO1xyXG4gICAgY29uZmlnOiBOZ3RQYW5lbENvbmZpZ0ludGVyZmFjZTtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgc2V0IGlzT3Blbih2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgICAgIHRoaXMuX2lzT3BlbiA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpc09wZW4oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzT3BlbjtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9wYW5lbFNlcnZpY2U6IE5ndFBhbmVsU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX2VsUmVmOiBFbGVtZW50UmVmLFxyXG4gICAgICAgICAgICAgICAgaW5qZWN0b3I6IEluamVjdG9yKSB7XHJcbiAgICAgICAgdGhpcy5jb25maWcgPSBPYmplY3QuYXNzaWduKHt9LCBERUZBVUxUX05HQ19QQU5FTF9DT05GSUcsIGluamVjdG9yLmdldChOR0NfUEFORUxfQ09ORklHKSk7XHJcbiAgICAgICAgdGhpcy5lbCA9IHRoaXMuX2VsUmVmO1xyXG4gICAgICAgIHRoaXMuZWxlbWVudCA9IHRoaXMuX2VsUmVmLm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgLy8gZGlyZWN0aW9uXHJcbiAgICAgICAgdGhpcy51cGRhdGVTdGF0ZXModGhpcy5kaXIpO1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKHRoaXMuX3BhbmVsU2VydmljZS5zdGF0ZUV2ZW50c1t0aGlzLmRpcl0uZXhwYW5kLnN1YnNjcmliZShfID0+IHRoaXMudXBkYXRlU3RhdGVzKHRoaXMuZGlyKSkpO1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKHRoaXMuX3BhbmVsU2VydmljZS5zdGF0ZUV2ZW50c1t0aGlzLmRpcl0uaGlkZS5zdWJzY3JpYmUoXyA9PiB0aGlzLnVwZGF0ZVN0YXRlcyh0aGlzLmRpcikpKTtcclxuXHJcbiAgICAgICAgLy8gZW5zdXJlIGlkIGF0dHJpYnV0ZSBleGlzdHNcclxuICAgICAgICBpZiAoIXRoaXMuaWQpIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcigncGFuZWwgbXVzdCBoYXZlIGFuIGlkJyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGFkZCBzZWxmICh0aGlzIHBhbmVsIGluc3RhbmNlKSB0byB0aGUgcGFuZWwgc2VydmljZSBzbyBpdCdzIGFjY2Vzc2libGUgZnJvbSBjb250cm9sbGVyc1xyXG4gICAgICAgIHRoaXMuX3BhbmVsU2VydmljZS5hZGQodGhpcyk7XHJcblxyXG4gICAgICAgIC8vIHN1YnNjcmliZSBldmVudHNcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaCh0aGlzLl9wYW5lbFNlcnZpY2UucGFuZWxXaWxsT3BlbmVkLnN1YnNjcmliZShcclxuICAgICAgICAgICAgKGlkOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChpZCA9PT0gdGhpcy5pZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3BlbigpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKHRoaXMuX3BhbmVsU2VydmljZS5wYW5lbFdpbGxDbG9zZWQuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAoaWQ6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGlkID09PSB0aGlzLmlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHJlbW92ZSBzZWxmIGZyb20gcGFuZWwgc2VydmljZSB3aGVuIGRpcmVjdGl2ZSBpcyBkZXN0cm95ZWRcclxuICAgIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX3BhbmVsU2VydmljZS5yZW1vdmUodGhpcy5pZCk7XHJcbiAgICAgICAgdGhpcy5fcGFuZWxTZXJ2aWNlLnJlbW92ZUZyb21BY3RpdmUodGhpcy5pZCk7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5lbGVtZW50KTtcclxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMuZm9yRWFjaCgoc3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb24pID0+IHtcclxuICAgICAgICAgICAgc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gb3BlbiBwYW5lbFxyXG4gICAgb3BlbigpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9wYW5lbFNlcnZpY2UuYWRkVG9BY3RpdmUodGhpcy5pZCk7XHJcbiAgICAgICAgdGhpcy5zZXRTdHlsZSh0aGlzLl9wYW5lbFNlcnZpY2UuYWN0aXZlUGFuZWxzLmxlbmd0aCk7XHJcbiAgICAgICAgdGhpcy5pc09wZW4gPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIGNsb3NlIHBhbmVsXHJcbiAgICBjbG9zZSgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9wYW5lbFNlcnZpY2UucmVtb3ZlRnJvbUFjdGl2ZSh0aGlzLmlkKTtcclxuICAgICAgICB0aGlzLmlzT3BlbiA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZVN0YXRlcyhkaXI6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMub3BlblN0YXRlID0gdGhpcy5zdGF0dXNlc1tkaXJdW3RoaXMuX3BhbmVsU2VydmljZS5wYW5lbFN0YXRlXS5vcGVuO1xyXG4gICAgICAgIHRoaXMuY2xvc2VTdGF0ZSA9IHRoaXMuc3RhdHVzZXNbZGlyXVt0aGlzLl9wYW5lbFNlcnZpY2UucGFuZWxTdGF0ZV0uY2xvc2U7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0U3R5bGUoaTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5zdHlsZXMgPSB7XHJcbiAgICAgICAgICAgICd6SW5kZXgnOiBpXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBhbmltYXRpb25BY3Rpb24oZXZlbnQpIHtcclxuICAgICAgICBzd2l0Y2ggKGV2ZW50LnBoYXNlTmFtZSkge1xyXG4gICAgICAgICAgICBjYXNlICdzdGFydCc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wZW5TdGF0dXNlcy5pbmNsdWRlcyhldmVudC50b1N0YXRlKSAmJiB0aGlzLl9wYW5lbFNlcnZpY2UucGFuZWxPcGVuaW5nRGlkU3RhcnQubmV4dCgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jbG9zZVN0YXR1c2VzLmluY2x1ZGVzKGV2ZW50LnRvU3RhdGUpICYmIHRoaXMuX3BhbmVsU2VydmljZS5wYW5lbENsb3NpbmdEaWRTdGFydC5uZXh0KCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnZG9uZSc6XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wZW5TdGF0dXNlcy5pbmNsdWRlcyhldmVudC50b1N0YXRlKSAmJiB0aGlzLl9wYW5lbFNlcnZpY2UucGFuZWxPcGVuaW5nRGlkRG9uZS5uZXh0KCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNsb3NlU3RhdHVzZXMuaW5jbHVkZXMoZXZlbnQudG9TdGF0ZSkgJiYgdGhpcy5fcGFuZWxTZXJ2aWNlLnBhbmVsQ2xvc2luZ0RpZERvbmUubmV4dCgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==