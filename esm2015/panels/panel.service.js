/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import * as _ from 'lodash';
import * as i0 from "@angular/core";
export class NgtPanelService {
    constructor() {
        this.panels = [];
        this.activePanels = [];
        this.panelWillOpened = new Subject();
        this.panelWillClosed = new Subject();
        this.panelClosingDidStart = new Subject();
        this.panelClosingDidDone = new Subject();
        this.panelOpeningDidStart = new Subject();
        this.panelOpeningDidDone = new Subject();
        this.isPanelsChanged = new Subject();
        this.isPanelHide = false;
        this.isPanelExpand = true;
        this.panelState = 'expanded';
        this.stateEvents = {
            left: {
                expand: new Subject(),
                hide: new Subject()
            },
            right: {
                expand: new Subject(),
                hide: new Subject()
            }
        };
        // Subscribe state events and update state
        this.stateEvents.left.expand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.isPanelExpand = status;
            this.updateState();
        }));
        this.stateEvents.left.hide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.isPanelHide = status;
            this.updateState();
        }));
        this.stateEvents.right.expand.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.isPanelExpand = status;
            this.updateState();
        }));
        this.stateEvents.right.hide.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.isPanelHide = status;
            this.updateState();
        }));
    }
    /**
     * @param {?} panel
     * @return {?}
     */
    add(panel) {
        // add panel to array of active panels-page
        this.panels.push(panel);
        this.isPanelsChanged.next(this.panels.slice());
    }
    /**
     * @param {?} id
     * @return {?}
     */
    remove(id) {
        // remove panel from array of active panels-page
        /** @type {?} */
        const panelToRemove = _.find(this.panels, { id: id });
        this.panels = _.without(this.panels, panelToRemove);
        this.isPanelsChanged.next(this.panels.slice());
    }
    /**
     * @param {?} id
     * @return {?}
     */
    addToActive(id) {
        this.activePanels.push(id);
    }
    /**
     * @param {?} id
     * @return {?}
     */
    removeFromActive(id) {
        /** @type {?} */
        const index = this.activePanels.indexOf(id);
        if (index !== -1) {
            this.activePanels.splice(index, 1);
        }
    }
    /**
     * @param {?} id
     * @return {?}
     */
    openPanel(id) {
        this.panelWillOpened.next(id);
    }
    /**
     * @param {?=} id
     * @return {?}
     */
    closePanel(id) {
        this.panelWillClosed.next(id || this.activePanels[this.activePanels.length - 1]);
    }
    /**
     * @return {?}
     */
    updateState() {
        this.panelState = this.isPanelHide ? 'hidden' : (this.isPanelExpand ? 'expanded' : 'collapsed');
    }
}
NgtPanelService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NgtPanelService.ctorParameters = () => [];
/** @nocollapse */ NgtPanelService.ngInjectableDef = i0.defineInjectable({ factory: function NgtPanelService_Factory() { return new NgtPanelService(); }, token: NgtPanelService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtPanelService.prototype.panels;
    /** @type {?} */
    NgtPanelService.prototype.activePanels;
    /** @type {?} */
    NgtPanelService.prototype.panelWillOpened;
    /** @type {?} */
    NgtPanelService.prototype.panelWillClosed;
    /** @type {?} */
    NgtPanelService.prototype.panelClosingDidStart;
    /** @type {?} */
    NgtPanelService.prototype.panelClosingDidDone;
    /** @type {?} */
    NgtPanelService.prototype.panelOpeningDidStart;
    /** @type {?} */
    NgtPanelService.prototype.panelOpeningDidDone;
    /** @type {?} */
    NgtPanelService.prototype.isPanelsChanged;
    /** @type {?} */
    NgtPanelService.prototype.isPanelHide;
    /** @type {?} */
    NgtPanelService.prototype.isPanelExpand;
    /** @type {?} */
    NgtPanelService.prototype.panelState;
    /** @type {?} */
    NgtPanelService.prototype.stateEvents;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsicGFuZWxzL3BhbmVsLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMvQixPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQzs7QUFPNUIsTUFBTSxPQUFPLGVBQWU7SUF3QnhCO1FBdkJRLFdBQU0sR0FBVSxFQUFFLENBQUM7UUFDM0IsaUJBQVksR0FBVSxFQUFFLENBQUM7UUFDekIsb0JBQWUsR0FBRyxJQUFJLE9BQU8sRUFBVSxDQUFDO1FBQ3hDLG9CQUFlLEdBQUcsSUFBSSxPQUFPLEVBQVUsQ0FBQztRQUN4Qyx5QkFBb0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3JDLHdCQUFtQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDcEMseUJBQW9CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNyQyx3QkFBbUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3BDLG9CQUFlLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNoQyxnQkFBVyxHQUFZLEtBQUssQ0FBQztRQUM3QixrQkFBYSxHQUFZLElBQUksQ0FBQztRQUM5QixlQUFVLEdBQUcsVUFBVSxDQUFDO1FBQ3hCLGdCQUFXLEdBQUc7WUFDVixJQUFJLEVBQUU7Z0JBQ0YsTUFBTSxFQUFFLElBQUksT0FBTyxFQUFXO2dCQUM5QixJQUFJLEVBQUUsSUFBSSxPQUFPLEVBQVc7YUFDL0I7WUFDRCxLQUFLLEVBQUU7Z0JBQ0gsTUFBTSxFQUFFLElBQUksT0FBTyxFQUFXO2dCQUM5QixJQUFJLEVBQUUsSUFBSSxPQUFPLEVBQVc7YUFDL0I7U0FDSixDQUFDO1FBR0UsMENBQTBDO1FBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUU7WUFDNUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7WUFDNUIsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVM7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUMxQyxJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQztZQUMxQixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDdkIsQ0FBQyxFQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUzs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQzdDLElBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDO1lBQzVCLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN2QixDQUFDLEVBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUU7WUFDM0MsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7WUFDMUIsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxHQUFHLENBQUMsS0FBZTtRQUNmLDJDQUEyQztRQUMzQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsRUFBVTs7O2NBRVAsYUFBYSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxhQUFhLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsRUFBVTtRQUNsQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUMvQixDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLEVBQVU7O2NBQ2pCLEtBQUssR0FBVyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUM7UUFDbkQsSUFBSSxLQUFLLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDZCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDdEM7SUFDTCxDQUFDOzs7OztJQUVELFNBQVMsQ0FBQyxFQUFVO1FBQ2hCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLEVBQVc7UUFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNyRixDQUFDOzs7O0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDcEcsQ0FBQzs7O1lBakZKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7Ozs7Ozs7OztJQUVHLGlDQUEyQjs7SUFDM0IsdUNBQXlCOztJQUN6QiwwQ0FBd0M7O0lBQ3hDLDBDQUF3Qzs7SUFDeEMsK0NBQXFDOztJQUNyQyw4Q0FBb0M7O0lBQ3BDLCtDQUFxQzs7SUFDckMsOENBQW9DOztJQUNwQywwQ0FBZ0M7O0lBQ2hDLHNDQUE2Qjs7SUFDN0Isd0NBQThCOztJQUM5QixxQ0FBd0I7O0lBQ3hCLHNDQVNFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuXHJcbmltcG9ydCB7IE5ndFBhbmVsIH0gZnJvbSAnLi9wYW5lbC5tb2RlbCc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndFBhbmVsU2VydmljZSB7XHJcbiAgICBwcml2YXRlIHBhbmVsczogYW55W10gPSBbXTtcclxuICAgIGFjdGl2ZVBhbmVsczogYW55W10gPSBbXTtcclxuICAgIHBhbmVsV2lsbE9wZW5lZCA9IG5ldyBTdWJqZWN0PHN0cmluZz4oKTtcclxuICAgIHBhbmVsV2lsbENsb3NlZCA9IG5ldyBTdWJqZWN0PHN0cmluZz4oKTtcclxuICAgIHBhbmVsQ2xvc2luZ0RpZFN0YXJ0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIHBhbmVsQ2xvc2luZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgcGFuZWxPcGVuaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgcGFuZWxPcGVuaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBpc1BhbmVsc0NoYW5nZWQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgaXNQYW5lbEhpZGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGlzUGFuZWxFeHBhbmQ6IGJvb2xlYW4gPSB0cnVlO1xyXG4gICAgcGFuZWxTdGF0ZSA9ICdleHBhbmRlZCc7XHJcbiAgICBzdGF0ZUV2ZW50cyA9IHtcclxuICAgICAgICBsZWZ0OiB7XHJcbiAgICAgICAgICAgIGV4cGFuZDogbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKSxcclxuICAgICAgICAgICAgaGlkZTogbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgcmlnaHQ6IHtcclxuICAgICAgICAgICAgZXhwYW5kOiBuZXcgU3ViamVjdDxib29sZWFuPigpLFxyXG4gICAgICAgICAgICBoaWRlOiBuZXcgU3ViamVjdDxib29sZWFuPigpXHJcbiAgICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICAvLyBTdWJzY3JpYmUgc3RhdGUgZXZlbnRzIGFuZCB1cGRhdGUgc3RhdGVcclxuICAgICAgICB0aGlzLnN0YXRlRXZlbnRzLmxlZnQuZXhwYW5kLnN1YnNjcmliZShzdGF0dXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmlzUGFuZWxFeHBhbmQgPSBzdGF0dXM7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlU3RhdGUoKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN0YXRlRXZlbnRzLmxlZnQuaGlkZS5zdWJzY3JpYmUoc3RhdHVzID0+IHtcclxuICAgICAgICAgICAgdGhpcy5pc1BhbmVsSGlkZSA9IHN0YXR1cztcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVTdGF0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuc3RhdGVFdmVudHMucmlnaHQuZXhwYW5kLnN1YnNjcmliZShzdGF0dXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmlzUGFuZWxFeHBhbmQgPSBzdGF0dXM7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlU3RhdGUoKTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLnN0YXRlRXZlbnRzLnJpZ2h0LmhpZGUuc3Vic2NyaWJlKHN0YXR1cyA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNQYW5lbEhpZGUgPSBzdGF0dXM7XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlU3RhdGUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBhZGQocGFuZWw6IE5ndFBhbmVsKSB7XHJcbiAgICAgICAgLy8gYWRkIHBhbmVsIHRvIGFycmF5IG9mIGFjdGl2ZSBwYW5lbHMtcGFnZVxyXG4gICAgICAgIHRoaXMucGFuZWxzLnB1c2gocGFuZWwpO1xyXG4gICAgICAgIHRoaXMuaXNQYW5lbHNDaGFuZ2VkLm5leHQodGhpcy5wYW5lbHMuc2xpY2UoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVtb3ZlKGlkOiBzdHJpbmcpIHtcclxuICAgICAgICAvLyByZW1vdmUgcGFuZWwgZnJvbSBhcnJheSBvZiBhY3RpdmUgcGFuZWxzLXBhZ2VcclxuICAgICAgICBjb25zdCBwYW5lbFRvUmVtb3ZlID0gXy5maW5kKHRoaXMucGFuZWxzLCB7aWQ6IGlkfSk7XHJcbiAgICAgICAgdGhpcy5wYW5lbHMgPSBfLndpdGhvdXQodGhpcy5wYW5lbHMsIHBhbmVsVG9SZW1vdmUpO1xyXG4gICAgICAgIHRoaXMuaXNQYW5lbHNDaGFuZ2VkLm5leHQodGhpcy5wYW5lbHMuc2xpY2UoKSk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkVG9BY3RpdmUoaWQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuYWN0aXZlUGFuZWxzLnB1c2goaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZUZyb21BY3RpdmUoaWQ6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IGluZGV4OiBudW1iZXIgPSB0aGlzLmFjdGl2ZVBhbmVscy5pbmRleE9mKGlkKTtcclxuICAgICAgICBpZiAoaW5kZXggIT09IC0xKSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWN0aXZlUGFuZWxzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9wZW5QYW5lbChpZDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5wYW5lbFdpbGxPcGVuZWQubmV4dChpZCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2xvc2VQYW5lbChpZD86IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMucGFuZWxXaWxsQ2xvc2VkLm5leHQoaWQgfHwgdGhpcy5hY3RpdmVQYW5lbHNbdGhpcy5hY3RpdmVQYW5lbHMubGVuZ3RoIC0gMV0pO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZVN0YXRlKCkge1xyXG4gICAgICAgIHRoaXMucGFuZWxTdGF0ZSA9IHRoaXMuaXNQYW5lbEhpZGUgPyAnaGlkZGVuJyA6ICh0aGlzLmlzUGFuZWxFeHBhbmQgPyAnZXhwYW5kZWQnIDogJ2NvbGxhcHNlZCcpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==