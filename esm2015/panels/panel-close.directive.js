/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtPanelService } from './panel.service';
export class NgtPanelCloseDirective {
    /**
     * @param {?} _panelService
     */
    constructor(_panelService) {
        this._panelService = _panelService;
    }
    /**
     * @param {?} e
     * @return {?}
     */
    onClick(e) {
        e.stopPropagation();
        this._panelService.closePanel(this.id);
    }
}
NgtPanelCloseDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtPanelClose]'
            },] }
];
/** @nocollapse */
NgtPanelCloseDirective.ctorParameters = () => [
    { type: NgtPanelService }
];
NgtPanelCloseDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtPanelClose',] }],
    onClick: [{ type: HostListener, args: ['click', ['$event'],] }]
};
if (false) {
    /** @type {?} */
    NgtPanelCloseDirective.prototype.id;
    /**
     * @type {?}
     * @private
     */
    NgtPanelCloseDirective.prototype._panelService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtY2xvc2UuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJwYW5lbHMvcGFuZWwtY2xvc2UuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBS2xELE1BQU0sT0FBTyxzQkFBc0I7Ozs7SUFHL0IsWUFBb0IsYUFBOEI7UUFBOUIsa0JBQWEsR0FBYixhQUFhLENBQWlCO0lBQ2xELENBQUM7Ozs7O0lBR0QsT0FBTyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzNDLENBQUM7OztZQWJKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsaUJBQWlCO2FBQzlCOzs7O1lBSlEsZUFBZTs7O2lCQU1uQixLQUFLLFNBQUMsZUFBZTtzQkFLckIsWUFBWSxTQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQzs7OztJQUxqQyxvQ0FBbUM7Ozs7O0lBRXZCLCtDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTmd0UGFuZWxTZXJ2aWNlIH0gZnJvbSAnLi9wYW5lbC5zZXJ2aWNlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbbmd0UGFuZWxDbG9zZV0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYW5lbENsb3NlRGlyZWN0aXZlIHtcclxuICAgIEBJbnB1dCgnbmd0UGFuZWxDbG9zZScpIGlkOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfcGFuZWxTZXJ2aWNlOiBOZ3RQYW5lbFNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdjbGljaycsIFsnJGV2ZW50J10pXHJcbiAgICBvbkNsaWNrKGUpIHtcclxuICAgICAgICBlLnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgIHRoaXMuX3BhbmVsU2VydmljZS5jbG9zZVBhbmVsKHRoaXMuaWQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==