/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtPanelService } from './panel.service';
export class NgtPanelOpenDirective {
    /**
     * @param {?} _panelService
     */
    constructor(_panelService) {
        this._panelService = _panelService;
    }
    /**
     * @param {?} e
     * @return {?}
     */
    onClick(e) {
        e.stopPropagation();
        this._panelService.openPanel(this.id);
    }
}
NgtPanelOpenDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtPanelOpen]'
            },] }
];
/** @nocollapse */
NgtPanelOpenDirective.ctorParameters = () => [
    { type: NgtPanelService }
];
NgtPanelOpenDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtPanelOpen',] }],
    onClick: [{ type: HostListener, args: ['click', ['$event'],] }]
};
if (false) {
    /** @type {?} */
    NgtPanelOpenDirective.prototype.id;
    /**
     * @type {?}
     * @private
     */
    NgtPanelOpenDirective.prototype._panelService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFuZWwtb3Blbi5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInBhbmVscy9wYW5lbC1vcGVuLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9ELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUtsRCxNQUFNLE9BQU8scUJBQXFCOzs7O0lBRzlCLFlBQW9CLGFBQThCO1FBQTlCLGtCQUFhLEdBQWIsYUFBYSxDQUFpQjtJQUNsRCxDQUFDOzs7OztJQUdELE9BQU8sQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7WUFiSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGdCQUFnQjthQUM3Qjs7OztZQUpRLGVBQWU7OztpQkFNbkIsS0FBSyxTQUFDLGNBQWM7c0JBS3BCLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7Ozs7SUFMakMsbUNBQWtDOzs7OztJQUV0Qiw4Q0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IE5ndFBhbmVsU2VydmljZSB9IGZyb20gJy4vcGFuZWwuc2VydmljZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndFBhbmVsT3Blbl0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYW5lbE9wZW5EaXJlY3RpdmUge1xyXG4gICAgQElucHV0KCduZ3RQYW5lbE9wZW4nKSBpZDogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX3BhbmVsU2VydmljZTogTmd0UGFuZWxTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snLCBbJyRldmVudCddKVxyXG4gICAgb25DbGljayhlKSB7XHJcbiAgICAgICAgZS5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB0aGlzLl9wYW5lbFNlcnZpY2Uub3BlblBhbmVsKHRoaXMuaWQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==