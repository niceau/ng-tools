/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, HostBinding } from '@angular/core';
import { NgtDropdownService } from './dropdown.service';
/**
 * Marks an element to which dropdown menu will be anchored. This is a simple version
 * of the NgtDropdownToggleDirective directive. It plays the same role as NgtDropdownToggleDirective but
 * doesn't listen to click events to toggle dropdown menu thus enabling support for
 * events other than click.
 *
 * \@since 1.1.0
 */
export class NgtDropdownAnchorDirective {
    /**
     * @param {?} _elementRef
     * @param {?} $dropdownService
     */
    constructor(_elementRef, $dropdownService) {
        this._elementRef = _elementRef;
        this.$dropdownService = $dropdownService;
        this.class = true;
        this.ariaHaspopup = true;
        this.ariaExpanded = this.$dropdownService.isOpen();
        this.anchorEl = _elementRef.nativeElement;
    }
    /**
     * @return {?}
     */
    getNativeElement() {
        return this._elementRef.nativeElement;
    }
}
NgtDropdownAnchorDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdownAnchor]'
            },] }
];
/** @nocollapse */
NgtDropdownAnchorDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: NgtDropdownService }
];
NgtDropdownAnchorDirective.propDecorators = {
    class: [{ type: HostBinding, args: ['class.dropdown-toggle',] }],
    ariaHaspopup: [{ type: HostBinding, args: ['attr.aria-haspopup.true',] }],
    ariaExpanded: [{ type: HostBinding, args: ['attr.aria-expanded',] }]
};
if (false) {
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.anchorEl;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.class;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.ariaHaspopup;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.ariaExpanded;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownAnchorDirective.prototype._elementRef;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.$dropdownService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tYW5jaG9yLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiZHJvcGRvd24vZHJvcGRvd24tYW5jaG9yLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG9CQUFvQixDQUFDOzs7Ozs7Ozs7QUFheEQsTUFBTSxPQUFPLDBCQUEwQjs7Ozs7SUFPbkMsWUFBb0IsV0FBb0MsRUFDckMsZ0JBQW9DO1FBRG5DLGdCQUFXLEdBQVgsV0FBVyxDQUF5QjtRQUNyQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQW9CO1FBTGpCLFVBQUssR0FBRyxJQUFJLENBQUM7UUFDWCxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUN6QixpQkFBWSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUk3RSxJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxhQUFhLENBQUM7SUFDOUMsQ0FBQzs7OztJQUVELGdCQUFnQjtRQUNaLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7SUFDMUMsQ0FBQzs7O1lBakJKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUscUJBQXFCO2FBQ2xDOzs7O1lBYm1CLFVBQVU7WUFDckIsa0JBQWtCOzs7b0JBZ0J0QixXQUFXLFNBQUMsdUJBQXVCOzJCQUNuQyxXQUFXLFNBQUMseUJBQXlCOzJCQUNyQyxXQUFXLFNBQUMsb0JBQW9COzs7O0lBSmpDLDhDQUFTOztJQUVULDJDQUFtRDs7SUFDbkQsa0RBQTREOztJQUM1RCxrREFBaUY7Ozs7O0lBRXJFLGlEQUE0Qzs7SUFDNUMsc0RBQTJDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBFbGVtZW50UmVmLCBIb3N0QmluZGluZyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOZ3REcm9wZG93blNlcnZpY2UgfSBmcm9tICcuL2Ryb3Bkb3duLnNlcnZpY2UnO1xyXG5cclxuLyoqXHJcbiAqIE1hcmtzIGFuIGVsZW1lbnQgdG8gd2hpY2ggZHJvcGRvd24gbWVudSB3aWxsIGJlIGFuY2hvcmVkLiBUaGlzIGlzIGEgc2ltcGxlIHZlcnNpb25cclxuICogb2YgdGhlIE5ndERyb3Bkb3duVG9nZ2xlRGlyZWN0aXZlIGRpcmVjdGl2ZS4gSXQgcGxheXMgdGhlIHNhbWUgcm9sZSBhcyBOZ3REcm9wZG93blRvZ2dsZURpcmVjdGl2ZSBidXRcclxuICogZG9lc24ndCBsaXN0ZW4gdG8gY2xpY2sgZXZlbnRzIHRvIHRvZ2dsZSBkcm9wZG93biBtZW51IHRodXMgZW5hYmxpbmcgc3VwcG9ydCBmb3JcclxuICogZXZlbnRzIG90aGVyIHRoYW4gY2xpY2suXHJcbiAqXHJcbiAqIEBzaW5jZSAxLjEuMFxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3REcm9wZG93bkFuY2hvcl0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3REcm9wZG93bkFuY2hvckRpcmVjdGl2ZSB7XHJcbiAgICBhbmNob3JFbDtcclxuXHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLmRyb3Bkb3duLXRvZ2dsZScpIGNsYXNzID0gdHJ1ZTtcclxuICAgIEBIb3N0QmluZGluZygnYXR0ci5hcmlhLWhhc3BvcHVwLnRydWUnKSBhcmlhSGFzcG9wdXAgPSB0cnVlO1xyXG4gICAgQEhvc3RCaW5kaW5nKCdhdHRyLmFyaWEtZXhwYW5kZWQnKSBhcmlhRXhwYW5kZWQgPSB0aGlzLiRkcm9wZG93blNlcnZpY2UuaXNPcGVuKCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBfZWxlbWVudFJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4sXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgJGRyb3Bkb3duU2VydmljZTogTmd0RHJvcGRvd25TZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5hbmNob3JFbCA9IF9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TmF0aXZlRWxlbWVudCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==