/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ChangeDetectorRef, ContentChild, ContentChildren, Directive, ElementRef, EventEmitter, forwardRef, HostBinding, HostListener, Inject, Input, NgZone, Output, QueryList, Renderer2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Subject } from 'rxjs';
import { positionElements } from '../util/positioning';
import { ngtAutoClose } from '../util/autoclose';
import { Key } from '../util/key';
import { NgtDropdownConfig } from './dropdown-config';
/**
 * A directive you should put put on a dropdown item to enable keyboard navigation.
 * Keyboard navigation using arrow keys will move focus between items marked with this directive.
 */
export class NgtDropdownItemDirective {
    /**
     * @param {?} elementRef
     */
    constructor(elementRef) {
        this.elementRef = elementRef;
        this._disabled = false;
        this.class = true;
        this.classDisabled = this.disabled;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set disabled(value) {
        this._disabled = (/** @type {?} */ (value)) === '' || value === true; // accept an empty attribute as true
        this.classDisabled = this._disabled;
    }
    /**
     * @return {?}
     */
    get disabled() {
        return this._disabled;
    }
}
NgtDropdownItemDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdownItem]'
            },] }
];
/** @nocollapse */
NgtDropdownItemDirective.ctorParameters = () => [
    { type: ElementRef }
];
NgtDropdownItemDirective.propDecorators = {
    class: [{ type: HostBinding, args: ['class.dropdown-item',] }],
    classDisabled: [{ type: HostBinding, args: ['class.disabled',] }],
    disabled: [{ type: Input }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtDropdownItemDirective.prototype._disabled;
    /** @type {?} */
    NgtDropdownItemDirective.prototype.class;
    /** @type {?} */
    NgtDropdownItemDirective.prototype.classDisabled;
    /** @type {?} */
    NgtDropdownItemDirective.prototype.elementRef;
}
/**
 *
 */
export class NgtDropdownMenuDirective {
    /**
     * @param {?} dropdown
     * @param {?} _elementRef
     * @param {?} _renderer
     */
    constructor(dropdown, _elementRef, _renderer) {
        this.dropdown = dropdown;
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this.placement = 'bottom';
        this.isOpen = false;
        this.dropdownMenu = true;
        this.show = this.dropdown.isOpen();
        this.xPlacement = this.placement;
        this.dropdown.openChange.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.show = status;
        }));
    }
    /**
     * @return {?}
     */
    getNativeElement() {
        return this._elementRef.nativeElement;
    }
    /**
     * @param {?} triggerEl
     * @param {?} placement
     * @return {?}
     */
    position(triggerEl, placement) {
        this.applyPlacement(positionElements(triggerEl, this._elementRef.nativeElement, placement));
    }
    /**
     * @param {?} _placement
     * @return {?}
     */
    applyPlacement(_placement) {
        // remove the current placement classes
        this._renderer.removeClass(this._elementRef.nativeElement.parentNode, 'dropup');
        this._renderer.removeClass(this._elementRef.nativeElement.parentNode, 'dropdown');
        this.placement = _placement;
        /**
         * apply the new placement
         * in case of top use up-arrow or down-arrow otherwise
         */
        if (_placement.search('^top') !== -1) {
            this._renderer.addClass(this._elementRef.nativeElement.parentNode, 'dropup');
        }
        else {
            this._renderer.addClass(this._elementRef.nativeElement.parentNode, 'dropdown');
        }
    }
}
NgtDropdownMenuDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdownMenu]'
            },] }
];
/** @nocollapse */
NgtDropdownMenuDirective.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [forwardRef((/**
                     * @return {?}
                     */
                    () => NgtDropdownDirective)),] }] },
    { type: ElementRef },
    { type: Renderer2 }
];
NgtDropdownMenuDirective.propDecorators = {
    menuItems: [{ type: ContentChildren, args: [NgtDropdownItemDirective,] }],
    dropdownMenu: [{ type: HostBinding, args: ['class.dropdown-menu',] }],
    show: [{ type: HostBinding, args: ['class.show',] }],
    xPlacement: [{ type: HostBinding, args: ['attr.x-placement',] }]
};
if (false) {
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.placement;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.isOpen;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.menuItems;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.dropdownMenu;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.show;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.xPlacement;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.dropdown;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownMenuDirective.prototype._elementRef;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownMenuDirective.prototype._renderer;
}
/**
 * Marks an element to which dropdown menu will be anchored. This is a simple version
 * of the NgtDropdownToggleDirective. It plays the same role as NgtDropdownToggleDirective but
 * doesn't listen to click events to toggle dropdown menu thus enabling support for
 * events other than click.
 */
export class NgtDropdownAnchorDirective {
    /**
     * @param {?} dropdown
     * @param {?} _elementRef
     */
    constructor(dropdown, _elementRef) {
        this.dropdown = dropdown;
        this._elementRef = _elementRef;
        this.class = true;
        this.ariaHaspopup = true;
        this.ariaExpanded = this.dropdown.isOpen();
        this.anchorEl = _elementRef.nativeElement;
    }
    /**
     * @return {?}
     */
    getNativeElement() {
        return this._elementRef.nativeElement;
    }
}
NgtDropdownAnchorDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdownAnchor]'
            },] }
];
/** @nocollapse */
NgtDropdownAnchorDirective.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [forwardRef((/**
                     * @return {?}
                     */
                    () => NgtDropdownDirective)),] }] },
    { type: ElementRef }
];
NgtDropdownAnchorDirective.propDecorators = {
    class: [{ type: HostBinding, args: ['class.dropdown-toggle',] }],
    ariaHaspopup: [{ type: HostBinding, args: ['attr.aria-haspopup.true',] }],
    ariaExpanded: [{ type: HostBinding, args: ['attr.aria-expanded',] }]
};
if (false) {
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.anchorEl;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.class;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.ariaHaspopup;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.ariaExpanded;
    /** @type {?} */
    NgtDropdownAnchorDirective.prototype.dropdown;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownAnchorDirective.prototype._elementRef;
}
/**
 * Allows the dropdown to be toggled via click. This directive is optional: you can use NgtDropdownAnchorDirective as an
 * alternative.
 */
export class NgtDropdownToggleDirective extends NgtDropdownAnchorDirective {
    /**
     * @param {?} dropdown
     * @param {?} elementRef
     */
    constructor(dropdown, elementRef) {
        super(dropdown, elementRef);
        this.class = true;
        this.ariaHaspopup = true;
        this.ariaExpanded = this.dropdown.isOpen();
    }
    /**
     * @return {?}
     */
    onClick() {
        this.toggleOpen();
    }
    /**
     * @return {?}
     */
    toggleOpen() {
        this.dropdown.toggle();
    }
}
NgtDropdownToggleDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdownToggle]',
                providers: [{ provide: NgtDropdownAnchorDirective, useExisting: forwardRef((/**
                         * @return {?}
                         */
                        () => NgtDropdownToggleDirective)) }]
            },] }
];
/** @nocollapse */
NgtDropdownToggleDirective.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [forwardRef((/**
                     * @return {?}
                     */
                    () => NgtDropdownDirective)),] }] },
    { type: ElementRef }
];
NgtDropdownToggleDirective.propDecorators = {
    class: [{ type: HostBinding, args: ['class.dropdown-toggle',] }],
    ariaHaspopup: [{ type: HostBinding, args: ['attr.aria-haspopup.true',] }],
    ariaExpanded: [{ type: HostBinding, args: ['attr.aria-expanded',] }],
    onClick: [{ type: HostListener, args: ['click',] }]
};
if (false) {
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.class;
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.ariaHaspopup;
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.ariaExpanded;
}
/**
 * Transforms a node into a dropdown.
 */
export class NgtDropdownDirective {
    /**
     * @param {?} _changeDetector
     * @param {?} config
     * @param {?} _document
     * @param {?} _ngZone
     * @param {?} _elementRef
     * @param {?} _renderer
     */
    constructor(_changeDetector, config, _document, _ngZone, _elementRef, _renderer) {
        this._changeDetector = _changeDetector;
        this._document = _document;
        this._ngZone = _ngZone;
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this._closed$ = new Subject();
        /**
         *  Defines whether or not the dropdown-menu is open initially.
         */
        this._open = false;
        /**
         *  An event fired when the dropdown is opened or closed.
         *  Event's payload equals whether dropdown is open.
         */
        this.openChange = new EventEmitter();
        this.show = this.isOpen();
        this.placement = config.placement;
        this.container = config.container;
        this.autoClose = config.autoClose;
        this._zoneSubscription = _ngZone.onStable.subscribe((/**
         * @return {?}
         */
        () => {
            this._positionMenu();
        }));
        this.openChange.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.show = status;
        }));
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onArrowUp($event) {
        this.onKeyDown($event);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onArrowArrowDown($event) {
        this.onKeyDown($event);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onArrowHome($event) {
        this.onKeyDown($event);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onArrowEnd($event) {
        this.onKeyDown($event);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this._applyPlacementClasses();
        if (this._open) {
            this._setCloseHandlers();
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.container && this._open) {
            this._applyContainer(this.container);
        }
        if (changes.placement && !changes.placement.isFirstChange) {
            this._applyPlacementClasses();
        }
    }
    /**
     * Checks if the dropdown menu is open or not.
     * @return {?}
     */
    isOpen() {
        return this._open;
    }
    /**
     * Opens the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    open() {
        if (!this._open) {
            this._open = true;
            this._applyContainer(this.container);
            this._positionMenu();
            this.openChange.emit(true);
            this._setCloseHandlers();
        }
    }
    /**
     * @private
     * @return {?}
     */
    _setCloseHandlers() {
        ngtAutoClose(this._ngZone, this._document, this.autoClose, (/**
         * @return {?}
         */
        () => this.close()), this._closed$, this._menu ? [this._menu.getNativeElement()] : [], this._anchor ? [this._anchor.getNativeElement()] : []);
    }
    /**
     * Closes the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    close() {
        if (this._open) {
            this._open = false;
            this._resetContainer();
            this._closed$.next();
            this.openChange.emit(false);
            this._changeDetector.markForCheck();
        }
    }
    /**
     * Toggles the dropdown menu of a given navbar or tabbed navigation.
     * @return {?}
     */
    toggle() {
        if (this.isOpen()) {
            this.close();
        }
        else {
            this.open();
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this._resetContainer();
        this._closed$.next();
        this._zoneSubscription.unsubscribe();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyDown(event) {
        /** @type {?} */
        const itemElements = this._getMenuElements();
        if (!itemElements.length) {
            return false;
        }
        /** @type {?} */
        let position = -1;
        /** @type {?} */
        let isEventFromItems = false;
        /** @type {?} */
        const isEventFromToggle = this._isEventFromToggle(event);
        if (!isEventFromToggle) {
            itemElements.forEach((/**
             * @param {?} itemElement
             * @param {?} index
             * @return {?}
             */
            (itemElement, index) => {
                if (itemElement.contains((/** @type {?} */ (event.target)))) {
                    isEventFromItems = true;
                }
                if (itemElement === this._document.activeElement) {
                    position = index;
                }
            }));
        }
        if (isEventFromToggle || isEventFromItems) {
            if (!this.isOpen()) {
                this.open();
            }
            // tslint:disable-next-line:deprecation
            switch (event.which) {
                case Key.ArrowDown:
                    position = Math.min(position + 1, itemElements.length - 1);
                    break;
                case Key.ArrowUp:
                    if (this._isDropup() && position === -1) {
                        position = itemElements.length - 1;
                        break;
                    }
                    position = Math.max(position - 1, 0);
                    break;
                case Key.Home:
                    position = 0;
                    break;
                case Key.End:
                    position = itemElements.length - 1;
                    break;
            }
            itemElements[position].focus();
            event.preventDefault();
        }
    }
    /**
     * @private
     * @return {?}
     */
    _isDropup() {
        return this._elementRef.nativeElement.classList.contains('dropup');
    }
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    _isEventFromToggle(event) {
        return this._anchor.getNativeElement().contains((/** @type {?} */ (event.target)));
    }
    /**
     * @private
     * @return {?}
     */
    _getMenuElements() {
        if (this._menu == null) {
            return [];
        }
        return this._menu.menuItems.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => !item.disabled)).map((/**
         * @param {?} item
         * @return {?}
         */
        item => item.elementRef.nativeElement));
    }
    /**
     * @private
     * @return {?}
     */
    _positionMenu() {
        if (this.isOpen() && this._menu) {
            this._applyPlacementClasses(positionElements(this._anchor.anchorEl, this._bodyContainer || this._menuElement.nativeElement, this.placement, this.container === 'body'));
        }
    }
    /**
     * @private
     * @return {?}
     */
    _resetContainer() {
        /** @type {?} */
        const renderer = this._renderer;
        if (this._menuElement) {
            /** @type {?} */
            const dropdownElement = this._elementRef.nativeElement;
            /** @type {?} */
            const dropdownMenuElement = this._menuElement.nativeElement;
            renderer.appendChild(dropdownElement, dropdownMenuElement);
            renderer.removeStyle(dropdownMenuElement, 'position');
            renderer.removeStyle(dropdownMenuElement, 'transform');
        }
        if (this._bodyContainer) {
            renderer.removeChild(this._document.body, this._bodyContainer);
            this._bodyContainer = null;
        }
    }
    /**
     * @private
     * @param {?=} container
     * @return {?}
     */
    _applyContainer(container = null) {
        this._resetContainer();
        if (container === 'body') {
            /** @type {?} */
            const renderer = this._renderer;
            /** @type {?} */
            const dropdownMenuElement = this._menuElement.nativeElement;
            /** @type {?} */
            const bodyContainer = this._bodyContainer = this._bodyContainer || renderer.createElement('div');
            // Override some styles to have the positionning working
            renderer.setStyle(bodyContainer, 'position', 'absolute');
            renderer.setStyle(dropdownMenuElement, 'position', 'static');
            renderer.appendChild(bodyContainer, dropdownMenuElement);
            renderer.appendChild(this._document.body, bodyContainer);
        }
    }
    /**
     * @private
     * @param {?=} placement
     * @return {?}
     */
    _applyPlacementClasses(placement) {
        if (this._menu) {
            if (!placement) {
                placement = Array.isArray(this.placement) ? this.placement[0] : (/** @type {?} */ (this.placement));
            }
            /** @type {?} */
            const renderer = this._renderer;
            /** @type {?} */
            const dropdownElement = this._elementRef.nativeElement;
            // remove the current placement classes
            renderer.removeClass(dropdownElement, 'dropup');
            renderer.removeClass(dropdownElement, 'dropdown');
            this.placement = placement;
            this._menu.placement = placement;
            /*
                        * apply the new placement
                        * in case of top use up-arrow or down-arrow otherwise
                        */
            /** @type {?} */
            const dropdownClass = placement.search('^top') !== -1 ? 'dropup' : 'dropdown';
            renderer.addClass(dropdownElement, dropdownClass);
            /** @type {?} */
            const bodyContainer = this._bodyContainer;
            if (bodyContainer) {
                renderer.removeClass(bodyContainer, 'dropup');
                renderer.removeClass(bodyContainer, 'dropdown');
                renderer.addClass(bodyContainer, dropdownClass);
            }
        }
    }
}
NgtDropdownDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdown]',
                exportAs: 'ngtDropdown'
            },] }
];
/** @nocollapse */
NgtDropdownDirective.ctorParameters = () => [
    { type: ChangeDetectorRef },
    { type: NgtDropdownConfig },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: NgZone },
    { type: ElementRef },
    { type: Renderer2 }
];
NgtDropdownDirective.propDecorators = {
    _menu: [{ type: ContentChild, args: [NgtDropdownMenuDirective,] }],
    _menuElement: [{ type: ContentChild, args: [NgtDropdownMenuDirective, { read: ElementRef },] }],
    _anchor: [{ type: ContentChild, args: [NgtDropdownAnchorDirective,] }],
    autoClose: [{ type: Input }],
    _open: [{ type: Input, args: ['open',] }],
    placement: [{ type: Input }],
    container: [{ type: Input }],
    openChange: [{ type: Output }],
    show: [{ type: HostBinding, args: ['class.show',] }],
    onArrowUp: [{ type: HostListener, args: ['document:keydown.ArrowUp', ['$event'],] }],
    onArrowArrowDown: [{ type: HostListener, args: ['document:keydown.ArrowDown', ['$event'],] }],
    onArrowHome: [{ type: HostListener, args: ['document:keydown.Home', ['$event'],] }],
    onArrowEnd: [{ type: HostListener, args: ['document:keydown.End', ['$event'],] }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._closed$;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._zoneSubscription;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._bodyContainer;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._menu;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._menuElement;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._anchor;
    /**
     * Indicates that dropdown should be closed when selecting one of dropdown items (click) or pressing ESC.
     * When it is true (default) dropdowns are automatically closed on both outside and inside (menu) clicks.
     * When it is false dropdowns are never automatically closed.
     * When it is 'outside' dropdowns are automatically closed on outside clicks but not on menu clicks.
     * When it is 'inside' dropdowns are automatically on menu clicks but not on outside clicks.
     * @type {?}
     */
    NgtDropdownDirective.prototype.autoClose;
    /**
     *  Defines whether or not the dropdown-menu is open initially.
     * @type {?}
     */
    NgtDropdownDirective.prototype._open;
    /**
     * Placement of a popover accepts:
     *    "top", "top-left", "top-right", "bottom", "bottom-left", "bottom-right",
     *    "left", "left-top", "left-bottom", "right", "right-top", "right-bottom"
     *  array or a space separated string of above values
     * @type {?}
     */
    NgtDropdownDirective.prototype.placement;
    /**
     * A selector specifying the element the dropdown should be appended to.
     * Currently only supports "body".
     * @type {?}
     */
    NgtDropdownDirective.prototype.container;
    /**
     *  An event fired when the dropdown is opened or closed.
     *  Event's payload equals whether dropdown is open.
     * @type {?}
     */
    NgtDropdownDirective.prototype.openChange;
    /** @type {?} */
    NgtDropdownDirective.prototype.show;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._changeDetector;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._document;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._ngZone;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._elementRef;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownDirective.prototype._renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24uanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImRyb3Bkb3duL2Ryb3Bkb3duLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0gsaUJBQWlCLEVBQ2pCLFlBQVksRUFBRSxlQUFlLEVBQzdCLFNBQVMsRUFDVCxVQUFVLEVBQ1YsWUFBWSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUNuRCxNQUFNLEVBQ04sS0FBSyxFQUNMLE1BQU0sRUFHTixNQUFNLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFDL0IsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxPQUFPLEVBQWdCLE1BQU0sTUFBTSxDQUFDO0FBRTdDLE9BQU8sRUFBNkIsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNsRixPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDakQsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUVsQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQzs7Ozs7QUFTdEQsTUFBTSxPQUFPLHdCQUF3Qjs7OztJQWdCakMsWUFBbUIsVUFBbUM7UUFBbkMsZUFBVSxHQUFWLFVBQVUsQ0FBeUI7UUFmOUMsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUVVLFVBQUssR0FBRyxJQUFJLENBQUM7UUFDbEIsa0JBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBYTdELENBQUM7Ozs7O0lBWEQsSUFDSSxRQUFRLENBQUMsS0FBYztRQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLG1CQUFLLEtBQUssRUFBQSxLQUFLLEVBQUUsSUFBSSxLQUFLLEtBQUssSUFBSSxDQUFDLENBQUUsb0NBQW9DO1FBQzNGLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUN4QyxDQUFDOzs7O0lBRUQsSUFBSSxRQUFRO1FBQ1IsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7OztZQWpCSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLG1CQUFtQjthQUNoQzs7OztZQXhCRyxVQUFVOzs7b0JBNEJULFdBQVcsU0FBQyxxQkFBcUI7NEJBQ2pDLFdBQVcsU0FBQyxnQkFBZ0I7dUJBRTVCLEtBQUs7Ozs7Ozs7SUFMTiw2Q0FBMEI7O0lBRTFCLHlDQUFpRDs7SUFDakQsaURBQTZEOztJQVlqRCw4Q0FBMEM7Ozs7O0FBUzFELE1BQU0sT0FBTyx3QkFBd0I7Ozs7OztJQVVqQyxZQUMyRCxRQUFRLEVBQ3ZELFdBQW9DLEVBQ3BDLFNBQW9CO1FBRjJCLGFBQVEsR0FBUixRQUFRLENBQUE7UUFDdkQsZ0JBQVcsR0FBWCxXQUFXLENBQXlCO1FBQ3BDLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFaaEMsY0FBUyxHQUFjLFFBQVEsQ0FBQztRQUNoQyxXQUFNLEdBQUcsS0FBSyxDQUFDO1FBSXFCLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQzdCLFNBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3hCLGVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBTXpELElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFNBQVM7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUN6QyxJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztRQUN0QixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxnQkFBZ0I7UUFDWixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDO0lBQzFDLENBQUM7Ozs7OztJQUVELFFBQVEsQ0FBQyxTQUFTLEVBQUUsU0FBUztRQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQ2hHLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLFVBQXFCO1FBQ2hDLHVDQUF1QztRQUN2QyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDaEYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ2xGLElBQUksQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDO1FBQzVCOzs7V0FHRztRQUNILElBQUksVUFBVSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtZQUNsQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FDaEY7YUFBTTtZQUNILElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztTQUNsRjtJQUNMLENBQUM7OztZQTVDSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLG1CQUFtQjthQUNoQzs7Ozs0Q0FZUSxNQUFNLFNBQUMsVUFBVTs7O29CQUFDLEdBQUcsRUFBRSxDQUFDLG9CQUFvQixFQUFDO1lBN0RsRCxVQUFVO1lBT1MsU0FBUzs7O3dCQStDM0IsZUFBZSxTQUFDLHdCQUF3QjsyQkFFeEMsV0FBVyxTQUFDLHFCQUFxQjttQkFDakMsV0FBVyxTQUFDLFlBQVk7eUJBQ3hCLFdBQVcsU0FBQyxrQkFBa0I7Ozs7SUFQL0IsNkNBQWdDOztJQUNoQywwQ0FBZTs7SUFFZiw2Q0FBMEY7O0lBRTFGLGdEQUF3RDs7SUFDeEQsd0NBQXlEOztJQUN6RCw4Q0FBNkQ7O0lBR3pELDRDQUErRDs7Ozs7SUFDL0QsK0NBQTRDOzs7OztJQUM1Qyw2Q0FBNEI7Ozs7Ozs7O0FBeUNwQyxNQUFNLE9BQU8sMEJBQTBCOzs7OztJQU9uQyxZQUFtRSxRQUFRLEVBQ3ZELFdBQW9DO1FBRFcsYUFBUSxHQUFSLFFBQVEsQ0FBQTtRQUN2RCxnQkFBVyxHQUFYLFdBQVcsQ0FBeUI7UUFMbEIsVUFBSyxHQUFHLElBQUksQ0FBQztRQUNYLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLGlCQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUlyRSxJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxhQUFhLENBQUM7SUFDOUMsQ0FBQzs7OztJQUVELGdCQUFnQjtRQUNaLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7SUFDMUMsQ0FBQzs7O1lBakJKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUscUJBQXFCO2FBQ2xDOzs7OzRDQVFnQixNQUFNLFNBQUMsVUFBVTs7O29CQUFDLEdBQUcsRUFBRSxDQUFDLG9CQUFvQixFQUFDO1lBL0cxRCxVQUFVOzs7b0JBMkdULFdBQVcsU0FBQyx1QkFBdUI7MkJBQ25DLFdBQVcsU0FBQyx5QkFBeUI7MkJBQ3JDLFdBQVcsU0FBQyxvQkFBb0I7Ozs7SUFKakMsOENBQVM7O0lBRVQsMkNBQW1EOztJQUNuRCxrREFBNEQ7O0lBQzVELGtEQUF5RTs7SUFFN0QsOENBQStEOzs7OztJQUMvRCxpREFBNEM7Ozs7OztBQWtCNUQsTUFBTSxPQUFPLDBCQUEyQixTQUFRLDBCQUEwQjs7Ozs7SUFVdEUsWUFBNEQsUUFBUSxFQUN4RCxVQUFtQztRQUMzQyxLQUFLLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBVk0sVUFBSyxHQUFHLElBQUksQ0FBQztRQUNYLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLGlCQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQVN6RSxDQUFDOzs7O0lBUHNCLE9BQU87UUFDMUIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7SUFPRCxVQUFVO1FBQ04sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7WUFyQkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxxQkFBcUI7Z0JBQy9CLFNBQVMsRUFBRSxDQUFDLEVBQUMsT0FBTyxFQUFFLDBCQUEwQixFQUFFLFdBQVcsRUFBRSxVQUFVOzs7d0JBQUMsR0FBRyxFQUFFLENBQUMsMEJBQTBCLEVBQUMsRUFBQyxDQUFDO2FBQ2hIOzs7OzRDQVdnQixNQUFNLFNBQUMsVUFBVTs7O29CQUFDLEdBQUcsRUFBRSxDQUFDLG9CQUFvQixFQUFDO1lBNUkxRCxVQUFVOzs7b0JBb0lULFdBQVcsU0FBQyx1QkFBdUI7MkJBQ25DLFdBQVcsU0FBQyx5QkFBeUI7MkJBQ3JDLFdBQVcsU0FBQyxvQkFBb0I7c0JBRWhDLFlBQVksU0FBQyxPQUFPOzs7O0lBSnJCLDJDQUFtRDs7SUFDbkQsa0RBQTREOztJQUM1RCxrREFBeUU7Ozs7O0FBdUI3RSxNQUFNLE9BQU8sb0JBQW9COzs7Ozs7Ozs7SUE2RDdCLFlBQ1ksZUFBa0MsRUFBRSxNQUF5QixFQUE0QixTQUFjLEVBQ3ZHLE9BQWUsRUFBVSxXQUFvQyxFQUFVLFNBQW9CO1FBRDNGLG9CQUFlLEdBQWYsZUFBZSxDQUFtQjtRQUF1RCxjQUFTLEdBQVQsU0FBUyxDQUFLO1FBQ3ZHLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFBVSxnQkFBVyxHQUFYLFdBQVcsQ0FBeUI7UUFBVSxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBOUQvRixhQUFRLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQzs7OztRQW9CeEIsVUFBSyxHQUFHLEtBQUssQ0FBQzs7Ozs7UUFvQm5CLGVBQVUsR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO1FBRXhCLFNBQUksR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFxQjVDLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUM7UUFDbEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsU0FBUyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRTtZQUNyRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDekIsQ0FBQyxFQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVM7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUMvQixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztRQUN2QixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBNUJxRCxTQUFTLENBQUMsTUFBTTtRQUNsRSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBRXVELGdCQUFnQixDQUFDLE1BQU07UUFDM0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMzQixDQUFDOzs7OztJQUVrRCxXQUFXLENBQUMsTUFBTTtRQUNqRSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBRWlELFVBQVUsQ0FBQyxNQUFNO1FBQy9ELElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7OztJQWdCRCxRQUFRO1FBQ0osSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFDOUIsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1osSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7U0FDNUI7SUFDTCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUM5QixJQUFJLE9BQU8sQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNqQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN4QztRQUVELElBQUksT0FBTyxDQUFDLFNBQVMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFO1lBQ3ZELElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1NBQ2pDO0lBQ0wsQ0FBQzs7Ozs7SUFLRCxNQUFNO1FBQ0YsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBS0QsSUFBSTtRQUNBLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ2IsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7WUFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDckMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQzVCO0lBQ0wsQ0FBQzs7Ozs7SUFFTyxpQkFBaUI7UUFDckIsWUFBWSxDQUNSLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUzs7O1FBQUUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxHQUFFLElBQUksQ0FBQyxRQUFRLEVBQy9FLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNsSCxDQUFDOzs7OztJQUtELEtBQUs7UUFDRCxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDWixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztZQUNuQixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRSxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQzs7Ozs7SUFLRCxNQUFNO1FBQ0YsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUU7WUFDZixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDaEI7YUFBTTtZQUNILElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNmO0lBQ0wsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFdkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDekMsQ0FBQzs7Ozs7SUFFRCxTQUFTLENBQUMsS0FBb0I7O2NBQ3BCLFlBQVksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7UUFFNUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDdEIsT0FBTyxLQUFLLENBQUM7U0FDaEI7O1lBRUcsUUFBUSxHQUFHLENBQUMsQ0FBQzs7WUFDYixnQkFBZ0IsR0FBRyxLQUFLOztjQUN0QixpQkFBaUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDO1FBRXhELElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUNwQixZQUFZLENBQUMsT0FBTzs7Ozs7WUFBQyxDQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDeEMsSUFBSSxXQUFXLENBQUMsUUFBUSxDQUFDLG1CQUFBLEtBQUssQ0FBQyxNQUFNLEVBQWUsQ0FBQyxFQUFFO29CQUNuRCxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7aUJBQzNCO2dCQUNELElBQUksV0FBVyxLQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFO29CQUM5QyxRQUFRLEdBQUcsS0FBSyxDQUFDO2lCQUNwQjtZQUNMLENBQUMsRUFBQyxDQUFDO1NBQ047UUFFRCxJQUFJLGlCQUFpQixJQUFJLGdCQUFnQixFQUFFO1lBQ3ZDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUU7Z0JBQ2hCLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQzthQUNmO1lBQ0QsdUNBQXVDO1lBQ3ZDLFFBQVEsS0FBSyxDQUFDLEtBQUssRUFBRTtnQkFDakIsS0FBSyxHQUFHLENBQUMsU0FBUztvQkFDZCxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxFQUFFLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQzNELE1BQU07Z0JBQ1YsS0FBSyxHQUFHLENBQUMsT0FBTztvQkFDWixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxRQUFRLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQ3JDLFFBQVEsR0FBRyxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzt3QkFDbkMsTUFBTTtxQkFDVDtvQkFDRCxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUNyQyxNQUFNO2dCQUNWLEtBQUssR0FBRyxDQUFDLElBQUk7b0JBQ1QsUUFBUSxHQUFHLENBQUMsQ0FBQztvQkFDYixNQUFNO2dCQUNWLEtBQUssR0FBRyxDQUFDLEdBQUc7b0JBQ1IsUUFBUSxHQUFHLFlBQVksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO29CQUNuQyxNQUFNO2FBQ2I7WUFDRCxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDL0IsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1NBQzFCO0lBQ0wsQ0FBQzs7Ozs7SUFFTyxTQUFTO1FBQ2IsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7Ozs7OztJQUVPLGtCQUFrQixDQUFDLEtBQW9CO1FBQzNDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFFBQVEsQ0FBQyxtQkFBQSxLQUFLLENBQUMsTUFBTSxFQUFlLENBQUMsQ0FBQztJQUNqRixDQUFDOzs7OztJQUVPLGdCQUFnQjtRQUNwQixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ3BCLE9BQU8sRUFBRSxDQUFDO1NBQ2I7UUFDRCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLE1BQU07Ozs7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBQyxDQUFDLEdBQUc7Ozs7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFDLENBQUM7SUFDMUcsQ0FBQzs7Ozs7SUFFTyxhQUFhO1FBQ2pCLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDN0IsSUFBSSxDQUFDLHNCQUFzQixDQUN2QixnQkFBZ0IsQ0FDWixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsY0FBYyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQzdGLElBQUksQ0FBQyxTQUFTLEtBQUssTUFBTSxDQUFDLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7Ozs7O0lBRU8sZUFBZTs7Y0FDYixRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVM7UUFDL0IsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFOztrQkFDYixlQUFlLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhOztrQkFDaEQsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhO1lBRTNELFFBQVEsQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFLG1CQUFtQixDQUFDLENBQUM7WUFDM0QsUUFBUSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUN0RCxRQUFRLENBQUMsV0FBVyxDQUFDLG1CQUFtQixFQUFFLFdBQVcsQ0FBQyxDQUFDO1NBQzFEO1FBQ0QsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3JCLFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQy9ELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1NBQzlCO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sZUFBZSxDQUFDLFlBQTJCLElBQUk7UUFDbkQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3ZCLElBQUksU0FBUyxLQUFLLE1BQU0sRUFBRTs7a0JBQ2hCLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUzs7a0JBQ3pCLG1CQUFtQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYTs7a0JBQ3JELGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxjQUFjLElBQUksUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7WUFFaEcsd0RBQXdEO1lBQ3hELFFBQVEsQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLFVBQVUsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUN6RCxRQUFRLENBQUMsUUFBUSxDQUFDLG1CQUFtQixFQUFFLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUU3RCxRQUFRLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO1lBQ3pELFFBQVEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLENBQUM7U0FDNUQ7SUFDTCxDQUFDOzs7Ozs7SUFFTyxzQkFBc0IsQ0FBQyxTQUFxQjtRQUNoRCxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDWixJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNaLFNBQVMsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsbUJBQUEsSUFBSSxDQUFDLFNBQVMsRUFBYSxDQUFDO2FBQy9GOztrQkFFSyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVM7O2tCQUN6QixlQUFlLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhO1lBRXRELHVDQUF1QztZQUN2QyxRQUFRLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUNoRCxRQUFRLENBQUMsV0FBVyxDQUFDLGVBQWUsRUFBRSxVQUFVLENBQUMsQ0FBQztZQUNsRCxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztZQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7Ozs7OztrQkFNM0IsYUFBYSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsVUFBVTtZQUM3RSxRQUFRLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxhQUFhLENBQUMsQ0FBQzs7a0JBRTVDLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYztZQUN6QyxJQUFJLGFBQWEsRUFBRTtnQkFDZixRQUFRLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDOUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBQ2hELFFBQVEsQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLGFBQWEsQ0FBQyxDQUFDO2FBQ25EO1NBQ0o7SUFDTCxDQUFDOzs7WUFoU0osU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxlQUFlO2dCQUN6QixRQUFRLEVBQUUsYUFBYTthQUMxQjs7OztZQS9KRyxpQkFBaUI7WUFtQlosaUJBQWlCOzRDQTJNc0QsTUFBTSxTQUFDLFFBQVE7WUF2TjNGLE1BQU07WUFKTixVQUFVO1lBT1MsU0FBUzs7O29CQTJKM0IsWUFBWSxTQUFDLHdCQUF3QjsyQkFDckMsWUFBWSxTQUFDLHdCQUF3QixFQUFFLEVBQUMsSUFBSSxFQUFFLFVBQVUsRUFBQztzQkFDekQsWUFBWSxTQUFDLDBCQUEwQjt3QkFTdkMsS0FBSztvQkFLTCxLQUFLLFNBQUMsTUFBTTt3QkFRWixLQUFLO3dCQU1MLEtBQUs7eUJBTUwsTUFBTTttQkFFTixXQUFXLFNBQUMsWUFBWTt3QkFFeEIsWUFBWSxTQUFDLDBCQUEwQixFQUFFLENBQUMsUUFBUSxDQUFDOytCQUluRCxZQUFZLFNBQUMsNEJBQTRCLEVBQUUsQ0FBQyxRQUFRLENBQUM7MEJBSXJELFlBQVksU0FBQyx1QkFBdUIsRUFBRSxDQUFDLFFBQVEsQ0FBQzt5QkFJaEQsWUFBWSxTQUFDLHNCQUFzQixFQUFFLENBQUMsUUFBUSxDQUFDOzs7Ozs7O0lBeERoRCx3Q0FBdUM7Ozs7O0lBQ3ZDLGlEQUF3Qzs7Ozs7SUFDeEMsOENBQW9DOzs7OztJQUVwQyxxQ0FBZ0Y7Ozs7O0lBQ2hGLDRDQUE2Rjs7Ozs7SUFDN0YsdUNBQXNGOzs7Ozs7Ozs7SUFTdEYseUNBQW1EOzs7OztJQUtuRCxxQ0FBNkI7Ozs7Ozs7O0lBUTdCLHlDQUFtQzs7Ozs7O0lBTW5DLHlDQUFrQzs7Ozs7O0lBTWxDLDBDQUFtRDs7SUFFbkQsb0NBQWdEOzs7OztJQW1CNUMsK0NBQTBDOzs7OztJQUE2Qix5Q0FBd0M7Ozs7O0lBQy9HLHVDQUF1Qjs7Ozs7SUFBRSwyQ0FBNEM7Ozs7O0lBQUUseUNBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICAgIENoYW5nZURldGVjdG9yUmVmLFxyXG4gICAgQ29udGVudENoaWxkLCBDb250ZW50Q2hpbGRyZW4sXHJcbiAgICBEaXJlY3RpdmUsXHJcbiAgICBFbGVtZW50UmVmLFxyXG4gICAgRXZlbnRFbWl0dGVyLCBmb3J3YXJkUmVmLCBIb3N0QmluZGluZywgSG9zdExpc3RlbmVyLFxyXG4gICAgSW5qZWN0LFxyXG4gICAgSW5wdXQsXHJcbiAgICBOZ1pvbmUsIE9uQ2hhbmdlcyxcclxuICAgIE9uRGVzdHJveSxcclxuICAgIE9uSW5pdCxcclxuICAgIE91dHB1dCwgUXVlcnlMaXN0LCBSZW5kZXJlcjIsIFNpbXBsZUNoYW5nZXNcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRE9DVU1FTlQgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBTdWJqZWN0LCBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IFBsYWNlbWVudCwgUGxhY2VtZW50QXJyYXksIHBvc2l0aW9uRWxlbWVudHMgfSBmcm9tICcuLi91dGlsL3Bvc2l0aW9uaW5nJztcclxuaW1wb3J0IHsgbmd0QXV0b0Nsb3NlIH0gZnJvbSAnLi4vdXRpbC9hdXRvY2xvc2UnO1xyXG5pbXBvcnQgeyBLZXkgfSBmcm9tICcuLi91dGlsL2tleSc7XHJcblxyXG5pbXBvcnQgeyBOZ3REcm9wZG93bkNvbmZpZyB9IGZyb20gJy4vZHJvcGRvd24tY29uZmlnJztcclxuXHJcbi8qKlxyXG4gKiBBIGRpcmVjdGl2ZSB5b3Ugc2hvdWxkIHB1dCBwdXQgb24gYSBkcm9wZG93biBpdGVtIHRvIGVuYWJsZSBrZXlib2FyZCBuYXZpZ2F0aW9uLlxyXG4gKiBLZXlib2FyZCBuYXZpZ2F0aW9uIHVzaW5nIGFycm93IGtleXMgd2lsbCBtb3ZlIGZvY3VzIGJldHdlZW4gaXRlbXMgbWFya2VkIHdpdGggdGhpcyBkaXJlY3RpdmUuXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndERyb3Bkb3duSXRlbV0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3REcm9wZG93bkl0ZW1EaXJlY3RpdmUge1xyXG4gICAgcHJpdmF0ZSBfZGlzYWJsZWQgPSBmYWxzZTtcclxuXHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLmRyb3Bkb3duLWl0ZW0nKSBjbGFzcyA9IHRydWU7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLmRpc2FibGVkJykgY2xhc3NEaXNhYmxlZCA9IHRoaXMuZGlzYWJsZWQ7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHNldCBkaXNhYmxlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgICAgIHRoaXMuX2Rpc2FibGVkID0gPGFueT52YWx1ZSA9PT0gJycgfHwgdmFsdWUgPT09IHRydWU7ICAvLyBhY2NlcHQgYW4gZW1wdHkgYXR0cmlidXRlIGFzIHRydWVcclxuICAgICAgICB0aGlzLmNsYXNzRGlzYWJsZWQgPSB0aGlzLl9kaXNhYmxlZDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGlzYWJsZWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2Rpc2FibGVkO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBlbGVtZW50UmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50Pikge1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICovXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbbmd0RHJvcGRvd25NZW51XSdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndERyb3Bkb3duTWVudURpcmVjdGl2ZSB7XHJcbiAgICBwbGFjZW1lbnQ6IFBsYWNlbWVudCA9ICdib3R0b20nO1xyXG4gICAgaXNPcGVuID0gZmFsc2U7XHJcblxyXG4gICAgQENvbnRlbnRDaGlsZHJlbihOZ3REcm9wZG93bkl0ZW1EaXJlY3RpdmUpIG1lbnVJdGVtczogUXVlcnlMaXN0PE5ndERyb3Bkb3duSXRlbURpcmVjdGl2ZT47XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5kcm9wZG93bi1tZW51JykgZHJvcGRvd25NZW51ID0gdHJ1ZTtcclxuICAgIEBIb3N0QmluZGluZygnY2xhc3Muc2hvdycpIHNob3cgPSB0aGlzLmRyb3Bkb3duLmlzT3BlbigpO1xyXG4gICAgQEhvc3RCaW5kaW5nKCdhdHRyLngtcGxhY2VtZW50JykgeFBsYWNlbWVudCA9IHRoaXMucGxhY2VtZW50O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIEBJbmplY3QoZm9yd2FyZFJlZigoKSA9PiBOZ3REcm9wZG93bkRpcmVjdGl2ZSkpIHB1YmxpYyBkcm9wZG93bixcclxuICAgICAgICBwcml2YXRlIF9lbGVtZW50UmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PixcclxuICAgICAgICBwcml2YXRlIF9yZW5kZXJlcjogUmVuZGVyZXIyKSB7XHJcbiAgICAgICAgdGhpcy5kcm9wZG93bi5vcGVuQ2hhbmdlLnN1YnNjcmliZShzdGF0dXMgPT4ge1xyXG4gICAgICAgICAgIHRoaXMuc2hvdyA9IHN0YXR1cztcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXROYXRpdmVFbGVtZW50KCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgcG9zaXRpb24odHJpZ2dlckVsLCBwbGFjZW1lbnQpIHtcclxuICAgICAgICB0aGlzLmFwcGx5UGxhY2VtZW50KHBvc2l0aW9uRWxlbWVudHModHJpZ2dlckVsLCB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsIHBsYWNlbWVudCkpO1xyXG4gICAgfVxyXG5cclxuICAgIGFwcGx5UGxhY2VtZW50KF9wbGFjZW1lbnQ6IFBsYWNlbWVudCkge1xyXG4gICAgICAgIC8vIHJlbW92ZSB0aGUgY3VycmVudCBwbGFjZW1lbnQgY2xhc3Nlc1xyXG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZUNsYXNzKHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5wYXJlbnROb2RlLCAnZHJvcHVwJyk7XHJcbiAgICAgICAgdGhpcy5fcmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnBhcmVudE5vZGUsICdkcm9wZG93bicpO1xyXG4gICAgICAgIHRoaXMucGxhY2VtZW50ID0gX3BsYWNlbWVudDtcclxuICAgICAgICAvKipcclxuICAgICAgICAgKiBhcHBseSB0aGUgbmV3IHBsYWNlbWVudFxyXG4gICAgICAgICAqIGluIGNhc2Ugb2YgdG9wIHVzZSB1cC1hcnJvdyBvciBkb3duLWFycm93IG90aGVyd2lzZVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIGlmIChfcGxhY2VtZW50LnNlYXJjaCgnXnRvcCcpICE9PSAtMSkge1xyXG4gICAgICAgICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQucGFyZW50Tm9kZSwgJ2Ryb3B1cCcpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5wYXJlbnROb2RlLCAnZHJvcGRvd24nKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG4vKipcclxuICogTWFya3MgYW4gZWxlbWVudCB0byB3aGljaCBkcm9wZG93biBtZW51IHdpbGwgYmUgYW5jaG9yZWQuIFRoaXMgaXMgYSBzaW1wbGUgdmVyc2lvblxyXG4gKiBvZiB0aGUgTmd0RHJvcGRvd25Ub2dnbGVEaXJlY3RpdmUuIEl0IHBsYXlzIHRoZSBzYW1lIHJvbGUgYXMgTmd0RHJvcGRvd25Ub2dnbGVEaXJlY3RpdmUgYnV0XHJcbiAqIGRvZXNuJ3QgbGlzdGVuIHRvIGNsaWNrIGV2ZW50cyB0byB0b2dnbGUgZHJvcGRvd24gbWVudSB0aHVzIGVuYWJsaW5nIHN1cHBvcnQgZm9yXHJcbiAqIGV2ZW50cyBvdGhlciB0aGFuIGNsaWNrLlxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3REcm9wZG93bkFuY2hvcl0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3REcm9wZG93bkFuY2hvckRpcmVjdGl2ZSB7XHJcbiAgICBhbmNob3JFbDtcclxuXHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLmRyb3Bkb3duLXRvZ2dsZScpIGNsYXNzID0gdHJ1ZTtcclxuICAgIEBIb3N0QmluZGluZygnYXR0ci5hcmlhLWhhc3BvcHVwLnRydWUnKSBhcmlhSGFzcG9wdXAgPSB0cnVlO1xyXG4gICAgQEhvc3RCaW5kaW5nKCdhdHRyLmFyaWEtZXhwYW5kZWQnKSBhcmlhRXhwYW5kZWQgPSB0aGlzLmRyb3Bkb3duLmlzT3BlbigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKEBJbmplY3QoZm9yd2FyZFJlZigoKSA9PiBOZ3REcm9wZG93bkRpcmVjdGl2ZSkpIHB1YmxpYyBkcm9wZG93bixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX2VsZW1lbnRSZWY6IEVsZW1lbnRSZWY8SFRNTEVsZW1lbnQ+KSB7XHJcbiAgICAgICAgdGhpcy5hbmNob3JFbCA9IF9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TmF0aXZlRWxlbWVudCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50O1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuLyoqXHJcbiAqIEFsbG93cyB0aGUgZHJvcGRvd24gdG8gYmUgdG9nZ2xlZCB2aWEgY2xpY2suIFRoaXMgZGlyZWN0aXZlIGlzIG9wdGlvbmFsOiB5b3UgY2FuIHVzZSBOZ3REcm9wZG93bkFuY2hvckRpcmVjdGl2ZSBhcyBhblxyXG4gKiBhbHRlcm5hdGl2ZS5cclxuICovXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbbmd0RHJvcGRvd25Ub2dnbGVdJyxcclxuICAgIHByb3ZpZGVyczogW3twcm92aWRlOiBOZ3REcm9wZG93bkFuY2hvckRpcmVjdGl2ZSwgdXNlRXhpc3Rpbmc6IGZvcndhcmRSZWYoKCkgPT4gTmd0RHJvcGRvd25Ub2dnbGVEaXJlY3RpdmUpfV1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndERyb3Bkb3duVG9nZ2xlRGlyZWN0aXZlIGV4dGVuZHMgTmd0RHJvcGRvd25BbmNob3JEaXJlY3RpdmUge1xyXG5cclxuICAgIEBIb3N0QmluZGluZygnY2xhc3MuZHJvcGRvd24tdG9nZ2xlJykgY2xhc3MgPSB0cnVlO1xyXG4gICAgQEhvc3RCaW5kaW5nKCdhdHRyLmFyaWEtaGFzcG9wdXAudHJ1ZScpIGFyaWFIYXNwb3B1cCA9IHRydWU7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2F0dHIuYXJpYS1leHBhbmRlZCcpIGFyaWFFeHBhbmRlZCA9IHRoaXMuZHJvcGRvd24uaXNPcGVuKCk7XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snKSBvbkNsaWNrKCkge1xyXG4gICAgICAgIHRoaXMudG9nZ2xlT3BlbigpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKEBJbmplY3QoZm9yd2FyZFJlZigoKSA9PiBOZ3REcm9wZG93bkRpcmVjdGl2ZSkpIGRyb3Bkb3duLFxyXG4gICAgICAgICAgICAgICAgZWxlbWVudFJlZjogRWxlbWVudFJlZjxIVE1MRWxlbWVudD4pIHtcclxuICAgICAgICBzdXBlcihkcm9wZG93biwgZWxlbWVudFJlZik7XHJcbiAgICB9XHJcblxyXG4gICAgdG9nZ2xlT3BlbigpIHtcclxuICAgICAgICB0aGlzLmRyb3Bkb3duLnRvZ2dsZSgpO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogVHJhbnNmb3JtcyBhIG5vZGUgaW50byBhIGRyb3Bkb3duLlxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3REcm9wZG93bl0nLFxyXG4gICAgZXhwb3J0QXM6ICduZ3REcm9wZG93bidcclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndERyb3Bkb3duRGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIE9uQ2hhbmdlcyB7XHJcbiAgICBwcml2YXRlIF9jbG9zZWQkID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuICAgIHByaXZhdGUgX3pvbmVTdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuICAgIHByaXZhdGUgX2JvZHlDb250YWluZXI6IEhUTUxFbGVtZW50O1xyXG5cclxuICAgIEBDb250ZW50Q2hpbGQoTmd0RHJvcGRvd25NZW51RGlyZWN0aXZlKSBwcml2YXRlIF9tZW51OiBOZ3REcm9wZG93bk1lbnVEaXJlY3RpdmU7XHJcbiAgICBAQ29udGVudENoaWxkKE5ndERyb3Bkb3duTWVudURpcmVjdGl2ZSwge3JlYWQ6IEVsZW1lbnRSZWZ9KSBwcml2YXRlIF9tZW51RWxlbWVudDogRWxlbWVudFJlZjtcclxuICAgIEBDb250ZW50Q2hpbGQoTmd0RHJvcGRvd25BbmNob3JEaXJlY3RpdmUpIHByaXZhdGUgX2FuY2hvcjogTmd0RHJvcGRvd25BbmNob3JEaXJlY3RpdmU7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJbmRpY2F0ZXMgdGhhdCBkcm9wZG93biBzaG91bGQgYmUgY2xvc2VkIHdoZW4gc2VsZWN0aW5nIG9uZSBvZiBkcm9wZG93biBpdGVtcyAoY2xpY2spIG9yIHByZXNzaW5nIEVTQy5cclxuICAgICAqIFdoZW4gaXQgaXMgdHJ1ZSAoZGVmYXVsdCkgZHJvcGRvd25zIGFyZSBhdXRvbWF0aWNhbGx5IGNsb3NlZCBvbiBib3RoIG91dHNpZGUgYW5kIGluc2lkZSAobWVudSkgY2xpY2tzLlxyXG4gICAgICogV2hlbiBpdCBpcyBmYWxzZSBkcm9wZG93bnMgYXJlIG5ldmVyIGF1dG9tYXRpY2FsbHkgY2xvc2VkLlxyXG4gICAgICogV2hlbiBpdCBpcyAnb3V0c2lkZScgZHJvcGRvd25zIGFyZSBhdXRvbWF0aWNhbGx5IGNsb3NlZCBvbiBvdXRzaWRlIGNsaWNrcyBidXQgbm90IG9uIG1lbnUgY2xpY2tzLlxyXG4gICAgICogV2hlbiBpdCBpcyAnaW5zaWRlJyBkcm9wZG93bnMgYXJlIGF1dG9tYXRpY2FsbHkgb24gbWVudSBjbGlja3MgYnV0IG5vdCBvbiBvdXRzaWRlIGNsaWNrcy5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgYXV0b0Nsb3NlOiBib29sZWFuIHwgJ291dHNpZGUnIHwgJ2luc2lkZSc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgRGVmaW5lcyB3aGV0aGVyIG9yIG5vdCB0aGUgZHJvcGRvd24tbWVudSBpcyBvcGVuIGluaXRpYWxseS5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCdvcGVuJykgX29wZW4gPSBmYWxzZTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFBsYWNlbWVudCBvZiBhIHBvcG92ZXIgYWNjZXB0czpcclxuICAgICAqICAgIFwidG9wXCIsIFwidG9wLWxlZnRcIiwgXCJ0b3AtcmlnaHRcIiwgXCJib3R0b21cIiwgXCJib3R0b20tbGVmdFwiLCBcImJvdHRvbS1yaWdodFwiLFxyXG4gICAgICogICAgXCJsZWZ0XCIsIFwibGVmdC10b3BcIiwgXCJsZWZ0LWJvdHRvbVwiLCBcInJpZ2h0XCIsIFwicmlnaHQtdG9wXCIsIFwicmlnaHQtYm90dG9tXCJcclxuICAgICAqICBhcnJheSBvciBhIHNwYWNlIHNlcGFyYXRlZCBzdHJpbmcgb2YgYWJvdmUgdmFsdWVzXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHBsYWNlbWVudDogUGxhY2VtZW50QXJyYXk7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBIHNlbGVjdG9yIHNwZWNpZnlpbmcgdGhlIGVsZW1lbnQgdGhlIGRyb3Bkb3duIHNob3VsZCBiZSBhcHBlbmRlZCB0by5cclxuICAgICAqIEN1cnJlbnRseSBvbmx5IHN1cHBvcnRzIFwiYm9keVwiLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBjb250YWluZXI6IG51bGwgfCAnYm9keSc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgQW4gZXZlbnQgZmlyZWQgd2hlbiB0aGUgZHJvcGRvd24gaXMgb3BlbmVkIG9yIGNsb3NlZC5cclxuICAgICAqICBFdmVudCdzIHBheWxvYWQgZXF1YWxzIHdoZXRoZXIgZHJvcGRvd24gaXMgb3Blbi5cclxuICAgICAqL1xyXG4gICAgQE91dHB1dCgpIG9wZW5DaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5zaG93Jykgc2hvdyA9IHRoaXMuaXNPcGVuKCk7XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6a2V5ZG93bi5BcnJvd1VwJywgWyckZXZlbnQnXSkgb25BcnJvd1VwKCRldmVudCkge1xyXG4gICAgICAgIHRoaXMub25LZXlEb3duKCRldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6a2V5ZG93bi5BcnJvd0Rvd24nLCBbJyRldmVudCddKSBvbkFycm93QXJyb3dEb3duKCRldmVudCkge1xyXG4gICAgICAgIHRoaXMub25LZXlEb3duKCRldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6a2V5ZG93bi5Ib21lJywgWyckZXZlbnQnXSkgb25BcnJvd0hvbWUoJGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5vbktleURvd24oJGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdkb2N1bWVudDprZXlkb3duLkVuZCcsIFsnJGV2ZW50J10pIG9uQXJyb3dFbmQoJGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5vbktleURvd24oJGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF9jaGFuZ2VEZXRlY3RvcjogQ2hhbmdlRGV0ZWN0b3JSZWYsIGNvbmZpZzogTmd0RHJvcGRvd25Db25maWcsIEBJbmplY3QoRE9DVU1FTlQpIHByaXZhdGUgX2RvY3VtZW50OiBhbnksXHJcbiAgICAgICAgcHJpdmF0ZSBfbmdab25lOiBOZ1pvbmUsIHByaXZhdGUgX2VsZW1lbnRSZWY6IEVsZW1lbnRSZWY8SFRNTEVsZW1lbnQ+LCBwcml2YXRlIF9yZW5kZXJlcjogUmVuZGVyZXIyKSB7XHJcbiAgICAgICAgdGhpcy5wbGFjZW1lbnQgPSBjb25maWcucGxhY2VtZW50O1xyXG4gICAgICAgIHRoaXMuY29udGFpbmVyID0gY29uZmlnLmNvbnRhaW5lcjtcclxuICAgICAgICB0aGlzLmF1dG9DbG9zZSA9IGNvbmZpZy5hdXRvQ2xvc2U7XHJcbiAgICAgICAgdGhpcy5fem9uZVN1YnNjcmlwdGlvbiA9IF9uZ1pvbmUub25TdGFibGUuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fcG9zaXRpb25NZW51KCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5vcGVuQ2hhbmdlLnN1YnNjcmliZShzdGF0dXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNob3cgPSBzdGF0dXM7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5fYXBwbHlQbGFjZW1lbnRDbGFzc2VzKCk7XHJcbiAgICAgICAgaWYgKHRoaXMuX29wZW4pIHtcclxuICAgICAgICAgICAgdGhpcy5fc2V0Q2xvc2VIYW5kbGVycygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAgICAgaWYgKGNoYW5nZXMuY29udGFpbmVyICYmIHRoaXMuX29wZW4pIHtcclxuICAgICAgICAgICAgdGhpcy5fYXBwbHlDb250YWluZXIodGhpcy5jb250YWluZXIpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNoYW5nZXMucGxhY2VtZW50ICYmICFjaGFuZ2VzLnBsYWNlbWVudC5pc0ZpcnN0Q2hhbmdlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX2FwcGx5UGxhY2VtZW50Q2xhc3NlcygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBpZiB0aGUgZHJvcGRvd24gbWVudSBpcyBvcGVuIG9yIG5vdC5cclxuICAgICAqL1xyXG4gICAgaXNPcGVuKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9vcGVuO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT3BlbnMgdGhlIGRyb3Bkb3duIG1lbnUgb2YgYSBnaXZlbiBuYXZiYXIgb3IgdGFiYmVkIG5hdmlnYXRpb24uXHJcbiAgICAgKi9cclxuICAgIG9wZW4oKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKCF0aGlzLl9vcGVuKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX29wZW4gPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLl9hcHBseUNvbnRhaW5lcih0aGlzLmNvbnRhaW5lcik7XHJcbiAgICAgICAgICAgIHRoaXMuX3Bvc2l0aW9uTWVudSgpO1xyXG4gICAgICAgICAgICB0aGlzLm9wZW5DaGFuZ2UuZW1pdCh0cnVlKTtcclxuICAgICAgICAgICAgdGhpcy5fc2V0Q2xvc2VIYW5kbGVycygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9zZXRDbG9zZUhhbmRsZXJzKCkge1xyXG4gICAgICAgIG5ndEF1dG9DbG9zZShcclxuICAgICAgICAgICAgdGhpcy5fbmdab25lLCB0aGlzLl9kb2N1bWVudCwgdGhpcy5hdXRvQ2xvc2UsICgpID0+IHRoaXMuY2xvc2UoKSwgdGhpcy5fY2xvc2VkJCxcclxuICAgICAgICAgICAgdGhpcy5fbWVudSA/IFt0aGlzLl9tZW51LmdldE5hdGl2ZUVsZW1lbnQoKV0gOiBbXSwgdGhpcy5fYW5jaG9yID8gW3RoaXMuX2FuY2hvci5nZXROYXRpdmVFbGVtZW50KCldIDogW10pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2xvc2VzIHRoZSBkcm9wZG93biBtZW51IG9mIGEgZ2l2ZW4gbmF2YmFyIG9yIHRhYmJlZCBuYXZpZ2F0aW9uLlxyXG4gICAgICovXHJcbiAgICBjbG9zZSgpOiB2b2lkIHtcclxuICAgICAgICBpZiAodGhpcy5fb3Blbikge1xyXG4gICAgICAgICAgICB0aGlzLl9vcGVuID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuX3Jlc2V0Q29udGFpbmVyKCk7XHJcbiAgICAgICAgICAgIHRoaXMuX2Nsb3NlZCQubmV4dCgpO1xyXG4gICAgICAgICAgICB0aGlzLm9wZW5DaGFuZ2UuZW1pdChmYWxzZSk7XHJcbiAgICAgICAgICAgIHRoaXMuX2NoYW5nZURldGVjdG9yLm1hcmtGb3JDaGVjaygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRvZ2dsZXMgdGhlIGRyb3Bkb3duIG1lbnUgb2YgYSBnaXZlbiBuYXZiYXIgb3IgdGFiYmVkIG5hdmlnYXRpb24uXHJcbiAgICAgKi9cclxuICAgIHRvZ2dsZSgpOiB2b2lkIHtcclxuICAgICAgICBpZiAodGhpcy5pc09wZW4oKSkge1xyXG4gICAgICAgICAgICB0aGlzLmNsb3NlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5vcGVuKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMuX3Jlc2V0Q29udGFpbmVyKCk7XHJcblxyXG4gICAgICAgIHRoaXMuX2Nsb3NlZCQubmV4dCgpO1xyXG4gICAgICAgIHRoaXMuX3pvbmVTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgIH1cclxuXHJcbiAgICBvbktleURvd24oZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcclxuICAgICAgICBjb25zdCBpdGVtRWxlbWVudHMgPSB0aGlzLl9nZXRNZW51RWxlbWVudHMoKTtcclxuXHJcbiAgICAgICAgaWYgKCFpdGVtRWxlbWVudHMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGxldCBwb3NpdGlvbiA9IC0xO1xyXG4gICAgICAgIGxldCBpc0V2ZW50RnJvbUl0ZW1zID0gZmFsc2U7XHJcbiAgICAgICAgY29uc3QgaXNFdmVudEZyb21Ub2dnbGUgPSB0aGlzLl9pc0V2ZW50RnJvbVRvZ2dsZShldmVudCk7XHJcblxyXG4gICAgICAgIGlmICghaXNFdmVudEZyb21Ub2dnbGUpIHtcclxuICAgICAgICAgICAgaXRlbUVsZW1lbnRzLmZvckVhY2goKGl0ZW1FbGVtZW50LCBpbmRleCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGl0ZW1FbGVtZW50LmNvbnRhaW5zKGV2ZW50LnRhcmdldCBhcyBIVE1MRWxlbWVudCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBpc0V2ZW50RnJvbUl0ZW1zID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChpdGVtRWxlbWVudCA9PT0gdGhpcy5fZG9jdW1lbnQuYWN0aXZlRWxlbWVudCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uID0gaW5kZXg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGlzRXZlbnRGcm9tVG9nZ2xlIHx8IGlzRXZlbnRGcm9tSXRlbXMpIHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLmlzT3BlbigpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wZW4oKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6ZGVwcmVjYXRpb25cclxuICAgICAgICAgICAgc3dpdGNoIChldmVudC53aGljaCkge1xyXG4gICAgICAgICAgICAgICAgY2FzZSBLZXkuQXJyb3dEb3duOlxyXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uID0gTWF0aC5taW4ocG9zaXRpb24gKyAxLCBpdGVtRWxlbWVudHMubGVuZ3RoIC0gMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIEtleS5BcnJvd1VwOlxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLl9pc0Ryb3B1cCgpICYmIHBvc2l0aW9uID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbiA9IGl0ZW1FbGVtZW50cy5sZW5ndGggLSAxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb24gPSBNYXRoLm1heChwb3NpdGlvbiAtIDEsIDApO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBLZXkuSG9tZTpcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbiA9IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIEtleS5FbmQ6XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb24gPSBpdGVtRWxlbWVudHMubGVuZ3RoIC0gMTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpdGVtRWxlbWVudHNbcG9zaXRpb25dLmZvY3VzKCk7XHJcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2lzRHJvcHVwKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQuY2xhc3NMaXN0LmNvbnRhaW5zKCdkcm9wdXAnKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9pc0V2ZW50RnJvbVRvZ2dsZShldmVudDogS2V5Ym9hcmRFdmVudCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9hbmNob3IuZ2V0TmF0aXZlRWxlbWVudCgpLmNvbnRhaW5zKGV2ZW50LnRhcmdldCBhcyBIVE1MRWxlbWVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfZ2V0TWVudUVsZW1lbnRzKCk6IEhUTUxFbGVtZW50W10ge1xyXG4gICAgICAgIGlmICh0aGlzLl9tZW51ID09IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIFtdO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fbWVudS5tZW51SXRlbXMuZmlsdGVyKGl0ZW0gPT4gIWl0ZW0uZGlzYWJsZWQpLm1hcChpdGVtID0+IGl0ZW0uZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9wb3NpdGlvbk1lbnUoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNPcGVuKCkgJiYgdGhpcy5fbWVudSkge1xyXG4gICAgICAgICAgICB0aGlzLl9hcHBseVBsYWNlbWVudENsYXNzZXMoXHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbkVsZW1lbnRzKFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2FuY2hvci5hbmNob3JFbCwgdGhpcy5fYm9keUNvbnRhaW5lciB8fCB0aGlzLl9tZW51RWxlbWVudC5uYXRpdmVFbGVtZW50LCB0aGlzLnBsYWNlbWVudCxcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRhaW5lciA9PT0gJ2JvZHknKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX3Jlc2V0Q29udGFpbmVyKCkge1xyXG4gICAgICAgIGNvbnN0IHJlbmRlcmVyID0gdGhpcy5fcmVuZGVyZXI7XHJcbiAgICAgICAgaWYgKHRoaXMuX21lbnVFbGVtZW50KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRyb3Bkb3duRWxlbWVudCA9IHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudDtcclxuICAgICAgICAgICAgY29uc3QgZHJvcGRvd25NZW51RWxlbWVudCA9IHRoaXMuX21lbnVFbGVtZW50Lm5hdGl2ZUVsZW1lbnQ7XHJcblxyXG4gICAgICAgICAgICByZW5kZXJlci5hcHBlbmRDaGlsZChkcm9wZG93bkVsZW1lbnQsIGRyb3Bkb3duTWVudUVsZW1lbnQpO1xyXG4gICAgICAgICAgICByZW5kZXJlci5yZW1vdmVTdHlsZShkcm9wZG93bk1lbnVFbGVtZW50LCAncG9zaXRpb24nKTtcclxuICAgICAgICAgICAgcmVuZGVyZXIucmVtb3ZlU3R5bGUoZHJvcGRvd25NZW51RWxlbWVudCwgJ3RyYW5zZm9ybScpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5fYm9keUNvbnRhaW5lcikge1xyXG4gICAgICAgICAgICByZW5kZXJlci5yZW1vdmVDaGlsZCh0aGlzLl9kb2N1bWVudC5ib2R5LCB0aGlzLl9ib2R5Q29udGFpbmVyKTtcclxuICAgICAgICAgICAgdGhpcy5fYm9keUNvbnRhaW5lciA9IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2FwcGx5Q29udGFpbmVyKGNvbnRhaW5lcjogbnVsbCB8ICdib2R5JyA9IG51bGwpIHtcclxuICAgICAgICB0aGlzLl9yZXNldENvbnRhaW5lcigpO1xyXG4gICAgICAgIGlmIChjb250YWluZXIgPT09ICdib2R5Jykge1xyXG4gICAgICAgICAgICBjb25zdCByZW5kZXJlciA9IHRoaXMuX3JlbmRlcmVyO1xyXG4gICAgICAgICAgICBjb25zdCBkcm9wZG93bk1lbnVFbGVtZW50ID0gdGhpcy5fbWVudUVsZW1lbnQubmF0aXZlRWxlbWVudDtcclxuICAgICAgICAgICAgY29uc3QgYm9keUNvbnRhaW5lciA9IHRoaXMuX2JvZHlDb250YWluZXIgPSB0aGlzLl9ib2R5Q29udGFpbmVyIHx8IHJlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG5cclxuICAgICAgICAgICAgLy8gT3ZlcnJpZGUgc29tZSBzdHlsZXMgdG8gaGF2ZSB0aGUgcG9zaXRpb25uaW5nIHdvcmtpbmdcclxuICAgICAgICAgICAgcmVuZGVyZXIuc2V0U3R5bGUoYm9keUNvbnRhaW5lciwgJ3Bvc2l0aW9uJywgJ2Fic29sdXRlJyk7XHJcbiAgICAgICAgICAgIHJlbmRlcmVyLnNldFN0eWxlKGRyb3Bkb3duTWVudUVsZW1lbnQsICdwb3NpdGlvbicsICdzdGF0aWMnKTtcclxuXHJcbiAgICAgICAgICAgIHJlbmRlcmVyLmFwcGVuZENoaWxkKGJvZHlDb250YWluZXIsIGRyb3Bkb3duTWVudUVsZW1lbnQpO1xyXG4gICAgICAgICAgICByZW5kZXJlci5hcHBlbmRDaGlsZCh0aGlzLl9kb2N1bWVudC5ib2R5LCBib2R5Q29udGFpbmVyKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfYXBwbHlQbGFjZW1lbnRDbGFzc2VzKHBsYWNlbWVudD86IFBsYWNlbWVudCkge1xyXG4gICAgICAgIGlmICh0aGlzLl9tZW51KSB7XHJcbiAgICAgICAgICAgIGlmICghcGxhY2VtZW50KSB7XHJcbiAgICAgICAgICAgICAgICBwbGFjZW1lbnQgPSBBcnJheS5pc0FycmF5KHRoaXMucGxhY2VtZW50KSA/IHRoaXMucGxhY2VtZW50WzBdIDogdGhpcy5wbGFjZW1lbnQgYXMgUGxhY2VtZW50O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCByZW5kZXJlciA9IHRoaXMuX3JlbmRlcmVyO1xyXG4gICAgICAgICAgICBjb25zdCBkcm9wZG93bkVsZW1lbnQgPSB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQ7XHJcblxyXG4gICAgICAgICAgICAvLyByZW1vdmUgdGhlIGN1cnJlbnQgcGxhY2VtZW50IGNsYXNzZXNcclxuICAgICAgICAgICAgcmVuZGVyZXIucmVtb3ZlQ2xhc3MoZHJvcGRvd25FbGVtZW50LCAnZHJvcHVwJyk7XHJcbiAgICAgICAgICAgIHJlbmRlcmVyLnJlbW92ZUNsYXNzKGRyb3Bkb3duRWxlbWVudCwgJ2Ryb3Bkb3duJyk7XHJcbiAgICAgICAgICAgIHRoaXMucGxhY2VtZW50ID0gcGxhY2VtZW50O1xyXG4gICAgICAgICAgICB0aGlzLl9tZW51LnBsYWNlbWVudCA9IHBsYWNlbWVudDtcclxuXHJcbiAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICogYXBwbHkgdGhlIG5ldyBwbGFjZW1lbnRcclxuICAgICAgICAgICAgKiBpbiBjYXNlIG9mIHRvcCB1c2UgdXAtYXJyb3cgb3IgZG93bi1hcnJvdyBvdGhlcndpc2VcclxuICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgY29uc3QgZHJvcGRvd25DbGFzcyA9IHBsYWNlbWVudC5zZWFyY2goJ150b3AnKSAhPT0gLTEgPyAnZHJvcHVwJyA6ICdkcm9wZG93bic7XHJcbiAgICAgICAgICAgIHJlbmRlcmVyLmFkZENsYXNzKGRyb3Bkb3duRWxlbWVudCwgZHJvcGRvd25DbGFzcyk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBib2R5Q29udGFpbmVyID0gdGhpcy5fYm9keUNvbnRhaW5lcjtcclxuICAgICAgICAgICAgaWYgKGJvZHlDb250YWluZXIpIHtcclxuICAgICAgICAgICAgICAgIHJlbmRlcmVyLnJlbW92ZUNsYXNzKGJvZHlDb250YWluZXIsICdkcm9wdXAnKTtcclxuICAgICAgICAgICAgICAgIHJlbmRlcmVyLnJlbW92ZUNsYXNzKGJvZHlDb250YWluZXIsICdkcm9wZG93bicpO1xyXG4gICAgICAgICAgICAgICAgcmVuZGVyZXIuYWRkQ2xhc3MoYm9keUNvbnRhaW5lciwgZHJvcGRvd25DbGFzcyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19