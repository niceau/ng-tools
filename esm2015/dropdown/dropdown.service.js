/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
export class NgtDropdownService {
    constructor() {
        this._isOpen = new BehaviorSubject(false);
        this.onToggleChange = new Subject();
    }
    /**
     * @return {?}
     */
    isOpen() {
        return this._isOpen.value;
    }
}
NgtDropdownService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
NgtDropdownService.ctorParameters = () => [];
if (false) {
    /** @type {?} */
    NgtDropdownService.prototype._isOpen;
    /** @type {?} */
    NgtDropdownService.prototype.onToggleChange;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiZHJvcGRvd24vZHJvcGRvd24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUdoRCxNQUFNLE9BQU8sa0JBQWtCO0lBSTNCO1FBSEEsWUFBTyxHQUFHLElBQUksZUFBZSxDQUFVLEtBQUssQ0FBQyxDQUFDO1FBQzlDLG1CQUFjLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztJQUcvQixDQUFDOzs7O0lBRUQsTUFBTTtRQUNGLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUM7SUFDOUIsQ0FBQzs7O1lBVkosVUFBVTs7Ozs7O0lBRVAscUNBQThDOztJQUM5Qyw0Q0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgTmd0RHJvcGRvd25TZXJ2aWNlIHtcclxuICAgIF9pc09wZW4gPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGZhbHNlKTtcclxuICAgIG9uVG9nZ2xlQ2hhbmdlID0gbmV3IFN1YmplY3QoKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgIH1cclxuXHJcbiAgICBpc09wZW4oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzT3Blbi52YWx1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=