/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ContentChildren, Directive, ElementRef, HostBinding, QueryList, Renderer2 } from '@angular/core';
import { NgtDropdownItemDirective } from './dropdown-item.directive';
import { positionElements } from '../util/positioning';
import { NgtDropdownService } from './dropdown.service';
/**
 *
 */
export class NgtDropdownMenuDirective {
    /**
     * @param {?} _elementRef
     * @param {?} $dropdownService
     * @param {?} _renderer
     */
    constructor(_elementRef, $dropdownService, _renderer) {
        this._elementRef = _elementRef;
        this.$dropdownService = $dropdownService;
        this._renderer = _renderer;
        this.placement = 'bottom';
        this.isOpen = false;
        this.dropdownMenu = true;
        this.show = false;
        this.xPlacement = this.placement;
        this.isOpenSubscription = this.$dropdownService._isOpen.subscribe((/**
         * @param {?} status
         * @return {?}
         */
        status => {
            this.show = status;
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.isOpenSubscription.unsubscribe();
    }
    /**
     * @return {?}
     */
    getNativeElement() {
        return this._elementRef.nativeElement;
    }
    /**
     * @param {?} triggerEl
     * @param {?} placement
     * @return {?}
     */
    position(triggerEl, placement) {
        this.applyPlacement(positionElements(triggerEl, this._elementRef.nativeElement, placement));
    }
    /**
     * @param {?} _placement
     * @return {?}
     */
    applyPlacement(_placement) {
        // remove the current placement classes
        this._renderer.removeClass(this._elementRef.nativeElement.parentNode, 'dropup');
        this._renderer.removeClass(this._elementRef.nativeElement.parentNode, 'dropdown');
        this.placement = _placement;
        /**
         * apply the new placement
         * in case of top use up-arrow or down-arrow otherwise
         */
        if (_placement.search('^top') !== -1) {
            this._renderer.addClass(this._elementRef.nativeElement.parentNode, 'dropup');
        }
        else {
            this._renderer.addClass(this._elementRef.nativeElement.parentNode, 'dropdown');
        }
    }
}
NgtDropdownMenuDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdownMenu]'
            },] }
];
/** @nocollapse */
NgtDropdownMenuDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: NgtDropdownService },
    { type: Renderer2 }
];
NgtDropdownMenuDirective.propDecorators = {
    menuItems: [{ type: ContentChildren, args: [NgtDropdownItemDirective,] }],
    dropdownMenu: [{ type: HostBinding, args: ['class.dropdown-menu',] }],
    show: [{ type: HostBinding, args: ['class.show',] }],
    xPlacement: [{ type: HostBinding, args: ['attr.x-placement',] }]
};
if (false) {
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.placement;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.isOpen;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.isOpenSubscription;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.menuItems;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.dropdownMenu;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.show;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.xPlacement;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownMenuDirective.prototype._elementRef;
    /** @type {?} */
    NgtDropdownMenuDirective.prototype.$dropdownService;
    /**
     * @type {?}
     * @private
     */
    NgtDropdownMenuDirective.prototype._renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tbWVudS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImRyb3Bkb3duL2Ryb3Bkb3duLW1lbnUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsZUFBZSxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFhLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckgsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDckUsT0FBTyxFQUFhLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFbEUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7Ozs7QUFTeEQsTUFBTSxPQUFPLHdCQUF3Qjs7Ozs7O0lBV2pDLFlBQW9CLFdBQW9DLEVBQ3JDLGdCQUFvQyxFQUNuQyxTQUFvQjtRQUZwQixnQkFBVyxHQUFYLFdBQVcsQ0FBeUI7UUFDckMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFvQjtRQUNuQyxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBWnhDLGNBQVMsR0FBYyxRQUFRLENBQUM7UUFDaEMsV0FBTSxHQUFHLEtBQUssQ0FBQztRQUtxQixpQkFBWSxHQUFHLElBQUksQ0FBQztRQUM3QixTQUFJLEdBQUcsS0FBSyxDQUFDO1FBQ1AsZUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFLekQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsU0FBUzs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFO1lBQ3ZFLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDMUMsQ0FBQzs7OztJQUVELGdCQUFnQjtRQUNaLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7SUFDMUMsQ0FBQzs7Ozs7O0lBRUQsUUFBUSxDQUFDLFNBQVMsRUFBRSxTQUFTO1FBQ3pCLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDaEcsQ0FBQzs7Ozs7SUFFRCxjQUFjLENBQUMsVUFBcUI7UUFDaEMsdUNBQXVDO1FBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNoRixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDbEYsSUFBSSxDQUFDLFNBQVMsR0FBRyxVQUFVLENBQUM7UUFDNUI7OztXQUdHO1FBQ0gsSUFBSSxVQUFVLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxRQUFRLENBQUMsQ0FBQztTQUNoRjthQUFNO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1NBQ2xGO0lBQ0wsQ0FBQzs7O1lBaERKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsbUJBQW1CO2FBQ2hDOzs7O1lBWm9DLFVBQVU7WUFJdEMsa0JBQWtCO1lBSnlELFNBQVM7Ozt3QkFrQnhGLGVBQWUsU0FBQyx3QkFBd0I7MkJBRXhDLFdBQVcsU0FBQyxxQkFBcUI7bUJBQ2pDLFdBQVcsU0FBQyxZQUFZO3lCQUN4QixXQUFXLFNBQUMsa0JBQWtCOzs7O0lBUi9CLDZDQUFnQzs7SUFDaEMsMENBQWU7O0lBQ2Ysc0RBQWlDOztJQUVqQyw2Q0FBMEY7O0lBRTFGLGdEQUF3RDs7SUFDeEQsd0NBQXdDOztJQUN4Qyw4Q0FBNkQ7Ozs7O0lBRWpELCtDQUE0Qzs7SUFDNUMsb0RBQTJDOzs7OztJQUMzQyw2Q0FBNEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb250ZW50Q2hpbGRyZW4sIERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSG9zdEJpbmRpbmcsIE9uRGVzdHJveSwgUXVlcnlMaXN0LCBSZW5kZXJlcjIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmd0RHJvcGRvd25JdGVtRGlyZWN0aXZlIH0gZnJvbSAnLi9kcm9wZG93bi1pdGVtLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IFBsYWNlbWVudCwgcG9zaXRpb25FbGVtZW50cyB9IGZyb20gJy4uL3V0aWwvcG9zaXRpb25pbmcnO1xyXG5cclxuaW1wb3J0IHsgTmd0RHJvcGRvd25TZXJ2aWNlIH0gZnJvbSAnLi9kcm9wZG93bi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcblxyXG5cclxuLyoqXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndERyb3Bkb3duTWVudV0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3REcm9wZG93bk1lbnVEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkRlc3Ryb3kge1xyXG4gICAgcGxhY2VtZW50OiBQbGFjZW1lbnQgPSAnYm90dG9tJztcclxuICAgIGlzT3BlbiA9IGZhbHNlO1xyXG4gICAgaXNPcGVuU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblxyXG4gICAgQENvbnRlbnRDaGlsZHJlbihOZ3REcm9wZG93bkl0ZW1EaXJlY3RpdmUpIG1lbnVJdGVtczogUXVlcnlMaXN0PE5ndERyb3Bkb3duSXRlbURpcmVjdGl2ZT47XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5kcm9wZG93bi1tZW51JykgZHJvcGRvd25NZW51ID0gdHJ1ZTtcclxuICAgIEBIb3N0QmluZGluZygnY2xhc3Muc2hvdycpIHNob3cgPSBmYWxzZTtcclxuICAgIEBIb3N0QmluZGluZygnYXR0ci54LXBsYWNlbWVudCcpIHhQbGFjZW1lbnQgPSB0aGlzLnBsYWNlbWVudDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9lbGVtZW50UmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PixcclxuICAgICAgICAgICAgICAgIHB1YmxpYyAkZHJvcGRvd25TZXJ2aWNlOiBOZ3REcm9wZG93blNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9yZW5kZXJlcjogUmVuZGVyZXIyKSB7XHJcbiAgICAgICAgdGhpcy5pc09wZW5TdWJzY3JpcHRpb24gPSB0aGlzLiRkcm9wZG93blNlcnZpY2UuX2lzT3Blbi5zdWJzY3JpYmUoc3RhdHVzID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zaG93ID0gc3RhdHVzO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuaXNPcGVuU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TmF0aXZlRWxlbWVudCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIHBvc2l0aW9uKHRyaWdnZXJFbCwgcGxhY2VtZW50KSB7XHJcbiAgICAgICAgdGhpcy5hcHBseVBsYWNlbWVudChwb3NpdGlvbkVsZW1lbnRzKHRyaWdnZXJFbCwgdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCBwbGFjZW1lbnQpKTtcclxuICAgIH1cclxuXHJcbiAgICBhcHBseVBsYWNlbWVudChfcGxhY2VtZW50OiBQbGFjZW1lbnQpIHtcclxuICAgICAgICAvLyByZW1vdmUgdGhlIGN1cnJlbnQgcGxhY2VtZW50IGNsYXNzZXNcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQucGFyZW50Tm9kZSwgJ2Ryb3B1cCcpO1xyXG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZUNsYXNzKHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5wYXJlbnROb2RlLCAnZHJvcGRvd24nKTtcclxuICAgICAgICB0aGlzLnBsYWNlbWVudCA9IF9wbGFjZW1lbnQ7XHJcbiAgICAgICAgLyoqXHJcbiAgICAgICAgICogYXBwbHkgdGhlIG5ldyBwbGFjZW1lbnRcclxuICAgICAgICAgKiBpbiBjYXNlIG9mIHRvcCB1c2UgdXAtYXJyb3cgb3IgZG93bi1hcnJvdyBvdGhlcndpc2VcclxuICAgICAgICAgKi9cclxuICAgICAgICBpZiAoX3BsYWNlbWVudC5zZWFyY2goJ150b3AnKSAhPT0gLTEpIHtcclxuICAgICAgICAgICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnBhcmVudE5vZGUsICdkcm9wdXAnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQucGFyZW50Tm9kZSwgJ2Ryb3Bkb3duJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==