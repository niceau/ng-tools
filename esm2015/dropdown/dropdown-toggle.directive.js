/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, ElementRef, forwardRef, HostBinding, HostListener } from '@angular/core';
import { NgtDropdownAnchorDirective } from './dropdown-anchor.directive';
import { NgtDropdownService } from './dropdown.service';
/**
 * Allows the dropdown to be toggled via click. This directive is optional: you can use NgtDropdownAnchorDirective as an
 * alternative.
 */
export class NgtDropdownToggleDirective extends NgtDropdownAnchorDirective {
    /**
     * @param {?} elementRef
     * @param {?} $dropdownService
     */
    constructor(elementRef, $dropdownService) {
        super(elementRef, $dropdownService);
        this.class = true;
        this.ariaHaspopup = true;
        this.ariaExpanded = this.$dropdownService.isOpen();
    }
    /**
     * @return {?}
     */
    onClick() {
        this.toggleOpen();
    }
    /**
     * @return {?}
     */
    toggleOpen() {
        this.$dropdownService.onToggleChange.next();
    }
}
NgtDropdownToggleDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtDropdownToggle]',
                providers: [{ provide: NgtDropdownAnchorDirective, useExisting: forwardRef((/**
                         * @return {?}
                         */
                        () => NgtDropdownToggleDirective)) }]
            },] }
];
/** @nocollapse */
NgtDropdownToggleDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: NgtDropdownService }
];
NgtDropdownToggleDirective.propDecorators = {
    class: [{ type: HostBinding, args: ['class.dropdown-toggle',] }],
    ariaHaspopup: [{ type: HostBinding, args: ['attr.aria-haspopup.true',] }],
    ariaExpanded: [{ type: HostBinding, args: ['attr.aria-expanded',] }],
    onClick: [{ type: HostListener, args: ['click',] }]
};
if (false) {
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.class;
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.ariaHaspopup;
    /** @type {?} */
    NgtDropdownToggleDirective.prototype.ariaExpanded;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24tdG9nZ2xlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiZHJvcGRvd24vZHJvcGRvd24tdG9nZ2xlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0YsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDekUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7Ozs7O0FBVXhELE1BQU0sT0FBTywwQkFBMkIsU0FBUSwwQkFBMEI7Ozs7O0lBVXRFLFlBQVksVUFBbUMsRUFBRSxnQkFBb0M7UUFDakYsS0FBSyxDQUFDLFVBQVUsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBVEYsVUFBSyxHQUFHLElBQUksQ0FBQztRQUNYLGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQ3pCLGlCQUFZLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxDQUFDO0lBUWpGLENBQUM7Ozs7SUFOc0IsT0FBTztRQUMxQixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7OztJQU1ELFVBQVU7UUFDTixJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2hELENBQUM7OztZQXBCSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHFCQUFxQjtnQkFDL0IsU0FBUyxFQUFFLENBQUMsRUFBQyxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsV0FBVyxFQUFFLFVBQVU7Ozt3QkFBQyxHQUFHLEVBQUUsQ0FBQywwQkFBMEIsRUFBQyxFQUFDLENBQUM7YUFDaEg7Ozs7WUFYbUIsVUFBVTtZQUVyQixrQkFBa0I7OztvQkFZdEIsV0FBVyxTQUFDLHVCQUF1QjsyQkFDbkMsV0FBVyxTQUFDLHlCQUF5QjsyQkFDckMsV0FBVyxTQUFDLG9CQUFvQjtzQkFFaEMsWUFBWSxTQUFDLE9BQU87Ozs7SUFKckIsMkNBQW1EOztJQUNuRCxrREFBNEQ7O0lBQzVELGtEQUFpRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgZm9yd2FyZFJlZiwgSG9zdEJpbmRpbmcsIEhvc3RMaXN0ZW5lciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOZ3REcm9wZG93bkFuY2hvckRpcmVjdGl2ZSB9IGZyb20gJy4vZHJvcGRvd24tYW5jaG9yLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IE5ndERyb3Bkb3duU2VydmljZSB9IGZyb20gJy4vZHJvcGRvd24uc2VydmljZSc7XHJcblxyXG4vKipcclxuICogQWxsb3dzIHRoZSBkcm9wZG93biB0byBiZSB0b2dnbGVkIHZpYSBjbGljay4gVGhpcyBkaXJlY3RpdmUgaXMgb3B0aW9uYWw6IHlvdSBjYW4gdXNlIE5ndERyb3Bkb3duQW5jaG9yRGlyZWN0aXZlIGFzIGFuXHJcbiAqIGFsdGVybmF0aXZlLlxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3REcm9wZG93blRvZ2dsZV0nLFxyXG4gICAgcHJvdmlkZXJzOiBbe3Byb3ZpZGU6IE5ndERyb3Bkb3duQW5jaG9yRGlyZWN0aXZlLCB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBOZ3REcm9wZG93blRvZ2dsZURpcmVjdGl2ZSl9XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0RHJvcGRvd25Ub2dnbGVEaXJlY3RpdmUgZXh0ZW5kcyBOZ3REcm9wZG93bkFuY2hvckRpcmVjdGl2ZSB7XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5kcm9wZG93bi10b2dnbGUnKSBjbGFzcyA9IHRydWU7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2F0dHIuYXJpYS1oYXNwb3B1cC50cnVlJykgYXJpYUhhc3BvcHVwID0gdHJ1ZTtcclxuICAgIEBIb3N0QmluZGluZygnYXR0ci5hcmlhLWV4cGFuZGVkJykgYXJpYUV4cGFuZGVkID0gdGhpcy4kZHJvcGRvd25TZXJ2aWNlLmlzT3BlbigpO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJykgb25DbGljaygpIHtcclxuICAgICAgICB0aGlzLnRvZ2dsZU9wZW4oKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihlbGVtZW50UmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50PiwgJGRyb3Bkb3duU2VydmljZTogTmd0RHJvcGRvd25TZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIoZWxlbWVudFJlZiwgJGRyb3Bkb3duU2VydmljZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdG9nZ2xlT3BlbigpIHtcclxuICAgICAgICB0aGlzLiRkcm9wZG93blNlcnZpY2Uub25Ub2dnbGVDaGFuZ2UubmV4dCgpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==