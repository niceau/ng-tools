/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { merge, Observable } from 'rxjs';
import { delay, filter, map, share } from 'rxjs/operators';
export class Trigger {
    /**
     * @param {?} open
     * @param {?=} close
     */
    constructor(open, close) {
        this.open = open;
        this.close = close;
        if (!close) {
            this.close = open;
        }
    }
    /**
     * @return {?}
     */
    isManual() {
        return this.open === 'manual' || this.close === 'manual';
    }
}
if (false) {
    /** @type {?} */
    Trigger.prototype.open;
    /** @type {?} */
    Trigger.prototype.close;
}
/** @type {?} */
const DEFAULT_ALIASES = {
    'hover': ['mouseenter', 'mouseleave'],
    'focus': ['focusin', 'focusout'],
};
/**
 * @param {?} triggers
 * @param {?=} aliases
 * @return {?}
 */
export function parseTriggers(triggers, aliases = DEFAULT_ALIASES) {
    /** @type {?} */
    const trimmedTriggers = (triggers || '').trim();
    if (trimmedTriggers.length === 0) {
        return [];
    }
    /** @type {?} */
    const parsedTriggers = trimmedTriggers.split(/\s+/).map((/**
     * @param {?} trigger
     * @return {?}
     */
    trigger => trigger.split(':'))).map((/**
     * @param {?} triggerPair
     * @return {?}
     */
    (triggerPair) => {
        /** @type {?} */
        let alias = aliases[triggerPair[0]] || triggerPair;
        return new Trigger(alias[0], alias[1]);
    }));
    /** @type {?} */
    const manualTriggers = parsedTriggers.filter((/**
     * @param {?} triggerPair
     * @return {?}
     */
    triggerPair => triggerPair.isManual()));
    if (manualTriggers.length > 1) {
        throw 'Triggers parse error: only one manual trigger is allowed';
    }
    if (manualTriggers.length === 1 && parsedTriggers.length > 1) {
        throw 'Triggers parse error: manual trigger can\'t be mixed with other triggers';
    }
    return parsedTriggers;
}
/**
 * @param {?} renderer
 * @param {?} nativeElement
 * @param {?} triggers
 * @param {?} isOpenedFn
 * @return {?}
 */
export function observeTriggers(renderer, nativeElement, triggers, isOpenedFn) {
    return new Observable((/**
     * @param {?} subscriber
     * @return {?}
     */
    subscriber => {
        /** @type {?} */
        const listeners = [];
        /** @type {?} */
        const openFn = (/**
         * @return {?}
         */
        () => subscriber.next(true));
        /** @type {?} */
        const closeFn = (/**
         * @return {?}
         */
        () => subscriber.next(false));
        /** @type {?} */
        const toggleFn = (/**
         * @return {?}
         */
        () => subscriber.next(!isOpenedFn()));
        triggers.forEach((/**
         * @param {?} trigger
         * @return {?}
         */
        (trigger) => {
            if (trigger.open === trigger.close) {
                listeners.push(renderer.listen(nativeElement, trigger.open, toggleFn));
            }
            else {
                listeners.push(renderer.listen(nativeElement, trigger.open, openFn), renderer.listen(nativeElement, trigger.close, closeFn));
            }
        }));
        return (/**
         * @return {?}
         */
        () => {
            listeners.forEach((/**
             * @param {?} unsubscribeFn
             * @return {?}
             */
            unsubscribeFn => unsubscribeFn()));
        });
    }));
}
/** @type {?} */
const delayOrNoop = (/**
 * @template T
 * @param {?} time
 * @return {?}
 */
(time) => time > 0 ? delay(time) : (/**
 * @param {?} a
 * @return {?}
 */
(a) => a));
const ɵ0 = delayOrNoop;
/**
 * @param {?} openDelay
 * @param {?} closeDelay
 * @param {?} isOpenedFn
 * @return {?}
 */
export function triggerDelay(openDelay, closeDelay, isOpenedFn) {
    return (/**
     * @param {?} input$
     * @return {?}
     */
    (input$) => {
        /** @type {?} */
        let pending = null;
        /** @type {?} */
        const filteredInput$ = input$.pipe(map((/**
         * @param {?} open
         * @return {?}
         */
        open => ({ open }))), filter((/**
         * @param {?} event
         * @return {?}
         */
        event => {
            /** @type {?} */
            const currentlyOpen = isOpenedFn();
            if (currentlyOpen !== event.open && (!pending || pending.open === currentlyOpen)) {
                pending = event;
                return true;
            }
            if (pending && pending.open !== event.open) {
                pending = null;
            }
            return false;
        })), share());
        /** @type {?} */
        const delayedOpen$ = filteredInput$.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => event.open)), delayOrNoop(openDelay));
        /** @type {?} */
        const delayedClose$ = filteredInput$.pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => !event.open)), delayOrNoop(closeDelay));
        return merge(delayedOpen$, delayedClose$)
            .pipe(filter((/**
         * @param {?} event
         * @return {?}
         */
        event => {
            if (event === pending) {
                pending = null;
                return event.open !== isOpenedFn();
            }
            return false;
        })), map((/**
         * @param {?} event
         * @return {?}
         */
        event => event.open)));
    });
}
/**
 * @param {?} renderer
 * @param {?} nativeElement
 * @param {?} triggers
 * @param {?} isOpenedFn
 * @param {?} openFn
 * @param {?} closeFn
 * @param {?=} openDelay
 * @param {?=} closeDelay
 * @return {?}
 */
export function listenToTriggers(renderer, nativeElement, triggers, isOpenedFn, openFn, closeFn, openDelay = 0, closeDelay = 0) {
    /** @type {?} */
    const parsedTriggers = parseTriggers(triggers);
    if (parsedTriggers.length === 1 && parsedTriggers[0].isManual()) {
        return (/**
         * @return {?}
         */
        () => {
        });
    }
    /** @type {?} */
    const subscription = observeTriggers(renderer, nativeElement, parsedTriggers, isOpenedFn)
        .pipe(triggerDelay(openDelay, closeDelay, isOpenedFn))
        .subscribe((/**
     * @param {?} open
     * @return {?}
     */
    open => (open ? openFn() : closeFn())));
    return (/**
     * @return {?}
     */
    () => subscription.unsubscribe());
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJpZ2dlcnMuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInV0aWwvdHJpZ2dlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUUzRCxNQUFNLE9BQU8sT0FBTzs7Ozs7SUFDaEIsWUFBbUIsSUFBWSxFQUFTLEtBQWM7UUFBbkMsU0FBSSxHQUFKLElBQUksQ0FBUTtRQUFTLFVBQUssR0FBTCxLQUFLLENBQVM7UUFDbEQsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNSLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1NBQ3JCO0lBQ0wsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixPQUFPLElBQUksQ0FBQyxJQUFJLEtBQUssUUFBUSxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssUUFBUSxDQUFDO0lBQzdELENBQUM7Q0FDSjs7O0lBVGUsdUJBQW1COztJQUFFLHdCQUFxQjs7O01BV3BELGVBQWUsR0FBRztJQUNwQixPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsWUFBWSxDQUFDO0lBQ3JDLE9BQU8sRUFBRSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUM7Q0FDbkM7Ozs7OztBQUVELE1BQU0sVUFBVSxhQUFhLENBQUMsUUFBZ0IsRUFBRSxPQUFPLEdBQUcsZUFBZTs7VUFDL0QsZUFBZSxHQUFHLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRTtJQUUvQyxJQUFJLGVBQWUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1FBQzlCLE9BQU8sRUFBRSxDQUFDO0tBQ2I7O1VBRUssY0FBYyxHQUFHLGVBQWUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRzs7OztJQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUFDLEdBQUc7Ozs7SUFBQyxDQUFDLFdBQVcsRUFBRSxFQUFFOztZQUNuRyxLQUFLLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLFdBQVc7UUFDbEQsT0FBTyxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDM0MsQ0FBQyxFQUFDOztVQUVJLGNBQWMsR0FBRyxjQUFjLENBQUMsTUFBTTs7OztJQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxFQUFDO0lBRW5GLElBQUksY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7UUFDM0IsTUFBTSwwREFBMEQsQ0FBQztLQUNwRTtJQUVELElBQUksY0FBYyxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7UUFDMUQsTUFBTSwwRUFBMEUsQ0FBQztLQUNwRjtJQUVELE9BQU8sY0FBYyxDQUFDO0FBQzFCLENBQUM7Ozs7Ozs7O0FBRUQsTUFBTSxVQUFVLGVBQWUsQ0FBQyxRQUFhLEVBQUUsYUFBa0IsRUFBRSxRQUFtQixFQUFFLFVBQXlCO0lBQzdHLE9BQU8sSUFBSSxVQUFVOzs7O0lBQVUsVUFBVSxDQUFDLEVBQUU7O2NBQ2xDLFNBQVMsR0FBRyxFQUFFOztjQUNkLE1BQU07OztRQUFHLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUE7O2NBQ3BDLE9BQU87OztRQUFHLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7O2NBQ3RDLFFBQVE7OztRQUFHLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFBO1FBRXJELFFBQVEsQ0FBQyxPQUFPOzs7O1FBQUMsQ0FBQyxPQUFnQixFQUFFLEVBQUU7WUFDbEMsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLE9BQU8sQ0FBQyxLQUFLLEVBQUU7Z0JBQ2hDLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDO2FBQzFFO2lCQUFNO2dCQUNILFNBQVMsQ0FBQyxJQUFJLENBQ1YsUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsRUFDcEQsUUFBUSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDO2FBQy9EO1FBQ0wsQ0FBQyxFQUFDLENBQUM7UUFFSDs7O1FBQU8sR0FBRyxFQUFFO1lBQ1IsU0FBUyxDQUFDLE9BQU87Ozs7WUFBQyxhQUFhLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxFQUFDLENBQUM7UUFDeEQsQ0FBQyxFQUFDO0lBQ04sQ0FBQyxFQUFDLENBQUM7QUFDUCxDQUFDOztNQUVLLFdBQVc7Ozs7O0FBQUcsQ0FBSSxJQUFZLEVBQUUsRUFBRSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBSSxJQUFJLENBQUMsQ0FBQyxDQUFDOzs7O0FBQUMsQ0FBQyxDQUFnQixFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUEsQ0FBQTs7Ozs7Ozs7QUFFNUYsTUFBTSxVQUFVLFlBQVksQ0FBQyxTQUFpQixFQUFFLFVBQWtCLEVBQUUsVUFBeUI7SUFDekY7Ozs7SUFBTyxDQUFDLE1BQTJCLEVBQUUsRUFBRTs7WUFDL0IsT0FBTyxHQUFHLElBQUk7O2NBQ1osY0FBYyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQzlCLEdBQUc7Ozs7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsRUFBQyxJQUFJLEVBQUMsQ0FBQyxFQUFDLEVBQUUsTUFBTTs7OztRQUFDLEtBQUssQ0FBQyxFQUFFOztrQkFDNUIsYUFBYSxHQUFHLFVBQVUsRUFBRTtZQUNsQyxJQUFJLGFBQWEsS0FBSyxLQUFLLENBQUMsSUFBSSxJQUFJLENBQUMsQ0FBQyxPQUFPLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxhQUFhLENBQUMsRUFBRTtnQkFDOUUsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDaEIsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUNELElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssS0FBSyxDQUFDLElBQUksRUFBRTtnQkFDeEMsT0FBTyxHQUFHLElBQUksQ0FBQzthQUNsQjtZQUNELE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUMsRUFBQyxFQUNGLEtBQUssRUFBRSxDQUFDOztjQUNOLFlBQVksR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU07Ozs7UUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUMsRUFBRSxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7O2NBQ3ZGLGFBQWEsR0FBRyxjQUFjLENBQUMsSUFBSSxDQUFDLE1BQU07Ozs7UUFBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksRUFBQyxFQUFFLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNoRyxPQUFPLEtBQUssQ0FBQyxZQUFZLEVBQUUsYUFBYSxDQUFDO2FBQ3BDLElBQUksQ0FDRCxNQUFNOzs7O1FBQUMsS0FBSyxDQUFDLEVBQUU7WUFDWCxJQUFJLEtBQUssS0FBSyxPQUFPLEVBQUU7Z0JBQ25CLE9BQU8sR0FBRyxJQUFJLENBQUM7Z0JBQ2YsT0FBTyxLQUFLLENBQUMsSUFBSSxLQUFLLFVBQVUsRUFBRSxDQUFDO2FBQ3RDO1lBQ0QsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQyxFQUFDLEVBQ0YsR0FBRzs7OztRQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDLElBQUksRUFBQyxDQUFDLENBQUM7SUFDdEMsQ0FBQyxFQUFDO0FBQ04sQ0FBQzs7Ozs7Ozs7Ozs7O0FBRUQsTUFBTSxVQUFVLGdCQUFnQixDQUM1QixRQUFhLEVBQUUsYUFBa0IsRUFBRSxRQUFnQixFQUFFLFVBQXlCLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxTQUFTLEdBQUcsQ0FBQyxFQUM5RyxVQUFVLEdBQUcsQ0FBQzs7VUFDUixjQUFjLEdBQUcsYUFBYSxDQUFDLFFBQVEsQ0FBQztJQUU5QyxJQUFJLGNBQWMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxJQUFJLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtRQUM3RDs7O1FBQU8sR0FBRyxFQUFFO1FBQ1osQ0FBQyxFQUFDO0tBQ0w7O1VBRUssWUFBWSxHQUFHLGVBQWUsQ0FBQyxRQUFRLEVBQUUsYUFBYSxFQUFFLGNBQWMsRUFBRSxVQUFVLENBQUM7U0FDcEYsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQUUsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1NBQ3JELFNBQVM7Ozs7SUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUMsRUFBQztJQUVyRDs7O0lBQU8sR0FBRyxFQUFFLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxFQUFDO0FBQzVDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBtZXJnZSwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBkZWxheSwgZmlsdGVyLCBtYXAsIHNoYXJlIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFRyaWdnZXIge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIG9wZW46IHN0cmluZywgcHVibGljIGNsb3NlPzogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKCFjbG9zZSkge1xyXG4gICAgICAgICAgICB0aGlzLmNsb3NlID0gb3BlbjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaXNNYW51YWwoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub3BlbiA9PT0gJ21hbnVhbCcgfHwgdGhpcy5jbG9zZSA9PT0gJ21hbnVhbCc7XHJcbiAgICB9XHJcbn1cclxuXHJcbmNvbnN0IERFRkFVTFRfQUxJQVNFUyA9IHtcclxuICAgICdob3Zlcic6IFsnbW91c2VlbnRlcicsICdtb3VzZWxlYXZlJ10sXHJcbiAgICAnZm9jdXMnOiBbJ2ZvY3VzaW4nLCAnZm9jdXNvdXQnXSxcclxufTtcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBwYXJzZVRyaWdnZXJzKHRyaWdnZXJzOiBzdHJpbmcsIGFsaWFzZXMgPSBERUZBVUxUX0FMSUFTRVMpOiBUcmlnZ2VyW10ge1xyXG4gICAgY29uc3QgdHJpbW1lZFRyaWdnZXJzID0gKHRyaWdnZXJzIHx8ICcnKS50cmltKCk7XHJcblxyXG4gICAgaWYgKHRyaW1tZWRUcmlnZ2Vycy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICByZXR1cm4gW107XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3QgcGFyc2VkVHJpZ2dlcnMgPSB0cmltbWVkVHJpZ2dlcnMuc3BsaXQoL1xccysvKS5tYXAodHJpZ2dlciA9PiB0cmlnZ2VyLnNwbGl0KCc6JykpLm1hcCgodHJpZ2dlclBhaXIpID0+IHtcclxuICAgICAgICBsZXQgYWxpYXMgPSBhbGlhc2VzW3RyaWdnZXJQYWlyWzBdXSB8fCB0cmlnZ2VyUGFpcjtcclxuICAgICAgICByZXR1cm4gbmV3IFRyaWdnZXIoYWxpYXNbMF0sIGFsaWFzWzFdKTtcclxuICAgIH0pO1xyXG5cclxuICAgIGNvbnN0IG1hbnVhbFRyaWdnZXJzID0gcGFyc2VkVHJpZ2dlcnMuZmlsdGVyKHRyaWdnZXJQYWlyID0+IHRyaWdnZXJQYWlyLmlzTWFudWFsKCkpO1xyXG5cclxuICAgIGlmIChtYW51YWxUcmlnZ2Vycy5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgdGhyb3cgJ1RyaWdnZXJzIHBhcnNlIGVycm9yOiBvbmx5IG9uZSBtYW51YWwgdHJpZ2dlciBpcyBhbGxvd2VkJztcclxuICAgIH1cclxuXHJcbiAgICBpZiAobWFudWFsVHJpZ2dlcnMubGVuZ3RoID09PSAxICYmIHBhcnNlZFRyaWdnZXJzLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICB0aHJvdyAnVHJpZ2dlcnMgcGFyc2UgZXJyb3I6IG1hbnVhbCB0cmlnZ2VyIGNhblxcJ3QgYmUgbWl4ZWQgd2l0aCBvdGhlciB0cmlnZ2Vycyc7XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIHBhcnNlZFRyaWdnZXJzO1xyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gb2JzZXJ2ZVRyaWdnZXJzKHJlbmRlcmVyOiBhbnksIG5hdGl2ZUVsZW1lbnQ6IGFueSwgdHJpZ2dlcnM6IFRyaWdnZXJbXSwgaXNPcGVuZWRGbjogKCkgPT4gYm9vbGVhbikge1xyXG4gICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlPGJvb2xlYW4+KHN1YnNjcmliZXIgPT4ge1xyXG4gICAgICAgIGNvbnN0IGxpc3RlbmVycyA9IFtdO1xyXG4gICAgICAgIGNvbnN0IG9wZW5GbiA9ICgpID0+IHN1YnNjcmliZXIubmV4dCh0cnVlKTtcclxuICAgICAgICBjb25zdCBjbG9zZUZuID0gKCkgPT4gc3Vic2NyaWJlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICBjb25zdCB0b2dnbGVGbiA9ICgpID0+IHN1YnNjcmliZXIubmV4dCghaXNPcGVuZWRGbigpKTtcclxuXHJcbiAgICAgICAgdHJpZ2dlcnMuZm9yRWFjaCgodHJpZ2dlcjogVHJpZ2dlcikgPT4ge1xyXG4gICAgICAgICAgICBpZiAodHJpZ2dlci5vcGVuID09PSB0cmlnZ2VyLmNsb3NlKSB7XHJcbiAgICAgICAgICAgICAgICBsaXN0ZW5lcnMucHVzaChyZW5kZXJlci5saXN0ZW4obmF0aXZlRWxlbWVudCwgdHJpZ2dlci5vcGVuLCB0b2dnbGVGbikpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgbGlzdGVuZXJzLnB1c2goXHJcbiAgICAgICAgICAgICAgICAgICAgcmVuZGVyZXIubGlzdGVuKG5hdGl2ZUVsZW1lbnQsIHRyaWdnZXIub3Blbiwgb3BlbkZuKSxcclxuICAgICAgICAgICAgICAgICAgICByZW5kZXJlci5saXN0ZW4obmF0aXZlRWxlbWVudCwgdHJpZ2dlci5jbG9zZSwgY2xvc2VGbikpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgICAgIGxpc3RlbmVycy5mb3JFYWNoKHVuc3Vic2NyaWJlRm4gPT4gdW5zdWJzY3JpYmVGbigpKTtcclxuICAgICAgICB9O1xyXG4gICAgfSk7XHJcbn1cclxuXHJcbmNvbnN0IGRlbGF5T3JOb29wID0gPFQ+KHRpbWU6IG51bWJlcikgPT4gdGltZSA+IDAgPyBkZWxheTxUPih0aW1lKSA6IChhOiBPYnNlcnZhYmxlPFQ+KSA9PiBhO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHRyaWdnZXJEZWxheShvcGVuRGVsYXk6IG51bWJlciwgY2xvc2VEZWxheTogbnVtYmVyLCBpc09wZW5lZEZuOiAoKSA9PiBib29sZWFuKSB7XHJcbiAgICByZXR1cm4gKGlucHV0JDogT2JzZXJ2YWJsZTxib29sZWFuPikgPT4ge1xyXG4gICAgICAgIGxldCBwZW5kaW5nID0gbnVsbDtcclxuICAgICAgICBjb25zdCBmaWx0ZXJlZElucHV0JCA9IGlucHV0JC5waXBlKFxyXG4gICAgICAgICAgICBtYXAob3BlbiA9PiAoe29wZW59KSksIGZpbHRlcihldmVudCA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjdXJyZW50bHlPcGVuID0gaXNPcGVuZWRGbigpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGN1cnJlbnRseU9wZW4gIT09IGV2ZW50Lm9wZW4gJiYgKCFwZW5kaW5nIHx8IHBlbmRpbmcub3BlbiA9PT0gY3VycmVudGx5T3BlbikpIHtcclxuICAgICAgICAgICAgICAgICAgICBwZW5kaW5nID0gZXZlbnQ7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAocGVuZGluZyAmJiBwZW5kaW5nLm9wZW4gIT09IGV2ZW50Lm9wZW4pIHtcclxuICAgICAgICAgICAgICAgICAgICBwZW5kaW5nID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgIHNoYXJlKCkpO1xyXG4gICAgICAgIGNvbnN0IGRlbGF5ZWRPcGVuJCA9IGZpbHRlcmVkSW5wdXQkLnBpcGUoZmlsdGVyKGV2ZW50ID0+IGV2ZW50Lm9wZW4pLCBkZWxheU9yTm9vcChvcGVuRGVsYXkpKTtcclxuICAgICAgICBjb25zdCBkZWxheWVkQ2xvc2UkID0gZmlsdGVyZWRJbnB1dCQucGlwZShmaWx0ZXIoZXZlbnQgPT4gIWV2ZW50Lm9wZW4pLCBkZWxheU9yTm9vcChjbG9zZURlbGF5KSk7XHJcbiAgICAgICAgcmV0dXJuIG1lcmdlKGRlbGF5ZWRPcGVuJCwgZGVsYXllZENsb3NlJClcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBmaWx0ZXIoZXZlbnQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChldmVudCA9PT0gcGVuZGluZykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwZW5kaW5nID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGV2ZW50Lm9wZW4gIT09IGlzT3BlbmVkRm4oKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBtYXAoZXZlbnQgPT4gZXZlbnQub3BlbikpO1xyXG4gICAgfTtcclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGxpc3RlblRvVHJpZ2dlcnMoXHJcbiAgICByZW5kZXJlcjogYW55LCBuYXRpdmVFbGVtZW50OiBhbnksIHRyaWdnZXJzOiBzdHJpbmcsIGlzT3BlbmVkRm46ICgpID0+IGJvb2xlYW4sIG9wZW5GbiwgY2xvc2VGbiwgb3BlbkRlbGF5ID0gMCxcclxuICAgIGNsb3NlRGVsYXkgPSAwKSB7XHJcbiAgICBjb25zdCBwYXJzZWRUcmlnZ2VycyA9IHBhcnNlVHJpZ2dlcnModHJpZ2dlcnMpO1xyXG5cclxuICAgIGlmIChwYXJzZWRUcmlnZ2Vycy5sZW5ndGggPT09IDEgJiYgcGFyc2VkVHJpZ2dlcnNbMF0uaXNNYW51YWwoKSkge1xyXG4gICAgICAgIHJldHVybiAoKSA9PiB7XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdCBzdWJzY3JpcHRpb24gPSBvYnNlcnZlVHJpZ2dlcnMocmVuZGVyZXIsIG5hdGl2ZUVsZW1lbnQsIHBhcnNlZFRyaWdnZXJzLCBpc09wZW5lZEZuKVxyXG4gICAgICAgIC5waXBlKHRyaWdnZXJEZWxheShvcGVuRGVsYXksIGNsb3NlRGVsYXksIGlzT3BlbmVkRm4pKVxyXG4gICAgICAgIC5zdWJzY3JpYmUob3BlbiA9PiAob3BlbiA/IG9wZW5GbigpIDogY2xvc2VGbigpKSk7XHJcblxyXG4gICAgcmV0dXJuICgpID0+IHN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpO1xyXG59XHJcbiJdfQ==