/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtPagination component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the paginations used in the application.
 */
export class NgtPaginationConfig {
    constructor() {
        this.disabled = false;
        this.boundaryLinks = false;
        this.directionLinks = true;
        this.ellipses = true;
        this.maxSize = 0;
        this.pageSize = 10;
        this.rotate = false;
    }
}
NgtPaginationConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtPaginationConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtPaginationConfig_Factory() { return new NgtPaginationConfig(); }, token: NgtPaginationConfig, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtPaginationConfig.prototype.disabled;
    /** @type {?} */
    NgtPaginationConfig.prototype.boundaryLinks;
    /** @type {?} */
    NgtPaginationConfig.prototype.directionLinks;
    /** @type {?} */
    NgtPaginationConfig.prototype.ellipses;
    /** @type {?} */
    NgtPaginationConfig.prototype.maxSize;
    /** @type {?} */
    NgtPaginationConfig.prototype.pageSize;
    /** @type {?} */
    NgtPaginationConfig.prototype.rotate;
    /** @type {?} */
    NgtPaginationConfig.prototype.size;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi1jb25maWcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInBhZ2luYXRpb24vcGFnaW5hdGlvbi1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFRM0MsTUFBTSxPQUFPLG1CQUFtQjtJQURoQztRQUVJLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsYUFBUSxHQUFHLElBQUksQ0FBQztRQUNoQixZQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQ1osYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNkLFdBQU0sR0FBRyxLQUFLLENBQUM7S0FFbEI7OztZQVZBLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7Ozs7O0lBRTVCLHVDQUFpQjs7SUFDakIsNENBQXNCOztJQUN0Qiw2Q0FBc0I7O0lBQ3RCLHVDQUFnQjs7SUFDaEIsc0NBQVk7O0lBQ1osdUNBQWM7O0lBQ2QscUNBQWU7O0lBQ2YsbUNBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuLyoqXHJcbiAqIENvbmZpZ3VyYXRpb24gc2VydmljZSBmb3IgdGhlIE5ndFBhZ2luYXRpb24gY29tcG9uZW50LlxyXG4gKiBZb3UgY2FuIGluamVjdCB0aGlzIHNlcnZpY2UsIHR5cGljYWxseSBpbiB5b3VyIHJvb3QgY29tcG9uZW50LCBhbmQgY3VzdG9taXplIHRoZSB2YWx1ZXMgb2YgaXRzIHByb3BlcnRpZXMgaW5cclxuICogb3JkZXIgdG8gcHJvdmlkZSBkZWZhdWx0IHZhbHVlcyBmb3IgYWxsIHRoZSBwYWdpbmF0aW9ucyB1c2VkIGluIHRoZSBhcHBsaWNhdGlvbi5cclxuICovXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTmd0UGFnaW5hdGlvbkNvbmZpZyB7XHJcbiAgICBkaXNhYmxlZCA9IGZhbHNlO1xyXG4gICAgYm91bmRhcnlMaW5rcyA9IGZhbHNlO1xyXG4gICAgZGlyZWN0aW9uTGlua3MgPSB0cnVlO1xyXG4gICAgZWxsaXBzZXMgPSB0cnVlO1xyXG4gICAgbWF4U2l6ZSA9IDA7XHJcbiAgICBwYWdlU2l6ZSA9IDEwO1xyXG4gICAgcm90YXRlID0gZmFsc2U7XHJcbiAgICBzaXplOiAnc20nIHwgJ2xnJztcclxufVxyXG4iXX0=