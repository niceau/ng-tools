/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ContentChild, Directive, EventEmitter, Input, Output, ChangeDetectionStrategy, TemplateRef } from '@angular/core';
import { getValueInRange, isNumber } from '../util/util';
import { NgtPaginationConfig } from './pagination-config';
/**
 * Context for the pagination 'first', 'previous', 'next', 'last' or 'ellipsis' cell
 * in case you want to override one
 *
 * \@since 4.1.0
 * @record
 */
export function NgtPaginationLinkContext() { }
if (false) {
    /**
     * Page number currently selected
     * @type {?}
     */
    NgtPaginationLinkContext.prototype.currentPage;
    /**
     * If true the link in question is disabled
     * @type {?}
     */
    NgtPaginationLinkContext.prototype.disabled;
}
/**
 * Context for the pagination 'number' cell in case you want to override one.
 * Extends 'NgtPaginationLinkContext'
 *
 * \@since 4.1.0
 * @record
 */
export function NgtPaginationNumberContext() { }
if (false) {
    /**
     * Page number displayed by the current cell
     * @type {?}
     */
    NgtPaginationNumberContext.prototype.$implicit;
}
/**
 * The directive to match the 'ellipsis' cell template
 *
 * \@since 4.1.0
 */
export class NgtPaginationEllipsis {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationEllipsis.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationEllipsis]' },] }
];
/** @nocollapse */
NgtPaginationEllipsis.ctorParameters = () => [
    { type: TemplateRef }
];
if (false) {
    /** @type {?} */
    NgtPaginationEllipsis.prototype.templateRef;
}
/**
 * The directive to match the 'first' cell template
 *
 * \@since 4.1.0
 */
export class NgtPaginationFirst {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationFirst.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationFirst]' },] }
];
/** @nocollapse */
NgtPaginationFirst.ctorParameters = () => [
    { type: TemplateRef }
];
if (false) {
    /** @type {?} */
    NgtPaginationFirst.prototype.templateRef;
}
/**
 * The directive to match the 'last' cell template
 *
 * \@since 4.1.0
 */
export class NgtPaginationLast {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationLast.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationLast]' },] }
];
/** @nocollapse */
NgtPaginationLast.ctorParameters = () => [
    { type: TemplateRef }
];
if (false) {
    /** @type {?} */
    NgtPaginationLast.prototype.templateRef;
}
/**
 * The directive to match the 'next' cell template
 *
 * \@since 4.1.0
 */
export class NgtPaginationNext {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationNext.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationNext]' },] }
];
/** @nocollapse */
NgtPaginationNext.ctorParameters = () => [
    { type: TemplateRef }
];
if (false) {
    /** @type {?} */
    NgtPaginationNext.prototype.templateRef;
}
/**
 * The directive to match the page 'number' cell template
 *
 * \@since 4.1.0
 */
export class NgtPaginationNumber {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationNumber.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationNumber]' },] }
];
/** @nocollapse */
NgtPaginationNumber.ctorParameters = () => [
    { type: TemplateRef }
];
if (false) {
    /** @type {?} */
    NgtPaginationNumber.prototype.templateRef;
}
/**
 * The directive to match the 'previous' cell template
 *
 * \@since 4.1.0
 */
export class NgtPaginationPrevious {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtPaginationPrevious.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtPaginationPrevious]' },] }
];
/** @nocollapse */
NgtPaginationPrevious.ctorParameters = () => [
    { type: TemplateRef }
];
if (false) {
    /** @type {?} */
    NgtPaginationPrevious.prototype.templateRef;
}
/**
 * A component that displays page numbers and allows to customize them in several ways
 */
export class NgtPagination {
    /**
     * @param {?} config
     */
    constructor(config) {
        this.pageCount = 0;
        this.pages = [];
        /**
         *  Current page. Page numbers start with 1
         */
        this.page = 1;
        /**
         *  An event fired when the page is changed.
         *  Event's payload equals to the newly selected page.
         *  Will fire only if collection size is set and all values are valid.
         *  Page numbers start with 1
         */
        this.pageChange = new EventEmitter(true);
        this.disabled = config.disabled;
        this.boundaryLinks = config.boundaryLinks;
        this.directionLinks = config.directionLinks;
        this.ellipses = config.ellipses;
        this.maxSize = config.maxSize;
        this.pageSize = config.pageSize;
        this.rotate = config.rotate;
        this.size = config.size;
    }
    /**
     * @return {?}
     */
    hasPrevious() {
        return this.page > 1;
    }
    /**
     * @return {?}
     */
    hasNext() {
        return this.page < this.pageCount;
    }
    /**
     * @return {?}
     */
    nextDisabled() {
        return !this.hasNext() || this.disabled;
    }
    /**
     * @return {?}
     */
    previousDisabled() {
        return !this.hasPrevious() || this.disabled;
    }
    /**
     * @param {?} pageNumber
     * @return {?}
     */
    selectPage(pageNumber) {
        this._updatePages(pageNumber);
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        this._updatePages(this.page);
    }
    /**
     * @param {?} pageNumber
     * @return {?}
     */
    isEllipsis(pageNumber) {
        return pageNumber === -1;
    }
    /**
     * Appends ellipses and first/last page number to the displayed pages
     * @private
     * @param {?} start
     * @param {?} end
     * @return {?}
     */
    _applyEllipses(start, end) {
        if (this.ellipses) {
            if (start > 0) {
                if (start > 1) {
                    this.pages.unshift(-1);
                }
                this.pages.unshift(1);
            }
            if (end < this.pageCount) {
                if (end < (this.pageCount - 1)) {
                    this.pages.push(-1);
                }
                this.pages.push(this.pageCount);
            }
        }
    }
    /**
     * Rotates page numbers based on maxSize items visible.
     * Currently selected page stays in the middle:
     *
     * Ex. for selected page = 6:
     * [5,*6*,7] for maxSize = 3
     * [4,5,*6*,7] for maxSize = 4
     * @private
     * @return {?}
     */
    _applyRotation() {
        /** @type {?} */
        let start = 0;
        /** @type {?} */
        let end = this.pageCount;
        /** @type {?} */
        let leftOffset = Math.floor(this.maxSize / 2);
        /** @type {?} */
        let rightOffset = this.maxSize % 2 === 0 ? leftOffset - 1 : leftOffset;
        if (this.page <= leftOffset) {
            // very beginning, no rotation -> [0..maxSize]
            end = this.maxSize;
        }
        else if (this.pageCount - this.page < leftOffset) {
            // very end, no rotation -> [len-maxSize..len]
            start = this.pageCount - this.maxSize;
        }
        else {
            // rotate
            start = this.page - leftOffset - 1;
            end = this.page + rightOffset;
        }
        return [start, end];
    }
    /**
     * Paginates page numbers based on maxSize items per page
     * @private
     * @return {?}
     */
    _applyPagination() {
        /** @type {?} */
        let page = Math.ceil(this.page / this.maxSize) - 1;
        /** @type {?} */
        let start = page * this.maxSize;
        /** @type {?} */
        let end = start + this.maxSize;
        return [start, end];
    }
    /**
     * @private
     * @param {?} newPageNo
     * @return {?}
     */
    _setPageInRange(newPageNo) {
        /** @type {?} */
        const prevPageNo = this.page;
        this.page = getValueInRange(newPageNo, this.pageCount, 1);
        if (this.page !== prevPageNo && isNumber(this.collectionSize)) {
            this.pageChange.emit(this.page);
        }
    }
    /**
     * @private
     * @param {?} newPage
     * @return {?}
     */
    _updatePages(newPage) {
        this.pageCount = Math.ceil(this.collectionSize / this.pageSize);
        if (!isNumber(this.pageCount)) {
            this.pageCount = 0;
        }
        // fill-in model needed to render pages
        this.pages.length = 0;
        for (let i = 1; i <= this.pageCount; i++) {
            this.pages.push(i);
        }
        // set page within 1..max range
        this._setPageInRange(newPage);
        // apply maxSize if necessary
        if (this.maxSize > 0 && this.pageCount > this.maxSize) {
            /** @type {?} */
            let start = 0;
            /** @type {?} */
            let end = this.pageCount;
            // either paginating or rotating page numbers
            if (this.rotate) {
                [start, end] = this._applyRotation();
            }
            else {
                [start, end] = this._applyPagination();
            }
            this.pages = this.pages.slice(start, end);
            // adding ellipses
            this._applyEllipses(start, end);
        }
    }
}
NgtPagination.decorators = [
    { type: Component, args: [{
                selector: 'ngt-pagination',
                changeDetection: ChangeDetectionStrategy.OnPush,
                host: { 'role': 'navigation' },
                template: `
    <ng-template #first><span aria-hidden="true" i18n="@@ngt.pagination.first">&laquo;&laquo;</span></ng-template>
    <ng-template #previous><span aria-hidden="true" i18n="@@ngt.pagination.previous">&laquo;</span></ng-template>
    <ng-template #next><span aria-hidden="true" i18n="@@ngt.pagination.next">&raquo;</span></ng-template>
    <ng-template #last><span aria-hidden="true" i18n="@@ngt.pagination.last">&raquo;&raquo;</span></ng-template>
    <ng-template #ellipsis>...</ng-template>
    <ng-template #defaultNumber let-page let-currentPage="currentPage">
      {{ page }}
      <span *ngIf="page === currentPage" class="sr-only">(current)</span>
    </ng-template>
    <ul [class]="'pagination' + (size ? ' pagination-' + size : '')">
      <li *ngIf="boundaryLinks" class="page-item"
        [class.disabled]="previousDisabled()">
        <a aria-label="First" i18n-aria-label="@@ngt.pagination.first-aria" class="page-link" href
          (click)="selectPage(1); $event.preventDefault()" [attr.tabindex]="(hasPrevious() ? null : '-1')">
          <ng-template [ngTemplateOutlet]="tplFirst?.templateRef || first"
                       [ngTemplateOutletContext]="{disabled: previousDisabled(), currentPage: page}"></ng-template>
        </a>
      </li>
      <li *ngIf="directionLinks" class="page-item"
        [class.disabled]="previousDisabled()">
        <a aria-label="Previous" i18n-aria-label="@@ngt.pagination.previous-aria" class="page-link" href
          (click)="selectPage(page-1); $event.preventDefault()" [attr.tabindex]="(hasPrevious() ? null : '-1')">
          <ng-template [ngTemplateOutlet]="tplPrevious?.templateRef || previous"
                       [ngTemplateOutletContext]="{disabled: previousDisabled()}"></ng-template>
        </a>
      </li>
      <li *ngFor="let pageNumber of pages" class="page-item" [class.active]="pageNumber === page"
        [class.disabled]="isEllipsis(pageNumber) || disabled">
        <a *ngIf="isEllipsis(pageNumber)" class="page-link">
          <ng-template [ngTemplateOutlet]="tplEllipsis?.templateRef || ellipsis"
                       [ngTemplateOutletContext]="{disabled: true, currentPage: page}"></ng-template>
        </a>
        <a *ngIf="!isEllipsis(pageNumber)" class="page-link" href (click)="selectPage(pageNumber); $event.preventDefault()">
          <ng-template [ngTemplateOutlet]="tplNumber?.templateRef || defaultNumber"
                       [ngTemplateOutletContext]="{disabled: disabled, $implicit: pageNumber, currentPage: page}"></ng-template>
        </a>
      </li>
      <li *ngIf="directionLinks" class="page-item" [class.disabled]="nextDisabled()">
        <a aria-label="Next" i18n-aria-label="@@ngt.pagination.next-aria" class="page-link" href
          (click)="selectPage(page+1); $event.preventDefault()" [attr.tabindex]="(hasNext() ? null : '-1')">
          <ng-template [ngTemplateOutlet]="tplNext?.templateRef || next"
                       [ngTemplateOutletContext]="{disabled: nextDisabled(), currentPage: page}"></ng-template>
        </a>
      </li>
      <li *ngIf="boundaryLinks" class="page-item" [class.disabled]="nextDisabled()">
        <a aria-label="Last" i18n-aria-label="@@ngt.pagination.last-aria" class="page-link" href
          (click)="selectPage(pageCount); $event.preventDefault()" [attr.tabindex]="(hasNext() ? null : '-1')">
          <ng-template [ngTemplateOutlet]="tplLast?.templateRef || last"
                       [ngTemplateOutletContext]="{disabled: nextDisabled(), currentPage: page}"></ng-template>
        </a>
      </li>
    </ul>
  `
            }] }
];
/** @nocollapse */
NgtPagination.ctorParameters = () => [
    { type: NgtPaginationConfig }
];
NgtPagination.propDecorators = {
    tplEllipsis: [{ type: ContentChild, args: [NgtPaginationEllipsis,] }],
    tplFirst: [{ type: ContentChild, args: [NgtPaginationFirst,] }],
    tplLast: [{ type: ContentChild, args: [NgtPaginationLast,] }],
    tplNext: [{ type: ContentChild, args: [NgtPaginationNext,] }],
    tplNumber: [{ type: ContentChild, args: [NgtPaginationNumber,] }],
    tplPrevious: [{ type: ContentChild, args: [NgtPaginationPrevious,] }],
    disabled: [{ type: Input }],
    boundaryLinks: [{ type: Input }],
    directionLinks: [{ type: Input }],
    ellipses: [{ type: Input }],
    rotate: [{ type: Input }],
    collectionSize: [{ type: Input }],
    maxSize: [{ type: Input }],
    page: [{ type: Input }],
    pageSize: [{ type: Input }],
    pageChange: [{ type: Output }],
    size: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NgtPagination.prototype.pageCount;
    /** @type {?} */
    NgtPagination.prototype.pages;
    /** @type {?} */
    NgtPagination.prototype.tplEllipsis;
    /** @type {?} */
    NgtPagination.prototype.tplFirst;
    /** @type {?} */
    NgtPagination.prototype.tplLast;
    /** @type {?} */
    NgtPagination.prototype.tplNext;
    /** @type {?} */
    NgtPagination.prototype.tplNumber;
    /** @type {?} */
    NgtPagination.prototype.tplPrevious;
    /**
     * Whether to disable buttons from user input
     * @type {?}
     */
    NgtPagination.prototype.disabled;
    /**
     *  Whether to show the "First" and "Last" page links
     * @type {?}
     */
    NgtPagination.prototype.boundaryLinks;
    /**
     *  Whether to show the "Next" and "Previous" page links
     * @type {?}
     */
    NgtPagination.prototype.directionLinks;
    /**
     *  Whether to show ellipsis symbols and first/last page numbers when maxSize > number of pages
     * @type {?}
     */
    NgtPagination.prototype.ellipses;
    /**
     *  Whether to rotate pages when maxSize > number of pages.
     *  Current page will be in the middle
     * @type {?}
     */
    NgtPagination.prototype.rotate;
    /**
     *  Number of items in collection.
     * @type {?}
     */
    NgtPagination.prototype.collectionSize;
    /**
     *  Maximum number of pages to display.
     * @type {?}
     */
    NgtPagination.prototype.maxSize;
    /**
     *  Current page. Page numbers start with 1
     * @type {?}
     */
    NgtPagination.prototype.page;
    /**
     *  Number of items per page.
     * @type {?}
     */
    NgtPagination.prototype.pageSize;
    /**
     *  An event fired when the page is changed.
     *  Event's payload equals to the newly selected page.
     *  Will fire only if collection size is set and all values are valid.
     *  Page numbers start with 1
     * @type {?}
     */
    NgtPagination.prototype.pageChange;
    /**
     * Pagination display size: small or large
     * @type {?}
     */
    NgtPagination.prototype.size;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsicGFnaW5hdGlvbi9wYWdpbmF0aW9uLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0gsU0FBUyxFQUNULFlBQVksRUFDWixTQUFTLEVBQ1QsWUFBWSxFQUNaLEtBQUssRUFDTCxNQUFNLEVBRU4sdUJBQXVCLEVBRXZCLFdBQVcsRUFDZCxNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUN6RCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQzs7Ozs7Ozs7QUFRMUQsOENBVUM7Ozs7OztJQU5HLCtDQUFvQjs7Ozs7SUFLcEIsNENBQWtCOzs7Ozs7Ozs7QUFTdEIsZ0RBS0M7Ozs7OztJQURHLCtDQUFrQjs7Ozs7OztBQVN0QixNQUFNLE9BQU8scUJBQXFCOzs7O0lBQzlCLFlBQW1CLFdBQWtEO1FBQWxELGdCQUFXLEdBQVgsV0FBVyxDQUF1QztJQUNyRSxDQUFDOzs7WUFISixTQUFTLFNBQUMsRUFBQyxRQUFRLEVBQUUsb0NBQW9DLEVBQUM7Ozs7WUF6Q3ZELFdBQVc7Ozs7SUEyQ0MsNENBQXlEOzs7Ozs7O0FBVXpFLE1BQU0sT0FBTyxrQkFBa0I7Ozs7SUFDM0IsWUFBbUIsV0FBa0Q7UUFBbEQsZ0JBQVcsR0FBWCxXQUFXLENBQXVDO0lBQ3JFLENBQUM7OztZQUhKLFNBQVMsU0FBQyxFQUFDLFFBQVEsRUFBRSxpQ0FBaUMsRUFBQzs7OztZQXBEcEQsV0FBVzs7OztJQXNEQyx5Q0FBeUQ7Ozs7Ozs7QUFVekUsTUFBTSxPQUFPLGlCQUFpQjs7OztJQUMxQixZQUFtQixXQUFrRDtRQUFsRCxnQkFBVyxHQUFYLFdBQVcsQ0FBdUM7SUFDckUsQ0FBQzs7O1lBSEosU0FBUyxTQUFDLEVBQUMsUUFBUSxFQUFFLGdDQUFnQyxFQUFDOzs7O1lBL0RuRCxXQUFXOzs7O0lBaUVDLHdDQUF5RDs7Ozs7OztBQVV6RSxNQUFNLE9BQU8saUJBQWlCOzs7O0lBQzFCLFlBQW1CLFdBQWtEO1FBQWxELGdCQUFXLEdBQVgsV0FBVyxDQUF1QztJQUNyRSxDQUFDOzs7WUFISixTQUFTLFNBQUMsRUFBQyxRQUFRLEVBQUUsZ0NBQWdDLEVBQUM7Ozs7WUExRW5ELFdBQVc7Ozs7SUE0RUMsd0NBQXlEOzs7Ozs7O0FBVXpFLE1BQU0sT0FBTyxtQkFBbUI7Ozs7SUFDNUIsWUFBbUIsV0FBb0Q7UUFBcEQsZ0JBQVcsR0FBWCxXQUFXLENBQXlDO0lBQ3ZFLENBQUM7OztZQUhKLFNBQVMsU0FBQyxFQUFDLFFBQVEsRUFBRSxrQ0FBa0MsRUFBQzs7OztZQXJGckQsV0FBVzs7OztJQXVGQywwQ0FBMkQ7Ozs7Ozs7QUFVM0UsTUFBTSxPQUFPLHFCQUFxQjs7OztJQUM5QixZQUFtQixXQUFrRDtRQUFsRCxnQkFBVyxHQUFYLFdBQVcsQ0FBdUM7SUFDckUsQ0FBQzs7O1lBSEosU0FBUyxTQUFDLEVBQUMsUUFBUSxFQUFFLG9DQUFvQyxFQUFDOzs7O1lBaEd2RCxXQUFXOzs7O0lBa0dDLDRDQUF5RDs7Ozs7QUFrRXpFLE1BQU0sT0FBTyxhQUFhOzs7O0lBc0V0QixZQUFZLE1BQTJCO1FBckV2QyxjQUFTLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsVUFBSyxHQUFhLEVBQUUsQ0FBQzs7OztRQWdEWixTQUFJLEdBQUcsQ0FBQyxDQUFDOzs7Ozs7O1FBYVIsZUFBVSxHQUFHLElBQUksWUFBWSxDQUFTLElBQUksQ0FBQyxDQUFDO1FBUWxELElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNoQyxJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQyxhQUFhLENBQUM7UUFDMUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLENBQUMsY0FBYyxDQUFDO1FBQzVDLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUNoQyxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUM7UUFDOUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUM1QixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7SUFDNUIsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxPQUFPLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0gsT0FBTyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDdEMsQ0FBQzs7OztJQUVELFlBQVk7UUFDUixPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDNUMsQ0FBQzs7OztJQUVELGdCQUFnQjtRQUNaLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUNoRCxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxVQUFrQjtRQUN6QixJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQzlCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLFVBQVU7UUFDakIsT0FBTyxVQUFVLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDN0IsQ0FBQzs7Ozs7Ozs7SUFLTyxjQUFjLENBQUMsS0FBYSxFQUFFLEdBQVc7UUFDN0MsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2YsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFO2dCQUNYLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRTtvQkFDWCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMxQjtnQkFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN6QjtZQUNELElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ3RCLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdkI7Z0JBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ25DO1NBQ0o7SUFDTCxDQUFDOzs7Ozs7Ozs7OztJQVVPLGNBQWM7O1lBQ2QsS0FBSyxHQUFHLENBQUM7O1lBQ1QsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTOztZQUNwQixVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQzs7WUFDekMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVTtRQUV0RSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksVUFBVSxFQUFFO1lBQ3pCLDhDQUE4QztZQUM5QyxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUN0QjthQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLFVBQVUsRUFBRTtZQUNoRCw4Q0FBOEM7WUFDOUMsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUN6QzthQUFNO1lBQ0gsU0FBUztZQUNULEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxHQUFHLFVBQVUsR0FBRyxDQUFDLENBQUM7WUFDbkMsR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDO1NBQ2pDO1FBRUQsT0FBTyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztJQUN4QixDQUFDOzs7Ozs7SUFLTyxnQkFBZ0I7O1lBQ2hCLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7O1lBQzlDLEtBQUssR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU87O1lBQzNCLEdBQUcsR0FBRyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU87UUFFOUIsT0FBTyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztJQUN4QixDQUFDOzs7Ozs7SUFFTyxlQUFlLENBQUMsU0FBUzs7Y0FDdkIsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJO1FBQzVCLElBQUksQ0FBQyxJQUFJLEdBQUcsZUFBZSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBRTFELElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxVQUFVLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRTtZQUMzRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbkM7SUFDTCxDQUFDOzs7Ozs7SUFFTyxZQUFZLENBQUMsT0FBZTtRQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFaEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDM0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7U0FDdEI7UUFFRCx1Q0FBdUM7UUFDdkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3RDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3RCO1FBRUQsK0JBQStCO1FBQy9CLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFOUIsNkJBQTZCO1FBQzdCLElBQUksSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLElBQUksSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsT0FBTyxFQUFFOztnQkFDL0MsS0FBSyxHQUFHLENBQUM7O2dCQUNULEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUztZQUV4Qiw2Q0FBNkM7WUFDN0MsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNiLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUN4QztpQkFBTTtnQkFDSCxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzthQUMxQztZQUVELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBRTFDLGtCQUFrQjtZQUNsQixJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztTQUNuQztJQUNMLENBQUM7OztZQTlRSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGdCQUFnQjtnQkFDMUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07Z0JBQy9DLElBQUksRUFBRSxFQUFDLE1BQU0sRUFBRSxZQUFZLEVBQUM7Z0JBQzVCLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FxRFg7YUFDRjs7OztZQWhLUSxtQkFBbUI7OzswQkFxS3ZCLFlBQVksU0FBQyxxQkFBcUI7dUJBQ2xDLFlBQVksU0FBQyxrQkFBa0I7c0JBQy9CLFlBQVksU0FBQyxpQkFBaUI7c0JBQzlCLFlBQVksU0FBQyxpQkFBaUI7d0JBQzlCLFlBQVksU0FBQyxtQkFBbUI7MEJBQ2hDLFlBQVksU0FBQyxxQkFBcUI7dUJBS2xDLEtBQUs7NEJBS0wsS0FBSzs2QkFLTCxLQUFLO3VCQUtMLEtBQUs7cUJBTUwsS0FBSzs2QkFLTCxLQUFLO3NCQUtMLEtBQUs7bUJBS0wsS0FBSzt1QkFLTCxLQUFLO3lCQVFMLE1BQU07bUJBS04sS0FBSzs7OztJQW5FTixrQ0FBYzs7SUFDZCw4QkFBcUI7O0lBRXJCLG9DQUF3RTs7SUFDeEUsaUNBQStEOztJQUMvRCxnQ0FBNEQ7O0lBQzVELGdDQUE0RDs7SUFDNUQsa0NBQWtFOztJQUNsRSxvQ0FBd0U7Ozs7O0lBS3hFLGlDQUEyQjs7Ozs7SUFLM0Isc0NBQWdDOzs7OztJQUtoQyx1Q0FBaUM7Ozs7O0lBS2pDLGlDQUEyQjs7Ozs7O0lBTTNCLCtCQUF5Qjs7Ozs7SUFLekIsdUNBQWdDOzs7OztJQUtoQyxnQ0FBeUI7Ozs7O0lBS3pCLDZCQUFrQjs7Ozs7SUFLbEIsaUNBQTBCOzs7Ozs7OztJQVExQixtQ0FBc0Q7Ozs7O0lBS3RELDZCQUEyQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBDb21wb25lbnQsXHJcbiAgICBDb250ZW50Q2hpbGQsXHJcbiAgICBEaXJlY3RpdmUsXHJcbiAgICBFdmVudEVtaXR0ZXIsXHJcbiAgICBJbnB1dCxcclxuICAgIE91dHB1dCxcclxuICAgIE9uQ2hhbmdlcyxcclxuICAgIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LFxyXG4gICAgU2ltcGxlQ2hhbmdlcyxcclxuICAgIFRlbXBsYXRlUmVmXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IGdldFZhbHVlSW5SYW5nZSwgaXNOdW1iZXIgfSBmcm9tICcuLi91dGlsL3V0aWwnO1xyXG5pbXBvcnQgeyBOZ3RQYWdpbmF0aW9uQ29uZmlnIH0gZnJvbSAnLi9wYWdpbmF0aW9uLWNvbmZpZyc7XHJcblxyXG4vKipcclxuICogQ29udGV4dCBmb3IgdGhlIHBhZ2luYXRpb24gJ2ZpcnN0JywgJ3ByZXZpb3VzJywgJ25leHQnLCAnbGFzdCcgb3IgJ2VsbGlwc2lzJyBjZWxsXHJcbiAqIGluIGNhc2UgeW91IHdhbnQgdG8gb3ZlcnJpZGUgb25lXHJcbiAqXHJcbiAqIEBzaW5jZSA0LjEuMFxyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBOZ3RQYWdpbmF0aW9uTGlua0NvbnRleHQge1xyXG4gICAgLyoqXHJcbiAgICAgKiBQYWdlIG51bWJlciBjdXJyZW50bHkgc2VsZWN0ZWRcclxuICAgICAqL1xyXG4gICAgY3VycmVudFBhZ2U6IG51bWJlcjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIElmIHRydWUgdGhlIGxpbmsgaW4gcXVlc3Rpb24gaXMgZGlzYWJsZWRcclxuICAgICAqL1xyXG4gICAgZGlzYWJsZWQ6IGJvb2xlYW47XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBDb250ZXh0IGZvciB0aGUgcGFnaW5hdGlvbiAnbnVtYmVyJyBjZWxsIGluIGNhc2UgeW91IHdhbnQgdG8gb3ZlcnJpZGUgb25lLlxyXG4gKiBFeHRlbmRzICdOZ3RQYWdpbmF0aW9uTGlua0NvbnRleHQnXHJcbiAqXHJcbiAqIEBzaW5jZSA0LjEuMFxyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBOZ3RQYWdpbmF0aW9uTnVtYmVyQ29udGV4dCBleHRlbmRzIE5ndFBhZ2luYXRpb25MaW5rQ29udGV4dCB7XHJcbiAgICAvKipcclxuICAgICAqIFBhZ2UgbnVtYmVyIGRpc3BsYXllZCBieSB0aGUgY3VycmVudCBjZWxsXHJcbiAgICAgKi9cclxuICAgICRpbXBsaWNpdDogbnVtYmVyO1xyXG59XHJcblxyXG4vKipcclxuICogVGhlIGRpcmVjdGl2ZSB0byBtYXRjaCB0aGUgJ2VsbGlwc2lzJyBjZWxsIHRlbXBsYXRlXHJcbiAqXHJcbiAqIEBzaW5jZSA0LjEuMFxyXG4gKi9cclxuQERpcmVjdGl2ZSh7c2VsZWN0b3I6ICduZy10ZW1wbGF0ZVtuZ3RQYWdpbmF0aW9uRWxsaXBzaXNdJ30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYWdpbmF0aW9uRWxsaXBzaXMge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHRlbXBsYXRlUmVmOiBUZW1wbGF0ZVJlZjxOZ3RQYWdpbmF0aW9uTGlua0NvbnRleHQ+KSB7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUaGUgZGlyZWN0aXZlIHRvIG1hdGNoIHRoZSAnZmlyc3QnIGNlbGwgdGVtcGxhdGVcclxuICpcclxuICogQHNpbmNlIDQuMS4wXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtzZWxlY3RvcjogJ25nLXRlbXBsYXRlW25ndFBhZ2luYXRpb25GaXJzdF0nfSlcclxuZXhwb3J0IGNsYXNzIE5ndFBhZ2luYXRpb25GaXJzdCB7XHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdGVtcGxhdGVSZWY6IFRlbXBsYXRlUmVmPE5ndFBhZ2luYXRpb25MaW5rQ29udGV4dD4pIHtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIFRoZSBkaXJlY3RpdmUgdG8gbWF0Y2ggdGhlICdsYXN0JyBjZWxsIHRlbXBsYXRlXHJcbiAqXHJcbiAqIEBzaW5jZSA0LjEuMFxyXG4gKi9cclxuQERpcmVjdGl2ZSh7c2VsZWN0b3I6ICduZy10ZW1wbGF0ZVtuZ3RQYWdpbmF0aW9uTGFzdF0nfSlcclxuZXhwb3J0IGNsYXNzIE5ndFBhZ2luYXRpb25MYXN0IHtcclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyB0ZW1wbGF0ZVJlZjogVGVtcGxhdGVSZWY8Tmd0UGFnaW5hdGlvbkxpbmtDb250ZXh0Pikge1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogVGhlIGRpcmVjdGl2ZSB0byBtYXRjaCB0aGUgJ25leHQnIGNlbGwgdGVtcGxhdGVcclxuICpcclxuICogQHNpbmNlIDQuMS4wXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtzZWxlY3RvcjogJ25nLXRlbXBsYXRlW25ndFBhZ2luYXRpb25OZXh0XSd9KVxyXG5leHBvcnQgY2xhc3MgTmd0UGFnaW5hdGlvbk5leHQge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHRlbXBsYXRlUmVmOiBUZW1wbGF0ZVJlZjxOZ3RQYWdpbmF0aW9uTGlua0NvbnRleHQ+KSB7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUaGUgZGlyZWN0aXZlIHRvIG1hdGNoIHRoZSBwYWdlICdudW1iZXInIGNlbGwgdGVtcGxhdGVcclxuICpcclxuICogQHNpbmNlIDQuMS4wXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtzZWxlY3RvcjogJ25nLXRlbXBsYXRlW25ndFBhZ2luYXRpb25OdW1iZXJdJ30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYWdpbmF0aW9uTnVtYmVyIHtcclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyB0ZW1wbGF0ZVJlZjogVGVtcGxhdGVSZWY8Tmd0UGFnaW5hdGlvbk51bWJlckNvbnRleHQ+KSB7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUaGUgZGlyZWN0aXZlIHRvIG1hdGNoIHRoZSAncHJldmlvdXMnIGNlbGwgdGVtcGxhdGVcclxuICpcclxuICogQHNpbmNlIDQuMS4wXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtzZWxlY3RvcjogJ25nLXRlbXBsYXRlW25ndFBhZ2luYXRpb25QcmV2aW91c10nfSlcclxuZXhwb3J0IGNsYXNzIE5ndFBhZ2luYXRpb25QcmV2aW91cyB7XHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdGVtcGxhdGVSZWY6IFRlbXBsYXRlUmVmPE5ndFBhZ2luYXRpb25MaW5rQ29udGV4dD4pIHtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIEEgY29tcG9uZW50IHRoYXQgZGlzcGxheXMgcGFnZSBudW1iZXJzIGFuZCBhbGxvd3MgdG8gY3VzdG9taXplIHRoZW0gaW4gc2V2ZXJhbCB3YXlzXHJcbiAqL1xyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbmd0LXBhZ2luYXRpb24nLFxyXG4gICAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXHJcbiAgICBob3N0OiB7J3JvbGUnOiAnbmF2aWdhdGlvbid9LFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgIDxuZy10ZW1wbGF0ZSAjZmlyc3Q+PHNwYW4gYXJpYS1oaWRkZW49XCJ0cnVlXCIgaTE4bj1cIkBAbmd0LnBhZ2luYXRpb24uZmlyc3RcIj4mbGFxdW87JmxhcXVvOzwvc3Bhbj48L25nLXRlbXBsYXRlPlxyXG4gICAgPG5nLXRlbXBsYXRlICNwcmV2aW91cz48c3BhbiBhcmlhLWhpZGRlbj1cInRydWVcIiBpMThuPVwiQEBuZ3QucGFnaW5hdGlvbi5wcmV2aW91c1wiPiZsYXF1bzs8L3NwYW4+PC9uZy10ZW1wbGF0ZT5cclxuICAgIDxuZy10ZW1wbGF0ZSAjbmV4dD48c3BhbiBhcmlhLWhpZGRlbj1cInRydWVcIiBpMThuPVwiQEBuZ3QucGFnaW5hdGlvbi5uZXh0XCI+JnJhcXVvOzwvc3Bhbj48L25nLXRlbXBsYXRlPlxyXG4gICAgPG5nLXRlbXBsYXRlICNsYXN0PjxzcGFuIGFyaWEtaGlkZGVuPVwidHJ1ZVwiIGkxOG49XCJAQG5ndC5wYWdpbmF0aW9uLmxhc3RcIj4mcmFxdW87JnJhcXVvOzwvc3Bhbj48L25nLXRlbXBsYXRlPlxyXG4gICAgPG5nLXRlbXBsYXRlICNlbGxpcHNpcz4uLi48L25nLXRlbXBsYXRlPlxyXG4gICAgPG5nLXRlbXBsYXRlICNkZWZhdWx0TnVtYmVyIGxldC1wYWdlIGxldC1jdXJyZW50UGFnZT1cImN1cnJlbnRQYWdlXCI+XHJcbiAgICAgIHt7IHBhZ2UgfX1cclxuICAgICAgPHNwYW4gKm5nSWY9XCJwYWdlID09PSBjdXJyZW50UGFnZVwiIGNsYXNzPVwic3Itb25seVwiPihjdXJyZW50KTwvc3Bhbj5cclxuICAgIDwvbmctdGVtcGxhdGU+XHJcbiAgICA8dWwgW2NsYXNzXT1cIidwYWdpbmF0aW9uJyArIChzaXplID8gJyBwYWdpbmF0aW9uLScgKyBzaXplIDogJycpXCI+XHJcbiAgICAgIDxsaSAqbmdJZj1cImJvdW5kYXJ5TGlua3NcIiBjbGFzcz1cInBhZ2UtaXRlbVwiXHJcbiAgICAgICAgW2NsYXNzLmRpc2FibGVkXT1cInByZXZpb3VzRGlzYWJsZWQoKVwiPlxyXG4gICAgICAgIDxhIGFyaWEtbGFiZWw9XCJGaXJzdFwiIGkxOG4tYXJpYS1sYWJlbD1cIkBAbmd0LnBhZ2luYXRpb24uZmlyc3QtYXJpYVwiIGNsYXNzPVwicGFnZS1saW5rXCIgaHJlZlxyXG4gICAgICAgICAgKGNsaWNrKT1cInNlbGVjdFBhZ2UoMSk7ICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXCIgW2F0dHIudGFiaW5kZXhdPVwiKGhhc1ByZXZpb3VzKCkgPyBudWxsIDogJy0xJylcIj5cclxuICAgICAgICAgIDxuZy10ZW1wbGF0ZSBbbmdUZW1wbGF0ZU91dGxldF09XCJ0cGxGaXJzdD8udGVtcGxhdGVSZWYgfHwgZmlyc3RcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgIFtuZ1RlbXBsYXRlT3V0bGV0Q29udGV4dF09XCJ7ZGlzYWJsZWQ6IHByZXZpb3VzRGlzYWJsZWQoKSwgY3VycmVudFBhZ2U6IHBhZ2V9XCI+PC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICA8L2E+XHJcbiAgICAgIDwvbGk+XHJcbiAgICAgIDxsaSAqbmdJZj1cImRpcmVjdGlvbkxpbmtzXCIgY2xhc3M9XCJwYWdlLWl0ZW1cIlxyXG4gICAgICAgIFtjbGFzcy5kaXNhYmxlZF09XCJwcmV2aW91c0Rpc2FibGVkKClcIj5cclxuICAgICAgICA8YSBhcmlhLWxhYmVsPVwiUHJldmlvdXNcIiBpMThuLWFyaWEtbGFiZWw9XCJAQG5ndC5wYWdpbmF0aW9uLnByZXZpb3VzLWFyaWFcIiBjbGFzcz1cInBhZ2UtbGlua1wiIGhyZWZcclxuICAgICAgICAgIChjbGljayk9XCJzZWxlY3RQYWdlKHBhZ2UtMSk7ICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXCIgW2F0dHIudGFiaW5kZXhdPVwiKGhhc1ByZXZpb3VzKCkgPyBudWxsIDogJy0xJylcIj5cclxuICAgICAgICAgIDxuZy10ZW1wbGF0ZSBbbmdUZW1wbGF0ZU91dGxldF09XCJ0cGxQcmV2aW91cz8udGVtcGxhdGVSZWYgfHwgcHJldmlvdXNcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgIFtuZ1RlbXBsYXRlT3V0bGV0Q29udGV4dF09XCJ7ZGlzYWJsZWQ6IHByZXZpb3VzRGlzYWJsZWQoKX1cIj48L25nLXRlbXBsYXRlPlxyXG4gICAgICAgIDwvYT5cclxuICAgICAgPC9saT5cclxuICAgICAgPGxpICpuZ0Zvcj1cImxldCBwYWdlTnVtYmVyIG9mIHBhZ2VzXCIgY2xhc3M9XCJwYWdlLWl0ZW1cIiBbY2xhc3MuYWN0aXZlXT1cInBhZ2VOdW1iZXIgPT09IHBhZ2VcIlxyXG4gICAgICAgIFtjbGFzcy5kaXNhYmxlZF09XCJpc0VsbGlwc2lzKHBhZ2VOdW1iZXIpIHx8IGRpc2FibGVkXCI+XHJcbiAgICAgICAgPGEgKm5nSWY9XCJpc0VsbGlwc2lzKHBhZ2VOdW1iZXIpXCIgY2xhc3M9XCJwYWdlLWxpbmtcIj5cclxuICAgICAgICAgIDxuZy10ZW1wbGF0ZSBbbmdUZW1wbGF0ZU91dGxldF09XCJ0cGxFbGxpcHNpcz8udGVtcGxhdGVSZWYgfHwgZWxsaXBzaXNcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgIFtuZ1RlbXBsYXRlT3V0bGV0Q29udGV4dF09XCJ7ZGlzYWJsZWQ6IHRydWUsIGN1cnJlbnRQYWdlOiBwYWdlfVwiPjwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgPC9hPlxyXG4gICAgICAgIDxhICpuZ0lmPVwiIWlzRWxsaXBzaXMocGFnZU51bWJlcilcIiBjbGFzcz1cInBhZ2UtbGlua1wiIGhyZWYgKGNsaWNrKT1cInNlbGVjdFBhZ2UocGFnZU51bWJlcik7ICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXCI+XHJcbiAgICAgICAgICA8bmctdGVtcGxhdGUgW25nVGVtcGxhdGVPdXRsZXRdPVwidHBsTnVtYmVyPy50ZW1wbGF0ZVJlZiB8fCBkZWZhdWx0TnVtYmVyXCJcclxuICAgICAgICAgICAgICAgICAgICAgICBbbmdUZW1wbGF0ZU91dGxldENvbnRleHRdPVwie2Rpc2FibGVkOiBkaXNhYmxlZCwgJGltcGxpY2l0OiBwYWdlTnVtYmVyLCBjdXJyZW50UGFnZTogcGFnZX1cIj48L25nLXRlbXBsYXRlPlxyXG4gICAgICAgIDwvYT5cclxuICAgICAgPC9saT5cclxuICAgICAgPGxpICpuZ0lmPVwiZGlyZWN0aW9uTGlua3NcIiBjbGFzcz1cInBhZ2UtaXRlbVwiIFtjbGFzcy5kaXNhYmxlZF09XCJuZXh0RGlzYWJsZWQoKVwiPlxyXG4gICAgICAgIDxhIGFyaWEtbGFiZWw9XCJOZXh0XCIgaTE4bi1hcmlhLWxhYmVsPVwiQEBuZ3QucGFnaW5hdGlvbi5uZXh0LWFyaWFcIiBjbGFzcz1cInBhZ2UtbGlua1wiIGhyZWZcclxuICAgICAgICAgIChjbGljayk9XCJzZWxlY3RQYWdlKHBhZ2UrMSk7ICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXCIgW2F0dHIudGFiaW5kZXhdPVwiKGhhc05leHQoKSA/IG51bGwgOiAnLTEnKVwiPlxyXG4gICAgICAgICAgPG5nLXRlbXBsYXRlIFtuZ1RlbXBsYXRlT3V0bGV0XT1cInRwbE5leHQ/LnRlbXBsYXRlUmVmIHx8IG5leHRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgIFtuZ1RlbXBsYXRlT3V0bGV0Q29udGV4dF09XCJ7ZGlzYWJsZWQ6IG5leHREaXNhYmxlZCgpLCBjdXJyZW50UGFnZTogcGFnZX1cIj48L25nLXRlbXBsYXRlPlxyXG4gICAgICAgIDwvYT5cclxuICAgICAgPC9saT5cclxuICAgICAgPGxpICpuZ0lmPVwiYm91bmRhcnlMaW5rc1wiIGNsYXNzPVwicGFnZS1pdGVtXCIgW2NsYXNzLmRpc2FibGVkXT1cIm5leHREaXNhYmxlZCgpXCI+XHJcbiAgICAgICAgPGEgYXJpYS1sYWJlbD1cIkxhc3RcIiBpMThuLWFyaWEtbGFiZWw9XCJAQG5ndC5wYWdpbmF0aW9uLmxhc3QtYXJpYVwiIGNsYXNzPVwicGFnZS1saW5rXCIgaHJlZlxyXG4gICAgICAgICAgKGNsaWNrKT1cInNlbGVjdFBhZ2UocGFnZUNvdW50KTsgJGV2ZW50LnByZXZlbnREZWZhdWx0KClcIiBbYXR0ci50YWJpbmRleF09XCIoaGFzTmV4dCgpID8gbnVsbCA6ICctMScpXCI+XHJcbiAgICAgICAgICA8bmctdGVtcGxhdGUgW25nVGVtcGxhdGVPdXRsZXRdPVwidHBsTGFzdD8udGVtcGxhdGVSZWYgfHwgbGFzdFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgW25nVGVtcGxhdGVPdXRsZXRDb250ZXh0XT1cIntkaXNhYmxlZDogbmV4dERpc2FibGVkKCksIGN1cnJlbnRQYWdlOiBwYWdlfVwiPjwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgPC9hPlxyXG4gICAgICA8L2xpPlxyXG4gICAgPC91bD5cclxuICBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RQYWdpbmF0aW9uIGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuICAgIHBhZ2VDb3VudCA9IDA7XHJcbiAgICBwYWdlczogbnVtYmVyW10gPSBbXTtcclxuXHJcbiAgICBAQ29udGVudENoaWxkKE5ndFBhZ2luYXRpb25FbGxpcHNpcykgdHBsRWxsaXBzaXM6IE5ndFBhZ2luYXRpb25FbGxpcHNpcztcclxuICAgIEBDb250ZW50Q2hpbGQoTmd0UGFnaW5hdGlvbkZpcnN0KSB0cGxGaXJzdDogTmd0UGFnaW5hdGlvbkZpcnN0O1xyXG4gICAgQENvbnRlbnRDaGlsZChOZ3RQYWdpbmF0aW9uTGFzdCkgdHBsTGFzdDogTmd0UGFnaW5hdGlvbkxhc3Q7XHJcbiAgICBAQ29udGVudENoaWxkKE5ndFBhZ2luYXRpb25OZXh0KSB0cGxOZXh0OiBOZ3RQYWdpbmF0aW9uTmV4dDtcclxuICAgIEBDb250ZW50Q2hpbGQoTmd0UGFnaW5hdGlvbk51bWJlcikgdHBsTnVtYmVyOiBOZ3RQYWdpbmF0aW9uTnVtYmVyO1xyXG4gICAgQENvbnRlbnRDaGlsZChOZ3RQYWdpbmF0aW9uUHJldmlvdXMpIHRwbFByZXZpb3VzOiBOZ3RQYWdpbmF0aW9uUHJldmlvdXM7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBXaGV0aGVyIHRvIGRpc2FibGUgYnV0dG9ucyBmcm9tIHVzZXIgaW5wdXRcclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgZGlzYWJsZWQ6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgV2hldGhlciB0byBzaG93IHRoZSBcIkZpcnN0XCIgYW5kIFwiTGFzdFwiIHBhZ2UgbGlua3NcclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgYm91bmRhcnlMaW5rczogYm9vbGVhbjtcclxuXHJcbiAgICAvKipcclxuICAgICAqICBXaGV0aGVyIHRvIHNob3cgdGhlIFwiTmV4dFwiIGFuZCBcIlByZXZpb3VzXCIgcGFnZSBsaW5rc1xyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBkaXJlY3Rpb25MaW5rczogYm9vbGVhbjtcclxuXHJcbiAgICAvKipcclxuICAgICAqICBXaGV0aGVyIHRvIHNob3cgZWxsaXBzaXMgc3ltYm9scyBhbmQgZmlyc3QvbGFzdCBwYWdlIG51bWJlcnMgd2hlbiBtYXhTaXplID4gbnVtYmVyIG9mIHBhZ2VzXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGVsbGlwc2VzOiBib29sZWFuO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogIFdoZXRoZXIgdG8gcm90YXRlIHBhZ2VzIHdoZW4gbWF4U2l6ZSA+IG51bWJlciBvZiBwYWdlcy5cclxuICAgICAqICBDdXJyZW50IHBhZ2Ugd2lsbCBiZSBpbiB0aGUgbWlkZGxlXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHJvdGF0ZTogYm9vbGVhbjtcclxuXHJcbiAgICAvKipcclxuICAgICAqICBOdW1iZXIgb2YgaXRlbXMgaW4gY29sbGVjdGlvbi5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgY29sbGVjdGlvblNpemU6IG51bWJlcjtcclxuXHJcbiAgICAvKipcclxuICAgICAqICBNYXhpbXVtIG51bWJlciBvZiBwYWdlcyB0byBkaXNwbGF5LlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBtYXhTaXplOiBudW1iZXI7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgQ3VycmVudCBwYWdlLiBQYWdlIG51bWJlcnMgc3RhcnQgd2l0aCAxXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHBhZ2UgPSAxO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogIE51bWJlciBvZiBpdGVtcyBwZXIgcGFnZS5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgcGFnZVNpemU6IG51bWJlcjtcclxuXHJcbiAgICAvKipcclxuICAgICAqICBBbiBldmVudCBmaXJlZCB3aGVuIHRoZSBwYWdlIGlzIGNoYW5nZWQuXHJcbiAgICAgKiAgRXZlbnQncyBwYXlsb2FkIGVxdWFscyB0byB0aGUgbmV3bHkgc2VsZWN0ZWQgcGFnZS5cclxuICAgICAqICBXaWxsIGZpcmUgb25seSBpZiBjb2xsZWN0aW9uIHNpemUgaXMgc2V0IGFuZCBhbGwgdmFsdWVzIGFyZSB2YWxpZC5cclxuICAgICAqICBQYWdlIG51bWJlcnMgc3RhcnQgd2l0aCAxXHJcbiAgICAgKi9cclxuICAgIEBPdXRwdXQoKSBwYWdlQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxudW1iZXI+KHRydWUpO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogUGFnaW5hdGlvbiBkaXNwbGF5IHNpemU6IHNtYWxsIG9yIGxhcmdlXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHNpemU6ICdzbScgfCAnbGcnO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNvbmZpZzogTmd0UGFnaW5hdGlvbkNvbmZpZykge1xyXG4gICAgICAgIHRoaXMuZGlzYWJsZWQgPSBjb25maWcuZGlzYWJsZWQ7XHJcbiAgICAgICAgdGhpcy5ib3VuZGFyeUxpbmtzID0gY29uZmlnLmJvdW5kYXJ5TGlua3M7XHJcbiAgICAgICAgdGhpcy5kaXJlY3Rpb25MaW5rcyA9IGNvbmZpZy5kaXJlY3Rpb25MaW5rcztcclxuICAgICAgICB0aGlzLmVsbGlwc2VzID0gY29uZmlnLmVsbGlwc2VzO1xyXG4gICAgICAgIHRoaXMubWF4U2l6ZSA9IGNvbmZpZy5tYXhTaXplO1xyXG4gICAgICAgIHRoaXMucGFnZVNpemUgPSBjb25maWcucGFnZVNpemU7XHJcbiAgICAgICAgdGhpcy5yb3RhdGUgPSBjb25maWcucm90YXRlO1xyXG4gICAgICAgIHRoaXMuc2l6ZSA9IGNvbmZpZy5zaXplO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc1ByZXZpb3VzKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnBhZ2UgPiAxO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc05leHQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucGFnZSA8IHRoaXMucGFnZUNvdW50O1xyXG4gICAgfVxyXG5cclxuICAgIG5leHREaXNhYmxlZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gIXRoaXMuaGFzTmV4dCgpIHx8IHRoaXMuZGlzYWJsZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJldmlvdXNEaXNhYmxlZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gIXRoaXMuaGFzUHJldmlvdXMoKSB8fCB0aGlzLmRpc2FibGVkO1xyXG4gICAgfVxyXG5cclxuICAgIHNlbGVjdFBhZ2UocGFnZU51bWJlcjogbnVtYmVyKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fdXBkYXRlUGFnZXMocGFnZU51bWJlcik7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX3VwZGF0ZVBhZ2VzKHRoaXMucGFnZSk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNFbGxpcHNpcyhwYWdlTnVtYmVyKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHBhZ2VOdW1iZXIgPT09IC0xO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQXBwZW5kcyBlbGxpcHNlcyBhbmQgZmlyc3QvbGFzdCBwYWdlIG51bWJlciB0byB0aGUgZGlzcGxheWVkIHBhZ2VzXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgX2FwcGx5RWxsaXBzZXMoc3RhcnQ6IG51bWJlciwgZW5kOiBudW1iZXIpIHtcclxuICAgICAgICBpZiAodGhpcy5lbGxpcHNlcykge1xyXG4gICAgICAgICAgICBpZiAoc3RhcnQgPiAwKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoc3RhcnQgPiAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wYWdlcy51bnNoaWZ0KC0xKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMucGFnZXMudW5zaGlmdCgxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoZW5kIDwgdGhpcy5wYWdlQ291bnQpIHtcclxuICAgICAgICAgICAgICAgIGlmIChlbmQgPCAodGhpcy5wYWdlQ291bnQgLSAxKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucGFnZXMucHVzaCgtMSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLnBhZ2VzLnB1c2godGhpcy5wYWdlQ291bnQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUm90YXRlcyBwYWdlIG51bWJlcnMgYmFzZWQgb24gbWF4U2l6ZSBpdGVtcyB2aXNpYmxlLlxyXG4gICAgICogQ3VycmVudGx5IHNlbGVjdGVkIHBhZ2Ugc3RheXMgaW4gdGhlIG1pZGRsZTpcclxuICAgICAqXHJcbiAgICAgKiBFeC4gZm9yIHNlbGVjdGVkIHBhZ2UgPSA2OlxyXG4gICAgICogWzUsKjYqLDddIGZvciBtYXhTaXplID0gM1xyXG4gICAgICogWzQsNSwqNiosN10gZm9yIG1heFNpemUgPSA0XHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgX2FwcGx5Um90YXRpb24oKTogW251bWJlciwgbnVtYmVyXSB7XHJcbiAgICAgICAgbGV0IHN0YXJ0ID0gMDtcclxuICAgICAgICBsZXQgZW5kID0gdGhpcy5wYWdlQ291bnQ7XHJcbiAgICAgICAgbGV0IGxlZnRPZmZzZXQgPSBNYXRoLmZsb29yKHRoaXMubWF4U2l6ZSAvIDIpO1xyXG4gICAgICAgIGxldCByaWdodE9mZnNldCA9IHRoaXMubWF4U2l6ZSAlIDIgPT09IDAgPyBsZWZ0T2Zmc2V0IC0gMSA6IGxlZnRPZmZzZXQ7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnBhZ2UgPD0gbGVmdE9mZnNldCkge1xyXG4gICAgICAgICAgICAvLyB2ZXJ5IGJlZ2lubmluZywgbm8gcm90YXRpb24gLT4gWzAuLm1heFNpemVdXHJcbiAgICAgICAgICAgIGVuZCA9IHRoaXMubWF4U2l6ZTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMucGFnZUNvdW50IC0gdGhpcy5wYWdlIDwgbGVmdE9mZnNldCkge1xyXG4gICAgICAgICAgICAvLyB2ZXJ5IGVuZCwgbm8gcm90YXRpb24gLT4gW2xlbi1tYXhTaXplLi5sZW5dXHJcbiAgICAgICAgICAgIHN0YXJ0ID0gdGhpcy5wYWdlQ291bnQgLSB0aGlzLm1heFNpemU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLy8gcm90YXRlXHJcbiAgICAgICAgICAgIHN0YXJ0ID0gdGhpcy5wYWdlIC0gbGVmdE9mZnNldCAtIDE7XHJcbiAgICAgICAgICAgIGVuZCA9IHRoaXMucGFnZSArIHJpZ2h0T2Zmc2V0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIFtzdGFydCwgZW5kXTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFBhZ2luYXRlcyBwYWdlIG51bWJlcnMgYmFzZWQgb24gbWF4U2l6ZSBpdGVtcyBwZXIgcGFnZVxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIF9hcHBseVBhZ2luYXRpb24oKTogW251bWJlciwgbnVtYmVyXSB7XHJcbiAgICAgICAgbGV0IHBhZ2UgPSBNYXRoLmNlaWwodGhpcy5wYWdlIC8gdGhpcy5tYXhTaXplKSAtIDE7XHJcbiAgICAgICAgbGV0IHN0YXJ0ID0gcGFnZSAqIHRoaXMubWF4U2l6ZTtcclxuICAgICAgICBsZXQgZW5kID0gc3RhcnQgKyB0aGlzLm1heFNpemU7XHJcblxyXG4gICAgICAgIHJldHVybiBbc3RhcnQsIGVuZF07XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfc2V0UGFnZUluUmFuZ2UobmV3UGFnZU5vKSB7XHJcbiAgICAgICAgY29uc3QgcHJldlBhZ2VObyA9IHRoaXMucGFnZTtcclxuICAgICAgICB0aGlzLnBhZ2UgPSBnZXRWYWx1ZUluUmFuZ2UobmV3UGFnZU5vLCB0aGlzLnBhZ2VDb3VudCwgMSk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnBhZ2UgIT09IHByZXZQYWdlTm8gJiYgaXNOdW1iZXIodGhpcy5jb2xsZWN0aW9uU2l6ZSkpIHtcclxuICAgICAgICAgICAgdGhpcy5wYWdlQ2hhbmdlLmVtaXQodGhpcy5wYWdlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfdXBkYXRlUGFnZXMobmV3UGFnZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5wYWdlQ291bnQgPSBNYXRoLmNlaWwodGhpcy5jb2xsZWN0aW9uU2l6ZSAvIHRoaXMucGFnZVNpemUpO1xyXG5cclxuICAgICAgICBpZiAoIWlzTnVtYmVyKHRoaXMucGFnZUNvdW50KSkge1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2VDb3VudCA9IDA7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBmaWxsLWluIG1vZGVsIG5lZWRlZCB0byByZW5kZXIgcGFnZXNcclxuICAgICAgICB0aGlzLnBhZ2VzLmxlbmd0aCA9IDA7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDE7IGkgPD0gdGhpcy5wYWdlQ291bnQ7IGkrKykge1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2VzLnB1c2goaSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBzZXQgcGFnZSB3aXRoaW4gMS4ubWF4IHJhbmdlXHJcbiAgICAgICAgdGhpcy5fc2V0UGFnZUluUmFuZ2UobmV3UGFnZSk7XHJcblxyXG4gICAgICAgIC8vIGFwcGx5IG1heFNpemUgaWYgbmVjZXNzYXJ5XHJcbiAgICAgICAgaWYgKHRoaXMubWF4U2l6ZSA+IDAgJiYgdGhpcy5wYWdlQ291bnQgPiB0aGlzLm1heFNpemUpIHtcclxuICAgICAgICAgICAgbGV0IHN0YXJ0ID0gMDtcclxuICAgICAgICAgICAgbGV0IGVuZCA9IHRoaXMucGFnZUNvdW50O1xyXG5cclxuICAgICAgICAgICAgLy8gZWl0aGVyIHBhZ2luYXRpbmcgb3Igcm90YXRpbmcgcGFnZSBudW1iZXJzXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnJvdGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgW3N0YXJ0LCBlbmRdID0gdGhpcy5fYXBwbHlSb3RhdGlvbigpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgW3N0YXJ0LCBlbmRdID0gdGhpcy5fYXBwbHlQYWdpbmF0aW9uKCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMucGFnZXMgPSB0aGlzLnBhZ2VzLnNsaWNlKHN0YXJ0LCBlbmQpO1xyXG5cclxuICAgICAgICAgICAgLy8gYWRkaW5nIGVsbGlwc2VzXHJcbiAgICAgICAgICAgIHRoaXMuX2FwcGx5RWxsaXBzZXMoc3RhcnQsIGVuZCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==