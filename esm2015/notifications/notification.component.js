/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, HostBinding, Input } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { Subject } from 'rxjs';
export class NgtNotificationComponent {
    /**
     * @param {?} elRef
     */
    constructor(elRef) {
        this.elRef = elRef;
        this.animation = true;
        this.display = 'block';
        this.aside = false;
        this.timer = new Subject();
        this.progress = 0;
        this.lastProgress = 0;
        this.result = new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this._resolve = resolve;
            this._reject = reject;
        }));
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.timeout > 0) {
            this.initProgressBar();
        }
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.timeout && this.timer.next();
    }
    /**
     * @return {?}
     */
    initProgressBar() {
        /** @type {?} */
        const k = this.timeout / 100;
        this.progressBar = this.elRef.nativeElement.querySelector('.notification-progress-bar');
        /** @type {?} */
        const sub = this.timer.subscribe((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const started = new Date().getTime();
            this.interval = setInterval((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const curTime = new Date().getTime();
                this.progress = this.lastProgress + (curTime - started) / k;
                if (this.progress >= 100) {
                    clearInterval(this.interval);
                    sub.unsubscribe();
                    this._ref.destroy();
                }
                else {
                    this.progressBar['style'].width = this.progress + '%';
                }
            }), 0);
        }));
    }
    /**
     * @return {?}
     */
    closeNotification() {
        this._resolve();
        clearInterval(this.interval);
        this._ref.destroy();
    }
    /**
     * @return {?}
     */
    onMouseenter() {
        clearInterval(this.interval);
        this.lastProgress = this.progress;
    }
    /**
     * @return {?}
     */
    onMouseleave() {
        this.timer.next();
    }
}
NgtNotificationComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-notification',
                template: `
        <div
            (mouseenter)="onMouseenter()" (mouseleave)="onMouseleave()"
            class="notification show {{ typeClass }} {{ aside ? 'with-aside' : '' }}">
            <div class="notification-progress-bar"></div>
            <div *ngIf="aside" class="notification-aside" [innerHtml]="aside"></div>
            <div class="notification-inner">
                <div class="notification-header">
                    <span class="notification-title" [innerHtml]="title"></span>
                    <button (click)="closeNotification()" class="notification-close"><i class="ft-x"></i></button>
                </div>
                <div class="notification-body" [innerHtml]="message"></div>
            </div>
        </div>

    `,
                animations: [
                    trigger('flyInOut', [
                        transition('void => *', [
                            style({ transform: 'translateX(-100%)', opacity: 0 }),
                            animate(150)
                        ]),
                        transition('* => void', [
                            animate(250, style({ transform: 'translateX(100%)', opacity: 0 }))
                        ])
                    ])
                ]
            }] }
];
/** @nocollapse */
NgtNotificationComponent.ctorParameters = () => [
    { type: ElementRef }
];
NgtNotificationComponent.propDecorators = {
    animation: [{ type: HostBinding, args: ['@flyInOut',] }],
    display: [{ type: HostBinding, args: ['style.display',] }],
    type: [{ type: Input }],
    typeClass: [{ type: Input }],
    title: [{ type: Input }],
    message: [{ type: Input }],
    aside: [{ type: Input }],
    timeout: [{ type: Input }],
    _ref: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NgtNotificationComponent.prototype.animation;
    /** @type {?} */
    NgtNotificationComponent.prototype.display;
    /** @type {?} */
    NgtNotificationComponent.prototype.type;
    /** @type {?} */
    NgtNotificationComponent.prototype.typeClass;
    /** @type {?} */
    NgtNotificationComponent.prototype.title;
    /** @type {?} */
    NgtNotificationComponent.prototype.message;
    /** @type {?} */
    NgtNotificationComponent.prototype.aside;
    /** @type {?} */
    NgtNotificationComponent.prototype.timeout;
    /** @type {?} */
    NgtNotificationComponent.prototype._ref;
    /** @type {?} */
    NgtNotificationComponent.prototype.timer;
    /** @type {?} */
    NgtNotificationComponent.prototype.progressBar;
    /** @type {?} */
    NgtNotificationComponent.prototype.interval;
    /** @type {?} */
    NgtNotificationComponent.prototype.progress;
    /** @type {?} */
    NgtNotificationComponent.prototype.lastProgress;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationComponent.prototype._resolve;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationComponent.prototype._reject;
    /**
     * A promise that is resolved when the notification is closed
     * @type {?}
     */
    NgtNotificationComponent.prototype.result;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationComponent.prototype.elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQWlCLFNBQVMsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUNqRyxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDMUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQWtDL0IsTUFBTSxPQUFPLHdCQUF3Qjs7OztJQXVCakMsWUFBb0IsS0FBaUI7UUFBakIsVUFBSyxHQUFMLEtBQUssQ0FBWTtRQXRCWCxjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2IsWUFBTyxHQUFHLE9BQU8sQ0FBQztRQUt2QyxVQUFLLEdBQUcsS0FBSyxDQUFDO1FBR3ZCLFVBQUssR0FBRyxJQUFJLE9BQU8sRUFBVSxDQUFDO1FBRzlCLGFBQVEsR0FBRyxDQUFDLENBQUM7UUFDYixpQkFBWSxHQUFHLENBQUMsQ0FBQztRQVViLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxPQUFPOzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFO1lBQzFDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1lBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQzFCLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztTQUMxQjtJQUNMLENBQUM7Ozs7SUFFRCxlQUFlO1FBQ1gsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCxlQUFlOztjQUNMLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUc7UUFDNUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsNEJBQTRCLENBQUMsQ0FBQzs7Y0FDbEYsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUzs7O1FBQzVCLEdBQUcsRUFBRTs7a0JBQ0ssT0FBTyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxRQUFRLEdBQUcsV0FBVzs7O1lBQUMsR0FBRyxFQUFFOztzQkFDdkIsT0FBTyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFO2dCQUNwQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUM1RCxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksR0FBRyxFQUFFO29CQUN0QixhQUFhLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUM3QixHQUFHLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ2xCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNILElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO2lCQUN6RDtZQUNMLENBQUMsR0FBRSxDQUFDLENBQUMsQ0FBQztRQUNWLENBQUMsRUFDSjtJQUNMLENBQUM7Ozs7SUFFRCxpQkFBaUI7UUFDYixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEIsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3hCLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1IsYUFBYSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDdEMsQ0FBQzs7OztJQUVELFlBQVk7UUFDUixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3RCLENBQUM7OztZQXhHSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7S0FlVDtnQkFDRCxVQUFVLEVBQUU7b0JBQ1IsT0FBTyxDQUFDLFVBQVUsRUFBRTt3QkFDaEIsVUFBVSxDQUFDLFdBQVcsRUFBRTs0QkFDcEIsS0FBSyxDQUFDLEVBQUMsU0FBUyxFQUFFLG1CQUFtQixFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQzs0QkFDbkQsT0FBTyxDQUFDLEdBQUcsQ0FBQzt5QkFDZixDQUFDO3dCQUNGLFVBQVUsQ0FBQyxXQUFXLEVBQUU7NEJBQ3BCLE9BQU8sQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLEVBQUMsU0FBUyxFQUFFLGtCQUFrQixFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQyxDQUFDO3lCQUNuRSxDQUFDO3FCQUNMLENBQUM7aUJBQ0w7YUFDSjs7OztZQW5Da0MsVUFBVTs7O3dCQXFDeEMsV0FBVyxTQUFDLFdBQVc7c0JBQ3ZCLFdBQVcsU0FBQyxlQUFlO21CQUMzQixLQUFLO3dCQUNMLEtBQUs7b0JBQ0wsS0FBSztzQkFDTCxLQUFLO29CQUNMLEtBQUs7c0JBQ0wsS0FBSzttQkFDTCxLQUFLOzs7O0lBUk4sNkNBQTJDOztJQUMzQywyQ0FBZ0Q7O0lBQ2hELHdDQUFtQjs7SUFDbkIsNkNBQTJCOztJQUMzQix5Q0FBdUI7O0lBQ3ZCLDJDQUF5Qjs7SUFDekIseUNBQXVCOztJQUN2QiwyQ0FBc0I7O0lBQ3RCLHdDQUFtQjs7SUFDbkIseUNBQThCOztJQUM5QiwrQ0FBd0I7O0lBQ3hCLDRDQUFTOztJQUNULDRDQUFhOztJQUNiLGdEQUFpQjs7Ozs7SUFFakIsNENBQXlDOzs7OztJQUN6QywyQ0FBd0M7Ozs7O0lBSXhDLDBDQUFxQjs7Ozs7SUFFVCx5Q0FBeUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBZnRlclZpZXdJbml0LCBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEhvc3RCaW5kaW5nLCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IGFuaW1hdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCB0cmlnZ2VyIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IE5ndE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICduZ3Qtbm90aWZpY2F0aW9uJyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPGRpdlxyXG4gICAgICAgICAgICAobW91c2VlbnRlcik9XCJvbk1vdXNlZW50ZXIoKVwiIChtb3VzZWxlYXZlKT1cIm9uTW91c2VsZWF2ZSgpXCJcclxuICAgICAgICAgICAgY2xhc3M9XCJub3RpZmljYXRpb24gc2hvdyB7eyB0eXBlQ2xhc3MgfX0ge3sgYXNpZGUgPyAnd2l0aC1hc2lkZScgOiAnJyB9fVwiPlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVwibm90aWZpY2F0aW9uLXByb2dyZXNzLWJhclwiPjwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2ICpuZ0lmPVwiYXNpZGVcIiBjbGFzcz1cIm5vdGlmaWNhdGlvbi1hc2lkZVwiIFtpbm5lckh0bWxdPVwiYXNpZGVcIj48L2Rpdj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm5vdGlmaWNhdGlvbi1pbm5lclwiPlxyXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm5vdGlmaWNhdGlvbi1oZWFkZXJcIj5cclxuICAgICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz1cIm5vdGlmaWNhdGlvbi10aXRsZVwiIFtpbm5lckh0bWxdPVwidGl0bGVcIj48L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiAoY2xpY2spPVwiY2xvc2VOb3RpZmljYXRpb24oKVwiIGNsYXNzPVwibm90aWZpY2F0aW9uLWNsb3NlXCI+PGkgY2xhc3M9XCJmdC14XCI+PC9pPjwvYnV0dG9uPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibm90aWZpY2F0aW9uLWJvZHlcIiBbaW5uZXJIdG1sXT1cIm1lc3NhZ2VcIj48L2Rpdj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgYCxcclxuICAgIGFuaW1hdGlvbnM6IFtcclxuICAgICAgICB0cmlnZ2VyKCdmbHlJbk91dCcsIFtcclxuICAgICAgICAgICAgdHJhbnNpdGlvbigndm9pZCA9PiAqJywgW1xyXG4gICAgICAgICAgICAgICAgc3R5bGUoe3RyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoLTEwMCUpJywgb3BhY2l0eTogMH0pLFxyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZSgxNTApXHJcbiAgICAgICAgICAgIF0pLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCcqID0+IHZvaWQnLCBbXHJcbiAgICAgICAgICAgICAgICBhbmltYXRlKDI1MCwgc3R5bGUoe3RyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMTAwJSknLCBvcGFjaXR5OiAwfSkpXHJcbiAgICAgICAgICAgIF0pXHJcbiAgICAgICAgXSlcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndE5vdGlmaWNhdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XHJcbiAgICBASG9zdEJpbmRpbmcoJ0BmbHlJbk91dCcpIGFuaW1hdGlvbiA9IHRydWU7XHJcbiAgICBASG9zdEJpbmRpbmcoJ3N0eWxlLmRpc3BsYXknKSBkaXNwbGF5ID0gJ2Jsb2NrJztcclxuICAgIEBJbnB1dCgpIHR5cGU6IGFueTtcclxuICAgIEBJbnB1dCgpIHR5cGVDbGFzczogc3RyaW5nO1xyXG4gICAgQElucHV0KCkgdGl0bGU6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIG1lc3NhZ2U6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIGFzaWRlID0gZmFsc2U7XHJcbiAgICBASW5wdXQoKSB0aW1lb3V0OiBhbnk7XHJcbiAgICBASW5wdXQoKSBfcmVmOiBhbnk7XHJcbiAgICB0aW1lciA9IG5ldyBTdWJqZWN0PG51bWJlcj4oKTtcclxuICAgIHByb2dyZXNzQmFyOiBFbGVtZW50UmVmO1xyXG4gICAgaW50ZXJ2YWw7XHJcbiAgICBwcm9ncmVzcyA9IDA7XHJcbiAgICBsYXN0UHJvZ3Jlc3MgPSAwO1xyXG5cclxuICAgIHByaXZhdGUgX3Jlc29sdmU6IChyZXN1bHQ/OiBhbnkpID0+IHZvaWQ7XHJcbiAgICBwcml2YXRlIF9yZWplY3Q6IChyZWFzb24/OiBhbnkpID0+IHZvaWQ7XHJcbiAgICAvKipcclxuICAgICAqIEEgcHJvbWlzZSB0aGF0IGlzIHJlc29sdmVkIHdoZW4gdGhlIG5vdGlmaWNhdGlvbiBpcyBjbG9zZWRcclxuICAgICAqL1xyXG4gICAgcmVzdWx0OiBQcm9taXNlPGFueT47XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBlbFJlZjogRWxlbWVudFJlZikge1xyXG4gICAgICAgIHRoaXMucmVzdWx0ID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9yZXNvbHZlID0gcmVzb2x2ZTtcclxuICAgICAgICAgICAgdGhpcy5fcmVqZWN0ID0gcmVqZWN0O1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLnRpbWVvdXQgPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaW5pdFByb2dyZXNzQmFyKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgICAgICB0aGlzLnRpbWVvdXQgJiYgdGhpcy50aW1lci5uZXh0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdFByb2dyZXNzQmFyKCkge1xyXG4gICAgICAgIGNvbnN0IGsgPSB0aGlzLnRpbWVvdXQgLyAxMDA7XHJcbiAgICAgICAgdGhpcy5wcm9ncmVzc0JhciA9IHRoaXMuZWxSZWYubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKCcubm90aWZpY2F0aW9uLXByb2dyZXNzLWJhcicpO1xyXG4gICAgICAgIGNvbnN0IHN1YiA9IHRoaXMudGltZXIuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzdGFydGVkID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmludGVydmFsID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGN1clRpbWUgPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2dyZXNzID0gdGhpcy5sYXN0UHJvZ3Jlc3MgKyAoY3VyVGltZSAtIHN0YXJ0ZWQpIC8gaztcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5wcm9ncmVzcyA+PSAxMDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLmludGVydmFsKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3ViLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuX3JlZi5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9ncmVzc0Jhclsnc3R5bGUnXS53aWR0aCA9IHRoaXMucHJvZ3Jlc3MgKyAnJSc7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSwgMCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGNsb3NlTm90aWZpY2F0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuX3Jlc29sdmUoKTtcclxuICAgICAgICBjbGVhckludGVydmFsKHRoaXMuaW50ZXJ2YWwpO1xyXG4gICAgICAgIHRoaXMuX3JlZi5kZXN0cm95KCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25Nb3VzZWVudGVyKCkge1xyXG4gICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5pbnRlcnZhbCk7XHJcbiAgICAgICAgdGhpcy5sYXN0UHJvZ3Jlc3MgPSB0aGlzLnByb2dyZXNzO1xyXG4gICAgfVxyXG5cclxuICAgIG9uTW91c2VsZWF2ZSgpIHtcclxuICAgICAgICB0aGlzLnRpbWVyLm5leHQoKTtcclxuICAgIH1cclxufVxyXG4iXX0=