/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ComponentFactoryResolver, Injectable } from '@angular/core';
import { NgtNotificationType } from './notification.model';
import { NgtNotificationStack } from './notification-stack';
import * as i0 from "@angular/core";
import * as i1 from "./notification-stack";
export class NgtNotificationService {
    /**
     * @param {?} stack
     * @param {?} _moduleCFR
     */
    constructor(stack, _moduleCFR) {
        this.stack = stack;
        this._moduleCFR = _moduleCFR;
        this.timeout = 2500;
        this.notificationsData = {
            success: {
                typeClass: 'notification-success',
                aside: '<i class="ft-check"></i>'
            },
            error: {
                typeClass: 'notification-danger',
                aside: '<i class="ft-x"></i>'
            },
            info: {
                typeClass: 'notification-info',
                aside: '<i class="ft-info"></i>'
            },
            warning: {
                typeClass: 'notification-warning',
                aside: '<i class="ft-alert-triangle"></i>'
            },
        };
    }
    /**
     * @param {?} type
     * @param {?} kind
     * @return {?}
     */
    getInfo(type, kind) {
        if (!kind) {
            return;
        }
        // return css class based on notification type
        switch (type) {
            case NgtNotificationType.Success:
                return this.notificationsData.success[kind];
            case NgtNotificationType.Error:
                return this.notificationsData.error[kind];
            case NgtNotificationType.Info:
                return this.notificationsData.info[kind];
            case NgtNotificationType.Warning:
                return this.notificationsData.warning[kind];
        }
    }
    /**
     * @param {?} data
     * @return {?}
     */
    success(data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Success }, data));
    }
    /**
     * @param {?} data
     * @return {?}
     */
    error(data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Error }, data));
    }
    /**
     * @param {?} data
     * @return {?}
     */
    info(data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Info }, data));
    }
    /**
     * @param {?} data
     * @return {?}
     */
    warn(data) {
        this.notification(Object.assign({}, { type: NgtNotificationType.Warning }, data));
    }
    /**
     * @param {?} data
     * @return {?}
     */
    notification(data) {
        if (data.type || data.type === 0) {
            data.typeClass = this.getInfo(data.type, 'typeClass');
            if (!data.aside) {
                data.aside = this.getInfo(data.type, 'aside');
            }
        }
        /** @type {?} */
        const combinedOptions = (/** @type {?} */ (Object.assign({}, { timeout: this.timeout }, data)));
        this.stack.show(this._moduleCFR, combinedOptions);
    }
    /**
     * @return {?}
     */
    clear() {
        // clear alerts
        this.stack.clearAll();
    }
}
NgtNotificationService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NgtNotificationService.ctorParameters = () => [
    { type: NgtNotificationStack },
    { type: ComponentFactoryResolver }
];
/** @nocollapse */ NgtNotificationService.ngInjectableDef = i0.defineInjectable({ factory: function NgtNotificationService_Factory() { return new NgtNotificationService(i0.inject(i1.NgtNotificationStack), i0.inject(i0.ComponentFactoryResolver)); }, token: NgtNotificationService, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtNotificationService.prototype.timeout;
    /** @type {?} */
    NgtNotificationService.prototype.notificationsData;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationService.prototype.stack;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationService.prototype._moduleCFR;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbIm5vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFckUsT0FBTyxFQUFtQixtQkFBbUIsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQzVFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDOzs7QUFLNUQsTUFBTSxPQUFPLHNCQUFzQjs7Ozs7SUFxQi9CLFlBQW9CLEtBQTJCLEVBQzNCLFVBQW9DO1FBRHBDLFVBQUssR0FBTCxLQUFLLENBQXNCO1FBQzNCLGVBQVUsR0FBVixVQUFVLENBQTBCO1FBckJ4RCxZQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ2Ysc0JBQWlCLEdBQUc7WUFDaEIsT0FBTyxFQUFFO2dCQUNMLFNBQVMsRUFBRSxzQkFBc0I7Z0JBQ2pDLEtBQUssRUFBRSwwQkFBMEI7YUFDcEM7WUFDRCxLQUFLLEVBQUU7Z0JBQ0gsU0FBUyxFQUFFLHFCQUFxQjtnQkFDaEMsS0FBSyxFQUFFLHNCQUFzQjthQUNoQztZQUNELElBQUksRUFBRTtnQkFDRixTQUFTLEVBQUUsbUJBQW1CO2dCQUM5QixLQUFLLEVBQUUseUJBQXlCO2FBQ25DO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLFNBQVMsRUFBRSxzQkFBc0I7Z0JBQ2pDLEtBQUssRUFBRSxtQ0FBbUM7YUFDN0M7U0FDSixDQUFDO0lBSUYsQ0FBQzs7Ozs7O0lBRUQsT0FBTyxDQUFDLElBQUksRUFBRSxJQUFJO1FBQ2QsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNQLE9BQU87U0FDVjtRQUVELDhDQUE4QztRQUM5QyxRQUFRLElBQUksRUFBRTtZQUNWLEtBQUssbUJBQW1CLENBQUMsT0FBTztnQkFDNUIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2hELEtBQUssbUJBQW1CLENBQUMsS0FBSztnQkFDMUIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlDLEtBQUssbUJBQW1CLENBQUMsSUFBSTtnQkFDekIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzdDLEtBQUssbUJBQW1CLENBQUMsT0FBTztnQkFDNUIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ25EO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsSUFBSTtRQUNSLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsRUFBQyxJQUFJLEVBQUUsbUJBQW1CLENBQUMsT0FBTyxFQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUNwRixDQUFDOzs7OztJQUVELEtBQUssQ0FBQyxJQUFJO1FBQ04sSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxFQUFDLElBQUksRUFBRSxtQkFBbUIsQ0FBQyxLQUFLLEVBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQ2xGLENBQUM7Ozs7O0lBRUQsSUFBSSxDQUFDLElBQUk7UUFDTCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLEVBQUMsSUFBSSxFQUFFLG1CQUFtQixDQUFDLElBQUksRUFBQyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDakYsQ0FBQzs7Ozs7SUFFRCxJQUFJLENBQUMsSUFBSTtRQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsRUFBQyxJQUFJLEVBQUUsbUJBQW1CLENBQUMsT0FBTyxFQUFDLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUNwRixDQUFDOzs7OztJQUVELFlBQVksQ0FBQyxJQUFJO1FBQ2IsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssQ0FBQyxFQUFFO1lBQzlCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBRXRELElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO2dCQUNiLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2FBQ2pEO1NBQ0o7O2NBQ0ssZUFBZSxHQUFHLG1CQUFpQixNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxFQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFDLEVBQUUsSUFBSSxDQUFDLEVBQUE7UUFDekYsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxlQUFlLENBQUMsQ0FBQztJQUN0RCxDQUFDOzs7O0lBRUQsS0FBSztRQUNELGVBQWU7UUFDZixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzFCLENBQUM7OztZQTdFSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFKUSxvQkFBb0I7WUFIcEIsd0JBQXdCOzs7OztJQVM3Qix5Q0FBZTs7SUFDZixtREFpQkU7Ozs7O0lBRVUsdUNBQW1DOzs7OztJQUNuQyw0Q0FBNEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IE5ndE5vdGlmaWNhdGlvbiwgTmd0Tm90aWZpY2F0aW9uVHlwZSB9IGZyb20gJy4vbm90aWZpY2F0aW9uLm1vZGVsJztcclxuaW1wb3J0IHsgTmd0Tm90aWZpY2F0aW9uU3RhY2sgfSBmcm9tICcuL25vdGlmaWNhdGlvbi1zdGFjayc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndE5vdGlmaWNhdGlvblNlcnZpY2Uge1xyXG4gICAgdGltZW91dCA9IDI1MDA7XHJcbiAgICBub3RpZmljYXRpb25zRGF0YSA9IHtcclxuICAgICAgICBzdWNjZXNzOiB7XHJcbiAgICAgICAgICAgIHR5cGVDbGFzczogJ25vdGlmaWNhdGlvbi1zdWNjZXNzJyxcclxuICAgICAgICAgICAgYXNpZGU6ICc8aSBjbGFzcz1cImZ0LWNoZWNrXCI+PC9pPidcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVycm9yOiB7XHJcbiAgICAgICAgICAgIHR5cGVDbGFzczogJ25vdGlmaWNhdGlvbi1kYW5nZXInLFxyXG4gICAgICAgICAgICBhc2lkZTogJzxpIGNsYXNzPVwiZnQteFwiPjwvaT4nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBpbmZvOiB7XHJcbiAgICAgICAgICAgIHR5cGVDbGFzczogJ25vdGlmaWNhdGlvbi1pbmZvJyxcclxuICAgICAgICAgICAgYXNpZGU6ICc8aSBjbGFzcz1cImZ0LWluZm9cIj48L2k+J1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgd2FybmluZzoge1xyXG4gICAgICAgICAgICB0eXBlQ2xhc3M6ICdub3RpZmljYXRpb24td2FybmluZycsXHJcbiAgICAgICAgICAgIGFzaWRlOiAnPGkgY2xhc3M9XCJmdC1hbGVydC10cmlhbmdsZVwiPjwvaT4nXHJcbiAgICAgICAgfSxcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBzdGFjazogTmd0Tm90aWZpY2F0aW9uU3RhY2ssXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9tb2R1bGVDRlI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcikge1xyXG4gICAgfVxyXG5cclxuICAgIGdldEluZm8odHlwZSwga2luZCkge1xyXG4gICAgICAgIGlmICgha2luZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyByZXR1cm4gY3NzIGNsYXNzIGJhc2VkIG9uIG5vdGlmaWNhdGlvbiB0eXBlXHJcbiAgICAgICAgc3dpdGNoICh0eXBlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgTmd0Tm90aWZpY2F0aW9uVHlwZS5TdWNjZXNzOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubm90aWZpY2F0aW9uc0RhdGEuc3VjY2Vzc1traW5kXTtcclxuICAgICAgICAgICAgY2FzZSBOZ3ROb3RpZmljYXRpb25UeXBlLkVycm9yOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubm90aWZpY2F0aW9uc0RhdGEuZXJyb3Jba2luZF07XHJcbiAgICAgICAgICAgIGNhc2UgTmd0Tm90aWZpY2F0aW9uVHlwZS5JbmZvOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubm90aWZpY2F0aW9uc0RhdGEuaW5mb1traW5kXTtcclxuICAgICAgICAgICAgY2FzZSBOZ3ROb3RpZmljYXRpb25UeXBlLldhcm5pbmc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5ub3RpZmljYXRpb25zRGF0YS53YXJuaW5nW2tpbmRdO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzdWNjZXNzKGRhdGEpIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbihPYmplY3QuYXNzaWduKHt9LCB7dHlwZTogTmd0Tm90aWZpY2F0aW9uVHlwZS5TdWNjZXNzfSwgZGF0YSkpO1xyXG4gICAgfVxyXG5cclxuICAgIGVycm9yKGRhdGEpIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbihPYmplY3QuYXNzaWduKHt9LCB7dHlwZTogTmd0Tm90aWZpY2F0aW9uVHlwZS5FcnJvcn0sIGRhdGEpKTtcclxuICAgIH1cclxuXHJcbiAgICBpbmZvKGRhdGEpIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbihPYmplY3QuYXNzaWduKHt9LCB7dHlwZTogTmd0Tm90aWZpY2F0aW9uVHlwZS5JbmZvfSwgZGF0YSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHdhcm4oZGF0YSkge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uKE9iamVjdC5hc3NpZ24oe30sIHt0eXBlOiBOZ3ROb3RpZmljYXRpb25UeXBlLldhcm5pbmd9LCBkYXRhKSk7XHJcbiAgICB9XHJcblxyXG4gICAgbm90aWZpY2F0aW9uKGRhdGEpIHtcclxuICAgICAgICBpZiAoZGF0YS50eXBlIHx8IGRhdGEudHlwZSA9PT0gMCkge1xyXG4gICAgICAgICAgICBkYXRhLnR5cGVDbGFzcyA9IHRoaXMuZ2V0SW5mbyhkYXRhLnR5cGUsICd0eXBlQ2xhc3MnKTtcclxuXHJcbiAgICAgICAgICAgIGlmICghZGF0YS5hc2lkZSkge1xyXG4gICAgICAgICAgICAgICAgZGF0YS5hc2lkZSA9IHRoaXMuZ2V0SW5mbyhkYXRhLnR5cGUsICdhc2lkZScpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IGNvbWJpbmVkT3B0aW9ucyA9IDxOZ3ROb3RpZmljYXRpb24+T2JqZWN0LmFzc2lnbih7fSwge3RpbWVvdXQ6IHRoaXMudGltZW91dH0sIGRhdGEpO1xyXG4gICAgICAgIHRoaXMuc3RhY2suc2hvdyh0aGlzLl9tb2R1bGVDRlIsIGNvbWJpbmVkT3B0aW9ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgY2xlYXIoKSB7XHJcbiAgICAgICAgLy8gY2xlYXIgYWxlcnRzXHJcbiAgICAgICAgdGhpcy5zdGFjay5jbGVhckFsbCgpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==