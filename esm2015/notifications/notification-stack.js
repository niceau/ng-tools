/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ApplicationRef, Inject, Injectable, Injector, RendererFactory2 } from '@angular/core';
import { NgtNotificationComponent } from './notification.component';
import { DOCUMENT } from '@angular/common';
import { isDefined } from '../util/util';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
export class NgtNotificationStack {
    /**
     * @param {?} _injector
     * @param {?} _appRef
     * @param {?} _rendererFactory
     * @param {?} _document
     */
    constructor(_injector, _appRef, _rendererFactory, _document) {
        this._injector = _injector;
        this._appRef = _appRef;
        this._rendererFactory = _rendererFactory;
        this._document = _document;
        this.containerEl = this._document.body;
        this._notificationRefs = [];
        this._notificationAttributes = ['type', 'message', 'timeout', 'typeClass', 'aside', 'title', '_ref'];
        this.initContainer();
    }
    /**
     * @return {?}
     */
    initContainer() {
        /** @type {?} */
        const renderer = this._rendererFactory.createRenderer(null, null);
        /** @type {?} */
        const containerEl = renderer.createElement('div');
        /** @type {?} */
        const wrapperEl = renderer.createElement('div');
        renderer.addClass(containerEl, 'notification-overlay-container');
        renderer.addClass(wrapperEl, 'notification-wrapper');
        renderer.appendChild(containerEl, wrapperEl);
        renderer.appendChild(this._document.body, containerEl);
        this.containerEl = wrapperEl;
    }
    /**
     * @param {?} moduleCFR
     * @param {?} options
     * @return {?}
     */
    show(moduleCFR, options) {
        /** @type {?} */
        const notificationCmptRef = this._attachNotificationComponent(moduleCFR, this.containerEl);
        this._registerNotificationRef(notificationCmptRef.instance);
        this._applyNotificationOptions(notificationCmptRef.instance, (/** @type {?} */ (Object.assign({}, { _ref: notificationCmptRef }, options))));
    }
    /**
     * @return {?}
     */
    clearAll() {
        this._notificationRefs.forEach((/**
         * @param {?} ngtNotificationRef
         * @return {?}
         */
        ngtNotificationRef => ngtNotificationRef.closeNotification()));
    }
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @return {?}
     */
    _attachNotificationComponent(moduleCFR, containerEl) {
        /** @type {?} */
        const notificationFactory = moduleCFR.resolveComponentFactory(NgtNotificationComponent);
        /** @type {?} */
        const notificationCmptRef = notificationFactory.create(this._injector);
        // Attach component to the appRef so that it's inside the ng component tree
        this._appRef.attachView(notificationCmptRef.hostView);
        // Append DOM element to the body
        containerEl.appendChild(notificationCmptRef.location.nativeElement);
        return notificationCmptRef;
    }
    /**
     * @private
     * @param {?} notificationInstance
     * @param {?} options
     * @return {?}
     */
    _applyNotificationOptions(notificationInstance, options) {
        this._notificationAttributes.forEach((/**
         * @param {?} optionName
         * @return {?}
         */
        (optionName) => {
            if (isDefined(options[optionName])) {
                notificationInstance[optionName] = options[optionName];
            }
        }));
    }
    /**
     * @private
     * @param {?} ngtNotificationComponent
     * @return {?}
     */
    _registerNotificationRef(ngtNotificationComponent) {
        /** @type {?} */
        const _unregisterNotificationRef = (/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const index = this._notificationRefs.indexOf(ngtNotificationComponent);
            if (index > -1) {
                this._notificationRefs.splice(index, 1);
            }
        });
        this._notificationRefs.push(ngtNotificationComponent);
        ngtNotificationComponent.result.then(_unregisterNotificationRef, _unregisterNotificationRef);
    }
}
NgtNotificationStack.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
NgtNotificationStack.ctorParameters = () => [
    { type: Injector },
    { type: ApplicationRef },
    { type: RendererFactory2 },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] }
];
/** @nocollapse */ NgtNotificationStack.ngInjectableDef = i0.defineInjectable({ factory: function NgtNotificationStack_Factory() { return new NgtNotificationStack(i0.inject(i0.INJECTOR), i0.inject(i0.ApplicationRef), i0.inject(i0.RendererFactory2), i0.inject(i1.DOCUMENT)); }, token: NgtNotificationStack, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtNotificationStack.prototype.containerEl;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._notificationRefs;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._notificationAttributes;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._injector;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._appRef;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._rendererFactory;
    /**
     * @type {?}
     * @private
     */
    NgtNotificationStack.prototype._document;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLXN0YWNrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbi1zdGFjay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUNILGNBQWMsRUFHZCxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFDakQsTUFBTSxlQUFlLENBQUM7QUFHdkIsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDcEUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxjQUFjLENBQUM7OztBQUd6QyxNQUFNLE9BQU8sb0JBQW9COzs7Ozs7O0lBSzdCLFlBQW9CLFNBQW1CLEVBQ25CLE9BQXVCLEVBQ3ZCLGdCQUFrQyxFQUNoQixTQUFjO1FBSGhDLGNBQVMsR0FBVCxTQUFTLENBQVU7UUFDbkIsWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFDdkIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNoQixjQUFTLEdBQVQsU0FBUyxDQUFLO1FBUHBELGdCQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7UUFDMUIsc0JBQWlCLEdBQStCLEVBQUUsQ0FBQztRQUNuRCw0QkFBdUIsR0FBRyxDQUFDLE1BQU0sRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBTXBHLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsYUFBYTs7Y0FDSCxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDOztjQUMzRCxXQUFXLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7O2NBQzNDLFNBQVMsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQztRQUMvQyxRQUFRLENBQUMsUUFBUSxDQUFDLFdBQVcsRUFBRSxnQ0FBZ0MsQ0FBQyxDQUFDO1FBQ2pFLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLHNCQUFzQixDQUFDLENBQUM7UUFDckQsUUFBUSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDN0MsUUFBUSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQztRQUN2RCxJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQztJQUNqQyxDQUFDOzs7Ozs7SUFFRCxJQUFJLENBQUMsU0FBbUMsRUFBRSxPQUF3Qjs7Y0FDeEQsbUJBQW1CLEdBQTJDLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUNsSSxJQUFJLENBQUMsd0JBQXdCLENBQUMsbUJBQW1CLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLHlCQUF5QixDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxtQkFBaUIsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsRUFBQyxJQUFJLEVBQUUsbUJBQW1CLEVBQUMsRUFBRSxPQUFPLENBQUMsRUFBQSxDQUFDLENBQUM7SUFDM0ksQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTzs7OztRQUFDLGtCQUFrQixDQUFDLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQyxpQkFBaUIsRUFBRSxFQUFDLENBQUM7SUFDakcsQ0FBQzs7Ozs7OztJQUVPLDRCQUE0QixDQUFDLFNBQW1DLEVBQUUsV0FBZ0I7O2NBRWhGLG1CQUFtQixHQUFHLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyx3QkFBd0IsQ0FBQzs7Y0FDakYsbUJBQW1CLEdBQUcsbUJBQW1CLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDdEUsMkVBQTJFO1FBQzNFLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3RELGlDQUFpQztRQUNqQyxXQUFXLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNwRSxPQUFPLG1CQUFtQixDQUFDO0lBQy9CLENBQUM7Ozs7Ozs7SUFFTyx5QkFBeUIsQ0FBQyxvQkFBcUMsRUFBRSxPQUFlO1FBQ3BGLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPOzs7O1FBQUMsQ0FBQyxVQUFrQixFQUFFLEVBQUU7WUFDeEQsSUFBSSxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEVBQUU7Z0JBQ2hDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUMxRDtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBRU8sd0JBQXdCLENBQUMsd0JBQWtEOztjQUN6RSwwQkFBMEI7OztRQUFHLEdBQUcsRUFBRTs7a0JBQzlCLEtBQUssR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLHdCQUF3QixDQUFDO1lBQ3RFLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUNaLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQzNDO1FBQ0wsQ0FBQyxDQUFBO1FBQ0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3RELHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztJQUNqRyxDQUFDOzs7WUE5REosVUFBVSxTQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQzs7OztZQVJSLFFBQVE7WUFINUIsY0FBYztZQUdnQixnQkFBZ0I7NENBaUJqQyxNQUFNLFNBQUMsUUFBUTs7Ozs7SUFQNUIsMkNBQWtDOzs7OztJQUNsQyxpREFBMkQ7Ozs7O0lBQzNELHVEQUF3Rzs7Ozs7SUFFNUYseUNBQTJCOzs7OztJQUMzQix1Q0FBK0I7Ozs7O0lBQy9CLGdEQUEwQzs7Ozs7SUFDMUMseUNBQXdDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICAgIEFwcGxpY2F0aW9uUmVmLFxyXG4gICAgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxyXG4gICAgQ29tcG9uZW50UmVmLFxyXG4gICAgSW5qZWN0LCBJbmplY3RhYmxlLCBJbmplY3RvciwgUmVuZGVyZXJGYWN0b3J5MlxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTmd0Tm90aWZpY2F0aW9uIH0gZnJvbSAnLi9ub3RpZmljYXRpb24ubW9kZWwnO1xyXG5pbXBvcnQgeyBOZ3ROb3RpZmljYXRpb25Db21wb25lbnQgfSBmcm9tICcuL25vdGlmaWNhdGlvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBET0NVTUVOVCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IGlzRGVmaW5lZCB9IGZyb20gJy4uL3V0aWwvdXRpbCc7XHJcblxyXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcclxuZXhwb3J0IGNsYXNzIE5ndE5vdGlmaWNhdGlvblN0YWNrIHtcclxuICAgIGNvbnRhaW5lckVsID0gdGhpcy5fZG9jdW1lbnQuYm9keTtcclxuICAgIHByaXZhdGUgX25vdGlmaWNhdGlvblJlZnM6IE5ndE5vdGlmaWNhdGlvbkNvbXBvbmVudFtdID0gW107XHJcbiAgICBwcml2YXRlIF9ub3RpZmljYXRpb25BdHRyaWJ1dGVzID0gWyd0eXBlJywgJ21lc3NhZ2UnLCAndGltZW91dCcsICd0eXBlQ2xhc3MnLCAnYXNpZGUnLCAndGl0bGUnLCAnX3JlZiddO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX2luamVjdG9yOiBJbmplY3RvcixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX2FwcFJlZjogQXBwbGljYXRpb25SZWYsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9yZW5kZXJlckZhY3Rvcnk6IFJlbmRlcmVyRmFjdG9yeTIsXHJcbiAgICAgICAgICAgICAgICBASW5qZWN0KERPQ1VNRU5UKSBwcml2YXRlIF9kb2N1bWVudDogYW55KSB7XHJcbiAgICAgICAgdGhpcy5pbml0Q29udGFpbmVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdENvbnRhaW5lcigpIHtcclxuICAgICAgICBjb25zdCByZW5kZXJlciA9IHRoaXMuX3JlbmRlcmVyRmFjdG9yeS5jcmVhdGVSZW5kZXJlcihudWxsLCBudWxsKTtcclxuICAgICAgICBjb25zdCBjb250YWluZXJFbCA9IHJlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgICAgIGNvbnN0IHdyYXBwZXJFbCA9IHJlbmRlcmVyLmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xyXG4gICAgICAgIHJlbmRlcmVyLmFkZENsYXNzKGNvbnRhaW5lckVsLCAnbm90aWZpY2F0aW9uLW92ZXJsYXktY29udGFpbmVyJyk7XHJcbiAgICAgICAgcmVuZGVyZXIuYWRkQ2xhc3Mod3JhcHBlckVsLCAnbm90aWZpY2F0aW9uLXdyYXBwZXInKTtcclxuICAgICAgICByZW5kZXJlci5hcHBlbmRDaGlsZChjb250YWluZXJFbCwgd3JhcHBlckVsKTtcclxuICAgICAgICByZW5kZXJlci5hcHBlbmRDaGlsZCh0aGlzLl9kb2N1bWVudC5ib2R5LCBjb250YWluZXJFbCk7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXJFbCA9IHdyYXBwZXJFbDtcclxuICAgIH1cclxuXHJcbiAgICBzaG93KG1vZHVsZUNGUjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBvcHRpb25zOiBOZ3ROb3RpZmljYXRpb24pIHtcclxuICAgICAgICBjb25zdCBub3RpZmljYXRpb25DbXB0UmVmOiBDb21wb25lbnRSZWY8Tmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50PiA9IHRoaXMuX2F0dGFjaE5vdGlmaWNhdGlvbkNvbXBvbmVudChtb2R1bGVDRlIsIHRoaXMuY29udGFpbmVyRWwpO1xyXG4gICAgICAgIHRoaXMuX3JlZ2lzdGVyTm90aWZpY2F0aW9uUmVmKG5vdGlmaWNhdGlvbkNtcHRSZWYuaW5zdGFuY2UpO1xyXG4gICAgICAgIHRoaXMuX2FwcGx5Tm90aWZpY2F0aW9uT3B0aW9ucyhub3RpZmljYXRpb25DbXB0UmVmLmluc3RhbmNlLCA8Tmd0Tm90aWZpY2F0aW9uPk9iamVjdC5hc3NpZ24oe30sIHtfcmVmOiBub3RpZmljYXRpb25DbXB0UmVmfSwgb3B0aW9ucykpO1xyXG4gICAgfVxyXG5cclxuICAgIGNsZWFyQWxsKCkge1xyXG4gICAgICAgIHRoaXMuX25vdGlmaWNhdGlvblJlZnMuZm9yRWFjaChuZ3ROb3RpZmljYXRpb25SZWYgPT4gbmd0Tm90aWZpY2F0aW9uUmVmLmNsb3NlTm90aWZpY2F0aW9uKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2F0dGFjaE5vdGlmaWNhdGlvbkNvbXBvbmVudChtb2R1bGVDRlI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29udGFpbmVyRWw6IGFueSk6XHJcbiAgICAgICAgQ29tcG9uZW50UmVmPE5ndE5vdGlmaWNhdGlvbkNvbXBvbmVudD4ge1xyXG4gICAgICAgIGNvbnN0IG5vdGlmaWNhdGlvbkZhY3RvcnkgPSBtb2R1bGVDRlIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkoTmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50KTtcclxuICAgICAgICBjb25zdCBub3RpZmljYXRpb25DbXB0UmVmID0gbm90aWZpY2F0aW9uRmFjdG9yeS5jcmVhdGUodGhpcy5faW5qZWN0b3IpO1xyXG4gICAgICAgIC8vIEF0dGFjaCBjb21wb25lbnQgdG8gdGhlIGFwcFJlZiBzbyB0aGF0IGl0J3MgaW5zaWRlIHRoZSBuZyBjb21wb25lbnQgdHJlZVxyXG4gICAgICAgIHRoaXMuX2FwcFJlZi5hdHRhY2hWaWV3KG5vdGlmaWNhdGlvbkNtcHRSZWYuaG9zdFZpZXcpO1xyXG4gICAgICAgIC8vIEFwcGVuZCBET00gZWxlbWVudCB0byB0aGUgYm9keVxyXG4gICAgICAgIGNvbnRhaW5lckVsLmFwcGVuZENoaWxkKG5vdGlmaWNhdGlvbkNtcHRSZWYubG9jYXRpb24ubmF0aXZlRWxlbWVudCk7XHJcbiAgICAgICAgcmV0dXJuIG5vdGlmaWNhdGlvbkNtcHRSZWY7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfYXBwbHlOb3RpZmljYXRpb25PcHRpb25zKG5vdGlmaWNhdGlvbkluc3RhbmNlOiBOZ3ROb3RpZmljYXRpb24sIG9wdGlvbnM6IE9iamVjdCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX25vdGlmaWNhdGlvbkF0dHJpYnV0ZXMuZm9yRWFjaCgob3B0aW9uTmFtZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChpc0RlZmluZWQob3B0aW9uc1tvcHRpb25OYW1lXSkpIHtcclxuICAgICAgICAgICAgICAgIG5vdGlmaWNhdGlvbkluc3RhbmNlW29wdGlvbk5hbWVdID0gb3B0aW9uc1tvcHRpb25OYW1lXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX3JlZ2lzdGVyTm90aWZpY2F0aW9uUmVmKG5ndE5vdGlmaWNhdGlvbkNvbXBvbmVudDogTmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50KSB7XHJcbiAgICAgICAgY29uc3QgX3VucmVnaXN0ZXJOb3RpZmljYXRpb25SZWYgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5fbm90aWZpY2F0aW9uUmVmcy5pbmRleE9mKG5ndE5vdGlmaWNhdGlvbkNvbXBvbmVudCk7XHJcbiAgICAgICAgICAgIGlmIChpbmRleCA+IC0xKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9ub3RpZmljYXRpb25SZWZzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuX25vdGlmaWNhdGlvblJlZnMucHVzaChuZ3ROb3RpZmljYXRpb25Db21wb25lbnQpO1xyXG4gICAgICAgIG5ndE5vdGlmaWNhdGlvbkNvbXBvbmVudC5yZXN1bHQudGhlbihfdW5yZWdpc3Rlck5vdGlmaWNhdGlvblJlZiwgX3VucmVnaXN0ZXJOb3RpZmljYXRpb25SZWYpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==