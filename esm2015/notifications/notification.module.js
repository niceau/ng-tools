/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtNotificationComponent } from './notification.component';
export { NgtNotificationService } from './notification.service';
export { NgtNotificationStack } from './notification-stack';
export { NgtNotificationComponent } from './notification.component';
export { NgtNotification } from './notification.model';
/** @type {?} */
const NGC_MODAL_DIRECTIVES = [
    NgtNotificationComponent
];
export class NgtNotificationModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtNotificationModule };
    }
}
NgtNotificationModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NGC_MODAL_DIRECTIVES],
                exports: [NGC_MODAL_DIRECTIVES],
                imports: [CommonModule],
                entryComponents: [NgtNotificationComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQXVCLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFJL0MsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFcEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDaEUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDNUQsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDcEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHNCQUFzQixDQUFDOztNQUVqRCxvQkFBb0IsR0FBRztJQUN6Qix3QkFBd0I7Q0FDM0I7QUFRRCxNQUFNLE9BQU8scUJBQXFCOzs7O0lBQzlCLE1BQU0sQ0FBQyxPQUFPO1FBQ1YsT0FBTyxFQUFDLFFBQVEsRUFBRSxxQkFBcUIsRUFBQyxDQUFDO0lBQzdDLENBQUM7OztZQVRKLFFBQVEsU0FBQztnQkFDTixZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztnQkFDcEMsT0FBTyxFQUFFLENBQUMsb0JBQW9CLENBQUM7Z0JBQy9CLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztnQkFDdkIsZUFBZSxFQUFFLENBQUMsd0JBQXdCLENBQUM7YUFDOUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgTmd0Tm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOZ3ROb3RpZmljYXRpb25TdGFjayB9IGZyb20gJy4vbm90aWZpY2F0aW9uLXN0YWNrJztcclxuaW1wb3J0IHsgTmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50IH0gZnJvbSAnLi9ub3RpZmljYXRpb24uY29tcG9uZW50JztcclxuXHJcbmV4cG9ydCB7IE5ndE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuZXhwb3J0IHsgTmd0Tm90aWZpY2F0aW9uU3RhY2sgfSBmcm9tICcuL25vdGlmaWNhdGlvbi1zdGFjayc7XHJcbmV4cG9ydCB7IE5ndE5vdGlmaWNhdGlvbkNvbXBvbmVudCB9IGZyb20gJy4vbm90aWZpY2F0aW9uLmNvbXBvbmVudCc7XHJcbmV4cG9ydCB7IE5ndE5vdGlmaWNhdGlvbiB9IGZyb20gJy4vbm90aWZpY2F0aW9uLm1vZGVsJztcclxuXHJcbmNvbnN0IE5HQ19NT0RBTF9ESVJFQ1RJVkVTID0gW1xyXG4gICAgTmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50XHJcbl07XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbTkdDX01PREFMX0RJUkVDVElWRVNdLFxyXG4gICAgZXhwb3J0czogW05HQ19NT0RBTF9ESVJFQ1RJVkVTXSxcclxuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBbTmd0Tm90aWZpY2F0aW9uQ29tcG9uZW50XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0Tm90aWZpY2F0aW9uTW9kdWxlIHtcclxuICAgIHN0YXRpYyBmb3JSb290KCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgICAgIHJldHVybiB7bmdNb2R1bGU6IE5ndE5vdGlmaWNhdGlvbk1vZHVsZX07XHJcbiAgICB9XHJcbn1cclxuIl19