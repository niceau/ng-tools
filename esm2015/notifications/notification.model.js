/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
export class NgtNotification {
}
if (false) {
    /** @type {?} */
    NgtNotification.prototype.type;
    /** @type {?} */
    NgtNotification.prototype.message;
    /** @type {?} */
    NgtNotification.prototype.timeout;
    /** @type {?} */
    NgtNotification.prototype.typeClass;
    /** @type {?} */
    NgtNotification.prototype.aside;
    /** @type {?} */
    NgtNotification.prototype.title;
    /** @type {?} */
    NgtNotification.prototype._ref;
}
/** @enum {number} */
const NgtNotificationType = {
    Success: 0,
    Error: 1,
    Info: 2,
    Warning: 3,
};
export { NgtNotificationType };
NgtNotificationType[NgtNotificationType.Success] = 'Success';
NgtNotificationType[NgtNotificationType.Error] = 'Error';
NgtNotificationType[NgtNotificationType.Info] = 'Info';
NgtNotificationType[NgtNotificationType.Warning] = 'Warning';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBRUEsTUFBTSxPQUFPLGVBQWU7Q0FRM0I7OztJQVBHLCtCQUEwQjs7SUFDMUIsa0NBQWdCOztJQUNoQixrQ0FBZ0I7O0lBQ2hCLG9DQUFrQjs7SUFDbEIsZ0NBQWU7O0lBQ2YsZ0NBQWM7O0lBQ2QsK0JBQW9DOzs7O0lBSXBDLFVBQU87SUFDUCxRQUFLO0lBQ0wsT0FBSTtJQUNKLFVBQU8iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnRSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmV4cG9ydCBjbGFzcyBOZ3ROb3RpZmljYXRpb24ge1xyXG4gICAgdHlwZTogTmd0Tm90aWZpY2F0aW9uVHlwZTtcclxuICAgIG1lc3NhZ2U6IHN0cmluZztcclxuICAgIHRpbWVvdXQ6IG51bWJlcjtcclxuICAgIHR5cGVDbGFzczogc3RyaW5nO1xyXG4gICAgYXNpZGU6IGJvb2xlYW47XHJcbiAgICB0aXRsZTogc3RyaW5nO1xyXG4gICAgX3JlZjogQ29tcG9uZW50UmVmPE5ndE5vdGlmaWNhdGlvbj47XHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIE5ndE5vdGlmaWNhdGlvblR5cGUge1xyXG4gICAgU3VjY2VzcyxcclxuICAgIEVycm9yLFxyXG4gICAgSW5mbyxcclxuICAgIFdhcm5pbmdcclxufVxyXG4iXX0=