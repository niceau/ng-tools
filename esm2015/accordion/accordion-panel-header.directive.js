/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, TemplateRef } from '@angular/core';
/**
 * A directive to wrap an accordion panel header to contain any HTML markup and a toggling button with `NgtAccordionPanelToggleDirective`
 */
export class NgtAccordionPanelHeaderDirective {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtAccordionPanelHeaderDirective.decorators = [
    { type: Directive, args: [{
                selector: 'ng-template[ngtAccordionPanelHeader]'
            },] }
];
/** @nocollapse */
NgtAccordionPanelHeaderDirective.ctorParameters = () => [
    { type: TemplateRef }
];
if (false) {
    /** @type {?} */
    NgtAccordionPanelHeaderDirective.prototype.templateRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3JkaW9uLXBhbmVsLWhlYWRlci5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImFjY29yZGlvbi9hY2NvcmRpb24tcGFuZWwtaGVhZGVyLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7QUFTdkQsTUFBTSxPQUFPLGdDQUFnQzs7OztJQUN6QyxZQUFtQixXQUE2QjtRQUE3QixnQkFBVyxHQUFYLFdBQVcsQ0FBa0I7SUFDaEQsQ0FBQzs7O1lBTkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxzQ0FBc0M7YUFDbkQ7Ozs7WUFQbUIsV0FBVzs7OztJQVVmLHVEQUFvQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgVGVtcGxhdGVSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbi8qKlxyXG4gKiBBIGRpcmVjdGl2ZSB0byB3cmFwIGFuIGFjY29yZGlvbiBwYW5lbCBoZWFkZXIgdG8gY29udGFpbiBhbnkgSFRNTCBtYXJrdXAgYW5kIGEgdG9nZ2xpbmcgYnV0dG9uIHdpdGggYE5ndEFjY29yZGlvblBhbmVsVG9nZ2xlRGlyZWN0aXZlYFxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ25nLXRlbXBsYXRlW25ndEFjY29yZGlvblBhbmVsSGVhZGVyXSdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBOZ3RBY2NvcmRpb25QYW5lbEhlYWRlckRpcmVjdGl2ZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdGVtcGxhdGVSZWY6IFRlbXBsYXRlUmVmPGFueT4pIHtcclxuICAgIH1cclxufVxyXG4iXX0=