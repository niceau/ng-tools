/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtAccordion component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the accordions used in the application.
 */
export class NgtAccordionConfig {
    constructor() {
        this.closeOthers = true;
    }
}
NgtAccordionConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtAccordionConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtAccordionConfig_Factory() { return new NgtAccordionConfig(); }, token: NgtAccordionConfig, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtAccordionConfig.prototype.closeOthers;
    /** @type {?} */
    NgtAccordionConfig.prototype.type;
    /** @type {?} */
    NgtAccordionConfig.prototype.bg;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3JkaW9uLWNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiYWNjb3JkaW9uL2FjY29yZGlvbi1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFRM0MsTUFBTSxPQUFPLGtCQUFrQjtJQUQvQjtRQUVJLGdCQUFXLEdBQUcsSUFBSSxDQUFDO0tBR3RCOzs7WUFMQSxVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7OztJQUU1Qix5Q0FBbUI7O0lBQ25CLGtDQUFhOztJQUNiLGdDQUFXIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuLyoqXHJcbiAqIENvbmZpZ3VyYXRpb24gc2VydmljZSBmb3IgdGhlIE5ndEFjY29yZGlvbiBjb21wb25lbnQuXHJcbiAqIFlvdSBjYW4gaW5qZWN0IHRoaXMgc2VydmljZSwgdHlwaWNhbGx5IGluIHlvdXIgcm9vdCBjb21wb25lbnQsIGFuZCBjdXN0b21pemUgdGhlIHZhbHVlcyBvZiBpdHMgcHJvcGVydGllcyBpblxyXG4gKiBvcmRlciB0byBwcm92aWRlIGRlZmF1bHQgdmFsdWVzIGZvciBhbGwgdGhlIGFjY29yZGlvbnMgdXNlZCBpbiB0aGUgYXBwbGljYXRpb24uXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcclxuZXhwb3J0IGNsYXNzIE5ndEFjY29yZGlvbkNvbmZpZyB7XHJcbiAgICBjbG9zZU90aGVycyA9IHRydWU7XHJcbiAgICB0eXBlOiBzdHJpbmc7XHJcbiAgICBiZzogc3RyaW5nO1xyXG59XHJcbiJdfQ==