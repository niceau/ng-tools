/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ContentChildren, Directive, Input, QueryList, } from '@angular/core';
import { NgtAccordionPanelTitleDirective } from './accordion-panel-title.directive';
import { NgtAccordionPanelHeaderDirective } from './accordion-panel-header.directive';
import { NgtAccordionPanelContentDirective } from './accordion-panel-content.directive';
import { NgtAccordionService } from './accordion.service';
export class NgtAccordionPanelDirective {
    /**
     * @param {?} $accordionService
     */
    constructor($accordionService) {
        this.$accordionService = $accordionService;
        this._isOpen = false;
        this.disabled = false;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set isOpen(value) {
        this._isOpen = value;
    }
    /**
     * @return {?}
     */
    get isOpen() {
        return this._isOpen;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.id) {
            this.id = `accordion-panel-${this.$accordionService.nextId++}`;
        }
    }
    /**
     * @return {?}
     */
    ngAfterContentChecked() {
        // We are using @ContentChildren instead of @ContentChild as in the Angular version being used
        // only @ContentChildren allows us to specify the {descendants: false} option.
        // Without {descendants: false} we are hitting bugs described in:
        // https://github.com/ng-bootstrap/ng-bootstrap/issues/2240
        this.titleTpl = this.titleTpls.first;
        this.headerTpl = this.headerTpls.first;
        this.contentTpl = this.contentTpls.first;
    }
}
NgtAccordionPanelDirective.decorators = [
    { type: Directive, args: [{
                selector: 'ngt-accordion-panel',
            },] }
];
/** @nocollapse */
NgtAccordionPanelDirective.ctorParameters = () => [
    { type: NgtAccordionService }
];
NgtAccordionPanelDirective.propDecorators = {
    id: [{ type: Input }],
    title: [{ type: Input }],
    bg: [{ type: Input }],
    disabled: [{ type: Input }],
    isOpen: [{ type: Input }],
    titleTpls: [{ type: ContentChildren, args: [NgtAccordionPanelTitleDirective, { descendants: false },] }],
    headerTpls: [{ type: ContentChildren, args: [NgtAccordionPanelHeaderDirective, { descendants: false },] }],
    contentTpls: [{ type: ContentChildren, args: [NgtAccordionPanelContentDirective, { descendants: false },] }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtAccordionPanelDirective.prototype._isOpen;
    /** @type {?} */
    NgtAccordionPanelDirective.prototype.id;
    /** @type {?} */
    NgtAccordionPanelDirective.prototype.title;
    /** @type {?} */
    NgtAccordionPanelDirective.prototype.bg;
    /** @type {?} */
    NgtAccordionPanelDirective.prototype.disabled;
    /** @type {?} */
    NgtAccordionPanelDirective.prototype.titleTpl;
    /** @type {?} */
    NgtAccordionPanelDirective.prototype.headerTpl;
    /** @type {?} */
    NgtAccordionPanelDirective.prototype.contentTpl;
    /** @type {?} */
    NgtAccordionPanelDirective.prototype.titleTpls;
    /** @type {?} */
    NgtAccordionPanelDirective.prototype.headerTpls;
    /** @type {?} */
    NgtAccordionPanelDirective.prototype.contentTpls;
    /**
     * @type {?}
     * @private
     */
    NgtAccordionPanelDirective.prototype.$accordionService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3JkaW9uLXBhbmVsLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsiYWNjb3JkaW9uL2FjY29yZGlvbi1wYW5lbC5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFFSCxlQUFlLEVBQUUsU0FBUyxFQUMxQixLQUFLLEVBQ0wsU0FBUyxHQUNaLE1BQU0sZUFBZSxDQUFDO0FBRXZCLE9BQU8sRUFBRSwrQkFBK0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxnQ0FBZ0MsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxpQ0FBaUMsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3hGLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBTTFELE1BQU0sT0FBTywwQkFBMEI7Ozs7SUErQm5DLFlBQW9CLGlCQUFzQztRQUF0QyxzQkFBaUIsR0FBakIsaUJBQWlCLENBQXFCO1FBOUJsRCxZQUFPLEdBQUcsS0FBSyxDQUFDO1FBV3hCLGFBQVEsR0FBRyxLQUFLLENBQUM7SUFvQmpCLENBQUM7Ozs7O0lBbEJELElBQ0ksTUFBTSxDQUFDLEtBQWM7UUFDckIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQzs7OztJQUVELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDOzs7O0lBYUQsUUFBUTtRQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ1YsSUFBSSxDQUFDLEVBQUUsR0FBRyxtQkFBbUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUM7U0FDbEU7SUFDTCxDQUFDOzs7O0lBRUQscUJBQXFCO1FBQ2pCLDhGQUE4RjtRQUM5Riw4RUFBOEU7UUFDOUUsaUVBQWlFO1FBQ2pFLDJEQUEyRDtRQUMzRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUM7UUFDdkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztJQUM3QyxDQUFDOzs7WUFwREosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxxQkFBcUI7YUFDbEM7Ozs7WUFKUSxtQkFBbUI7OztpQkFRdkIsS0FBSztvQkFHTCxLQUFLO2lCQUdMLEtBQUs7dUJBR0wsS0FBSztxQkFHTCxLQUFLO3dCQWFMLGVBQWUsU0FBQywrQkFBK0IsRUFBRSxFQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUM7eUJBQ3JFLGVBQWUsU0FBQyxnQ0FBZ0MsRUFBRSxFQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUM7MEJBQ3RFLGVBQWUsU0FBQyxpQ0FBaUMsRUFBRSxFQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUM7Ozs7Ozs7SUE1QnhFLDZDQUF3Qjs7SUFDeEIsd0NBQ1c7O0lBRVgsMkNBQ2M7O0lBRWQsd0NBQ1c7O0lBRVgsOENBQ2lCOztJQVdqQiw4Q0FBaUQ7O0lBQ2pELCtDQUFtRDs7SUFDbkQsZ0RBQXFEOztJQUVyRCwrQ0FBOEg7O0lBQzlILGdEQUFpSTs7SUFDakksaURBQW9JOzs7OztJQUV4SCx1REFBOEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgQWZ0ZXJDb250ZW50Q2hlY2tlZCxcclxuICAgIENvbnRlbnRDaGlsZHJlbiwgRGlyZWN0aXZlLFxyXG4gICAgSW5wdXQsIE9uSW5pdCxcclxuICAgIFF1ZXJ5TGlzdCxcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IE5ndEFjY29yZGlvblBhbmVsVGl0bGVEaXJlY3RpdmUgfSBmcm9tICcuL2FjY29yZGlvbi1wYW5lbC10aXRsZS5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBOZ3RBY2NvcmRpb25QYW5lbEhlYWRlckRpcmVjdGl2ZSB9IGZyb20gJy4vYWNjb3JkaW9uLXBhbmVsLWhlYWRlci5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBOZ3RBY2NvcmRpb25QYW5lbENvbnRlbnREaXJlY3RpdmUgfSBmcm9tICcuL2FjY29yZGlvbi1wYW5lbC1jb250ZW50LmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IE5ndEFjY29yZGlvblNlcnZpY2UgfSBmcm9tICcuL2FjY29yZGlvbi5zZXJ2aWNlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICduZ3QtYWNjb3JkaW9uLXBhbmVsJyxcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBOZ3RBY2NvcmRpb25QYW5lbERpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJDb250ZW50Q2hlY2tlZCB7XHJcbiAgICBwcml2YXRlIF9pc09wZW4gPSBmYWxzZTtcclxuICAgIEBJbnB1dCgpXHJcbiAgICBpZDogc3RyaW5nO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICB0aXRsZTogc3RyaW5nO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBiZzogc3RyaW5nO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBkaXNhYmxlZCA9IGZhbHNlO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzZXQgaXNPcGVuKHZhbHVlOiBib29sZWFuKSB7XHJcbiAgICAgICAgdGhpcy5faXNPcGVuID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGlzT3BlbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5faXNPcGVuO1xyXG4gICAgfVxyXG5cclxuICAgIHRpdGxlVHBsOiBOZ3RBY2NvcmRpb25QYW5lbFRpdGxlRGlyZWN0aXZlIHwgbnVsbDtcclxuICAgIGhlYWRlclRwbDogTmd0QWNjb3JkaW9uUGFuZWxIZWFkZXJEaXJlY3RpdmUgfCBudWxsO1xyXG4gICAgY29udGVudFRwbDogTmd0QWNjb3JkaW9uUGFuZWxDb250ZW50RGlyZWN0aXZlIHwgbnVsbDtcclxuXHJcbiAgICBAQ29udGVudENoaWxkcmVuKE5ndEFjY29yZGlvblBhbmVsVGl0bGVEaXJlY3RpdmUsIHtkZXNjZW5kYW50czogZmFsc2V9KSB0aXRsZVRwbHM6IFF1ZXJ5TGlzdDxOZ3RBY2NvcmRpb25QYW5lbFRpdGxlRGlyZWN0aXZlPjtcclxuICAgIEBDb250ZW50Q2hpbGRyZW4oTmd0QWNjb3JkaW9uUGFuZWxIZWFkZXJEaXJlY3RpdmUsIHtkZXNjZW5kYW50czogZmFsc2V9KSBoZWFkZXJUcGxzOiBRdWVyeUxpc3Q8Tmd0QWNjb3JkaW9uUGFuZWxIZWFkZXJEaXJlY3RpdmU+O1xyXG4gICAgQENvbnRlbnRDaGlsZHJlbihOZ3RBY2NvcmRpb25QYW5lbENvbnRlbnREaXJlY3RpdmUsIHtkZXNjZW5kYW50czogZmFsc2V9KSBjb250ZW50VHBsczogUXVlcnlMaXN0PE5ndEFjY29yZGlvblBhbmVsQ29udGVudERpcmVjdGl2ZT47XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSAkYWNjb3JkaW9uU2VydmljZTogTmd0QWNjb3JkaW9uU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICghdGhpcy5pZCkge1xyXG4gICAgICAgICAgICB0aGlzLmlkID0gYGFjY29yZGlvbi1wYW5lbC0ke3RoaXMuJGFjY29yZGlvblNlcnZpY2UubmV4dElkKyt9YDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdBZnRlckNvbnRlbnRDaGVja2VkKCkge1xyXG4gICAgICAgIC8vIFdlIGFyZSB1c2luZyBAQ29udGVudENoaWxkcmVuIGluc3RlYWQgb2YgQENvbnRlbnRDaGlsZCBhcyBpbiB0aGUgQW5ndWxhciB2ZXJzaW9uIGJlaW5nIHVzZWRcclxuICAgICAgICAvLyBvbmx5IEBDb250ZW50Q2hpbGRyZW4gYWxsb3dzIHVzIHRvIHNwZWNpZnkgdGhlIHtkZXNjZW5kYW50czogZmFsc2V9IG9wdGlvbi5cclxuICAgICAgICAvLyBXaXRob3V0IHtkZXNjZW5kYW50czogZmFsc2V9IHdlIGFyZSBoaXR0aW5nIGJ1Z3MgZGVzY3JpYmVkIGluOlxyXG4gICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9uZy1ib290c3RyYXAvbmctYm9vdHN0cmFwL2lzc3Vlcy8yMjQwXHJcbiAgICAgICAgdGhpcy50aXRsZVRwbCA9IHRoaXMudGl0bGVUcGxzLmZpcnN0O1xyXG4gICAgICAgIHRoaXMuaGVhZGVyVHBsID0gdGhpcy5oZWFkZXJUcGxzLmZpcnN0O1xyXG4gICAgICAgIHRoaXMuY29udGVudFRwbCA9IHRoaXMuY29udGVudFRwbHMuZmlyc3Q7XHJcbiAgICB9XHJcbn1cclxuIl19