/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, forwardRef, Host, HostListener, Inject, Input, Optional } from '@angular/core';
import { NgtAccordionPanelDirective } from './accordion-panel.directive';
import { NgtAccordionComponent } from './accordion.component';
/**
 * A directive to put on a button that toggles panel opening and closing.
 * To be used inside the `NgtAccordionPanelHeaderDirective`
 */
export class NgtAccordionPanelToggleDirective {
    /**
     * @param {?} accordion
     * @param {?} panel
     */
    constructor(accordion, panel) {
        this.accordion = accordion;
        this.panel = panel;
    }
    /**
     * @param {?} panel
     * @return {?}
     */
    set ngtAccordionPanelToggle(panel) {
        if (panel) {
            this.panel = panel;
        }
    }
    /**
     * @return {?}
     */
    onClick() {
        this.accordion.toggle(this.panel.id);
    }
}
NgtAccordionPanelToggleDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtAccordionPanelToggle]',
                host: {
                    '[class.disabled]': 'panel.disabled',
                    '[class.collapsed]': '!panel.isOpen',
                    '[attr.aria-expanded]': 'panel.isOpen',
                    '[attr.aria-controls]': 'panel.id',
                }
            },] }
];
/** @nocollapse */
NgtAccordionPanelToggleDirective.ctorParameters = () => [
    { type: NgtAccordionComponent, decorators: [{ type: Inject, args: [forwardRef((/**
                     * @return {?}
                     */
                    () => NgtAccordionComponent)),] }] },
    { type: NgtAccordionPanelDirective, decorators: [{ type: Optional }, { type: Host }, { type: Inject, args: [forwardRef((/**
                     * @return {?}
                     */
                    () => NgtAccordionPanelDirective)),] }] }
];
NgtAccordionPanelToggleDirective.propDecorators = {
    ngtAccordionPanelToggle: [{ type: Input }],
    onClick: [{ type: HostListener, args: ['click',] }]
};
if (false) {
    /** @type {?} */
    NgtAccordionPanelToggleDirective.prototype.accordion;
    /** @type {?} */
    NgtAccordionPanelToggleDirective.prototype.panel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3JkaW9uLXBhbmVsLXRvZ2dsZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbImFjY29yZGlvbi9hY2NvcmRpb24tcGFuZWwtdG9nZ2xlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRyxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUN6RSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQzs7Ozs7QUFlOUQsTUFBTSxPQUFPLGdDQUFnQzs7Ozs7SUFZekMsWUFDNEQsU0FBZ0MsRUFDUCxLQUFpQztRQUQxRCxjQUFTLEdBQVQsU0FBUyxDQUF1QjtRQUNQLFVBQUssR0FBTCxLQUFLLENBQTRCO0lBQUcsQ0FBQzs7Ozs7SUFiMUgsSUFDSSx1QkFBdUIsQ0FBQyxLQUFpQztRQUN6RCxJQUFJLEtBQUssRUFBRTtZQUNQLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1NBQ3RCO0lBQ0wsQ0FBQzs7OztJQUVELE9BQU87UUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7OztZQW5CSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLDJCQUEyQjtnQkFDckMsSUFBSSxFQUFFO29CQUNGLGtCQUFrQixFQUFFLGdCQUFnQjtvQkFDcEMsbUJBQW1CLEVBQUUsZUFBZTtvQkFDcEMsc0JBQXNCLEVBQUUsY0FBYztvQkFDdEMsc0JBQXNCLEVBQUUsVUFBVTtpQkFDckM7YUFDSjs7OztZQWRRLHFCQUFxQix1QkE0QnJCLE1BQU0sU0FBQyxVQUFVOzs7b0JBQUMsR0FBRyxFQUFFLENBQUMscUJBQXFCLEVBQUM7WUE3QjlDLDBCQUEwQix1QkE4QjFCLFFBQVEsWUFBSSxJQUFJLFlBQUksTUFBTSxTQUFDLFVBQVU7OztvQkFBQyxHQUFHLEVBQUUsQ0FBQywwQkFBMEIsRUFBQzs7O3NDQWIzRSxLQUFLO3NCQU1MLFlBQVksU0FBQyxPQUFPOzs7O0lBTWpCLHFEQUF3Rjs7SUFDeEYsaURBQWtIIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBmb3J3YXJkUmVmLCBIb3N0LCBIb3N0TGlzdGVuZXIsIEluamVjdCwgSW5wdXQsIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5ndEFjY29yZGlvblBhbmVsRGlyZWN0aXZlIH0gZnJvbSAnLi9hY2NvcmRpb24tcGFuZWwuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgTmd0QWNjb3JkaW9uQ29tcG9uZW50IH0gZnJvbSAnLi9hY2NvcmRpb24uY29tcG9uZW50JztcclxuXHJcbi8qKlxyXG4gKiBBIGRpcmVjdGl2ZSB0byBwdXQgb24gYSBidXR0b24gdGhhdCB0b2dnbGVzIHBhbmVsIG9wZW5pbmcgYW5kIGNsb3NpbmcuXHJcbiAqIFRvIGJlIHVzZWQgaW5zaWRlIHRoZSBgTmd0QWNjb3JkaW9uUGFuZWxIZWFkZXJEaXJlY3RpdmVgXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndEFjY29yZGlvblBhbmVsVG9nZ2xlXScsXHJcbiAgICBob3N0OiB7XHJcbiAgICAgICAgJ1tjbGFzcy5kaXNhYmxlZF0nOiAncGFuZWwuZGlzYWJsZWQnLFxyXG4gICAgICAgICdbY2xhc3MuY29sbGFwc2VkXSc6ICchcGFuZWwuaXNPcGVuJyxcclxuICAgICAgICAnW2F0dHIuYXJpYS1leHBhbmRlZF0nOiAncGFuZWwuaXNPcGVuJyxcclxuICAgICAgICAnW2F0dHIuYXJpYS1jb250cm9sc10nOiAncGFuZWwuaWQnLFxyXG4gICAgfVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0QWNjb3JkaW9uUGFuZWxUb2dnbGVEaXJlY3RpdmUge1xyXG4gICAgQElucHV0KClcclxuICAgIHNldCBuZ3RBY2NvcmRpb25QYW5lbFRvZ2dsZShwYW5lbDogTmd0QWNjb3JkaW9uUGFuZWxEaXJlY3RpdmUpIHtcclxuICAgICAgICBpZiAocGFuZWwpIHtcclxuICAgICAgICAgICAgdGhpcy5wYW5lbCA9IHBhbmVsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJylcclxuICAgIG9uQ2xpY2soKSB7XHJcbiAgICAgICAgdGhpcy5hY2NvcmRpb24udG9nZ2xlKHRoaXMucGFuZWwuaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIEBJbmplY3QoZm9yd2FyZFJlZigoKSA9PiBOZ3RBY2NvcmRpb25Db21wb25lbnQpKSBwdWJsaWMgYWNjb3JkaW9uOiBOZ3RBY2NvcmRpb25Db21wb25lbnQsXHJcbiAgICAgICAgQE9wdGlvbmFsKCkgQEhvc3QoKSBASW5qZWN0KGZvcndhcmRSZWYoKCkgPT4gTmd0QWNjb3JkaW9uUGFuZWxEaXJlY3RpdmUpKSBwdWJsaWMgcGFuZWw6IE5ndEFjY29yZGlvblBhbmVsRGlyZWN0aXZlKSB7fVxyXG59XHJcbiJdfQ==