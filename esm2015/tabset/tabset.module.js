/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtTabset, NgtTab, NgtTabContent, NgtTabTitle } from './tabset';
export { NgtTabset, NgtTab, NgtTabContent, NgtTabTitle } from './tabset';
export { NgtTabsetConfig } from './tabset-config';
/** @type {?} */
const NGT_TABSET_DIRECTIVES = [NgtTabset, NgtTab, NgtTabContent, NgtTabTitle];
export class NgtTabsetModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtTabsetModule };
    }
}
NgtTabsetModule.decorators = [
    { type: NgModule, args: [{
                declarations: NGT_TABSET_DIRECTIVES,
                exports: NGT_TABSET_DIRECTIVES,
                imports: [CommonModule]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFic2V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsidGFic2V0L3RhYnNldC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLE1BQU0sVUFBVSxDQUFDO0FBRXpFLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQXFCLE1BQU0sVUFBVSxDQUFDO0FBQzVGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQzs7TUFFNUMscUJBQXFCLEdBQUcsQ0FBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLGFBQWEsRUFBRSxXQUFXLENBQUM7QUFPN0UsTUFBTSxPQUFPLGVBQWU7Ozs7SUFDeEIsTUFBTSxDQUFDLE9BQU87UUFDVixPQUFPLEVBQUMsUUFBUSxFQUFFLGVBQWUsRUFBQyxDQUFDO0lBQ3ZDLENBQUM7OztZQVJKLFFBQVEsU0FBQztnQkFDTixZQUFZLEVBQUUscUJBQXFCO2dCQUNuQyxPQUFPLEVBQUUscUJBQXFCO2dCQUM5QixPQUFPLEVBQUUsQ0FBQyxZQUFZLENBQUM7YUFDMUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgTmd0VGFic2V0LCBOZ3RUYWIsIE5ndFRhYkNvbnRlbnQsIE5ndFRhYlRpdGxlIH0gZnJvbSAnLi90YWJzZXQnO1xyXG5cclxuZXhwb3J0IHsgTmd0VGFic2V0LCBOZ3RUYWIsIE5ndFRhYkNvbnRlbnQsIE5ndFRhYlRpdGxlLCBOZ3RUYWJDaGFuZ2VFdmVudCB9IGZyb20gJy4vdGFic2V0JztcclxuZXhwb3J0IHsgTmd0VGFic2V0Q29uZmlnIH0gZnJvbSAnLi90YWJzZXQtY29uZmlnJztcclxuXHJcbmNvbnN0IE5HVF9UQUJTRVRfRElSRUNUSVZFUyA9IFtOZ3RUYWJzZXQsIE5ndFRhYiwgTmd0VGFiQ29udGVudCwgTmd0VGFiVGl0bGVdO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogTkdUX1RBQlNFVF9ESVJFQ1RJVkVTLFxyXG4gICAgZXhwb3J0czogTkdUX1RBQlNFVF9ESVJFQ1RJVkVTLFxyXG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndFRhYnNldE1vZHVsZSB7XHJcbiAgICBzdGF0aWMgZm9yUm9vdCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge25nTW9kdWxlOiBOZ3RUYWJzZXRNb2R1bGV9O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==