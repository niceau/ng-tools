/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ContentChildren, QueryList, Directive, TemplateRef, Output, EventEmitter } from '@angular/core';
import { NgtTabsetConfig } from './tabset-config';
/** @type {?} */
let nextId = 0;
/**
 * This directive should be used to wrap tab titles that need to contain HTML markup or other directives.
 */
export class NgtTabTitle {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtTabTitle.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtTabTitle]' },] }
];
/** @nocollapse */
NgtTabTitle.ctorParameters = () => [
    { type: TemplateRef }
];
if (false) {
    /** @type {?} */
    NgtTabTitle.prototype.templateRef;
}
/**
 * This directive must be used to wrap content to be displayed in a tab.
 */
export class NgtTabContent {
    /**
     * @param {?} templateRef
     */
    constructor(templateRef) {
        this.templateRef = templateRef;
    }
}
NgtTabContent.decorators = [
    { type: Directive, args: [{ selector: 'ng-template[ngtTabContent]' },] }
];
/** @nocollapse */
NgtTabContent.ctorParameters = () => [
    { type: TemplateRef }
];
if (false) {
    /** @type {?} */
    NgtTabContent.prototype.templateRef;
}
/**
 * A directive representing an individual tab.
 */
export class NgtTab {
    constructor() {
        /**
         * Unique tab identifier. Must be unique for the entire document for proper accessibility support.
         */
        this.id = `ngt-tab-${nextId++}`;
        /**
         * Allows toggling disabled state of a given state. Disabled tabs can't be selected.
         */
        this.disabled = false;
    }
    /**
     * @return {?}
     */
    ngAfterContentChecked() {
        // We are using @ContentChildren instead of @ContentChild as in the Angular version being used
        // only @ContentChildren allows us to specify the {descendants: false} option.
        // Without {descendants: false} we are hitting bugs described in:
        // https://github.com/ng-bootstrap/ng-bootstrap/issues/2240
        this.titleTpl = this.titleTpls.first;
        this.contentTpl = this.contentTpls.first;
    }
}
NgtTab.decorators = [
    { type: Directive, args: [{ selector: 'ngt-tab' },] }
];
NgtTab.propDecorators = {
    id: [{ type: Input }],
    title: [{ type: Input }],
    disabled: [{ type: Input }],
    titleTpls: [{ type: ContentChildren, args: [NgtTabTitle, { descendants: false },] }],
    contentTpls: [{ type: ContentChildren, args: [NgtTabContent, { descendants: false },] }]
};
if (false) {
    /**
     * Unique tab identifier. Must be unique for the entire document for proper accessibility support.
     * @type {?}
     */
    NgtTab.prototype.id;
    /**
     * Simple (string only) title. Use the "NgtTabTitle" directive for more complex use-cases.
     * @type {?}
     */
    NgtTab.prototype.title;
    /**
     * Allows toggling disabled state of a given state. Disabled tabs can't be selected.
     * @type {?}
     */
    NgtTab.prototype.disabled;
    /** @type {?} */
    NgtTab.prototype.titleTpl;
    /** @type {?} */
    NgtTab.prototype.contentTpl;
    /** @type {?} */
    NgtTab.prototype.titleTpls;
    /** @type {?} */
    NgtTab.prototype.contentTpls;
}
/**
 * The payload of the change event fired right before the tab change
 * @record
 */
export function NgtTabChangeEvent() { }
if (false) {
    /**
     * Id of the currently active tab
     * @type {?}
     */
    NgtTabChangeEvent.prototype.activeId;
    /**
     * Id of the newly selected tab
     * @type {?}
     */
    NgtTabChangeEvent.prototype.nextId;
    /**
     * Function that will prevent tab switch if called
     * @type {?}
     */
    NgtTabChangeEvent.prototype.preventDefault;
}
/**
 * A component that makes it easy to create tabbed interface.
 */
export class NgtTabset {
    /**
     * @param {?} config
     */
    constructor(config) {
        /**
         * Whether the closed tabs should be hidden without destroying them
         */
        this.destroyOnHide = true;
        /**
         * A tab change event fired right before the tab selection happens. See NgtTabChangeEvent for payload details
         */
        this.tabChange = new EventEmitter();
        this.type = config.type;
        this.justify = config.justify;
        this.orientation = config.orientation;
    }
    /**
     * The horizontal alignment of the nav with flexbox utilities. Can be one of 'start', 'center', 'end', 'fill' or
     * 'justified'
     * The default value is 'start'.
     * @param {?} className
     * @return {?}
     */
    set justify(className) {
        if (className === 'fill' || className === 'justified') {
            this.justifyClass = `nav-${className}`;
        }
        else {
            this.justifyClass = `justify-content-${className}`;
        }
    }
    /**
     * Selects the tab with the given id and shows its associated pane.
     * Any other tab that was previously selected becomes unselected and its associated pane is hidden.
     * @param {?} tabId
     * @return {?}
     */
    select(tabId) {
        /** @type {?} */
        let selectedTab = this._getTabById(tabId);
        if (selectedTab && !selectedTab.disabled && this.activeId !== selectedTab.id) {
            /** @type {?} */
            let defaultPrevented = false;
            this.tabChange.emit({
                activeId: this.activeId, nextId: selectedTab.id, preventDefault: (/**
                 * @return {?}
                 */
                () => {
                    defaultPrevented = true;
                })
            });
            if (!defaultPrevented) {
                this.activeId = selectedTab.id;
            }
        }
    }
    /**
     * @return {?}
     */
    ngAfterContentChecked() {
        // auto-correct activeId that might have been set incorrectly as input
        /** @type {?} */
        let activeTab = this._getTabById(this.activeId);
        this.activeId = activeTab ? activeTab.id : (this.tabs.length ? this.tabs.first.id : null);
    }
    /**
     * @private
     * @param {?} id
     * @return {?}
     */
    _getTabById(id) {
        /** @type {?} */
        let tabsWithId = this.tabs.filter((/**
         * @param {?} tab
         * @return {?}
         */
        tab => tab.id === id));
        return tabsWithId.length ? tabsWithId[0] : null;
    }
}
NgtTabset.decorators = [
    { type: Component, args: [{
                selector: 'ngt-tabset',
                exportAs: 'ngtTabset',
                template: `
        <ul [class]="'nav nav-' + type + (orientation == 'horizontal'?  ' ' + justifyClass : ' flex-column')" role="tablist">
            <li class="nav-item" *ngFor="let tab of tabs">
                <a [id]="tab.id" class="nav-link" [class.active]="tab.id === activeId" [class.disabled]="tab.disabled"
                   href (click)="select(tab.id); $event.preventDefault()" role="tab" [attr.tabindex]="(tab.disabled ? '-1': undefined)"
                   [attr.aria-controls]="(!destroyOnHide || tab.id === activeId ? tab.id + '-panel' : null)"
                   [attr.aria-expanded]="tab.id === activeId" [attr.aria-disabled]="tab.disabled">
                    {{tab.title}}
                    <ng-template [ngTemplateOutlet]="tab.titleTpl?.templateRef"></ng-template>
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <ng-template ngFor let-tab [ngForOf]="tabs">
                <div
                    class="tab-pane {{tab.id === activeId ? 'active' : null}}"
                    *ngIf="!destroyOnHide || tab.id === activeId"
                    role="tabpanel"
                    [attr.aria-labelledby]="tab.id" id="{{tab.id}}-panel"
                    [attr.aria-expanded]="tab.id === activeId">
                    <ng-template [ngTemplateOutlet]="tab.contentTpl?.templateRef"></ng-template>
                </div>
            </ng-template>
        </div>
    `
            }] }
];
/** @nocollapse */
NgtTabset.ctorParameters = () => [
    { type: NgtTabsetConfig }
];
NgtTabset.propDecorators = {
    tabs: [{ type: ContentChildren, args: [NgtTab,] }],
    activeId: [{ type: Input }],
    destroyOnHide: [{ type: Input }],
    justify: [{ type: Input }],
    orientation: [{ type: Input }],
    type: [{ type: Input }],
    tabChange: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    NgtTabset.prototype.justifyClass;
    /** @type {?} */
    NgtTabset.prototype.tabs;
    /**
     * An identifier of an initially selected (active) tab. Use the "select" method to switch a tab programmatically.
     * @type {?}
     */
    NgtTabset.prototype.activeId;
    /**
     * Whether the closed tabs should be hidden without destroying them
     * @type {?}
     */
    NgtTabset.prototype.destroyOnHide;
    /**
     * The orientation of the nav (horizontal or vertical).
     * The default value is 'horizontal'.
     * @type {?}
     */
    NgtTabset.prototype.orientation;
    /**
     * Type of navigation to be used for tabs. Can be one of Bootstrap defined types ('tabs' or 'pills').
     * Since 3.0.0 can also be an arbitrary string (for custom themes).
     * @type {?}
     */
    NgtTabset.prototype.type;
    /**
     * A tab change event fired right before the tab selection happens. See NgtTabChangeEvent for payload details
     * @type {?}
     */
    NgtTabset.prototype.tabChange;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFic2V0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJ0YWJzZXQvdGFic2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0gsU0FBUyxFQUNULEtBQUssRUFDTCxlQUFlLEVBQ2YsU0FBUyxFQUNULFNBQVMsRUFDVCxXQUFXLEVBR1gsTUFBTSxFQUNOLFlBQVksRUFDZixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUJBQWlCLENBQUM7O0lBRTlDLE1BQU0sR0FBRyxDQUFDOzs7O0FBTWQsTUFBTSxPQUFPLFdBQVc7Ozs7SUFDcEIsWUFBbUIsV0FBNkI7UUFBN0IsZ0JBQVcsR0FBWCxXQUFXLENBQWtCO0lBQ2hELENBQUM7OztZQUhKLFNBQVMsU0FBQyxFQUFDLFFBQVEsRUFBRSwwQkFBMEIsRUFBQzs7OztZQWI3QyxXQUFXOzs7O0lBZUMsa0NBQW9DOzs7OztBQVFwRCxNQUFNLE9BQU8sYUFBYTs7OztJQUN0QixZQUFtQixXQUE2QjtRQUE3QixnQkFBVyxHQUFYLFdBQVcsQ0FBa0I7SUFDaEQsQ0FBQzs7O1lBSEosU0FBUyxTQUFDLEVBQUMsUUFBUSxFQUFFLDRCQUE0QixFQUFDOzs7O1lBdEIvQyxXQUFXOzs7O0lBd0JDLG9DQUFvQzs7Ozs7QUFRcEQsTUFBTSxPQUFPLE1BQU07SUFEbkI7Ozs7UUFLYSxPQUFFLEdBQUcsV0FBVyxNQUFNLEVBQUUsRUFBRSxDQUFDOzs7O1FBUTNCLGFBQVEsR0FBRyxLQUFLLENBQUM7SUFnQjlCLENBQUM7Ozs7SUFSRyxxQkFBcUI7UUFDakIsOEZBQThGO1FBQzlGLDhFQUE4RTtRQUM5RSxpRUFBaUU7UUFDakUsMkRBQTJEO1FBQzNELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUM7UUFDckMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztJQUM3QyxDQUFDOzs7WUE1QkosU0FBUyxTQUFDLEVBQUMsUUFBUSxFQUFFLFNBQVMsRUFBQzs7O2lCQUszQixLQUFLO29CQUlMLEtBQUs7dUJBSUwsS0FBSzt3QkFLTCxlQUFlLFNBQUMsV0FBVyxFQUFFLEVBQUMsV0FBVyxFQUFFLEtBQUssRUFBQzswQkFDakQsZUFBZSxTQUFDLGFBQWEsRUFBRSxFQUFDLFdBQVcsRUFBRSxLQUFLLEVBQUM7Ozs7Ozs7SUFkcEQsb0JBQW9DOzs7OztJQUlwQyx1QkFBdUI7Ozs7O0lBSXZCLDBCQUEwQjs7SUFFMUIsMEJBQTZCOztJQUM3Qiw0QkFBaUM7O0lBRWpDLDJCQUFzRjs7SUFDdEYsNkJBQTRGOzs7Ozs7QUFlaEcsdUNBZUM7Ozs7OztJQVhHLHFDQUFpQjs7Ozs7SUFLakIsbUNBQWU7Ozs7O0lBS2YsMkNBQTJCOzs7OztBQW1DL0IsTUFBTSxPQUFPLFNBQVM7Ozs7SUE4Q2xCLFlBQVksTUFBdUI7Ozs7UUFqQzFCLGtCQUFhLEdBQUcsSUFBSSxDQUFDOzs7O1FBK0JwQixjQUFTLEdBQUcsSUFBSSxZQUFZLEVBQXFCLENBQUM7UUFHeEQsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQztRQUM5QixJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUM7SUFDMUMsQ0FBQzs7Ozs7Ozs7SUE5QkQsSUFDSSxPQUFPLENBQUMsU0FBNEQ7UUFDcEUsSUFBSSxTQUFTLEtBQUssTUFBTSxJQUFJLFNBQVMsS0FBSyxXQUFXLEVBQUU7WUFDbkQsSUFBSSxDQUFDLFlBQVksR0FBRyxPQUFPLFNBQVMsRUFBRSxDQUFDO1NBQzFDO2FBQU07WUFDSCxJQUFJLENBQUMsWUFBWSxHQUFHLG1CQUFtQixTQUFTLEVBQUUsQ0FBQztTQUN0RDtJQUNMLENBQUM7Ozs7Ozs7SUE2QkQsTUFBTSxDQUFDLEtBQWE7O1lBQ1osV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1FBQ3pDLElBQUksV0FBVyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLFdBQVcsQ0FBQyxFQUFFLEVBQUU7O2dCQUN0RSxnQkFBZ0IsR0FBRyxLQUFLO1lBRTVCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUNmO2dCQUNJLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxXQUFXLENBQUMsRUFBRSxFQUFFLGNBQWM7OztnQkFBRSxHQUFHLEVBQUU7b0JBQ2xFLGdCQUFnQixHQUFHLElBQUksQ0FBQztnQkFDNUIsQ0FBQyxDQUFBO2FBQ0osQ0FBQyxDQUFDO1lBRVAsSUFBSSxDQUFDLGdCQUFnQixFQUFFO2dCQUNuQixJQUFJLENBQUMsUUFBUSxHQUFHLFdBQVcsQ0FBQyxFQUFFLENBQUM7YUFDbEM7U0FDSjtJQUNMLENBQUM7Ozs7SUFFRCxxQkFBcUI7OztZQUViLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDL0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDOUYsQ0FBQzs7Ozs7O0lBRU8sV0FBVyxDQUFDLEVBQVU7O1lBQ3RCLFVBQVUsR0FBYSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU07Ozs7UUFBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFDO1FBQ2pFLE9BQU8sVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7SUFDcEQsQ0FBQzs7O1lBaEhKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0tBd0JUO2FBQ0o7Ozs7WUEzR1EsZUFBZTs7O21CQStHbkIsZUFBZSxTQUFDLE1BQU07dUJBS3RCLEtBQUs7NEJBS0wsS0FBSztzQkFPTCxLQUFLOzBCQWFMLEtBQUs7bUJBTUwsS0FBSzt3QkFLTCxNQUFNOzs7O0lBM0NQLGlDQUFxQjs7SUFFckIseUJBQWlEOzs7OztJQUtqRCw2QkFBMEI7Ozs7O0lBSzFCLGtDQUE4Qjs7Ozs7O0lBb0I5QixnQ0FBZ0Q7Ozs7OztJQU1oRCx5QkFBeUM7Ozs7O0lBS3pDLDhCQUE0RCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBDb21wb25lbnQsXHJcbiAgICBJbnB1dCxcclxuICAgIENvbnRlbnRDaGlsZHJlbixcclxuICAgIFF1ZXJ5TGlzdCxcclxuICAgIERpcmVjdGl2ZSxcclxuICAgIFRlbXBsYXRlUmVmLFxyXG4gICAgQ29udGVudENoaWxkLFxyXG4gICAgQWZ0ZXJDb250ZW50Q2hlY2tlZCxcclxuICAgIE91dHB1dCxcclxuICAgIEV2ZW50RW1pdHRlclxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOZ3RUYWJzZXRDb25maWcgfSBmcm9tICcuL3RhYnNldC1jb25maWcnO1xyXG5cclxubGV0IG5leHRJZCA9IDA7XHJcblxyXG4vKipcclxuICogVGhpcyBkaXJlY3RpdmUgc2hvdWxkIGJlIHVzZWQgdG8gd3JhcCB0YWIgdGl0bGVzIHRoYXQgbmVlZCB0byBjb250YWluIEhUTUwgbWFya3VwIG9yIG90aGVyIGRpcmVjdGl2ZXMuXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtzZWxlY3RvcjogJ25nLXRlbXBsYXRlW25ndFRhYlRpdGxlXSd9KVxyXG5leHBvcnQgY2xhc3MgTmd0VGFiVGl0bGUge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHRlbXBsYXRlUmVmOiBUZW1wbGF0ZVJlZjxhbnk+KSB7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUaGlzIGRpcmVjdGl2ZSBtdXN0IGJlIHVzZWQgdG8gd3JhcCBjb250ZW50IHRvIGJlIGRpc3BsYXllZCBpbiBhIHRhYi5cclxuICovXHJcbkBEaXJlY3RpdmUoe3NlbGVjdG9yOiAnbmctdGVtcGxhdGVbbmd0VGFiQ29udGVudF0nfSlcclxuZXhwb3J0IGNsYXNzIE5ndFRhYkNvbnRlbnQge1xyXG4gICAgY29uc3RydWN0b3IocHVibGljIHRlbXBsYXRlUmVmOiBUZW1wbGF0ZVJlZjxhbnk+KSB7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBBIGRpcmVjdGl2ZSByZXByZXNlbnRpbmcgYW4gaW5kaXZpZHVhbCB0YWIuXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtzZWxlY3RvcjogJ25ndC10YWInfSlcclxuZXhwb3J0IGNsYXNzIE5ndFRhYiB7XHJcbiAgICAvKipcclxuICAgICAqIFVuaXF1ZSB0YWIgaWRlbnRpZmllci4gTXVzdCBiZSB1bmlxdWUgZm9yIHRoZSBlbnRpcmUgZG9jdW1lbnQgZm9yIHByb3BlciBhY2Nlc3NpYmlsaXR5IHN1cHBvcnQuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGlkID0gYG5ndC10YWItJHtuZXh0SWQrK31gO1xyXG4gICAgLyoqXHJcbiAgICAgKiBTaW1wbGUgKHN0cmluZyBvbmx5KSB0aXRsZS4gVXNlIHRoZSBcIk5ndFRhYlRpdGxlXCIgZGlyZWN0aXZlIGZvciBtb3JlIGNvbXBsZXggdXNlLWNhc2VzLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSB0aXRsZTogc3RyaW5nO1xyXG4gICAgLyoqXHJcbiAgICAgKiBBbGxvd3MgdG9nZ2xpbmcgZGlzYWJsZWQgc3RhdGUgb2YgYSBnaXZlbiBzdGF0ZS4gRGlzYWJsZWQgdGFicyBjYW4ndCBiZSBzZWxlY3RlZC5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgZGlzYWJsZWQgPSBmYWxzZTtcclxuXHJcbiAgICB0aXRsZVRwbDogTmd0VGFiVGl0bGUgfCBudWxsO1xyXG4gICAgY29udGVudFRwbDogTmd0VGFiQ29udGVudCB8IG51bGw7XHJcblxyXG4gICAgQENvbnRlbnRDaGlsZHJlbihOZ3RUYWJUaXRsZSwge2Rlc2NlbmRhbnRzOiBmYWxzZX0pIHRpdGxlVHBsczogUXVlcnlMaXN0PE5ndFRhYlRpdGxlPjtcclxuICAgIEBDb250ZW50Q2hpbGRyZW4oTmd0VGFiQ29udGVudCwge2Rlc2NlbmRhbnRzOiBmYWxzZX0pIGNvbnRlbnRUcGxzOiBRdWVyeUxpc3Q8Tmd0VGFiQ29udGVudD47XHJcblxyXG4gICAgbmdBZnRlckNvbnRlbnRDaGVja2VkKCkge1xyXG4gICAgICAgIC8vIFdlIGFyZSB1c2luZyBAQ29udGVudENoaWxkcmVuIGluc3RlYWQgb2YgQENvbnRlbnRDaGlsZCBhcyBpbiB0aGUgQW5ndWxhciB2ZXJzaW9uIGJlaW5nIHVzZWRcclxuICAgICAgICAvLyBvbmx5IEBDb250ZW50Q2hpbGRyZW4gYWxsb3dzIHVzIHRvIHNwZWNpZnkgdGhlIHtkZXNjZW5kYW50czogZmFsc2V9IG9wdGlvbi5cclxuICAgICAgICAvLyBXaXRob3V0IHtkZXNjZW5kYW50czogZmFsc2V9IHdlIGFyZSBoaXR0aW5nIGJ1Z3MgZGVzY3JpYmVkIGluOlxyXG4gICAgICAgIC8vIGh0dHBzOi8vZ2l0aHViLmNvbS9uZy1ib290c3RyYXAvbmctYm9vdHN0cmFwL2lzc3Vlcy8yMjQwXHJcbiAgICAgICAgdGhpcy50aXRsZVRwbCA9IHRoaXMudGl0bGVUcGxzLmZpcnN0O1xyXG4gICAgICAgIHRoaXMuY29udGVudFRwbCA9IHRoaXMuY29udGVudFRwbHMuZmlyc3Q7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBUaGUgcGF5bG9hZCBvZiB0aGUgY2hhbmdlIGV2ZW50IGZpcmVkIHJpZ2h0IGJlZm9yZSB0aGUgdGFiIGNoYW5nZVxyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBOZ3RUYWJDaGFuZ2VFdmVudCB7XHJcbiAgICAvKipcclxuICAgICAqIElkIG9mIHRoZSBjdXJyZW50bHkgYWN0aXZlIHRhYlxyXG4gICAgICovXHJcbiAgICBhY3RpdmVJZDogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogSWQgb2YgdGhlIG5ld2x5IHNlbGVjdGVkIHRhYlxyXG4gICAgICovXHJcbiAgICBuZXh0SWQ6IHN0cmluZztcclxuXHJcbiAgICAvKipcclxuICAgICAqIEZ1bmN0aW9uIHRoYXQgd2lsbCBwcmV2ZW50IHRhYiBzd2l0Y2ggaWYgY2FsbGVkXHJcbiAgICAgKi9cclxuICAgIHByZXZlbnREZWZhdWx0OiAoKSA9PiB2b2lkO1xyXG59XHJcblxyXG4vKipcclxuICogQSBjb21wb25lbnQgdGhhdCBtYWtlcyBpdCBlYXN5IHRvIGNyZWF0ZSB0YWJiZWQgaW50ZXJmYWNlLlxyXG4gKi9cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC10YWJzZXQnLFxyXG4gICAgZXhwb3J0QXM6ICduZ3RUYWJzZXQnLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8dWwgW2NsYXNzXT1cIiduYXYgbmF2LScgKyB0eXBlICsgKG9yaWVudGF0aW9uID09ICdob3Jpem9udGFsJz8gICcgJyArIGp1c3RpZnlDbGFzcyA6ICcgZmxleC1jb2x1bW4nKVwiIHJvbGU9XCJ0YWJsaXN0XCI+XHJcbiAgICAgICAgICAgIDxsaSBjbGFzcz1cIm5hdi1pdGVtXCIgKm5nRm9yPVwibGV0IHRhYiBvZiB0YWJzXCI+XHJcbiAgICAgICAgICAgICAgICA8YSBbaWRdPVwidGFiLmlkXCIgY2xhc3M9XCJuYXYtbGlua1wiIFtjbGFzcy5hY3RpdmVdPVwidGFiLmlkID09PSBhY3RpdmVJZFwiIFtjbGFzcy5kaXNhYmxlZF09XCJ0YWIuZGlzYWJsZWRcIlxyXG4gICAgICAgICAgICAgICAgICAgaHJlZiAoY2xpY2spPVwic2VsZWN0KHRhYi5pZCk7ICRldmVudC5wcmV2ZW50RGVmYXVsdCgpXCIgcm9sZT1cInRhYlwiIFthdHRyLnRhYmluZGV4XT1cIih0YWIuZGlzYWJsZWQgPyAnLTEnOiB1bmRlZmluZWQpXCJcclxuICAgICAgICAgICAgICAgICAgIFthdHRyLmFyaWEtY29udHJvbHNdPVwiKCFkZXN0cm95T25IaWRlIHx8IHRhYi5pZCA9PT0gYWN0aXZlSWQgPyB0YWIuaWQgKyAnLXBhbmVsJyA6IG51bGwpXCJcclxuICAgICAgICAgICAgICAgICAgIFthdHRyLmFyaWEtZXhwYW5kZWRdPVwidGFiLmlkID09PSBhY3RpdmVJZFwiIFthdHRyLmFyaWEtZGlzYWJsZWRdPVwidGFiLmRpc2FibGVkXCI+XHJcbiAgICAgICAgICAgICAgICAgICAge3t0YWIudGl0bGV9fVxyXG4gICAgICAgICAgICAgICAgICAgIDxuZy10ZW1wbGF0ZSBbbmdUZW1wbGF0ZU91dGxldF09XCJ0YWIudGl0bGVUcGw/LnRlbXBsYXRlUmVmXCI+PC9uZy10ZW1wbGF0ZT5cclxuICAgICAgICAgICAgICAgIDwvYT5cclxuICAgICAgICAgICAgPC9saT5cclxuICAgICAgICA8L3VsPlxyXG4gICAgICAgIDxkaXYgY2xhc3M9XCJ0YWItY29udGVudFwiPlxyXG4gICAgICAgICAgICA8bmctdGVtcGxhdGUgbmdGb3IgbGV0LXRhYiBbbmdGb3JPZl09XCJ0YWJzXCI+XHJcbiAgICAgICAgICAgICAgICA8ZGl2XHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJ0YWItcGFuZSB7e3RhYi5pZCA9PT0gYWN0aXZlSWQgPyAnYWN0aXZlJyA6IG51bGx9fVwiXHJcbiAgICAgICAgICAgICAgICAgICAgKm5nSWY9XCIhZGVzdHJveU9uSGlkZSB8fCB0YWIuaWQgPT09IGFjdGl2ZUlkXCJcclxuICAgICAgICAgICAgICAgICAgICByb2xlPVwidGFicGFuZWxcIlxyXG4gICAgICAgICAgICAgICAgICAgIFthdHRyLmFyaWEtbGFiZWxsZWRieV09XCJ0YWIuaWRcIiBpZD1cInt7dGFiLmlkfX0tcGFuZWxcIlxyXG4gICAgICAgICAgICAgICAgICAgIFthdHRyLmFyaWEtZXhwYW5kZWRdPVwidGFiLmlkID09PSBhY3RpdmVJZFwiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxuZy10ZW1wbGF0ZSBbbmdUZW1wbGF0ZU91dGxldF09XCJ0YWIuY29udGVudFRwbD8udGVtcGxhdGVSZWZcIj48L25nLXRlbXBsYXRlPlxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvbmctdGVtcGxhdGU+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RUYWJzZXQgaW1wbGVtZW50cyBBZnRlckNvbnRlbnRDaGVja2VkIHtcclxuICAgIGp1c3RpZnlDbGFzczogc3RyaW5nO1xyXG5cclxuICAgIEBDb250ZW50Q2hpbGRyZW4oTmd0VGFiKSB0YWJzOiBRdWVyeUxpc3Q8Tmd0VGFiPjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEFuIGlkZW50aWZpZXIgb2YgYW4gaW5pdGlhbGx5IHNlbGVjdGVkIChhY3RpdmUpIHRhYi4gVXNlIHRoZSBcInNlbGVjdFwiIG1ldGhvZCB0byBzd2l0Y2ggYSB0YWIgcHJvZ3JhbW1hdGljYWxseS5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgYWN0aXZlSWQ6IHN0cmluZztcclxuXHJcbiAgICAvKipcclxuICAgICAqIFdoZXRoZXIgdGhlIGNsb3NlZCB0YWJzIHNob3VsZCBiZSBoaWRkZW4gd2l0aG91dCBkZXN0cm95aW5nIHRoZW1cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgZGVzdHJveU9uSGlkZSA9IHRydWU7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGUgaG9yaXpvbnRhbCBhbGlnbm1lbnQgb2YgdGhlIG5hdiB3aXRoIGZsZXhib3ggdXRpbGl0aWVzLiBDYW4gYmUgb25lIG9mICdzdGFydCcsICdjZW50ZXInLCAnZW5kJywgJ2ZpbGwnIG9yXHJcbiAgICAgKiAnanVzdGlmaWVkJ1xyXG4gICAgICogVGhlIGRlZmF1bHQgdmFsdWUgaXMgJ3N0YXJ0Jy5cclxuICAgICAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNldCBqdXN0aWZ5KGNsYXNzTmFtZTogJ3N0YXJ0JyB8ICdjZW50ZXInIHwgJ2VuZCcgfCAnZmlsbCcgfCAnanVzdGlmaWVkJykge1xyXG4gICAgICAgIGlmIChjbGFzc05hbWUgPT09ICdmaWxsJyB8fCBjbGFzc05hbWUgPT09ICdqdXN0aWZpZWQnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuanVzdGlmeUNsYXNzID0gYG5hdi0ke2NsYXNzTmFtZX1gO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuanVzdGlmeUNsYXNzID0gYGp1c3RpZnktY29udGVudC0ke2NsYXNzTmFtZX1gO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRoZSBvcmllbnRhdGlvbiBvZiB0aGUgbmF2IChob3Jpem9udGFsIG9yIHZlcnRpY2FsKS5cclxuICAgICAqIFRoZSBkZWZhdWx0IHZhbHVlIGlzICdob3Jpem9udGFsJy5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgb3JpZW50YXRpb246ICdob3Jpem9udGFsJyB8ICd2ZXJ0aWNhbCc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUeXBlIG9mIG5hdmlnYXRpb24gdG8gYmUgdXNlZCBmb3IgdGFicy4gQ2FuIGJlIG9uZSBvZiBCb290c3RyYXAgZGVmaW5lZCB0eXBlcyAoJ3RhYnMnIG9yICdwaWxscycpLlxyXG4gICAgICogU2luY2UgMy4wLjAgY2FuIGFsc28gYmUgYW4gYXJiaXRyYXJ5IHN0cmluZyAoZm9yIGN1c3RvbSB0aGVtZXMpLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSB0eXBlOiAndGFicycgfCAncGlsbHMnIHwgc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQSB0YWIgY2hhbmdlIGV2ZW50IGZpcmVkIHJpZ2h0IGJlZm9yZSB0aGUgdGFiIHNlbGVjdGlvbiBoYXBwZW5zLiBTZWUgTmd0VGFiQ2hhbmdlRXZlbnQgZm9yIHBheWxvYWQgZGV0YWlsc1xyXG4gICAgICovXHJcbiAgICBAT3V0cHV0KCkgdGFiQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxOZ3RUYWJDaGFuZ2VFdmVudD4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihjb25maWc6IE5ndFRhYnNldENvbmZpZykge1xyXG4gICAgICAgIHRoaXMudHlwZSA9IGNvbmZpZy50eXBlO1xyXG4gICAgICAgIHRoaXMuanVzdGlmeSA9IGNvbmZpZy5qdXN0aWZ5O1xyXG4gICAgICAgIHRoaXMub3JpZW50YXRpb24gPSBjb25maWcub3JpZW50YXRpb247XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZWxlY3RzIHRoZSB0YWIgd2l0aCB0aGUgZ2l2ZW4gaWQgYW5kIHNob3dzIGl0cyBhc3NvY2lhdGVkIHBhbmUuXHJcbiAgICAgKiBBbnkgb3RoZXIgdGFiIHRoYXQgd2FzIHByZXZpb3VzbHkgc2VsZWN0ZWQgYmVjb21lcyB1bnNlbGVjdGVkIGFuZCBpdHMgYXNzb2NpYXRlZCBwYW5lIGlzIGhpZGRlbi5cclxuICAgICAqL1xyXG4gICAgc2VsZWN0KHRhYklkOiBzdHJpbmcpIHtcclxuICAgICAgICBsZXQgc2VsZWN0ZWRUYWIgPSB0aGlzLl9nZXRUYWJCeUlkKHRhYklkKTtcclxuICAgICAgICBpZiAoc2VsZWN0ZWRUYWIgJiYgIXNlbGVjdGVkVGFiLmRpc2FibGVkICYmIHRoaXMuYWN0aXZlSWQgIT09IHNlbGVjdGVkVGFiLmlkKSB7XHJcbiAgICAgICAgICAgIGxldCBkZWZhdWx0UHJldmVudGVkID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnRhYkNoYW5nZS5lbWl0KFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIGFjdGl2ZUlkOiB0aGlzLmFjdGl2ZUlkLCBuZXh0SWQ6IHNlbGVjdGVkVGFiLmlkLCBwcmV2ZW50RGVmYXVsdDogKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWZhdWx0UHJldmVudGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGlmICghZGVmYXVsdFByZXZlbnRlZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hY3RpdmVJZCA9IHNlbGVjdGVkVGFiLmlkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nQWZ0ZXJDb250ZW50Q2hlY2tlZCgpIHtcclxuICAgICAgICAvLyBhdXRvLWNvcnJlY3QgYWN0aXZlSWQgdGhhdCBtaWdodCBoYXZlIGJlZW4gc2V0IGluY29ycmVjdGx5IGFzIGlucHV0XHJcbiAgICAgICAgbGV0IGFjdGl2ZVRhYiA9IHRoaXMuX2dldFRhYkJ5SWQodGhpcy5hY3RpdmVJZCk7XHJcbiAgICAgICAgdGhpcy5hY3RpdmVJZCA9IGFjdGl2ZVRhYiA/IGFjdGl2ZVRhYi5pZCA6ICh0aGlzLnRhYnMubGVuZ3RoID8gdGhpcy50YWJzLmZpcnN0LmlkIDogbnVsbCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfZ2V0VGFiQnlJZChpZDogc3RyaW5nKTogTmd0VGFiIHtcclxuICAgICAgICBsZXQgdGFic1dpdGhJZDogTmd0VGFiW10gPSB0aGlzLnRhYnMuZmlsdGVyKHRhYiA9PiB0YWIuaWQgPT09IGlkKTtcclxuICAgICAgICByZXR1cm4gdGFic1dpdGhJZC5sZW5ndGggPyB0YWJzV2l0aElkWzBdIDogbnVsbDtcclxuICAgIH1cclxufVxyXG4iXX0=