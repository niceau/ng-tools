/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Subject } from 'rxjs';
/**
 * A reference to an active (currently opened) modal. Instances of this class
 * can be injected into components passed as modal content.
 */
export class NgtActiveModal {
    constructor() {
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
    }
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    close(result) { }
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    dismiss(reason) { }
}
if (false) {
    /** @type {?} */
    NgtActiveModal.prototype.modalOpeningDidStart;
    /** @type {?} */
    NgtActiveModal.prototype.modalOpeningDidDone;
    /** @type {?} */
    NgtActiveModal.prototype.modalClosingDidStart;
    /** @type {?} */
    NgtActiveModal.prototype.modalClosingDidDone;
    /** @type {?} */
    NgtActiveModal.prototype.backdropOpeningDidStart;
    /** @type {?} */
    NgtActiveModal.prototype.backdropOpeningDidDone;
    /** @type {?} */
    NgtActiveModal.prototype.backdropClosingDidStart;
    /** @type {?} */
    NgtActiveModal.prototype.backdropClosingDidDone;
}
/**
 * A reference to a newly opened modal returned by the 'NgtModalComponent.open()' method.
 */
export class NgtModalRef {
    /**
     * @param {?} _windowCmptRef
     * @param {?} _contentRef
     * @param {?=} _backdropCmptRef
     * @param {?=} _beforeDismiss
     */
    constructor(_windowCmptRef, _contentRef, _backdropCmptRef, _beforeDismiss) {
        this._windowCmptRef = _windowCmptRef;
        this._contentRef = _contentRef;
        this._backdropCmptRef = _backdropCmptRef;
        this._beforeDismiss = _beforeDismiss;
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
        _windowCmptRef.instance.dismissEvent.subscribe((/**
         * @param {?} reason
         * @return {?}
         */
        (reason) => this.dismiss(reason)));
        _windowCmptRef.instance.modalOpeningDidStart.subscribe((/**
         * @return {?}
         */
        () => this.modalOpeningDidStart.next()));
        _windowCmptRef.instance.modalOpeningDidDone.subscribe((/**
         * @return {?}
         */
        () => this.modalOpeningDidDone.next()));
        _windowCmptRef.instance.modalClosingDidStart.subscribe((/**
         * @return {?}
         */
        () => this.modalClosingDidStart.next()));
        _windowCmptRef.instance.modalClosingDidDone.subscribe((/**
         * @return {?}
         */
        () => this.modalClosingDidDone.next()));
        _backdropCmptRef.instance.backdropOpeningDidStart.subscribe((/**
         * @return {?}
         */
        () => this.backdropOpeningDidStart.next()));
        _backdropCmptRef.instance.backdropOpeningDidDone.subscribe((/**
         * @return {?}
         */
        () => this.backdropOpeningDidDone.next()));
        _backdropCmptRef.instance.backdropClosingDidStart.subscribe((/**
         * @return {?}
         */
        () => this.backdropClosingDidStart.next()));
        _backdropCmptRef.instance.backdropClosingDidDone.subscribe((/**
         * @return {?}
         */
        () => this.backdropClosingDidDone.next()));
        this.result = new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        (resolve, reject) => {
            this._resolve = resolve;
            this._reject = reject;
        }));
        this.result.then(null, (/**
         * @return {?}
         */
        () => { }));
    }
    /**
     * The instance of component used as modal's content.
     * Undefined when a TemplateRef is used as modal's content.
     * @return {?}
     */
    get componentInstance() {
        if (this._contentRef.componentRef) {
            return this._contentRef.componentRef.instance;
        }
    }
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    close(result) {
        if (this._windowCmptRef) {
            this._resolve(result);
            this._removeModalElements();
        }
    }
    /**
     * @private
     * @param {?=} reason
     * @return {?}
     */
    _dismiss(reason) {
        this._reject(reason);
        this._removeModalElements();
    }
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    dismiss(reason) {
        if (this._windowCmptRef) {
            if (!this._beforeDismiss) {
                this._dismiss(reason);
            }
            else {
                /** @type {?} */
                const dismiss = this._beforeDismiss();
                if (dismiss && dismiss.then) {
                    dismiss.then((/**
                     * @param {?} result
                     * @return {?}
                     */
                    result => {
                        if (result !== false) {
                            this._dismiss(reason);
                        }
                    }), (/**
                     * @return {?}
                     */
                    () => {
                    }));
                }
                else if (dismiss !== false) {
                    this._dismiss(reason);
                }
            }
        }
    }
    /**
     * @private
     * @return {?}
     */
    _removeModalElements() {
        this.modalClosingDidDone.subscribe((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const windowNativeEl = this._windowCmptRef.location.nativeElement;
            windowNativeEl.parentNode.removeChild(windowNativeEl);
            this._windowCmptRef.destroy();
            this._windowCmptRef = null;
            if (this._contentRef && this._contentRef.viewRef) {
                this._contentRef.viewRef.destroy();
            }
            this._contentRef = null;
        }));
        this._windowCmptRef.instance.animation = 'close';
        if (this._backdropCmptRef) {
            this.backdropClosingDidDone.subscribe((/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const backdropNativeEl = this._backdropCmptRef.location.nativeElement;
                backdropNativeEl.parentNode.removeChild(backdropNativeEl);
                this._backdropCmptRef.destroy();
                this._backdropCmptRef = null;
            }));
            this._backdropCmptRef.instance.animation = 'close';
        }
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._resolve;
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._reject;
    /** @type {?} */
    NgtModalRef.prototype.modalOpeningDidStart;
    /** @type {?} */
    NgtModalRef.prototype.modalOpeningDidDone;
    /** @type {?} */
    NgtModalRef.prototype.modalClosingDidStart;
    /** @type {?} */
    NgtModalRef.prototype.modalClosingDidDone;
    /** @type {?} */
    NgtModalRef.prototype.backdropOpeningDidStart;
    /** @type {?} */
    NgtModalRef.prototype.backdropOpeningDidDone;
    /** @type {?} */
    NgtModalRef.prototype.backdropClosingDidStart;
    /** @type {?} */
    NgtModalRef.prototype.backdropClosingDidDone;
    /**
     * A promise that is resolved when the modal is closed and rejected when the modal is dismissed.
     * @type {?}
     */
    NgtModalRef.prototype.result;
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._windowCmptRef;
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._contentRef;
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._backdropCmptRef;
    /**
     * @type {?}
     * @private
     */
    NgtModalRef.prototype._beforeDismiss;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtcmVmLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJtb2RhbC9tb2RhbC1yZWYudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUdBLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7Ozs7O0FBTy9CLE1BQU0sT0FBTyxjQUFjO0lBQTNCO1FBQ0kseUJBQW9CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNyQyx3QkFBbUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3BDLHlCQUFvQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDckMsd0JBQW1CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUVwQyw0QkFBdUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3hDLDJCQUFzQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDdkMsNEJBQXVCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUN4QywyQkFBc0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO0lBYTNDLENBQUM7Ozs7Ozs7SUFSRyxLQUFLLENBQUMsTUFBWSxJQUFTLENBQUM7Ozs7Ozs7SUFNNUIsT0FBTyxDQUFDLE1BQVksSUFBUyxDQUFDO0NBRWpDOzs7SUFyQkcsOENBQXFDOztJQUNyQyw2Q0FBb0M7O0lBQ3BDLDhDQUFxQzs7SUFDckMsNkNBQW9DOztJQUVwQyxpREFBd0M7O0lBQ3hDLGdEQUF1Qzs7SUFDdkMsaURBQXdDOztJQUN4QyxnREFBdUM7Ozs7O0FBa0IzQyxNQUFNLE9BQU8sV0FBVzs7Ozs7OztJQTZCcEIsWUFDWSxjQUFxRCxFQUNyRCxXQUF1QixFQUN2QixnQkFBMEQsRUFDMUQsY0FBeUI7UUFIekIsbUJBQWMsR0FBZCxjQUFjLENBQXVDO1FBQ3JELGdCQUFXLEdBQVgsV0FBVyxDQUFZO1FBQ3ZCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBMEM7UUFDMUQsbUJBQWMsR0FBZCxjQUFjLENBQVc7UUE3QnJDLHlCQUFvQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDckMsd0JBQW1CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNwQyx5QkFBb0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3JDLHdCQUFtQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFFcEMsNEJBQXVCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUN4QywyQkFBc0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3ZDLDRCQUF1QixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDeEMsMkJBQXNCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQXNCbkMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsU0FBUzs7OztRQUFDLENBQUMsTUFBVyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxFQUFDLENBQUM7UUFDdEYsY0FBYyxDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLEVBQUMsQ0FBQztRQUMvRixjQUFjLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsRUFBQyxDQUFDO1FBQzdGLGNBQWMsQ0FBQyxRQUFRLENBQUMsb0JBQW9CLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxFQUFDLENBQUM7UUFDL0YsY0FBYyxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLEVBQUMsQ0FBQztRQUM3RixnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksRUFBRSxFQUFDLENBQUM7UUFDdkcsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLHNCQUFzQixDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsRUFBQyxDQUFDO1FBQ3JHLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLEVBQUMsQ0FBQztRQUN2RyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsc0JBQXNCLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksRUFBRSxFQUFDLENBQUM7UUFFckcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLE9BQU87Ozs7O1FBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDMUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7WUFDeEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDMUIsQ0FBQyxFQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJOzs7UUFBRSxHQUFHLEVBQUUsR0FBRSxDQUFDLEVBQUMsQ0FBQztJQUNyQyxDQUFDOzs7Ozs7SUEvQkQsSUFBSSxpQkFBaUI7UUFDakIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRTtZQUMvQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQztTQUNqRDtJQUNMLENBQUM7Ozs7Ozs7SUFpQ0QsS0FBSyxDQUFDLE1BQVk7UUFDZCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztTQUMvQjtJQUNMLENBQUM7Ozs7OztJQUVPLFFBQVEsQ0FBQyxNQUFZO1FBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDckIsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7SUFDaEMsQ0FBQzs7Ozs7OztJQU1ELE9BQU8sQ0FBQyxNQUFZO1FBQ2hCLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRTtnQkFDdEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUN6QjtpQkFBTTs7c0JBQ0csT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUU7Z0JBQ3JDLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxJQUFJLEVBQUU7b0JBQ3pCLE9BQU8sQ0FBQyxJQUFJOzs7O29CQUNSLE1BQU0sQ0FBQyxFQUFFO3dCQUNMLElBQUksTUFBTSxLQUFLLEtBQUssRUFBRTs0QkFDbEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQzt5QkFDekI7b0JBQ0wsQ0FBQzs7O29CQUNELEdBQUcsRUFBRTtvQkFDTCxDQUFDLEVBQUMsQ0FBQztpQkFDVjtxQkFBTSxJQUFJLE9BQU8sS0FBSyxLQUFLLEVBQUU7b0JBQzFCLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ3pCO2FBQ0o7U0FDSjtJQUNMLENBQUM7Ozs7O0lBRU8sb0JBQW9CO1FBQ3hCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUU7O2tCQUM5QixjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYTtZQUNqRSxjQUFjLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzlCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBRTNCLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRTtnQkFDOUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDdEM7WUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUM1QixDQUFDLEVBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7UUFFakQsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDdkIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVM7OztZQUFDLEdBQUcsRUFBRTs7c0JBQ2pDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsYUFBYTtnQkFDckUsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUMxRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7WUFDakMsQ0FBQyxFQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7U0FDdEQ7SUFFTCxDQUFDO0NBQ0o7Ozs7OztJQXRIRywrQkFBeUM7Ozs7O0lBQ3pDLDhCQUF3Qzs7SUFFeEMsMkNBQXFDOztJQUNyQywwQ0FBb0M7O0lBQ3BDLDJDQUFxQzs7SUFDckMsMENBQW9DOztJQUVwQyw4Q0FBd0M7O0lBQ3hDLDZDQUF1Qzs7SUFDdkMsOENBQXdDOztJQUN4Qyw2Q0FBdUM7Ozs7O0lBZXZDLDZCQUFxQjs7Ozs7SUFHakIscUNBQTZEOzs7OztJQUM3RCxrQ0FBK0I7Ozs7O0lBQy9CLHVDQUFrRTs7Ozs7SUFDbEUscUNBQWlDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5ndE1vZGFsV2luZG93Q29tcG9uZW50IH0gZnJvbSAnLi9tb2RhbC13aW5kb3cuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTmd0TW9kYWxCYWNrZHJvcENvbXBvbmVudCB9IGZyb20gJy4vbW9kYWwtYmFja2Ryb3AuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBDb250ZW50UmVmIH0gZnJvbSAnLi4vdXRpbC9wb3B1cCc7XHJcblxyXG4vKipcclxuICogQSByZWZlcmVuY2UgdG8gYW4gYWN0aXZlIChjdXJyZW50bHkgb3BlbmVkKSBtb2RhbC4gSW5zdGFuY2VzIG9mIHRoaXMgY2xhc3NcclxuICogY2FuIGJlIGluamVjdGVkIGludG8gY29tcG9uZW50cyBwYXNzZWQgYXMgbW9kYWwgY29udGVudC5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBOZ3RBY3RpdmVNb2RhbCB7XHJcbiAgICBtb2RhbE9wZW5pbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBtb2RhbE9wZW5pbmdEaWREb25lID0gbmV3IFN1YmplY3QoKTtcclxuICAgIG1vZGFsQ2xvc2luZ0RpZFN0YXJ0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIG1vZGFsQ2xvc2luZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG5cclxuICAgIGJhY2tkcm9wT3BlbmluZ0RpZFN0YXJ0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIGJhY2tkcm9wT3BlbmluZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgYmFja2Ryb3BDbG9zaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgYmFja2Ryb3BDbG9zaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICAvKipcclxuICAgICAqIENsb3NlcyB0aGUgbW9kYWwgd2l0aCBhbiBvcHRpb25hbCAncmVzdWx0JyB2YWx1ZS5cclxuICAgICAqIFRoZSAnTmd0TW9iYWxSZWYucmVzdWx0JyBwcm9taXNlIHdpbGwgYmUgcmVzb2x2ZWQgd2l0aCBwcm92aWRlZCB2YWx1ZS5cclxuICAgICAqL1xyXG4gICAgY2xvc2UocmVzdWx0PzogYW55KTogdm9pZCB7fVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGlzbWlzc2VzIHRoZSBtb2RhbCB3aXRoIGFuIG9wdGlvbmFsICdyZWFzb24nIHZhbHVlLlxyXG4gICAgICogVGhlICdOZ3RNb2RhbFJlZi5yZXN1bHQnIHByb21pc2Ugd2lsbCBiZSByZWplY3RlZCB3aXRoIHByb3ZpZGVkIHZhbHVlLlxyXG4gICAgICovXHJcbiAgICBkaXNtaXNzKHJlYXNvbj86IGFueSk6IHZvaWQge31cclxuXHJcbn1cclxuXHJcbi8qKlxyXG4gKiBBIHJlZmVyZW5jZSB0byBhIG5ld2x5IG9wZW5lZCBtb2RhbCByZXR1cm5lZCBieSB0aGUgJ05ndE1vZGFsQ29tcG9uZW50Lm9wZW4oKScgbWV0aG9kLlxyXG4gKi9cclxuZXhwb3J0IGNsYXNzIE5ndE1vZGFsUmVmIHtcclxuICAgIHByaXZhdGUgX3Jlc29sdmU6IChyZXN1bHQ/OiBhbnkpID0+IHZvaWQ7XHJcbiAgICBwcml2YXRlIF9yZWplY3Q6IChyZWFzb24/OiBhbnkpID0+IHZvaWQ7XHJcblxyXG4gICAgbW9kYWxPcGVuaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgbW9kYWxPcGVuaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBtb2RhbENsb3NpbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBtb2RhbENsb3NpbmdEaWREb25lID0gbmV3IFN1YmplY3QoKTtcclxuXHJcbiAgICBiYWNrZHJvcE9wZW5pbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBiYWNrZHJvcE9wZW5pbmdEaWREb25lID0gbmV3IFN1YmplY3QoKTtcclxuICAgIGJhY2tkcm9wQ2xvc2luZ0RpZFN0YXJ0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIGJhY2tkcm9wQ2xvc2luZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhlIGluc3RhbmNlIG9mIGNvbXBvbmVudCB1c2VkIGFzIG1vZGFsJ3MgY29udGVudC5cclxuICAgICAqIFVuZGVmaW5lZCB3aGVuIGEgVGVtcGxhdGVSZWYgaXMgdXNlZCBhcyBtb2RhbCdzIGNvbnRlbnQuXHJcbiAgICAgKi9cclxuICAgIGdldCBjb21wb25lbnRJbnN0YW5jZSgpOiBhbnkge1xyXG4gICAgICAgIGlmICh0aGlzLl9jb250ZW50UmVmLmNvbXBvbmVudFJlZikge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5fY29udGVudFJlZi5jb21wb25lbnRSZWYuaW5zdGFuY2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQSBwcm9taXNlIHRoYXQgaXMgcmVzb2x2ZWQgd2hlbiB0aGUgbW9kYWwgaXMgY2xvc2VkIGFuZCByZWplY3RlZCB3aGVuIHRoZSBtb2RhbCBpcyBkaXNtaXNzZWQuXHJcbiAgICAgKi9cclxuICAgIHJlc3VsdDogUHJvbWlzZTxhbnk+O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgX3dpbmRvd0NtcHRSZWY6IENvbXBvbmVudFJlZjxOZ3RNb2RhbFdpbmRvd0NvbXBvbmVudD4sXHJcbiAgICAgICAgcHJpdmF0ZSBfY29udGVudFJlZjogQ29udGVudFJlZixcclxuICAgICAgICBwcml2YXRlIF9iYWNrZHJvcENtcHRSZWY/OiBDb21wb25lbnRSZWY8Tmd0TW9kYWxCYWNrZHJvcENvbXBvbmVudD4sXHJcbiAgICAgICAgcHJpdmF0ZSBfYmVmb3JlRGlzbWlzcz86IEZ1bmN0aW9uKSB7XHJcbiAgICAgICAgX3dpbmRvd0NtcHRSZWYuaW5zdGFuY2UuZGlzbWlzc0V2ZW50LnN1YnNjcmliZSgocmVhc29uOiBhbnkpID0+IHRoaXMuZGlzbWlzcyhyZWFzb24pKTtcclxuICAgICAgICBfd2luZG93Q21wdFJlZi5pbnN0YW5jZS5tb2RhbE9wZW5pbmdEaWRTdGFydC5zdWJzY3JpYmUoKCkgPT4gdGhpcy5tb2RhbE9wZW5pbmdEaWRTdGFydC5uZXh0KCkpO1xyXG4gICAgICAgIF93aW5kb3dDbXB0UmVmLmluc3RhbmNlLm1vZGFsT3BlbmluZ0RpZERvbmUuc3Vic2NyaWJlKCgpID0+IHRoaXMubW9kYWxPcGVuaW5nRGlkRG9uZS5uZXh0KCkpO1xyXG4gICAgICAgIF93aW5kb3dDbXB0UmVmLmluc3RhbmNlLm1vZGFsQ2xvc2luZ0RpZFN0YXJ0LnN1YnNjcmliZSgoKSA9PiB0aGlzLm1vZGFsQ2xvc2luZ0RpZFN0YXJ0Lm5leHQoKSk7XHJcbiAgICAgICAgX3dpbmRvd0NtcHRSZWYuaW5zdGFuY2UubW9kYWxDbG9zaW5nRGlkRG9uZS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5tb2RhbENsb3NpbmdEaWREb25lLm5leHQoKSk7XHJcbiAgICAgICAgX2JhY2tkcm9wQ21wdFJlZi5pbnN0YW5jZS5iYWNrZHJvcE9wZW5pbmdEaWRTdGFydC5zdWJzY3JpYmUoKCkgPT4gdGhpcy5iYWNrZHJvcE9wZW5pbmdEaWRTdGFydC5uZXh0KCkpO1xyXG4gICAgICAgIF9iYWNrZHJvcENtcHRSZWYuaW5zdGFuY2UuYmFja2Ryb3BPcGVuaW5nRGlkRG9uZS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5iYWNrZHJvcE9wZW5pbmdEaWREb25lLm5leHQoKSk7XHJcbiAgICAgICAgX2JhY2tkcm9wQ21wdFJlZi5pbnN0YW5jZS5iYWNrZHJvcENsb3NpbmdEaWRTdGFydC5zdWJzY3JpYmUoKCkgPT4gdGhpcy5iYWNrZHJvcENsb3NpbmdEaWRTdGFydC5uZXh0KCkpO1xyXG4gICAgICAgIF9iYWNrZHJvcENtcHRSZWYuaW5zdGFuY2UuYmFja2Ryb3BDbG9zaW5nRGlkRG9uZS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5iYWNrZHJvcENsb3NpbmdEaWREb25lLm5leHQoKSk7XHJcblxyXG4gICAgICAgIHRoaXMucmVzdWx0ID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLl9yZXNvbHZlID0gcmVzb2x2ZTtcclxuICAgICAgICAgICAgdGhpcy5fcmVqZWN0ID0gcmVqZWN0O1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMucmVzdWx0LnRoZW4obnVsbCwgKCkgPT4ge30pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2xvc2VzIHRoZSBtb2RhbCB3aXRoIGFuIG9wdGlvbmFsICdyZXN1bHQnIHZhbHVlLlxyXG4gICAgICogVGhlICdOZ3RNb2JhbFJlZi5yZXN1bHQnIHByb21pc2Ugd2lsbCBiZSByZXNvbHZlZCB3aXRoIHByb3ZpZGVkIHZhbHVlLlxyXG4gICAgICovXHJcbiAgICBjbG9zZShyZXN1bHQ/OiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBpZiAodGhpcy5fd2luZG93Q21wdFJlZikge1xyXG4gICAgICAgICAgICB0aGlzLl9yZXNvbHZlKHJlc3VsdCk7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlbW92ZU1vZGFsRWxlbWVudHMoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfZGlzbWlzcyhyZWFzb24/OiBhbnkpIHtcclxuICAgICAgICB0aGlzLl9yZWplY3QocmVhc29uKTtcclxuICAgICAgICB0aGlzLl9yZW1vdmVNb2RhbEVsZW1lbnRzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEaXNtaXNzZXMgdGhlIG1vZGFsIHdpdGggYW4gb3B0aW9uYWwgJ3JlYXNvbicgdmFsdWUuXHJcbiAgICAgKiBUaGUgJ05ndE1vZGFsUmVmLnJlc3VsdCcgcHJvbWlzZSB3aWxsIGJlIHJlamVjdGVkIHdpdGggcHJvdmlkZWQgdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIGRpc21pc3MocmVhc29uPzogYW55KTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHRoaXMuX3dpbmRvd0NtcHRSZWYpIHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLl9iZWZvcmVEaXNtaXNzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9kaXNtaXNzKHJlYXNvbik7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkaXNtaXNzID0gdGhpcy5fYmVmb3JlRGlzbWlzcygpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGRpc21pc3MgJiYgZGlzbWlzcy50aGVuKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGlzbWlzcy50aGVuKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc3VsdCAhPT0gZmFsc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLl9kaXNtaXNzKHJlYXNvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGRpc21pc3MgIT09IGZhbHNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fZGlzbWlzcyhyZWFzb24pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX3JlbW92ZU1vZGFsRWxlbWVudHMoKSB7XHJcbiAgICAgICAgdGhpcy5tb2RhbENsb3NpbmdEaWREb25lLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHdpbmRvd05hdGl2ZUVsID0gdGhpcy5fd2luZG93Q21wdFJlZi5sb2NhdGlvbi5uYXRpdmVFbGVtZW50O1xyXG4gICAgICAgICAgICB3aW5kb3dOYXRpdmVFbC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHdpbmRvd05hdGl2ZUVsKTtcclxuICAgICAgICAgICAgdGhpcy5fd2luZG93Q21wdFJlZi5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgIHRoaXMuX3dpbmRvd0NtcHRSZWYgPSBudWxsO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuX2NvbnRlbnRSZWYgJiYgdGhpcy5fY29udGVudFJlZi52aWV3UmVmKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9jb250ZW50UmVmLnZpZXdSZWYuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9jb250ZW50UmVmID0gbnVsbDtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLl93aW5kb3dDbXB0UmVmLmluc3RhbmNlLmFuaW1hdGlvbiA9ICdjbG9zZSc7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl9iYWNrZHJvcENtcHRSZWYpIHtcclxuICAgICAgICAgICAgdGhpcy5iYWNrZHJvcENsb3NpbmdEaWREb25lLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBiYWNrZHJvcE5hdGl2ZUVsID0gdGhpcy5fYmFja2Ryb3BDbXB0UmVmLmxvY2F0aW9uLm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICAgICAgICAgICAgICBiYWNrZHJvcE5hdGl2ZUVsLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoYmFja2Ryb3BOYXRpdmVFbCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9iYWNrZHJvcENtcHRSZWYuZGVzdHJveSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fYmFja2Ryb3BDbXB0UmVmID0gbnVsbDtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuX2JhY2tkcm9wQ21wdFJlZi5pbnN0YW5jZS5hbmltYXRpb24gPSAnY2xvc2UnO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbn1cclxuIl19