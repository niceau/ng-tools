/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtModalService } from './modal.service';
export class NgtModalCloseDirective {
    /**
     * @param {?} _modalService
     */
    constructor(_modalService) {
        this._modalService = _modalService;
    }
    /**
     * @return {?}
     */
    onClick() {
        this._modalService.closeById(this.id, this.result);
    }
}
NgtModalCloseDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtModalClose]'
            },] }
];
/** @nocollapse */
NgtModalCloseDirective.ctorParameters = () => [
    { type: NgtModalService }
];
NgtModalCloseDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtModalClose',] }],
    result: [{ type: Input }],
    onClick: [{ type: HostListener, args: ['click',] }]
};
if (false) {
    /** @type {?} */
    NgtModalCloseDirective.prototype.id;
    /** @type {?} */
    NgtModalCloseDirective.prototype.result;
    /**
     * @type {?}
     * @private
     */
    NgtModalCloseDirective.prototype._modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtY2xvc2UuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJtb2RhbC9tb2RhbC1jbG9zZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUvRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFLbEQsTUFBTSxPQUFPLHNCQUFzQjs7OztJQUkvQixZQUFvQixhQUE4QjtRQUE5QixrQkFBYSxHQUFiLGFBQWEsQ0FBaUI7SUFDbEQsQ0FBQzs7OztJQUdELE9BQU87UUFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN2RCxDQUFDOzs7WUFiSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGlCQUFpQjthQUM5Qjs7OztZQUpRLGVBQWU7OztpQkFNbkIsS0FBSyxTQUFDLGVBQWU7cUJBQ3JCLEtBQUs7c0JBS0wsWUFBWSxTQUFDLE9BQU87Ozs7SUFOckIsb0NBQW1DOztJQUNuQyx3Q0FBd0I7Ozs7O0lBRVosK0NBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBIb3N0TGlzdGVuZXIsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBOZ3RNb2RhbFNlcnZpY2UgfSBmcm9tICcuL21vZGFsLnNlcnZpY2UnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3RNb2RhbENsb3NlXSdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndE1vZGFsQ2xvc2VEaXJlY3RpdmUge1xyXG4gICAgQElucHV0KCduZ3RNb2RhbENsb3NlJykgaWQ6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIHJlc3VsdDogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX21vZGFsU2VydmljZTogTmd0TW9kYWxTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snKVxyXG4gICAgb25DbGljaygpIHtcclxuICAgICAgICB0aGlzLl9tb2RhbFNlcnZpY2UuY2xvc2VCeUlkKHRoaXMuaWQsIHRoaXMucmVzdWx0KTtcclxuICAgIH1cclxufVxyXG4iXX0=