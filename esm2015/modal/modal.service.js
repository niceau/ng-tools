/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Injector, ComponentFactoryResolver } from '@angular/core';
import * as _ from 'lodash';
import { NgtModalConfig } from './modal-config';
import { NgtModalStack } from './modal-stack';
import * as i0 from "@angular/core";
import * as i1 from "./modal-stack";
import * as i2 from "./modal-config";
/**
 * A service to open modal windows. Creating a modal is straightforward: create a template and pass it as an argument to
 * the "open" method!
 */
export class NgtModalService {
    /**
     * @param {?} _moduleCFR
     * @param {?} _injector
     * @param {?} _modalStack
     * @param {?} _config
     */
    constructor(_moduleCFR, _injector, _modalStack, _config) {
        this._moduleCFR = _moduleCFR;
        this._injector = _injector;
        this._modalStack = _modalStack;
        this._config = _config;
        this._components = [];
    }
    /**
     * Opens a new modal window with the specified content and using supplied options. Content can be provided
     * as a TemplateRef or a component type. If you pass a component type as content, then instances of those
     * components can be injected with an instance of the NgtActiveModal class. You can use methods on the
     * NgtActiveModal class to close / dismiss modals from "inside" of a component.
     * @param {?} content
     * @param {?=} options
     * @return {?}
     */
    open(content, options = {}) {
        /** @type {?} */
        const combinedOptions = Object.assign({}, this._config, options);
        return this._modalStack.open(this._moduleCFR, this._injector, content, combinedOptions);
    }
    /**
     * Dismiss all currently displayed modal windows with the supplied reason.
     * @param {?=} reason
     * @return {?}
     */
    dismissAll(reason) {
        this._modalStack.dismissAll(reason);
    }
    /**
     * Indicates if there are currently any open modal windows in the application.
     * @return {?}
     */
    hasOpenModals() {
        return this._modalStack.hasOpenModals();
    }
    /**
     * Add modal component instance to _components list.
     * !NOTE: modal must have id;
     * @param {?} componentRef
     * @return {?}
     */
    add(componentRef) {
        this._components.push(componentRef);
    }
    /**
     * Remove modal component instance from _components list.
     * @param {?} id
     * @return {?}
     */
    remove(id) {
        /** @type {?} */
        const modalToRemove = _.find(this._components, { id: id });
        this._components = _.without(this._components, modalToRemove);
    }
    /**
     * Opens a new modal window with the specified content and using supplied options founded in
     * _components list by specified id.
     * @param {?} id
     * @return {?}
     */
    openById(id) {
        /** @type {?} */
        const modalToOpen = _.find(this._components, { id: id });
        this.open(modalToOpen, modalToOpen.options);
    }
    /**
     * Call close method in modal component instance founded by specified id.
     * @param {?} id
     * @param {?=} result
     * @return {?}
     */
    closeById(id, result) {
        _.find(this._components, { id: id }).close(result);
    }
    /**
     * Call dismiss method in modal component instance founded by specified id.
     * @param {?} id
     * @param {?=} reason
     * @return {?}
     */
    dismissById(id, reason) {
        _.find(this._components, { id: id }).dismiss(reason);
    }
}
NgtModalService.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
NgtModalService.ctorParameters = () => [
    { type: ComponentFactoryResolver },
    { type: Injector },
    { type: NgtModalStack },
    { type: NgtModalConfig }
];
/** @nocollapse */ NgtModalService.ngInjectableDef = i0.defineInjectable({ factory: function NgtModalService_Factory() { return new NgtModalService(i0.inject(i0.ComponentFactoryResolver), i0.inject(i0.INJECTOR), i0.inject(i1.NgtModalStack), i0.inject(i2.NgtModalConfig)); }, token: NgtModalService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtModalService.prototype._components;
    /**
     * @type {?}
     * @private
     */
    NgtModalService.prototype._moduleCFR;
    /**
     * @type {?}
     * @private
     */
    NgtModalService.prototype._injector;
    /**
     * @type {?}
     * @private
     */
    NgtModalService.prototype._modalStack;
    /**
     * @type {?}
     * @private
     */
    NgtModalService.prototype._config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibW9kYWwvbW9kYWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFFNUIsT0FBTyxFQUFFLGNBQWMsRUFBbUIsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7OztBQVU5QyxNQUFNLE9BQU8sZUFBZTs7Ozs7OztJQUd4QixZQUNZLFVBQW9DLEVBQ3BDLFNBQW1CLEVBQ25CLFdBQTBCLEVBQzFCLE9BQXVCO1FBSHZCLGVBQVUsR0FBVixVQUFVLENBQTBCO1FBQ3BDLGNBQVMsR0FBVCxTQUFTLENBQVU7UUFDbkIsZ0JBQVcsR0FBWCxXQUFXLENBQWU7UUFDMUIsWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFOM0IsZ0JBQVcsR0FBd0IsRUFBRSxDQUFDO0lBTzlDLENBQUM7Ozs7Ozs7Ozs7SUFRRCxJQUFJLENBQUMsT0FBWSxFQUFFLFVBQTJCLEVBQUU7O2NBQ3RDLGVBQWUsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQztRQUNoRSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxPQUFPLEVBQUUsZUFBZSxDQUFDLENBQUM7SUFDNUYsQ0FBQzs7Ozs7O0lBS0QsVUFBVSxDQUFDLE1BQVk7UUFDbkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDeEMsQ0FBQzs7Ozs7SUFLRCxhQUFhO1FBQ1QsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQzVDLENBQUM7Ozs7Ozs7SUFNRCxHQUFHLENBQUMsWUFBK0I7UUFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDeEMsQ0FBQzs7Ozs7O0lBS0QsTUFBTSxDQUFDLEVBQVU7O2NBQ1AsYUFBYSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxhQUFhLENBQUMsQ0FBQztJQUNsRSxDQUFDOzs7Ozs7O0lBTUQsUUFBUSxDQUFDLEVBQVU7O2NBQ1QsV0FBVyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFDLEVBQUUsRUFBRSxFQUFFLEVBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDaEQsQ0FBQzs7Ozs7OztJQUtELFNBQVMsQ0FBQyxFQUFVLEVBQUUsTUFBTztRQUN6QixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBQyxFQUFFLEVBQUUsRUFBRSxFQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDckQsQ0FBQzs7Ozs7OztJQUtELFdBQVcsQ0FBQyxFQUFVLEVBQUUsTUFBTztRQUMzQixDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBQyxFQUFFLEVBQUUsRUFBRSxFQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDdkQsQ0FBQzs7O1lBekVKLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7Ozs7WUFiRCx3QkFBd0I7WUFBbEMsUUFBUTtZQUlwQixhQUFhO1lBRGIsY0FBYzs7Ozs7Ozs7SUFZbkIsc0NBQThDOzs7OztJQUcxQyxxQ0FBNEM7Ozs7O0lBQzVDLG9DQUEyQjs7Ozs7SUFDM0Isc0NBQWtDOzs7OztJQUNsQyxrQ0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuXHJcbmltcG9ydCB7IE5ndE1vZGFsQ29uZmlnLCBOZ3RNb2RhbE9wdGlvbnMgfSBmcm9tICcuL21vZGFsLWNvbmZpZyc7XHJcbmltcG9ydCB7IE5ndE1vZGFsU3RhY2sgfSBmcm9tICcuL21vZGFsLXN0YWNrJztcclxuaW1wb3J0IHsgTmd0TW9kYWxSZWYgfSBmcm9tICcuL21vZGFsLXJlZic7XHJcbmltcG9ydCB7IE5ndE1vZGFsQ29tcG9uZW50IH0gZnJvbSAnLi9tb2RhbC5jb21wb25lbnQnO1xyXG5cclxuXHJcbi8qKlxyXG4gKiBBIHNlcnZpY2UgdG8gb3BlbiBtb2RhbCB3aW5kb3dzLiBDcmVhdGluZyBhIG1vZGFsIGlzIHN0cmFpZ2h0Zm9yd2FyZDogY3JlYXRlIGEgdGVtcGxhdGUgYW5kIHBhc3MgaXQgYXMgYW4gYXJndW1lbnQgdG9cclxuICogdGhlIFwib3BlblwiIG1ldGhvZCFcclxuICovXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTmd0TW9kYWxTZXJ2aWNlIHtcclxuICAgIHByaXZhdGUgX2NvbXBvbmVudHM6IE5ndE1vZGFsQ29tcG9uZW50W10gPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF9tb2R1bGVDRlI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcclxuICAgICAgICBwcml2YXRlIF9pbmplY3RvcjogSW5qZWN0b3IsXHJcbiAgICAgICAgcHJpdmF0ZSBfbW9kYWxTdGFjazogTmd0TW9kYWxTdGFjayxcclxuICAgICAgICBwcml2YXRlIF9jb25maWc6IE5ndE1vZGFsQ29uZmlnKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBPcGVucyBhIG5ldyBtb2RhbCB3aW5kb3cgd2l0aCB0aGUgc3BlY2lmaWVkIGNvbnRlbnQgYW5kIHVzaW5nIHN1cHBsaWVkIG9wdGlvbnMuIENvbnRlbnQgY2FuIGJlIHByb3ZpZGVkXHJcbiAgICAgKiBhcyBhIFRlbXBsYXRlUmVmIG9yIGEgY29tcG9uZW50IHR5cGUuIElmIHlvdSBwYXNzIGEgY29tcG9uZW50IHR5cGUgYXMgY29udGVudCwgdGhlbiBpbnN0YW5jZXMgb2YgdGhvc2VcclxuICAgICAqIGNvbXBvbmVudHMgY2FuIGJlIGluamVjdGVkIHdpdGggYW4gaW5zdGFuY2Ugb2YgdGhlIE5ndEFjdGl2ZU1vZGFsIGNsYXNzLiBZb3UgY2FuIHVzZSBtZXRob2RzIG9uIHRoZVxyXG4gICAgICogTmd0QWN0aXZlTW9kYWwgY2xhc3MgdG8gY2xvc2UgLyBkaXNtaXNzIG1vZGFscyBmcm9tIFwiaW5zaWRlXCIgb2YgYSBjb21wb25lbnQuXHJcbiAgICAgKi9cclxuICAgIG9wZW4oY29udGVudDogYW55LCBvcHRpb25zOiBOZ3RNb2RhbE9wdGlvbnMgPSB7fSk6IE5ndE1vZGFsUmVmIHtcclxuICAgICAgICBjb25zdCBjb21iaW5lZE9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLl9jb25maWcsIG9wdGlvbnMpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9tb2RhbFN0YWNrLm9wZW4odGhpcy5fbW9kdWxlQ0ZSLCB0aGlzLl9pbmplY3RvciwgY29udGVudCwgY29tYmluZWRPcHRpb25zKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERpc21pc3MgYWxsIGN1cnJlbnRseSBkaXNwbGF5ZWQgbW9kYWwgd2luZG93cyB3aXRoIHRoZSBzdXBwbGllZCByZWFzb24uXHJcbiAgICAgKi9cclxuICAgIGRpc21pc3NBbGwocmVhc29uPzogYW55KSB7XHJcbiAgICAgICAgdGhpcy5fbW9kYWxTdGFjay5kaXNtaXNzQWxsKHJlYXNvbik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJbmRpY2F0ZXMgaWYgdGhlcmUgYXJlIGN1cnJlbnRseSBhbnkgb3BlbiBtb2RhbCB3aW5kb3dzIGluIHRoZSBhcHBsaWNhdGlvbi5cclxuICAgICAqL1xyXG4gICAgaGFzT3Blbk1vZGFscygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbW9kYWxTdGFjay5oYXNPcGVuTW9kYWxzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBZGQgbW9kYWwgY29tcG9uZW50IGluc3RhbmNlIHRvIF9jb21wb25lbnRzIGxpc3QuXHJcbiAgICAgKiAhTk9URTogbW9kYWwgbXVzdCBoYXZlIGlkO1xyXG4gICAgICovXHJcbiAgICBhZGQoY29tcG9uZW50UmVmOiBOZ3RNb2RhbENvbXBvbmVudCkge1xyXG4gICAgICAgIHRoaXMuX2NvbXBvbmVudHMucHVzaChjb21wb25lbnRSZWYpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVtb3ZlIG1vZGFsIGNvbXBvbmVudCBpbnN0YW5jZSBmcm9tIF9jb21wb25lbnRzIGxpc3QuXHJcbiAgICAgKi9cclxuICAgIHJlbW92ZShpZDogc3RyaW5nKSB7XHJcbiAgICAgICAgY29uc3QgbW9kYWxUb1JlbW92ZSA9IF8uZmluZCh0aGlzLl9jb21wb25lbnRzLCB7aWQ6IGlkfSk7XHJcbiAgICAgICAgdGhpcy5fY29tcG9uZW50cyA9IF8ud2l0aG91dCh0aGlzLl9jb21wb25lbnRzLCBtb2RhbFRvUmVtb3ZlKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE9wZW5zIGEgbmV3IG1vZGFsIHdpbmRvdyB3aXRoIHRoZSBzcGVjaWZpZWQgY29udGVudCBhbmQgdXNpbmcgc3VwcGxpZWQgb3B0aW9ucyBmb3VuZGVkIGluXHJcbiAgICAgKiBfY29tcG9uZW50cyBsaXN0IGJ5IHNwZWNpZmllZCBpZC5cclxuICAgICAqL1xyXG4gICAgb3BlbkJ5SWQoaWQ6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IG1vZGFsVG9PcGVuID0gXy5maW5kKHRoaXMuX2NvbXBvbmVudHMsIHtpZDogaWR9KTtcclxuICAgICAgICB0aGlzLm9wZW4obW9kYWxUb09wZW4sIG1vZGFsVG9PcGVuLm9wdGlvbnMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2FsbCBjbG9zZSBtZXRob2QgaW4gbW9kYWwgY29tcG9uZW50IGluc3RhbmNlIGZvdW5kZWQgYnkgc3BlY2lmaWVkIGlkLlxyXG4gICAgICovXHJcbiAgICBjbG9zZUJ5SWQoaWQ6IHN0cmluZywgcmVzdWx0Pykge1xyXG4gICAgICAgIF8uZmluZCh0aGlzLl9jb21wb25lbnRzLCB7aWQ6IGlkfSkuY2xvc2UocmVzdWx0KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENhbGwgZGlzbWlzcyBtZXRob2QgaW4gbW9kYWwgY29tcG9uZW50IGluc3RhbmNlIGZvdW5kZWQgYnkgc3BlY2lmaWVkIGlkLlxyXG4gICAgICovXHJcbiAgICBkaXNtaXNzQnlJZChpZDogc3RyaW5nLCByZWFzb24/KSB7XHJcbiAgICAgICAgXy5maW5kKHRoaXMuX2NvbXBvbmVudHMsIHtpZDogaWR9KS5kaXNtaXNzKHJlYXNvbik7XHJcbiAgICB9XHJcbn1cclxuIl19