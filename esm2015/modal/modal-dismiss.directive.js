/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtModalService } from './modal.service';
export class NgtModalDismissDirective {
    /**
     * @param {?} _modalService
     */
    constructor(_modalService) {
        this._modalService = _modalService;
    }
    /**
     * @return {?}
     */
    onClick() {
        this._modalService.dismissById(this.id, this.reason);
    }
}
NgtModalDismissDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtModalDismiss]'
            },] }
];
/** @nocollapse */
NgtModalDismissDirective.ctorParameters = () => [
    { type: NgtModalService }
];
NgtModalDismissDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtModalDismiss',] }],
    reason: [{ type: Input }],
    onClick: [{ type: HostListener, args: ['click',] }]
};
if (false) {
    /** @type {?} */
    NgtModalDismissDirective.prototype.id;
    /** @type {?} */
    NgtModalDismissDirective.prototype.reason;
    /**
     * @type {?}
     * @private
     */
    NgtModalDismissDirective.prototype._modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtZGlzbWlzcy5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbIm1vZGFsL21vZGFsLWRpc21pc3MuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBS2xELE1BQU0sT0FBTyx3QkFBd0I7Ozs7SUFJakMsWUFBb0IsYUFBOEI7UUFBOUIsa0JBQWEsR0FBYixhQUFhLENBQWlCO0lBQ2xELENBQUM7Ozs7SUFHRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDekQsQ0FBQzs7O1lBYkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxtQkFBbUI7YUFDaEM7Ozs7WUFKUSxlQUFlOzs7aUJBTW5CLEtBQUssU0FBQyxpQkFBaUI7cUJBQ3ZCLEtBQUs7c0JBS0wsWUFBWSxTQUFDLE9BQU87Ozs7SUFOckIsc0NBQXFDOztJQUNyQywwQ0FBd0I7Ozs7O0lBRVosaURBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRGlyZWN0aXZlLCBIb3N0TGlzdGVuZXIsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBOZ3RNb2RhbFNlcnZpY2UgfSBmcm9tICcuL21vZGFsLnNlcnZpY2UnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tuZ3RNb2RhbERpc21pc3NdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTmd0TW9kYWxEaXNtaXNzRGlyZWN0aXZlIHtcclxuICAgIEBJbnB1dCgnbmd0TW9kYWxEaXNtaXNzJykgaWQ6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIHJlYXNvbjogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX21vZGFsU2VydmljZTogTmd0TW9kYWxTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snKVxyXG4gICAgb25DbGljaygpIHtcclxuICAgICAgICB0aGlzLl9tb2RhbFNlcnZpY2UuZGlzbWlzc0J5SWQodGhpcy5pZCwgdGhpcy5yZWFzb24pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==