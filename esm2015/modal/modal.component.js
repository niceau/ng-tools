/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ElementRef, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { NgtModalService } from './modal.service';
import { NgtModalConfig } from './modal-config';
import { NgtActiveModal } from './modal-ref';
import { isDefined } from '../util/util';
export class NgtModalComponent {
    /**
     * @param {?} activeModal
     * @param {?} _modalService
     * @param {?} _config
     * @param {?} _elRef
     */
    constructor(activeModal, _modalService, _config, _elRef) {
        this.activeModal = activeModal;
        this._modalService = _modalService;
        this._config = _config;
        this._elRef = _elRef;
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.options = {};
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        // set options
        if (isDefined(this.backdrop)) {
            this.options.backdrop = this.backdrop;
        }
        if (isDefined(this.beforeDismiss)) {
            this.options.beforeDismiss = this.beforeDismiss;
        }
        if (isDefined(this.centered)) {
            this.options.centered = this.centered;
        }
        if (isDefined(this.container)) {
            this.options.container = this.container;
        }
        if (isDefined(this.keyboard)) {
            this.options.keyboard = this.keyboard;
        }
        if (isDefined(this.size)) {
            this.options.size = this.size;
        }
        if (isDefined(this.scrollableContent)) {
            this.options.scrollableContent = this.scrollableContent;
        }
        if (isDefined(this.windowClass)) {
            this.options.windowClass = this.windowClass;
        }
        if (isDefined(this.backdropClass)) {
            this.options.backdropClass = this.backdropClass;
        }
        // apply events
        this.activeModal.modalOpeningDidStart.subscribe((/**
         * @return {?}
         */
        () => this.modalOpeningDidStart.next()));
        this.activeModal.modalOpeningDidDone.subscribe((/**
         * @return {?}
         */
        () => this.modalOpeningDidDone.next()));
        this.activeModal.modalClosingDidStart.subscribe((/**
         * @return {?}
         */
        () => this.modalClosingDidStart.next()));
        this.activeModal.modalClosingDidDone.subscribe((/**
         * @return {?}
         */
        () => this.modalClosingDidDone.next()));
        this.activeModal.backdropOpeningDidStart.subscribe((/**
         * @return {?}
         */
        () => this.backdropOpeningDidStart.next()));
        this.activeModal.backdropOpeningDidDone.subscribe((/**
         * @return {?}
         */
        () => this.backdropOpeningDidDone.next()));
        this.activeModal.backdropClosingDidStart.subscribe((/**
         * @return {?}
         */
        () => this.backdropClosingDidStart.next()));
        this.activeModal.backdropClosingDidDone.subscribe((/**
         * @return {?}
         */
        () => this.backdropClosingDidDone.next()));
        if (!this.id) {
            // console.error('modal must have an id');
            return;
        }
        this._modalService.add(this);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this._modalService.remove(this.id);
    }
    /**
     * Open the modal with an modal component reference without id.
     * @return {?}
     */
    open() {
        this._modalService.open(this, this.options);
    }
    /**
     * Closes the modal with an optional 'result' value.
     * The 'NgtMobalRef.result' promise will be resolved with provided value.
     * @param {?=} result
     * @return {?}
     */
    close(result) {
        this.activeModal.close(result);
    }
    /**
     * Dismisses the modal with an optional 'reason' value.
     * The 'NgtModalRef.result' promise will be rejected with provided value.
     * @param {?=} reason
     * @return {?}
     */
    dismiss(reason) {
        this.activeModal.dismiss(reason);
    }
}
NgtModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-modal',
                template: `
        <ng-content></ng-content>
    `,
                providers: [NgtActiveModal]
            }] }
];
/** @nocollapse */
NgtModalComponent.ctorParameters = () => [
    { type: NgtActiveModal },
    { type: NgtModalService },
    { type: NgtModalConfig },
    { type: ElementRef }
];
NgtModalComponent.propDecorators = {
    id: [{ type: Input }],
    backdrop: [{ type: Input }],
    beforeDismiss: [{ type: Input }],
    centered: [{ type: Input }],
    container: [{ type: Input }],
    keyboard: [{ type: Input }],
    size: [{ type: Input }],
    scrollableContent: [{ type: Input }],
    windowClass: [{ type: Input }],
    backdropClass: [{ type: Input }],
    modalClosingDidStart: [{ type: Output }],
    modalClosingDidDone: [{ type: Output }],
    modalOpeningDidStart: [{ type: Output }],
    modalOpeningDidDone: [{ type: Output }],
    backdropClosingDidStart: [{ type: Output }],
    backdropClosingDidDone: [{ type: Output }],
    backdropOpeningDidStart: [{ type: Output }],
    backdropOpeningDidDone: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    NgtModalComponent.prototype.id;
    /** @type {?} */
    NgtModalComponent.prototype.backdrop;
    /** @type {?} */
    NgtModalComponent.prototype.beforeDismiss;
    /** @type {?} */
    NgtModalComponent.prototype.centered;
    /** @type {?} */
    NgtModalComponent.prototype.container;
    /** @type {?} */
    NgtModalComponent.prototype.keyboard;
    /** @type {?} */
    NgtModalComponent.prototype.size;
    /** @type {?} */
    NgtModalComponent.prototype.scrollableContent;
    /** @type {?} */
    NgtModalComponent.prototype.windowClass;
    /** @type {?} */
    NgtModalComponent.prototype.backdropClass;
    /** @type {?} */
    NgtModalComponent.prototype.modalClosingDidStart;
    /** @type {?} */
    NgtModalComponent.prototype.modalClosingDidDone;
    /** @type {?} */
    NgtModalComponent.prototype.modalOpeningDidStart;
    /** @type {?} */
    NgtModalComponent.prototype.modalOpeningDidDone;
    /** @type {?} */
    NgtModalComponent.prototype.backdropClosingDidStart;
    /** @type {?} */
    NgtModalComponent.prototype.backdropClosingDidDone;
    /** @type {?} */
    NgtModalComponent.prototype.backdropOpeningDidStart;
    /** @type {?} */
    NgtModalComponent.prototype.backdropOpeningDidDone;
    /** @type {?} */
    NgtModalComponent.prototype.options;
    /** @type {?} */
    NgtModalComponent.prototype.activeModal;
    /**
     * @type {?}
     * @private
     */
    NgtModalComponent.prototype._modalService;
    /**
     * @type {?}
     * @private
     */
    NgtModalComponent.prototype._config;
    /**
     * @type {?}
     * @private
     */
    NgtModalComponent.prototype._elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJtb2RhbC9tb2RhbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFDSCxTQUFTLEVBQ1QsS0FBSyxFQUdMLFVBQVUsRUFDVixNQUFNLEVBQ1QsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUUvQixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDbEQsT0FBTyxFQUFFLGNBQWMsRUFBbUIsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzdDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFVekMsTUFBTSxPQUFPLGlCQUFpQjs7Ozs7OztJQXFCMUIsWUFBbUIsV0FBMkIsRUFDMUIsYUFBOEIsRUFDOUIsT0FBdUIsRUFDdkIsTUFBa0I7UUFIbkIsZ0JBQVcsR0FBWCxXQUFXLENBQWdCO1FBQzFCLGtCQUFhLEdBQWIsYUFBYSxDQUFpQjtRQUM5QixZQUFPLEdBQVAsT0FBTyxDQUFnQjtRQUN2QixXQUFNLEdBQU4sTUFBTSxDQUFZO1FBYjVCLHlCQUFvQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDckMsd0JBQW1CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNwQyx5QkFBb0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3JDLHdCQUFtQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDcEMsNEJBQXVCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUN4QywyQkFBc0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3ZDLDRCQUF1QixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDeEMsMkJBQXNCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNqRCxZQUFPLEdBQW9CLEVBQUUsQ0FBQztJQU05QixDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLGNBQWM7UUFDZCxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztTQUN6QztRQUNELElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsRUFBRTtZQUMvQixJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1NBQ25EO1FBQ0QsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7U0FDekM7UUFDRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztTQUMzQztRQUNELElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1NBQ3pDO1FBQ0QsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDakM7UUFDRCxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsRUFBRTtZQUNuQyxJQUFJLENBQUMsT0FBTyxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztTQUMzRDtRQUNELElBQUksU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1NBQy9DO1FBQ0QsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxFQUFFO1lBQy9CLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7U0FDbkQ7UUFFRCxlQUFlO1FBQ2YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLEVBQUMsQ0FBQztRQUN4RixJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQixDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsRUFBQyxDQUFDO1FBQ3RGLElBQUksQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksRUFBRSxFQUFDLENBQUM7UUFDeEYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxFQUFFLEVBQUMsQ0FBQztRQUN0RixJQUFJLENBQUMsV0FBVyxDQUFDLHVCQUF1QixDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsRUFBQyxDQUFDO1FBQzlGLElBQUksQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksRUFBRSxFQUFDLENBQUM7UUFDNUYsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLEVBQUMsQ0FBQztRQUM5RixJQUFJLENBQUMsV0FBVyxDQUFDLHNCQUFzQixDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsRUFBQyxDQUFDO1FBRTVGLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ1YsMENBQTBDO1lBQzFDLE9BQU87U0FDVjtRQUNELElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7O0lBS0QsSUFBSTtRQUNBLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDaEQsQ0FBQzs7Ozs7OztJQU1ELEtBQUssQ0FBQyxNQUFZO1FBQ2QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Ozs7OztJQU1ELE9BQU8sQ0FBQyxNQUFZO1FBQ2hCLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3JDLENBQUM7OztZQTNHSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLFFBQVEsRUFBRTs7S0FFVDtnQkFDRCxTQUFTLEVBQUUsQ0FBQyxjQUFjLENBQUM7YUFDOUI7Ozs7WUFUUSxjQUFjO1lBRmQsZUFBZTtZQUNmLGNBQWM7WUFObkIsVUFBVTs7O2lCQW1CVCxLQUFLO3VCQUNMLEtBQUs7NEJBQ0wsS0FBSzt1QkFDTCxLQUFLO3dCQUNMLEtBQUs7dUJBQ0wsS0FBSzttQkFDTCxLQUFLO2dDQUNMLEtBQUs7MEJBQ0wsS0FBSzs0QkFDTCxLQUFLO21DQUNMLE1BQU07a0NBQ04sTUFBTTttQ0FDTixNQUFNO2tDQUNOLE1BQU07c0NBQ04sTUFBTTtxQ0FDTixNQUFNO3NDQUNOLE1BQU07cUNBQ04sTUFBTTs7OztJQWpCUCwrQkFBb0I7O0lBQ3BCLHFDQUFzQzs7SUFDdEMsMENBQXlEOztJQUN6RCxxQ0FBMkI7O0lBQzNCLHNDQUEyQjs7SUFDM0IscUNBQTJCOztJQUMzQixpQ0FBMkI7O0lBQzNCLDhDQUFvQzs7SUFDcEMsd0NBQTZCOztJQUM3QiwwQ0FBK0I7O0lBQy9CLGlEQUErQzs7SUFDL0MsZ0RBQThDOztJQUM5QyxpREFBK0M7O0lBQy9DLGdEQUE4Qzs7SUFDOUMsb0RBQWtEOztJQUNsRCxtREFBaUQ7O0lBQ2pELG9EQUFrRDs7SUFDbEQsbURBQWlEOztJQUNqRCxvQ0FBOEI7O0lBRWxCLHdDQUFrQzs7Ozs7SUFDbEMsMENBQXNDOzs7OztJQUN0QyxvQ0FBK0I7Ozs7O0lBQy9CLG1DQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBDb21wb25lbnQsXHJcbiAgICBJbnB1dCxcclxuICAgIE9uSW5pdCxcclxuICAgIE9uRGVzdHJveSxcclxuICAgIEVsZW1lbnRSZWYsXHJcbiAgICBPdXRwdXRcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgTmd0TW9kYWxTZXJ2aWNlIH0gZnJvbSAnLi9tb2RhbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTmd0TW9kYWxDb25maWcsIE5ndE1vZGFsT3B0aW9ucyB9IGZyb20gJy4vbW9kYWwtY29uZmlnJztcclxuaW1wb3J0IHsgTmd0QWN0aXZlTW9kYWwgfSBmcm9tICcuL21vZGFsLXJlZic7XHJcbmltcG9ydCB7IGlzRGVmaW5lZCB9IGZyb20gJy4uL3V0aWwvdXRpbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbmd0LW1vZGFsJyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxyXG4gICAgYCxcclxuICAgIHByb3ZpZGVyczogW05ndEFjdGl2ZU1vZGFsXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIE5ndE1vZGFsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gICAgQElucHV0KCkgaWQ6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIGJhY2tkcm9wOiBib29sZWFuIHwgJ3N0YXRpYyc7XHJcbiAgICBASW5wdXQoKSBiZWZvcmVEaXNtaXNzOiAoKSA9PiBib29sZWFuIHwgUHJvbWlzZTxib29sZWFuPjtcclxuICAgIEBJbnB1dCgpIGNlbnRlcmVkOiBib29sZWFuO1xyXG4gICAgQElucHV0KCkgY29udGFpbmVyOiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKSBrZXlib2FyZDogYm9vbGVhbjtcclxuICAgIEBJbnB1dCgpIHNpemU6ICdzbScgfCAnbGcnO1xyXG4gICAgQElucHV0KCkgc2Nyb2xsYWJsZUNvbnRlbnQ6IGJvb2xlYW47XHJcbiAgICBASW5wdXQoKSB3aW5kb3dDbGFzczogc3RyaW5nO1xyXG4gICAgQElucHV0KCkgYmFja2Ryb3BDbGFzczogc3RyaW5nO1xyXG4gICAgQE91dHB1dCgpIG1vZGFsQ2xvc2luZ0RpZFN0YXJ0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIEBPdXRwdXQoKSBtb2RhbENsb3NpbmdEaWREb25lID0gbmV3IFN1YmplY3QoKTtcclxuICAgIEBPdXRwdXQoKSBtb2RhbE9wZW5pbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBAT3V0cHV0KCkgbW9kYWxPcGVuaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBAT3V0cHV0KCkgYmFja2Ryb3BDbG9zaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgQE91dHB1dCgpIGJhY2tkcm9wQ2xvc2luZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgQE91dHB1dCgpIGJhY2tkcm9wT3BlbmluZ0RpZFN0YXJ0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIEBPdXRwdXQoKSBiYWNrZHJvcE9wZW5pbmdEaWREb25lID0gbmV3IFN1YmplY3QoKTtcclxuICAgIG9wdGlvbnM6IE5ndE1vZGFsT3B0aW9ucyA9IHt9O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBhY3RpdmVNb2RhbDogTmd0QWN0aXZlTW9kYWwsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9tb2RhbFNlcnZpY2U6IE5ndE1vZGFsU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX2NvbmZpZzogTmd0TW9kYWxDb25maWcsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIF9lbFJlZjogRWxlbWVudFJlZikge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIC8vIHNldCBvcHRpb25zXHJcbiAgICAgICAgaWYgKGlzRGVmaW5lZCh0aGlzLmJhY2tkcm9wKSkge1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMuYmFja2Ryb3AgPSB0aGlzLmJhY2tkcm9wO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoaXNEZWZpbmVkKHRoaXMuYmVmb3JlRGlzbWlzcykpIHtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zLmJlZm9yZURpc21pc3MgPSB0aGlzLmJlZm9yZURpc21pc3M7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChpc0RlZmluZWQodGhpcy5jZW50ZXJlZCkpIHtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zLmNlbnRlcmVkID0gdGhpcy5jZW50ZXJlZDtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGlzRGVmaW5lZCh0aGlzLmNvbnRhaW5lcikpIHtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zLmNvbnRhaW5lciA9IHRoaXMuY29udGFpbmVyO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoaXNEZWZpbmVkKHRoaXMua2V5Ym9hcmQpKSB7XHJcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy5rZXlib2FyZCA9IHRoaXMua2V5Ym9hcmQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChpc0RlZmluZWQodGhpcy5zaXplKSkge1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMuc2l6ZSA9IHRoaXMuc2l6ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGlzRGVmaW5lZCh0aGlzLnNjcm9sbGFibGVDb250ZW50KSkge1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMuc2Nyb2xsYWJsZUNvbnRlbnQgPSB0aGlzLnNjcm9sbGFibGVDb250ZW50O1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoaXNEZWZpbmVkKHRoaXMud2luZG93Q2xhc3MpKSB7XHJcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucy53aW5kb3dDbGFzcyA9IHRoaXMud2luZG93Q2xhc3M7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChpc0RlZmluZWQodGhpcy5iYWNrZHJvcENsYXNzKSkge1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMuYmFja2Ryb3BDbGFzcyA9IHRoaXMuYmFja2Ryb3BDbGFzcztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGFwcGx5IGV2ZW50c1xyXG4gICAgICAgIHRoaXMuYWN0aXZlTW9kYWwubW9kYWxPcGVuaW5nRGlkU3RhcnQuc3Vic2NyaWJlKCgpID0+IHRoaXMubW9kYWxPcGVuaW5nRGlkU3RhcnQubmV4dCgpKTtcclxuICAgICAgICB0aGlzLmFjdGl2ZU1vZGFsLm1vZGFsT3BlbmluZ0RpZERvbmUuc3Vic2NyaWJlKCgpID0+IHRoaXMubW9kYWxPcGVuaW5nRGlkRG9uZS5uZXh0KCkpO1xyXG4gICAgICAgIHRoaXMuYWN0aXZlTW9kYWwubW9kYWxDbG9zaW5nRGlkU3RhcnQuc3Vic2NyaWJlKCgpID0+IHRoaXMubW9kYWxDbG9zaW5nRGlkU3RhcnQubmV4dCgpKTtcclxuICAgICAgICB0aGlzLmFjdGl2ZU1vZGFsLm1vZGFsQ2xvc2luZ0RpZERvbmUuc3Vic2NyaWJlKCgpID0+IHRoaXMubW9kYWxDbG9zaW5nRGlkRG9uZS5uZXh0KCkpO1xyXG4gICAgICAgIHRoaXMuYWN0aXZlTW9kYWwuYmFja2Ryb3BPcGVuaW5nRGlkU3RhcnQuc3Vic2NyaWJlKCgpID0+IHRoaXMuYmFja2Ryb3BPcGVuaW5nRGlkU3RhcnQubmV4dCgpKTtcclxuICAgICAgICB0aGlzLmFjdGl2ZU1vZGFsLmJhY2tkcm9wT3BlbmluZ0RpZERvbmUuc3Vic2NyaWJlKCgpID0+IHRoaXMuYmFja2Ryb3BPcGVuaW5nRGlkRG9uZS5uZXh0KCkpO1xyXG4gICAgICAgIHRoaXMuYWN0aXZlTW9kYWwuYmFja2Ryb3BDbG9zaW5nRGlkU3RhcnQuc3Vic2NyaWJlKCgpID0+IHRoaXMuYmFja2Ryb3BDbG9zaW5nRGlkU3RhcnQubmV4dCgpKTtcclxuICAgICAgICB0aGlzLmFjdGl2ZU1vZGFsLmJhY2tkcm9wQ2xvc2luZ0RpZERvbmUuc3Vic2NyaWJlKCgpID0+IHRoaXMuYmFja2Ryb3BDbG9zaW5nRGlkRG9uZS5uZXh0KCkpO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuaWQpIHtcclxuICAgICAgICAgICAgLy8gY29uc29sZS5lcnJvcignbW9kYWwgbXVzdCBoYXZlIGFuIGlkJyk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5fbW9kYWxTZXJ2aWNlLmFkZCh0aGlzKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9tb2RhbFNlcnZpY2UucmVtb3ZlKHRoaXMuaWQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT3BlbiB0aGUgbW9kYWwgd2l0aCBhbiBtb2RhbCBjb21wb25lbnQgcmVmZXJlbmNlIHdpdGhvdXQgaWQuXHJcbiAgICAgKi9cclxuICAgIG9wZW4oKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fbW9kYWxTZXJ2aWNlLm9wZW4odGhpcywgdGhpcy5vcHRpb25zKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENsb3NlcyB0aGUgbW9kYWwgd2l0aCBhbiBvcHRpb25hbCAncmVzdWx0JyB2YWx1ZS5cclxuICAgICAqIFRoZSAnTmd0TW9iYWxSZWYucmVzdWx0JyBwcm9taXNlIHdpbGwgYmUgcmVzb2x2ZWQgd2l0aCBwcm92aWRlZCB2YWx1ZS5cclxuICAgICAqL1xyXG4gICAgY2xvc2UocmVzdWx0PzogYW55KTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5hY3RpdmVNb2RhbC5jbG9zZShyZXN1bHQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGlzbWlzc2VzIHRoZSBtb2RhbCB3aXRoIGFuIG9wdGlvbmFsICdyZWFzb24nIHZhbHVlLlxyXG4gICAgICogVGhlICdOZ3RNb2RhbFJlZi5yZXN1bHQnIHByb21pc2Ugd2lsbCBiZSByZWplY3RlZCB3aXRoIHByb3ZpZGVkIHZhbHVlLlxyXG4gICAgICovXHJcbiAgICBkaXNtaXNzKHJlYXNvbj86IGFueSk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuYWN0aXZlTW9kYWwuZGlzbWlzcyhyZWFzb24pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==