/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, HostBinding, HostListener, Input, Output } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Subject } from 'rxjs';
export class NgtModalBackdropComponent {
    constructor() {
        this.backdropClosingDidStart = new Subject();
        this.backdropClosingDidDone = new Subject();
        this.backdropOpeningDidStart = new Subject();
        this.backdropOpeningDidDone = new Subject();
        this.zIndex = '1050';
        this.animation = 'start';
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onAnimationStart($event) {
        this.animationAction($event);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onAnimationDone($event) {
        this.animationAction($event);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.class = 'modal-backdrop fade show' + (this.backdropClass ? ' ' + this.backdropClass : '');
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    animationAction($event) {
        switch ($event.phaseName) {
            case 'start':
                switch ($event.toState) {
                    case 'start':
                        this.backdropOpeningDidStart.next();
                        break;
                    case 'close':
                        this.backdropClosingDidStart.next();
                        break;
                }
                break;
            case 'done':
                switch ($event.toState) {
                    case 'start':
                        this.backdropOpeningDidDone.next();
                        break;
                    case 'close':
                        this.backdropClosingDidDone.next();
                        break;
                }
                break;
        }
    }
}
NgtModalBackdropComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-modal-backdrop',
                template: '',
                animations: [
                    trigger('animation', [
                        state('close', style({ opacity: 0 })),
                        transition('void => *', [
                            style({ opacity: 0 }),
                            animate(200)
                        ]),
                        transition('* => void', [
                            animate(200, style({ opacity: 0 }))
                        ]),
                        transition('* => close', animate('0.3s'))
                    ])
                ]
            }] }
];
NgtModalBackdropComponent.propDecorators = {
    backdropClass: [{ type: Input }],
    backdropClosingDidStart: [{ type: Output, args: ['backdropClosingDidStart',] }],
    backdropClosingDidDone: [{ type: Output, args: ['backdropClosingDidDone',] }],
    backdropOpeningDidStart: [{ type: Output, args: ['backdropOpeningDidStart',] }],
    backdropOpeningDidDone: [{ type: Output, args: ['backdropOpeningDidDone',] }],
    class: [{ type: HostBinding, args: ['class',] }],
    zIndex: [{ type: HostBinding, args: ['style.z-index',] }],
    animation: [{ type: HostBinding, args: ['@animation',] }],
    onAnimationStart: [{ type: HostListener, args: ['@animation.start', ['$event'],] }],
    onAnimationDone: [{ type: HostListener, args: ['@animation.done', ['$event'],] }]
};
if (false) {
    /** @type {?} */
    NgtModalBackdropComponent.prototype.backdropClass;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.backdropClosingDidStart;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.backdropClosingDidDone;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.backdropOpeningDidStart;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.backdropOpeningDidDone;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.class;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.zIndex;
    /** @type {?} */
    NgtModalBackdropComponent.prototype.animation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtYmFja2Ryb3AuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJtb2RhbC9tb2RhbC1iYWNrZHJvcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQVUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzVGLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDakYsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQW1CL0IsTUFBTSxPQUFPLHlCQUF5QjtJQWpCdEM7UUFtQnVDLDRCQUF1QixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDekMsMkJBQXNCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUN0Qyw0QkFBdUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3pDLDJCQUFzQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFFM0MsV0FBTSxHQUFHLE1BQU0sQ0FBQztRQUNuQixjQUFTLEdBQUcsT0FBTyxDQUFDO0lBc0NuRCxDQUFDOzs7OztJQXBDaUQsZ0JBQWdCLENBQUMsTUFBTTtRQUNqRSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBRTRDLGVBQWUsQ0FBQyxNQUFNO1FBQy9ELElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDakMsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsS0FBSyxHQUFHLDBCQUEwQixHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ25HLENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLE1BQU07UUFDbEIsUUFBUSxNQUFNLENBQUMsU0FBUyxFQUFFO1lBQ3RCLEtBQUssT0FBTztnQkFDUixRQUFRLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0JBQ3BCLEtBQUssT0FBTzt3QkFDUixJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQ3BDLE1BQU07b0JBQ1YsS0FBSyxPQUFPO3dCQUNSLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDcEMsTUFBTTtpQkFDYjtnQkFDRCxNQUFNO1lBQ1YsS0FBSyxNQUFNO2dCQUNQLFFBQVEsTUFBTSxDQUFDLE9BQU8sRUFBRTtvQkFDcEIsS0FBSyxPQUFPO3dCQUNSLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDbkMsTUFBTTtvQkFDVixLQUFLLE9BQU87d0JBQ1IsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksRUFBRSxDQUFDO3dCQUNuQyxNQUFNO2lCQUNiO2dCQUNELE1BQU07U0FDYjtJQUNMLENBQUM7OztZQTlESixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osVUFBVSxFQUFFO29CQUNSLE9BQU8sQ0FBQyxXQUFXLEVBQUU7d0JBQ2pCLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7d0JBQ25DLFVBQVUsQ0FBQyxXQUFXLEVBQUU7NEJBQ3BCLEtBQUssQ0FBQyxFQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUMsQ0FBQzs0QkFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQzt5QkFDZixDQUFDO3dCQUNGLFVBQVUsQ0FBQyxXQUFXLEVBQUU7NEJBQ3BCLE9BQU8sQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUM7eUJBQ3BDLENBQUM7d0JBQ0YsVUFBVSxDQUFDLFlBQVksRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7cUJBQzVDLENBQUM7aUJBQ0w7YUFDSjs7OzRCQUVJLEtBQUs7c0NBQ0wsTUFBTSxTQUFDLHlCQUF5QjtxQ0FDaEMsTUFBTSxTQUFDLHdCQUF3QjtzQ0FDL0IsTUFBTSxTQUFDLHlCQUF5QjtxQ0FDaEMsTUFBTSxTQUFDLHdCQUF3QjtvQkFDL0IsV0FBVyxTQUFDLE9BQU87cUJBQ25CLFdBQVcsU0FBQyxlQUFlO3dCQUMzQixXQUFXLFNBQUMsWUFBWTsrQkFFeEIsWUFBWSxTQUFDLGtCQUFrQixFQUFFLENBQUMsUUFBUSxDQUFDOzhCQUkzQyxZQUFZLFNBQUMsaUJBQWlCLEVBQUUsQ0FBQyxRQUFRLENBQUM7Ozs7SUFiM0Msa0RBQStCOztJQUMvQiw0REFBMkU7O0lBQzNFLDJEQUF5RTs7SUFDekUsNERBQTJFOztJQUMzRSwyREFBeUU7O0lBQ3pFLDBDQUE0Qjs7SUFDNUIsMkNBQThDOztJQUM5Qyw4Q0FBK0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEhvc3RCaW5kaW5nLCBIb3N0TGlzdGVuZXIsIElucHV0LCBPbkluaXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBhbmltYXRlLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIHRyaWdnZXIgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ25ndC1tb2RhbC1iYWNrZHJvcCcsXHJcbiAgICB0ZW1wbGF0ZTogJycsXHJcbiAgICBhbmltYXRpb25zOiBbXHJcbiAgICAgICAgdHJpZ2dlcignYW5pbWF0aW9uJywgW1xyXG4gICAgICAgICAgICBzdGF0ZSgnY2xvc2UnLCBzdHlsZSh7b3BhY2l0eTogMH0pKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbigndm9pZCA9PiAqJywgW1xyXG4gICAgICAgICAgICAgICAgc3R5bGUoe29wYWNpdHk6IDB9KSxcclxuICAgICAgICAgICAgICAgIGFuaW1hdGUoMjAwKVxyXG4gICAgICAgICAgICBdKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbignKiA9PiB2b2lkJywgW1xyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZSgyMDAsIHN0eWxlKHtvcGFjaXR5OiAwfSkpXHJcbiAgICAgICAgICAgIF0pLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCcqID0+IGNsb3NlJywgYW5pbWF0ZSgnMC4zcycpKVxyXG4gICAgICAgIF0pXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RNb2RhbEJhY2tkcm9wQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgpIGJhY2tkcm9wQ2xhc3M6IHN0cmluZztcclxuICAgIEBPdXRwdXQoJ2JhY2tkcm9wQ2xvc2luZ0RpZFN0YXJ0JykgYmFja2Ryb3BDbG9zaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgQE91dHB1dCgnYmFja2Ryb3BDbG9zaW5nRGlkRG9uZScpIGJhY2tkcm9wQ2xvc2luZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgQE91dHB1dCgnYmFja2Ryb3BPcGVuaW5nRGlkU3RhcnQnKSBiYWNrZHJvcE9wZW5pbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBAT3V0cHV0KCdiYWNrZHJvcE9wZW5pbmdEaWREb25lJykgYmFja2Ryb3BPcGVuaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzJykgY2xhc3M7XHJcbiAgICBASG9zdEJpbmRpbmcoJ3N0eWxlLnotaW5kZXgnKSB6SW5kZXggPSAnMTA1MCc7XHJcbiAgICBASG9zdEJpbmRpbmcoJ0BhbmltYXRpb24nKSBhbmltYXRpb24gPSAnc3RhcnQnO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ0BhbmltYXRpb24uc3RhcnQnLCBbJyRldmVudCddKSBvbkFuaW1hdGlvblN0YXJ0KCRldmVudCkge1xyXG4gICAgICAgIHRoaXMuYW5pbWF0aW9uQWN0aW9uKCRldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignQGFuaW1hdGlvbi5kb25lJywgWyckZXZlbnQnXSkgb25BbmltYXRpb25Eb25lKCRldmVudCkge1xyXG4gICAgICAgIHRoaXMuYW5pbWF0aW9uQWN0aW9uKCRldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jbGFzcyA9ICdtb2RhbC1iYWNrZHJvcCBmYWRlIHNob3cnICsgKHRoaXMuYmFja2Ryb3BDbGFzcyA/ICcgJyArIHRoaXMuYmFja2Ryb3BDbGFzcyA6ICcnKTtcclxuICAgIH1cclxuXHJcbiAgICBhbmltYXRpb25BY3Rpb24oJGV2ZW50KSB7XHJcbiAgICAgICAgc3dpdGNoICgkZXZlbnQucGhhc2VOYW1lKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ3N0YXJ0JzpcclxuICAgICAgICAgICAgICAgIHN3aXRjaCAoJGV2ZW50LnRvU3RhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdzdGFydCc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYmFja2Ryb3BPcGVuaW5nRGlkU3RhcnQubmV4dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdjbG9zZSc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYmFja2Ryb3BDbG9zaW5nRGlkU3RhcnQubmV4dCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICdkb25lJzpcclxuICAgICAgICAgICAgICAgIHN3aXRjaCAoJGV2ZW50LnRvU3RhdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdzdGFydCc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYmFja2Ryb3BPcGVuaW5nRGlkRG9uZS5uZXh0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2Nsb3NlJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5iYWNrZHJvcENsb3NpbmdEaWREb25lLm5leHQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19