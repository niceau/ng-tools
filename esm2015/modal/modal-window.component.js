/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, HostBinding, HostListener, Inject, Input, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalDismissReasons } from './modal-dismiss-reasons';
import { animate, state, style, transition, trigger } from '@angular/animations';
export class NgtModalWindowComponent {
    /**
     * @param {?} _document
     * @param {?} _elRef
     */
    constructor(_document, _elRef) {
        this._document = _document;
        this._elRef = _elRef;
        this.backdrop = true;
        this.keyboard = true;
        this.dismissEvent = new Subject();
        this.modalClosingDidStart = new Subject();
        this.modalClosingDidDone = new Subject();
        this.modalOpeningDidStart = new Subject();
        this.modalOpeningDidDone = new Subject();
        this.animation = 'open';
        this.tabindex = '-1';
        this.ariaModal = true;
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onBackdropClick($event) {
        if (this.backdrop === true && this._elRef.nativeElement === $event.target) {
            this.dismiss(ModalDismissReasons.BACKDROP_CLICK);
        }
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onEscKey($event) {
        if (this.keyboard && !$event.defaultPrevented) {
            this.dismiss(ModalDismissReasons.ESC);
        }
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onAnimationStart($event) {
        this.animationAction($event);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onAnimationDone($event) {
        this.animationAction($event);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.class = 'modal fade show d-block' + (this.windowClass ? ' ' + this.windowClass : '');
    }
    /**
     * @param {?} reason
     * @return {?}
     */
    dismiss(reason) {
        this.dismissEvent.next(reason);
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    animationAction($event) {
        switch ($event.phaseName) {
            case 'start':
                switch ($event.toState) {
                    case 'open':
                        this.modalOpeningDidStart.next();
                        break;
                    case 'close':
                        this.modalClosingDidStart.next();
                        break;
                }
                break;
            case 'done':
                switch ($event.toState) {
                    case 'open':
                        this.modalOpeningDidDone.next();
                        break;
                    case 'close':
                        this.modalClosingDidDone.next();
                        break;
                }
                break;
        }
    }
}
NgtModalWindowComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngt-modal-window',
                template: `
        <div [class]="'modal-dialog' + (size ? ' modal-' + size : '') + (centered ? ' modal-dialog-centered' : '') + (scrollableContent ? ' modal-dialog-scrollable' : '')" role="document">
            <div class="modal-content">
                <ng-content></ng-content>
            </div>
        </div>
    `,
                animations: [
                    trigger('animation', [
                        state('close', style({ opacity: 0, transform: 'scale(0, 0)' })),
                        transition('void => *', [
                            style({ opacity: 0, transform: 'scale(0, 0)' }),
                            animate('0.3s cubic-bezier(0.680, -0.550, 0.265, 1.550)')
                        ]),
                        transition('* => close', animate('0.3s cubic-bezier(0.680, -0.550, 0.265, 1.550)'))
                    ])
                ]
            }] }
];
/** @nocollapse */
NgtModalWindowComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: ElementRef }
];
NgtModalWindowComponent.propDecorators = {
    backdrop: [{ type: Input }],
    centered: [{ type: Input }],
    keyboard: [{ type: Input }],
    size: [{ type: Input }],
    scrollableContent: [{ type: Input }],
    windowClass: [{ type: Input }],
    dialogClass: [{ type: Input }],
    dismissEvent: [{ type: Output }],
    modalClosingDidStart: [{ type: Output }],
    modalClosingDidDone: [{ type: Output }],
    modalOpeningDidStart: [{ type: Output }],
    modalOpeningDidDone: [{ type: Output }],
    animation: [{ type: HostBinding, args: ['@animation',] }],
    class: [{ type: HostBinding, args: ['class',] }],
    tabindex: [{ type: HostBinding, args: ['tabindex',] }],
    ariaModal: [{ type: HostBinding, args: ['attr.aria-modal.true',] }],
    onBackdropClick: [{ type: HostListener, args: ['click', ['$event'],] }],
    onEscKey: [{ type: HostListener, args: ['document:keyup.esc', ['$event'],] }],
    onAnimationStart: [{ type: HostListener, args: ['@animation.start', ['$event'],] }],
    onAnimationDone: [{ type: HostListener, args: ['@animation.done', ['$event'],] }]
};
if (false) {
    /** @type {?} */
    NgtModalWindowComponent.prototype.backdrop;
    /** @type {?} */
    NgtModalWindowComponent.prototype.centered;
    /** @type {?} */
    NgtModalWindowComponent.prototype.keyboard;
    /** @type {?} */
    NgtModalWindowComponent.prototype.size;
    /** @type {?} */
    NgtModalWindowComponent.prototype.scrollableContent;
    /** @type {?} */
    NgtModalWindowComponent.prototype.windowClass;
    /** @type {?} */
    NgtModalWindowComponent.prototype.dialogClass;
    /** @type {?} */
    NgtModalWindowComponent.prototype.dismissEvent;
    /** @type {?} */
    NgtModalWindowComponent.prototype.modalClosingDidStart;
    /** @type {?} */
    NgtModalWindowComponent.prototype.modalClosingDidDone;
    /** @type {?} */
    NgtModalWindowComponent.prototype.modalOpeningDidStart;
    /** @type {?} */
    NgtModalWindowComponent.prototype.modalOpeningDidDone;
    /** @type {?} */
    NgtModalWindowComponent.prototype.animation;
    /** @type {?} */
    NgtModalWindowComponent.prototype.class;
    /** @type {?} */
    NgtModalWindowComponent.prototype.tabindex;
    /** @type {?} */
    NgtModalWindowComponent.prototype.ariaModal;
    /**
     * @type {?}
     * @private
     */
    NgtModalWindowComponent.prototype._document;
    /**
     * @type {?}
     * @private
     */
    NgtModalWindowComponent.prototype._elRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtd2luZG93LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsibW9kYWwvbW9kYWwtd2luZG93LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFDSCxTQUFTLEVBQ1QsVUFBVSxFQUNWLFdBQVcsRUFBRSxZQUFZLEVBQ3pCLE1BQU0sRUFDTixLQUFLLEVBQ0wsTUFBTSxFQUNULE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDOUQsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQXNCakYsTUFBTSxPQUFPLHVCQUF1Qjs7Ozs7SUF5Q2hDLFlBQXNDLFNBQWMsRUFDaEMsTUFBK0I7UUFEYixjQUFTLEdBQVQsU0FBUyxDQUFLO1FBQ2hDLFdBQU0sR0FBTixNQUFNLENBQXlCO1FBeEMxQyxhQUFRLEdBQXFCLElBQUksQ0FBQztRQUVsQyxhQUFRLEdBQUcsSUFBSSxDQUFDO1FBTWYsaUJBQVksR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQzdCLHlCQUFvQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDckMsd0JBQW1CLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNwQyx5QkFBb0IsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3JDLHdCQUFtQixHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFFbkIsY0FBUyxHQUFHLE1BQU0sQ0FBQztRQUVyQixhQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ0osY0FBUyxHQUFHLElBQUksQ0FBQztJQXdCdEQsQ0FBQzs7Ozs7SUF0QmtDLGVBQWUsQ0FBQyxNQUFNO1FBQ3JELElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLEtBQUssTUFBTSxDQUFDLE1BQU0sRUFBRTtZQUN2RSxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ3BEO0lBQ0wsQ0FBQzs7Ozs7SUFFK0MsUUFBUSxDQUFDLE1BQU07UUFDM0QsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixFQUFFO1lBQzNDLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDekM7SUFDTCxDQUFDOzs7OztJQUU2QyxnQkFBZ0IsQ0FBQyxNQUFNO1FBQ2pFLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFNEMsZUFBZSxDQUFDLE1BQU07UUFDL0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7O0lBTUQsUUFBUTtRQUNKLElBQUksQ0FBQyxLQUFLLEdBQUcseUJBQXlCLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDOUYsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsTUFBTTtRQUNWLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLE1BQU07UUFDbEIsUUFBUSxNQUFNLENBQUMsU0FBUyxFQUFFO1lBQ3RCLEtBQUssT0FBTztnQkFDUixRQUFRLE1BQU0sQ0FBQyxPQUFPLEVBQUU7b0JBQ3BCLEtBQUssTUFBTTt3QkFDUCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLENBQUM7d0JBQ2pDLE1BQU07b0JBQ1YsS0FBSyxPQUFPO3dCQUNSLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDakMsTUFBTTtpQkFDYjtnQkFDRCxNQUFNO1lBQ1YsS0FBSyxNQUFNO2dCQUNQLFFBQVEsTUFBTSxDQUFDLE9BQU8sRUFBRTtvQkFDcEIsS0FBSyxNQUFNO3dCQUNQLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQzt3QkFDaEMsTUFBTTtvQkFDVixLQUFLLE9BQU87d0JBQ1IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksRUFBRSxDQUFDO3dCQUNoQyxNQUFNO2lCQUNiO2dCQUNELE1BQU07U0FDYjtJQUNMLENBQUM7OztZQWhHSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsUUFBUSxFQUFFOzs7Ozs7S0FNVDtnQkFDRCxVQUFVLEVBQUU7b0JBQ1IsT0FBTyxDQUFDLFdBQVcsRUFBRTt3QkFDakIsS0FBSyxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsRUFBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUMsQ0FBQyxDQUFDO3dCQUM3RCxVQUFVLENBQUMsV0FBVyxFQUFFOzRCQUNwQixLQUFLLENBQUMsRUFBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUMsQ0FBQzs0QkFDN0MsT0FBTyxDQUFDLGdEQUFnRCxDQUFDO3lCQUM1RCxDQUFDO3dCQUNGLFVBQVUsQ0FBQyxZQUFZLEVBQUUsT0FBTyxDQUFDLGdEQUFnRCxDQUFDLENBQUM7cUJBQ3RGLENBQUM7aUJBQ0w7YUFDSjs7Ozs0Q0EwQ2dCLE1BQU0sU0FBQyxRQUFRO1lBdkU1QixVQUFVOzs7dUJBZ0NULEtBQUs7dUJBQ0wsS0FBSzt1QkFDTCxLQUFLO21CQUNMLEtBQUs7Z0NBQ0wsS0FBSzswQkFDTCxLQUFLOzBCQUNMLEtBQUs7MkJBRUwsTUFBTTttQ0FDTixNQUFNO2tDQUNOLE1BQU07bUNBQ04sTUFBTTtrQ0FDTixNQUFNO3dCQUVOLFdBQVcsU0FBQyxZQUFZO29CQUN4QixXQUFXLFNBQUMsT0FBTzt1QkFDbkIsV0FBVyxTQUFDLFVBQVU7d0JBQ3RCLFdBQVcsU0FBQyxzQkFBc0I7OEJBRWxDLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7dUJBTWhDLFlBQVksU0FBQyxvQkFBb0IsRUFBRSxDQUFDLFFBQVEsQ0FBQzsrQkFNN0MsWUFBWSxTQUFDLGtCQUFrQixFQUFFLENBQUMsUUFBUSxDQUFDOzhCQUkzQyxZQUFZLFNBQUMsaUJBQWlCLEVBQUUsQ0FBQyxRQUFRLENBQUM7Ozs7SUFuQzNDLDJDQUEyQzs7SUFDM0MsMkNBQTBCOztJQUMxQiwyQ0FBeUI7O0lBQ3pCLHVDQUFzQjs7SUFDdEIsb0RBQW1DOztJQUNuQyw4Q0FBNkI7O0lBQzdCLDhDQUE2Qjs7SUFFN0IsK0NBQXVDOztJQUN2Qyx1REFBK0M7O0lBQy9DLHNEQUE4Qzs7SUFDOUMsdURBQStDOztJQUMvQyxzREFBOEM7O0lBRTlDLDRDQUE4Qzs7SUFDOUMsd0NBQTRCOztJQUM1QiwyQ0FBeUM7O0lBQ3pDLDRDQUFzRDs7Ozs7SUFzQjFDLDRDQUF3Qzs7Ozs7SUFDeEMseUNBQXVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRE9DVU1FTlQgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQge1xyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgRWxlbWVudFJlZixcclxuICAgIEhvc3RCaW5kaW5nLCBIb3N0TGlzdGVuZXIsXHJcbiAgICBJbmplY3QsXHJcbiAgICBJbnB1dCwgT25Jbml0LFxyXG4gICAgT3V0cHV0XHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTW9kYWxEaXNtaXNzUmVhc29ucyB9IGZyb20gJy4vbW9kYWwtZGlzbWlzcy1yZWFzb25zJztcclxuaW1wb3J0IHsgYW5pbWF0ZSwgc3RhdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCB0cmlnZ2VyIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbmd0LW1vZGFsLXdpbmRvdycsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxkaXYgW2NsYXNzXT1cIidtb2RhbC1kaWFsb2cnICsgKHNpemUgPyAnIG1vZGFsLScgKyBzaXplIDogJycpICsgKGNlbnRlcmVkID8gJyBtb2RhbC1kaWFsb2ctY2VudGVyZWQnIDogJycpICsgKHNjcm9sbGFibGVDb250ZW50ID8gJyBtb2RhbC1kaWFsb2ctc2Nyb2xsYWJsZScgOiAnJylcIiByb2xlPVwiZG9jdW1lbnRcIj5cclxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cIm1vZGFsLWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgIDxuZy1jb250ZW50PjwvbmctY29udGVudD5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICBgLFxyXG4gICAgYW5pbWF0aW9uczogW1xyXG4gICAgICAgIHRyaWdnZXIoJ2FuaW1hdGlvbicsIFtcclxuICAgICAgICAgICAgc3RhdGUoJ2Nsb3NlJywgc3R5bGUoe29wYWNpdHk6IDAsIHRyYW5zZm9ybTogJ3NjYWxlKDAsIDApJ30pKSxcclxuICAgICAgICAgICAgdHJhbnNpdGlvbigndm9pZCA9PiAqJywgW1xyXG4gICAgICAgICAgICAgICAgc3R5bGUoe29wYWNpdHk6IDAsIHRyYW5zZm9ybTogJ3NjYWxlKDAsIDApJ30pLFxyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZSgnMC4zcyBjdWJpYy1iZXppZXIoMC42ODAsIC0wLjU1MCwgMC4yNjUsIDEuNTUwKScpXHJcbiAgICAgICAgICAgIF0pLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCcqID0+IGNsb3NlJywgYW5pbWF0ZSgnMC4zcyBjdWJpYy1iZXppZXIoMC42ODAsIC0wLjU1MCwgMC4yNjUsIDEuNTUwKScpKVxyXG4gICAgICAgIF0pXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RNb2RhbFdpbmRvd0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KCkgYmFja2Ryb3A6IGJvb2xlYW4gfCBzdHJpbmcgPSB0cnVlO1xyXG4gICAgQElucHV0KCkgY2VudGVyZWQ6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIGtleWJvYXJkID0gdHJ1ZTtcclxuICAgIEBJbnB1dCgpIHNpemU6IHN0cmluZztcclxuICAgIEBJbnB1dCgpIHNjcm9sbGFibGVDb250ZW50OiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKSB3aW5kb3dDbGFzczogc3RyaW5nO1xyXG4gICAgQElucHV0KCkgZGlhbG9nQ2xhc3M6IHN0cmluZztcclxuXHJcbiAgICBAT3V0cHV0KCkgZGlzbWlzc0V2ZW50ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIEBPdXRwdXQoKSBtb2RhbENsb3NpbmdEaWRTdGFydCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBAT3V0cHV0KCkgbW9kYWxDbG9zaW5nRGlkRG9uZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICBAT3V0cHV0KCkgbW9kYWxPcGVuaW5nRGlkU3RhcnQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgQE91dHB1dCgpIG1vZGFsT3BlbmluZ0RpZERvbmUgPSBuZXcgU3ViamVjdCgpO1xyXG5cclxuICAgIEBIb3N0QmluZGluZygnQGFuaW1hdGlvbicpIGFuaW1hdGlvbiA9ICdvcGVuJztcclxuICAgIEBIb3N0QmluZGluZygnY2xhc3MnKSBjbGFzcztcclxuICAgIEBIb3N0QmluZGluZygndGFiaW5kZXgnKSB0YWJpbmRleCA9ICctMSc7XHJcbiAgICBASG9zdEJpbmRpbmcoJ2F0dHIuYXJpYS1tb2RhbC50cnVlJykgYXJpYU1vZGFsID0gdHJ1ZTtcclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdjbGljaycsIFsnJGV2ZW50J10pIG9uQmFja2Ryb3BDbGljaygkZXZlbnQpIHtcclxuICAgICAgICBpZiAodGhpcy5iYWNrZHJvcCA9PT0gdHJ1ZSAmJiB0aGlzLl9lbFJlZi5uYXRpdmVFbGVtZW50ID09PSAkZXZlbnQudGFyZ2V0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzbWlzcyhNb2RhbERpc21pc3NSZWFzb25zLkJBQ0tEUk9QX0NMSUNLKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6a2V5dXAuZXNjJywgWyckZXZlbnQnXSkgb25Fc2NLZXkoJGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMua2V5Ym9hcmQgJiYgISRldmVudC5kZWZhdWx0UHJldmVudGVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzbWlzcyhNb2RhbERpc21pc3NSZWFzb25zLkVTQyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ0BhbmltYXRpb24uc3RhcnQnLCBbJyRldmVudCddKSBvbkFuaW1hdGlvblN0YXJ0KCRldmVudCkge1xyXG4gICAgICAgIHRoaXMuYW5pbWF0aW9uQWN0aW9uKCRldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignQGFuaW1hdGlvbi5kb25lJywgWyckZXZlbnQnXSkgb25BbmltYXRpb25Eb25lKCRldmVudCkge1xyXG4gICAgICAgIHRoaXMuYW5pbWF0aW9uQWN0aW9uKCRldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoQEluamVjdChET0NVTUVOVCkgcHJpdmF0ZSBfZG9jdW1lbnQ6IGFueSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgX2VsUmVmOiBFbGVtZW50UmVmPEhUTUxFbGVtZW50Pikge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY2xhc3MgPSAnbW9kYWwgZmFkZSBzaG93IGQtYmxvY2snICsgKHRoaXMud2luZG93Q2xhc3MgPyAnICcgKyB0aGlzLndpbmRvd0NsYXNzIDogJycpO1xyXG4gICAgfVxyXG5cclxuICAgIGRpc21pc3MocmVhc29uKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5kaXNtaXNzRXZlbnQubmV4dChyZWFzb24pO1xyXG4gICAgfVxyXG5cclxuICAgIGFuaW1hdGlvbkFjdGlvbigkZXZlbnQpIHtcclxuICAgICAgICBzd2l0Y2ggKCRldmVudC5waGFzZU5hbWUpIHtcclxuICAgICAgICAgICAgY2FzZSAnc3RhcnQnOlxyXG4gICAgICAgICAgICAgICAgc3dpdGNoICgkZXZlbnQudG9TdGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ29wZW4nOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZGFsT3BlbmluZ0RpZFN0YXJ0Lm5leHQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnY2xvc2UnOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZGFsQ2xvc2luZ0RpZFN0YXJ0Lm5leHQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnZG9uZSc6XHJcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKCRldmVudC50b1N0YXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnb3Blbic6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubW9kYWxPcGVuaW5nRGlkRG9uZS5uZXh0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2Nsb3NlJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tb2RhbENsb3NpbmdEaWREb25lLm5leHQoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19