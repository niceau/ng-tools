/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Represent options available when opening new modal windows.
 * @record
 */
export function NgtModalOptions() { }
if (false) {
    /**
     * Whether a backdrop element should be created for a given modal (true by default).
     * Alternatively, specify 'static' for a backdrop which doesn't close the modal on click.
     * @type {?|undefined}
     */
    NgtModalOptions.prototype.backdrop;
    /**
     * Function called when a modal will be dismissed.
     * If this function returns false, the promise is resolved with false or the promise is rejected, the modal is not
     * dismissed.
     * @type {?|undefined}
     */
    NgtModalOptions.prototype.beforeDismiss;
    /**
     * To center the modal vertically (false by default).
     * @type {?|undefined}
     */
    NgtModalOptions.prototype.centered;
    /**
     * An element to which to attach newly opened modal windows.
     * @type {?|undefined}
     */
    NgtModalOptions.prototype.container;
    /**
     * Injector to use for modal content.
     * @type {?|undefined}
     */
    NgtModalOptions.prototype.injector;
    /**
     * Whether to close the modal when escape key is pressed (true by default).
     * @type {?|undefined}
     */
    NgtModalOptions.prototype.keyboard;
    /**
     * Size of a new modal window.
     * @type {?|undefined}
     */
    NgtModalOptions.prototype.size;
    /**
     * Scrollable content option
     * @type {?|undefined}
     */
    NgtModalOptions.prototype.scrollableContent;
    /**
     * Custom class to append to the modal window
     * @type {?|undefined}
     */
    NgtModalOptions.prototype.windowClass;
    /**
     * Custom class to append to the modal backdrop
     *
     * \@since 1.1.0
     * @type {?|undefined}
     */
    NgtModalOptions.prototype.backdropClass;
}
/**
 * Configuration object token for the NgtModal service.
 * You can provide this configuration, typically in your root module in order to provide default option values for every
 * modal.
 */
export class NgtModalConfig {
    constructor() {
        this.backdrop = true;
        this.keyboard = true;
    }
}
NgtModalConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtModalConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtModalConfig_Factory() { return new NgtModalConfig(); }, token: NgtModalConfig, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtModalConfig.prototype.backdrop;
    /** @type {?} */
    NgtModalConfig.prototype.keyboard;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJtb2RhbC9tb2RhbC1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQVksTUFBTSxlQUFlLENBQUM7Ozs7OztBQUtyRCxxQ0F1REM7Ozs7Ozs7SUFsREcsbUNBQThCOzs7Ozs7O0lBTzlCLHdDQUFpRDs7Ozs7SUFLakQsbUNBQW1COzs7OztJQUtuQixvQ0FBbUI7Ozs7O0lBS25CLG1DQUFvQjs7Ozs7SUFLcEIsbUNBQW1COzs7OztJQUtuQiwrQkFBbUI7Ozs7O0lBS25CLDRDQUE0Qjs7Ozs7SUFLNUIsc0NBQXFCOzs7Ozs7O0lBT3JCLHdDQUF1Qjs7Ozs7OztBQVMzQixNQUFNLE9BQU8sY0FBYztJQUQzQjtRQUVJLGFBQVEsR0FBdUIsSUFBSSxDQUFDO1FBQ3BDLGFBQVEsR0FBRyxJQUFJLENBQUM7S0FDbkI7OztZQUpBLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7Ozs7O0lBRTVCLGtDQUFvQzs7SUFDcEMsa0NBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0b3IgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbi8qKlxyXG4gKiBSZXByZXNlbnQgb3B0aW9ucyBhdmFpbGFibGUgd2hlbiBvcGVuaW5nIG5ldyBtb2RhbCB3aW5kb3dzLlxyXG4gKi9cclxuZXhwb3J0IGludGVyZmFjZSBOZ3RNb2RhbE9wdGlvbnMge1xyXG4gICAgLyoqXHJcbiAgICAgKiBXaGV0aGVyIGEgYmFja2Ryb3AgZWxlbWVudCBzaG91bGQgYmUgY3JlYXRlZCBmb3IgYSBnaXZlbiBtb2RhbCAodHJ1ZSBieSBkZWZhdWx0KS5cclxuICAgICAqIEFsdGVybmF0aXZlbHksIHNwZWNpZnkgJ3N0YXRpYycgZm9yIGEgYmFja2Ryb3Agd2hpY2ggZG9lc24ndCBjbG9zZSB0aGUgbW9kYWwgb24gY2xpY2suXHJcbiAgICAgKi9cclxuICAgIGJhY2tkcm9wPzogYm9vbGVhbiB8ICdzdGF0aWMnO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogRnVuY3Rpb24gY2FsbGVkIHdoZW4gYSBtb2RhbCB3aWxsIGJlIGRpc21pc3NlZC5cclxuICAgICAqIElmIHRoaXMgZnVuY3Rpb24gcmV0dXJucyBmYWxzZSwgdGhlIHByb21pc2UgaXMgcmVzb2x2ZWQgd2l0aCBmYWxzZSBvciB0aGUgcHJvbWlzZSBpcyByZWplY3RlZCwgdGhlIG1vZGFsIGlzIG5vdFxyXG4gICAgICogZGlzbWlzc2VkLlxyXG4gICAgICovXHJcbiAgICBiZWZvcmVEaXNtaXNzPzogKCkgPT4gYm9vbGVhbiB8IFByb21pc2U8Ym9vbGVhbj47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUbyBjZW50ZXIgdGhlIG1vZGFsIHZlcnRpY2FsbHkgKGZhbHNlIGJ5IGRlZmF1bHQpLlxyXG4gICAgICovXHJcbiAgICBjZW50ZXJlZD86IGJvb2xlYW47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBbiBlbGVtZW50IHRvIHdoaWNoIHRvIGF0dGFjaCBuZXdseSBvcGVuZWQgbW9kYWwgd2luZG93cy5cclxuICAgICAqL1xyXG4gICAgY29udGFpbmVyPzogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW5qZWN0b3IgdG8gdXNlIGZvciBtb2RhbCBjb250ZW50LlxyXG4gICAgICovXHJcbiAgICBpbmplY3Rvcj86IEluamVjdG9yO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogV2hldGhlciB0byBjbG9zZSB0aGUgbW9kYWwgd2hlbiBlc2NhcGUga2V5IGlzIHByZXNzZWQgKHRydWUgYnkgZGVmYXVsdCkuXHJcbiAgICAgKi9cclxuICAgIGtleWJvYXJkPzogYm9vbGVhbjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFNpemUgb2YgYSBuZXcgbW9kYWwgd2luZG93LlxyXG4gICAgICovXHJcbiAgICBzaXplPzogJ3NtJyB8ICdsZyc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTY3JvbGxhYmxlIGNvbnRlbnQgb3B0aW9uXHJcbiAgICAgKi9cclxuICAgIHNjcm9sbGFibGVDb250ZW50PzogYm9vbGVhbjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEN1c3RvbSBjbGFzcyB0byBhcHBlbmQgdG8gdGhlIG1vZGFsIHdpbmRvd1xyXG4gICAgICovXHJcbiAgICB3aW5kb3dDbGFzcz86IHN0cmluZztcclxuXHJcbiAgICAvKipcclxuICAgICAqIEN1c3RvbSBjbGFzcyB0byBhcHBlbmQgdG8gdGhlIG1vZGFsIGJhY2tkcm9wXHJcbiAgICAgKlxyXG4gICAgICogQHNpbmNlIDEuMS4wXHJcbiAgICAgKi9cclxuICAgIGJhY2tkcm9wQ2xhc3M/OiBzdHJpbmc7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBDb25maWd1cmF0aW9uIG9iamVjdCB0b2tlbiBmb3IgdGhlIE5ndE1vZGFsIHNlcnZpY2UuXHJcbiAqIFlvdSBjYW4gcHJvdmlkZSB0aGlzIGNvbmZpZ3VyYXRpb24sIHR5cGljYWxseSBpbiB5b3VyIHJvb3QgbW9kdWxlIGluIG9yZGVyIHRvIHByb3ZpZGUgZGVmYXVsdCBvcHRpb24gdmFsdWVzIGZvciBldmVyeVxyXG4gKiBtb2RhbC5cclxuICovXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTmd0TW9kYWxDb25maWcgaW1wbGVtZW50cyBOZ3RNb2RhbE9wdGlvbnMge1xyXG4gICAgYmFja2Ryb3A6IGJvb2xlYW4gfCAnc3RhdGljJyA9IHRydWU7XHJcbiAgICBrZXlib2FyZCA9IHRydWU7XHJcbn1cclxuIl19