/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ApplicationRef, Inject, Injectable, Injector, RendererFactory2, TemplateRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Subject } from 'rxjs';
import { NgtActiveModal, NgtModalRef } from './modal-ref';
import { ScrollBar } from '../util/scrollbar';
import { isDefined, isString } from '../util/util';
import { ngtFocusTrap } from '../util/focus-trap';
import { NgtModalWindowComponent } from './modal-window.component';
import { NgtModalBackdropComponent } from './modal-backdrop.component';
import { ContentRef } from '../util/popup';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "../util/scrollbar";
export class NgtModalStack {
    /**
     * @param {?} _applicationRef
     * @param {?} _injector
     * @param {?} _document
     * @param {?} _scrollBar
     * @param {?} _rendererFactory
     */
    constructor(_applicationRef, _injector, _document, _scrollBar, _rendererFactory) {
        this._applicationRef = _applicationRef;
        this._injector = _injector;
        this._document = _document;
        this._scrollBar = _scrollBar;
        this._rendererFactory = _rendererFactory;
        this._activeWindowCmptHasChanged = new Subject();
        this._ariaHiddenValues = new Map();
        this._modalRefs = [];
        this._backdropAttributes = ['backdropClass'];
        this._windowAttributes = ['backdrop', 'centered', 'keyboard', 'size', 'scrollableContent', 'windowClass'];
        this._backdropEvents = ['backdropOpeningDidStart', 'backdropOpeningDidDone', 'backdropClosingDidStart', 'backdropClosingDidDone'];
        this._windowEvents = ['modalOpeningDidStart', 'modalOpeningDidDone', 'modalClosingDidStart', 'modalClosingDidDone'];
        this._windowCmpts = [];
        // Trap focus on active WindowCmpt
        this._activeWindowCmptHasChanged.subscribe((/**
         * @return {?}
         */
        () => {
            if (this._windowCmpts.length) {
                /** @type {?} */
                const activeWindowCmpt = this._windowCmpts[this._windowCmpts.length - 1];
                ngtFocusTrap(activeWindowCmpt.location.nativeElement, this._activeWindowCmptHasChanged);
                this._revertAriaHidden();
                this._setAriaHidden(activeWindowCmpt.location.nativeElement);
            }
        }));
    }
    /**
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @param {?} options
     * @return {?}
     */
    open(moduleCFR, contentInjector, content, options) {
        /** @type {?} */
        const containerEl = isDefined(options.container) ? this._document.querySelector(options.container) : this._document.body;
        /** @type {?} */
        const renderer = this._rendererFactory.createRenderer(null, null);
        /** @type {?} */
        const revertPaddingForScrollBar = this._scrollBar.compensate();
        /** @type {?} */
        const removeBodyClass = (/**
         * @return {?}
         */
        () => {
            if (!this._modalRefs.length) {
                renderer.removeClass(this._document.body, 'modal-open');
            }
        });
        if (!containerEl) {
            throw new Error(`The specified modal container "${options.container || 'body'}" was not found in the DOM.`);
        }
        const { contentRef, activeModal } = this._getContentRef(moduleCFR, options.injector || contentInjector, content);
        /** @type {?} */
        const backdropCmptRef = options.backdrop !== false ? this._attachBackdrop(moduleCFR, containerEl) : null;
        /** @type {?} */
        const windowCmptRef = this._attachWindowComponent(moduleCFR, containerEl, contentRef);
        /** @type {?} */
        const ngtModalRef = new NgtModalRef(windowCmptRef, contentRef, backdropCmptRef, options.beforeDismiss);
        this._registerModalRef(ngtModalRef);
        this._registerWindowCmpt(windowCmptRef);
        ngtModalRef.result.then(revertPaddingForScrollBar, revertPaddingForScrollBar);
        ngtModalRef.result.then(removeBodyClass, removeBodyClass);
        activeModal.close = (/**
         * @param {?} result
         * @return {?}
         */
        (result) => {
            ngtModalRef.close(result);
        });
        activeModal.dismiss = (/**
         * @param {?} reason
         * @return {?}
         */
        (reason) => {
            ngtModalRef.dismiss(reason);
        });
        this._applyWindowOptions(windowCmptRef.instance, options);
        this._applyEvents(ngtModalRef, activeModal, this._windowEvents);
        if (this._modalRefs.length === 1) {
            renderer.addClass(this._document.body, 'modal-open');
        }
        if (backdropCmptRef && backdropCmptRef.instance) {
            this._applyBackdropOptions(backdropCmptRef.instance, options);
            this._applyEvents(ngtModalRef, activeModal, this._backdropEvents);
        }
        return ngtModalRef;
    }
    /**
     * @param {?=} reason
     * @return {?}
     */
    dismissAll(reason) {
        this._modalRefs.forEach((/**
         * @param {?} ngtModalRef
         * @return {?}
         */
        ngtModalRef => ngtModalRef.dismiss(reason)));
    }
    /**
     * @return {?}
     */
    hasOpenModals() {
        return this._modalRefs.length > 0;
    }
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @return {?}
     */
    _attachBackdrop(moduleCFR, containerEl) {
        /** @type {?} */
        const backdropFactory = moduleCFR.resolveComponentFactory(NgtModalBackdropComponent);
        /** @type {?} */
        const backdropCmptRef = backdropFactory.create(this._injector);
        this._applicationRef.attachView(backdropCmptRef.hostView);
        containerEl.appendChild(backdropCmptRef.location.nativeElement);
        return backdropCmptRef;
    }
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} containerEl
     * @param {?} contentRef
     * @return {?}
     */
    _attachWindowComponent(moduleCFR, containerEl, contentRef) {
        /** @type {?} */
        const windowFactory = moduleCFR.resolveComponentFactory(NgtModalWindowComponent);
        /** @type {?} */
        const windowCmptRef = windowFactory.create(this._injector, contentRef.nodes);
        this._applicationRef.attachView(windowCmptRef.hostView);
        containerEl.appendChild(windowCmptRef.location.nativeElement);
        return windowCmptRef;
    }
    /**
     * @private
     * @param {?} windowInstance
     * @param {?} options
     * @return {?}
     */
    _applyWindowOptions(windowInstance, options) {
        this._windowAttributes.forEach((/**
         * @param {?} optionName
         * @return {?}
         */
        (optionName) => {
            if (isDefined(options[optionName])) {
                windowInstance[optionName] = options[optionName];
            }
        }));
    }
    /**
     * @private
     * @param {?} backdropInstance
     * @param {?} options
     * @return {?}
     */
    _applyBackdropOptions(backdropInstance, options) {
        this._backdropAttributes.forEach((/**
         * @param {?} optionName
         * @return {?}
         */
        (optionName) => {
            if (isDefined(options[optionName])) {
                backdropInstance[optionName] = options[optionName];
            }
        }));
    }
    /**
     * @private
     * @param {?} instanceToSubscribe
     * @param {?} instanceToTrigger
     * @param {?} events
     * @return {?}
     */
    _applyEvents(instanceToSubscribe, instanceToTrigger, events) {
        events.forEach((/**
         * @param {?} eventName
         * @return {?}
         */
        (eventName) => {
            if (isDefined(instanceToSubscribe[eventName]) && isDefined(instanceToTrigger[eventName])) {
                instanceToSubscribe[eventName].subscribe((/**
                 * @return {?}
                 */
                () => instanceToTrigger[eventName].next()));
            }
        }));
    }
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @return {?}
     */
    _getContentRef(moduleCFR, contentInjector, content) {
        /** @type {?} */
        let activeModal = new NgtActiveModal();
        /** @type {?} */
        let contentRef;
        if (!content) {
            contentRef = new ContentRef([]);
        }
        else if (content instanceof TemplateRef) {
            contentRef = this._createFromTemplateRef(content, activeModal);
        }
        else if (isString(content)) {
            contentRef = this._createFromString(content);
        }
        else if (typeof content === 'function') {
            contentRef = this._createFromComponentConstructor(moduleCFR, contentInjector, content, activeModal);
        }
        else {
            contentRef = this._createFromComponentRef(content);
            activeModal = contentRef.componentRef.activeModal || activeModal;
        }
        return { contentRef, activeModal };
    }
    /**
     * @private
     * @param {?} content
     * @param {?} activeModal
     * @return {?}
     */
    _createFromTemplateRef(content, activeModal) {
        /** @type {?} */
        const context = {
            $implicit: activeModal,
            /**
             * @param {?} result
             * @return {?}
             */
            close(result) {
                activeModal.close(result);
            },
            /**
             * @param {?} reason
             * @return {?}
             */
            dismiss(reason) {
                activeModal.dismiss(reason);
            }
        };
        /** @type {?} */
        const viewRef = content.createEmbeddedView(context);
        this._applicationRef.attachView(viewRef);
        return new ContentRef([viewRef.rootNodes], viewRef);
    }
    /**
     * @private
     * @param {?} content
     * @return {?}
     */
    _createFromString(content) {
        /** @type {?} */
        const component = this._document.createTextNode(`${content}`);
        return new ContentRef([[component]]);
    }
    /**
     * @private
     * @param {?} moduleCFR
     * @param {?} contentInjector
     * @param {?} content
     * @param {?} context
     * @return {?}
     */
    _createFromComponentConstructor(moduleCFR, contentInjector, content, context) {
        /** @type {?} */
        const contentCmptFactory = moduleCFR.resolveComponentFactory(content);
        /** @type {?} */
        const modalContentInjector = Injector.create({ providers: [{ provide: NgtActiveModal, useValue: context }], parent: contentInjector });
        /** @type {?} */
        const componentRef = contentCmptFactory.create(modalContentInjector);
        this._applicationRef.attachView(componentRef.hostView);
        return new ContentRef([[componentRef.location.nativeElement]], componentRef.hostView, componentRef);
    }
    /**
     * @private
     * @param {?} componentRef
     * @return {?}
     */
    _createFromComponentRef(componentRef) {
        return new ContentRef([[componentRef._elRef.nativeElement]], null, componentRef);
    }
    /**
     * @private
     * @param {?} element
     * @return {?}
     */
    _setAriaHidden(element) {
        /** @type {?} */
        const parent = element.parentElement;
        if (parent && element !== this._document.body) {
            Array.from(parent.children).forEach((/**
             * @param {?} sibling
             * @return {?}
             */
            sibling => {
                if (sibling !== element && sibling.nodeName !== 'SCRIPT') {
                    this._ariaHiddenValues.set(sibling, sibling.getAttribute('aria-hidden'));
                    sibling.setAttribute('aria-hidden', 'true');
                }
            }));
            this._setAriaHidden(parent);
        }
    }
    /**
     * @private
     * @return {?}
     */
    _revertAriaHidden() {
        this._ariaHiddenValues.forEach((/**
         * @param {?} value
         * @param {?} element
         * @return {?}
         */
        (value, element) => {
            if (value) {
                element.setAttribute('aria-hidden', value);
            }
            else {
                element.removeAttribute('aria-hidden');
            }
        }));
        this._ariaHiddenValues.clear();
    }
    /**
     * @private
     * @param {?} ngtModalRef
     * @return {?}
     */
    _registerModalRef(ngtModalRef) {
        /** @type {?} */
        const unregisterModalRef = (/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const index = this._modalRefs.indexOf(ngtModalRef);
            if (index > -1) {
                this._modalRefs.splice(index, 1);
            }
        });
        this._modalRefs.push(ngtModalRef);
        ngtModalRef.result.then(unregisterModalRef, unregisterModalRef);
    }
    /**
     * @private
     * @param {?} ngtWindowCmpt
     * @return {?}
     */
    _registerWindowCmpt(ngtWindowCmpt) {
        this._windowCmpts.push(ngtWindowCmpt);
        this._activeWindowCmptHasChanged.next();
        ngtWindowCmpt.onDestroy((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const index = this._windowCmpts.indexOf(ngtWindowCmpt);
            if (index > -1) {
                this._windowCmpts.splice(index, 1);
                this._activeWindowCmptHasChanged.next();
            }
        }));
    }
}
NgtModalStack.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */
NgtModalStack.ctorParameters = () => [
    { type: ApplicationRef },
    { type: Injector },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
    { type: ScrollBar },
    { type: RendererFactory2 }
];
/** @nocollapse */ NgtModalStack.ngInjectableDef = i0.defineInjectable({ factory: function NgtModalStack_Factory() { return new NgtModalStack(i0.inject(i0.ApplicationRef), i0.inject(i0.INJECTOR), i0.inject(i1.DOCUMENT), i0.inject(i2.ScrollBar), i0.inject(i0.RendererFactory2)); }, token: NgtModalStack, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._activeWindowCmptHasChanged;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._ariaHiddenValues;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._modalRefs;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._backdropAttributes;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._windowAttributes;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._backdropEvents;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._windowEvents;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._windowCmpts;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._applicationRef;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._injector;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._document;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._scrollBar;
    /**
     * @type {?}
     * @private
     */
    NgtModalStack.prototype._rendererFactory;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtc3RhY2suanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbIm1vZGFsL21vZGFsLXN0YWNrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0gsY0FBYyxFQUdkLE1BQU0sRUFDTixVQUFVLEVBQ1YsUUFBUSxFQUNSLGdCQUFnQixFQUNoQixXQUFXLEVBQ2QsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLGNBQWMsRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDMUQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ25ELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNuRSxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN2RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7O0FBRzNDLE1BQU0sT0FBTyxhQUFhOzs7Ozs7OztJQVV0QixZQUNZLGVBQStCLEVBQy9CLFNBQW1CLEVBQ0QsU0FBYyxFQUNoQyxVQUFxQixFQUNyQixnQkFBa0M7UUFKbEMsb0JBQWUsR0FBZixlQUFlLENBQWdCO1FBQy9CLGNBQVMsR0FBVCxTQUFTLENBQVU7UUFDRCxjQUFTLEdBQVQsU0FBUyxDQUFLO1FBQ2hDLGVBQVUsR0FBVixVQUFVLENBQVc7UUFDckIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQWR0QyxnQ0FBMkIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQzVDLHNCQUFpQixHQUF5QixJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ3BELGVBQVUsR0FBa0IsRUFBRSxDQUFDO1FBQy9CLHdCQUFtQixHQUFHLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDeEMsc0JBQWlCLEdBQUcsQ0FBQyxVQUFVLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsbUJBQW1CLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDckcsb0JBQWUsR0FBRyxDQUFDLHlCQUF5QixFQUFFLHdCQUF3QixFQUFFLHlCQUF5QixFQUFFLHdCQUF3QixDQUFDLENBQUM7UUFDN0gsa0JBQWEsR0FBRyxDQUFDLHNCQUFzQixFQUFFLHFCQUFxQixFQUFFLHNCQUFzQixFQUFFLHFCQUFxQixDQUFDLENBQUM7UUFDL0csaUJBQVksR0FBNEMsRUFBRSxDQUFDO1FBUS9ELGtDQUFrQztRQUNsQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFO1lBQzVDLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7O3NCQUNwQixnQkFBZ0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztnQkFDeEUsWUFBWSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUM7Z0JBQ3hGLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2dCQUN6QixJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUNoRTtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7Ozs7SUFFRCxJQUFJLENBQUMsU0FBbUMsRUFBRSxlQUF5QixFQUFFLE9BQVksRUFBRSxPQUFPOztjQUNoRixXQUFXLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUk7O2NBQ2xILFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUM7O2NBRTNELHlCQUF5QixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFOztjQUN4RCxlQUFlOzs7UUFBRyxHQUFHLEVBQUU7WUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFO2dCQUN6QixRQUFRLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLFlBQVksQ0FBQyxDQUFDO2FBQzNEO1FBQ0wsQ0FBQyxDQUFBO1FBRUQsSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNkLE1BQU0sSUFBSSxLQUFLLENBQUMsa0NBQWtDLE9BQU8sQ0FBQyxTQUFTLElBQUksTUFBTSw2QkFBNkIsQ0FBQyxDQUFDO1NBQy9HO2NBRUssRUFBQyxVQUFVLEVBQUUsV0FBVyxFQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLFFBQVEsSUFBSSxlQUFlLEVBQUUsT0FBTyxDQUFDOztjQUV4RyxlQUFlLEdBQ2pCLE9BQU8sQ0FBQyxRQUFRLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTs7Y0FDOUUsYUFBYSxHQUEwQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUM7O2NBQ3RILFdBQVcsR0FBZ0IsSUFBSSxXQUFXLENBQUMsYUFBYSxFQUFFLFVBQVUsRUFBRSxlQUFlLEVBQUUsT0FBTyxDQUFDLGFBQWEsQ0FBQztRQUVuSCxJQUFJLENBQUMsaUJBQWlCLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDcEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3hDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLHlCQUF5QixFQUFFLHlCQUF5QixDQUFDLENBQUM7UUFDOUUsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBRTFELFdBQVcsQ0FBQyxLQUFLOzs7O1FBQUcsQ0FBQyxNQUFXLEVBQUUsRUFBRTtZQUNoQyxXQUFXLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQSxDQUFDO1FBQ0YsV0FBVyxDQUFDLE9BQU87Ozs7UUFBRyxDQUFDLE1BQVcsRUFBRSxFQUFFO1lBQ2xDLFdBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDaEMsQ0FBQyxDQUFBLENBQUM7UUFFRixJQUFJLENBQUMsbUJBQW1CLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUMxRCxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxXQUFXLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2hFLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQzlCLFFBQVEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsWUFBWSxDQUFDLENBQUM7U0FDeEQ7UUFFRCxJQUFJLGVBQWUsSUFBSSxlQUFlLENBQUMsUUFBUSxFQUFFO1lBQzdDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLFdBQVcsRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDckU7UUFDRCxPQUFPLFdBQVcsQ0FBQztJQUN2QixDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxNQUFZO1FBQ25CLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTzs7OztRQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsRUFBQyxDQUFDO0lBQ3hFLENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1QsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Ozs7OztJQUVPLGVBQWUsQ0FBQyxTQUFtQyxFQUFFLFdBQWdCOztjQUNuRSxlQUFlLEdBQUcsU0FBUyxDQUFDLHVCQUF1QixDQUFDLHlCQUF5QixDQUFDOztjQUM5RSxlQUFlLEdBQUcsZUFBZSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzlELElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMxRCxXQUFXLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDaEUsT0FBTyxlQUFlLENBQUM7SUFDM0IsQ0FBQzs7Ozs7Ozs7SUFFTyxzQkFBc0IsQ0FBQyxTQUFtQyxFQUFFLFdBQWdCLEVBQUUsVUFBZTs7Y0FFM0YsYUFBYSxHQUFHLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyx1QkFBdUIsQ0FBQzs7Y0FDMUUsYUFBYSxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUMsS0FBSyxDQUFDO1FBQzVFLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN4RCxXQUFXLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDOUQsT0FBTyxhQUFhLENBQUM7SUFDekIsQ0FBQzs7Ozs7OztJQUVPLG1CQUFtQixDQUFDLGNBQXVDLEVBQUUsT0FBZTtRQUNoRixJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTzs7OztRQUFDLENBQUMsVUFBa0IsRUFBRSxFQUFFO1lBQ2xELElBQUksU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFO2dCQUNoQyxjQUFjLENBQUMsVUFBVSxDQUFDLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ3BEO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7O0lBRU8scUJBQXFCLENBQUMsZ0JBQTJDLEVBQUUsT0FBZTtRQUN0RixJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTzs7OztRQUFDLENBQUMsVUFBa0IsRUFBRSxFQUFFO1lBQ3BELElBQUksU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsQ0FBQyxFQUFFO2dCQUNoQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDdEQ7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7O0lBRU8sWUFBWSxDQUFDLG1CQUF3QixFQUFFLGlCQUFzQixFQUFFLE1BQU07UUFDekUsTUFBTSxDQUFDLE9BQU87Ozs7UUFBQyxDQUFDLFNBQWlCLEVBQUUsRUFBRTtZQUNqQyxJQUFJLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFO2dCQUN0RixtQkFBbUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTOzs7Z0JBQUMsR0FBRyxFQUFFLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUMsQ0FBQzthQUN2RjtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7Ozs7SUFFTyxjQUFjLENBQUMsU0FBbUMsRUFBRSxlQUF5QixFQUFFLE9BQVk7O1lBQzNGLFdBQVcsR0FBRyxJQUFJLGNBQWMsRUFBRTs7WUFDbEMsVUFBVTtRQUNkLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDVixVQUFVLEdBQUcsSUFBSSxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDbkM7YUFBTSxJQUFJLE9BQU8sWUFBWSxXQUFXLEVBQUU7WUFDdkMsVUFBVSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLEVBQUUsV0FBVyxDQUFDLENBQUM7U0FDbEU7YUFBTSxJQUFJLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUMxQixVQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ2hEO2FBQU0sSUFBSSxPQUFPLE9BQU8sS0FBSyxVQUFVLEVBQUU7WUFDdEMsVUFBVSxHQUFHLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxTQUFTLEVBQUUsZUFBZSxFQUFFLE9BQU8sRUFBRSxXQUFXLENBQUMsQ0FBQztTQUN2RzthQUFNO1lBQ0gsVUFBVSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNuRCxXQUFXLEdBQUcsVUFBVSxDQUFDLFlBQVksQ0FBQyxXQUFXLElBQUksV0FBVyxDQUFDO1NBQ3BFO1FBQ0QsT0FBTyxFQUFDLFVBQVUsRUFBRSxXQUFXLEVBQUMsQ0FBQztJQUNyQyxDQUFDOzs7Ozs7O0lBRU8sc0JBQXNCLENBQUMsT0FBeUIsRUFBRSxXQUEyQjs7Y0FDM0UsT0FBTyxHQUFHO1lBQ1osU0FBUyxFQUFFLFdBQVc7Ozs7O1lBQ3RCLEtBQUssQ0FBQyxNQUFNO2dCQUNSLFdBQVcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDOUIsQ0FBQzs7Ozs7WUFDRCxPQUFPLENBQUMsTUFBTTtnQkFDVixXQUFXLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hDLENBQUM7U0FDSjs7Y0FDSyxPQUFPLEdBQUcsT0FBTyxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQztRQUNuRCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxPQUFPLElBQUksVUFBVSxDQUFDLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ3hELENBQUM7Ozs7OztJQUVPLGlCQUFpQixDQUFDLE9BQWU7O2NBQy9CLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLE9BQU8sRUFBRSxDQUFDO1FBQzdELE9BQU8sSUFBSSxVQUFVLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7Ozs7Ozs7SUFFTywrQkFBK0IsQ0FDbkMsU0FBbUMsRUFBRSxlQUF5QixFQUFFLE9BQVksRUFDNUUsT0FBdUI7O2NBQ2pCLGtCQUFrQixHQUFHLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLENBQUM7O2NBQy9ELG9CQUFvQixHQUN0QixRQUFRLENBQUMsTUFBTSxDQUFDLEVBQUMsU0FBUyxFQUFFLENBQUMsRUFBQyxPQUFPLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUMsQ0FBQyxFQUFFLE1BQU0sRUFBRSxlQUFlLEVBQUMsQ0FBQzs7Y0FDbkcsWUFBWSxHQUFHLGtCQUFrQixDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQztRQUNwRSxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDdkQsT0FBTyxJQUFJLFVBQVUsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUM7SUFDeEcsQ0FBQzs7Ozs7O0lBRU8sdUJBQXVCLENBQUMsWUFBaUI7UUFDN0MsT0FBTyxJQUFJLFVBQVUsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQyxFQUFFLElBQUksRUFBRSxZQUFZLENBQUMsQ0FBQztJQUNyRixDQUFDOzs7Ozs7SUFFTyxjQUFjLENBQUMsT0FBZ0I7O2NBQzdCLE1BQU0sR0FBRyxPQUFPLENBQUMsYUFBYTtRQUNwQyxJQUFJLE1BQU0sSUFBSSxPQUFPLEtBQUssSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUU7WUFDM0MsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTzs7OztZQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUMxQyxJQUFJLE9BQU8sS0FBSyxPQUFPLElBQUksT0FBTyxDQUFDLFFBQVEsS0FBSyxRQUFRLEVBQUU7b0JBQ3RELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQztvQkFDekUsT0FBTyxDQUFDLFlBQVksQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDLENBQUM7aUJBQy9DO1lBQ0wsQ0FBQyxFQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQy9CO0lBQ0wsQ0FBQzs7Ozs7SUFFTyxpQkFBaUI7UUFDckIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU87Ozs7O1FBQUMsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLEVBQUU7WUFDOUMsSUFBSSxLQUFLLEVBQUU7Z0JBQ1AsT0FBTyxDQUFDLFlBQVksQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDOUM7aUJBQU07Z0JBQ0gsT0FBTyxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsQ0FBQzthQUMxQztRQUNMLENBQUMsRUFBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ25DLENBQUM7Ozs7OztJQUVPLGlCQUFpQixDQUFDLFdBQXdCOztjQUN4QyxrQkFBa0I7OztRQUFHLEdBQUcsRUFBRTs7a0JBQ3RCLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUM7WUFDbEQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQ1osSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQ3BDO1FBQ0wsQ0FBQyxDQUFBO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDbEMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztJQUNwRSxDQUFDOzs7Ozs7SUFFTyxtQkFBbUIsQ0FBQyxhQUFvRDtRQUM1RSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFeEMsYUFBYSxDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRTs7a0JBQ25CLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7WUFDdEQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQ1osSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUNuQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDM0M7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7OztZQWhPSixVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7O1lBbkI1QixjQUFjO1lBS2QsUUFBUTs0Q0E0QkgsTUFBTSxTQUFDLFFBQVE7WUFyQmYsU0FBUztZQU5kLGdCQUFnQjs7Ozs7Ozs7SUFlaEIsb0RBQW9EOzs7OztJQUNwRCwwQ0FBNEQ7Ozs7O0lBQzVELG1DQUF1Qzs7Ozs7SUFDdkMsNENBQWdEOzs7OztJQUNoRCwwQ0FBNkc7Ozs7O0lBQzdHLHdDQUFxSTs7Ozs7SUFDckksc0NBQXVIOzs7OztJQUN2SCxxQ0FBbUU7Ozs7O0lBRy9ELHdDQUF1Qzs7Ozs7SUFDdkMsa0NBQTJCOzs7OztJQUMzQixrQ0FBd0M7Ozs7O0lBQ3hDLG1DQUE2Qjs7Ozs7SUFDN0IseUNBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICAgIEFwcGxpY2F0aW9uUmVmLFxyXG4gICAgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxyXG4gICAgQ29tcG9uZW50UmVmLFxyXG4gICAgSW5qZWN0LFxyXG4gICAgSW5qZWN0YWJsZSxcclxuICAgIEluamVjdG9yLFxyXG4gICAgUmVuZGVyZXJGYWN0b3J5MixcclxuICAgIFRlbXBsYXRlUmVmXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERPQ1VNRU5UIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBOZ3RBY3RpdmVNb2RhbCwgTmd0TW9kYWxSZWYgfSBmcm9tICcuL21vZGFsLXJlZic7XHJcbmltcG9ydCB7IFNjcm9sbEJhciB9IGZyb20gJy4uL3V0aWwvc2Nyb2xsYmFyJztcclxuaW1wb3J0IHsgaXNEZWZpbmVkLCBpc1N0cmluZyB9IGZyb20gJy4uL3V0aWwvdXRpbCc7XHJcbmltcG9ydCB7IG5ndEZvY3VzVHJhcCB9IGZyb20gJy4uL3V0aWwvZm9jdXMtdHJhcCc7XHJcbmltcG9ydCB7IE5ndE1vZGFsV2luZG93Q29tcG9uZW50IH0gZnJvbSAnLi9tb2RhbC13aW5kb3cuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTmd0TW9kYWxCYWNrZHJvcENvbXBvbmVudCB9IGZyb20gJy4vbW9kYWwtYmFja2Ryb3AuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29udGVudFJlZiB9IGZyb20gJy4uL3V0aWwvcG9wdXAnO1xyXG5cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RNb2RhbFN0YWNrIHtcclxuICAgIHByaXZhdGUgX2FjdGl2ZVdpbmRvd0NtcHRIYXNDaGFuZ2VkID0gbmV3IFN1YmplY3QoKTtcclxuICAgIHByaXZhdGUgX2FyaWFIaWRkZW5WYWx1ZXM6IE1hcDxFbGVtZW50LCBzdHJpbmc+ID0gbmV3IE1hcCgpO1xyXG4gICAgcHJpdmF0ZSBfbW9kYWxSZWZzOiBOZ3RNb2RhbFJlZltdID0gW107XHJcbiAgICBwcml2YXRlIF9iYWNrZHJvcEF0dHJpYnV0ZXMgPSBbJ2JhY2tkcm9wQ2xhc3MnXTtcclxuICAgIHByaXZhdGUgX3dpbmRvd0F0dHJpYnV0ZXMgPSBbJ2JhY2tkcm9wJywgJ2NlbnRlcmVkJywgJ2tleWJvYXJkJywgJ3NpemUnLCAnc2Nyb2xsYWJsZUNvbnRlbnQnLCAnd2luZG93Q2xhc3MnXTtcclxuICAgIHByaXZhdGUgX2JhY2tkcm9wRXZlbnRzID0gWydiYWNrZHJvcE9wZW5pbmdEaWRTdGFydCcsICdiYWNrZHJvcE9wZW5pbmdEaWREb25lJywgJ2JhY2tkcm9wQ2xvc2luZ0RpZFN0YXJ0JywgJ2JhY2tkcm9wQ2xvc2luZ0RpZERvbmUnXTtcclxuICAgIHByaXZhdGUgX3dpbmRvd0V2ZW50cyA9IFsnbW9kYWxPcGVuaW5nRGlkU3RhcnQnLCAnbW9kYWxPcGVuaW5nRGlkRG9uZScsICdtb2RhbENsb3NpbmdEaWRTdGFydCcsICdtb2RhbENsb3NpbmdEaWREb25lJ107XHJcbiAgICBwcml2YXRlIF93aW5kb3dDbXB0czogQ29tcG9uZW50UmVmPE5ndE1vZGFsV2luZG93Q29tcG9uZW50PltdID0gW107XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBfYXBwbGljYXRpb25SZWY6IEFwcGxpY2F0aW9uUmVmLFxyXG4gICAgICAgIHByaXZhdGUgX2luamVjdG9yOiBJbmplY3RvcixcclxuICAgICAgICBASW5qZWN0KERPQ1VNRU5UKSBwcml2YXRlIF9kb2N1bWVudDogYW55LFxyXG4gICAgICAgIHByaXZhdGUgX3Njcm9sbEJhcjogU2Nyb2xsQmFyLFxyXG4gICAgICAgIHByaXZhdGUgX3JlbmRlcmVyRmFjdG9yeTogUmVuZGVyZXJGYWN0b3J5Mikge1xyXG4gICAgICAgIC8vIFRyYXAgZm9jdXMgb24gYWN0aXZlIFdpbmRvd0NtcHRcclxuICAgICAgICB0aGlzLl9hY3RpdmVXaW5kb3dDbXB0SGFzQ2hhbmdlZC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5fd2luZG93Q21wdHMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBhY3RpdmVXaW5kb3dDbXB0ID0gdGhpcy5fd2luZG93Q21wdHNbdGhpcy5fd2luZG93Q21wdHMubGVuZ3RoIC0gMV07XHJcbiAgICAgICAgICAgICAgICBuZ3RGb2N1c1RyYXAoYWN0aXZlV2luZG93Q21wdC5sb2NhdGlvbi5uYXRpdmVFbGVtZW50LCB0aGlzLl9hY3RpdmVXaW5kb3dDbXB0SGFzQ2hhbmdlZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9yZXZlcnRBcmlhSGlkZGVuKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9zZXRBcmlhSGlkZGVuKGFjdGl2ZVdpbmRvd0NtcHQubG9jYXRpb24ubmF0aXZlRWxlbWVudCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvcGVuKG1vZHVsZUNGUjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb250ZW50SW5qZWN0b3I6IEluamVjdG9yLCBjb250ZW50OiBhbnksIG9wdGlvbnMpIHtcclxuICAgICAgICBjb25zdCBjb250YWluZXJFbCA9IGlzRGVmaW5lZChvcHRpb25zLmNvbnRhaW5lcikgPyB0aGlzLl9kb2N1bWVudC5xdWVyeVNlbGVjdG9yKG9wdGlvbnMuY29udGFpbmVyKSA6IHRoaXMuX2RvY3VtZW50LmJvZHk7XHJcbiAgICAgICAgY29uc3QgcmVuZGVyZXIgPSB0aGlzLl9yZW5kZXJlckZhY3RvcnkuY3JlYXRlUmVuZGVyZXIobnVsbCwgbnVsbCk7XHJcblxyXG4gICAgICAgIGNvbnN0IHJldmVydFBhZGRpbmdGb3JTY3JvbGxCYXIgPSB0aGlzLl9zY3JvbGxCYXIuY29tcGVuc2F0ZSgpO1xyXG4gICAgICAgIGNvbnN0IHJlbW92ZUJvZHlDbGFzcyA9ICgpID0+IHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLl9tb2RhbFJlZnMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICByZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLl9kb2N1bWVudC5ib2R5LCAnbW9kYWwtb3BlbicpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgaWYgKCFjb250YWluZXJFbCkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoYFRoZSBzcGVjaWZpZWQgbW9kYWwgY29udGFpbmVyIFwiJHtvcHRpb25zLmNvbnRhaW5lciB8fCAnYm9keSd9XCIgd2FzIG5vdCBmb3VuZCBpbiB0aGUgRE9NLmApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qge2NvbnRlbnRSZWYsIGFjdGl2ZU1vZGFsfSA9IHRoaXMuX2dldENvbnRlbnRSZWYobW9kdWxlQ0ZSLCBvcHRpb25zLmluamVjdG9yIHx8IGNvbnRlbnRJbmplY3RvciwgY29udGVudCk7XHJcblxyXG4gICAgICAgIGNvbnN0IGJhY2tkcm9wQ21wdFJlZjogQ29tcG9uZW50UmVmPE5ndE1vZGFsQmFja2Ryb3BDb21wb25lbnQ+ID1cclxuICAgICAgICAgICAgb3B0aW9ucy5iYWNrZHJvcCAhPT0gZmFsc2UgPyB0aGlzLl9hdHRhY2hCYWNrZHJvcChtb2R1bGVDRlIsIGNvbnRhaW5lckVsKSA6IG51bGw7XHJcbiAgICAgICAgY29uc3Qgd2luZG93Q21wdFJlZjogQ29tcG9uZW50UmVmPE5ndE1vZGFsV2luZG93Q29tcG9uZW50PiA9IHRoaXMuX2F0dGFjaFdpbmRvd0NvbXBvbmVudChtb2R1bGVDRlIsIGNvbnRhaW5lckVsLCBjb250ZW50UmVmKTtcclxuICAgICAgICBjb25zdCBuZ3RNb2RhbFJlZjogTmd0TW9kYWxSZWYgPSBuZXcgTmd0TW9kYWxSZWYod2luZG93Q21wdFJlZiwgY29udGVudFJlZiwgYmFja2Ryb3BDbXB0UmVmLCBvcHRpb25zLmJlZm9yZURpc21pc3MpO1xyXG5cclxuICAgICAgICB0aGlzLl9yZWdpc3Rlck1vZGFsUmVmKG5ndE1vZGFsUmVmKTtcclxuICAgICAgICB0aGlzLl9yZWdpc3RlcldpbmRvd0NtcHQod2luZG93Q21wdFJlZik7XHJcbiAgICAgICAgbmd0TW9kYWxSZWYucmVzdWx0LnRoZW4ocmV2ZXJ0UGFkZGluZ0ZvclNjcm9sbEJhciwgcmV2ZXJ0UGFkZGluZ0ZvclNjcm9sbEJhcik7XHJcbiAgICAgICAgbmd0TW9kYWxSZWYucmVzdWx0LnRoZW4ocmVtb3ZlQm9keUNsYXNzLCByZW1vdmVCb2R5Q2xhc3MpO1xyXG5cclxuICAgICAgICBhY3RpdmVNb2RhbC5jbG9zZSA9IChyZXN1bHQ6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICBuZ3RNb2RhbFJlZi5jbG9zZShyZXN1bHQpO1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgYWN0aXZlTW9kYWwuZGlzbWlzcyA9IChyZWFzb246IGFueSkgPT4ge1xyXG4gICAgICAgICAgICBuZ3RNb2RhbFJlZi5kaXNtaXNzKHJlYXNvbik7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdGhpcy5fYXBwbHlXaW5kb3dPcHRpb25zKHdpbmRvd0NtcHRSZWYuaW5zdGFuY2UsIG9wdGlvbnMpO1xyXG4gICAgICAgIHRoaXMuX2FwcGx5RXZlbnRzKG5ndE1vZGFsUmVmLCBhY3RpdmVNb2RhbCwgdGhpcy5fd2luZG93RXZlbnRzKTtcclxuICAgICAgICBpZiAodGhpcy5fbW9kYWxSZWZzLmxlbmd0aCA9PT0gMSkge1xyXG4gICAgICAgICAgICByZW5kZXJlci5hZGRDbGFzcyh0aGlzLl9kb2N1bWVudC5ib2R5LCAnbW9kYWwtb3BlbicpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGJhY2tkcm9wQ21wdFJlZiAmJiBiYWNrZHJvcENtcHRSZWYuaW5zdGFuY2UpIHtcclxuICAgICAgICAgICAgdGhpcy5fYXBwbHlCYWNrZHJvcE9wdGlvbnMoYmFja2Ryb3BDbXB0UmVmLmluc3RhbmNlLCBvcHRpb25zKTtcclxuICAgICAgICAgICAgdGhpcy5fYXBwbHlFdmVudHMobmd0TW9kYWxSZWYsIGFjdGl2ZU1vZGFsLCB0aGlzLl9iYWNrZHJvcEV2ZW50cyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBuZ3RNb2RhbFJlZjtcclxuICAgIH1cclxuXHJcbiAgICBkaXNtaXNzQWxsKHJlYXNvbj86IGFueSkge1xyXG4gICAgICAgIHRoaXMuX21vZGFsUmVmcy5mb3JFYWNoKG5ndE1vZGFsUmVmID0+IG5ndE1vZGFsUmVmLmRpc21pc3MocmVhc29uKSk7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzT3Blbk1vZGFscygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fbW9kYWxSZWZzLmxlbmd0aCA+IDA7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfYXR0YWNoQmFja2Ryb3AobW9kdWxlQ0ZSOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIGNvbnRhaW5lckVsOiBhbnkpOiBDb21wb25lbnRSZWY8Tmd0TW9kYWxCYWNrZHJvcENvbXBvbmVudD4ge1xyXG4gICAgICAgIGNvbnN0IGJhY2tkcm9wRmFjdG9yeSA9IG1vZHVsZUNGUi5yZXNvbHZlQ29tcG9uZW50RmFjdG9yeShOZ3RNb2RhbEJhY2tkcm9wQ29tcG9uZW50KTtcclxuICAgICAgICBjb25zdCBiYWNrZHJvcENtcHRSZWYgPSBiYWNrZHJvcEZhY3RvcnkuY3JlYXRlKHRoaXMuX2luamVjdG9yKTtcclxuICAgICAgICB0aGlzLl9hcHBsaWNhdGlvblJlZi5hdHRhY2hWaWV3KGJhY2tkcm9wQ21wdFJlZi5ob3N0Vmlldyk7XHJcbiAgICAgICAgY29udGFpbmVyRWwuYXBwZW5kQ2hpbGQoYmFja2Ryb3BDbXB0UmVmLmxvY2F0aW9uLm5hdGl2ZUVsZW1lbnQpO1xyXG4gICAgICAgIHJldHVybiBiYWNrZHJvcENtcHRSZWY7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfYXR0YWNoV2luZG93Q29tcG9uZW50KG1vZHVsZUNGUjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBjb250YWluZXJFbDogYW55LCBjb250ZW50UmVmOiBhbnkpOlxyXG4gICAgICAgIENvbXBvbmVudFJlZjxOZ3RNb2RhbFdpbmRvd0NvbXBvbmVudD4ge1xyXG4gICAgICAgIGNvbnN0IHdpbmRvd0ZhY3RvcnkgPSBtb2R1bGVDRlIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkoTmd0TW9kYWxXaW5kb3dDb21wb25lbnQpO1xyXG4gICAgICAgIGNvbnN0IHdpbmRvd0NtcHRSZWYgPSB3aW5kb3dGYWN0b3J5LmNyZWF0ZSh0aGlzLl9pbmplY3RvciwgY29udGVudFJlZi5ub2Rlcyk7XHJcbiAgICAgICAgdGhpcy5fYXBwbGljYXRpb25SZWYuYXR0YWNoVmlldyh3aW5kb3dDbXB0UmVmLmhvc3RWaWV3KTtcclxuICAgICAgICBjb250YWluZXJFbC5hcHBlbmRDaGlsZCh3aW5kb3dDbXB0UmVmLmxvY2F0aW9uLm5hdGl2ZUVsZW1lbnQpO1xyXG4gICAgICAgIHJldHVybiB3aW5kb3dDbXB0UmVmO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2FwcGx5V2luZG93T3B0aW9ucyh3aW5kb3dJbnN0YW5jZTogTmd0TW9kYWxXaW5kb3dDb21wb25lbnQsIG9wdGlvbnM6IE9iamVjdCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX3dpbmRvd0F0dHJpYnV0ZXMuZm9yRWFjaCgob3B0aW9uTmFtZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChpc0RlZmluZWQob3B0aW9uc1tvcHRpb25OYW1lXSkpIHtcclxuICAgICAgICAgICAgICAgIHdpbmRvd0luc3RhbmNlW29wdGlvbk5hbWVdID0gb3B0aW9uc1tvcHRpb25OYW1lXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2FwcGx5QmFja2Ryb3BPcHRpb25zKGJhY2tkcm9wSW5zdGFuY2U6IE5ndE1vZGFsQmFja2Ryb3BDb21wb25lbnQsIG9wdGlvbnM6IE9iamVjdCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX2JhY2tkcm9wQXR0cmlidXRlcy5mb3JFYWNoKChvcHRpb25OYW1lOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgaWYgKGlzRGVmaW5lZChvcHRpb25zW29wdGlvbk5hbWVdKSkge1xyXG4gICAgICAgICAgICAgICAgYmFja2Ryb3BJbnN0YW5jZVtvcHRpb25OYW1lXSA9IG9wdGlvbnNbb3B0aW9uTmFtZV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9hcHBseUV2ZW50cyhpbnN0YW5jZVRvU3Vic2NyaWJlOiBhbnksIGluc3RhbmNlVG9UcmlnZ2VyOiBhbnksIGV2ZW50cyk6IHZvaWQge1xyXG4gICAgICAgIGV2ZW50cy5mb3JFYWNoKChldmVudE5hbWU6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICBpZiAoaXNEZWZpbmVkKGluc3RhbmNlVG9TdWJzY3JpYmVbZXZlbnROYW1lXSkgJiYgaXNEZWZpbmVkKGluc3RhbmNlVG9UcmlnZ2VyW2V2ZW50TmFtZV0pKSB7XHJcbiAgICAgICAgICAgICAgICBpbnN0YW5jZVRvU3Vic2NyaWJlW2V2ZW50TmFtZV0uc3Vic2NyaWJlKCgpID0+IGluc3RhbmNlVG9UcmlnZ2VyW2V2ZW50TmFtZV0ubmV4dCgpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2dldENvbnRlbnRSZWYobW9kdWxlQ0ZSOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsIGNvbnRlbnRJbmplY3RvcjogSW5qZWN0b3IsIGNvbnRlbnQ6IGFueSk6IHsgY29udGVudFJlZjogQ29udGVudFJlZiwgYWN0aXZlTW9kYWw6IE5ndEFjdGl2ZU1vZGFsIH0ge1xyXG4gICAgICAgIGxldCBhY3RpdmVNb2RhbCA9IG5ldyBOZ3RBY3RpdmVNb2RhbCgpO1xyXG4gICAgICAgIGxldCBjb250ZW50UmVmO1xyXG4gICAgICAgIGlmICghY29udGVudCkge1xyXG4gICAgICAgICAgICBjb250ZW50UmVmID0gbmV3IENvbnRlbnRSZWYoW10pO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoY29udGVudCBpbnN0YW5jZW9mIFRlbXBsYXRlUmVmKSB7XHJcbiAgICAgICAgICAgIGNvbnRlbnRSZWYgPSB0aGlzLl9jcmVhdGVGcm9tVGVtcGxhdGVSZWYoY29udGVudCwgYWN0aXZlTW9kYWwpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXNTdHJpbmcoY29udGVudCkpIHtcclxuICAgICAgICAgICAgY29udGVudFJlZiA9IHRoaXMuX2NyZWF0ZUZyb21TdHJpbmcoY29udGVudCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0eXBlb2YgY29udGVudCA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgICAgICBjb250ZW50UmVmID0gdGhpcy5fY3JlYXRlRnJvbUNvbXBvbmVudENvbnN0cnVjdG9yKG1vZHVsZUNGUiwgY29udGVudEluamVjdG9yLCBjb250ZW50LCBhY3RpdmVNb2RhbCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29udGVudFJlZiA9IHRoaXMuX2NyZWF0ZUZyb21Db21wb25lbnRSZWYoY29udGVudCk7XHJcbiAgICAgICAgICAgIGFjdGl2ZU1vZGFsID0gY29udGVudFJlZi5jb21wb25lbnRSZWYuYWN0aXZlTW9kYWwgfHwgYWN0aXZlTW9kYWw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB7Y29udGVudFJlZiwgYWN0aXZlTW9kYWx9O1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2NyZWF0ZUZyb21UZW1wbGF0ZVJlZihjb250ZW50OiBUZW1wbGF0ZVJlZjxhbnk+LCBhY3RpdmVNb2RhbDogTmd0QWN0aXZlTW9kYWwpOiBDb250ZW50UmVmIHtcclxuICAgICAgICBjb25zdCBjb250ZXh0ID0ge1xyXG4gICAgICAgICAgICAkaW1wbGljaXQ6IGFjdGl2ZU1vZGFsLFxyXG4gICAgICAgICAgICBjbG9zZShyZXN1bHQpIHtcclxuICAgICAgICAgICAgICAgIGFjdGl2ZU1vZGFsLmNsb3NlKHJlc3VsdCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGRpc21pc3MocmVhc29uKSB7XHJcbiAgICAgICAgICAgICAgICBhY3RpdmVNb2RhbC5kaXNtaXNzKHJlYXNvbik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgICAgIGNvbnN0IHZpZXdSZWYgPSBjb250ZW50LmNyZWF0ZUVtYmVkZGVkVmlldyhjb250ZXh0KTtcclxuICAgICAgICB0aGlzLl9hcHBsaWNhdGlvblJlZi5hdHRhY2hWaWV3KHZpZXdSZWYpO1xyXG4gICAgICAgIHJldHVybiBuZXcgQ29udGVudFJlZihbdmlld1JlZi5yb290Tm9kZXNdLCB2aWV3UmVmKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9jcmVhdGVGcm9tU3RyaW5nKGNvbnRlbnQ6IHN0cmluZyk6IENvbnRlbnRSZWYge1xyXG4gICAgICAgIGNvbnN0IGNvbXBvbmVudCA9IHRoaXMuX2RvY3VtZW50LmNyZWF0ZVRleHROb2RlKGAke2NvbnRlbnR9YCk7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBDb250ZW50UmVmKFtbY29tcG9uZW50XV0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgX2NyZWF0ZUZyb21Db21wb25lbnRDb25zdHJ1Y3RvcihcclxuICAgICAgICBtb2R1bGVDRlI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlciwgY29udGVudEluamVjdG9yOiBJbmplY3RvciwgY29udGVudDogYW55LFxyXG4gICAgICAgIGNvbnRleHQ6IE5ndEFjdGl2ZU1vZGFsKTogQ29udGVudFJlZiB7XHJcbiAgICAgICAgY29uc3QgY29udGVudENtcHRGYWN0b3J5ID0gbW9kdWxlQ0ZSLnJlc29sdmVDb21wb25lbnRGYWN0b3J5KGNvbnRlbnQpO1xyXG4gICAgICAgIGNvbnN0IG1vZGFsQ29udGVudEluamVjdG9yID1cclxuICAgICAgICAgICAgSW5qZWN0b3IuY3JlYXRlKHtwcm92aWRlcnM6IFt7cHJvdmlkZTogTmd0QWN0aXZlTW9kYWwsIHVzZVZhbHVlOiBjb250ZXh0fV0sIHBhcmVudDogY29udGVudEluamVjdG9yfSk7XHJcbiAgICAgICAgY29uc3QgY29tcG9uZW50UmVmID0gY29udGVudENtcHRGYWN0b3J5LmNyZWF0ZShtb2RhbENvbnRlbnRJbmplY3Rvcik7XHJcbiAgICAgICAgdGhpcy5fYXBwbGljYXRpb25SZWYuYXR0YWNoVmlldyhjb21wb25lbnRSZWYuaG9zdFZpZXcpO1xyXG4gICAgICAgIHJldHVybiBuZXcgQ29udGVudFJlZihbW2NvbXBvbmVudFJlZi5sb2NhdGlvbi5uYXRpdmVFbGVtZW50XV0sIGNvbXBvbmVudFJlZi5ob3N0VmlldywgY29tcG9uZW50UmVmKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9jcmVhdGVGcm9tQ29tcG9uZW50UmVmKGNvbXBvbmVudFJlZjogYW55KSB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBDb250ZW50UmVmKFtbY29tcG9uZW50UmVmLl9lbFJlZi5uYXRpdmVFbGVtZW50XV0sIG51bGwsIGNvbXBvbmVudFJlZik7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBfc2V0QXJpYUhpZGRlbihlbGVtZW50OiBFbGVtZW50KSB7XHJcbiAgICAgICAgY29uc3QgcGFyZW50ID0gZWxlbWVudC5wYXJlbnRFbGVtZW50O1xyXG4gICAgICAgIGlmIChwYXJlbnQgJiYgZWxlbWVudCAhPT0gdGhpcy5fZG9jdW1lbnQuYm9keSkge1xyXG4gICAgICAgICAgICBBcnJheS5mcm9tKHBhcmVudC5jaGlsZHJlbikuZm9yRWFjaChzaWJsaW5nID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChzaWJsaW5nICE9PSBlbGVtZW50ICYmIHNpYmxpbmcubm9kZU5hbWUgIT09ICdTQ1JJUFQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fYXJpYUhpZGRlblZhbHVlcy5zZXQoc2libGluZywgc2libGluZy5nZXRBdHRyaWJ1dGUoJ2FyaWEtaGlkZGVuJykpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNpYmxpbmcuc2V0QXR0cmlidXRlKCdhcmlhLWhpZGRlbicsICd0cnVlJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5fc2V0QXJpYUhpZGRlbihwYXJlbnQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9yZXZlcnRBcmlhSGlkZGVuKCkge1xyXG4gICAgICAgIHRoaXMuX2FyaWFIaWRkZW5WYWx1ZXMuZm9yRWFjaCgodmFsdWUsIGVsZW1lbnQpID0+IHtcclxuICAgICAgICAgICAgaWYgKHZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICBlbGVtZW50LnNldEF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nLCB2YWx1ZSk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBlbGVtZW50LnJlbW92ZUF0dHJpYnV0ZSgnYXJpYS1oaWRkZW4nKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuX2FyaWFIaWRkZW5WYWx1ZXMuY2xlYXIoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9yZWdpc3Rlck1vZGFsUmVmKG5ndE1vZGFsUmVmOiBOZ3RNb2RhbFJlZikge1xyXG4gICAgICAgIGNvbnN0IHVucmVnaXN0ZXJNb2RhbFJlZiA9ICgpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLl9tb2RhbFJlZnMuaW5kZXhPZihuZ3RNb2RhbFJlZik7XHJcbiAgICAgICAgICAgIGlmIChpbmRleCA+IC0xKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9tb2RhbFJlZnMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5fbW9kYWxSZWZzLnB1c2gobmd0TW9kYWxSZWYpO1xyXG4gICAgICAgIG5ndE1vZGFsUmVmLnJlc3VsdC50aGVuKHVucmVnaXN0ZXJNb2RhbFJlZiwgdW5yZWdpc3Rlck1vZGFsUmVmKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIF9yZWdpc3RlcldpbmRvd0NtcHQobmd0V2luZG93Q21wdDogQ29tcG9uZW50UmVmPE5ndE1vZGFsV2luZG93Q29tcG9uZW50Pikge1xyXG4gICAgICAgIHRoaXMuX3dpbmRvd0NtcHRzLnB1c2gobmd0V2luZG93Q21wdCk7XHJcbiAgICAgICAgdGhpcy5fYWN0aXZlV2luZG93Q21wdEhhc0NoYW5nZWQubmV4dCgpO1xyXG5cclxuICAgICAgICBuZ3RXaW5kb3dDbXB0Lm9uRGVzdHJveSgoKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5fd2luZG93Q21wdHMuaW5kZXhPZihuZ3RXaW5kb3dDbXB0KTtcclxuICAgICAgICAgICAgaWYgKGluZGV4ID4gLTEpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3dpbmRvd0NtcHRzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9hY3RpdmVXaW5kb3dDbXB0SGFzQ2hhbmdlZC5uZXh0KCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=