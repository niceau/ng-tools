/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Directive, HostListener, Input } from '@angular/core';
import { NgtModalService } from './modal.service';
export class NgtModalOpenDirective {
    /**
     * @param {?} _modalService
     */
    constructor(_modalService) {
        this._modalService = _modalService;
    }
    /**
     * @return {?}
     */
    onClick() {
        this._modalService.openById(this.id);
    }
}
NgtModalOpenDirective.decorators = [
    { type: Directive, args: [{
                selector: '[ngtModalOpen]'
            },] }
];
/** @nocollapse */
NgtModalOpenDirective.ctorParameters = () => [
    { type: NgtModalService }
];
NgtModalOpenDirective.propDecorators = {
    id: [{ type: Input, args: ['ngtModalOpen',] }],
    onClick: [{ type: HostListener, args: ['click',] }]
};
if (false) {
    /** @type {?} */
    NgtModalOpenDirective.prototype.id;
    /**
     * @type {?}
     * @private
     */
    NgtModalOpenDirective.prototype._modalService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwtb3Blbi5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbIm1vZGFsL21vZGFsLW9wZW4uZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFL0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBS2xELE1BQU0sT0FBTyxxQkFBcUI7Ozs7SUFHOUIsWUFBb0IsYUFBOEI7UUFBOUIsa0JBQWEsR0FBYixhQUFhLENBQWlCO0lBQ2xELENBQUM7Ozs7SUFHRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7OztZQVpKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsZ0JBQWdCO2FBQzdCOzs7O1lBSlEsZUFBZTs7O2lCQU1uQixLQUFLLFNBQUMsY0FBYztzQkFLcEIsWUFBWSxTQUFDLE9BQU87Ozs7SUFMckIsbUNBQWtDOzs7OztJQUV0Qiw4Q0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IE5ndE1vZGFsU2VydmljZSB9IGZyb20gJy4vbW9kYWwuc2VydmljZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW25ndE1vZGFsT3Blbl0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RNb2RhbE9wZW5EaXJlY3RpdmUge1xyXG4gICAgQElucHV0KCduZ3RNb2RhbE9wZW4nKSBpZDogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgX21vZGFsU2VydmljZTogTmd0TW9kYWxTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snKVxyXG4gICAgb25DbGljaygpIHtcclxuICAgICAgICB0aGlzLl9tb2RhbFNlcnZpY2Uub3BlbkJ5SWQodGhpcy5pZCk7XHJcbiAgICB9XHJcbn1cclxuIl19