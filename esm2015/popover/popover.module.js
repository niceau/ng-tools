/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { NgtPopover, NgtPopoverWindow } from './popover';
import { CommonModule } from '@angular/common';
export { NgtPopover } from './popover';
export { NgtPopoverConfig } from './popover-config';
export class NgtPopoverModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtPopoverModule };
    }
}
NgtPopoverModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NgtPopover, NgtPopoverWindow],
                exports: [NgtPopover],
                imports: [CommonModule],
                entryComponents: [NgtPopoverWindow]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9wb3Zlci5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInBvcG92ZXIvcG9wb3Zlci5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTlELE9BQU8sRUFBRSxVQUFVLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxXQUFXLENBQUM7QUFDekQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxXQUFXLENBQUM7QUFDdkMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFTcEQsTUFBTSxPQUFPLGdCQUFnQjs7OztJQUN6QixNQUFNLENBQUMsT0FBTztRQUNWLE9BQU8sRUFBQyxRQUFRLEVBQUUsZ0JBQWdCLEVBQUMsQ0FBQztJQUN4QyxDQUFDOzs7WUFUSixRQUFRLFNBQUM7Z0JBQ04sWUFBWSxFQUFFLENBQUMsVUFBVSxFQUFFLGdCQUFnQixDQUFDO2dCQUM1QyxPQUFPLEVBQUUsQ0FBQyxVQUFVLENBQUM7Z0JBQ3JCLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQztnQkFDdkIsZUFBZSxFQUFFLENBQUMsZ0JBQWdCLENBQUM7YUFDdEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2R1bGVXaXRoUHJvdmlkZXJzLCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTmd0UG9wb3ZlciwgTmd0UG9wb3ZlcldpbmRvdyB9IGZyb20gJy4vcG9wb3Zlcic7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5leHBvcnQgeyBOZ3RQb3BvdmVyIH0gZnJvbSAnLi9wb3BvdmVyJztcclxuZXhwb3J0IHsgTmd0UG9wb3ZlckNvbmZpZyB9IGZyb20gJy4vcG9wb3Zlci1jb25maWcnO1xyXG5leHBvcnQgeyBQbGFjZW1lbnQgfSBmcm9tICcuLi91dGlsL3Bvc2l0aW9uaW5nJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtOZ3RQb3BvdmVyLCBOZ3RQb3BvdmVyV2luZG93XSxcclxuICAgIGV4cG9ydHM6IFtOZ3RQb3BvdmVyXSxcclxuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBbTmd0UG9wb3ZlcldpbmRvd11cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ndFBvcG92ZXJNb2R1bGUge1xyXG4gICAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICAgICAgcmV0dXJuIHtuZ01vZHVsZTogTmd0UG9wb3Zlck1vZHVsZX07XHJcbiAgICB9XHJcbn1cclxuIl19