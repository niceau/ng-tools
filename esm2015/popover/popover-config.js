/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtPopover directive.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the popovers used in the application.
 */
export class NgtPopoverConfig {
    constructor() {
        this.autoClose = true;
        this.placement = 'auto';
        this.triggers = 'click';
        this.disablePopover = false;
        this.openDelay = 0;
        this.closeDelay = 0;
    }
}
NgtPopoverConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtPopoverConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtPopoverConfig_Factory() { return new NgtPopoverConfig(); }, token: NgtPopoverConfig, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtPopoverConfig.prototype.autoClose;
    /** @type {?} */
    NgtPopoverConfig.prototype.placement;
    /** @type {?} */
    NgtPopoverConfig.prototype.triggers;
    /** @type {?} */
    NgtPopoverConfig.prototype.container;
    /** @type {?} */
    NgtPopoverConfig.prototype.disablePopover;
    /** @type {?} */
    NgtPopoverConfig.prototype.popoverClass;
    /** @type {?} */
    NgtPopoverConfig.prototype.openDelay;
    /** @type {?} */
    NgtPopoverConfig.prototype.closeDelay;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicG9wb3Zlci1jb25maWcuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9uZy10b29scy8iLCJzb3VyY2VzIjpbInBvcG92ZXIvcG9wb3Zlci1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFTM0MsTUFBTSxPQUFPLGdCQUFnQjtJQUQ3QjtRQUVJLGNBQVMsR0FBbUMsSUFBSSxDQUFDO1FBQ2pELGNBQVMsR0FBbUIsTUFBTSxDQUFDO1FBQ25DLGFBQVEsR0FBRyxPQUFPLENBQUM7UUFFbkIsbUJBQWMsR0FBRyxLQUFLLENBQUM7UUFFdkIsY0FBUyxHQUFHLENBQUMsQ0FBQztRQUNkLGVBQVUsR0FBRyxDQUFDLENBQUM7S0FDbEI7OztZQVZBLFVBQVUsU0FBQyxFQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUM7Ozs7O0lBRTVCLHFDQUFpRDs7SUFDakQscUNBQW1DOztJQUNuQyxvQ0FBbUI7O0lBQ25CLHFDQUFrQjs7SUFDbEIsMENBQXVCOztJQUN2Qix3Q0FBcUI7O0lBQ3JCLHFDQUFjOztJQUNkLHNDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQbGFjZW1lbnRBcnJheSB9IGZyb20gJy4uL3V0aWwvcG9zaXRpb25pbmcnO1xyXG5cclxuLyoqXHJcbiAqIENvbmZpZ3VyYXRpb24gc2VydmljZSBmb3IgdGhlIE5ndFBvcG92ZXIgZGlyZWN0aXZlLlxyXG4gKiBZb3UgY2FuIGluamVjdCB0aGlzIHNlcnZpY2UsIHR5cGljYWxseSBpbiB5b3VyIHJvb3QgY29tcG9uZW50LCBhbmQgY3VzdG9taXplIHRoZSB2YWx1ZXMgb2YgaXRzIHByb3BlcnRpZXMgaW5cclxuICogb3JkZXIgdG8gcHJvdmlkZSBkZWZhdWx0IHZhbHVlcyBmb3IgYWxsIHRoZSBwb3BvdmVycyB1c2VkIGluIHRoZSBhcHBsaWNhdGlvbi5cclxuICovXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTmd0UG9wb3ZlckNvbmZpZyB7XHJcbiAgICBhdXRvQ2xvc2U6IGJvb2xlYW4gfCAnaW5zaWRlJyB8ICdvdXRzaWRlJyA9IHRydWU7XHJcbiAgICBwbGFjZW1lbnQ6IFBsYWNlbWVudEFycmF5ID0gJ2F1dG8nO1xyXG4gICAgdHJpZ2dlcnMgPSAnY2xpY2snO1xyXG4gICAgY29udGFpbmVyOiBzdHJpbmc7XHJcbiAgICBkaXNhYmxlUG9wb3ZlciA9IGZhbHNlO1xyXG4gICAgcG9wb3ZlckNsYXNzOiBzdHJpbmc7XHJcbiAgICBvcGVuRGVsYXkgPSAwO1xyXG4gICAgY2xvc2VEZWxheSA9IDA7XHJcbn1cclxuIl19