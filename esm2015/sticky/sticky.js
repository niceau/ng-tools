/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, ElementRef, Input, Output, EventEmitter, HostListener, Renderer2 } from '@angular/core';
import { NgtStickyConfig } from './sticky-config';
export class NgtSticky {
    /**
     * @param {?} element
     * @param {?} _renderer
     * @param {?} config
     */
    constructor(element, _renderer, config) {
        this.element = element;
        this._renderer = _renderer;
        this.activated = new EventEmitter();
        this.deactivated = new EventEmitter();
        this.reset = new EventEmitter();
        this.isStuck = false;
        this.orientation = config.orientation;
        this.sticky = config.sticky;
        this.zIndex = config.zIndex;
        this.width = config.width;
        this.offsetTop = config.offsetTop;
        this.offsetBottom = config.offsetBottom;
        this.start = config.start;
        this.stickClass = config.stickClass;
        this.endStickClass = config.endStickClass;
        this.mediaQuery = config.mediaQuery;
        this.parentMode = config.parentMode;
        this.orientation = config.orientation;
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    onChange($event) {
        this.sticker();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.elem = this.element.nativeElement;
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        // define scroll container as parent element
        this.container = this._renderer.parentNode(this.elem);
        this.defineOriginalDimensions();
        this.sticker();
    }
    /**
     * @return {?}
     */
    defineOriginalDimensions() {
        this.originalCss = {
            zIndex: this.getCssValue(this.elem, 'zIndex'),
            position: this.getCssValue(this.elem, 'position'),
            top: this.getCssValue(this.elem, 'top'),
            right: this.getCssValue(this.elem, 'right'),
            left: this.getCssValue(this.elem, 'left'),
            bottom: this.getCssValue(this.elem, 'bottom'),
            width: this.getCssValue(this.elem, 'width'),
        };
    }
    /**
     * @return {?}
     */
    defineYDimensions() {
        this.containerTop = this.getBoundingClientRectValue(this.container, 'top');
        this.windowHeight = window.innerHeight;
        this.elemHeight = this.getCssNumber(this.elem, 'height');
        this.containerHeight = this.getCssNumber(this.container, 'height');
        this.containerStart = this.containerTop + this.getCssNumber(this.container, 'padding-top') + this.scrollbarYPos() - this.offsetTop + this.start;
        if (this.parentMode) {
            this.scrollFinish = this.containerStart - this.start - this.offsetBottom + (this.containerHeight - this.elemHeight);
        }
        else {
            this.scrollFinish = document.body.offsetHeight;
        }
    }
    /**
     * @return {?}
     */
    defineXDimensions() {
        this.containerWidth = this.getCssNumber(this.container, 'width');
        this.setStyles(this.elem, {
            display: 'block',
            position: 'static',
            width: this.width
        });
        this.originalCss.width = this.getCssValue(this.elem, 'width');
    }
    /**
     * @return {?}
     */
    resetElement() {
        this.elem.classList.remove(this.stickClass);
        this.setStyles(this.elem, this.originalCss);
        this.reset.next(this.elem);
    }
    /**
     * @return {?}
     */
    stuckElement() {
        this.isStuck = true;
        this.elem.classList.remove(this.endStickClass);
        this.elem.classList.add(this.stickClass);
        this.setStyles(this.elem, {
            zIndex: this.zIndex,
            position: 'fixed',
            top: this.offsetTop + 'px',
            right: 'auto',
            bottom: 'auto',
            left: this.getBoundingClientRectValue(this.elem, 'left') + 'px',
            width: this.getCssValue(this.elem, 'width')
        });
        this.activated.next(this.elem);
    }
    /**
     * @return {?}
     */
    unstuckElement() {
        this.isStuck = false;
        this.elem.classList.add(this.endStickClass);
        this.container.style.position = 'relative';
        this.setStyles(this.elem, {
            position: 'absolute',
            top: 'auto',
            left: 'auto',
            right: this.getCssValue(this.elem, 'float') === 'right' || this.orientation === 'right' ? 0 : 'auto',
            bottom: this.offsetBottom + 'px',
            width: this.getCssValue(this.elem, 'width')
        });
        this.deactivated.next(this.elem);
    }
    /**
     * @return {?}
     */
    matchMediaQuery() {
        if (!this.mediaQuery) {
            return true;
        }
        return (window.matchMedia('(' + this.mediaQuery + ')').matches ||
            window.matchMedia(this.mediaQuery).matches);
    }
    /**
     * @return {?}
     */
    sticker() {
        // check media query
        if (this.isStuck && !this.matchMediaQuery()) {
            this.resetElement();
            return;
        }
        // detecting when a container's height, width or top position changes
        /** @type {?} */
        const currentContainerHeight = this.getCssNumber(this.container, 'height');
        /** @type {?} */
        const currentContainerWidth = this.getCssNumber(this.container, 'width');
        /** @type {?} */
        const currentContainerTop = this.getBoundingClientRectValue(this.container, 'top');
        if (currentContainerHeight !== this.containerHeight) {
            this.defineYDimensions();
        }
        if (currentContainerTop !== this.containerTop) {
            this.defineYDimensions();
        }
        if (currentContainerWidth !== this.containerWidth) {
            this.defineXDimensions();
        }
        // check if the sticky element is above the container
        if (this.elemHeight >= currentContainerHeight) {
            return;
        }
        /** @type {?} */
        const position = this.scrollbarYPos();
        if (this.isStuck && (position < this.containerStart || position > this.scrollFinish) || position > this.scrollFinish) { // unstick
            this.resetElement();
            if (position > this.scrollFinish) {
                this.unstuckElement();
            }
            this.isStuck = false;
        }
        else if (position > this.containerStart && position < this.scrollFinish) { // stick
            this.stuckElement();
        }
    }
    /**
     * @private
     * @return {?}
     */
    scrollbarYPos() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }
    /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    getBoundingClientRectValue(element, property) {
        /** @type {?} */
        let result = 0;
        if (element && element.getBoundingClientRect) {
            /** @type {?} */
            const rect = element.getBoundingClientRect();
            result = (typeof rect[property] !== 'undefined') ? rect[property] : 0;
        }
        return result;
    }
    /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    getCssValue(element, property) {
        /** @type {?} */
        let result = '';
        if (typeof window.getComputedStyle !== 'undefined') {
            result = window.getComputedStyle(element, '').getPropertyValue(property);
        }
        else if (typeof element.currentStyle !== 'undefined') {
            result = element.currentStyle[property];
        }
        return result;
    }
    /**
     * @private
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    getCssNumber(element, property) {
        if (typeof element === 'undefined') {
            return 0;
        }
        return parseInt(this.getCssValue(element, property), 10) || 0;
    }
    /**
     * @private
     * @param {?} element
     * @param {?} styles
     * @return {?}
     */
    setStyles(element, styles) {
        if (typeof styles === 'object') {
            for (const property in styles) {
                if (styles.hasOwnProperty(property)) {
                    this._renderer.setStyle(element, property, styles[property]);
                }
            }
        }
    }
}
NgtSticky.decorators = [
    { type: Component, args: [{
                selector: 'ngt-sticky,[ngtSticky]',
                template: '<ng-content></ng-content>'
            }] }
];
/** @nocollapse */
NgtSticky.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: NgtStickyConfig }
];
NgtSticky.propDecorators = {
    sticky: [{ type: Input, args: ['sticky',] }],
    zIndex: [{ type: Input, args: ['sticky-zIndex',] }],
    width: [{ type: Input, args: ['sticky-width',] }],
    offsetTop: [{ type: Input, args: ['sticky-offset-top',] }],
    offsetBottom: [{ type: Input, args: ['sticky-offset-bottom',] }],
    start: [{ type: Input, args: ['sticky-start',] }],
    stickClass: [{ type: Input, args: ['sticky-class',] }],
    endStickClass: [{ type: Input, args: ['sticky-end-class',] }],
    mediaQuery: [{ type: Input, args: ['sticky-media-query',] }],
    parentMode: [{ type: Input, args: ['sticky-parent',] }],
    orientation: [{ type: Input, args: ['sticky-orientation',] }],
    activated: [{ type: Output }],
    deactivated: [{ type: Output }],
    reset: [{ type: Output }],
    onChange: [{ type: HostListener, args: ['window:scroll', ['$event'],] }, { type: HostListener, args: ['window:resize', ['$event'],] }, { type: HostListener, args: ['window:orientationchange', ['$event'],] }]
};
if (false) {
    /**
     * Makes an element sticky
     *
     *  <sticky>Sticky element</sticky>
     *  <div sticky>Sticky div</div>
     * @type {?}
     */
    NgtSticky.prototype.sticky;
    /**
     * Controls z-index CSS parameter of the sticky element
     * @type {?}
     */
    NgtSticky.prototype.zIndex;
    /**
     * Width of the sticky element
     * @type {?}
     */
    NgtSticky.prototype.width;
    /**
     * Pixels between the top of the page or container and the element
     * @type {?}
     */
    NgtSticky.prototype.offsetTop;
    /**
     *  Pixels between the bottom of the page or container and the element
     * @type {?}
     */
    NgtSticky.prototype.offsetBottom;
    /**
     * Position where the element should start to stick
     * @type {?}
     */
    NgtSticky.prototype.start;
    /**
     * CSS class that will be added after the element starts sticking
     * @type {?}
     */
    NgtSticky.prototype.stickClass;
    /**
     * CSS class that will be added to the sticky element after it ends sticking
     * @type {?}
     */
    NgtSticky.prototype.endStickClass;
    /**
     * Media query that allows to use sticky
     * @type {?}
     */
    NgtSticky.prototype.mediaQuery;
    /**
     * If true, then the sticky element will be stuck relatively to the parent containers. Otherwise, window will be used as the parent container.
     * NOTE: the "position: relative" styling is added to the parent element automatically in order to use the absolute positioning
     * @type {?}
     */
    NgtSticky.prototype.parentMode;
    /**
     * Orientation for sticky element ("left", "right", "none")
     * @type {?}
     */
    NgtSticky.prototype.orientation;
    /** @type {?} */
    NgtSticky.prototype.activated;
    /** @type {?} */
    NgtSticky.prototype.deactivated;
    /** @type {?} */
    NgtSticky.prototype.reset;
    /** @type {?} */
    NgtSticky.prototype.isStuck;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.elem;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.container;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.originalCss;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.windowHeight;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.containerHeight;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.containerWidth;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.containerTop;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.elemHeight;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.containerStart;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.scrollFinish;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype.element;
    /**
     * @type {?}
     * @private
     */
    NgtSticky.prototype._renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RpY2t5LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctdG9vbHMvIiwic291cmNlcyI6WyJzdGlja3kvc3RpY2t5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQ0gsU0FBUyxFQUNULFVBQVUsRUFDVixLQUFLLEVBQ0wsTUFBTSxFQUNOLFlBQVksRUFHWixZQUFZLEVBQ1osU0FBUyxFQUNaLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQU1sRCxNQUFNLE9BQU8sU0FBUzs7Ozs7O0lBcUZsQixZQUFvQixPQUFtQixFQUNuQixTQUFvQixFQUM1QixNQUF1QjtRQUZmLFlBQU8sR0FBUCxPQUFPLENBQVk7UUFDbkIsY0FBUyxHQUFULFNBQVMsQ0FBVztRQTFCOUIsY0FBUyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDL0IsZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2pDLFVBQUssR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRXJDLFlBQU8sR0FBWSxLQUFLLENBQUM7UUF3QnJCLElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUN0QyxJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDNUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO1FBQzVCLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUM7UUFDbEMsSUFBSSxDQUFDLFlBQVksR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFDcEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFDO1FBQzFDLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQztRQUNwQyxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsV0FBVyxDQUFDO0lBQzFDLENBQUM7Ozs7O0lBbkJELFFBQVEsQ0FBQyxNQUFNO1FBQ1gsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ25CLENBQUM7Ozs7SUFtQkQsUUFBUTtRQUNKLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUM7SUFDM0MsQ0FBQzs7OztJQUVELGVBQWU7UUFDWCw0Q0FBNEM7UUFDNUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUM7UUFDaEMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ25CLENBQUM7Ozs7SUFFRCx3QkFBd0I7UUFDcEIsSUFBSSxDQUFDLFdBQVcsR0FBRztZQUNmLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDO1lBQzdDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDO1lBQ2pELEdBQUcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDO1lBQ3ZDLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDO1lBQzNDLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDO1lBQ3pDLE1BQU0sRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDO1lBQzdDLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDO1NBQzlDLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsaUJBQWlCO1FBQ2IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMzRSxJQUFJLENBQUMsWUFBWSxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUM7UUFDdkMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ2hKLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDdkg7YUFBTTtZQUNILElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7U0FDbEQ7SUFDTCxDQUFDOzs7O0lBRUQsaUJBQWlCO1FBQ2IsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDakUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ3RCLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztTQUNwQixDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDbEUsQ0FBQzs7OztJQUVELFlBQVk7UUFDUixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFFNUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQy9CLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1IsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFFcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXpDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUN0QixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDbkIsUUFBUSxFQUFFLE9BQU87WUFDakIsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSTtZQUMxQixLQUFLLEVBQUUsTUFBTTtZQUNiLE1BQU0sRUFBRSxNQUFNO1lBQ2QsSUFBSSxFQUFFLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLE1BQU0sQ0FBQyxHQUFHLElBQUk7WUFDL0QsS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUM7U0FDOUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7SUFHRCxjQUFjO1FBQ1YsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFFckIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUU1QyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO1FBRTNDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUN0QixRQUFRLEVBQUUsVUFBVTtZQUNwQixHQUFHLEVBQUUsTUFBTTtZQUNYLElBQUksRUFBRSxNQUFNO1lBQ1osS0FBSyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsS0FBSyxPQUFPLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTTtZQUNwRyxNQUFNLEVBQUUsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJO1lBQ2hDLEtBQUssRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDO1NBQzlDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNyQyxDQUFDOzs7O0lBRUQsZUFBZTtRQUNYLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2xCLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxPQUFPLENBQ0gsTUFBTSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsQ0FBQyxPQUFPO1lBQ3RELE1BQU0sQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE9BQU8sQ0FDN0MsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0gsb0JBQW9CO1FBQ3BCLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsRUFBRTtZQUN6QyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDcEIsT0FBTztTQUNWOzs7Y0FHSyxzQkFBc0IsR0FBVyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsUUFBUSxDQUFDOztjQUM1RSxxQkFBcUIsR0FBVyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDOztjQUMxRSxtQkFBbUIsR0FBVyxJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUM7UUFDMUYsSUFBSSxzQkFBc0IsS0FBSyxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ2pELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQzVCO1FBQ0QsSUFBSSxtQkFBbUIsS0FBSyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQzNDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQzVCO1FBQ0QsSUFBSSxxQkFBcUIsS0FBSyxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQy9DLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQzVCO1FBRUQscURBQXFEO1FBQ3JELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxzQkFBc0IsRUFBRTtZQUMzQyxPQUFPO1NBQ1Y7O2NBRUssUUFBUSxHQUFXLElBQUksQ0FBQyxhQUFhLEVBQUU7UUFFN0MsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFLFVBQVU7WUFDOUgsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ3BCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQzlCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUN6QjtZQUNELElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQ3hCO2FBQU0sSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFLFFBQVE7WUFDakYsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1NBQ3ZCO0lBQ0wsQ0FBQzs7Ozs7SUFFTyxhQUFhO1FBQ2pCLE9BQU8sTUFBTSxDQUFDLFdBQVcsSUFBSSxRQUFRLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQztJQUNwRSxDQUFDOzs7Ozs7O0lBRU8sMEJBQTBCLENBQUMsT0FBWSxFQUFFLFFBQWdCOztZQUN6RCxNQUFNLEdBQUcsQ0FBQztRQUNkLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRTs7a0JBQ3BDLElBQUksR0FBRyxPQUFPLENBQUMscUJBQXFCLEVBQUU7WUFDNUMsTUFBTSxHQUFHLENBQUMsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ3pFO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7Ozs7OztJQUVPLFdBQVcsQ0FBQyxPQUFZLEVBQUUsUUFBZ0I7O1lBQzFDLE1BQU0sR0FBUSxFQUFFO1FBQ3BCLElBQUksT0FBTyxNQUFNLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxFQUFFO1lBQ2hELE1BQU0sR0FBRyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFLEVBQUUsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQzVFO2FBQU0sSUFBSSxPQUFPLE9BQU8sQ0FBQyxZQUFZLEtBQUssV0FBVyxFQUFFO1lBQ3BELE1BQU0sR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQzNDO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7Ozs7OztJQUVPLFlBQVksQ0FBQyxPQUFZLEVBQUUsUUFBZ0I7UUFDL0MsSUFBSSxPQUFPLE9BQU8sS0FBSyxXQUFXLEVBQUU7WUFDaEMsT0FBTyxDQUFDLENBQUM7U0FDWjtRQUNELE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNsRSxDQUFDOzs7Ozs7O0lBRU8sU0FBUyxDQUFDLE9BQVksRUFBRSxNQUFXO1FBQ3ZDLElBQUksT0FBTyxNQUFNLEtBQUssUUFBUSxFQUFFO1lBQzVCLEtBQUssTUFBTSxRQUFRLElBQUksTUFBTSxFQUFFO2dCQUMzQixJQUFJLE1BQU0sQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLEVBQUU7b0JBQ2pDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7aUJBQ2hFO2FBQ0o7U0FDSjtJQUNMLENBQUM7OztZQTdSSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHdCQUF3QjtnQkFDbEMsUUFBUSxFQUFFLDJCQUEyQjthQUN4Qzs7OztZQWRHLFVBQVU7WUFPVixTQUFTO1lBRUosZUFBZTs7O3FCQWFuQixLQUFLLFNBQUMsUUFBUTtxQkFLZCxLQUFLLFNBQUMsZUFBZTtvQkFLckIsS0FBSyxTQUFDLGNBQWM7d0JBS3BCLEtBQUssU0FBQyxtQkFBbUI7MkJBS3pCLEtBQUssU0FBQyxzQkFBc0I7b0JBSzVCLEtBQUssU0FBQyxjQUFjO3lCQUtwQixLQUFLLFNBQUMsY0FBYzs0QkFLcEIsS0FBSyxTQUFDLGtCQUFrQjt5QkFLeEIsS0FBSyxTQUFDLG9CQUFvQjt5QkFNMUIsS0FBSyxTQUFDLGVBQWU7MEJBS3JCLEtBQUssU0FBQyxvQkFBb0I7d0JBRTFCLE1BQU07MEJBQ04sTUFBTTtvQkFDTixNQUFNO3VCQWdCTixZQUFZLFNBQUMsZUFBZSxFQUFFLENBQUMsUUFBUSxDQUFDLGNBQ3hDLFlBQVksU0FBQyxlQUFlLEVBQUUsQ0FBQyxRQUFRLENBQUMsY0FDeEMsWUFBWSxTQUFDLDBCQUEwQixFQUFFLENBQUMsUUFBUSxDQUFDOzs7Ozs7Ozs7O0lBekVwRCwyQkFBZ0M7Ozs7O0lBS2hDLDJCQUF1Qzs7Ozs7SUFLdkMsMEJBQXFDOzs7OztJQUtyQyw4QkFBOEM7Ozs7O0lBSzlDLGlDQUFvRDs7Ozs7SUFLcEQsMEJBQXFDOzs7OztJQUtyQywrQkFBMEM7Ozs7O0lBSzFDLGtDQUFpRDs7Ozs7SUFLakQsK0JBQWdEOzs7Ozs7SUFNaEQsK0JBQTRDOzs7OztJQUs1QyxnQ0FBaUQ7O0lBRWpELDhCQUF5Qzs7SUFDekMsZ0NBQTJDOztJQUMzQywwQkFBcUM7O0lBRXJDLDRCQUF5Qjs7Ozs7SUFFekIseUJBQWtCOzs7OztJQUNsQiw4QkFBdUI7Ozs7O0lBQ3ZCLGdDQUF5Qjs7Ozs7SUFFekIsaUNBQTZCOzs7OztJQUM3QixvQ0FBZ0M7Ozs7O0lBQ2hDLG1DQUErQjs7Ozs7SUFDL0IsaUNBQTZCOzs7OztJQUM3QiwrQkFBMkI7Ozs7O0lBQzNCLG1DQUErQjs7Ozs7SUFDL0IsaUNBQTZCOzs7OztJQVNqQiw0QkFBMkI7Ozs7O0lBQzNCLDhCQUE0QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBDb21wb25lbnQsXHJcbiAgICBFbGVtZW50UmVmLFxyXG4gICAgSW5wdXQsXHJcbiAgICBPdXRwdXQsXHJcbiAgICBFdmVudEVtaXR0ZXIsXHJcbiAgICBPbkluaXQsXHJcbiAgICBBZnRlclZpZXdJbml0LFxyXG4gICAgSG9zdExpc3RlbmVyLFxyXG4gICAgUmVuZGVyZXIyXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5ndFN0aWNreUNvbmZpZyB9IGZyb20gJy4vc3RpY2t5LWNvbmZpZyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbmd0LXN0aWNreSxbbmd0U3RpY2t5XScsXHJcbiAgICB0ZW1wbGF0ZTogJzxuZy1jb250ZW50PjwvbmctY29udGVudD4nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RTdGlja3kgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQge1xyXG4gICAgLyoqXHJcbiAgICAgKiBNYWtlcyBhbiBlbGVtZW50IHN0aWNreVxyXG4gICAgICpcclxuICAgICAqICA8c3RpY2t5PlN0aWNreSBlbGVtZW50PC9zdGlja3k+XHJcbiAgICAgKiAgPGRpdiBzdGlja3k+U3RpY2t5IGRpdjwvZGl2PlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoJ3N0aWNreScpIHN0aWNreTogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udHJvbHMgei1pbmRleCBDU1MgcGFyYW1ldGVyIG9mIHRoZSBzdGlja3kgZWxlbWVudFxyXG4gICAgICovXHJcbiAgICBASW5wdXQoJ3N0aWNreS16SW5kZXgnKSB6SW5kZXg6IG51bWJlcjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFdpZHRoIG9mIHRoZSBzdGlja3kgZWxlbWVudFxyXG4gICAgICovXHJcbiAgICBASW5wdXQoJ3N0aWNreS13aWR0aCcpIHdpZHRoOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQaXhlbHMgYmV0d2VlbiB0aGUgdG9wIG9mIHRoZSBwYWdlIG9yIGNvbnRhaW5lciBhbmQgdGhlIGVsZW1lbnRcclxuICAgICAqL1xyXG4gICAgQElucHV0KCdzdGlja3ktb2Zmc2V0LXRvcCcpIG9mZnNldFRvcDogbnVtYmVyO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogIFBpeGVscyBiZXR3ZWVuIHRoZSBib3R0b20gb2YgdGhlIHBhZ2Ugb3IgY29udGFpbmVyIGFuZCB0aGUgZWxlbWVudFxyXG4gICAgICovXHJcbiAgICBASW5wdXQoJ3N0aWNreS1vZmZzZXQtYm90dG9tJykgb2Zmc2V0Qm90dG9tOiBudW1iZXI7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQb3NpdGlvbiB3aGVyZSB0aGUgZWxlbWVudCBzaG91bGQgc3RhcnQgdG8gc3RpY2tcclxuICAgICAqL1xyXG4gICAgQElucHV0KCdzdGlja3ktc3RhcnQnKSBzdGFydDogbnVtYmVyO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ1NTIGNsYXNzIHRoYXQgd2lsbCBiZSBhZGRlZCBhZnRlciB0aGUgZWxlbWVudCBzdGFydHMgc3RpY2tpbmdcclxuICAgICAqL1xyXG4gICAgQElucHV0KCdzdGlja3ktY2xhc3MnKSBzdGlja0NsYXNzOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDU1MgY2xhc3MgdGhhdCB3aWxsIGJlIGFkZGVkIHRvIHRoZSBzdGlja3kgZWxlbWVudCBhZnRlciBpdCBlbmRzIHN0aWNraW5nXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgnc3RpY2t5LWVuZC1jbGFzcycpIGVuZFN0aWNrQ2xhc3M6IHN0cmluZztcclxuXHJcbiAgICAvKipcclxuICAgICAqIE1lZGlhIHF1ZXJ5IHRoYXQgYWxsb3dzIHRvIHVzZSBzdGlja3lcclxuICAgICAqL1xyXG4gICAgQElucHV0KCdzdGlja3ktbWVkaWEtcXVlcnknKSBtZWRpYVF1ZXJ5OiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJZiB0cnVlLCB0aGVuIHRoZSBzdGlja3kgZWxlbWVudCB3aWxsIGJlIHN0dWNrIHJlbGF0aXZlbHkgdG8gdGhlIHBhcmVudCBjb250YWluZXJzLiBPdGhlcndpc2UsIHdpbmRvdyB3aWxsIGJlIHVzZWQgYXMgdGhlIHBhcmVudCBjb250YWluZXIuXHJcbiAgICAgKiBOT1RFOiB0aGUgXCJwb3NpdGlvbjogcmVsYXRpdmVcIiBzdHlsaW5nIGlzIGFkZGVkIHRvIHRoZSBwYXJlbnQgZWxlbWVudCBhdXRvbWF0aWNhbGx5IGluIG9yZGVyIHRvIHVzZSB0aGUgYWJzb2x1dGUgcG9zaXRpb25pbmdcclxuICAgICAqL1xyXG4gICAgQElucHV0KCdzdGlja3ktcGFyZW50JykgcGFyZW50TW9kZTogYm9vbGVhbjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIE9yaWVudGF0aW9uIGZvciBzdGlja3kgZWxlbWVudCAoXCJsZWZ0XCIsIFwicmlnaHRcIiwgXCJub25lXCIpXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgnc3RpY2t5LW9yaWVudGF0aW9uJykgb3JpZW50YXRpb246IHN0cmluZztcclxuXHJcbiAgICBAT3V0cHV0KCkgYWN0aXZhdGVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gICAgQE91dHB1dCgpIGRlYWN0aXZhdGVkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gICAgQE91dHB1dCgpIHJlc2V0ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICAgIGlzU3R1Y2s6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBwcml2YXRlIGVsZW06IGFueTtcclxuICAgIHByaXZhdGUgY29udGFpbmVyOiBhbnk7XHJcbiAgICBwcml2YXRlIG9yaWdpbmFsQ3NzOiBhbnk7XHJcblxyXG4gICAgcHJpdmF0ZSB3aW5kb3dIZWlnaHQ6IG51bWJlcjtcclxuICAgIHByaXZhdGUgY29udGFpbmVySGVpZ2h0OiBudW1iZXI7XHJcbiAgICBwcml2YXRlIGNvbnRhaW5lcldpZHRoOiBudW1iZXI7XHJcbiAgICBwcml2YXRlIGNvbnRhaW5lclRvcDogbnVtYmVyO1xyXG4gICAgcHJpdmF0ZSBlbGVtSGVpZ2h0OiBudW1iZXI7XHJcbiAgICBwcml2YXRlIGNvbnRhaW5lclN0YXJ0OiBudW1iZXI7XHJcbiAgICBwcml2YXRlIHNjcm9sbEZpbmlzaDogbnVtYmVyO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpzY3JvbGwnLCBbJyRldmVudCddKVxyXG4gICAgQEhvc3RMaXN0ZW5lcignd2luZG93OnJlc2l6ZScsIFsnJGV2ZW50J10pXHJcbiAgICBASG9zdExpc3RlbmVyKCd3aW5kb3c6b3JpZW50YXRpb25jaGFuZ2UnLCBbJyRldmVudCddKVxyXG4gICAgb25DaGFuZ2UoJGV2ZW50KTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5zdGlja2VyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBlbGVtZW50OiBFbGVtZW50UmVmLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBfcmVuZGVyZXI6IFJlbmRlcmVyMixcclxuICAgICAgICAgICAgICAgIGNvbmZpZzogTmd0U3RpY2t5Q29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy5vcmllbnRhdGlvbiA9IGNvbmZpZy5vcmllbnRhdGlvbjtcclxuICAgICAgICB0aGlzLnN0aWNreSA9IGNvbmZpZy5zdGlja3k7XHJcbiAgICAgICAgdGhpcy56SW5kZXggPSBjb25maWcuekluZGV4O1xyXG4gICAgICAgIHRoaXMud2lkdGggPSBjb25maWcud2lkdGg7XHJcbiAgICAgICAgdGhpcy5vZmZzZXRUb3AgPSBjb25maWcub2Zmc2V0VG9wO1xyXG4gICAgICAgIHRoaXMub2Zmc2V0Qm90dG9tID0gY29uZmlnLm9mZnNldEJvdHRvbTtcclxuICAgICAgICB0aGlzLnN0YXJ0ID0gY29uZmlnLnN0YXJ0O1xyXG4gICAgICAgIHRoaXMuc3RpY2tDbGFzcyA9IGNvbmZpZy5zdGlja0NsYXNzO1xyXG4gICAgICAgIHRoaXMuZW5kU3RpY2tDbGFzcyA9IGNvbmZpZy5lbmRTdGlja0NsYXNzO1xyXG4gICAgICAgIHRoaXMubWVkaWFRdWVyeSA9IGNvbmZpZy5tZWRpYVF1ZXJ5O1xyXG4gICAgICAgIHRoaXMucGFyZW50TW9kZSA9IGNvbmZpZy5wYXJlbnRNb2RlO1xyXG4gICAgICAgIHRoaXMub3JpZW50YXRpb24gPSBjb25maWcub3JpZW50YXRpb247XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5lbGVtID0gdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xyXG4gICAgICAgIC8vIGRlZmluZSBzY3JvbGwgY29udGFpbmVyIGFzIHBhcmVudCBlbGVtZW50XHJcbiAgICAgICAgdGhpcy5jb250YWluZXIgPSB0aGlzLl9yZW5kZXJlci5wYXJlbnROb2RlKHRoaXMuZWxlbSk7XHJcbiAgICAgICAgdGhpcy5kZWZpbmVPcmlnaW5hbERpbWVuc2lvbnMoKTtcclxuICAgICAgICB0aGlzLnN0aWNrZXIoKTtcclxuICAgIH1cclxuXHJcbiAgICBkZWZpbmVPcmlnaW5hbERpbWVuc2lvbnMoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5vcmlnaW5hbENzcyA9IHtcclxuICAgICAgICAgICAgekluZGV4OiB0aGlzLmdldENzc1ZhbHVlKHRoaXMuZWxlbSwgJ3pJbmRleCcpLFxyXG4gICAgICAgICAgICBwb3NpdGlvbjogdGhpcy5nZXRDc3NWYWx1ZSh0aGlzLmVsZW0sICdwb3NpdGlvbicpLFxyXG4gICAgICAgICAgICB0b3A6IHRoaXMuZ2V0Q3NzVmFsdWUodGhpcy5lbGVtLCAndG9wJyksXHJcbiAgICAgICAgICAgIHJpZ2h0OiB0aGlzLmdldENzc1ZhbHVlKHRoaXMuZWxlbSwgJ3JpZ2h0JyksXHJcbiAgICAgICAgICAgIGxlZnQ6IHRoaXMuZ2V0Q3NzVmFsdWUodGhpcy5lbGVtLCAnbGVmdCcpLFxyXG4gICAgICAgICAgICBib3R0b206IHRoaXMuZ2V0Q3NzVmFsdWUodGhpcy5lbGVtLCAnYm90dG9tJyksXHJcbiAgICAgICAgICAgIHdpZHRoOiB0aGlzLmdldENzc1ZhbHVlKHRoaXMuZWxlbSwgJ3dpZHRoJyksXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBkZWZpbmVZRGltZW5zaW9ucygpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lclRvcCA9IHRoaXMuZ2V0Qm91bmRpbmdDbGllbnRSZWN0VmFsdWUodGhpcy5jb250YWluZXIsICd0b3AnKTtcclxuICAgICAgICB0aGlzLndpbmRvd0hlaWdodCA9IHdpbmRvdy5pbm5lckhlaWdodDtcclxuICAgICAgICB0aGlzLmVsZW1IZWlnaHQgPSB0aGlzLmdldENzc051bWJlcih0aGlzLmVsZW0sICdoZWlnaHQnKTtcclxuICAgICAgICB0aGlzLmNvbnRhaW5lckhlaWdodCA9IHRoaXMuZ2V0Q3NzTnVtYmVyKHRoaXMuY29udGFpbmVyLCAnaGVpZ2h0Jyk7XHJcbiAgICAgICAgdGhpcy5jb250YWluZXJTdGFydCA9IHRoaXMuY29udGFpbmVyVG9wICsgdGhpcy5nZXRDc3NOdW1iZXIodGhpcy5jb250YWluZXIsICdwYWRkaW5nLXRvcCcpICsgdGhpcy5zY3JvbGxiYXJZUG9zKCkgLSB0aGlzLm9mZnNldFRvcCArIHRoaXMuc3RhcnQ7XHJcbiAgICAgICAgaWYgKHRoaXMucGFyZW50TW9kZSkge1xyXG4gICAgICAgICAgICB0aGlzLnNjcm9sbEZpbmlzaCA9IHRoaXMuY29udGFpbmVyU3RhcnQgLSB0aGlzLnN0YXJ0IC0gdGhpcy5vZmZzZXRCb3R0b20gKyAodGhpcy5jb250YWluZXJIZWlnaHQgLSB0aGlzLmVsZW1IZWlnaHQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2Nyb2xsRmluaXNoID0gZG9jdW1lbnQuYm9keS5vZmZzZXRIZWlnaHQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGRlZmluZVhEaW1lbnNpb25zKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY29udGFpbmVyV2lkdGggPSB0aGlzLmdldENzc051bWJlcih0aGlzLmNvbnRhaW5lciwgJ3dpZHRoJyk7XHJcbiAgICAgICAgdGhpcy5zZXRTdHlsZXModGhpcy5lbGVtLCB7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6ICdibG9jaycsXHJcbiAgICAgICAgICAgIHBvc2l0aW9uOiAnc3RhdGljJyxcclxuICAgICAgICAgICAgd2lkdGg6IHRoaXMud2lkdGhcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLm9yaWdpbmFsQ3NzLndpZHRoID0gdGhpcy5nZXRDc3NWYWx1ZSh0aGlzLmVsZW0sICd3aWR0aCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0RWxlbWVudCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmVsZW0uY2xhc3NMaXN0LnJlbW92ZSh0aGlzLnN0aWNrQ2xhc3MpO1xyXG4gICAgICAgIHRoaXMuc2V0U3R5bGVzKHRoaXMuZWxlbSwgdGhpcy5vcmlnaW5hbENzcyk7XHJcblxyXG4gICAgICAgIHRoaXMucmVzZXQubmV4dCh0aGlzLmVsZW0pO1xyXG4gICAgfVxyXG5cclxuICAgIHN0dWNrRWxlbWVudCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmlzU3R1Y2sgPSB0cnVlO1xyXG5cclxuICAgICAgICB0aGlzLmVsZW0uY2xhc3NMaXN0LnJlbW92ZSh0aGlzLmVuZFN0aWNrQ2xhc3MpO1xyXG4gICAgICAgIHRoaXMuZWxlbS5jbGFzc0xpc3QuYWRkKHRoaXMuc3RpY2tDbGFzcyk7XHJcblxyXG4gICAgICAgIHRoaXMuc2V0U3R5bGVzKHRoaXMuZWxlbSwge1xyXG4gICAgICAgICAgICB6SW5kZXg6IHRoaXMuekluZGV4LFxyXG4gICAgICAgICAgICBwb3NpdGlvbjogJ2ZpeGVkJyxcclxuICAgICAgICAgICAgdG9wOiB0aGlzLm9mZnNldFRvcCArICdweCcsXHJcbiAgICAgICAgICAgIHJpZ2h0OiAnYXV0bycsXHJcbiAgICAgICAgICAgIGJvdHRvbTogJ2F1dG8nLFxyXG4gICAgICAgICAgICBsZWZ0OiB0aGlzLmdldEJvdW5kaW5nQ2xpZW50UmVjdFZhbHVlKHRoaXMuZWxlbSwgJ2xlZnQnKSArICdweCcsXHJcbiAgICAgICAgICAgIHdpZHRoOiB0aGlzLmdldENzc1ZhbHVlKHRoaXMuZWxlbSwgJ3dpZHRoJylcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5hY3RpdmF0ZWQubmV4dCh0aGlzLmVsZW0pO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICB1bnN0dWNrRWxlbWVudCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmlzU3R1Y2sgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgdGhpcy5lbGVtLmNsYXNzTGlzdC5hZGQodGhpcy5lbmRTdGlja0NsYXNzKTtcclxuXHJcbiAgICAgICAgdGhpcy5jb250YWluZXIuc3R5bGUucG9zaXRpb24gPSAncmVsYXRpdmUnO1xyXG5cclxuICAgICAgICB0aGlzLnNldFN0eWxlcyh0aGlzLmVsZW0sIHtcclxuICAgICAgICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXHJcbiAgICAgICAgICAgIHRvcDogJ2F1dG8nLFxyXG4gICAgICAgICAgICBsZWZ0OiAnYXV0bycsXHJcbiAgICAgICAgICAgIHJpZ2h0OiB0aGlzLmdldENzc1ZhbHVlKHRoaXMuZWxlbSwgJ2Zsb2F0JykgPT09ICdyaWdodCcgfHwgdGhpcy5vcmllbnRhdGlvbiA9PT0gJ3JpZ2h0JyA/IDAgOiAnYXV0bycsXHJcbiAgICAgICAgICAgIGJvdHRvbTogdGhpcy5vZmZzZXRCb3R0b20gKyAncHgnLFxyXG4gICAgICAgICAgICB3aWR0aDogdGhpcy5nZXRDc3NWYWx1ZSh0aGlzLmVsZW0sICd3aWR0aCcpXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuZGVhY3RpdmF0ZWQubmV4dCh0aGlzLmVsZW0pO1xyXG4gICAgfVxyXG5cclxuICAgIG1hdGNoTWVkaWFRdWVyeSgpOiBhbnkge1xyXG4gICAgICAgIGlmICghdGhpcy5tZWRpYVF1ZXJ5KSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICB3aW5kb3cubWF0Y2hNZWRpYSgnKCcgKyB0aGlzLm1lZGlhUXVlcnkgKyAnKScpLm1hdGNoZXMgfHxcclxuICAgICAgICAgICAgd2luZG93Lm1hdGNoTWVkaWEodGhpcy5tZWRpYVF1ZXJ5KS5tYXRjaGVzXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBzdGlja2VyKCk6IHZvaWQge1xyXG4gICAgICAgIC8vIGNoZWNrIG1lZGlhIHF1ZXJ5XHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdHVjayAmJiAhdGhpcy5tYXRjaE1lZGlhUXVlcnkoKSkge1xyXG4gICAgICAgICAgICB0aGlzLnJlc2V0RWxlbWVudCgpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBkZXRlY3Rpbmcgd2hlbiBhIGNvbnRhaW5lcidzIGhlaWdodCwgd2lkdGggb3IgdG9wIHBvc2l0aW9uIGNoYW5nZXNcclxuICAgICAgICBjb25zdCBjdXJyZW50Q29udGFpbmVySGVpZ2h0OiBudW1iZXIgPSB0aGlzLmdldENzc051bWJlcih0aGlzLmNvbnRhaW5lciwgJ2hlaWdodCcpO1xyXG4gICAgICAgIGNvbnN0IGN1cnJlbnRDb250YWluZXJXaWR0aDogbnVtYmVyID0gdGhpcy5nZXRDc3NOdW1iZXIodGhpcy5jb250YWluZXIsICd3aWR0aCcpO1xyXG4gICAgICAgIGNvbnN0IGN1cnJlbnRDb250YWluZXJUb3A6IG51bWJlciA9IHRoaXMuZ2V0Qm91bmRpbmdDbGllbnRSZWN0VmFsdWUodGhpcy5jb250YWluZXIsICd0b3AnKTtcclxuICAgICAgICBpZiAoY3VycmVudENvbnRhaW5lckhlaWdodCAhPT0gdGhpcy5jb250YWluZXJIZWlnaHQpIHtcclxuICAgICAgICAgICAgdGhpcy5kZWZpbmVZRGltZW5zaW9ucygpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoY3VycmVudENvbnRhaW5lclRvcCAhPT0gdGhpcy5jb250YWluZXJUb3ApIHtcclxuICAgICAgICAgICAgdGhpcy5kZWZpbmVZRGltZW5zaW9ucygpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoY3VycmVudENvbnRhaW5lcldpZHRoICE9PSB0aGlzLmNvbnRhaW5lcldpZHRoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVmaW5lWERpbWVuc2lvbnMoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIGNoZWNrIGlmIHRoZSBzdGlja3kgZWxlbWVudCBpcyBhYm92ZSB0aGUgY29udGFpbmVyXHJcbiAgICAgICAgaWYgKHRoaXMuZWxlbUhlaWdodCA+PSBjdXJyZW50Q29udGFpbmVySGVpZ2h0KSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHBvc2l0aW9uOiBudW1iZXIgPSB0aGlzLnNjcm9sbGJhcllQb3MoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdHVjayAmJiAocG9zaXRpb24gPCB0aGlzLmNvbnRhaW5lclN0YXJ0IHx8IHBvc2l0aW9uID4gdGhpcy5zY3JvbGxGaW5pc2gpIHx8IHBvc2l0aW9uID4gdGhpcy5zY3JvbGxGaW5pc2gpIHsgLy8gdW5zdGlja1xyXG4gICAgICAgICAgICB0aGlzLnJlc2V0RWxlbWVudCgpO1xyXG4gICAgICAgICAgICBpZiAocG9zaXRpb24gPiB0aGlzLnNjcm9sbEZpbmlzaCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51bnN0dWNrRWxlbWVudCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuaXNTdHVjayA9IGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAocG9zaXRpb24gPiB0aGlzLmNvbnRhaW5lclN0YXJ0ICYmIHBvc2l0aW9uIDwgdGhpcy5zY3JvbGxGaW5pc2gpIHsgLy8gc3RpY2tcclxuICAgICAgICAgICAgdGhpcy5zdHVja0VsZW1lbnQoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzY3JvbGxiYXJZUG9zKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHdpbmRvdy5wYWdlWU9mZnNldCB8fCBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0Qm91bmRpbmdDbGllbnRSZWN0VmFsdWUoZWxlbWVudDogYW55LCBwcm9wZXJ0eTogc3RyaW5nKTogbnVtYmVyIHtcclxuICAgICAgICBsZXQgcmVzdWx0ID0gMDtcclxuICAgICAgICBpZiAoZWxlbWVudCAmJiBlbGVtZW50LmdldEJvdW5kaW5nQ2xpZW50UmVjdCkge1xyXG4gICAgICAgICAgICBjb25zdCByZWN0ID0gZWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKTtcclxuICAgICAgICAgICAgcmVzdWx0ID0gKHR5cGVvZiByZWN0W3Byb3BlcnR5XSAhPT0gJ3VuZGVmaW5lZCcpID8gcmVjdFtwcm9wZXJ0eV0gOiAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0Q3NzVmFsdWUoZWxlbWVudDogYW55LCBwcm9wZXJ0eTogc3RyaW5nKTogYW55IHtcclxuICAgICAgICBsZXQgcmVzdWx0OiBhbnkgPSAnJztcclxuICAgICAgICBpZiAodHlwZW9mIHdpbmRvdy5nZXRDb21wdXRlZFN0eWxlICE9PSAndW5kZWZpbmVkJykge1xyXG4gICAgICAgICAgICByZXN1bHQgPSB3aW5kb3cuZ2V0Q29tcHV0ZWRTdHlsZShlbGVtZW50LCAnJykuZ2V0UHJvcGVydHlWYWx1ZShwcm9wZXJ0eSk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0eXBlb2YgZWxlbWVudC5jdXJyZW50U3R5bGUgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdCA9IGVsZW1lbnQuY3VycmVudFN0eWxlW3Byb3BlcnR5XTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldENzc051bWJlcihlbGVtZW50OiBhbnksIHByb3BlcnR5OiBzdHJpbmcpOiBudW1iZXIge1xyXG4gICAgICAgIGlmICh0eXBlb2YgZWxlbWVudCA9PT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIDA7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBwYXJzZUludCh0aGlzLmdldENzc1ZhbHVlKGVsZW1lbnQsIHByb3BlcnR5KSwgMTApIHx8IDA7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzZXRTdHlsZXMoZWxlbWVudDogYW55LCBzdHlsZXM6IGFueSk6IHZvaWQge1xyXG4gICAgICAgIGlmICh0eXBlb2Ygc3R5bGVzID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IHByb3BlcnR5IGluIHN0eWxlcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKHN0eWxlcy5oYXNPd25Qcm9wZXJ0eShwcm9wZXJ0eSkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZShlbGVtZW50LCBwcm9wZXJ0eSwgc3R5bGVzW3Byb3BlcnR5XSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19