/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * Configuration service for the NgtSticky component.
 * You can inject this service, typically in your root component, and customize the values of its properties in
 * order to provide default values for all the stickies used in the application.
 */
export class NgtStickyConfig {
    constructor() {
        this.zIndex = 10;
        this.width = 'auto';
        this.offsetTop = 0;
        this.offsetBottom = 0;
        this.start = 0;
        this.stickClass = 'sticky';
        this.endStickClass = 'sticky-end';
        this.mediaQuery = '';
        this.parentMode = true;
        this.orientation = 'none';
    }
}
NgtStickyConfig.decorators = [
    { type: Injectable, args: [{ providedIn: 'root' },] }
];
/** @nocollapse */ NgtStickyConfig.ngInjectableDef = i0.defineInjectable({ factory: function NgtStickyConfig_Factory() { return new NgtStickyConfig(); }, token: NgtStickyConfig, providedIn: "root" });
if (false) {
    /** @type {?} */
    NgtStickyConfig.prototype.sticky;
    /** @type {?} */
    NgtStickyConfig.prototype.zIndex;
    /** @type {?} */
    NgtStickyConfig.prototype.width;
    /** @type {?} */
    NgtStickyConfig.prototype.offsetTop;
    /** @type {?} */
    NgtStickyConfig.prototype.offsetBottom;
    /** @type {?} */
    NgtStickyConfig.prototype.start;
    /** @type {?} */
    NgtStickyConfig.prototype.stickClass;
    /** @type {?} */
    NgtStickyConfig.prototype.endStickClass;
    /** @type {?} */
    NgtStickyConfig.prototype.mediaQuery;
    /** @type {?} */
    NgtStickyConfig.prototype.parentMode;
    /** @type {?} */
    NgtStickyConfig.prototype.orientation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RpY2t5LWNvbmZpZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsic3RpY2t5L3N0aWNreS1jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7Ozs7QUFRM0MsTUFBTSxPQUFPLGVBQWU7SUFENUI7UUFHSSxXQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ1osVUFBSyxHQUFHLE1BQU0sQ0FBQztRQUNmLGNBQVMsR0FBRyxDQUFDLENBQUM7UUFDZCxpQkFBWSxHQUFHLENBQUMsQ0FBQztRQUNqQixVQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ1YsZUFBVSxHQUFHLFFBQVEsQ0FBQztRQUN0QixrQkFBYSxHQUFHLFlBQVksQ0FBQztRQUM3QixlQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFDbEIsZ0JBQVcsR0FBOEIsTUFBTSxDQUFDO0tBQ25EOzs7WUFiQSxVQUFVLFNBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDOzs7OztJQUU1QixpQ0FBZTs7SUFDZixpQ0FBWTs7SUFDWixnQ0FBZTs7SUFDZixvQ0FBYzs7SUFDZCx1Q0FBaUI7O0lBQ2pCLGdDQUFVOztJQUNWLHFDQUFzQjs7SUFDdEIsd0NBQTZCOztJQUM3QixxQ0FBZ0I7O0lBQ2hCLHFDQUFrQjs7SUFDbEIsc0NBQWdEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuLyoqXHJcbiAqIENvbmZpZ3VyYXRpb24gc2VydmljZSBmb3IgdGhlIE5ndFN0aWNreSBjb21wb25lbnQuXHJcbiAqIFlvdSBjYW4gaW5qZWN0IHRoaXMgc2VydmljZSwgdHlwaWNhbGx5IGluIHlvdXIgcm9vdCBjb21wb25lbnQsIGFuZCBjdXN0b21pemUgdGhlIHZhbHVlcyBvZiBpdHMgcHJvcGVydGllcyBpblxyXG4gKiBvcmRlciB0byBwcm92aWRlIGRlZmF1bHQgdmFsdWVzIGZvciBhbGwgdGhlIHN0aWNraWVzIHVzZWQgaW4gdGhlIGFwcGxpY2F0aW9uLlxyXG4gKi9cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBOZ3RTdGlja3lDb25maWcge1xyXG4gICAgc3RpY2t5OiBzdHJpbmc7XHJcbiAgICB6SW5kZXggPSAxMDtcclxuICAgIHdpZHRoID0gJ2F1dG8nO1xyXG4gICAgb2Zmc2V0VG9wID0gMDtcclxuICAgIG9mZnNldEJvdHRvbSA9IDA7XHJcbiAgICBzdGFydCA9IDA7XHJcbiAgICBzdGlja0NsYXNzID0gJ3N0aWNreSc7XHJcbiAgICBlbmRTdGlja0NsYXNzID0gJ3N0aWNreS1lbmQnO1xyXG4gICAgbWVkaWFRdWVyeSA9ICcnO1xyXG4gICAgcGFyZW50TW9kZSA9IHRydWU7XHJcbiAgICBvcmllbnRhdGlvbjogJ2xlZnQnIHwgJ3JpZ2h0JyB8ICdub25lJyA9ICdub25lJztcclxufVxyXG4iXX0=