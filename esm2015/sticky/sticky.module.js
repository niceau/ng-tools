/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgtSticky } from './sticky';
export { NgtSticky } from './sticky';
export { NgtStickyConfig } from './sticky-config';
/** @type {?} */
const NGT_STICKY_DIRECTIVES = [NgtSticky];
export class NgtStickyModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return { ngModule: NgtStickyModule };
    }
}
NgtStickyModule.decorators = [
    { type: NgModule, args: [{
                declarations: NGT_STICKY_DIRECTIVES,
                exports: NGT_STICKY_DIRECTIVES,
                imports: [CommonModule]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RpY2t5Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLXRvb2xzLyIsInNvdXJjZXMiOlsic3RpY2t5L3N0aWNreS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sVUFBVSxDQUFDO0FBRXJDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxVQUFVLENBQUM7QUFDckMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlCQUFpQixDQUFDOztNQUU1QyxxQkFBcUIsR0FBRyxDQUFDLFNBQVMsQ0FBQztBQU96QyxNQUFNLE9BQU8sZUFBZTs7OztJQUN4QixNQUFNLENBQUMsT0FBTztRQUNWLE9BQU8sRUFBQyxRQUFRLEVBQUUsZUFBZSxFQUFDLENBQUM7SUFDdkMsQ0FBQzs7O1lBUkosUUFBUSxTQUFDO2dCQUNOLFlBQVksRUFBRSxxQkFBcUI7Z0JBQ25DLE9BQU8sRUFBRSxxQkFBcUI7Z0JBQzlCLE9BQU8sRUFBRSxDQUFDLFlBQVksQ0FBQzthQUMxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcblxyXG5pbXBvcnQgeyBOZ3RTdGlja3kgfSBmcm9tICcuL3N0aWNreSc7XHJcblxyXG5leHBvcnQgeyBOZ3RTdGlja3kgfSBmcm9tICcuL3N0aWNreSc7XHJcbmV4cG9ydCB7IE5ndFN0aWNreUNvbmZpZyB9IGZyb20gJy4vc3RpY2t5LWNvbmZpZyc7XHJcblxyXG5jb25zdCBOR1RfU1RJQ0tZX0RJUkVDVElWRVMgPSBbTmd0U3RpY2t5XTtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IE5HVF9TVElDS1lfRElSRUNUSVZFUyxcclxuICAgIGV4cG9ydHM6IE5HVF9TVElDS1lfRElSRUNUSVZFUyxcclxuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGVdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZ3RTdGlja3lNb2R1bGUge1xyXG4gICAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICAgICAgcmV0dXJuIHtuZ01vZHVsZTogTmd0U3RpY2t5TW9kdWxlfTtcclxuICAgIH1cclxufVxyXG4iXX0=