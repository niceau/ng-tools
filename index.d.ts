import { ModuleWithProviders } from '@angular/core';
export { NgtAccordionModule, NgtAccordionConfig, NgtAccordionComponent, NgtAccordionPanelDirective, NgtAccordionPanelTitleDirective, NgtAccordionPanelContentDirective, NgtAccordionPanelHeaderDirective, NgtAccordionPanelToggleDirective, NgtAccordionPanelChangeEvent, } from './accordion/accordion.module';
export { NgtAlertModule, NgtAlertComponent, NgtAlertConfig } from './alert/alert.module';
export { NgtCollapseModule, NgtCollapseService, NgtCollapseDirective, NgtCollapseTriggerDirective } from './collapse/collapse.module';
export { NgtDropdownModule, NgtDropdownConfig, NgtDropdownDirective, NgtDropdownMenuDirective, NgtDropdownItemDirective, NgtDropdownAnchorDirective, NgtDropdownToggleDirective } from './dropdown/dropdown.module';
export { NgtLoaderModule, NgtLoader, NgtLoaderService, NgtLoaderComponent, NgtLoaderDirective } from './loader/loader.module';
export { NgtModalModule, NgtModalService, NgtModalConfig, NgtModalOptions, NgtActiveModal, NgtModalRef, ModalDismissReasons, NgtModalComponent, NgtModalOpenDirective, NgtModalCloseDirective } from './modal/modal.module';
export { NgtNotificationModule, NgtNotification, NgtNotificationService, NgtNotificationStack, NgtNotificationComponent, } from './notifications/notification.module';
export { NgtPaginationModule, NgtPagination, NgtPaginationFirst, NgtPaginationLast, NgtPaginationPrevious, NgtPaginationNext, NgtPaginationEllipsis, NgtPaginationNumber, NgtPaginationConfig, } from './pagination/pagination.module';
export { NgtPanelModule, NgtPanel, NgtPanelService, NgtPanelComponent, NgtPanelsComponent, NgtPanelOpenDirective, NgtPanelCloseDirective, NGC_PANEL_CONFIG, DEFAULT_NGC_PANEL_CONFIG, NgtPanelConfigInterface } from './panels/panel.module';
export { NgtPopoverModule, NgtPopover, NgtPopoverConfig } from './popover/popover.module';
export { NgtProgressbarModule, NgtProgressbar, NgtProgressbarConfig } from './progressbar/progressbar.module';
export { NgtScrollSpyModule, NgtScrollSpy } from './scroll-spy/scroll-spy.module';
export { NgtStickyModule, NgtSticky, NgtStickyConfig } from './sticky/sticky.module';
export { NgtTabsetModule, NgtTabset, NgtTab, NgtTabContent, NgtTabTitle, NgtTabChangeEvent, NgtTabsetConfig } from './tabset/tabset.module';
export { NgtTooltipModule, NgtTooltip, NgtTooltipConfig } from './tooltip/tooltip.module';
export declare class NgtModule {
    static forRoot(): ModuleWithProviders;
}
